//
// AbstractCatalogingRulesProxyManager.java
//
//     An adapter for a CatalogingRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CatalogingRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCatalogingRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.cataloging.rules.CatalogingRulesProxyManager>
    implements org.osid.cataloging.rules.CatalogingRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCatalogingRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCatalogingRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCatalogingRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogingRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerLookup() {
        return (getAdapteeManager().supportsCatalogEnablerLookup());
    }


    /**
     *  Tests if querying catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerQuery() {
        return (getAdapteeManager().supportsCatalogEnablerQuery());
    }


    /**
     *  Tests if searching catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSearch() {
        return (getAdapteeManager().supportsCatalogEnablerSearch());
    }


    /**
     *  Tests if a catalog enabler administrative service is supported. 
     *
     *  @return <code> true </code> if catalog enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerAdmin() {
        return (getAdapteeManager().supportsCatalogEnablerAdmin());
    }


    /**
     *  Tests if a catalog enabler notification service is supported. 
     *
     *  @return <code> true </code> if catalog enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerNotification() {
        return (getAdapteeManager().supportsCatalogEnablerNotification());
    }


    /**
     *  Tests if a catalog enabler catalog lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler catalog lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerCatalog() {
        return (getAdapteeManager().supportsCatalogEnablerCatalog());
    }


    /**
     *  Tests if a catalog enabler catalog service is supported. 
     *
     *  @return <code> true </code> if catalog enabler catalog assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerCatalogAssignment() {
        return (getAdapteeManager().supportsCatalogEnablerCatalogAssignment());
    }


    /**
     *  Tests if a catalog enabler catalog lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler catalog service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSmartCatalog() {
        return (getAdapteeManager().supportsCatalogEnablerSmartCatalog());
    }


    /**
     *  Tests if a catalog enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRuleLookup() {
        return (getAdapteeManager().supportsCatalogEnablerRuleLookup());
    }


    /**
     *  Tests if a catalog enabler rule application service is supported. 
     *
     *  @return <code> true </code> if catalog enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRuleApplication() {
        return (getAdapteeManager().supportsCatalogEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> CatalogEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CatalogEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogEnablerRecordTypes() {
        return (getAdapteeManager().getCatalogEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CatalogEnabler </code> record type is 
     *  supported. 
     *
     *  @param  catalogEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CatalogEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRecordType(org.osid.type.Type catalogEnablerRecordType) {
        return (getAdapteeManager().supportsCatalogEnablerRecordType(catalogEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CatalogEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> CatalogEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCatalogEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CatalogEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  catalogEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CatalogEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          catalogEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSearchRecordType(org.osid.type.Type catalogEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCatalogEnablerSearchRecordType(catalogEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerLookupSessionForCatalog(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerQuerySessionForCatalog(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enablers earch service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerSearchSessionForCatalog(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerAdminSessionForCatalog(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSession(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerNotificationSession(catalogEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service for the given catalog. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalog found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver, 
     *          catalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSessionForCatalog(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver, 
                                                                                                                      org.osid.id.Id catalogId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerNotificationSessionForCatalog(catalogEnablerReceiver, catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup catalog enabler/catalog 
     *  mappings for cataloging enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogSession getCatalogEnablerCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning catalog 
     *  enablers to catalogs for cataloging. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogAssignmentSession getCatalogEnablerCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage catalog enabler smart 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSmartCatalogSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSmartCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSmartCatalogSession getCatalogEnablerSmartCatalogSession(org.osid.id.Id catalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerSmartCatalogSession(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  catalog. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for the given catalog for looking up 
     *  rules applied to a catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerRuleLookupSessionForCatalog(catalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service to apply enablers to catalogs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service for the given catalog to apply enablers to 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogEnablerRuleApplicationSessionForCatalog(catalogId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
