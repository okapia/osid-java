//
// AbstractImmutableLocale.java
//
//     Wraps a mutable Locale to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.locale.locale.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Locale</code> to hide modifiers. This
 *  wrapper provides an immutized Locale from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying locale whose state changes are visible.
 */

public abstract class AbstractImmutableLocale
    implements org.osid.locale.Locale {

    private final org.osid.locale.Locale locale;


    /**
     *  Constructs a new <code>AbstractImmutableLocale</code>.
     *
     *  @param locale the locale to immutablize
     *  @throws org.osid.NullArgumentException <code>locale</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLocale(org.osid.locale.Locale locale) {
        this.locale = locale;
        return;
    }


    /**
     *  Gets the language <code>Type</code>. 
     *
     *  @return the language type 
     */

    @OSID @Override
    public org.osid.type.Type getLanguageType() {
        return (this.locale.getLanguageType());
    }


    /**
     *  Gets the script <code>Type</code>.
     *
     *  @return the script type 
     */

    @OSID @Override
    public org.osid.type.Type getScriptType() {
        return (this.locale.getScriptType());
    }


    /**
     *  Gets the calendar <code>Type</code>. 
     *
     *  @return the calendar type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarType() {
        return (this.locale.getCalendarType());
    }


    /**
     *  Gets the time <code>Type</code>. 
     *
     *  @return the time type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeType() {
        return (this.locale.getTimeType());
    }


    /**
     *  Gets the currency <code>Type</code>. 
     *
     *  @return the currency type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyType() {
        return (this.locale.getCurrencyType());
    }


    /**
     *  Gets the unit system <code>Type</code>. 
     *
     *  @return the unit system type 
     */

    @OSID @Override
    public org.osid.type.Type getUnitSystemType() {
        return (this.locale.getUnitSystemType());
    }


    /**
     *  Gets the numeric format <code>Type</code>. 
     *
     *  @return the numeric format type 
     */

    @OSID @Override
    public org.osid.type.Type getNumericFormatType() {
        return (this.locale.getNumericFormatType());
    }


    /**
     *  Gets the calendar format <code>Type</code>. 
     *
     *  @return the calendar format type 
     */

    @OSID @Override
    public org.osid.type.Type getCalendarFormatType() {
        return (this.locale.getCalendarFormatType());
    }


    /**
     *  Gets the time format <code>Type</code>. 
     *
     *  @return the time format type 
     */

    @OSID @Override
    public org.osid.type.Type getTimeFormatType() {
        return (this.locale.getTimeFormatType());
    }


    /**
     *  Gets the currency format <code>Type</code>. 
     *
     *  @return the currency format type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyFormatType() {
        return (this.locale.getCurrencyFormatType());
    }


    /**
     *  Gets the coordinate format <code>Type</code>. 
     *
     *  @return the coordinate format type 
     */

    @OSID @Override
    public org.osid.type.Type getCoordinateFormatType() {
        return (this.locale.getCoordinateFormatType());
    }
}

