//
// EntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.entry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class EntryElements
    extends net.okapia.osid.jamocha.spi.SourceableOsidObjectElements {


    /**
     *  Gets the EntryElement Id.
     *
     *  @return the entry element Id
     */

    public static org.osid.id.Id getEntryEntityId() {
        return (makeEntityId("osid.blogging.Entry"));
    }


    /**
     *  Gets the Timestamp element Id.
     *
     *  @return the Timestamp element Id
     */

    public static org.osid.id.Id getTimestamp() {
        return (makeElementId("osid.blogging.entry.Timestamp"));
    }


    /**
     *  Gets the PosterId element Id.
     *
     *  @return the PosterId element Id
     */

    public static org.osid.id.Id getPosterId() {
        return (makeElementId("osid.blogging.entry.PosterId"));
    }


    /**
     *  Gets the Poster element Id.
     *
     *  @return the Poster element Id
     */

    public static org.osid.id.Id getPoster() {
        return (makeElementId("osid.blogging.entry.Poster"));
    }


    /**
     *  Gets the PostingAgentId element Id.
     *
     *  @return the PostingAgentId element Id
     */

    public static org.osid.id.Id getPostingAgentId() {
        return (makeElementId("osid.blogging.entry.PostingAgentId"));
    }


    /**
     *  Gets the PostingAgent element Id.
     *
     *  @return the PostingAgent element Id
     */

    public static org.osid.id.Id getPostingAgent() {
        return (makeElementId("osid.blogging.entry.PostingAgent"));
    }


    /**
     *  Gets the SubjectLine element Id.
     *
     *  @return the SubjectLine element Id
     */

    public static org.osid.id.Id getSubjectLine() {
        return (makeElementId("osid.blogging.entry.SubjectLine"));
    }


    /**
     *  Gets the Summary element Id.
     *
     *  @return the Summary element Id
     */

    public static org.osid.id.Id getSummary() {
        return (makeElementId("osid.blogging.entry.Summary"));
    }


    /**
     *  Gets the Text element Id.
     *
     *  @return the Text element Id
     */

    public static org.osid.id.Id getText() {
        return (makeElementId("osid.blogging.entry.Text"));
    }


    /**
     *  Gets the Copyright element Id.
     *
     *  @return the Copyright element Id
     */

    public static org.osid.id.Id getCopyright() {
        return (makeElementId("osid.blogging.entry.Copyright"));
    }


    /**
     *  Gets the BlogId element Id.
     *
     *  @return the BlogId element Id
     */

    public static org.osid.id.Id getBlogId() {
        return (makeQueryElementId("osid.blogging.entry.BlogId"));
    }


    /**
     *  Gets the Blog element Id.
     *
     *  @return the Blog element Id
     */

    public static org.osid.id.Id getBlog() {
        return (makeQueryElementId("osid.blogging.entry.Blog"));
    }
}
