//
// InvariantMapAwardEntryLookupSession
//
//    Implements an AwardEntry lookup service backed by a fixed collection of
//    awardEntries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements an AwardEntry lookup service backed by a fixed
 *  collection of award entries. The award entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAwardEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapAwardEntryLookupSession
    implements org.osid.course.chronicle.AwardEntryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAwardEntryLookupSession</code> with no
     *  award entries.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAwardEntryLookupSession</code> with a single
     *  award entry.
     *  
     *  @param courseCatalog the course catalog
     *  @param awardEntry an single award entry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code awardEntry} is <code>null</code>
     */

      public InvariantMapAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.AwardEntry awardEntry) {
        this(courseCatalog);
        putAwardEntry(awardEntry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAwardEntryLookupSession</code> using an array
     *  of award entries.
     *  
     *  @param courseCatalog the course catalog
     *  @param awardEntries an array of award entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code awardEntries} is <code>null</code>
     */

      public InvariantMapAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.AwardEntry[] awardEntries) {
        this(courseCatalog);
        putAwardEntries(awardEntries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAwardEntryLookupSession</code> using a
     *  collection of award entries.
     *
     *  @param courseCatalog the course catalog
     *  @param awardEntries a collection of award entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code awardEntries} is <code>null</code>
     */

      public InvariantMapAwardEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.chronicle.AwardEntry> awardEntries) {
        this(courseCatalog);
        putAwardEntries(awardEntries);
        return;
    }
}
