//
// AbstractIssueValidator.java
//
//     Validates an Issue.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.tracking.issue.spi;


/**
 *  Validates an Issue.
 */

public abstract class AbstractIssueValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractIssueValidator</code>.
     */

    protected AbstractIssueValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractIssueValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractIssueValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Issue.
     *
     *  @param issue an issue to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>issue</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.tracking.Issue issue) {
        super.validate(issue);

        testNestedObject(issue, "getQueue");
        testNestedObject(issue, "getCustomer");
        testNestedObject(issue, "getTopic");
        testNestedObject(issue, "getMasterIssue");
        
        testConditionalMethod(issue, "getDuplicateIssueIds", issue.isDuplicate(), "isDuplicate()");
        testConditionalMethod(issue, "getDuplicateIssues", issue.isDuplicate(), "isDuplicate()");
        if (issue.isDuplicate()) {                       
            testNestedObjects(issue, "getDuplicateIssueIds", "getDuplicateIssues");
        }

        testConditionalMethod(issue, "getBranchedIssueId", issue.isBranched(), "isBranched()");
        testConditionalMethod(issue, "getBranchedIssue", issue.isBranched(), "isBranched()");
        if (issue.isBranched()) {                       
            testNestedObject(issue, "getBranchedIssue");
        }


        testNestedObject(issue, "getRootIssue");
        test(issue.getPriorityType(), "getPriorityType()");
        testNestedObject(issue, "getCreator");
        testNestedObject(issue, "getCreatingAgent");
        test(issue.getCreatedDate(), "getCreatedDate()");

        testConditionalMethod(issue, "getReopenerId", issue.isReopened(), "isReopened()");
        testConditionalMethod(issue, "getReopener", issue.isReopened(), "isReopened()");
        if (issue.isReopened()) {
            testNestedObject(issue, "getReopener");
        }
            
        testConditionalMethod(issue, "getReopeningAgentId", issue.isReopened(), "isReopened()");
        testConditionalMethod(issue, "getReopeningAgent", issue.isReopened(), "isReopened()");
        if (issue.isReopened()) {
            testNestedObject(issue, "getReopeningAgent");
        }

        testConditionalMethod(issue, "getReopenedDate", issue.isReopened(), "isReopened()");
        testConditionalMethod(issue, "getDueDate", issue.hasDueDate(), "hasDueDate()");

        testConditionalMethod(issue, "getBlockerIds", issue.isBlocked(), "isBlocked()");
        testConditionalMethod(issue, "getBlockers", issue.isBlocked(), "isBlocked()");
        if (issue.isBlocked()) {
            testNestedObjects(issue, "getBlockerIds", "getBlockers");
        }

        testConditionalMethod(issue, "getResolverId", issue.isResolved(), "isResolved()");
        testConditionalMethod(issue, "getResolver", issue.isResolved(), "isResolved()");
        if (issue.isResolved()) {
            testNestedObject(issue, "getResolver");
        }

        testConditionalMethod(issue, "getResolvingAgentId", issue.isResolved(), "isResolved()");
        testConditionalMethod(issue, "getResolvingAgent", issue.isResolved(), "isResolved()");
        if (issue.isResolved()) {
            testNestedObject(issue, "getResolvingAgent");
        }

        testConditionalMethod(issue, "getResolvedDate", issue.isResolved(), "isResolved()");
        testConditionalMethod(issue, "getResolutionType", issue.isResolved(), "isResolved()");

        testConditionalMethod(issue, "getCloserId", issue.isClosed(), "isClosed()");
        testConditionalMethod(issue, "getCloser", issue.isClosed(), "isClosed()");
        if (issue.isClosed()) {
            testNestedObject(issue, "getCloser");
        }

        testConditionalMethod(issue, "getClosingAgentId", issue.isClosed(), "isClosed()");
        testConditionalMethod(issue, "getClosingAgent", issue.isClosed(), "isClosed()");
        if (issue.isClosed()) {
            testNestedObject(issue, "getClosingAgent");
        }

        testConditionalMethod(issue, "getClosedDate", issue.isClosed(), "isClosed()");

        testConditionalMethod(issue, "getAssignedResourceId", issue.isAssigned(), "isAssigned()");
        testConditionalMethod(issue, "getAssignedResource", issue.isAssigned(), "isAssigned()");
        if (issue.isAssigned()) {
            testNestedObject(issue, "getAssignedResource");
        }

        return;
    }
}
