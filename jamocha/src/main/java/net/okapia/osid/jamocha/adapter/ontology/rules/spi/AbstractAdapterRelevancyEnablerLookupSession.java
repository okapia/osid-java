//
// AbstractAdapterRelevancyEnablerLookupSession.java
//
//    A RelevancyEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RelevancyEnabler lookup session adapter.
 */

public abstract class AbstractAdapterRelevancyEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {

    private final org.osid.ontology.rules.RelevancyEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRelevancyEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelevancyEnablerLookupSession(org.osid.ontology.rules.RelevancyEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Ontology/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Ontology Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.session.getOntologyId());
    }


    /**
     *  Gets the {@code Ontology} associated with this session.
     *
     *  @return the {@code Ontology} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOntology());
    }


    /**
     *  Tests if this user can perform {@code RelevancyEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRelevancyEnablers() {
        return (this.session.canLookupRelevancyEnablers());
    }


    /**
     *  A complete view of the {@code RelevancyEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelevancyEnablerView() {
        this.session.useComparativeRelevancyEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code RelevancyEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelevancyEnablerView() {
        this.session.usePlenaryRelevancyEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancy enablers in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.session.useFederatedOntologyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.session.useIsolatedOntologyView();
        return;
    }
    

    /**
     *  Only active relevancy enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRelevancyEnablerView() {
        this.session.useActiveRelevancyEnablerView();
        return;
    }


    /**
     *  Active and inactive relevancy enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRelevancyEnablerView() {
        this.session.useAnyStatusRelevancyEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code RelevancyEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RelevancyEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RelevancyEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param relevancyEnablerId {@code Id} of the {@code RelevancyEnabler}
     *  @return the relevancy enabler
     *  @throws org.osid.NotFoundException {@code relevancyEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code relevancyEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnabler getRelevancyEnabler(org.osid.id.Id relevancyEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnabler(relevancyEnablerId));
    }


    /**
     *  Gets a {@code RelevancyEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relevancyEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RelevancyEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  relevancyEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RelevancyEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByIds(org.osid.id.IdList relevancyEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablersByIds(relevancyEnablerIds));
    }


    /**
     *  Gets a {@code RelevancyEnablerList} corresponding to the given
     *  relevancy enabler genus {@code Type} which does not include
     *  relevancy enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  relevancyEnablerGenusType a relevancyEnabler genus type 
     *  @return the returned {@code RelevancyEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByGenusType(org.osid.type.Type relevancyEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablersByGenusType(relevancyEnablerGenusType));
    }


    /**
     *  Gets a {@code RelevancyEnablerList} corresponding to the given
     *  relevancy enabler genus {@code Type} and include any additional
     *  relevancy enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  relevancyEnablerGenusType a relevancyEnabler genus type 
     *  @return the returned {@code RelevancyEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByParentGenusType(org.osid.type.Type relevancyEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablersByParentGenusType(relevancyEnablerGenusType));
    }


    /**
     *  Gets a {@code RelevancyEnablerList} containing the given
     *  relevancy enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  relevancyEnablerRecordType a relevancyEnabler record type 
     *  @return the returned {@code RelevancyEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code relevancyEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByRecordType(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablersByRecordType(relevancyEnablerRecordType));
    }


    /**
     *  Gets a {@code RelevancyEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible
     *  through this session.
     *  
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RelevancyEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code RelevancyEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible
     *  through this session.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code RelevancyEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getRelevancyEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code RelevancyEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  relevancy enablers or an error results. Otherwise, the returned list
     *  may contain only those relevancy enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, relevancy enablers are returned that are currently
     *  active. In any status mode, active and inactive relevancy enablers
     *  are returned.
     *
     *  @return a list of {@code RelevancyEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRelevancyEnablers());
    }
}
