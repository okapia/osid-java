//
// InvariantMapCandidateLookupSession
//
//    Implements a Candidate lookup service backed by a fixed collection of
//    candidates.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Candidate lookup service backed by a fixed
 *  collection of candidates. The candidates are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCandidateLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCandidateLookupSession</code> with no
     *  candidates.
     *  
     *  @param polls the polls
     *  @throws org.osid.NullArgumnetException {@code polls} is
     *          {@code null}
     */

    public InvariantMapCandidateLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCandidateLookupSession</code> with a single
     *  candidate.
     *  
     *  @param polls the polls
     *  @param candidate a single candidate
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidate} is <code>null</code>
     */

      public InvariantMapCandidateLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.Candidate candidate) {
        this(polls);
        putCandidate(candidate);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCandidateLookupSession</code> using an array
     *  of candidates.
     *  
     *  @param polls the polls
     *  @param candidates an array of candidates
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidates} is <code>null</code>
     */

      public InvariantMapCandidateLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.Candidate[] candidates) {
        this(polls);
        putCandidates(candidates);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCandidateLookupSession</code> using a
     *  collection of candidates.
     *
     *  @param polls the polls
     *  @param candidates a collection of candidates
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidates} is <code>null</code>
     */

      public InvariantMapCandidateLookupSession(org.osid.voting.Polls polls,
                                               java.util.Collection<? extends org.osid.voting.Candidate> candidates) {
        this(polls);
        putCandidates(candidates);
        return;
    }
}
