//
// AbstractOsidRuleQuery
//
//     Defines an OsidRuleQuery.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidRuleQuery.
 */

public abstract class AbstractOsidRuleQuery
    extends AbstractOperableOsidObjectQuery
    implements org.osid.OsidRuleQuery {

    
    /**
     *  Match the <code> Id </code> of the rule. 
     *
     *  @param  ruleId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuleId(org.osid.id.Id ruleId, boolean match) {
        return;
    }


    /**
     *  Clears all rule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRuleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RuleQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if a rule query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleQuery() {
        return (false);
    }

    
    /**
     *  Gets the query for the rule. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the rule query 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuery getRuleQuery(boolean match) {
        throw new org.osid.UnsupportedException("supportsRuleQuery() is false");
    }


    /**
     *  Match any associated rule. 
     *
     *  @param  match <code> true </code> to match any rule, <code> false 
     *          </code> to match no rules 
     */

    @OSID @Override
    public void matchAnyRule(boolean match) {
        return;
    }


    /**
     *  Clears all rule terms. 
     */

    @OSID @Override
    public void clearRuleTerms() {
        return;
    }
}
