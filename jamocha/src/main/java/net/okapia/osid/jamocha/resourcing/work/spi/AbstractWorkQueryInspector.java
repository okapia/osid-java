//
// AbstractWorkQueryInspector.java
//
//     A template for making a WorkQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for works.
 */

public abstract class AbstractWorkQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.resourcing.WorkQueryInspector {

    private final java.util.Collection<org.osid.resourcing.records.WorkQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJobIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the created date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the completion date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCompletionDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the commission <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommissionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the commission query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CommissionQueryInspector[] getCommissionTerms() {
        return (new org.osid.resourcing.CommissionQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given work query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a work implementing the requested record.
     *
     *  @param workRecordType a work record type
     *  @return the work query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(workRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkQueryInspectorRecord getWorkQueryInspectorRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.WorkQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(workRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(workRecordType + " is not supported");
    }


    /**
     *  Adds a record to this work query. 
     *
     *  @param workQueryInspectorRecord work query inspector
     *         record
     *  @param workRecordType work record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addWorkQueryInspectorRecord(org.osid.resourcing.records.WorkQueryInspectorRecord workQueryInspectorRecord, 
                                                   org.osid.type.Type workRecordType) {

        addRecordType(workRecordType);
        nullarg(workRecordType, "work record type");
        this.records.add(workQueryInspectorRecord);        
        return;
    }
}
