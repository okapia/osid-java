//
// AbstractMapStateLookupSession
//
//    A simple framework for providing a State lookup service
//    backed by a fixed collection of states.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.IdMultiHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Simple implementation of a State lookup service backed by a
 *  fixed collection of states. The states are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>States</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStateLookupSession
    extends net.okapia.osid.jamocha.process.spi.AbstractStateLookupSession
    implements org.osid.process.StateLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.process.State> states = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.process.State>());
    private final MultiMap<org.osid.id.Id, org.osid.id.Id> validStates = new SynchronizedMultiMap<>(new IdMultiHashMap<org.osid.id.Id>());


    /**
     *  Makes a <code>State</code> available in this session.
     *
     *  @param  state a state
     *  @throws org.osid.NullArgumentException <code>state<code>
     *          is <code>null</code>
     */

    protected void putState(org.osid.process.State state) {
        this.states.put(state.getId(), state);
        return;
    }


    /**
     *  Makes an array of states available in this session.
     *
     *  @param  states an array of states
     *  @throws org.osid.NullArgumentException <code>states<code>
     *          is <code>null</code>
     */

    protected void putStates(org.osid.process.State[] states) {
        putStates(java.util.Arrays.asList(states));
        return;
    }


    /**
     *  Makes a collection of states available in this session.
     *
     *  @param  states a collection of states
     *  @throws org.osid.NullArgumentException <code>states<code>
     *          is <code>null</code>
     */

    protected void putStates(java.util.Collection<? extends org.osid.process.State> states) {
        for (org.osid.process.State state : states) {
            this.states.put(state.getId(), state);
        }

        return;
    }


    /**
     *  Removes a State from this session.
     *
     *  @param  stateId the <code>Id</code> of the state
     *  @throws org.osid.NullArgumentException <code>stateId<code> is
     *          <code>null</code>
     */

    protected void removeState(org.osid.id.Id stateId) {
        this.states.remove(stateId);
        return;
    }


    /**
     *  Adds a next valid state to an existing state.
     *
     *  @param stateId a state Id
     *  @param nextStateId a valid next state Id
     *  @throws org.osid.NullArgumentException <code>stateId<code> or
     *          <code>nextStateId</code> is <code>null</code>
     */

    protected void addNextValidState(org.osid.id.Id stateId, org.osid.id.Id nextStateId) {
        this.validStates.put(stateId, nextStateId);
        return;
    }


    /**
     *  Removes a next valid state.
     *
     *  @param stateId a state Id
     *  @param nextStateId a valid next state Id
     *  @throws org.osid.NullArgumentException <code>stateId<code> or
     *          <code>nextStateId</code> is <code>null</code>
     */

    protected void removeState(org.osid.id.Id stateId, org.osid.id.Id nextStateId) {
        this.validStates.remove(stateId, nextStateId);
        return;
    }


    /**
     *  Gets the <code>State</code> specified by its <code>Id</code>.
     *
     *  @param  stateId <code>Id</code> of the <code>State</code>
     *  @return the state
     *  @throws org.osid.NotFoundException <code>stateId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stateId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.State getState(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.process.State state = this.states.get(stateId);
        if (state == null) {
            throw new org.osid.NotFoundException("state not found: " + stateId);
        }

        return (state);
    }


    /**
     *  Gets all <code>States</code>. In plenary mode, the returned
     *  list contains all known states or an error
     *  results. Otherwise, the returned list may contain only those
     *  states that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>States</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.states.values()));
    }



    /**
     *  Gets the next valid states for the given state. 
     *
     *  @param  stateId a state <code>Id</code> 
     *  @return the valid next <code>States</code> 
     *  @throws org.osid.NotFoundException <code>stateId</code> is not found 
     *  @throws org.osid.NullArgumentException <code>stateId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.process.StateList getValidNextStates(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStatesByIds(new net.okapia.osid.jamocha.id.id.ArrayIdList(this.validStates.get(stateId))));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.states.clear();
        this.validStates.clear();

        super.close();
        return;
    }
}
