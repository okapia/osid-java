//
// AbstractAwardQueryInspector.java
//
//     A template for making an AwardQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.award.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for awards.
 */

public abstract class AbstractAwardQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.recognition.AwardQueryInspector {

    private final java.util.Collection<org.osid.recognition.records.AwardQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given award query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an award implementing the requested record.
     *
     *  @param awardRecordType an award record type
     *  @return the award query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>awardRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.AwardQueryInspectorRecord getAwardQueryInspectorRecord(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AwardQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(awardRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award query. 
     *
     *  @param awardQueryInspectorRecord award query inspector
     *         record
     *  @param awardRecordType award record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAwardQueryInspectorRecord(org.osid.recognition.records.AwardQueryInspectorRecord awardQueryInspectorRecord, 
                                                   org.osid.type.Type awardRecordType) {

        addRecordType(awardRecordType);
        nullarg(awardRecordType, "award record type");
        this.records.add(awardQueryInspectorRecord);        
        return;
    }
}
