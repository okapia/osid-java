//
// InvariantMapProxyRenovationLookupSession
//
//    Implements a Renovation lookup service backed by a fixed
//    collection of renovations. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction;


/**
 *  Implements a Renovation lookup service backed by a fixed
 *  collection of renovations. The renovations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRenovationLookupSession
    extends net.okapia.osid.jamocha.core.room.construction.spi.AbstractMapRenovationLookupSession
    implements org.osid.room.construction.RenovationLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRenovationLookupSession} with no
     *  renovations.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRenovationLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyRenovationLookupSession} with a single
     *  renovation.
     *
     *  @param campus the campus
     *  @param renovation a single renovation
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code renovation} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRenovationLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.construction.Renovation renovation, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRenovation(renovation);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRenovationLookupSession} using
     *  an array of renovations.
     *
     *  @param campus the campus
     *  @param renovations an array of renovations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code renovations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRenovationLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.construction.Renovation[] renovations, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRenovations(renovations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRenovationLookupSession} using a
     *  collection of renovations.
     *
     *  @param campus the campus
     *  @param renovations a collection of renovations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code renovations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRenovationLookupSession(org.osid.room.Campus campus,
                                                  java.util.Collection<? extends org.osid.room.construction.Renovation> renovations,
                                                  org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putRenovations(renovations);
        return;
    }
}
