//
// AbstractFederatingEdgeEnablerLookupSession.java
//
//     An abstract federating adapter for an EdgeEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EdgeEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.topology.rules.EdgeEnablerLookupSession>
    implements org.osid.topology.rules.EdgeEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Constructs a new <code>AbstractFederatingEdgeEnablerLookupSession</code>.
     */

    protected AbstractFederatingEdgeEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.topology.rules.EdgeEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>EdgeEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEdgeEnablers() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            if (session.canLookupEdgeEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>EdgeEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEdgeEnablerView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.useComparativeEdgeEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>EdgeEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEdgeEnablerView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.usePlenaryEdgeEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edge enablers in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.useFederatedGraphView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.useIsolatedGraphView();
        }

        return;
    }


    /**
     *  Only active edge enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveEdgeEnablerView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.useActiveEdgeEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive edge enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusEdgeEnablerView() {
        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            session.useAnyStatusEdgeEnablerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>EdgeEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>EdgeEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>EdgeEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerId <code>Id</code> of the
     *          <code>EdgeEnabler</code>
     *  @return the edge enabler
     *  @throws org.osid.NotFoundException <code>edgeEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>edgeEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler getEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            try {
                return (session.getEdgeEnabler(edgeEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(edgeEnablerId + " not found");
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  edgeEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>EdgeEnablers</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByIds(org.osid.id.IdList edgeEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.topology.rules.edgeenabler.MutableEdgeEnablerList ret = new net.okapia.osid.jamocha.topology.rules.edgeenabler.MutableEdgeEnablerList();

        try (org.osid.id.IdList ids = edgeEnablerIds) {
            while (ids.hasNext()) {
                ret.addEdgeEnabler(getEdgeEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the given
     *  edge enabler genus <code>Type</code> which does not include
     *  edge enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablersByGenusType(edgeEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the given
     *  edge enabler genus <code>Type</code> and include any additional
     *  edge enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByParentGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablersByParentGenusType(edgeEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> containing the given
     *  edge enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerRecordType an edgeEnabler record type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByRecordType(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablersByRecordType(edgeEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible
     *  through this session.
     *  
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>EdgeEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>EdgeEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible
     *  through this session.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>EdgeEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  edge enablers or an error results. Otherwise, the returned list
     *  may contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @return a list of <code>EdgeEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList ret = getEdgeEnablerList();

        for (org.osid.topology.rules.EdgeEnablerLookupSession session : getSessions()) {
            ret.addEdgeEnablerList(session.getEdgeEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.FederatingEdgeEnablerList getEdgeEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.ParallelEdgeEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.rules.edgeenabler.CompositeEdgeEnablerList());
        }
    }
}
