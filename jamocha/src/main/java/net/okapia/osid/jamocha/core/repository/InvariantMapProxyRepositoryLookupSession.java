//
// InvariantMapProxyRepositoryLookupSession
//
//    Implements a Repository lookup service backed by a fixed
//    collection of repositories. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Repository lookup service backed by a fixed
 *  collection of repositories. The repositories are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRepositoryLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRepositoryLookupSession} with no
     *  repositories.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyRepositoryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRepositoryLookupSession} with a
     *  single repository.
     *
     *  @param repository a single repository
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRepositoryLookupSession(org.osid.repository.Repository repository, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepository(repository);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRepositoryLookupSession} using
     *  an array of repositories.
     *
     *  @param repositories an array of repositories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repositories} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRepositoryLookupSession(org.osid.repository.Repository[] repositories, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepositories(repositories);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRepositoryLookupSession} using a
     *  collection of repositories.
     *
     *  @param repositories a collection of repositories
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repositories} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRepositoryLookupSession(java.util.Collection<? extends org.osid.repository.Repository> repositories,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putRepositories(repositories);
        return;
    }
}
