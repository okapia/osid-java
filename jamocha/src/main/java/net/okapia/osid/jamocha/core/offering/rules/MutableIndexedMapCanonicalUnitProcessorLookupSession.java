//
// MutableIndexedMapCanonicalUnitProcessorLookupSession
//
//    Implements a CanonicalUnitProcessor lookup service backed by a collection of
//    canonicalUnitProcessors indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitProcessor lookup service backed by a collection of
 *  canonical unit processors. The canonical unit processors are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some canonical unit processors may be compatible
 *  with more types than are indicated through these canonical unit processor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of canonical unit processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCanonicalUnitProcessorLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractIndexedMapCanonicalUnitProcessorLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCanonicalUnitProcessorLookupSession} with no canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

      public MutableIndexedMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitProcessorLookupSession} with a
     *  single canonical unit processor.
     *  
     *  @param catalogue the catalogue
     *  @param  canonicalUnitProcessor a single canonicalUnitProcessor
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessor} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        this(catalogue);
        putCanonicalUnitProcessor(canonicalUnitProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitProcessorLookupSession} using an
     *  array of canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessors} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        this(catalogue);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitProcessorLookupSession} using a
     *  collection of canonical unit processors.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnitProcessors a collection of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessors} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitProcessorLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {

        this(catalogue);
        putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }
    

    /**
     *  Makes a {@code CanonicalUnitProcessor} available in this session.
     *
     *  @param  canonicalUnitProcessor a canonical unit processor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitProcessor(org.osid.offering.rules.CanonicalUnitProcessor canonicalUnitProcessor) {
        super.putCanonicalUnitProcessor(canonicalUnitProcessor);
        return;
    }


    /**
     *  Makes an array of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors an array of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitProcessors(org.osid.offering.rules.CanonicalUnitProcessor[] canonicalUnitProcessors) {
        super.putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Makes collection of canonical unit processors available in this session.
     *
     *  @param  canonicalUnitProcessors a collection of canonical unit processors
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessor{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitProcessors(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessor> canonicalUnitProcessors) {
        super.putCanonicalUnitProcessors(canonicalUnitProcessors);
        return;
    }


    /**
     *  Removes a CanonicalUnitProcessor from this session.
     *
     *  @param canonicalUnitProcessorId the {@code Id} of the canonical unit processor
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCanonicalUnitProcessor(org.osid.id.Id canonicalUnitProcessorId) {
        super.removeCanonicalUnitProcessor(canonicalUnitProcessorId);
        return;
    }    
}
