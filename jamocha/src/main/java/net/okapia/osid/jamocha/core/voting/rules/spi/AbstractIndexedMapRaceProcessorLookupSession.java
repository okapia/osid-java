//
// AbstractIndexedMapRaceProcessorLookupSession.java
//
//    A simple framework for providing a RaceProcessor lookup service
//    backed by a fixed collection of race processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RaceProcessor lookup service backed by a
 *  fixed collection of race processors. The race processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some race processors may be compatible
 *  with more types than are indicated through these race processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRaceProcessorLookupSession
    extends AbstractMapRaceProcessorLookupSession
    implements org.osid.voting.rules.RaceProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceProcessor> raceProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceProcessor> raceProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceProcessor>());


    /**
     *  Makes a <code>RaceProcessor</code> available in this session.
     *
     *  @param  raceProcessor a race processor
     *  @throws org.osid.NullArgumentException <code>raceProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRaceProcessor(org.osid.voting.rules.RaceProcessor raceProcessor) {
        super.putRaceProcessor(raceProcessor);

        this.raceProcessorsByGenus.put(raceProcessor.getGenusType(), raceProcessor);
        
        try (org.osid.type.TypeList types = raceProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceProcessorsByRecord.put(types.getNextType(), raceProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a race processor from this session.
     *
     *  @param raceProcessorId the <code>Id</code> of the race processor
     *  @throws org.osid.NullArgumentException <code>raceProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRaceProcessor(org.osid.id.Id raceProcessorId) {
        org.osid.voting.rules.RaceProcessor raceProcessor;
        try {
            raceProcessor = getRaceProcessor(raceProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.raceProcessorsByGenus.remove(raceProcessor.getGenusType());

        try (org.osid.type.TypeList types = raceProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceProcessorsByRecord.remove(types.getNextType(), raceProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRaceProcessor(raceProcessorId);
        return;
    }


    /**
     *  Gets a <code>RaceProcessorList</code> corresponding to the given
     *  race processor genus <code>Type</code> which does not include
     *  race processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known race processors or an error results. Otherwise,
     *  the returned list may contain only those race processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  raceProcessorGenusType a race processor genus type 
     *  @return the returned <code>RaceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessor.ArrayRaceProcessorList(this.raceProcessorsByGenus.get(raceProcessorGenusType)));
    }


    /**
     *  Gets a <code>RaceProcessorList</code> containing the given
     *  race processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known race processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  race processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  raceProcessorRecordType a race processor record type 
     *  @return the returned <code>raceProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByRecordType(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessor.ArrayRaceProcessorList(this.raceProcessorsByRecord.get(raceProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceProcessorsByGenus.clear();
        this.raceProcessorsByRecord.clear();

        super.close();

        return;
    }
}
