//
// MutableMapDirectoryLookupSession
//
//    Implements a Directory lookup service backed by a collection of
//    directories that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a Directory lookup service backed by a collection of
 *  directories. The directories are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of directories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapDirectoryLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractMapDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {


    /**
     *  Constructs a new {@code MutableMapDirectoryLookupSession}
     *  with no directories.
     */

    public MutableMapDirectoryLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectoryLookupSession} with a
     *  single directory.
     *  
     *  @param directory a directory
     *  @throws org.osid.NullArgumentException {@code directory}
     *          is {@code null}
     */

    public MutableMapDirectoryLookupSession(org.osid.filing.Directory directory) {
        putDirectory(directory);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectoryLookupSession}
     *  using an array of directories.
     *
     *  @param directories an array of directories
     *  @throws org.osid.NullArgumentException {@code directories}
     *          is {@code null}
     */

    public MutableMapDirectoryLookupSession(org.osid.filing.Directory[] directories) {
        putDirectories(directories);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDirectoryLookupSession}
     *  using a collection of directories.
     *
     *  @param directories a collection of directories
     *  @throws org.osid.NullArgumentException {@code directories}
     *          is {@code null}
     */

    public MutableMapDirectoryLookupSession(java.util.Collection<? extends org.osid.filing.Directory> directories) {
        putDirectories(directories);
        return;
    }

    
    /**
     *  Makes a {@code Directory} available in this session.
     *
     *  @param directory a directory
     *  @throws org.osid.NullArgumentException {@code directory{@code  is
     *          {@code null}
     */

    @Override
    public void putDirectory(org.osid.filing.Directory directory) {
        super.putDirectory(directory);
        return;
    }


    /**
     *  Makes an array of directories available in this session.
     *
     *  @param directories an array of directories
     *  @throws org.osid.NullArgumentException {@code directories{@code 
     *          is {@code null}
     */

    @Override
    public void putDirectories(org.osid.filing.Directory[] directories) {
        super.putDirectories(directories);
        return;
    }


    /**
     *  Makes collection of directories available in this session.
     *
     *  @param directories a collection of directories
     *  @throws org.osid.NullArgumentException {@code directories{@code  is
     *          {@code null}
     */

    @Override
    public void putDirectories(java.util.Collection<? extends org.osid.filing.Directory> directories) {
        super.putDirectories(directories);
        return;
    }


    /**
     *  Removes a Directory from this session.
     *
     *  @param directoryId the {@code Id} of the directory
     *  @throws org.osid.NullArgumentException {@code directoryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeDirectory(org.osid.id.Id directoryId) {
        super.removeDirectory(directoryId);
        return;
    }    
}
