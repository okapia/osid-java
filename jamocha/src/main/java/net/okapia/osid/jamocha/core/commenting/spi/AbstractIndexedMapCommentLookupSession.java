//
// AbstractIndexedMapCommentLookupSession.java
//
//    A simple framework for providing a Comment lookup service
//    backed by a fixed collection of comments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Comment lookup service backed by a
 *  fixed collection of comments. The comments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some comments may be compatible
 *  with more types than are indicated through these comment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Comments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCommentLookupSession
    extends AbstractMapCommentLookupSession
    implements org.osid.commenting.CommentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.commenting.Comment> commentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.commenting.Comment>());
    private final MultiMap<org.osid.type.Type, org.osid.commenting.Comment> commentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.commenting.Comment>());


    /**
     *  Makes a <code>Comment</code> available in this session.
     *
     *  @param  comment a comment
     *  @throws org.osid.NullArgumentException <code>comment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putComment(org.osid.commenting.Comment comment) {
        super.putComment(comment);

        this.commentsByGenus.put(comment.getGenusType(), comment);
        
        try (org.osid.type.TypeList types = comment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commentsByRecord.put(types.getNextType(), comment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a comment from this session.
     *
     *  @param commentId the <code>Id</code> of the comment
     *  @throws org.osid.NullArgumentException <code>commentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeComment(org.osid.id.Id commentId) {
        org.osid.commenting.Comment comment;
        try {
            comment = getComment(commentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.commentsByGenus.remove(comment.getGenusType());

        try (org.osid.type.TypeList types = comment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commentsByRecord.remove(types.getNextType(), comment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeComment(commentId);
        return;
    }


    /**
     *  Gets a <code>CommentList</code> corresponding to the given
     *  comment genus <code>Type</code> which does not include
     *  comments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known comments or an error results. Otherwise,
     *  the returned list may contain only those comments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  commentGenusType a comment genus type 
     *  @return the returned <code>Comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByGenusType(org.osid.type.Type commentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.comment.ArrayCommentList(this.commentsByGenus.get(commentGenusType)));
    }


    /**
     *  Gets a <code>CommentList</code> containing the given
     *  comment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known comments or an error
     *  results. Otherwise, the returned list may contain only those
     *  comments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  commentRecordType a comment record type 
     *  @return the returned <code>comment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.CommentList getCommentsByRecordType(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.comment.ArrayCommentList(this.commentsByRecord.get(commentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commentsByGenus.clear();
        this.commentsByRecord.clear();

        super.close();

        return;
    }
}
