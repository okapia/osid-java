//
// AbstractInquestLookupSession.java
//
//    A starter implementation framework for providing an Inquest
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Inquest
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getInquests(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractInquestLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inquiry.InquestLookupSession {

    private boolean pedantic      = false;
    private org.osid.inquiry.Inquest inquest = new net.okapia.osid.jamocha.nil.inquiry.inquest.UnknownInquest();
    


    /**
     *  Tests if this user can perform <code>Inquest</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInquests() {
        return (true);
    }


    /**
     *  A complete view of the <code>Inquest</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquestView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Inquest</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquestView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Inquest</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inquest</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inquest</code> and
     *  retained for compatibility.
     *
     *  @param  inquestId <code>Id</code> of the
     *          <code>Inquest</code>
     *  @return the inquest
     *  @throws org.osid.NotFoundException <code>inquestId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inquestId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inquiry.InquestList inquests = getInquests()) {
            while (inquests.hasNext()) {
                org.osid.inquiry.Inquest inquest = inquests.getNextInquest();
                if (inquest.getId().equals(inquestId)) {
                    return (inquest);
                }
            }
        } 

        throw new org.osid.NotFoundException(inquestId + " not found");
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquests specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inquests</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inquestIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByIds(org.osid.id.IdList inquestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inquiry.Inquest> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = inquestIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getInquest(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("inquest " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inquiry.inquest.LinkedInquestList(ret));
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  inquest genus <code>Type</code> which does not include
     *  inquests of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.inquest.InquestGenusFilterList(getInquests(), inquestGenusType));
    }


    /**
     *  Gets an <code>InquestList</code> corresponding to the given
     *  inquest genus <code>Type</code> and include any additional
     *  inquests with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestGenusType an inquest genus type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByParentGenusType(org.osid.type.Type inquestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInquestsByGenusType(inquestGenusType));
    }


    /**
     *  Gets an <code>InquestList</code> containing the given
     *  inquest record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getInquests()</code>.
     *
     *  @param  inquestRecordType an inquest record type 
     *  @return the returned <code>Inquest</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByRecordType(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inquiry.inquest.InquestRecordFilterList(getInquests(), inquestRecordType));
    }


    /**
     *  Gets an <code>InquestList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known inquests or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  inquests that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Inquest</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquestsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.inquiry.inquest.InquestProviderFilterList(getInquests(), resourceId));
    }


    /**
     *  Gets all <code>Inquests</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquests or an error results. Otherwise, the returned list
     *  may contain only those inquests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Inquests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inquiry.InquestList getInquests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the inquest list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of inquests
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inquiry.InquestList filterInquestsOnViews(org.osid.inquiry.InquestList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
