//
// AbstractIndexedMapJobConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a JobConstrainerEnabler lookup service
//    backed by a fixed collection of job constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a JobConstrainerEnabler lookup service backed by a
 *  fixed collection of job constrainer enablers. The job constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some job constrainer enablers may be compatible
 *  with more types than are indicated through these job constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJobConstrainerEnablerLookupSession
    extends AbstractMapJobConstrainerEnablerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobConstrainerEnabler>());


    /**
     *  Makes a <code>JobConstrainerEnabler</code> available in this session.
     *
     *  @param  jobConstrainerEnabler a job constrainer enabler
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        super.putJobConstrainerEnabler(jobConstrainerEnabler);

        this.jobConstrainerEnablersByGenus.put(jobConstrainerEnabler.getGenusType(), jobConstrainerEnabler);
        
        try (org.osid.type.TypeList types = jobConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobConstrainerEnablersByRecord.put(types.getNextType(), jobConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a job constrainer enabler from this session.
     *
     *  @param jobConstrainerEnablerId the <code>Id</code> of the job constrainer enabler
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId) {
        org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler;
        try {
            jobConstrainerEnabler = getJobConstrainerEnabler(jobConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.jobConstrainerEnablersByGenus.remove(jobConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = jobConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobConstrainerEnablersByRecord.remove(types.getNextType(), jobConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJobConstrainerEnabler(jobConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> corresponding to the given
     *  job constrainer enabler genus <code>Type</code> which does not include
     *  job constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known job constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those job constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  jobConstrainerEnablerGenusType a job constrainer enabler genus type 
     *  @return the returned <code>JobConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByGenusType(org.osid.type.Type jobConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.ArrayJobConstrainerEnablerList(this.jobConstrainerEnablersByGenus.get(jobConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>JobConstrainerEnablerList</code> containing the given
     *  job constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known job constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  job constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  jobConstrainerEnablerRecordType a job constrainer enabler record type 
     *  @return the returned <code>jobConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablersByRecordType(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.ArrayJobConstrainerEnablerList(this.jobConstrainerEnablersByRecord.get(jobConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobConstrainerEnablersByGenus.clear();
        this.jobConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
