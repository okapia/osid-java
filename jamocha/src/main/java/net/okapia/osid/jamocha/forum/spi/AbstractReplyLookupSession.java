//
// AbstractReplyLookupSession.java
//
//    A starter implementation framework for providing a Reply
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Reply
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getReplies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractReplyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.forum.ReplyLookupSession {

    private boolean pedantic    = false;
    private boolean sequestered = false;
    private boolean federated   = false;
    private org.osid.forum.Forum forum = new net.okapia.osid.jamocha.nil.forum.forum.UnknownForum();
    

    /**
     *  Gets the <code>Forum/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Forum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.forum.getId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.forum);
    }


    /**
     *  Sets the <code>Forum</code>.
     *
     *  @param  forum the forum for this session
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected void setForum(org.osid.forum.Forum forum) {
        nullarg(forum, "forum");
        this.forum = forum;
        return;
    }


    /**
     *  Tests if this user can perform <code>Reply</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupReplies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Reply</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReplyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Reply</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReplyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include replies in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  replies.
     */

    @OSID @Override
    public void useSequesteredReplyView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All replies are returned including sequestered replies.
     */

    @OSID @Override
    public void useUnsequesteredReplyView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>Reply</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Reply</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Reply</code> and
     *  retained for compatibility.
     *
     *  @param  replyId <code>Id</code> of the
     *          <code>Reply</code>
     *  @return the reply
     *  @throws org.osid.NotFoundException <code>replyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>replyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Reply getReply(org.osid.id.Id replyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.forum.ReplyList replies = getReplies()) {
            while (replies.hasNext()) {
                org.osid.forum.Reply reply = replies.getNextReply();
                if (reply.getId().equals(replyId)) {
                    return (reply);
                }
            }
        } 

        throw new org.osid.NotFoundException(replyId + " not found");
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  replies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Replies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getReplies()</code>.
     *
     *  @param  replyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>replyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByIds(org.osid.id.IdList replyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.forum.Reply> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = replyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getReply(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("reply " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.forum.reply.LinkedReplyList(ret));
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  reply genus <code>Type</code> which does not include
     *  replies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getReplies()</code>.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyGenusFilterList(getReplies(), replyGenusType));
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  reply genus <code>Type</code> and include any additional
     *  replies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getReplies()</code>.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByParentGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRepliesByGenusType(replyGenusType));
    }


    /**
     *  Gets a <code>ReplyList</code> containing the given
     *  reply record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  replies or an error results. Otherwise, the returned list
     *  may contain only those replies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getReplies()</code>.
     *
     *  @param  replyRecordType a reply record type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByRecordType(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyRecordFilterList(getReplies(), replyRecordType));
    }


    /**
     *  Gets a list of all replies corresponding to the given date
     *  range inclusive.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In 
     *  unsequestered mode, all replies are returned. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDate(org.osid.calendaring.DateTime from, 
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new TimestampFilter(from, to), getReplies()));
    }        


    /**
     *  Gets a list of all replies corresponding to a post <code>
     *  Id. </code>
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.NullArgumentException <code>postId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new PostFilter(postId), getReplies()));
    }


    /**
     *  Gets a list of all replies corresponding to post
     *  <code>Id</code> in the given daterange inclusive.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replis that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>postId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPost(org.osid.id.Id postId, 
                                                            org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new TimestampFilter(from, to), getRepliesForPost(postId)));
    }        


    /**
     *  Gets a list of all replies corresponding to a poster.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  resourceId the resource <code>Id</code> 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new PosterFilter(resourceId), getReplies()));
    }        


    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> for the given poster within the date range
     *  inclusive.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  resourceId the resource <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPoster(org.osid.id.Id resourceId, 
                                                              org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new TimestampFilter(from, to), getRepliesForPoster(resourceId)));
    }        

    
    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> and poster.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post 
     *  @param  resourceId the resource <code>Id</code> 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.NullArgumentException <code>postId</code> or
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesForPostAndPoster(org.osid.id.Id postId, 
                                                               org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new PosterFilter(resourceId), getRepliesForPost(postId)));
    }        


    /**
     *  Gets a list of all replies corresponding to a post
     *  <code>Id</code> and poster within the given daterange
     *  incluisve.
     *  
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this session.
     *  
     *  In sequestered mode, no sequestered replies are returned. In
     *  unsequestered mode, all replies are returned.
     *
     *  @param  postId the <code>Id</code> of the post 
     *  @param  resourceId the resource <code>Id</code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ReplyList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>postId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByDateForPostAndPoster(org.osid.id.Id postId, 
                                                                     org.osid.id.Id resourceId, 
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilterList(new TimestampFilter(from, to), getRepliesForPostAndPoster(postId, resourceId)));
    }


    /**
     *  Gets all <code>Replies</code>. 
     *
     *  In plenary mode, the returned list contains all known replies
     *  or an error results. Otherwise, the returned list may contain
     *  only those replies that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Replies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.forum.ReplyList getReplies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the reply list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of replies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.forum.ReplyList filterRepliesOnViews(org.osid.forum.ReplyList list)
        throws org.osid.OperationFailedException {

        org.osid.forum.ReplyList ret = list;

        if (isSequestered()) {
            ret = new net.okapia.osid.jamocha.inline.filter.forum.reply.SequesteredReplyFilterList(ret);
        }

        return (ret);
    }


    public static class TimestampFilter
        implements net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;

        
        /**
         *  Constructs a new <code>TimestampFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public TimestampFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the ReplyFilterList to filter the reply list
         *  based on date.
         *
         *  @param reply the reply
         *  @return <code>true</code> to pass the reply,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.forum.Reply reply) {
            if (reply.getTimestamp().isLess(this.from)) {
                return (false);
            }

            if (reply.getTimestamp().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }    


    public static class PostFilter
        implements net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilter {

        private final org.osid.id.Id postId;

        
        /**
         *  Constructs a new <code>PostFilter</code>.
         *
         *  @param postId the post to filter
         *  @throws org.osid.NullArgumentException
         *          <code>postId</code> is <code>null</code>
         */

        public PostFilter(org.osid.id.Id postId) {
            nullarg(postId, "post Id");
            this.postId = postId;
            return;
        }


        /**
         *  Used by the ReplyFilterList to filter the reply list
         *  based on post.
         *
         *  @param reply the reply
         *  @return <code>true</code> to pass the reply,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.forum.Reply reply) {
            return (reply.getPostId().equals(this.postId));
        }
    }     


    public static class PosterFilter
        implements net.okapia.osid.jamocha.inline.filter.forum.reply.ReplyFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>PosterFilter</code>.
         *
         *  @param resourceId the poster to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public PosterFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "poster Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the ReplyFilterList to filter the reply list
         *  based on poster.
         *
         *  @param reply the reply
         *  @return <code>true</code> to pass the reply,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.forum.Reply reply) {
            return (reply.getPosterId().equals(this.resourceId));
        }
    }     
}
