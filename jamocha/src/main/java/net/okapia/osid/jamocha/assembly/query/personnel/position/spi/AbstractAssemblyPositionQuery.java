//
// AbstractAssemblyPositionQuery.java
//
//     A PositionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PositionQuery that stores terms.
 */

public abstract class AbstractAssemblyPositionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.personnel.PositionQuery,
               org.osid.personnel.PositionQueryInspector,
               org.osid.personnel.PositionSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.PositionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.PositionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.PositionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPositionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPositionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets an organization <code> Id. </code> 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getOrganizationIdColumn(), organizationId, match);
        return;
    }


    /**
     *  Clears all organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        getAssembler().clearTerms(getOrganizationIdColumn());
        return;
    }


    /**
     *  Gets the organization <code> Id </code> query terms. 
     *
     *  @return the organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (getAssembler().getIdTerms(getOrganizationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  organization. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOrganization(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOrganizationColumn(), style);
        return;
    }


    /**
     *  Gets the OrganizationId column name.
     *
     * @return the column name
     */

    protected String getOrganizationIdColumn() {
        return ("organization_id");
    }


    /**
     *  Tests if an <code> OrganizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Clears all organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        getAssembler().clearTerms(getOrganizationColumn());
        return;
    }


    /**
     *  Gets the organization query terms. 
     *
     *  @return the organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Tests if a organization search order is available. 
     *
     *  @return <code> true </code> if a organization search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the organization search order. 
     *
     *  @return the organization search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchOrder getOrganizationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOrganizationSearchOrder() is false");
    }


    /**
     *  Gets the Organization column name.
     *
     * @return the column name
     */

    protected String getOrganizationColumn() {
        return ("organization");
    }


    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches persons with any title. 
     *
     *  @param  match <code> true </code> to match positions with any title, 
     *          <code> false </code> to match positions with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the title. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getLevelIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears all grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        getAssembler().clearTerms(getLevelIdColumn());
        return;
    }


    /**
     *  Gets the grade level <code> Id </code> query terms. 
     *
     *  @return the grade level <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (getAssembler().getIdTerms(getLevelIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLevelColumn(), style);
        return;
    }


    /**
     *  Gets the LevelId column name.
     *
     * @return the column name
     */

    protected String getLevelIdColumn() {
        return ("level_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches positions with any level. 
     *
     *  @param  match <code> true </code> to match positions with any level, 
     *          <code> false </code> to match positions with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        getAssembler().addIdWildcardTerm(getLevelColumn(), match);
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        getAssembler().clearTerms(getLevelColumn());
        return;
    }


    /**
     *  Gets the grade level query terms. 
     *
     *  @return the grade level terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a grade level search order is available. 
     *
     *  @return <code> true </code> if a level search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets the level search order. 
     *
     *  @return the level search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Gets the Level column name.
     *
     * @return the column name
     */

    protected String getLevelColumn() {
        return ("level");
    }


    /**
     *  Sets an objective <code> Id. </code> 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualificationId(org.osid.id.Id objectiveId, boolean match) {
        getAssembler().addIdTerm(getQualificationIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears all objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearQualificationIdTerms() {
        getAssembler().clearTerms(getQualificationIdColumn());
        return;
    }


    /**
     *  Gets the qualification <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualificationIdTerms() {
        return (getAssembler().getIdTerms(getQualificationIdColumn()));
    }


    /**
     *  Gets the QualificationId column name.
     *
     * @return the column name
     */

    protected String getQualificationIdColumn() {
        return ("qualification_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualificationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualificationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getQualificationQuery() {
        throw new org.osid.UnimplementedException("supportsQualificationQuery() is false");
    }


    /**
     *  Matches positions with any qualification. 
     *
     *  @param  match <code> true </code> to match positions with any 
     *          qualification, <code> false </code> to match positions with no 
     *          qualifications 
     */

    @OSID @Override
    public void matchAnyQualification(boolean match) {
        getAssembler().addIdWildcardTerm(getQualificationColumn(), match);
        return;
    }


    /**
     *  Clears all qualification terms. 
     */

    @OSID @Override
    public void clearQualificationTerms() {
        getAssembler().clearTerms(getQualificationColumn());
        return;
    }


    /**
     *  Gets the qualification query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getQualificationTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the Qualification column name.
     *
     * @return the column name
     */

    protected String getQualificationColumn() {
        return ("qualification");
    }


    /**
     *  Matches a target appointments between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchTargetAppointments(long from, long to, boolean match) {
        getAssembler().addCardinalRangeTerm(getTargetAppointmentsColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any target appointments. 
     *
     *  @param  match <code> true </code> to match positions with any target 
     *          appointments, <code> false </code> to match positions with no 
     *          target appointments 
     */

    @OSID @Override
    public void matchAnyTargetAppointments(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getTargetAppointmentsColumn(), match);
        return;
    }


    /**
     *  Clears all target appointments terms. 
     */

    @OSID @Override
    public void clearTargetAppointmentsTerms() {
        getAssembler().clearTerms(getTargetAppointmentsColumn());
        return;
    }


    /**
     *  Gets the target appointments query terms. 
     *
     *  @return the target appointments terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getTargetAppointmentsTerms() {
        return (getAssembler().getCardinalRangeTerms(getTargetAppointmentsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the target 
     *  appointments. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTargetAppointments(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTargetAppointmentsColumn(), style);
        return;
    }


    /**
     *  Gets the TargetAppointments column name.
     *
     * @return the column name
     */

    protected String getTargetAppointmentsColumn() {
        return ("target_appointments");
    }


    /**
     *  Matches a required commitment between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchRequiredCommitment(long from, long to, boolean match) {
        getAssembler().addCardinalRangeTerm(getRequiredCommitmentColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any required commitment. 
     *
     *  @param  match <code> true </code> to match positions with any required 
     *          commitment, <code> false </code> to match positions with no 
     *          required commitment 
     */

    @OSID @Override
    public void matchAnyRequiredCommitment(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getRequiredCommitmentColumn(), match);
        return;
    }


    /**
     *  Clears all required commitment terms. 
     */

    @OSID @Override
    public void clearRequiredCommitmentTerms() {
        getAssembler().clearTerms(getRequiredCommitmentColumn());
        return;
    }


    /**
     *  Gets the required commitment query terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getRequiredCommitmentTerms() {
        return (getAssembler().getCardinalRangeTerms(getRequiredCommitmentColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the required 
     *  commitment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiredCommitment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiredCommitmentColumn(), style);
        return;
    }


    /**
     *  Gets the RequiredCommitment column name.
     *
     * @return the column name
     */

    protected String getRequiredCommitmentColumn() {
        return ("required_commitment");
    }


    /**
     *  Matches a low salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLowSalaryRange(org.osid.financials.Currency from, 
                                    org.osid.financials.Currency to, 
                                    boolean match) {
        getAssembler().addCurrencyRangeTerm(getLowSalaryRangeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any low salary. 
     *
     *  @param  match <code> true </code> to match positions with any low 
     *          salary range, commitment, <code> false </code> to match 
     *          positions with no low salary range 
     */

    @OSID @Override
    public void matchAnyLowSalaryRange(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getLowSalaryRangeColumn(), match);
        return;
    }


    /**
     *  Clears all low salary terms. 
     */

    @OSID @Override
    public void clearLowSalaryRangeTerms() {
        getAssembler().clearTerms(getLowSalaryRangeColumn());
        return;
    }


    /**
     *  Gets the low salary query terms. 
     *
     *  @return the low salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getLowSalaryRangeTerms() {
        return (getAssembler().getCurrencyRangeTerms(getLowSalaryRangeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the low salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLowSalaryRange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLowSalaryRangeColumn(), style);
        return;
    }


    /**
     *  Gets the LowSalaryRange column name.
     *
     * @return the column name
     */

    protected String getLowSalaryRangeColumn() {
        return ("low_salary_range");
    }


    /**
     *  Matches a midpoint salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchMidpointSalaryRange(org.osid.financials.Currency from, 
                                         org.osid.financials.Currency to, 
                                         boolean match) {
        getAssembler().addCurrencyRangeTerm(getMidpointSalaryRangeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any midpoint salary. 
     *
     *  @param  match <code> true </code> to match positions with any midpoint 
     *          salary range, commitment, <code> false </code> to match 
     *          positions with no midpoint salary range 
     */

    @OSID @Override
    public void matchAnyMidpointSalaryRange(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getMidpointSalaryRangeColumn(), match);
        return;
    }


    /**
     *  Clears all midpoint salary terms. 
     */

    @OSID @Override
    public void clearMidpointSalaryRangeTerms() {
        getAssembler().clearTerms(getMidpointSalaryRangeColumn());
        return;
    }


    /**
     *  Gets the midpoint salary query terms. 
     *
     *  @return the midpoint salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getMidpointSalaryRangeTerms() {
        return (getAssembler().getCurrencyRangeTerms(getMidpointSalaryRangeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the midpoint 
     *  salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMidpointSalaryRange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMidpointSalaryRangeColumn(), style);
        return;
    }


    /**
     *  Gets the MidpointSalaryRange column name.
     *
     * @return the column name
     */

    protected String getMidpointSalaryRangeColumn() {
        return ("midpoint_salary_range");
    }


    /**
     *  Matches a high salary between the given range inclusive. 
     *
     *  @param  from a starting salary 
     *  @param  to an ending salary 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchHighSalaryRange(org.osid.financials.Currency from, 
                                     org.osid.financials.Currency to, 
                                     boolean match) {
        getAssembler().addCurrencyRangeTerm(getHighSalaryRangeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches positions with any high salary. 
     *
     *  @param  match <code> true </code> to match positions with any high 
     *          salary range, <code> false </code> to match positions with no 
     *          high salary range 
     */

    @OSID @Override
    public void matchAnyHighSalaryRange(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getHighSalaryRangeColumn(), match);
        return;
    }


    /**
     *  Clears all high salary terms. 
     */

    @OSID @Override
    public void clearHighSalaryRangeTerms() {
        getAssembler().clearTerms(getHighSalaryRangeColumn());
        return;
    }


    /**
     *  Gets the high salary query terms. 
     *
     *  @return the high salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getHighSalaryRangeTerms() {
        return (getAssembler().getCurrencyRangeTerms(getHighSalaryRangeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the high salary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHighSalaryRange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getHighSalaryRangeColumn(), style);
        return;
    }


    /**
     *  Gets the HighSalaryRange column name.
     *
     * @return the column name
     */

    protected String getHighSalaryRangeColumn() {
        return ("high_salary_range");
    }


    /**
     *  Matches a compensation frequency between the given range inclusive. 
     *
     *  @param  low low range of time frequency 
     *  @param  high high range of time frequency 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> frequency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompensationFrequency(org.osid.calendaring.Duration low, 
                                           org.osid.calendaring.Duration high, 
                                           boolean match) {
        getAssembler().addDurationRangeTerm(getCompensationFrequencyColumn(), low, high, match);
        return;
    }


    /**
     *  Matches positions with any compensation frequency. 
     *
     *  @param  match <code> true </code> to match positions with any 
     *          compensation frequency, <code> false </code> to match 
     *          positions with no compensation frequency 
     */

    @OSID @Override
    public void matchAnyCompensationFrequency(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getCompensationFrequencyColumn(), match);
        return;
    }


    /**
     *  Clears all compensation frequency terms. 
     */

    @OSID @Override
    public void clearCompensationFrequencyTerms() {
        getAssembler().clearTerms(getCompensationFrequencyColumn());
        return;
    }


    /**
     *  Gets the compensation frequency terms. 
     *
     *  @return the frequency terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getCompensationFrequencyTerms() {
        return (getAssembler().getDurationRangeTerms(getCompensationFrequencyColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the compensation 
     *  frequency. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompensationFrequency(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCompensationFrequencyColumn(), style);
        return;
    }


    /**
     *  Gets the CompensationFrequency column name.
     *
     * @return the column name
     */

    protected String getCompensationFrequencyColumn() {
        return ("compensation_frequency");
    }


    /**
     *  Matches exempt positions. 
     *
     *  @param  match <code> true </code> to match exempt positions, <code> 
     *          false </code> to match non-exempt positions 
     */

    @OSID @Override
    public void matchExempt(boolean match) {
        getAssembler().addBooleanTerm(getExemptColumn(), match);
        return;
    }


    /**
     *  Matches positions with any exempt flag set. 
     *
     *  @param  match <code> true </code> to match positions with any exempt 
     *          status,, <code> false </code> to match positions with no 
     *          exempt status 
     */

    @OSID @Override
    public void matchAnyExempt(boolean match) {
        getAssembler().addBooleanWildcardTerm(getExemptColumn(), match);
        return;
    }


    /**
     *  Clears all exempt terms. 
     */

    @OSID @Override
    public void clearExemptTerms() {
        getAssembler().clearTerms(getExemptColumn());
        return;
    }


    /**
     *  Gets the exempt terms. 
     *
     *  @return the exempt terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExemptTerms() {
        return (getAssembler().getBooleanTerms(getExemptColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the exempt flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExempt(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getExemptColumn(), style);
        return;
    }


    /**
     *  Gets the Exempt column name.
     *
     * @return the column name
     */

    protected String getExemptColumn() {
        return ("exempt");
    }


    /**
     *  Matches a benefits type. 
     *
     *  @param  type a benefits type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> type </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBenefitsType(org.osid.type.Type type, boolean match) {
        getAssembler().addTypeTerm(getBenefitsTypeColumn(), type, match);
        return;
    }


    /**
     *  Matches positions with any benefits type. 
     *
     *  @param  match <code> true </code> to match positions with any benefits 
     *          type, <code> false </code> to match positions with no benefits 
     *          type 
     */

    @OSID @Override
    public void matchAnyBenefitsType(boolean match) {
        getAssembler().addTypeWildcardTerm(getBenefitsTypeColumn(), match);
        return;
    }


    /**
     *  Clears all benefits type terms. 
     */

    @OSID @Override
    public void clearBenefitsTypeTerms() {
        getAssembler().clearTerms(getBenefitsTypeColumn());
        return;
    }


    /**
     *  Gets the benefits type terms. 
     *
     *  @return the benefit type terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getBenefitsTypeTerms() {
        return (getAssembler().getTypeTerms(getBenefitsTypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the benefits 
     *  type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBenefitsType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBenefitsTypeColumn(), style);
        return;
    }


    /**
     *  Gets the BenefitsType column name.
     *
     * @return the column name
     */

    protected String getBenefitsTypeColumn() {
        return ("benefits_type");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match positions 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        getAssembler().clearTerms(getRealmIdColumn());
        return;
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (getAssembler().getIdTerms(getRealmIdColumn()));
    }


    /**
     *  Gets the RealmId column name.
     *
     * @return the column name
     */

    protected String getRealmIdColumn() {
        return ("realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        getAssembler().clearTerms(getRealmColumn());
        return;
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the Realm column name.
     *
     * @return the column name
     */

    protected String getRealmColumn() {
        return ("realm");
    }


    /**
     *  Tests if this position supports the given record
     *  <code>Type</code>.
     *
     *  @param  positionRecordType a position record type 
     *  @return <code>true</code> if the positionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type positionRecordType) {
        for (org.osid.personnel.records.PositionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(positionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  positionRecordType the position record type 
     *  @return the position query record 
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionQueryRecord getPositionQueryRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  positionRecordType the position record type 
     *  @return the position query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionQueryInspectorRecord getPositionQueryInspectorRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param positionRecordType the position record type
     *  @return the position search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>positionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionSearchOrderRecord getPositionSearchOrderRecord(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PositionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(positionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this position. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param positionQueryRecord the position query record
     *  @param positionQueryInspectorRecord the position query inspector
     *         record
     *  @param positionSearchOrderRecord the position search order record
     *  @param positionRecordType position record type
     *  @throws org.osid.NullArgumentException
     *          <code>positionQueryRecord</code>,
     *          <code>positionQueryInspectorRecord</code>,
     *          <code>positionSearchOrderRecord</code> or
     *          <code>positionRecordTypeposition</code> is
     *          <code>null</code>
     */
            
    protected void addPositionRecords(org.osid.personnel.records.PositionQueryRecord positionQueryRecord, 
                                      org.osid.personnel.records.PositionQueryInspectorRecord positionQueryInspectorRecord, 
                                      org.osid.personnel.records.PositionSearchOrderRecord positionSearchOrderRecord, 
                                      org.osid.type.Type positionRecordType) {

        addRecordType(positionRecordType);

        nullarg(positionQueryRecord, "position query record");
        nullarg(positionQueryInspectorRecord, "position query inspector record");
        nullarg(positionSearchOrderRecord, "position search odrer record");

        this.queryRecords.add(positionQueryRecord);
        this.queryInspectorRecords.add(positionQueryInspectorRecord);
        this.searchOrderRecords.add(positionSearchOrderRecord);
        
        return;
    }
}
