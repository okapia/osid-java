//
// AbstractOfferingQueryInspector.java
//
//     A template for making an OfferingQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for offerings.
 */

public abstract class AbstractOfferingQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.offering.OfferingQueryInspector {

    private final java.util.Collection<org.osid.offering.records.OfferingQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the canonical unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCanonicalUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the canonical unit query terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the time period <code> Id </code> query terms. 
     *
     *  @return the time period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period query terms. 
     *
     *  @return the time period query terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getResultOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given offering query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an offering implementing the requested record.
     *
     *  @param offeringRecordType an offering record type
     *  @return the offering query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.OfferingQueryInspectorRecord getOfferingQueryInspectorRecord(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.OfferingQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(offeringRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering query. 
     *
     *  @param offeringQueryInspectorRecord offering query inspector
     *         record
     *  @param offeringRecordType offering record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingQueryInspectorRecord(org.osid.offering.records.OfferingQueryInspectorRecord offeringQueryInspectorRecord, 
                                                   org.osid.type.Type offeringRecordType) {

        addRecordType(offeringRecordType);
        nullarg(offeringRecordType, "offering record type");
        this.records.add(offeringQueryInspectorRecord);        
        return;
    }
}
