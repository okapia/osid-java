//
// AbstractAdapterProfileItemLookupSession.java
//
//    A ProfileItem lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ProfileItem lookup session adapter.
 */

public abstract class AbstractAdapterProfileItemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.profile.ProfileItemLookupSession {

    private final org.osid.profile.ProfileItemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProfileItemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProfileItemLookupSession(org.osid.profile.ProfileItemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Profile/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Profile Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.session.getProfileId());
    }


    /**
     *  Gets the {@code Profile} associated with this session.
     *
     *  @return the {@code Profile} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProfile());
    }


    /**
     *  Tests if this user can perform {@code ProfileItem} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProfileItems() {
        return (this.session.canLookupProfileItems());
    }


    /**
     *  A complete view of the {@code ProfileItem} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileItemView() {
        this.session.useComparativeProfileItemView();
        return;
    }


    /**
     *  A complete view of the {@code ProfileItem} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileItemView() {
        this.session.usePlenaryProfileItemView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile items in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.session.useFederatedProfileView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.session.useIsolatedProfileView();
        return;
    }
    
     
    /**
     *  Gets the {@code ProfileItem} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ProfileItem} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ProfileItem} and
     *  retained for compatibility.
     *
     *  @param profileItemId {@code Id} of the {@code ProfileItem}
     *  @return the profile item
     *  @throws org.osid.NotFoundException {@code profileItemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code profileItemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItem(profileItemId));
    }


    /**
     *  Gets a {@code ProfileItemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileItems specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ProfileItems} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  profileItemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ProfileItem} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code profileItemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByIds(org.osid.id.IdList profileItemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItemsByIds(profileItemIds));
    }


    /**
     *  Gets a {@code ProfileItemList} corresponding to the given
     *  profile item genus {@code Type} which does not include
     *  profile items of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned {@code ProfileItem} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileItemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItemsByGenusType(profileItemGenusType));
    }


    /**
     *  Gets a {@code ProfileItemList} corresponding to the given
     *  profile item genus {@code Type} and include any additional
     *  profile items with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned {@code ProfileItem} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileItemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByParentGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItemsByParentGenusType(profileItemGenusType));
    }


    /**
     *  Gets a {@code ProfileItemList} containing the given
     *  profile item record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemRecordType a profileItem record type 
     *  @return the returned {@code ProfileItem} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileItemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByRecordType(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItemsByRecordType(profileItemRecordType));
    }


    /**
     *  Gets all {@code ProfileItems}. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code ProfileItems} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileItems());
    }
}
