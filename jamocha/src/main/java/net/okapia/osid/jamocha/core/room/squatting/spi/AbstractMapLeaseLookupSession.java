//
// AbstractMapLeaseLookupSession
//
//    A simple framework for providing a Lease lookup service
//    backed by a fixed collection of leases.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Lease lookup service backed by a
 *  fixed collection of leases. The leases are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Leases</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapLeaseLookupSession
    extends net.okapia.osid.jamocha.room.squatting.spi.AbstractLeaseLookupSession
    implements org.osid.room.squatting.LeaseLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.squatting.Lease> leases = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.squatting.Lease>());


    /**
     *  Makes a <code>Lease</code> available in this session.
     *
     *  @param  lease a lease
     *  @throws org.osid.NullArgumentException <code>lease<code>
     *          is <code>null</code>
     */

    protected void putLease(org.osid.room.squatting.Lease lease) {
        this.leases.put(lease.getId(), lease);
        return;
    }


    /**
     *  Makes an array of leases available in this session.
     *
     *  @param  leases an array of leases
     *  @throws org.osid.NullArgumentException <code>leases<code>
     *          is <code>null</code>
     */

    protected void putLeases(org.osid.room.squatting.Lease[] leases) {
        putLeases(java.util.Arrays.asList(leases));
        return;
    }


    /**
     *  Makes a collection of leases available in this session.
     *
     *  @param  leases a collection of leases
     *  @throws org.osid.NullArgumentException <code>leases<code>
     *          is <code>null</code>
     */

    protected void putLeases(java.util.Collection<? extends org.osid.room.squatting.Lease> leases) {
        for (org.osid.room.squatting.Lease lease : leases) {
            this.leases.put(lease.getId(), lease);
        }

        return;
    }


    /**
     *  Removes a Lease from this session.
     *
     *  @param  leaseId the <code>Id</code> of the lease
     *  @throws org.osid.NullArgumentException <code>leaseId<code> is
     *          <code>null</code>
     */

    protected void removeLease(org.osid.id.Id leaseId) {
        this.leases.remove(leaseId);
        return;
    }


    /**
     *  Gets the <code>Lease</code> specified by its <code>Id</code>.
     *
     *  @param  leaseId <code>Id</code> of the <code>Lease</code>
     *  @return the lease
     *  @throws org.osid.NotFoundException <code>leaseId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>leaseId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Lease getLease(org.osid.id.Id leaseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.squatting.Lease lease = this.leases.get(leaseId);
        if (lease == null) {
            throw new org.osid.NotFoundException("lease not found: " + leaseId);
        }

        return (lease);
    }


    /**
     *  Gets all <code>Leases</code>. In plenary mode, the returned
     *  list contains all known leases or an error
     *  results. Otherwise, the returned list may contain only those
     *  leases that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Leases</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeases()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.lease.ArrayLeaseList(this.leases.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.leases.clear();
        super.close();
        return;
    }
}
