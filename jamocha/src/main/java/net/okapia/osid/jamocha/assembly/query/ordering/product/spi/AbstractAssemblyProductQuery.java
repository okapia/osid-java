//
// AbstractAssemblyProductQuery.java
//
//     A ProductQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProductQuery that stores terms.
 */

public abstract class AbstractAssemblyProductQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.ordering.ProductQuery,
               org.osid.ordering.ProductQueryInspector,
               org.osid.ordering.ProductSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.ProductQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.ProductQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.ProductSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProductQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProductQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches product codes. 
     *
     *  @param  code a product code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches products with any code. 
     *
     *  @param  match <code> true </code> to match products with any code, 
     *          <code> false </code> to match products with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCodeColumn(), match);
        return;
    }


    /**
     *  Clears the code. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        getAssembler().clearTerms(getCodeColumn());
        return;
    }


    /**
     *  Gets the code terms. 
     *
     *  @return the code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (getAssembler().getStringTerms(getCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the code. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCodeColumn(), style);
        return;
    }


    /**
     *  Gets the Code column name.
     *
     * @return the column name
     */

    protected String getCodeColumn() {
        return ("code");
    }


    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        getAssembler().addIdTerm(getPriceScheduleIdColumn(), priceScheduleId, match);
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        getAssembler().clearTerms(getPriceScheduleIdColumn());
        return;
    }


    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (getAssembler().getIdTerms(getPriceScheduleIdColumn()));
    }


    /**
     *  Gets the PriceScheduleId column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleIdColumn() {
        return ("price_schedule_id");
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Matches products with any price schedule. 
     *
     *  @param  match <code> true </code> to match products with any price 
     *          schedule, <code> false </code> to match products with no price 
     *          schedule 
     */

    @OSID @Override
    public void matchAnyPriceSchedule(boolean match) {
        getAssembler().addIdWildcardTerm(getPriceScheduleColumn(), match);
        return;
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        getAssembler().clearTerms(getPriceScheduleColumn());
        return;
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Gets the PriceSchedule column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleColumn() {
        return ("price_schedule");
    }


    /**
     *  Matches product availability between the given range inclusive. 
     *
     *  @param  low a product code 
     *  @param  high a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchAvailability(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getAvailabilityColumn(), low, high, match);
        return;
    }


    /**
     *  Matches products with any availability set. 
     *
     *  @param  match <code> true </code> to match products with 
     *          anyavailability value, <code> false </code> to match products 
     *          with no availability value 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getAvailabilityColumn(), match);
        return;
    }


    /**
     *  Clears the availability terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        getAssembler().clearTerms(getAvailabilityColumn());
        return;
    }


    /**
     *  Gets the availability terms. 
     *
     *  @return the availability terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getAvailabilityTerms() {
        return (getAssembler().getCardinalRangeTerms(getAvailabilityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  availability. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAvailability(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAvailabilityColumn(), style);
        return;
    }


    /**
     *  Gets the Availability column name.
     *
     * @return the column name
     */

    protected String getAvailabilityColumn() {
        return ("availability");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an item query is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches prices used with any item. 
     *
     *  @param  match <code> true </code> to match prices with any order, 
     *          <code> false </code> to match prices with no orders 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the product <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this product supports the given record
     *  <code>Type</code>.
     *
     *  @param  productRecordType a product record type 
     *  @return <code>true</code> if the productRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type productRecordType) {
        for (org.osid.ordering.records.ProductQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(productRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  productRecordType the product record type 
     *  @return the product query record 
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductQueryRecord getProductQueryRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  productRecordType the product record type 
     *  @return the product query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductQueryInspectorRecord getProductQueryInspectorRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param productRecordType the product record type
     *  @return the product search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductSearchOrderRecord getProductSearchOrderRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this product. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param productQueryRecord the product query record
     *  @param productQueryInspectorRecord the product query inspector
     *         record
     *  @param productSearchOrderRecord the product search order record
     *  @param productRecordType product record type
     *  @throws org.osid.NullArgumentException
     *          <code>productQueryRecord</code>,
     *          <code>productQueryInspectorRecord</code>,
     *          <code>productSearchOrderRecord</code> or
     *          <code>productRecordTypeproduct</code> is
     *          <code>null</code>
     */
            
    protected void addProductRecords(org.osid.ordering.records.ProductQueryRecord productQueryRecord, 
                                      org.osid.ordering.records.ProductQueryInspectorRecord productQueryInspectorRecord, 
                                      org.osid.ordering.records.ProductSearchOrderRecord productSearchOrderRecord, 
                                      org.osid.type.Type productRecordType) {

        addRecordType(productRecordType);

        nullarg(productQueryRecord, "product query record");
        nullarg(productQueryInspectorRecord, "product query inspector record");
        nullarg(productSearchOrderRecord, "product search odrer record");

        this.queryRecords.add(productQueryRecord);
        this.queryInspectorRecords.add(productQueryInspectorRecord);
        this.searchOrderRecords.add(productSearchOrderRecord);
        
        return;
    }
}
