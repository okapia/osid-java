//
// MutableIndexedMapProxyRouteLookupSession
//
//    Implements a Route lookup service backed by a collection of
//    routes indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.route;


/**
 *  Implements a Route lookup service backed by a collection of
 *  routes. The routes are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some routes may be compatible
 *  with more types than are indicated through these route
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of routes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyRouteLookupSession
    extends net.okapia.osid.jamocha.core.mapping.route.spi.AbstractIndexedMapRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRouteLookupSession} with
     *  no route.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRouteLookupSession(org.osid.mapping.Map map,
                                                       org.osid.proxy.Proxy proxy) {
        setMap(map);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRouteLookupSession} with
     *  a single route.
     *
     *  @param map the map
     *  @param  route an route
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code route}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRouteLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.route.Route route, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putRoute(route);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRouteLookupSession} using
     *  an array of routes.
     *
     *  @param map the map
     *  @param  routes an array of routes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code routes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRouteLookupSession(org.osid.mapping.Map map,
                                                       org.osid.mapping.route.Route[] routes, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putRoutes(routes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRouteLookupSession} using
     *  a collection of routes.
     *
     *  @param map the map
     *  @param  routes a collection of routes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code routes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRouteLookupSession(org.osid.mapping.Map map,
                                                       java.util.Collection<? extends org.osid.mapping.route.Route> routes,
                                                       org.osid.proxy.Proxy proxy) {
        this(map, proxy);
        putRoutes(routes);
        return;
    }

    
    /**
     *  Makes a {@code Route} available in this session.
     *
     *  @param  route a route
     *  @throws org.osid.NullArgumentException {@code route{@code 
     *          is {@code null}
     */

    @Override
    public void putRoute(org.osid.mapping.route.Route route) {
        super.putRoute(route);
        return;
    }


    /**
     *  Makes an array of routes available in this session.
     *
     *  @param  routes an array of routes
     *  @throws org.osid.NullArgumentException {@code routes{@code 
     *          is {@code null}
     */

    @Override
    public void putRoutes(org.osid.mapping.route.Route[] routes) {
        super.putRoutes(routes);
        return;
    }


    /**
     *  Makes collection of routes available in this session.
     *
     *  @param  routes a collection of routes
     *  @throws org.osid.NullArgumentException {@code route{@code 
     *          is {@code null}
     */

    @Override
    public void putRoutes(java.util.Collection<? extends org.osid.mapping.route.Route> routes) {
        super.putRoutes(routes);
        return;
    }


    /**
     *  Removes a Route from this session.
     *
     *  @param routeId the {@code Id} of the route
     *  @throws org.osid.NullArgumentException {@code routeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRoute(org.osid.id.Id routeId) {
        super.removeRoute(routeId);
        return;
    }    
}
