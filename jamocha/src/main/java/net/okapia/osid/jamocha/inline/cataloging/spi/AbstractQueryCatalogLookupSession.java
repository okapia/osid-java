//
// AbstractQueryCatalogLookupSession.java
//
//    An inline adapter that maps a CatalogLookupSession to
//    a CatalogQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CatalogLookupSession to
 *  a CatalogQuerySession.
 */

public abstract class AbstractQueryCatalogLookupSession
    extends net.okapia.osid.jamocha.cataloging.spi.AbstractCatalogLookupSession
    implements org.osid.cataloging.CatalogLookupSession {

    private final org.osid.cataloging.CatalogQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCatalogLookupSession.
     *
     *  @param querySession the underlying catalog query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCatalogLookupSession(org.osid.cataloging.CatalogQuerySession querySession) {
        nullarg(querySession, "catalog query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Catalog</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCatalogs() {
        return (this.session.canSearchCatalogs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Catalog</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Catalog</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Catalog</code> and
     *  retained for compatibility.
     *
     *  @param  catalogId <code>Id</code> of the
     *          <code>Catalog</code>
     *  @return the catalog
     *  @throws org.osid.NotFoundException <code>catalogId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>catalogId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchId(catalogId, true);
        org.osid.cataloging.CatalogList catalogs = this.session.getCatalogsByQuery(query);
        if (catalogs.hasNext()) {
            return (catalogs.getNextCatalog());
        } 
        
        throw new org.osid.NotFoundException(catalogId + " not found");
    }


    /**
     *  Gets a <code>CatalogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Catalogs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  catalogIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Catalog</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>catalogIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByIds(org.osid.id.IdList catalogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();

        try (org.osid.id.IdList ids = catalogIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CatalogList</code> corresponding to the given
     *  catalog genus <code>Type</code> which does not include
     *  catalogs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogGenusType a catalog genus type 
     *  @return the returned <code>Catalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByGenusType(org.osid.type.Type catalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchGenusType(catalogGenusType, true);
        return (this.session.getCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CatalogList</code> corresponding to the given
     *  catalog genus <code>Type</code> and include any additional
     *  catalogs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogGenusType a catalog genus type 
     *  @return the returned <code>Catalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByParentGenusType(org.osid.type.Type catalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchParentGenusType(catalogGenusType, true);
        return (this.session.getCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CatalogList</code> containing the given
     *  catalog record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogRecordType a catalog record type 
     *  @return the returned <code>Catalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByRecordType(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchRecordType(catalogRecordType, true);
        return (this.session.getCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CatalogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known catalogs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  catalogs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Catalog</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCatalogsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Catalogs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Catalogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.cataloging.CatalogQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCatalogsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.cataloging.CatalogQuery getQuery() {
        org.osid.cataloging.CatalogQuery query = this.session.getCatalogQuery();
        
        return (query);
    }
}
