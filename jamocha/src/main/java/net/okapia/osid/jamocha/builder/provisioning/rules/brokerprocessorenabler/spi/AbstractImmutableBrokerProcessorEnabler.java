//
// AbstractImmutableBrokerProcessorEnabler.java
//
//     Wraps a mutable BrokerProcessorEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>BrokerProcessorEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized BrokerProcessorEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying brokerProcessorEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableBrokerProcessorEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.provisioning.rules.BrokerProcessorEnabler {

    private final org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableBrokerProcessorEnabler</code>.
     *
     *  @param brokerProcessorEnabler the broker processor enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBrokerProcessorEnabler(org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler) {
        super(brokerProcessorEnabler);
        this.brokerProcessorEnabler = brokerProcessorEnabler;
        return;
    }


    /**
     *  Gets the broker processor enabler record corresponding to the given 
     *  <code> BrokerProcessorEnabler </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> brokerProcessorEnablerRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(brokerProcessorEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  brokerProcessorEnablerRecordType the type of broker processor 
     *          enabler record to retrieve 
     *  @return the broker processor enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(brokerProcessorEnablerRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerRecord getBrokerProcessorEnablerRecord(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.brokerProcessorEnabler.getBrokerProcessorEnablerRecord(brokerProcessorEnablerRecordType));
    }
}

