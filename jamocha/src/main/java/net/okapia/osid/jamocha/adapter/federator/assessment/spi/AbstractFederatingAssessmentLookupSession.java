//
// AbstractFederatingAssessmentLookupSession.java
//
//     An abstract federating adapter for an AssessmentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AssessmentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAssessmentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.AssessmentLookupSession>
    implements org.osid.assessment.AssessmentLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingAssessmentLookupSession</code>.
     */

    protected AbstractFederatingAssessmentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.AssessmentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>Assessment</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessments() {
        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            if (session.canLookupAssessments()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Assessment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentView() {
        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            session.useComparativeAssessmentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Assessment</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentView() {
        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            session.usePlenaryAssessmentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Assessment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Assessment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Assessment</code> and
     *  retained for compatibility.
     *
     *  @param  assessmentId <code>Id</code> of the
     *          <code>Assessment</code>
     *  @return the assessment
     *  @throws org.osid.NotFoundException <code>assessmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment(org.osid.id.Id assessmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            try {
                return (session.getAssessment(assessmentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assessmentId + " not found");
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Assessments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  assessmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByIds(org.osid.id.IdList assessmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.assessment.MutableAssessmentList ret = new net.okapia.osid.jamocha.assessment.assessment.MutableAssessmentList();

        try (org.osid.id.IdList ids = assessmentIds) {
            while (ids.hasNext()) {
                ret.addAssessment(getAssessment(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  assessment genus <code>Type</code> which does not include
     *  assessments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessment.FederatingAssessmentList ret = getAssessmentList();

        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            ret.addAssessmentList(session.getAssessmentsByGenusType(assessmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentList</code> corresponding to the given
     *  assessment genus <code>Type</code> and include any additional
     *  assessments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentGenusType an assessment genus type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByParentGenusType(org.osid.type.Type assessmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessment.FederatingAssessmentList ret = getAssessmentList();

        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            ret.addAssessmentList(session.getAssessmentsByParentGenusType(assessmentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentList</code> containing the given
     *  assessment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return the returned <code>Assessment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessmentsByRecordType(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessment.FederatingAssessmentList ret = getAssessmentList();

        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            ret.addAssessmentList(session.getAssessmentsByRecordType(assessmentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Assessments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments or an error results. Otherwise, the returned list
     *  may contain only those assessments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Assessments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.assessment.FederatingAssessmentList ret = getAssessmentList();

        for (org.osid.assessment.AssessmentLookupSession session : getSessions()) {
            ret.addAssessmentList(session.getAssessments());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.assessment.FederatingAssessmentList getAssessmentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessment.ParallelAssessmentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.assessment.CompositeAssessmentList());
        }
    }
}
