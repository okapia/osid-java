//
// AbstractMapProvisionableLookupSession
//
//    A simple framework for providing a Provisionable lookup service
//    backed by a fixed collection of provisionables.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Provisionable lookup service backed by a
 *  fixed collection of provisionables. The provisionables are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Provisionables</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProvisionableLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractProvisionableLookupSession
    implements org.osid.provisioning.ProvisionableLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.Provisionable> provisionables = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.Provisionable>());


    /**
     *  Makes a <code>Provisionable</code> available in this session.
     *
     *  @param  provisionable a provisionable
     *  @throws org.osid.NullArgumentException <code>provisionable<code>
     *          is <code>null</code>
     */

    protected void putProvisionable(org.osid.provisioning.Provisionable provisionable) {
        this.provisionables.put(provisionable.getId(), provisionable);
        return;
    }


    /**
     *  Makes an array of provisionables available in this session.
     *
     *  @param  provisionables an array of provisionables
     *  @throws org.osid.NullArgumentException <code>provisionables<code>
     *          is <code>null</code>
     */

    protected void putProvisionables(org.osid.provisioning.Provisionable[] provisionables) {
        putProvisionables(java.util.Arrays.asList(provisionables));
        return;
    }


    /**
     *  Makes a collection of provisionables available in this session.
     *
     *  @param  provisionables a collection of provisionables
     *  @throws org.osid.NullArgumentException <code>provisionables<code>
     *          is <code>null</code>
     */

    protected void putProvisionables(java.util.Collection<? extends org.osid.provisioning.Provisionable> provisionables) {
        for (org.osid.provisioning.Provisionable provisionable : provisionables) {
            this.provisionables.put(provisionable.getId(), provisionable);
        }

        return;
    }


    /**
     *  Removes a Provisionable from this session.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @throws org.osid.NullArgumentException <code>provisionableId<code> is
     *          <code>null</code>
     */

    protected void removeProvisionable(org.osid.id.Id provisionableId) {
        this.provisionables.remove(provisionableId);
        return;
    }


    /**
     *  Gets the <code>Provisionable</code> specified by its <code>Id</code>.
     *
     *  @param  provisionableId <code>Id</code> of the <code>Provisionable</code>
     *  @return the provisionable
     *  @throws org.osid.NotFoundException <code>provisionableId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>provisionableId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.Provisionable provisionable = this.provisionables.get(provisionableId);
        if (provisionable == null) {
            throw new org.osid.NotFoundException("provisionable not found: " + provisionableId);
        }

        return (provisionable);
    }


    /**
     *  Gets all <code>Provisionables</code>. In plenary mode, the returned
     *  list contains all known provisionables or an error
     *  results. Otherwise, the returned list may contain only those
     *  provisionables that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Provisionables</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionables()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provisionable.ArrayProvisionableList(this.provisionables.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.provisionables.clear();
        super.close();
        return;
    }
}
