//
// AbstractBlockQuery.java
//
//     A template for making a Block Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.block.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for blocks.
 */

public abstract class AbstractBlockQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.hold.BlockQuery {

    private final java.util.Collection<org.osid.hold.records.BlockQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if an issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for an issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Matches blocks that have any issue. 
     *
     *  @param  match <code> true </code> to match blocks with any issue, 
     *          <code> false </code> to match blocks with no issue 
     */

    @OSID @Override
    public void matchAnyIssue(boolean match) {
        return;
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        return;
    }


    /**
     *  Sets the oubliette <code> Id </code> for this query to match blocks 
     *  assigned to foundries. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given block query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a block implementing the requested record.
     *
     *  @param blockRecordType a block record type
     *  @return the block query record
     *  @throws org.osid.NullArgumentException
     *          <code>blockRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blockRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.BlockQueryRecord getBlockQueryRecord(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.BlockQueryRecord record : this.records) {
            if (record.implementsRecordType(blockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this block query. 
     *
     *  @param blockQueryRecord block query record
     *  @param blockRecordType block record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBlockQueryRecord(org.osid.hold.records.BlockQueryRecord blockQueryRecord, 
                                          org.osid.type.Type blockRecordType) {

        addRecordType(blockRecordType);
        nullarg(blockQueryRecord, "block query record");
        this.records.add(blockQueryRecord);        
        return;
    }
}
