//
// AbstractKeyLookupSession.java
//
//    A starter implementation framework for providing a Key
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Key
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getKeys(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractKeyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authentication.keys.KeyLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();
    

    /**
     *  Gets the <code>Agency/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Agency Id</code> associated with this
     *          session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.agency.getId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.agency);
    }


    /**
     *  Sets the <code>Agency</code>.
     *
     *  @param  agency the agency for this session
     *  @throws org.osid.NullArgumentException <code>agency</code> is
     *          <code>null</code>
     */

    protected void setAgency(org.osid.authentication.Agency agency) {
        nullarg(agency, "agency");
        this.agency = agency;
        return;
    }

    /**
     *  Tests if this user can perform <code>Key</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupKeys() {
        return (true);
    }


    /**
     *  A complete view of the <code>Key</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeKeyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Key</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryKeyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include keys in agencies which are children of this
     *  agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Key</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>Key</code> may have a different <code> Id </code> than
     *  requested, such as the case where a duplicate <code> Id
     *  </code> was assigned to a <code>Key</code> and retained for
     *  compatibility.
     *
     *  @param  keyId <code>Id</code> of the
     *          <code>Key</code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code>keyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>keyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKey(org.osid.id.Id keyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authentication.keys.KeyList keys = getKeys()) {
            while (keys.hasNext()) {
                org.osid.authentication.keys.Key key = keys.getNextKey();
                if (key.getId().equals(keyId)) {
                    return (key);
                }
            }
        } 

        throw new org.osid.NotFoundException(keyId + " not found");
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the keys
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Keys</code> may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getKeys()</code>.
     *
     *  @param  keyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>keyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByIds(org.osid.id.IdList keyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.authentication.keys.Key> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = keyIds) {
                while (ids.hasNext()) {
                    org.osid.id.Id id = ids.getNextId();
                    try {
                        ret.add(getKey(id));
                    } catch (org.osid.NotFoundException nfe) {
                        if (!isComparative()) {
                            throw new org.osid.NotFoundException("key " + id + " not found");
                        }
                    }
                }
            }

        return (new net.okapia.osid.jamocha.authentication.keys.key.LinkedKeyList(ret));
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given key
     *  genus <code>Type</code> which does not include keys of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known keys
     *  or an error results. Otherwise, the returned list may contain
     *  only those keys that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getKeys()</code>.
     *
     *  @param  keyGenusType an key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authentication.keys.key.KeyGenusFilterList(getKeys(), keyGenusType));
    }


    /**
     *  Gets a <code>KeyList</code> corresponding to the given
     *  key genus <code>Type</code> and include any additional
     *  keys with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getKeys()</code>.
     *
     *  @param  keyGenusType an key genus type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByParentGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getKeysByGenusType(keyGenusType));
    }


    /**
     *  Gets a <code>KeyList</code> containing the given
     *  key record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known keys or
     *  an error results. Otherwise, the returned list may contain
     *  only those keys that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getKeys()</code>.
     *
     *  @param  keyRecordType a key record type 
     *  @return the returned <code>Key</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByRecordType(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authentication.keys.key.KeyRecordFilterList(getKeys(), keyRecordType));
    }


    /**
     *  Gets the agent key.
     *
     *  @param  agentId the <code> Id </code> of the <code> Agent </code>
     *  @return the key
     *  @throws org.osid.NotFoundException <code> agentId </code> is not found
     *          or no key exists
     *  @throws org.osid.NullArgumentException <code> agentId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKeyForAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authentication.keys.KeyList keys = getKeys()) {
            while (keys.hasNext()) {
                org.osid.authentication.keys.Key key = keys.getNextKey();
                if (key.getAgentId().equals(agentId)) {
                    return (key);
                }
            }
        }

        throw new org.osid.NotFoundException("key for agent " + agentId + " not found");
    }


    /**
     *  Gets all <code>Keys</code>. 
     *
     *  In plenary mode, the returned list contains all known keys or
     *  an error results. Otherwise, the returned list may contain
     *  only those keys that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Keys</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.authentication.keys.KeyList getKeys()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the key list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of keys
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.authentication.keys.KeyList filterKeysOnViews(org.osid.authentication.keys.KeyList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}