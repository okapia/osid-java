//
// AbstractAssemblyAwardEntryQuery.java
//
//     An AwardEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.chronicle.awardentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AwardEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyAwardEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.chronicle.AwardEntryQuery,
               org.osid.course.chronicle.AwardEntryQueryInspector,
               org.osid.course.chronicle.AwardEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAwardEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAwardEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getStudentIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        getAssembler().clearTerms(getStudentIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (getAssembler().getIdTerms(getStudentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStudentColumn(), style);
        return;
    }


    /**
     *  Gets the StudentId column name.
     *
     * @return the column name
     */

    protected String getStudentIdColumn() {
        return ("student_id");
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        getAssembler().clearTerms(getStudentColumn());
        return;
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Gets the Student column name.
     *
     * @return the column name
     */

    protected String getStudentColumn() {
        return ("student");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        getAssembler().addIdTerm(getAwardIdColumn(), awardId, match);
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        getAssembler().clearTerms(getAwardIdColumn());
        return;
    }


    /**
     *  Gets the award <code> Id </code> query terms. 
     *
     *  @return the award <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (getAssembler().getIdTerms(getAwardIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the award. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAward(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAwardColumn(), style);
        return;
    }


    /**
     *  Gets the AwardId column name.
     *
     * @return the column name
     */

    protected String getAwardIdColumn() {
        return ("award_id");
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        getAssembler().clearTerms(getAwardColumn());
        return;
    }


    /**
     *  Gets the award query terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Tests if an award order is available. 
     *
     *  @return <code> true </code> if an award order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSearchOrder() {
        return (false);
    }


    /**
     *  Gets the award order. 
     *
     *  @return the award search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchOrder getAwardSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAwardSearchOrder() is false");
    }


    /**
     *  Gets the Award column name.
     *
     * @return the column name
     */

    protected String getAwardColumn() {
        return ("award");
    }


    /**
     *  Matches award dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateAwarded(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateAwardedColumn(), from, to, match);
        return;
    }


    /**
     *  Matches entries that have any award date. 
     *
     *  @param  match <code> true </code> to match entries with any award 
     *          date, <code> false </code> to match entries with no award date 
     */

    @OSID @Override
    public void matchAnyDateAwarded(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateAwardedColumn(), match);
        return;
    }


    /**
     *  Clears the award date terms. 
     */

    @OSID @Override
    public void clearDateAwardedTerms() {
        getAssembler().clearTerms(getDateAwardedColumn());
        return;
    }


    /**
     *  Gets the award date query terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateAwardedTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateAwardedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the award date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDateAwarded(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateAwardedColumn(), style);
        return;
    }


    /**
     *  Gets the DateAwarded column name.
     *
     * @return the column name
     */

    protected String getDateAwardedColumn() {
        return ("date_awarded");
    }


    /**
     *  Sets the program <code> Id </code> for this query. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProgramColumn(), style);
        return;
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches entries that have any program. 
     *
     *  @param  match <code> true </code> to match entries with any program 
     *          <code> false </code> to match entries with no program 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramColumn(), match);
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the course <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches entries that have any course. 
     *
     *  @param  match <code> true </code> to match entries with any course, 
     *          <code> false </code> to match entries with no course 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseColumn(), match);
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query to match entries 
     *  that have an entry for the given assessment, 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches entries that have any assessment. 
     *
     *  @param  match <code> true </code> to match entries with any 
     *          assessment, <code> false </code> to match entries with no 
     *          assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getAssessmentTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if an assessment order is available. 
     *
     *  @return <code> true </code> if an assessment order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this awardEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  awardEntryRecordType an award entry record type 
     *  @return <code>true</code> if the awardEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type awardEntryRecordType) {
        for (org.osid.course.chronicle.records.AwardEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  awardEntryRecordType the award entry record type 
     *  @return the award entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryQueryRecord getAwardEntryQueryRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  awardEntryRecordType the award entry record type 
     *  @return the award entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord getAwardEntryQueryInspectorRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param awardEntryRecordType the award entry record type
     *  @return the award entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntrySearchOrderRecord getAwardEntrySearchOrderRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this award entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param awardEntryQueryRecord the award entry query record
     *  @param awardEntryQueryInspectorRecord the award entry query inspector
     *         record
     *  @param awardEntrySearchOrderRecord the award entry search order record
     *  @param awardEntryRecordType award entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryQueryRecord</code>,
     *          <code>awardEntryQueryInspectorRecord</code>,
     *          <code>awardEntrySearchOrderRecord</code> or
     *          <code>awardEntryRecordTypeawardEntry</code> is
     *          <code>null</code>
     */
            
    protected void addAwardEntryRecords(org.osid.course.chronicle.records.AwardEntryQueryRecord awardEntryQueryRecord, 
                                      org.osid.course.chronicle.records.AwardEntryQueryInspectorRecord awardEntryQueryInspectorRecord, 
                                      org.osid.course.chronicle.records.AwardEntrySearchOrderRecord awardEntrySearchOrderRecord, 
                                      org.osid.type.Type awardEntryRecordType) {

        addRecordType(awardEntryRecordType);

        nullarg(awardEntryQueryRecord, "award entry query record");
        nullarg(awardEntryQueryInspectorRecord, "award entry query inspector record");
        nullarg(awardEntrySearchOrderRecord, "award entry search odrer record");

        this.queryRecords.add(awardEntryQueryRecord);
        this.queryInspectorRecords.add(awardEntryQueryInspectorRecord);
        this.searchOrderRecords.add(awardEntrySearchOrderRecord);
        
        return;
    }
}
