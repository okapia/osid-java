//
// AbstractOsidObjectForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidObjectForm.
 */

public abstract class AbstractOsidObjectForm
    extends AbstractOsidBrowsableForm
    implements org.osid.OsidObjectForm {
    
    private String displayName;
    private boolean displayNameCleared = false;
    private org.osid.Metadata displayNameMetadata = getDefaultStringMetadata(getDisplayNameId(), "display name");    
    
    private String description;
    private boolean descriptionCleared = false;
    private org.osid.Metadata descriptionMetadata = getDefaultStringMetadata(getDescriptionId(), "description");    
    
    private org.osid.type.Type genusType;
    private boolean genusTypeCleared = false;
    private org.osid.Metadata genusTypeMetadata = getDefaultTypeMetadata(getGenusTypeId(), "genus type"); 


    /** 
     *  Constructs a new {@code AbstractOsidObjectForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidObjectForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the metadata for the display name.
     *
     *  @return metadata for the display name 
     */

    @OSID @Override
    public org.osid.Metadata getDisplayNameMetadata() {
        return (this.displayNameMetadata);
    }


    /**
     *  Sets the display name metadata.
     *
     *  @param metadata the display name metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setDisplayNameMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "display name metadata");
        this.displayNameMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the display name field.
     *
     *  @return the display name field Id
     */

    protected org.osid.id.Id getDisplayNameId() {
        return (OsidObjectElements.getDisplayName());
    }

    
    /**
     *  Sets a display name. 
     *
     *  @param  displayName the new display name 
     *  @throws org.osid.InvalidArgumentException <code> displayName </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> displayName </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setDisplayName(String displayName) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(displayName, getDisplayNameMetadata());

        this.displayName = displayName;
        this.displayNameCleared = false;

        return;
    }


    /**
     *  Clears the display name. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearDisplayName() {
        if (getDisplayNameMetadata().isRequired()) {
            throw new org.osid.NoAccessException("display name is required");
        }

        this.displayName = null;
        this.displayNameCleared = true;

        return;
    }


    /**
     *  Tests if the display name has been set in this form.
     *
     *  @return {@code true} if the display name has been set, {@code
     *          false} otherwise
     */

    protected boolean isDisplayNameSet() {
        return (this.displayName != null);
    }


    /**
     *  Tests if the display name has been cleared in this form.
     *
     *  @return {@code true} if the display name has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isDisplayNameCleared() {
        return (this.displayNameCleared);
    }


    /**
     *  Returns the current display name value.
     *
     *  @return the display name value or {@code null} if {@code
     *          isDisplayNameSet()} is {@code false}
     */

    protected String getDisplayName() {
        return (this.displayName);
    }


    /**
     *  Gets the metadata for the description.
     *
     *  @return metadata for the description 
     */

    @OSID @Override
    public org.osid.Metadata getDescriptionMetadata() {
        return (this.descriptionMetadata);
    }


    /**
     *  Sets the description metadata.
     *
     *  @param metadata the description metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setDescriptionMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "description metadata");
        this.descriptionMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the description field.
     *
     *  @return the description field Id
     */

    protected org.osid.id.Id getDescriptionId() {
        return (OsidObjectElements.getDescription());
    }

    
    /**
     *  Sets a description. 
     *
     *  @param  description the new description 
     *  @throws org.osid.InvalidArgumentException <code> description </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> description </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setDescription(String description) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(description, getDescriptionMetadata());

        this.description = description;
        this.descriptionCleared = false;

        return;
    }


    /**
     *  Clears the description. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearDescription() {
        if (getDescriptionMetadata().isRequired()) {
            throw new org.osid.NoAccessException("description is required");
        }

        this.description = null;
        this.descriptionCleared = true;

        return;
    }


    /**
     *  Tests if a description has been set in this form.
     *
     *  @return {@code true} if the description has been set, {@code
     *          false} otherwise
     */

    protected boolean isDescriptionSet() {
        return (this.description != null);
    }


    /**
     *  Tests if a description has been cleared in this form.
     *
     *  @return {@code true} if the description has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isDescriptionCleared() {
        return (this.descriptionCleared);
    }


    /**
     *  Returns the current description value.
     *
     *  @return the description value or {@code null} if {@code
     *          isDescriptionSet()} is {@code false}
     */

    protected String getDescription() {
        return (this.description);
    }


    /**
     *  Gets the metadata for the genus type.
     *
     *  @return metadata for the genus type 
     */

    @OSID @Override
    public org.osid.Metadata getGenusTypeMetadata() {
        return (this.genusTypeMetadata);
    }


    /**
     *  Sets the genus type metadata.
     *
     *  @param metadata the genus type metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setGenusTypeMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "genus type metadata");
        this.genusTypeMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the genus type field.
     *
     *  @return the genus type field Id
     */

    protected org.osid.id.Id getGenusTypeId() {
        return (OsidObjectElements.getGenusType());
    }

    
    /**
     *  Sets a genus type. 
     *
     *  @param  genusType the new genus type 
     *  @throws org.osid.InvalidArgumentException <code> genusType </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> genusType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void setGenusType(org.osid.type.Type genusType) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(genusType, getGenusTypeMetadata());

        this.genusType = genusType;
        this.genusTypeCleared = false;

        return;
    }


    /**
     *  Clears the genus type. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearGenusType() {
        if (getGenusTypeMetadata().isRequired()) {
            throw new org.osid.NoAccessException("genus type is required");
        }

        this.genusType = null;
        this.genusTypeCleared = true;

        return;
    }


    /**
     *  Tests if a genus type has been set in this form.
     *
     *  @return {@code true} if the genus type has been set, {@code
     *          false} otherwise
     */

    protected boolean isGenusTypeSet() {
        return (this.genusType != null);
    }


    /**
     *  Tests if a genus type has been cleared in this form.
     *
     *  @return {@code true} if the genus type has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isGenusTypeCleared() {
        return (this.genusTypeCleared);
    }


    /**
     *  Returns the current genus type value.
     *
     *  @return the genus type value or {@code null} if {@code
     *          isGenusTypeSet()} is {@code false}
     */

    protected org.osid.type.Type getGenusType() {
        return (this.genusType);
    }
}
