
CDX ("chemical/x-cdx", "ChemDraw eXchange", "cdx"),
    CIF("chemical/x-cif", "Crystallographic Interchange Format", "cif"),
    CMDF ("chemical/x-cmdf", "CrystalMaker Data Format", "cmdf"),
    CML ("chemical/x-cml", "Chemical Markup Language", "cml"),
    CSML ("chemical/x-csml", "Chemical Style Markup Language", "csml"),
    PDB ("chemical/x-pdb", "Protein Databank"),
    XYZ ("chemical/x-xyz", "Co-ordinate Animation Format", "xyz"),
