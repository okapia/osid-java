//
// AbstractMapDemographicLookupSession
//
//    A simple framework for providing a Demographic lookup service
//    backed by a fixed collection of demographics.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Demographic lookup service backed by a
 *  fixed collection of demographics. The demographics are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Demographics</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDemographicLookupSession
    extends net.okapia.osid.jamocha.resource.demographic.spi.AbstractDemographicLookupSession
    implements org.osid.resource.demographic.DemographicLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resource.demographic.Demographic> demographics = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resource.demographic.Demographic>());


    /**
     *  Makes a <code>Demographic</code> available in this session.
     *
     *  @param  demographic a demographic
     *  @throws org.osid.NullArgumentException <code>demographic<code>
     *          is <code>null</code>
     */

    protected void putDemographic(org.osid.resource.demographic.Demographic demographic) {
        this.demographics.put(demographic.getId(), demographic);
        return;
    }


    /**
     *  Makes an array of demographics available in this session.
     *
     *  @param  demographics an array of demographics
     *  @throws org.osid.NullArgumentException <code>demographics<code>
     *          is <code>null</code>
     */

    protected void putDemographics(org.osid.resource.demographic.Demographic[] demographics) {
        putDemographics(java.util.Arrays.asList(demographics));
        return;
    }


    /**
     *  Makes a collection of demographics available in this session.
     *
     *  @param  demographics a collection of demographics
     *  @throws org.osid.NullArgumentException <code>demographics<code>
     *          is <code>null</code>
     */

    protected void putDemographics(java.util.Collection<? extends org.osid.resource.demographic.Demographic> demographics) {
        for (org.osid.resource.demographic.Demographic demographic : demographics) {
            this.demographics.put(demographic.getId(), demographic);
        }

        return;
    }


    /**
     *  Removes a Demographic from this session.
     *
     *  @param  demographicId the <code>Id</code> of the demographic
     *  @throws org.osid.NullArgumentException <code>demographicId<code> is
     *          <code>null</code>
     */

    protected void removeDemographic(org.osid.id.Id demographicId) {
        this.demographics.remove(demographicId);
        return;
    }


    /**
     *  Gets the <code>Demographic</code> specified by its <code>Id</code>.
     *
     *  @param  demographicId <code>Id</code> of the <code>Demographic</code>
     *  @return the demographic
     *  @throws org.osid.NotFoundException <code>demographicId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>demographicId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.Demographic getDemographic(org.osid.id.Id demographicId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resource.demographic.Demographic demographic = this.demographics.get(demographicId);
        if (demographic == null) {
            throw new org.osid.NotFoundException("demographic not found: " + demographicId);
        }

        return (demographic);
    }


    /**
     *  Gets all <code>Demographics</code>. In plenary mode, the returned
     *  list contains all known demographics or an error
     *  results. Otherwise, the returned list may contain only those
     *  demographics that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Demographics</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographics()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographic.ArrayDemographicList(this.demographics.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.demographics.clear();
        super.close();
        return;
    }
}
