//
// AbstractRaceConstrainerEnablerQueryInspector.java
//
//     A template for making a RaceConstrainerEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for race constrainer enablers.
 */

public abstract class AbstractRaceConstrainerEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.voting.rules.RaceConstrainerEnablerQueryInspector {

    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the race constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRaceConstrainerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the race constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQueryInspector[] getRuledRaceConstrainerTerms() {
        return (new org.osid.voting.rules.RaceConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given race constrainer enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a race constrainer enabler implementing the requested record.
     *
     *  @param raceConstrainerEnablerRecordType a race constrainer enabler record type
     *  @return the race constrainer enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord getRaceConstrainerEnablerQueryInspectorRecord(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race constrainer enabler query. 
     *
     *  @param raceConstrainerEnablerQueryInspectorRecord race constrainer enabler query inspector
     *         record
     *  @param raceConstrainerEnablerRecordType raceConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceConstrainerEnablerQueryInspectorRecord(org.osid.voting.rules.records.RaceConstrainerEnablerQueryInspectorRecord raceConstrainerEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type raceConstrainerEnablerRecordType) {

        addRecordType(raceConstrainerEnablerRecordType);
        nullarg(raceConstrainerEnablerRecordType, "race constrainer enabler record type");
        this.records.add(raceConstrainerEnablerQueryInspectorRecord);        
        return;
    }
}
