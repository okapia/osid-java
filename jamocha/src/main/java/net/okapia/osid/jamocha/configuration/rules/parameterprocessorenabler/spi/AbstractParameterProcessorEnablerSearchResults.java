//
// AbstractParameterProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractParameterProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.configuration.rules.ParameterProcessorEnablerSearchResults {

    private org.osid.configuration.rules.ParameterProcessorEnablerList parameterProcessorEnablers;
    private final org.osid.configuration.rules.ParameterProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractParameterProcessorEnablerSearchResults.
     *
     *  @param parameterProcessorEnablers the result set
     *  @param parameterProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablers</code>
     *          or <code>parameterProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractParameterProcessorEnablerSearchResults(org.osid.configuration.rules.ParameterProcessorEnablerList parameterProcessorEnablers,
                                            org.osid.configuration.rules.ParameterProcessorEnablerQueryInspector parameterProcessorEnablerQueryInspector) {
        nullarg(parameterProcessorEnablers, "parameter processor enablers");
        nullarg(parameterProcessorEnablerQueryInspector, "parameter processor enabler query inspectpr");

        this.parameterProcessorEnablers = parameterProcessorEnablers;
        this.inspector = parameterProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the parameter processor enabler list resulting from a search.
     *
     *  @return a parameter processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablers() {
        if (this.parameterProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.configuration.rules.ParameterProcessorEnablerList parameterProcessorEnablers = this.parameterProcessorEnablers;
        this.parameterProcessorEnablers = null;
	return (parameterProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.configuration.rules.ParameterProcessorEnablerQueryInspector getParameterProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  parameter processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a parameterProcessorEnabler implementing the requested
     *  record.
     *
     *  @param parameterProcessorEnablerSearchRecordType a parameterProcessorEnabler search 
     *         record type 
     *  @return the parameter processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(parameterProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorEnablerSearchResultsRecord getParameterProcessorEnablerSearchResultsRecord(org.osid.type.Type parameterProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.configuration.rules.records.ParameterProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(parameterProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record parameter processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addParameterProcessorEnablerRecord(org.osid.configuration.rules.records.ParameterProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "parameter processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
