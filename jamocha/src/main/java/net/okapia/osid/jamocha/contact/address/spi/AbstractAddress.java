//
// AbstractAddress.java
//
//     Defines an Address.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Address</code>.
 */

public abstract class AbstractAddress
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.contact.Address {

    private org.osid.resource.Resource resource;
    private org.osid.locale.DisplayText addressText;

    private final java.util.Collection<org.osid.contact.records.AddressRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the resource <code> Id </code> to which this address
     *  belongs. All addresses belong to one resource but may be used
     *  for contacts among multiple other resources.
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource to which this address belongs. All addresses
     *  belong to one resource but may be used for contacts among
     *  multiple other resources.
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the textual representation of this address. 
     *
     *  @return the address 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getAddressText() {
        return (this.addressText);
    }


    /**
     *  Sets the address text.
     *
     *  @param text an address text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    protected void setAddressText(org.osid.locale.DisplayText text) {
        nullarg(text, "address text");
        this.addressText = text;
        return;
    }


    /**
     *  Tests if this address supports the given record
     *  <code>Type</code>.
     *
     *  @param  addressRecordType an address record type 
     *  @return <code>true</code> if the addressRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type addressRecordType) {
        for (org.osid.contact.records.AddressRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Address</code> record <code>Type</code>.
     *
     *  @param  addressRecordType the address record type 
     *  @return the address record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressRecord getAddressRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param addressRecord the address record
     *  @param addressRecordType address record type
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecord</code> or
     *          <code>addressRecordType</code> is <code>null</code>
     */
            
    protected void addAddressRecord(org.osid.contact.records.AddressRecord addressRecord, 
                                    org.osid.type.Type addressRecordType) {

        nullarg(addressRecord, "address record");
        addRecordType(addressRecordType);
        this.records.add(addressRecord);
        
        return;
    }
}
