//
// AbstractContactPeerList
//
//     Implements a filter for a ContactPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.contact.batch.contactpeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ContactPeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedContactPeerList
 *  to improve performance.
 */

public abstract class AbstractContactPeerFilterList
    extends net.okapia.osid.jamocha.contact.batch.contactpeer.spi.AbstractContactPeerList
    implements org.osid.contact.batch.ContactPeerList,
               net.okapia.osid.jamocha.inline.filter.contact.batch.contactpeer.ContactPeerFilter {

    private org.osid.contact.batch.ContactPeer contactPeer;
    private final org.osid.contact.batch.ContactPeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractContactPeerFilterList</code>.
     *
     *  @param contactPeerList a <code>ContactPeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>contactPeerList</code> is <code>null</code>
     */

    protected AbstractContactPeerFilterList(org.osid.contact.batch.ContactPeerList contactPeerList) {
        nullarg(contactPeerList, "contact peer list");
        this.list = contactPeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.contactPeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ContactPeer </code> in this list. 
     *
     *  @return the next <code> ContactPeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ContactPeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.batch.ContactPeer getNextContactPeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.contact.batch.ContactPeer contactPeer = this.contactPeer;
            this.contactPeer = null;
            return (contactPeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in contact peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.contactPeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ContactPeers.
     *
     *  @param contactPeer the contact peer to filter
     *  @return <code>true</code> if the contact peer passes the filter,
     *          <code>false</code> if the contact peer should be filtered
     */

    public abstract boolean pass(org.osid.contact.batch.ContactPeer contactPeer);


    protected void prime() {
        if (this.contactPeer != null) {
            return;
        }

        org.osid.contact.batch.ContactPeer contactPeer = null;

        while (this.list.hasNext()) {
            try {
                contactPeer = this.list.getNextContactPeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(contactPeer)) {
                this.contactPeer = contactPeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
