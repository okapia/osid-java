//
// AbstractAliasResponse.java
//
//     Defines an AliasResponse.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.batch.aliasresponse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AliasResponse</code>.
 */

public abstract class AbstractAliasResponse
    implements org.osid.transaction.batch.AliasResponse {

    private org.osid.id.Id referenceId;
    private org.osid.id.Id aliasId;
    private org.osid.locale.DisplayText message;
    

    /**
     *  Gets the reference <code> Id </code>.
     *
     *  @return the reference object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.referenceId);
    }


    /**
     *  Sets the reference Id.
     *
     *  @param referenceId the reference Id
     *  @throws org.osid.NullArgumentException <code>referenceId</code> is
     *          <code>null</code>
     */
    
    protected void setReferenceId(org.osid.id.Id referenceId) {
        nullarg(referenceId, "reference Id");
        this.referenceId = referenceId;
        return;
    }


    /**
     *  Gets the alias <code>Id</code>.
     *
     *  @return the reference object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAliasId() {
        return (this.aliasId);
    }


    /**
     *  Sets the alias Id.
     *
     *  @param aliasId the alias Id
     *  @throws org.osid.NullArgumentException <code>aliasId</code> is
     *          <code>null</code>
     */
    
    protected void setAliasId(org.osid.id.Id aliasId) {
        nullarg(aliasId, "alias Id");
        this.aliasId = aliasId;
        return;
    }


    /**
     *  Tests if this item within the alias operation was successful. 
     *
     *  @return <code> true </code> if the alias operation was
     *          successful, <code> false </code> if it was not
     *          successful
     */

    @OSID @Override
    public boolean isSuccessful() {
        if (this.message == null) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the error message for an unsuccessful item within the
     *  alias operation.
     *
     *  @return the message 
     *  @throws org.osid.IllegalStateException <code> isSuccessful()
     *          </code> is <code> true </code>
     */

    @OSID @Override
    public org.osid.locale.DisplayText getErrorMessage() {
        if (isSuccessful()) {
            throw new org.osid.IllegalStateException("isSuccessful() is true");
        }

        return (this.message);
    }

    
    /**
     *  Sets the error message.
     *
     *  @param message the error message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    protected void setErrorMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "error message");
        this.message = message;
        return;
    }
}
