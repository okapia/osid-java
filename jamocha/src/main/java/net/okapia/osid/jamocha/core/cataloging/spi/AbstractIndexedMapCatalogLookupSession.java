//
// AbstractIndexedMapCatalogLookupSession.java
//
//    A simple framework for providing a Catalog lookup service
//    backed by a fixed collection of catalogs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Catalog lookup service backed by a
 *  fixed collection of catalogs. The catalogs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some catalogs may be compatible
 *  with more types than are indicated through these catalog
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Catalogs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCatalogLookupSession
    extends AbstractMapCatalogLookupSession
    implements org.osid.cataloging.CatalogLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.cataloging.Catalog> catalogsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.cataloging.Catalog>());
    private final MultiMap<org.osid.type.Type, org.osid.cataloging.Catalog> catalogsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.cataloging.Catalog>());


    /**
     *  Makes a <code>Catalog</code> available in this session.
     *
     *  @param  catalog a catalog
     *  @throws org.osid.NullArgumentException <code>catalog<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCatalog(org.osid.cataloging.Catalog catalog) {
        super.putCatalog(catalog);

        this.catalogsByGenus.put(catalog.getGenusType(), catalog);
        
        try (org.osid.type.TypeList types = catalog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.catalogsByRecord.put(types.getNextType(), catalog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a catalog from this session.
     *
     *  @param catalogId the <code>Id</code> of the catalog
     *  @throws org.osid.NullArgumentException <code>catalogId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCatalog(org.osid.id.Id catalogId) {
        org.osid.cataloging.Catalog catalog;
        try {
            catalog = getCatalog(catalogId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.catalogsByGenus.remove(catalog.getGenusType());

        try (org.osid.type.TypeList types = catalog.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.catalogsByRecord.remove(types.getNextType(), catalog);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCatalog(catalogId);
        return;
    }


    /**
     *  Gets a <code>CatalogList</code> corresponding to the given
     *  catalog genus <code>Type</code> which does not include
     *  catalogs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known catalogs or an error results. Otherwise,
     *  the returned list may contain only those catalogs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  catalogGenusType a catalog genus type 
     *  @return the returned <code>Catalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByGenusType(org.osid.type.Type catalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.catalog.ArrayCatalogList(this.catalogsByGenus.get(catalogGenusType)));
    }


    /**
     *  Gets a <code>CatalogList</code> containing the given
     *  catalog record <code>Type</code>. In plenary mode, the
     *  returned list contains all known catalogs or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalogs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  catalogRecordType a catalog record type 
     *  @return the returned <code>catalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByRecordType(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.catalog.ArrayCatalogList(this.catalogsByRecord.get(catalogRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogsByGenus.clear();
        this.catalogsByRecord.clear();

        super.close();

        return;
    }
}
