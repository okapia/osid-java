//
// AbstractJournalEntryQuery.java
//
//     A template for making a JournalEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for journal entries.
 */

public abstract class AbstractJournalEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.journaling.JournalEntryQuery {

    private final java.util.Collection<org.osid.journaling.records.JournalEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the branch <code> Id </code> for this query to match branches 
     *  assigned to journals. 
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> branchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchId(org.osid.id.Id branchId, boolean match) {
        return;
    }


    /**
     *  Clears the branch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBranchIdTerms() {
        return;
    }


    /**
     *  Tests if a branch query is available. 
     *
     *  @return <code> true </code> if a branch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branch. 
     *
     *  @return the branch query 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuery getBranchQuery() {
        throw new org.osid.UnimplementedException("supportsBranchQuery() is false");
    }


    /**
     *  Clears the branch terms. 
     */

    @OSID @Override
    public void clearBranchTerms() {
        return;
    }


    /**
     *  Sets the source <code> Id. </code> 
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id sourceId, boolean match) {
        return;
    }


    /**
     *  Clears the source <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        return;
    }


    /**
     *  Sets the version <code> Id. </code> 
     *
     *  @param  versionId a version <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> versionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionId(org.osid.id.Id versionId, boolean match) {
        return;
    }


    /**
     *  Clears the version <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVersionIdTerms() {
        return;
    }


    /**
     *  Matches entries falling between the given times inclusive. 
     *
     *  @param  from start time 
     *  @param  to end time 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        return;
    }


    /**
     *  Matches entries following the given timestamp inclusive. 
     *
     *  @param  from start time 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> from </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEntriesSince(org.osid.calendaring.DateTime from, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the entries since terms. 
     */

    @OSID @Override
    public void clearEntriesSinceTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets an agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match entries 
     *  assigned to journals. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchJournalId(org.osid.id.Id journalId, boolean match) {
        return;
    }


    /**
     *  Clears the journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearJournalIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> supportsJournalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getJournalQuery() {
        throw new org.osid.UnimplementedException("supportsJournalQuery() is false");
    }


    /**
     *  Clears the journal terms. 
     */

    @OSID @Override
    public void clearJournalTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given journal entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a journal entry implementing the requested record.
     *
     *  @param journalEntryRecordType a journal entry record type
     *  @return the journal entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryQueryRecord getJournalEntryQueryRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal entry query. 
     *
     *  @param journalEntryQueryRecord journal entry query record
     *  @param journalEntryRecordType journalEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJournalEntryQueryRecord(org.osid.journaling.records.JournalEntryQueryRecord journalEntryQueryRecord, 
                                          org.osid.type.Type journalEntryRecordType) {

        addRecordType(journalEntryRecordType);
        nullarg(journalEntryQueryRecord, "journal entry query record");
        this.records.add(journalEntryQueryRecord);        
        return;
    }
}
