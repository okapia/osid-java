//
// AbstractPersonQuery.java
//
//     A template for making a Person Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for persons.
 */

public abstract class AbstractPersonQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.personnel.PersonQuery {

    private final java.util.Collection<org.osid.personnel.records.PersonQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a salutation. 
     *
     *  @param  salutation a salutation 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> salutation </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> salutation </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSalutation(String salutation, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        return;
    }


    /**
     *  Matches persons with any salutation. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          salutation, <code> false </code> to match persons with no 
     *          salutation 
     */

    @OSID @Override
    public void matchAnySalutation(boolean match) {
        return;
    }


    /**
     *  Clears all salutation terms. 
     */

    @OSID @Override
    public void clearSalutationTerms() {
        return;
    }


    /**
     *  Matches a given name. 
     *
     *  @param  givenName a given name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> givenName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> givenName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchGivenName(String givenName, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        return;
    }


    /**
     *  Matches persons with any given name. 
     *
     *  @param  match <code> true </code> to match persons with any given 
     *          name, <code> false </code> to match persons with no given name 
     */

    @OSID @Override
    public void matchAnyGivenName(boolean match) {
        return;
    }


    /**
     *  Clears all given name terms. 
     */

    @OSID @Override
    public void clearGivenNameTerms() {
        return;
    }


    /**
     *  Matches a preferred name. 
     *
     *  @param  preferredName a preferred name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> preferredName </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> preferredName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPreferredName(String preferredName, 
                                   org.osid.type.Type stringMatchType, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches persons with any preferred name. 
     *
     *  @param  match <code> true </code> to match persons with any preferred 
     *          name, <code> false </code> to match persons with no preferred 
     *          name 
     */

    @OSID @Override
    public void matchAnyPreferredName(boolean match) {
        return;
    }


    /**
     *  Clears all preferred name terms. 
     */

    @OSID @Override
    public void clearPreferredNameTerms() {
        return;
    }


    /**
     *  Matches a forename alias. 
     *
     *  @param  forenameAlias a forename alias 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> forenameAlias </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> forenameAlias </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchForenameAlias(String forenameAlias, 
                                   org.osid.type.Type stringMatchType, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches persons with any forename alias. 
     *
     *  @param  match <code> true </code> to match persons with any forename 
     *          alias, <code> false </code> to match persons with no forename 
     *          aliases 
     */

    @OSID @Override
    public void matchAnyForenameAlias(boolean match) {
        return;
    }


    /**
     *  Clears all forename alias terms. 
     */

    @OSID @Override
    public void clearForenameAliasTerms() {
        return;
    }


    /**
     *  Matches a middle name. 
     *
     *  @param  middleName a middle name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> middleName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> middleName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchMiddleName(String middleName, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        return;
    }


    /**
     *  Matches persons with any middle name. 
     *
     *  @param  match <code> true </code> to match persons with any middle 
     *          name, <code> false </code> to match persons with no middle 
     *          name 
     */

    @OSID @Override
    public void matchAnyMiddleName(boolean match) {
        return;
    }


    /**
     *  Clears all middle name terms. 
     */

    @OSID @Override
    public void clearMiddleNameTerms() {
        return;
    }


    /**
     *  Matches a surname. 
     *
     *  @param  surName a surname 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> surName </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> surName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSurname(String surName, 
                             org.osid.type.Type stringMatchType, boolean match) {
        return;
    }


    /**
     *  Matches persons with any surname. 
     *
     *  @param  match <code> true </code> to match persons with any surname, 
     *          <code> false </code> to match persons with no surname 
     */

    @OSID @Override
    public void matchAnySurname(boolean match) {
        return;
    }


    /**
     *  Clears all surname terms. 
     */

    @OSID @Override
    public void clearSurnameTerms() {
        return;
    }


    /**
     *  Matches a surname alias. 
     *
     *  @param  surnameAlias a surname alias 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> surnameAlias </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> surnameAlias </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSurnameAlias(String surnameAlias, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches persons with any surname alias. 
     *
     *  @param  match <code> true </code> to match persons with any surname 
     *          alias, <code> false </code> to match persons with no surname 
     *          alias 
     */

    @OSID @Override
    public void matchAnySurnameAlias(boolean match) {
        return;
    }


    /**
     *  Clears all surname alias terms. 
     */

    @OSID @Override
    public void clearSurnameAliasTerms() {
        return;
    }


    /**
     *  Matches a generation qualifier. 
     *
     *  @param  generationQualifier a generation qualifier 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> generationQualifier 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> generationQualifier 
     *          </code> or <code> stringMatchType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchGenerationQualifier(String generationQualifier, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches persons with any generation qualifier. 
     *
     *  @param  match <code> true </code> to match persons with any generation 
     *          qualifier, <code> false </code> to match persons with no 
     *          generation qualifier 
     */

    @OSID @Override
    public void matchAnyGenerationQualifier(boolean match) {
        return;
    }


    /**
     *  Clears all generation qualifier terms. 
     */

    @OSID @Override
    public void clearGenerationQualifierTerms() {
        return;
    }


    /**
     *  Matches a qualification suffix. 
     *
     *  @param  qualificationSuffix a qualification suffix 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> qualificationSuffix 
     *          </code> is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> qualificationSuffix 
     *          </code> or <code> stringMatchType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchQualificationSuffix(String qualificationSuffix, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches persons with any qualification suffix. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          qualification sufix, <code> false </code> to match persons 
     *          with no qualification suffix 
     */

    @OSID @Override
    public void matchAnyQualificationSuffix(boolean match) {
        return;
    }


    /**
     *  Clears all qualification suffix terms. 
     */

    @OSID @Override
    public void clearQualificationSuffixTerms() {
        return;
    }


    /**
     *  Matches a birth date bwteen the given range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBirthDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches persons with any birth date. 
     *
     *  @param  match <code> true </code> to match persons with any date of 
     *          birth, <code> false </code> to match persons with no date of 
     *          birth 
     */

    @OSID @Override
    public void matchAnyBirthDate(boolean match) {
        return;
    }


    /**
     *  Clears all birth date terms. 
     */

    @OSID @Override
    public void clearBirthDateTerms() {
        return;
    }


    /**
     *  Matches a death date between the given range inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDeathDate(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches persons with any death date. 
     *
     *  @param  match <code> true </code> to match persons with any date of 
     *          death, <code> false </code> to match persons who might still 
     *          be alive 
     */

    @OSID @Override
    public void matchAnyDeathDate(boolean match) {
        return;
    }


    /**
     *  Clears all death date terms. 
     */

    @OSID @Override
    public void clearDeathDateTerms() {
        return;
    }


    /**
     *  Matches a persons alive within the given dates inclusive. 
     *
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLivingDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        return;
    }


    /**
     *  Clears all living date terms. 
     */

    @OSID @Override
    public void clearLivingDateTerms() {
        return;
    }


    /**
     *  Matches an institutional identifier. 
     *
     *  @param  identifier an institutional identifier 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> identifier </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> identifier </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchInstitutionalIdentifier(String identifier, 
                                             org.osid.type.Type stringMatchType, 
                                             boolean match) {
        return;
    }


    /**
     *  Matches persons with any institutional identifier. 
     *
     *  @param  match <code> true </code> to match persons with any 
     *          institutional identifier, <code> false </code> to match 
     *          persons with no instituional identifier 
     */

    @OSID @Override
    public void matchAnyInstitutionalIdentifier(boolean match) {
        return;
    }


    /**
     *  Clears all institutional identifier terms. 
     */

    @OSID @Override
    public void clearInstitutionalIdentifierTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match persons 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given person query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a person implementing the requested record.
     *
     *  @param personRecordType a person record type
     *  @return the person query record
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(personRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PersonQueryRecord getPersonQueryRecord(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.PersonQueryRecord record : this.records) {
            if (record.implementsRecordType(personRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(personRecordType + " is not supported");
    }


    /**
     *  Adds a record to this person query. 
     *
     *  @param personQueryRecord person query record
     *  @param personRecordType person record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPersonQueryRecord(org.osid.personnel.records.PersonQueryRecord personQueryRecord, 
                                          org.osid.type.Type personRecordType) {

        addRecordType(personRecordType);
        nullarg(personQueryRecord, "person query record");
        this.records.add(personQueryRecord);        
        return;
    }
}
