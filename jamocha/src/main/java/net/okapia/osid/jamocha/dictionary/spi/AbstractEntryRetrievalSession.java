//
// AbstractEntryRetrievalSession.java
//
//    A starter implementation framework for providing an Entry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Entry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractEntryRetrievalSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.dictionary.EntryRetrievalSession {

    private boolean federated = false;
    private org.osid.dictionary.Dictionary dictionary = new net.okapia.osid.jamocha.nil.dictionary.dictionary.UnknownDictionary();
    

    /**
     *  Gets the <code>Dictionary/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Dictionary Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDictionaryId() {
        return (this.dictionary.getId());
    }


    /**
     *  Gets the <code>Dictionary</code> associated with this 
     *  session.
     *
     *  @return the <code>Dictionary</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.dictionary);
    }


    /**
     *  Sets the <code>Dictionary</code>.
     *
     *  @param  dictionary the dictionary for this session
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>
     */

    protected void setDictionary(org.osid.dictionary.Dictionary dictionary) {
        nullarg(dictionary, "dictionary");
        this.dictionary = dictionary;
        return;
    }


    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in dictionaries which are children
     *  of this dictionary in the dictionary hierarchy.
     */

    @OSID @Override
    public void useFederatedDictionaryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this dictionary only.
     */

    @OSID @Override
    public void useIsolatedDictionaryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Gets the <code> Dictionary </code> entry associated with the given key 
     *  and types. The <code> keyType </code> indicates the key object type 
     *  and the <code> valueType </code> indicates the value object return. 
     *
     *  @param  key the key of the entry to retrieve 
     *  @param  keyType the key type of the entry to retrieve 
     *  @param  valueType the value type of the entry to retrieve 
     *  @return the returned <code> object </code> 
     *  @throws org.osid.InvalidArgumentException <code> key </code> is not of 
     *          <code> keyType </code> 
     *  @throws org.osid.NotFoundException no entry found 
     *  @throws org.osid.NullArgumentException <code> key, keyType </code> or 
     *          <code> valueType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract java.lang.Object retrieveEntry(java.lang.Object key, 
                                                   org.osid.type.Type keyType, 
                                                   org.osid.type.Type valueType)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;
}
