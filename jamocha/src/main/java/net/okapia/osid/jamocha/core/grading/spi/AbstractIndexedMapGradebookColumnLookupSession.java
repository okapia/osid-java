//
// AbstractIndexedMapGradebookColumnLookupSession.java
//
//    A simple framework for providing a GradebookColumn lookup service
//    backed by a fixed collection of gradebook columns with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a GradebookColumn lookup service backed by a
 *  fixed collection of gradebook columns. The gradebook columns are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some gradebook columns may be compatible
 *  with more types than are indicated through these gradebook column
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradebookColumns</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradebookColumnLookupSession
    extends AbstractMapGradebookColumnLookupSession
    implements org.osid.grading.GradebookColumnLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.GradebookColumn> gradebookColumnsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradebookColumn>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.GradebookColumn> gradebookColumnsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.GradebookColumn>());


    /**
     *  Makes a <code>GradebookColumn</code> available in this session.
     *
     *  @param  gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException <code>gradebookColumn<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.putGradebookColumn(gradebookColumn);

        this.gradebookColumnsByGenus.put(gradebookColumn.getGenusType(), gradebookColumn);
        
        try (org.osid.type.TypeList types = gradebookColumn.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebookColumnsByRecord.put(types.getNextType(), gradebookColumn);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a gradebook column from this session.
     *
     *  @param gradebookColumnId the <code>Id</code> of the gradebook column
     *  @throws org.osid.NullArgumentException <code>gradebookColumnId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradebookColumn(org.osid.id.Id gradebookColumnId) {
        org.osid.grading.GradebookColumn gradebookColumn;
        try {
            gradebookColumn = getGradebookColumn(gradebookColumnId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradebookColumnsByGenus.remove(gradebookColumn.getGenusType());

        try (org.osid.type.TypeList types = gradebookColumn.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebookColumnsByRecord.remove(types.getNextType(), gradebookColumn);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradebookColumn(gradebookColumnId);
        return;
    }


    /**
     *  Gets a <code>GradebookColumnList</code> corresponding to the given
     *  gradebook column genus <code>Type</code> which does not include
     *  gradebook columns of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known gradebook columns or an error results. Otherwise,
     *  the returned list may contain only those gradebook columns that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradebookColumnGenusType a gradebook column genus type 
     *  @return the returned <code>GradebookColumn</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByGenusType(org.osid.type.Type gradebookColumnGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebookcolumn.ArrayGradebookColumnList(this.gradebookColumnsByGenus.get(gradebookColumnGenusType)));
    }


    /**
     *  Gets a <code>GradebookColumnList</code> containing the given
     *  gradebook column record <code>Type</code>. In plenary mode, the
     *  returned list contains all known gradebook columns or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebook columns that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradebookColumnRecordType a gradebook column record type 
     *  @return the returned <code>gradebookColumn</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByRecordType(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebookcolumn.ArrayGradebookColumnList(this.gradebookColumnsByRecord.get(gradebookColumnRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebookColumnsByGenus.clear();
        this.gradebookColumnsByRecord.clear();

        super.close();

        return;
    }
}
