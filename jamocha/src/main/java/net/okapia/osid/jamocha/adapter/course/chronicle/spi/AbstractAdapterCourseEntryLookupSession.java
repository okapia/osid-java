//
// AbstractAdapterCourseEntryLookupSession.java
//
//    A CourseEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CourseEntry lookup session adapter.
 */

public abstract class AbstractAdapterCourseEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.chronicle.CourseEntryLookupSession {

    private final org.osid.course.chronicle.CourseEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCourseEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCourseEntryLookupSession(org.osid.course.chronicle.CourseEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code CourseEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCourseEntries() {
        return (this.session.canLookupCourseEntries());
    }


    /**
     *  A complete view of the {@code CourseEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseEntryView() {
        this.session.useComparativeCourseEntryView();
        return;
    }


    /**
     *  A complete view of the {@code CourseEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseEntryView() {
        this.session.usePlenaryCourseEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only course entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCourseEntryView() {
        this.session.useEffectiveCourseEntryView();
        return;
    }
    

    /**
     *  All course entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCourseEntryView() {
        this.session.useAnyEffectiveCourseEntryView();
        return;
    }

     
    /**
     *  Gets the {@code CourseEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CourseEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CourseEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param courseEntryId {@code Id} of the {@code CourseEntry}
     *  @return the course entry
     *  @throws org.osid.NotFoundException {@code courseEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code courseEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntry getCourseEntry(org.osid.id.Id courseEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntry(courseEntryId));
    }


    /**
     *  Gets a {@code CourseEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CourseEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CourseEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code courseEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByIds(org.osid.id.IdList courseEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesByIds(courseEntryIds));
    }


    /**
     *  Gets a {@code CourseEntryList} corresponding to the given
     *  course entry genus {@code Type} which does not include
     *  course entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseEntryGenusType a courseEntry genus type 
     *  @return the returned {@code CourseEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByGenusType(org.osid.type.Type courseEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesByGenusType(courseEntryGenusType));
    }


    /**
     *  Gets a {@code CourseEntryList} corresponding to the given
     *  course entry genus {@code Type} and include any additional
     *  course entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseEntryGenusType a courseEntry genus type 
     *  @return the returned {@code CourseEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByParentGenusType(org.osid.type.Type courseEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesByParentGenusType(courseEntryGenusType));
    }


    /**
     *  Gets a {@code CourseEntryList} containing the given
     *  course entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseEntryRecordType a courseEntry record type 
     *  @return the returned {@code CourseEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code courseEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByRecordType(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesByRecordType(courseEntryRecordType));
    }


    /**
     *  Gets a {@code CourseEntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *  
     *  In active mode, course entries are returned that are currently
     *  active. In any status mode, active and inactive course entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CourseEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of course entries corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForStudent(resourceId));
    }


    /**
     *  Gets a list of course entries corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of course entries corresponding to a course
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  courseId the {@code Id} of the course
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code courseId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForCourse(courseId));
    }


    /**
     *  Gets a list of course entries corresponding to a course
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  courseId the {@code Id} of the course
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code courseId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForCourseOnDate(org.osid.id.Id courseId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForCourseOnDate(courseId, from, to));
    }


    /**
     *  Gets a list of course entries corresponding to student and course
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  courseId the {@code Id} of the course
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code courseId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentAndCourse(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForStudentAndCourse(resourceId, courseId));
    }


    /**
     *  Gets a list of course entries corresponding to student and course
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective. In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseId the {@code Id} of the course
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CourseEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code courseId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentAndCourseOnDate(org.osid.id.Id resourceId,
                                                                                               org.osid.id.Id courseId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntriesForStudentAndCourseOnDate(resourceId, courseId, from, to));
    }


    /**
     *  Gets all {@code CourseEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code CourseEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCourseEntries());
    }
}
