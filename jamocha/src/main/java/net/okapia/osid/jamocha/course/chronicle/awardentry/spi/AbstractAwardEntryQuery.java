//
// AbstractAwardEntryQuery.java
//
//     A template for making an AwardEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.awardentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for award entries.
 */

public abstract class AbstractAwardEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.chronicle.AwardEntryQuery {

    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        return;
    }


    /**
     *  Matches award dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDateAwarded(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches entries that have any award date. 
     *
     *  @param  match <code> true </code> to match entries with any award 
     *          date, <code> false </code> to match entries with no award date 
     */

    @OSID @Override
    public void matchAnyDateAwarded(boolean match) {
        return;
    }


    /**
     *  Clears the award date terms. 
     */

    @OSID @Override
    public void clearDateAwardedTerms() {
        return;
    }


    /**
     *  Sets the program <code> Id </code> for this query. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches entries that have any program. 
     *
     *  @param  match <code> true </code> to match entries with any program 
     *          <code> false </code> to match entries with no program 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        return;
    }


    /**
     *  Sets the course <code> Id </code> for this query to match entries that 
     *  have an entry for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches entries that have any course. 
     *
     *  @param  match <code> true </code> to match entries with any course, 
     *          <code> false </code> to match entries with no course 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets the assessment <code> Id </code> for this query to match entries 
     *  that have an entry for the given assessment, 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches entries that have any assessment. 
     *
     *  @param  match <code> true </code> to match entries with any 
     *          assessment, <code> false </code> to match entries with no 
     *          assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given award entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an award entry implementing the requested record.
     *
     *  @param awardEntryRecordType an award entry record type
     *  @return the award entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryQueryRecord getAwardEntryQueryRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award entry query. 
     *
     *  @param awardEntryQueryRecord award entry query record
     *  @param awardEntryRecordType awardEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAwardEntryQueryRecord(org.osid.course.chronicle.records.AwardEntryQueryRecord awardEntryQueryRecord, 
                                          org.osid.type.Type awardEntryRecordType) {

        addRecordType(awardEntryRecordType);
        nullarg(awardEntryQueryRecord, "award entry query record");
        this.records.add(awardEntryQueryRecord);        
        return;
    }
}
