//
// AbstractResourceRelationshipQueryInspector.java
//
//     A template for making a ResourceRelationshipQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for resource relationships.
 */

public abstract class AbstractResourceRelationshipQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.resource.ResourceRelationshipQueryInspector {

    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the source resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the source resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Destination resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the Destination resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDestinationResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the same resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSameResourceTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given resource relationship query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a resource relationship implementing the requested record.
     *
     *  @param resourceRelationshipRecordType a resource relationship record type
     *  @return the resource relationship query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipQueryInspectorRecord getResourceRelationshipQueryInspectorRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource relationship query. 
     *
     *  @param resourceRelationshipQueryInspectorRecord resource relationship query inspector
     *         record
     *  @param resourceRelationshipRecordType resourceRelationship record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResourceRelationshipQueryInspectorRecord(org.osid.resource.records.ResourceRelationshipQueryInspectorRecord resourceRelationshipQueryInspectorRecord, 
                                                   org.osid.type.Type resourceRelationshipRecordType) {

        addRecordType(resourceRelationshipRecordType);
        nullarg(resourceRelationshipRecordType, "resource relationship record type");
        this.records.add(resourceRelationshipQueryInspectorRecord);        
        return;
    }
}
