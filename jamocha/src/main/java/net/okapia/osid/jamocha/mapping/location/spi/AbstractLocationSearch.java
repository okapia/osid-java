//
// AbstractLocationSearch.java
//
//     A template for making a Location Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing location searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractLocationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.LocationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.records.LocationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.LocationSearchOrder locationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of locations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  locationIds list of locations
     *  @throws org.osid.NullArgumentException
     *          <code>locationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongLocations(org.osid.id.IdList locationIds) {
        while (locationIds.hasNext()) {
            try {
                this.ids.add(locationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongLocations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of location Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getLocationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  locationSearchOrder location search order 
     *  @throws org.osid.NullArgumentException
     *          <code>locationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>locationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderLocationResults(org.osid.mapping.LocationSearchOrder locationSearchOrder) {
	this.locationSearchOrder = locationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.LocationSearchOrder getLocationSearchOrder() {
	return (this.locationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given location search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a location implementing the requested record.
     *
     *  @param locationSearchRecordType a location search record
     *         type
     *  @return the location search record
     *  @throws org.osid.NullArgumentException
     *          <code>locationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationSearchRecord getLocationSearchRecord(org.osid.type.Type locationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.records.LocationSearchRecord record : this.records) {
            if (record.implementsRecordType(locationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this location search. 
     *
     *  @param locationSearchRecord location search record
     *  @param locationSearchRecordType location search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLocationSearchRecord(org.osid.mapping.records.LocationSearchRecord locationSearchRecord, 
                                           org.osid.type.Type locationSearchRecordType) {

        addRecordType(locationSearchRecordType);
        this.records.add(locationSearchRecord);        
        return;
    }
}
