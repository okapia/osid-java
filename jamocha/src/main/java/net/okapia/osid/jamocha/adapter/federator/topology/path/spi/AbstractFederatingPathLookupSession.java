//
// AbstractFederatingPathLookupSession.java
//
//     An abstract federating adapter for a PathLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PathLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPathLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.topology.path.PathLookupSession>
    implements org.osid.topology.path.PathLookupSession {

    private boolean parallel = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Constructs a new <code>AbstractFederatingPathLookupSession</code>.
     */

    protected AbstractFederatingPathLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.topology.path.PathLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Path</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            if (session.canLookupPaths()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.useComparativePathView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.usePlenaryPathView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.useFederatedGraphView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.useIsolatedGraphView();
        }

        return;
    }


    /**
     *  Only paths whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectivePathView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.useEffectivePathView();
        }

        return;
    }


    /**
     *  All paths of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectivePathView() {
        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            session.useAnyEffectivePathView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Path</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Path</code> and
     *  retained for compatibility.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            try {
                return (session.getPath(pathId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the paths
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  pathIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.topology.path.path.MutablePathList ret = new net.okapia.osid.jamocha.topology.path.path.MutablePathList();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                ret.addPath(getPath(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusType(pathGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> and include any additional
     *  paths with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByParentGenusType(pathGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByRecordType(pathRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *  
     *  In active mode, paths are returned that are currently
     *  active. In any status mode, active and inactive paths
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Path</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.path.PathList getPathsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>PathList</code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In active mode, paths are returned that are currently
     *  active. In any status mode, active and inactive paths are
     *  returned.
     *
     *  @param pathGenusType a path genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeOnDate(org.osid.type.Type pathGenusType,
                                                                     org.osid.calendaring.DateTime from,
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeOnDate(pathGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForStartingNode(org.osid.id.Id startingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForStartingNode(startingNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to a starting node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForStartingNodeOnDate(startingNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  starting node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code> or
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNode(org.osid.id.Id startingNodeId,
                                                                              org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForStartingNode(startingNodeId, pathGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  starting node <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>pathGenusType</code> <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                                    org.osid.type.Type pathGenusType,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForStartingNodeOnDate(startingNodeId, pathGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.path.PathList getPathsForEndingNode(org.osid.id.Id endingNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForEndingNode(endingNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to a ending node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>endingNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForEndingNodeOnDate(endingNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  ending node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingNodeId</code> or
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNode(org.osid.id.Id endingNodeId,
                                                                            org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForEndingNode(endingNodeId, pathGenusType));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a list of paths of a genus type corresponding to a
     *  ending node <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>endingNodeId</code>,
     *          <code>pathGenusType</code> <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                                  org.osid.type.Type pathGenusType,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForEndingNodeOnDate(endingNodeId, pathGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending node
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>endingNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodes(org.osid.id.Id startingNodeId,
                                                            org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForNodes(startingNodeId, endingNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending node
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible
     *  through this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                  org.osid.id.Id endingNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsForNodesOnDate(startingNodeId, endingNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths of a genus type corresponding to starting
     *  node and ending node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the <code>Id</code> of the starting node
     *  @param  endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>pathGenusType</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodes(org.osid.id.Id startingNodeId,
                                                                       org.osid.id.Id endingNodeId,
                                                                       org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForNodes(startingNodeId, endingNodeId, pathGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of paths of a genus type corresponding to starting
     *  node and ending node <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param startingNodeId the <code>Id</code> of the starting node
     *  @param endingNodeId the <code>Id</code> of the ending node
     *  @param pathGenusType a path genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>PathList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>startingNodeId</code>,
     *          <code>endingNodeId</code>, <code>pathGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                             org.osid.id.Id endingNodeId,
                                                                             org.osid.type.Type pathGenusType,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusTypeForNodesOnDate(startingNodeId, endingNodeId, pathGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given
     *  <code>Nodes</code>.
     *
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  <code> Paths </code> may be omitted from the list.
     *
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException <code>nodeIds</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodes(org.osid.id.IdList nodeIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsAlongNodes(nodeIds));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given
     *  <code>Nodes</code> and and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  <code> Paths </code> may be omitted from the list.
     *
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>nodeIds</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodesOnDate(org.osid.id.IdList nodeIds,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsAlongNodesOnDate(nodeIds, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Paths</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList ret = getPathList();

        for (org.osid.topology.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPaths());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.topology.path.path.FederatingPathList getPathList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.path.path.ParallelPathList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.path.path.CompositePathList());
        }
    }
}
