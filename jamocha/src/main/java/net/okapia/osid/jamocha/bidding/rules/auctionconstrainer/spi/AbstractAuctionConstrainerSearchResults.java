//
// AbstractAuctionConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuctionConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.bidding.rules.AuctionConstrainerSearchResults {

    private org.osid.bidding.rules.AuctionConstrainerList auctionConstrainers;
    private final org.osid.bidding.rules.AuctionConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuctionConstrainerSearchResults.
     *
     *  @param auctionConstrainers the result set
     *  @param auctionConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctionConstrainers</code>
     *          or <code>auctionConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuctionConstrainerSearchResults(org.osid.bidding.rules.AuctionConstrainerList auctionConstrainers,
                                            org.osid.bidding.rules.AuctionConstrainerQueryInspector auctionConstrainerQueryInspector) {
        nullarg(auctionConstrainers, "auction constrainers");
        nullarg(auctionConstrainerQueryInspector, "auction constrainer query inspectpr");

        this.auctionConstrainers = auctionConstrainers;
        this.inspector = auctionConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the auction constrainer list resulting from a search.
     *
     *  @return an auction constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainers() {
        if (this.auctionConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.bidding.rules.AuctionConstrainerList auctionConstrainers = this.auctionConstrainers;
        this.auctionConstrainers = null;
	return (auctionConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.bidding.rules.AuctionConstrainerQueryInspector getAuctionConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  auction constrainer search record <code> Type. </code> This method must
     *  be used to retrieve an auctionConstrainer implementing the requested
     *  record.
     *
     *  @param auctionConstrainerSearchRecordType an auctionConstrainer search 
     *         record type 
     *  @return the auction constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auctionConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerSearchResultsRecord getAuctionConstrainerSearchResultsRecord(org.osid.type.Type auctionConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.bidding.rules.records.AuctionConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auctionConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record auction constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuctionConstrainerRecord(org.osid.bidding.rules.records.AuctionConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "auction constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
