//
// AbstractInstruction.java
//
//     Defines an Instruction builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.instruction.spi;


/**
 *  Defines an <code>Instruction</code> builder.
 */

public abstract class AbstractInstructionBuilder<T extends AbstractInstructionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.rules.check.instruction.InstructionMiter instruction;


    /**
     *  Constructs a new <code>AbstractInstructionBuilder</code>.
     *
     *  @param instruction the instruction to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInstructionBuilder(net.okapia.osid.jamocha.builder.rules.check.instruction.InstructionMiter instruction) {
        super(instruction);
        this.instruction = instruction;
        return;
    }


    /**
     *  Builds the instruction.
     *
     *  @return the new instruction
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.rules.check.Instruction build() {
        (new net.okapia.osid.jamocha.builder.validator.rules.check.instruction.InstructionValidator(getValidations())).validate(this.instruction);
        return (new net.okapia.osid.jamocha.builder.rules.check.instruction.ImmutableInstruction(this.instruction));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the instruction miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.rules.check.instruction.InstructionMiter getMiter() {
        return (this.instruction);
    }


    /**
     *  Sets the agenda.
     *
     *  @param agenda an agenda
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>agenda</code> is <code>null</code>
     */

    public T agenda(org.osid.rules.check.Agenda agenda) {
        getMiter().setAgenda(agenda);
        return (self());
    }


    /**
     *  Sets the check.
     *
     *  @param check a check
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>check</code> is <code>null</code>
     */

    public T check(org.osid.rules.check.Check check) {
        getMiter().setCheck(check);
        return (self());
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    public T message(org.osid.locale.DisplayText message) {
        getMiter().setMessage(message);
        return (self());
    }


    /**
     *  Adds an Instruction record.
     *
     *  @param record an instruction record
     *  @param recordType the type of instruction record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.rules.check.records.InstructionRecord record, org.osid.type.Type recordType) {
        getMiter().addInstructionRecord(record, recordType);
        return (self());
    }
}       


