//
// AbstractBiddingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBiddingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.bidding.batch.BiddingBatchManager,
               org.osid.bidding.batch.BiddingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractBiddingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBiddingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of auctions is available. 
     *
     *  @return <code> true </code> if an auction bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of bids is available. 
     *
     *  @return <code> true </code> if a bid bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of auction houses is available. 
     *
     *  @return <code> true </code> if a auction house bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  administration service. 
     *
     *  @return an <code> AuctionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionBatchAdminSession getAuctionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchManager.getAuctionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionBatchAdminSession getAuctionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchProxyManager.getAuctionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionBatchAdminSession getAuctionBatchAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchManager.getAuctionBatchAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionBatchAdminSession getAuctionBatchAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchProxyManager.getAuctionBatchAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bid 
     *  administration service. 
     *
     *  @return a <code> BidBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BidBatchAdminSession getBidBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchManager.getBidBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bid 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BidBatchAdminSession getBidBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchProxyManager.getBidBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bid 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidBatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BidBatchAdminSession getBidBatchAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchManager.getBidBatchAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bid 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidBatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BidBatchAdminSession getBidBatchAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchProxyManager.getBidBatchAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  house administration service. 
     *
     *  @return a <code> AuctionHouseBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionHouseBatchAdminSession getAuctionHouseBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchManager.getAuctionHouseBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk auction 
     *  house administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.AuctionHouseBatchAdminSession getAuctionHouseBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.batch.BiddingBatchProxyManager.getAuctionHouseBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
