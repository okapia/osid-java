//
// AbstractRouteSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRouteSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.route.RouteSearchResults {

    private org.osid.mapping.route.RouteList routes;
    private final org.osid.mapping.route.RouteQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.route.records.RouteSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRouteSearchResults.
     *
     *  @param routes the result set
     *  @param routeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>routes</code>
     *          or <code>routeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRouteSearchResults(org.osid.mapping.route.RouteList routes,
                                            org.osid.mapping.route.RouteQueryInspector routeQueryInspector) {
        nullarg(routes, "routes");
        nullarg(routeQueryInspector, "route query inspectpr");

        this.routes = routes;
        this.inspector = routeQueryInspector;

        return;
    }


    /**
     *  Gets the route list resulting from a search.
     *
     *  @return a route list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutes() {
        if (this.routes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.route.RouteList routes = this.routes;
        this.routes = null;
	return (routes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.route.RouteQueryInspector getRouteQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  route search record <code> Type. </code> This method must
     *  be used to retrieve a route implementing the requested
     *  record.
     *
     *  @param routeSearchRecordType a route search 
     *         record type 
     *  @return the route search
     *  @throws org.osid.NullArgumentException
     *          <code>routeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(routeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSearchResultsRecord getRouteSearchResultsRecord(org.osid.type.Type routeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.route.records.RouteSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(routeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(routeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record route search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRouteRecord(org.osid.mapping.route.records.RouteSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "route record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
