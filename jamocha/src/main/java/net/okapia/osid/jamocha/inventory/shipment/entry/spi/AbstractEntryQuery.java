//
// AbstractEntryQuery.java
//
//     A template for making an Entry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for entries.
 */

public abstract class AbstractEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.inventory.shipment.EntryQuery {

    private final java.util.Collection<org.osid.inventory.shipment.records.EntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the stock <code> Id </code> for this query. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the stock <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Clears the stock terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        return;
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        return;
    }


    /**
     *  Clears the model <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a model. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches entries that have any model set. 
     *
     *  @param  match <code> true </code> to match entries with any model, 
     *          <code> false </code> to match entries with no model 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches entries that have any item set. 
     *
     *  @param  match <code> true </code> to match entries with any item, 
     *          <code> false </code> to match entries with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Matches entries with a quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchQuantity(java.math.BigDecimal low, 
                              java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Matches entries that have any quantity set. 
     *
     *  @param  match <code> true </code> to match entries with any quantity, 
     *          <code> false </code> to match entries with no quantity 
     */

    @OSID @Override
    public void matchAnyQuantity(boolean match) {
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        return;
    }


    /**
     *  Matches the a unit type. 
     *
     *  @param  type a unit tytpe 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> type </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchUnitType(org.osid.type.Type type, boolean match) {
        return;
    }


    /**
     *  Matches entries that have any unit type set. 
     *
     *  @param  match <code> true </code> to match entries with any unit type, 
     *          <code> false </code> to match entries with no unit type 
     */

    @OSID @Override
    public void matchAnyUnitType(boolean match) {
        return;
    }


    /**
     *  Clears the unit type terms. 
     */

    @OSID @Override
    public void clearUnitTypeTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match entries 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an entry implementing the requested record.
     *
     *  @param entryRecordType an entry record type
     *  @return the entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.EntryQueryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry query. 
     *
     *  @param entryQueryRecord entry query record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEntryQueryRecord(org.osid.inventory.shipment.records.EntryQueryRecord entryQueryRecord, 
                                          org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);
        nullarg(entryQueryRecord, "entry query record");
        this.records.add(entryQueryRecord);        
        return;
    }
}
