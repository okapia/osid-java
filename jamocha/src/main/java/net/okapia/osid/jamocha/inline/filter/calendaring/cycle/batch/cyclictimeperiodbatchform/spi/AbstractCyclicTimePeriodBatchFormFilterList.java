//
// AbstractCyclicTimePeriodBatchFormList
//
//     Implements a filter for a CyclicTimePeriodBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.cycle.batch.cyclictimeperiodbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CyclicTimePeriodBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCyclicTimePeriodBatchFormList
 *  to improve performance.
 */

public abstract class AbstractCyclicTimePeriodBatchFormFilterList
    extends net.okapia.osid.jamocha.calendaring.cycle.batch.cyclictimeperiodbatchform.spi.AbstractCyclicTimePeriodBatchFormList
    implements org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchFormList,
               net.okapia.osid.jamocha.inline.filter.calendaring.cycle.batch.cyclictimeperiodbatchform.CyclicTimePeriodBatchFormFilter {

    private org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchForm cyclicTimePeriodBatchForm;
    private final org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCyclicTimePeriodBatchFormFilterList</code>.
     *
     *  @param cyclicTimePeriodBatchFormList a <code>CyclicTimePeriodBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodBatchFormList</code> is <code>null</code>
     */

    protected AbstractCyclicTimePeriodBatchFormFilterList(org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchFormList cyclicTimePeriodBatchFormList) {
        nullarg(cyclicTimePeriodBatchFormList, "cyclic time period batch form list");
        this.list = cyclicTimePeriodBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.cyclicTimePeriodBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CyclicTimePeriodBatchForm </code> in this list. 
     *
     *  @return the next <code> CyclicTimePeriodBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CyclicTimePeriodBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchForm getNextCyclicTimePeriodBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchForm cyclicTimePeriodBatchForm = this.cyclicTimePeriodBatchForm;
            this.cyclicTimePeriodBatchForm = null;
            return (cyclicTimePeriodBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in cyclic time period batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicTimePeriodBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CyclicTimePeriodBatchForms.
     *
     *  @param cyclicTimePeriodBatchForm the cyclic time period batch form to filter
     *  @return <code>true</code> if the cyclic time period batch form passes the filter,
     *          <code>false</code> if the cyclic time period batch form should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchForm cyclicTimePeriodBatchForm);


    protected void prime() {
        if (this.cyclicTimePeriodBatchForm != null) {
            return;
        }

        org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchForm cyclicTimePeriodBatchForm = null;

        while (this.list.hasNext()) {
            try {
                cyclicTimePeriodBatchForm = this.list.getNextCyclicTimePeriodBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(cyclicTimePeriodBatchForm)) {
                this.cyclicTimePeriodBatchForm = cyclicTimePeriodBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
