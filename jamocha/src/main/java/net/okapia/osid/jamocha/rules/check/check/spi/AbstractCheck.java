//
// AbstractCheck.java
//
//     Defines a Check.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Check</code>.
 */

public abstract class AbstractCheck
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.rules.check.Check {

    private boolean failCheck = false;
    private boolean timeCheck = false;
    private org.osid.calendaring.DateTime timeCheckStartDate;
    private org.osid.calendaring.DateTime timeCheckEndDate;
    private org.osid.calendaring.Event timeCheckEvent;
    private org.osid.calendaring.cycle.CyclicEvent timeCheckCyclicEvent;
    private org.osid.hold.Block holdCheckBlock;
    private org.osid.inquiry.Audit inquiryCheckAudit;
    private org.osid.rules.check.Agenda processCheckAgenda;
    
    private final java.util.Collection<org.osid.rules.check.records.CheckRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this check is a placeholder check that always fails
     *  when evaluated. A failure check can be used to block certain
     *  conditions specified in an <code> Instruction. </code> If
     *  <code> isFailCheck() </code> is <code> true, </code> then
     *  <code> isTimeCheckByDate(), </code> <code>
     *  isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  isInquiryCheck(), </code> and <code> isProcessCheck() </code>
     *  must be <code> false. </code>
     *
     *  @return <code> true </code> if this is a fail check, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isFailCheck() {
        return (this.failCheck);
    }


    /**
     *  Sets the fail check.
     *
     *  @param failCheck <code> true </code> if this is a fail check,
     *          <code> false </code> otherwise
     */

    protected void setFailCheck(boolean failCheck) {
        if (failCheck) {
            clearChecks();
        }

        this.failCheck = failCheck;
        return;
    }


    /**
     *  Tests if this check is for a time check specified by a
     *  date. If <code> isTimeCheckByDate() </code> is <code> true,
     *  </code> then <code> isFailCheck(), </code> <code>
     *  isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  </code> <code> isInquiryCheck(), </code> and <code>
     *  isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time
     *          specified by date, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isTimeCheckByDate() {
        return (this.timeCheck);
    }


    /**
     *  Gets the time check start date. The check passes if the current
     *  time is within the time interval inclusive. If the start time
     *  of the interval is undefined, then the check for the start
     *  time always passes. If the end time of the interval is
     *  undefined, then the check for the deadline always passes.
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCheckStartDate() {
        if (!isTimeCheckByDate()) {
            throw new org.osid.IllegalStateException("isTimeCheckByDate() is false");
        }

        return (this.timeCheckStartDate);
    }


    /**
     *  Sets the start date for a time check.
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setTimeCheckStartDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "time check start date");
        clearChecks();

        this.timeCheckStartDate = date;
        this.timeCheck = true;

        return;
    }


    /**
     *  Gets the time check end date. The check passes if the current
     *  time is within the time interval inclusive. If the end time
     *  of the interval is undefined, then the check for the end
     *  time always passes. If the end time of the interval is
     *  undefined, then the check for the deadline always passes.
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimeCheckEndDate() {
        if (!isTimeCheckByDate()) {
            throw new org.osid.IllegalStateException("isTimeCheckByDate() is false");
        }

        return (this.timeCheckEndDate);
    }


    /**
     *  Sets the end date for a time check.
     *
     *  @param date a time check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setTimeCheckEndDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "time check end date");
        clearChecks();

        this.timeCheckEndDate = date;
        this.timeCheck = true;

        return;
    }


    /**
     *  Tests if this check is for a time check specified by an
     *  event. The starting and ending times of the event are used as
     *  a time interval to perform the time check. If <code>
     *  isTimeCheckByEvent() </code> is <code> true, </code> then
     *  <code> isFailCheck(), </code> <code> isTimeCheckByDate(),
     *  </code> <code> isTimeCheckByCyclicEvent(), </code> <code>
     *  isHoldCheck(), </code> <code> isInquiryCheck(), </code> and
     *  <code> isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time
     *          speciifed by event, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isTimeCheckByEvent() {
        return (this.timeCheckEvent != null);
    }


    /**
     *  Gets the time check event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code>
     *          isTimeCheckByEvent() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getTimeCheckEventId() {
        if (!isTimeCheckByEvent()) {
            throw new org.osid.IllegalStateException("isTimeCheckByEvent() is false");
        }

        return (this.timeCheckEvent.getId());
    }


    /**
     *  Gets the time check event. 
     *
     *  @return the event 
     *  @throws org.osid.IllegalStateException <code> isTimeCheckByEvent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getTimeCheckEvent()
        throws org.osid.OperationFailedException {

        if (!isTimeCheckByEvent()) {
            throw new org.osid.IllegalStateException("isTimeCheckByEvent() is false");
        }

        return (this.timeCheckEvent);
    }


    /**
     *  Sets the event for a time check.
     *
     *  @param event an event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void setTimeCheckEvent(org.osid.calendaring.Event event) {
        nullarg(event, "event");
        clearChecks();
        this.timeCheckEvent = event;
        return;
    }


    /**
     *  Tests if this check is for a time check specified by a cyclic
     *  event.  The time check is performed using the starting and
     *  ending dates of the derived event. If the event is a rceurring
     *  event, the time must be within the starting and ending dates
     *  of at least one of the events in the series. If <code>
     *  isTimeCheckByCyclicEvent() </code> is <code> true, </code>
     *  then <code> isFailCheck(), </code> <code> isTimeCheckByDate(),
     *  </code> <code> isTimeCheckByEvent(), </code> <code>
     *  isHoldCheck(), isInquiryCheck(), </code> and <code>
     *  isProcessCheck() </code> must be <code> false. </code>
     *
     *  @return <code> true </code> if this check is for a time check
     *          specified by cyclic event, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean isTimeCheckByCyclicEvent() {
        return (this.timeCheckCyclicEvent != null);
    }


    /**
     *  Gets the time check cyclic event <code> Id. </code> 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          isTimeCheckByCyclicEvent() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimeCheckCyclicEventId() {
        if (!isTimeCheckByCyclicEvent()) {
            throw new org.osid.IllegalStateException("isTimeCheckByCyclicEvent() is false");
        }

        return (this.timeCheckCyclicEvent.getId());
    }


    /**
     *  Gets the time check cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> 
     *          isTimeCheckByCyclicEvent() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getTimeCheckCyclicEvent()
        throws org.osid.OperationFailedException {

        if (!isTimeCheckByCyclicEvent()) {
            throw new org.osid.IllegalStateException("isTimeCheckByCyclicEvent() is false");
        }

        return (this.timeCheckCyclicEvent);
    }


    /**
     *  Sets the cyclic event for a time check.
     *
     *  @param cyclicEvent a time check cyclic event
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEvent</code> is <code>null</code>
     */

    protected void setTimeCheckCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        nullarg(cyclicEvent, "cyclic event");
        clearChecks();
        this.timeCheckCyclicEvent = cyclicEvent;
        return;
    }


    /**
     *  Tests if this check is for a hold service block. If a block
     *  exists for the agent being checked, the check fails. If <code>
     *  isHoldCheck() </code> is <code> true, </code> then <code>
     *  isFailCheck(), </code> <code> isTimeCheckByDate(), </code>
     *  <code> isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isInquiryCheck(),
     *  </code> and <code> isProcessCheck() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this check is for a block,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isHoldCheck() {
        return (this.holdCheckBlock != null);
    }


    /**
     *  Gets the <code> Block Id </code> for this check, 
     *
     *  @return the <code> Block Id </code> 
     *  @throws org.osid.IllegalStateException <code> isHoldCheck() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getHoldCheckBlockId() {
        if (!isHoldCheck()) {
            throw new org.osid.IllegalStateException("isHoldCheck() is false");
        }

        return (this.holdCheckBlock.getId());
    }


    /**
     *  Gets the <code> Block </code> for this check. 
     *
     *  @return the <code> Block </code> 
     *  @throws org.osid.IllegalStateException <code> isHoldCheck() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.Block getHoldCheckBlock()
        throws org.osid.OperationFailedException {

        if (!isHoldCheck()) {
            throw new org.osid.IllegalStateException("isHoldCheck() is false");
        }

        return (this.holdCheckBlock);
    }


    /**
     *  Sets the block for a hold check.
     *
     *  @param block a block
     *  @throws org.osid.NullArgumentException <code>block</code> is
     *          <code>null</code>
     */

    protected void setHoldCheckBlock(org.osid.hold.Block block) {
        nullarg(block, "block");
        clearChecks();
        this.holdCheckBlock = block;
        return;
    }


    /**
     *  Tests if this check is for a hold service block. If a block
     *  exists for the agent being checked, the check fails. If <code>
     *  isInquiryCheck() </code> is <code> true, </code> then <code>
     *  isFailCheck(), </code> <code> isTimeCheckByDate(), </code>
     *  <code> isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  </code> and <code> isProcessCheck() </code> must be <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this check is for a block,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isInquiryCheck() {
        return (this.inquiryCheckAudit != null);
    }


    /**
     *  Gets the <code> Audit Id </code> for this check, 
     *
     *  @return the <code> Block Id </code> 
     *  @throws org.osid.IllegalStateException <code> isInquiryCheck()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getInquiryCheckAuditId() {
        if (!isInquiryCheck()) {
            throw new org.osid.IllegalStateException("isInquiryCheck() is false");
        }

        return (this.inquiryCheckAudit.getId());
    }


    /**
     *  Gets the <code> Audit </code> for this check. 
     *
     *  @return the <code> Audit </code> 
     *  @throws org.osid.IllegalStateException <code> isInquiryCheck()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.Audit getInquiryCheckAudit()
        throws org.osid.OperationFailedException {

        if (!isInquiryCheck()) {
            throw new org.osid.IllegalStateException("isInquiryCheck() is false");
        }

        return (this.inquiryCheckAudit);
    }


    /**
     *  Sets the audit for an inquiry check
     *
     *  @param audit an audit
     *  @throws org.osid.NullArgumentException <code>audit</code> is
     *          <code>null</code>
     */

    protected void setInquiryCheckAudit(org.osid.inquiry.Audit audit) {
        nullarg(audit, "audit");
        clearChecks();
        this.inquiryCheckAudit = audit;
        return;
    }


    /**
     *  Tests if this check is for a another agenda. The agenda for
     *  the specified agenda is retrieved and processed before
     *  continuing with the checks in this agenda. If <code>
     *  isProcessCheck() </code> is <code> true, </code> then <code>
     *  isFailCheck(), </code> <code> isTimeCheckByDate(), </code>
     *  <code> isTimeCheckByEvent(), </code> <code>
     *  isTimeCheckByCyclicEvent(), </code> <code> isHoldCheck(),
     *  </code> and <code> isInquiryCheck() </code> must be <code>
     *  false.  </code>
     *
     *  @return <code> true </code> if this check is for another
     *          agenda, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isProcessCheck() {
        return (this.processCheckAgenda != null);
    }


    /**
     *  Gets the <code> Agenda Id </code> for this check. 
     *
     *  @return the <code> Agenda Id </code> 
     *  @throws org.osid.IllegalStateException <code> isProcessCheck() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessCheckAgendaId() {
        if (!isProcessCheck()) {
            throw new org.osid.IllegalStateException("isProcessCheck() is false");
        }

        return (this.processCheckAgenda.getId());
    }


    /**
     *  Gets the <code> Agenda </code> for this check. 
     *
     *  @return the <code> Agenda </code> 
     *  @throws org.osid.IllegalStateException <code> isProcessCheck()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getProcessCheckAgenda()
        throws org.osid.OperationFailedException {

        if (!isProcessCheck()) {
            throw new org.osid.IllegalStateException("isProcessCheck() is false");
        }

        return (this.processCheckAgenda);
    }


    /**
     *  Sets the agenda for a process check.
     *
     *  @param agenda an agenda
     *  @throws org.osid.NullArgumentException <code>agenda</code> is
     *          <code>null</code>
     */

    protected void setProcessCheckAgenda(org.osid.rules.check.Agenda agenda) {
        nullarg(agenda, "agenda");
        this.processCheckAgenda = agenda;
        return;
    }


    /**
     *  Tests if this check supports the given record
     *  <code>Type</code>.
     *
     *  @param  checkRecordType a check record type 
     *  @return <code>true</code> if the checkRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checkRecordType) {
        for (org.osid.rules.check.records.CheckRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Check</code>
     *  record <code>Type</code>.
     *
     *  @param  checkRecordType the check record type 
     *  @return the check record 
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckRecord getCheckRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Adds a record to this check. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checkRecord the check record
     *  @param checkRecordType check record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecord</code> or
     *          <code>checkRecordTypecheck</code> is
     *          <code>null</code>
     */
            
    protected void addCheckRecord(org.osid.rules.check.records.CheckRecord checkRecord, 
                                  org.osid.type.Type checkRecordType) {

        nullarg(checkRecord, "check record");
        addRecordType(checkRecordType);
        this.records.add(checkRecord);
        
        return;
    }


    private void clearChecks() {
        this.failCheck            = false;
        this.timeCheck            = false;
        this.timeCheckEvent       = null;
        this.timeCheckCyclicEvent = null;
        this.holdCheckBlock       = null;
        this.inquiryCheckAudit    = null;
        this.processCheckAgenda   = null;

        return;
    }
}
