//
// AbstractMapSequenceRuleLookupSession
//
//    A simple framework for providing a SequenceRule lookup service
//    backed by a fixed collection of sequence rules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SequenceRule lookup service backed by a
 *  fixed collection of sequence rules. The sequence rules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SequenceRules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.assessment.authoring.spi.AbstractSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.authoring.SequenceRule> sequenceRules = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.authoring.SequenceRule>());


    /**
     *  Makes a <code>SequenceRule</code> available in this session.
     *
     *  @param  sequenceRule a sequence rule
     *  @throws org.osid.NullArgumentException <code>sequenceRule<code>
     *          is <code>null</code>
     */

    protected void putSequenceRule(org.osid.assessment.authoring.SequenceRule sequenceRule) {
        this.sequenceRules.put(sequenceRule.getId(), sequenceRule);
        return;
    }


    /**
     *  Makes an array of sequence rules available in this session.
     *
     *  @param  sequenceRules an array of sequence rules
     *  @throws org.osid.NullArgumentException <code>sequenceRules<code>
     *          is <code>null</code>
     */

    protected void putSequenceRules(org.osid.assessment.authoring.SequenceRule[] sequenceRules) {
        putSequenceRules(java.util.Arrays.asList(sequenceRules));
        return;
    }


    /**
     *  Makes a collection of sequence rules available in this session.
     *
     *  @param  sequenceRules a collection of sequence rules
     *  @throws org.osid.NullArgumentException <code>sequenceRules<code>
     *          is <code>null</code>
     */

    protected void putSequenceRules(java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules) {
        for (org.osid.assessment.authoring.SequenceRule sequenceRule : sequenceRules) {
            this.sequenceRules.put(sequenceRule.getId(), sequenceRule);
        }

        return;
    }


    /**
     *  Removes a SequenceRule from this session.
     *
     *  @param  sequenceRuleId the <code>Id</code> of the sequence rule
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId<code> is
     *          <code>null</code>
     */

    protected void removeSequenceRule(org.osid.id.Id sequenceRuleId) {
        this.sequenceRules.remove(sequenceRuleId);
        return;
    }


    /**
     *  Gets the <code>SequenceRule</code> specified by its <code>Id</code>.
     *
     *  @param  sequenceRuleId <code>Id</code> of the <code>SequenceRule</code>
     *  @return the sequenceRule
     *  @throws org.osid.NotFoundException <code>sequenceRuleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRule getSequenceRule(org.osid.id.Id sequenceRuleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.authoring.SequenceRule sequenceRule = this.sequenceRules.get(sequenceRuleId);
        if (sequenceRule == null) {
            throw new org.osid.NotFoundException("sequenceRule not found: " + sequenceRuleId);
        }

        return (sequenceRule);
    }


    /**
     *  Gets all <code>SequenceRules</code>. In plenary mode, the returned
     *  list contains all known sequenceRules or an error
     *  results. Otherwise, the returned list may contain only those
     *  sequenceRules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SequenceRules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequencerule.ArraySequenceRuleList(this.sequenceRules.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.sequenceRules.clear();
        super.close();
        return;
    }
}
