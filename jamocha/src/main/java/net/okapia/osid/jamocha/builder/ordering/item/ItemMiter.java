//
// ItemMiter.java
//
//     Defines an Item miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.item;


/**
 *  Defines an <code>Item</code> miter for use with the builders.
 */

public interface ItemMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.ordering.Item {


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @throws org.osid.NullArgumentException <code>order</code> is
     *          <code>null</code>
     */

    public void setOrder(org.osid.ordering.Order order);


    /**
     *  Sets the derived flag.
     *
     *  @param derived <code> true </code> if this item is derived,
     *         <code> false </code> otherwise
     */

    public void setDerived(boolean derived);


    /**
     *  Sets the product.
     *
     *  @param product a product
     *  @throws org.osid.NullArgumentException <code>product</code> is
     *          <code>null</code>
     */

    public void setProduct(org.osid.ordering.Product product);


    /**
     *  Adds an unit price.
     *
     *  @param price an unit price
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public void addUnitPrice(org.osid.ordering.Price price);


    /**
     *  Sets all the unit prices.
     *
     *  @param prices a collection of unit prices
     *  @throws org.osid.NullArgumentException <code>prices</code> is
     *          <code>null</code>
     */

    public void setUnitPrices(java.util.Collection<org.osid.ordering.Price> prices);


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public void setQuantity(long quantity);


    /**
     *  Adds a cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public void addCost(org.osid.ordering.Cost cost);


    /**
     *  Sets all the costs.
     *
     *  @param costs a collection of costs
     *  @throws org.osid.NullArgumentException <code>costs</code> is
     *          <code>null</code>
     */

    public void setCosts(java.util.Collection<org.osid.ordering.Cost> costs);


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addItemRecord(org.osid.ordering.records.ItemRecord record, org.osid.type.Type recordType);
}       


