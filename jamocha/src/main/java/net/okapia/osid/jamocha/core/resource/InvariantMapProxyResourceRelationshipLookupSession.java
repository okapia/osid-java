//
// InvariantMapProxyResourceRelationshipLookupSession
//
//    Implements a ResourceRelationship lookup service backed by a fixed
//    collection of resourceRelationships. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a ResourceRelationship lookup service backed by a fixed
 *  collection of resource relationships. The resource relationships are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapResourceRelationshipLookupSession
    implements org.osid.resource.ResourceRelationshipLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyResourceRelationshipLookupSession} with no
     *  resource relationships.
     *
     *  @param bin the bin
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.proxy.Proxy proxy) {
        setBin(bin);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyResourceRelationshipLookupSession} with a single
     *  resource relationship.
     *
     *  @param bin the bin
     *  @param resourceRelationship a single resource relationship
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resourceRelationship} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.ResourceRelationship resourceRelationship, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putResourceRelationship(resourceRelationship);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyResourceRelationshipLookupSession} using
     *  an array of resource relationships.
     *
     *  @param bin the bin
     *  @param resourceRelationships an array of resource relationships
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resourceRelationships} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.ResourceRelationship[] resourceRelationships, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putResourceRelationships(resourceRelationships);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyResourceRelationshipLookupSession} using a
     *  collection of resource relationships.
     *
     *  @param bin the bin
     *  @param resourceRelationships a collection of resource relationships
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resourceRelationships} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResourceRelationshipLookupSession(org.osid.resource.Bin bin,
                                                  java.util.Collection<? extends org.osid.resource.ResourceRelationship> resourceRelationships,
                                                  org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putResourceRelationships(resourceRelationships);
        return;
    }
}
