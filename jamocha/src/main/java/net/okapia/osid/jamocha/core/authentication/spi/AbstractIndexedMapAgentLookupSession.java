//
// AbstractIndexedMapAgentLookupSession.java
//
//    A simple framework for providing an Agent lookup service
//    backed by a fixed collection of agents with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Agent lookup service backed by a
 *  fixed collection of agents. The agents are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some agents may be compatible
 *  with more types than are indicated through these agent
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAgentLookupSession
    extends AbstractMapAgentLookupSession
    implements org.osid.authentication.AgentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authentication.Agent> agentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.Agent>());
    private final MultiMap<org.osid.type.Type, org.osid.authentication.Agent> agentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authentication.Agent>());


    /**
     *  Makes an <code>Agent</code> available in this session.
     *
     *  @param  agent an agent
     *  @throws org.osid.NullArgumentException <code>agent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAgent(org.osid.authentication.Agent agent) {
        super.putAgent(agent);

        this.agentsByGenus.put(agent.getGenusType(), agent);
        
        try (org.osid.type.TypeList types = agent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agentsByRecord.put(types.getNextType(), agent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an agent from this session.
     *
     *  @param agentId the <code>Id</code> of the agent
     *  @throws org.osid.NullArgumentException <code>agentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAgent(org.osid.id.Id agentId) {
        org.osid.authentication.Agent agent;
        try {
            agent = getAgent(agentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.agentsByGenus.remove(agent.getGenusType());

        try (org.osid.type.TypeList types = agent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.agentsByRecord.remove(types.getNextType(), agent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAgent(agentId);
        return;
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  agent genus <code>Type</code> which does not include
     *  agents of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known agents or an error results. Otherwise,
     *  the returned list may contain only those agents that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agent.ArrayAgentList(this.agentsByGenus.get(agentGenusType)));
    }


    /**
     *  Gets an <code>AgentList</code> containing the given
     *  agent record <code>Type</code>. In plenary mode, the
     *  returned list contains all known agents or an error
     *  results. Otherwise, the returned list may contain only those
     *  agents that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  agentRecordType an agent record type 
     *  @return the returned <code>agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByRecordType(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agent.ArrayAgentList(this.agentsByRecord.get(agentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agentsByGenus.clear();
        this.agentsByRecord.clear();

        super.close();

        return;
    }
}
