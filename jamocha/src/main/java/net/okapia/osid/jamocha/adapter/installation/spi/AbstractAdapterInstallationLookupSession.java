//
// AbstractAdapterInstallationLookupSession.java
//
//    An Installation lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Installation lookup session adapter.
 */

public abstract class AbstractAdapterInstallationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.installation.InstallationLookupSession {

    private final org.osid.installation.InstallationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInstallationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInstallationLookupSession(org.osid.installation.InstallationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Site/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Site Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.session.getSiteId());
    }


    /**
     *  Gets the {@code Site} associated with this session.
     *
     *  @return the {@code Site} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSite());
    }


    /**
     *  Tests if this user can perform {@code Installation} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInstallations() {
        return (this.session.canLookupInstallations());
    }


    /**
     *  A complete view of the {@code Installation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInstallationView() {
        this.session.useComparativeInstallationView();
        return;
    }


    /**
     *  A complete view of the {@code Installation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInstallationView() {
        this.session.usePlenaryInstallationView();
        return;
    }


    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same installation.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        this.session.useNormalizedVersionView();
        return;
    }


    /**
     *  All versions of the same installation are returned. 
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        this.session.useDenormalizedVersionView();
        return;
    }


    /**
     *  Gets the {@code Installation} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Installation} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Installation} and
     *  retained for compatibility.
     *
     *  @param installationId {@code Id} of the {@code Installation}
     *  @return the installation
     *  @throws org.osid.NotFoundException {@code installationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code installationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Installation getInstallation(org.osid.id.Id installationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallation(installationId));
    }


    /**
     *  Gets an {@code InstallationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  installations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Installations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  installationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Installation} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code installationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByIds(org.osid.id.IdList installationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallationsByIds(installationIds));
    }


    /**
     *  Gets an {@code InstallationList} corresponding to the given
     *  installation genus {@code Type} which does not include
     *  installations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned {@code Installation} list
     *  @throws org.osid.NullArgumentException
     *          {@code installationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallationsByGenusType(installationGenusType));
    }


    /**
     *  Gets an {@code InstallationList} corresponding to the given
     *  installation genus {@code Type} and include any additional
     *  installations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned {@code Installation} list
     *  @throws org.osid.NullArgumentException
     *          {@code installationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByParentGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallationsByParentGenusType(installationGenusType));
    }


    /**
     *  Gets an {@code InstallationList} containing the given
     *  installation record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  installationRecordType an installation record type 
     *  @return the returned {@code Installation} list
     *  @throws org.osid.NullArgumentException
     *          {@code installationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByRecordType(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallationsByRecordType(installationRecordType));
    }


    /**
     *  Gets an {@code InstallationList} corresponding to the
     *  given {@code Package.} In plenary mode, the returned
     *  list contains all of the installations for the specified
     *  package, in the order of the list, including duplicates, or an
     *  error results if an {@code Id} in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible {@code
     *  Installations} may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  @param  packageId {@code Id} of a {@code Package} 
     *  @return the returned {@code Installation} list 
     *  @throws org.osid.NullArgumentException {@code packageId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByPackage(org.osid.id.Id packageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallationsByPackage(packageId));
    }


    /**
     *  Gets all {@code Installations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  installations or an error results. Otherwise, the returned list
     *  may contain only those installations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Installations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInstallations());
    }
}
