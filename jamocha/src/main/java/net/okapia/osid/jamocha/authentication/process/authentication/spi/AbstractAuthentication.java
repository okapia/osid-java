//
// AbstractAuthentication.java
//
//     Defines an Authentication.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.process.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Authentication</code>.
 */

public abstract class AbstractAuthentication
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.authentication.process.Authentication {

    private org.osid.authentication.Agent agent;
    private boolean valid = false;
    private java.util.Date expiration;

    private final java.util.Map<org.osid.type.Type, Object> credentials = new java.util.HashMap<>();
    private final java.util.Collection<org.osid.authentication.process.records.AuthenticationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> identified in 
     *  this authentication credential. 
     *
     *  @return the <code> Agent Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> identified in this authentication 
     *  credential. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {
        
        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent the authenticated agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests whether or not the credential represented by this <code>
     *  Authentication </code> is currently valid. A credential may be
     *  invalid because it has been destroyed, expired, or is somehow
     *  no longer able to be used.
     *
     *  @return <code> true </code> if this authentication credential is 
     *          valid, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isValid() {
        return (this.valid);
    }


    /**
     *  Sets the valid flag.
     *
     *  @param valid <code> true </code> if this authentication
     *          credential is valid, <code> false </code> otherwise
     */

    protected void setValid(boolean valid) {
        this.valid = valid;
        return;
    }


    /**
     *  Tests if this authentication has an expiration. 
     *
     *  @return <code> true </code> if this authentication has an expiration, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasExpiration() {
        if (this.expiration == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the expiration date associated with this authentication 
     *  credential. Consumers should check for the existence of a an 
     *  expiration mechanism via <code> hasExpiration(). </code> 
     *
     *  @return the expiration date of this authentication credential 
     *  @throws org.osid.IllegalStateException <code> hasExpiration() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.util.Date getExpiration() {
        if (!hasExpiration()) {
            throw new org.osid.IllegalStateException("hasExpiration() is false");
        }

        return (this.expiration);
    }


    /**
     *  Sets the expiration.
     *
     *  @param date the authenticated expiration
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setExpiration(java.util.Date date) {
        nullarg(date, "expiration date");
        this.expiration = date;
        return;
    }


    /**
     *  Tests if this authentication has a credential for export. 
     *
     *  @return <code> true </code> if this authentication has a credential, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCredential() {
        if (this.credentials.size() == 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the credential represented by the given <code> Type
     *  </code> for transport to a remote service.
     *
     *  @param credentialType the credential format <code> Type
     *         </code>
     *  @return the credential 
     *  @throws org.osid.IllegalStateException <code> hasCredential()
     *          </code> is <code> false </code>
     *  @throws org.osid.NullArgumentException <code> credentialType
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException the given <code>
     *          credentialType </code> is not supported
     */

    @OSID @Override
    public java.lang.Object getCredential(org.osid.type.Type credentialType) {
        if (!hasCredential()) {
            throw new org.osid.IllegalStateException("hasCredential() is false");
        }
        
        java.lang.Object credential = this.credentials.get(credentialType);
        if (credential == null) {
            throw new org.osid.UnsupportedException(credentialType + " not found");
        }

        return (credential);
    }


    /**
     *  Adds a credential for the given type.
     *
     *  @param credentialType the type of credential
     *  @param credential the authenticated credential
     *  @throws org.osid.NullArgumentException
     *          <code>credentialType</code> or <code>credential</code>
     *          is <code>null</code>
     */

    protected void addCredential(org.osid.type.Type credentialType, java.lang.Object credential) {
        nullarg(credentialType, "credential type");
        nullarg(credential, "credential");

        this.credentials.put(credentialType, credential);
        return;
    }


    /**
     *  Tests if this authentication supports the given record
     *  <code>Type</code>.
     *
     *  @param  authenticationRecordType an authentication record type 
     *  @return <code>true</code> if the authenticationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>authenticationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type authenticationRecordType) {
        for (org.osid.authentication.process.records.AuthenticationRecord record : this.records) {
            if (record.implementsRecordType(authenticationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Authentication</code>
     *  record <code>Type</code>.
     *
     *  @param  authenticationRecordType the authentication record type 
     *  @return the authentication record 
     *  @throws org.osid.NullArgumentException
     *          <code>authenticationRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authenticationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.process.records.AuthenticationRecord getAuthenticationRecord(org.osid.type.Type authenticationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.process.records.AuthenticationRecord record : this.records) {
            if (record.implementsRecordType(authenticationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authenticationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authentication. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param authenticationRecord the authentication record
     *  @param authenticationRecordType authentication record type
     *  @throws org.osid.NullArgumentException
     *          <code>authenticationRecord</code> or
     *          <code>authenticationRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAuthenticationRecord(org.osid.authentication.process.records.AuthenticationRecord authenticationRecord, 
                                  org.osid.type.Type authenticationRecordType) {

        nullarg(authenticationRecord, "authentication record");
        addRecordType(authenticationRecordType);
        this.records.add(authenticationRecord);
        
        return;
    }
}
