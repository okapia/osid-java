//
// AbstractImmutableIdentifiable
//
//     An immutable wrapper for an Id-based object.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an Id based object.
 */

public abstract class AbstractImmutableIdentifiable
    extends AbstractImmutable
    implements org.osid.Identifiable {

    private final org.osid.Identifiable identifiable;


    /**
     *  Constructs a new <code>AbstractImmutableIdentifiable</code>.
     *
     *  @param identifiable
     *  @throws org.osid.NullArgumentException <code>identifiable</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableIdentifiable(org.osid.Identifiable identifiable) {
        super(identifiable);
        this.identifiable = identifiable;
        return;
    }


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @return the <code> Id </code> 
     *  @throws org.osid.IllegalStateException the Id has not been set
     */
    
    @OSID @Override
    public org.osid.id.Id getId() {
        return (this.identifiable.getId());
    }


    /**
     *  Tests to see if the last method invoked retrieved up-to-date
     *  data.  Simple retrieval methods do not specify errors as,
     *  generally, the data is retrieved once at the time this object
     *  is instantiated. Some implementations may provide real-time
     *  data though the application may not always care. An
     *  implementation providing a real-time service may fall back to
     *  a previous snapshot in case of error. This method returns
     *  false if the data last retrieved was stale.
     *
     *  @return <code> true </code> if the object is up to date,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean isCurrent() {
        return (this.identifiable.isCurrent());
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public final boolean equals(Object obj) {
        return (this.identifiable.equals(obj));
    }
}
