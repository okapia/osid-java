//
// AbstractIndexedMapInstallationLookupSession.java
//
//    A simple framework for providing an Installation lookup service
//    backed by a fixed collection of installations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Installation lookup service backed by a
 *  fixed collection of installations. The installations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some installations may be compatible
 *  with more types than are indicated through these installation
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Installations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInstallationLookupSession
    extends AbstractMapInstallationLookupSession
    implements org.osid.installation.InstallationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.installation.Installation> installationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Installation>());
    private final MultiMap<org.osid.type.Type, org.osid.installation.Installation> installationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.installation.Installation>());


    /**
     *  Makes an <code>Installation</code> available in this session.
     *
     *  @param  installation an installation
     *  @throws org.osid.NullArgumentException <code>installation<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInstallation(org.osid.installation.Installation installation) {
        super.putInstallation(installation);

        this.installationsByGenus.put(installation.getGenusType(), installation);
        
        try (org.osid.type.TypeList types = installation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.installationsByRecord.put(types.getNextType(), installation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an installation from this session.
     *
     *  @param installationId the <code>Id</code> of the installation
     *  @throws org.osid.NullArgumentException <code>installationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInstallation(org.osid.id.Id installationId) {
        org.osid.installation.Installation installation;
        try {
            installation = getInstallation(installationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.installationsByGenus.remove(installation.getGenusType());

        try (org.osid.type.TypeList types = installation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.installationsByRecord.remove(types.getNextType(), installation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInstallation(installationId);
        return;
    }


    /**
     *  Gets an <code>InstallationList</code> corresponding to the given
     *  installation genus <code>Type</code> which does not include
     *  installations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known installations or an error results. Otherwise,
     *  the returned list may contain only those installations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  installationGenusType an installation genus type 
     *  @return the returned <code>Installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByGenusType(org.osid.type.Type installationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.installation.ArrayInstallationList(this.installationsByGenus.get(installationGenusType)));
    }


    /**
     *  Gets an <code>InstallationList</code> containing the given
     *  installation record <code>Type</code>. In plenary mode, the
     *  returned list contains all known installations or an error
     *  results. Otherwise, the returned list may contain only those
     *  installations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  installationRecordType an installation record type 
     *  @return the returned <code>installation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.InstallationList getInstallationsByRecordType(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.installation.installation.ArrayInstallationList(this.installationsByRecord.get(installationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.installationsByGenus.clear();
        this.installationsByRecord.clear();

        super.close();

        return;
    }
}
