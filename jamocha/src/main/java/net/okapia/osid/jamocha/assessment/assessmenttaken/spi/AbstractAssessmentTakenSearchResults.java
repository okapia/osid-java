//
// AbstractAssessmentTakenSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssessmentTakenSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.AssessmentTakenSearchResults {

    private org.osid.assessment.AssessmentTakenList assessmentsTaken;
    private final org.osid.assessment.AssessmentTakenQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssessmentTakenSearchResults.
     *
     *  @param assessmentsTaken the result set
     *  @param assessmentTakenQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assessmentsTaken</code>
     *          or <code>assessmentTakenQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssessmentTakenSearchResults(org.osid.assessment.AssessmentTakenList assessmentsTaken,
                                            org.osid.assessment.AssessmentTakenQueryInspector assessmentTakenQueryInspector) {
        nullarg(assessmentsTaken, "assessments taken");
        nullarg(assessmentTakenQueryInspector, "assessment taken query inspectpr");

        this.assessmentsTaken = assessmentsTaken;
        this.inspector = assessmentTakenQueryInspector;

        return;
    }


    /**
     *  Gets the assessment taken list resulting from a search.
     *
     *  @return an assessment taken list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTaken() {
        if (this.assessmentsTaken == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.AssessmentTakenList assessmentsTaken = this.assessmentsTaken;
        this.assessmentsTaken = null;
	return (assessmentsTaken);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.AssessmentTakenQueryInspector getAssessmentTakenQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  assessment taken search record <code> Type. </code> This method must
     *  be used to retrieve an assessmentTaken implementing the requested
     *  record.
     *
     *  @param assessmentTakenSearchRecordType an assessmentTaken search 
     *         record type 
     *  @return the assessment taken search
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assessmentTakenSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenSearchResultsRecord getAssessmentTakenSearchResultsRecord(org.osid.type.Type assessmentTakenSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.records.AssessmentTakenSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assessmentTakenSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record assessment taken search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssessmentTakenRecord(org.osid.assessment.records.AssessmentTakenSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "assessment taken record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
