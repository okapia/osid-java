//
// AbstractImmutableRoute.java
//
//     Wraps a mutable Route to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Route</code> to hide modifiers. This
 *  wrapper provides an immutized Route from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying route whose state changes are visible.
 */

public abstract class AbstractImmutableRoute
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.mapping.route.Route {

    private final org.osid.mapping.route.Route route;


    /**
     *  Constructs a new <code>AbstractImmutableRoute</code>.
     *
     *  @param route the route to immutablize
     *  @throws org.osid.NullArgumentException <code>route</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRoute(org.osid.mapping.route.Route route) {
        super(route);
        this.route = route;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the starting location of this route. 
     *
     *  @return the starting location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStartingLocationId() {
        return (this.route.getStartingLocationId());
    }


    /**
     *  Gets the starting location of this route. 
     *
     *  @return the starting location 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getStartingLocation()
        throws org.osid.OperationFailedException {

        return (this.route.getStartingLocation());
    }


    /**
     *  Gets the <code> Id </code> of the ending location of this route. 
     *
     *  @return the ending location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndingLocationId() {
        return (this.route.getEndingLocationId());
    }


    /**
     *  Gets the ending location of this route. 
     *
     *  @return the ending location 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getEndingLocation()
        throws org.osid.OperationFailedException {

        return (this.route.getEndingLocation());
    }


    /**
     *  Gets the total distance of this route. 
     *
     *  @return the total route distance 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistance() {
        return (this.route.getDistance());
    }


    /**
     *  Gets the estimated travel time across the entire route. 
     *
     *  @return the estimated travel time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getETA() {
        return (this.route.getETA());
    }


    /**
     *  Gets the route segment <code> Ids </code> of this route. 
     *
     *  @return the segment <code> Ids </code> of this route 
     */

    @OSID @Override
    public org.osid.id.IdList getSegmentIds() {
        return (this.route.getSegmentIds());
    }


    /**
     *  Gets the segments of this route. 
     *
     *  @return the segments of this route 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSegmentList getSegments()
        throws org.osid.OperationFailedException {

        return (this.route.getSegments());
    }


    /**
     *  Gets the route record corresponding to the given <code> Route </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> pathRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(routeRecordType) </code> is <code> true </code> . 
     *
     *  @param  routeRecordType the type of route record to retrieve 
     *  @return the route record 
     *  @throws org.osid.NullArgumentException <code> routeRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(routeRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteRecord getRouteRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        return (this.route.getRouteRecord(routeRecordType));
    }
}

