//
// AbstractRegistrationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRegistrationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.registration.RegistrationSearchResults {

    private org.osid.course.registration.RegistrationList registrations;
    private final org.osid.course.registration.RegistrationQueryInspector inspector;
    private final java.util.Collection<org.osid.course.registration.records.RegistrationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRegistrationSearchResults.
     *
     *  @param registrations the result set
     *  @param registrationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>registrations</code>
     *          or <code>registrationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRegistrationSearchResults(org.osid.course.registration.RegistrationList registrations,
                                            org.osid.course.registration.RegistrationQueryInspector registrationQueryInspector) {
        nullarg(registrations, "registrations");
        nullarg(registrationQueryInspector, "registration query inspectpr");

        this.registrations = registrations;
        this.inspector = registrationQueryInspector;

        return;
    }


    /**
     *  Gets the registration list resulting from a search.
     *
     *  @return a registration list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations() {
        if (this.registrations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.registration.RegistrationList registrations = this.registrations;
        this.registrations = null;
	return (registrations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.registration.RegistrationQueryInspector getRegistrationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  registration search record <code> Type. </code> This method must
     *  be used to retrieve a registration implementing the requested
     *  record.
     *
     *  @param registrationSearchRecordType a registration search 
     *         record type 
     *  @return the registration search
     *  @throws org.osid.NullArgumentException
     *          <code>registrationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(registrationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.RegistrationSearchResultsRecord getRegistrationSearchResultsRecord(org.osid.type.Type registrationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.registration.records.RegistrationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(registrationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(registrationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record registration search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRegistrationRecord(org.osid.course.registration.records.RegistrationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "registration record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
