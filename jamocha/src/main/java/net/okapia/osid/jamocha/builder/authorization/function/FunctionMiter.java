//
// FunctionMiter.java
//
//     Defines a Function miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.function;


/**
 *  Defines a <code>Function</code> miter for use with the builders.
 */

public interface FunctionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.authorization.Function {


    /**
     *  Sets the qualifier hierarchy.
     *
     *  @param hierarchy a qualifier hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          is <code>null</code>
     */

    public void setQualifierHierarchy(org.osid.hierarchy.Hierarchy hierarchy);


    /**
     *  Adds a Function record.
     *
     *  @param record a function record
     *  @param recordType the type of function record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addFunctionRecord(org.osid.authorization.records.FunctionRecord record, org.osid.type.Type recordType);
}       


