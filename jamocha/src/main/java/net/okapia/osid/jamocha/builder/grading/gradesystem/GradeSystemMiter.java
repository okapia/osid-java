//
// GradeSystemMiter.java
//
//     Defines a GradeSystem miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradesystem;


/**
 *  Defines a <code>GradeSystem</code> miter for use with the builders.
 */

public interface GradeSystemMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.grading.GradeSystem {


    /**
     *  Adds a grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public void addGrade(org.osid.grading.Grade grade);


    /**
     *  Sets all the grades.
     *
     *  @param grades a collection of grades
     *  @throws org.osid.NullArgumentException <code>grades</code> is
     *          <code>null</code>
     */

    public void setGrades(java.util.Collection<org.osid.grading.Grade> grades);


    /**
     *  Sets the lowest numeric score.
     *
     *  @param score a lowest numeric score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setLowestNumericScore(java.math.BigDecimal score);


    /**
     *  Sets the numeric score increment.
     *
     *  @param increment a numeric score increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    public void setNumericScoreIncrement(java.math.BigDecimal increment);


    /**
     *  Sets the highest numeric score.
     *
     *  @param score a highest numeric score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setHighestNumericScore(java.math.BigDecimal score);


    /**
     *  Adds a GradeSystem record.
     *
     *  @param record a gradeSystem record
     *  @param recordType the type of gradeSystem record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradeSystemRecord(org.osid.grading.records.GradeSystemRecord record, org.osid.type.Type recordType);
}       


