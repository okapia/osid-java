//
// AbstractQueryConfigurationLookupSession.java
//
//    A ConfigurationQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ConfigurationQuerySession adapter.
 */

public abstract class AbstractAdapterConfigurationQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.ConfigurationQuerySession {

    private final org.osid.configuration.ConfigurationQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterConfigurationQuerySession.
     *
     *  @param session the underlying configuration query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterConfigurationQuerySession(org.osid.configuration.ConfigurationQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeConfiguration</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchConfigurations() {
        return (this.session.canSearchConfigurations());
    }

      
    /**
     *  Gets a configuration query. The returned query will not have an
     *  extension query.
     *
     *  @return the configuration query 
     */
      
    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getConfigurationQuery() {
        return (this.session.getConfigurationQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  configurationQuery the configuration query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code configurationQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code configurationQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByQuery(org.osid.configuration.ConfigurationQuery configurationQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getConfigurationsByQuery(configurationQuery));
    }
}
