//
// AbstractMapCredentialLookupSession
//
//    A simple framework for providing a Credential lookup service
//    backed by a fixed collection of credentials.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Credential lookup service backed by a
 *  fixed collection of credentials. The credentials are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Credentials</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCredentialLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.program.Credential> credentials = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.program.Credential>());


    /**
     *  Makes a <code>Credential</code> available in this session.
     *
     *  @param  credential a credential
     *  @throws org.osid.NullArgumentException <code>credential<code>
     *          is <code>null</code>
     */

    protected void putCredential(org.osid.course.program.Credential credential) {
        this.credentials.put(credential.getId(), credential);
        return;
    }


    /**
     *  Makes an array of credentials available in this session.
     *
     *  @param  credentials an array of credentials
     *  @throws org.osid.NullArgumentException <code>credentials<code>
     *          is <code>null</code>
     */

    protected void putCredentials(org.osid.course.program.Credential[] credentials) {
        putCredentials(java.util.Arrays.asList(credentials));
        return;
    }


    /**
     *  Makes a collection of credentials available in this session.
     *
     *  @param  credentials a collection of credentials
     *  @throws org.osid.NullArgumentException <code>credentials<code>
     *          is <code>null</code>
     */

    protected void putCredentials(java.util.Collection<? extends org.osid.course.program.Credential> credentials) {
        for (org.osid.course.program.Credential credential : credentials) {
            this.credentials.put(credential.getId(), credential);
        }

        return;
    }


    /**
     *  Removes a Credential from this session.
     *
     *  @param  credentialId the <code>Id</code> of the credential
     *  @throws org.osid.NullArgumentException <code>credentialId<code> is
     *          <code>null</code>
     */

    protected void removeCredential(org.osid.id.Id credentialId) {
        this.credentials.remove(credentialId);
        return;
    }


    /**
     *  Gets the <code>Credential</code> specified by its <code>Id</code>.
     *
     *  @param  credentialId <code>Id</code> of the <code>Credential</code>
     *  @return the credential
     *  @throws org.osid.NotFoundException <code>credentialId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>credentialId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential(org.osid.id.Id credentialId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.Credential credential = this.credentials.get(credentialId);
        if (credential == null) {
            throw new org.osid.NotFoundException("credential not found: " + credentialId);
        }

        return (credential);
    }


    /**
     *  Gets all <code>Credentials</code>. In plenary mode, the returned
     *  list contains all known credentials or an error
     *  results. Otherwise, the returned list may contain only those
     *  credentials that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Credentials</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.credential.ArrayCredentialList(this.credentials.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.credentials.clear();
        super.close();
        return;
    }
}
