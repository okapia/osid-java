//
// Adapter.java
//
//     A generic adapter.
//
//
// Tom Coppeto
// Okapia
// 30 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A generic adapter. This adapter wraps a single delegate. Any
 *  methods not defined in a subclass are passed through to the
 *  delegate.
 */

public class Adapter<T>
    implements java.lang.reflect.InvocationHandler {
    
    private final T delegate;


    /**
     *  Constructs a new generic adapter.
     *
     *  @param delegate the underlying object
     *  @throws org.osid.NullArgumentException <code>delegate</code>
     *          id <code>null</code>
     */

    public Adapter(T delegate) {
        nullarg(delegate, "delegate");
        this.delegate = delegate;
        return;
    }


    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *  This method uses the manager identity methods of this adapter
     *  including Id, displayName, description, version, release date,
     *  license and provider. This behavior may be used by calling
     *  this method or replaced by overriding it completely.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    @Override
    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {
        
        if (args == null) {
            args = new Object[0];
        }

        for (java.lang.reflect.Method m : getClass().getDeclaredMethods()) {
            Class<?>[] params = m.getParameterTypes();
            if (m.getName().equals(method.getName()) && (args.length == params.length)) {
                for (int i = 0; i < args.length; i++) {
                    if (!args[i].getClass().getName().equals(params[i].getName())) {
                        return (method.invoke(getDelegate(), args));                    
                    }
                }
                
                return (m.invoke(this, args));
            }
        }

        try {
            return (method.invoke(getDelegate(), args));
        } catch (java.lang.reflect.InvocationTargetException ite) {
            throw ite.getTargetException();
        }
    }
    
    
    /**
     *  Returns a string representation of the object. In general, the
     *  <code>toString()</code> method returns a string that
     *  "textually represents" this object. The result should be a
     *  concise but informative representation that is easy for a
     *  person to read.
     *
     *  @return a string representation of the object.
     */
    
    @Override
    public String toString() {
        return (getDelegate().toString());
    }


    /**
     *  Creates the adapter for an object.
     *
     *  @return the proxy adapter
     */

    @SuppressWarnings("unchecked")
    public T create() {
        return ((T) java.lang.reflect.Proxy.newProxyInstance(getDelegate().getClass().getClassLoader(), 
                                                             getDelegate().getClass().getInterfaces(),
                                                             this));
    }


    /**
     *  Gets the delegate.
     *
     *  @return the delegate
     */

    public T getDelegate() {
        return (this.delegate);
    }
}
    
