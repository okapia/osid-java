//
// AbstractObjectiveBankLookupSession.java
//
//    A starter implementation framework for providing an ObjectiveBank
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an ObjectiveBank
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getObjectiveBanks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ObjectiveBankLookupSession {

    private boolean pedantic = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();
    

    /**
     *  Tests if this user can perform <code>ObjectiveBank</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObjectiveBanks() {
        return (true);
    }


    /**
     *  A complete view of the <code>ObjectiveBank</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObjectiveBankView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ObjectiveBank</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObjectiveBankView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>ObjectiveBank</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ObjectiveBank</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ObjectiveBank</code> and
     *  retained for compatibility.
     *
     *  @param  objectiveBankId <code>Id</code> of the
     *          <code>ObjectiveBank</code>
     *  @return the objective bank
     *  @throws org.osid.NotFoundException <code>objectiveBankId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>objectiveBankId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ObjectiveBankList objectiveBanks = getObjectiveBanks()) {
            while (objectiveBanks.hasNext()) {
                org.osid.learning.ObjectiveBank objectiveBank = objectiveBanks.getNextObjectiveBank();
                if (objectiveBank.getId().equals(objectiveBankId)) {
                    return (objectiveBank);
                }
            }
        } 

        throw new org.osid.NotFoundException(objectiveBankId + " not found");
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectiveBanks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ObjectiveBanks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getObjectiveBanks()</code>.
     *
     *  @param  objectiveBankIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByIds(org.osid.id.IdList objectiveBankIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.learning.ObjectiveBank> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = objectiveBankIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getObjectiveBank(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("objective bank " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.learning.objectivebank.LinkedObjectiveBankList(ret));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  objective bank genus <code>Type</code> which does not include
     *  objective banks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getObjectiveBanks()</code>.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.objectivebank.ObjectiveBankGenusFilterList(getObjectiveBanks(), objectiveBankGenusType));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  objective bank genus <code>Type</code> and include any additional
     *  objective banks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObjectiveBanks()</code>.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByParentGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectiveBanksByGenusType(objectiveBankGenusType));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> containing the given
     *  objective bank record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObjectiveBanks()</code>.
     *
     *  @param  objectiveBankRecordType an objectiveBank record type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByRecordType(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.learning.objectivebank.ObjectiveBankRecordFilterList(getObjectiveBanks(), objectiveBankRecordType));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known objective banks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  objective banks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>ObjectiveBank</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.learning.objectivebank.ObjectiveBankProviderFilterList(getObjectiveBanks(), resourceId));
    }


    /**
     *  Gets all <code>ObjectiveBanks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ObjectiveBanks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.learning.ObjectiveBankList getObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the objective bank list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of objective banks
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.learning.ObjectiveBankList filterObjectiveBanksOnViews(org.osid.learning.ObjectiveBankList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
