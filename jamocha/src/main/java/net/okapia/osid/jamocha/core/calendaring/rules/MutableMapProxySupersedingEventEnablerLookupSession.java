//
// MutableMapProxySupersedingEventEnablerLookupSession
//
//    Implements a SupersedingEventEnabler lookup service backed by a collection of
//    supersedingEventEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a SupersedingEventEnabler lookup service backed by a collection of
 *  supersedingEventEnablers. The supersedingEventEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of superseding event enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySupersedingEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapSupersedingEventEnablerLookupSession
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySupersedingEventEnablerLookupSession}
     *  with no superseding event enablers.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySupersedingEventEnablerLookupSession} with a
     *  single superseding event enabler.
     *
     *  @param calendar the calendar
     *  @param supersedingEventEnabler a superseding event enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEventEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putSupersedingEventEnabler(supersedingEventEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySupersedingEventEnablerLookupSession} using an
     *  array of superseding event enablers.
     *
     *  @param calendar the calendar
     *  @param supersedingEventEnablers an array of superseding event enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEventEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.rules.SupersedingEventEnabler[] supersedingEventEnablers, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySupersedingEventEnablerLookupSession} using a
     *  collection of superseding event enablers.
     *
     *  @param calendar the calendar
     *  @param supersedingEventEnablers a collection of superseding event enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEventEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                                java.util.Collection<? extends org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(calendar, proxy);
        setSessionProxy(proxy);
        putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }

    
    /**
     *  Makes a {@code SupersedingEventEnabler} available in this session.
     *
     *  @param supersedingEventEnabler an superseding event enabler
     *  @throws org.osid.NullArgumentException {@code supersedingEventEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSupersedingEventEnabler(org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler) {
        super.putSupersedingEventEnabler(supersedingEventEnabler);
        return;
    }


    /**
     *  Makes an array of supersedingEventEnablers available in this session.
     *
     *  @param supersedingEventEnablers an array of superseding event enablers
     *  @throws org.osid.NullArgumentException {@code supersedingEventEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putSupersedingEventEnablers(org.osid.calendaring.rules.SupersedingEventEnabler[] supersedingEventEnablers) {
        super.putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }


    /**
     *  Makes collection of superseding event enablers available in this session.
     *
     *  @param supersedingEventEnablers
     *  @throws org.osid.NullArgumentException {@code supersedingEventEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSupersedingEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablers) {
        super.putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }


    /**
     *  Removes a SupersedingEventEnabler from this session.
     *
     *  @param supersedingEventEnablerId the {@code Id} of the superseding event enabler
     *  @throws org.osid.NullArgumentException {@code supersedingEventEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId) {
        super.removeSupersedingEventEnabler(supersedingEventEnablerId);
        return;
    }    
}
