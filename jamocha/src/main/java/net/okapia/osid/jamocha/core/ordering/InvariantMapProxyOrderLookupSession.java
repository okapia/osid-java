//
// InvariantMapProxyOrderLookupSession
//
//    Implements an Order lookup service backed by a fixed
//    collection of orders. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements an Order lookup service backed by a fixed
 *  collection of orders. The orders are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyOrderLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractMapOrderLookupSession
    implements org.osid.ordering.OrderLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOrderLookupSession} with no
     *  orders.
     *
     *  @param store the store
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                  org.osid.proxy.Proxy proxy) {
        setStore(store);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyOrderLookupSession} with a single
     *  order.
     *
     *  @param store the store
     *  @param order an single order
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code order} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.Order order, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putOrder(order);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyOrderLookupSession} using
     *  an array of orders.
     *
     *  @param store the store
     *  @param orders an array of orders
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code orders} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                  org.osid.ordering.Order[] orders, org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putOrders(orders);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyOrderLookupSession} using a
     *  collection of orders.
     *
     *  @param store the store
     *  @param orders a collection of orders
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code orders} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyOrderLookupSession(org.osid.ordering.Store store,
                                                  java.util.Collection<? extends org.osid.ordering.Order> orders,
                                                  org.osid.proxy.Proxy proxy) {

        this(store, proxy);
        putOrders(orders);
        return;
    }
}
