//
// AbstractAuthorization.java
//
//     Defines an Authorization builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.authorization.spi;


/**
 *  Defines an <code>Authorization</code> builder.
 */

public abstract class AbstractAuthorizationBuilder<T extends AbstractAuthorizationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authorization.authorization.AuthorizationMiter authorization;


    /**
     *  Constructs a new <code>AbstractAuthorizationBuilder</code>.
     *
     *  @param authorization the authorization to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuthorizationBuilder(net.okapia.osid.jamocha.builder.authorization.authorization.AuthorizationMiter authorization) {
        super(authorization);
        this.authorization = authorization;
        return;
    }


    /**
     *  Builds the authorization.
     *
     *  @return the new authorization
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authorization.Authorization build() {
        (new net.okapia.osid.jamocha.builder.validator.authorization.authorization.AuthorizationValidator(getValidations())).validate(this.authorization);
        return (new net.okapia.osid.jamocha.builder.authorization.authorization.ImmutableAuthorization(this.authorization));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the authorization miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authorization.authorization.AuthorizationMiter getMiter() {
        return (this.authorization);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @return the builder
     */

    public T implicit() {
        getMiter().setImplicit(true);
        return (self());
    }


    /**
     *  Unsets the implicit flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setImplicit(false);
        return (self());
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the trust.
     *
     *  @param trust a trust
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>trust</code> is <code>null</code>
     */

    public T trust(org.osid.authentication.process.Trust trust) {
        getMiter().setTrust(trust);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the function.
     *
     *  @param function a function
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>function</code> is <code>null</code>
     */

    public T function(org.osid.authorization.Function function) {
        getMiter().setFunction(function);
        return (self());
    }


    /**
     *  Sets the qualifier.
     *
     *  @param qualifier a qualifier
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>qualifier</code> is <code>null</code>
     */

    public T qualifier(org.osid.authorization.Qualifier qualifier) {
        getMiter().setQualifier(qualifier);
        return (self());
    }


    /**
     *  Adds an Authorization record.
     *
     *  @param record an authorization record
     *  @param recordType the type of authorization record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authorization.records.AuthorizationRecord record, org.osid.type.Type recordType) {
        getMiter().addAuthorizationRecord(record, recordType);
        return (self());
    }
}       


