//
// AbstractIndexedMapRaceProcessorEnablerLookupSession.java
//
//    A simple framework for providing a RaceProcessorEnabler lookup service
//    backed by a fixed collection of race processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RaceProcessorEnabler lookup service backed by a
 *  fixed collection of race processor enablers. The race processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some race processor enablers may be compatible
 *  with more types than are indicated through these race processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRaceProcessorEnablerLookupSession
    extends AbstractMapRaceProcessorEnablerLookupSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceProcessorEnabler>());


    /**
     *  Makes a <code>RaceProcessorEnabler</code> available in this session.
     *
     *  @param  raceProcessorEnabler a race processor enabler
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRaceProcessorEnabler(org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler) {
        super.putRaceProcessorEnabler(raceProcessorEnabler);

        this.raceProcessorEnablersByGenus.put(raceProcessorEnabler.getGenusType(), raceProcessorEnabler);
        
        try (org.osid.type.TypeList types = raceProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceProcessorEnablersByRecord.put(types.getNextType(), raceProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a race processor enabler from this session.
     *
     *  @param raceProcessorEnablerId the <code>Id</code> of the race processor enabler
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId) {
        org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler;
        try {
            raceProcessorEnabler = getRaceProcessorEnabler(raceProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.raceProcessorEnablersByGenus.remove(raceProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = raceProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceProcessorEnablersByRecord.remove(types.getNextType(), raceProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRaceProcessorEnabler(raceProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to the given
     *  race processor enabler genus <code>Type</code> which does not include
     *  race processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known race processor enablers or an error results. Otherwise,
     *  the returned list may contain only those race processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  raceProcessorEnablerGenusType a race processor enabler genus type 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.ArrayRaceProcessorEnablerList(this.raceProcessorEnablersByGenus.get(raceProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> containing the given
     *  race processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known race processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  race processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  raceProcessorEnablerRecordType a race processor enabler record type 
     *  @return the returned <code>raceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByRecordType(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.ArrayRaceProcessorEnablerList(this.raceProcessorEnablersByRecord.get(raceProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceProcessorEnablersByGenus.clear();
        this.raceProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
