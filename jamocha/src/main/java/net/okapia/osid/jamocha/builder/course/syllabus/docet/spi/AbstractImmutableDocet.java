//
// AbstractImmutableDocet.java
//
//     Wraps a mutable Docet to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Docet</code> to hide modifiers. This
 *  wrapper provides an immutized Docet from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying docet whose state changes are visible.
 */

public abstract class AbstractImmutableDocet
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.syllabus.Docet {

    private final org.osid.course.syllabus.Docet docet;


    /**
     *  Constructs a new <code>AbstractImmutableDocet</code>.
     *
     *  @param docet the docet to immutablize
     *  @throws org.osid.NullArgumentException <code>docet</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDocet(org.osid.course.syllabus.Docet docet) {
        super(docet);
        this.docet = docet;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the learning module. 
     *
     *  @return the learning module <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getModuleId() {
        return (this.docet.getModuleId());
    }


    /**
     *  Gets the learning module 
     *
     *  @return the learning module 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule()
        throws org.osid.OperationFailedException {

        return (this.docet.getModule());
    }


    /**
     *  Gets the <code> Id </code> of the activity unit. 
     *
     *  @return the activity unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityUnitId() {
        return (this.docet.getActivityUnitId());
    }


    /**
     *  Gets the activity unit. 
     *
     *  @return the activity unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit()
        throws org.osid.OperationFailedException {

        return (this.docet.getActivityUnit());
    }


    /**
     *  Gets the <code> Ids </code> of the learning objectives. 
     *
     *  @return the learning objective <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        return (this.docet.getLearningObjectiveIds());
    }


    /**
     *  Gets the learning objectives. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (this.docet.getLearningObjectives());
    }


    /**
     *  Tests if this <code> Docet </code> occurs within an <code> Activity 
     *  </code> or it outlines efforts spent outside a convened activity. In 
     *  class docets can be used to align the estimated duration with the 
     *  schedule of activities. 
     *
     *  @return <code> true </code> if this occurs within an activity, <code> 
     *          false </code> if occurs outside a convened activity 
     */

    @OSID @Override
    public boolean isInClass() {
        return (this.docet.isInClass());
    }


    /**
     *  Gets the estimated duration. 
     *
     *  @return the duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration() {
        return (this.docet.getDuration());
    }


    /**
     *  Tests if this <code> Docet </code> has a reading materials expressed 
     *  as <code> Assets. </code> 
     *
     *  @return <code> true </code> if assets are avilable, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssets() {
        return (this.docet.hasAssets());
    }


    /**
     *  Gets a list of asset materials distributed to the students. 
     *
     *  @return the asset <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssets() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        return (this.docet.getAssetIds());
    }


    /**
     *  Gets the assets distributed to the students. 
     *
     *  @return a list of assets 
     *  @throws org.osid.IllegalStateException <code> hasAssets() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (this.docet.getAssets());
    }


    /**
     *  Tests if this <code> Docet </code> has a quiz or assignment expressed 
     *  as an <code> Assessment </code> . 
     *
     *  @return <code> true </code> if assessments are avilable, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssessments() {
        return (this.docet.hasAssessments());
    }


    /**
     *  Gets a list of assessments. 
     *
     *  @return the assessment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessments() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssessmentIds() {
        return (this.docet.getAssessmentIds());
    }


    /**
     *  Gets the assessments. 
     *
     *  @return a list of assessments 
     *  @throws org.osid.IllegalStateException <code> hasAssessments() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException {

        return (this.docet.getAssessments());
    }


    /**
     *  Gets the docet record corresponding to the given <code> Docet </code> 
     *  record <code> Type. </code> This method must be used to retrieve an 
     *  object implementing the requested record. The <code> docetRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(docetRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  docetRecordType the type of docet record to retrieve 
     *  @return the docet record 
     *  @throws org.osid.NullArgumentException <code> docetRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(docetRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetRecord getDocetRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        return (this.docet.getDocetRecord(docetRecordType));
    }
}

