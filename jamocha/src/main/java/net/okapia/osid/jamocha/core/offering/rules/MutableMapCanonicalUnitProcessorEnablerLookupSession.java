//
// MutableMapCanonicalUnitProcessorEnablerLookupSession
//
//    Implements a CanonicalUnitProcessorEnabler lookup service backed by a collection of
//    canonicalUnitProcessorEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitProcessorEnabler lookup service backed by a collection of
 *  canonical unit processor enablers. The canonical unit processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of canonical unit processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCanonicalUnitProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractMapCanonicalUnitProcessorEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapCanonicalUnitProcessorEnablerLookupSession}
     *  with no canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue} is
     *          {@code null}
     */

      public MutableMapCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCanonicalUnitProcessorEnablerLookupSession} with a
     *  single canonicalUnitProcessorEnabler.
     *
     *  @param catalogue the catalogue  
     *  @param canonicalUnitProcessorEnabler a canonical unit processor enabler
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessorEnabler} is {@code null}
     */

    public MutableMapCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                           org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {
        this(catalogue);
        putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCanonicalUnitProcessorEnablerLookupSession}
     *  using an array of canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessorEnablers an array of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessorEnablers} is {@code null}
     */

    public MutableMapCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                           org.osid.offering.rules.CanonicalUnitProcessorEnabler[] canonicalUnitProcessorEnablers) {
        this(catalogue);
        putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCanonicalUnitProcessorEnablerLookupSession}
     *  using a collection of canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessorEnablers a collection of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitProcessorEnablers} is {@code null}
     */

    public MutableMapCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                           java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers) {

        this(catalogue);
        putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code CanonicalUnitProcessorEnabler} available in this session.
     *
     *  @param canonicalUnitProcessorEnabler a canonical unit processor enabler
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitProcessorEnabler(org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {
        super.putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of canonical unit processor enablers available in this session.
     *
     *  @param canonicalUnitProcessorEnablers an array of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitProcessorEnablers(org.osid.offering.rules.CanonicalUnitProcessorEnabler[] canonicalUnitProcessorEnablers) {
        super.putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of canonical unit processor enablers available in this session.
     *
     *  @param canonicalUnitProcessorEnablers a collection of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitProcessorEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers) {
        super.putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }


    /**
     *  Removes a CanonicalUnitProcessorEnabler from this session.
     *
     *  @param canonicalUnitProcessorEnablerId the {@code Id} of the canonical unit processor enabler
     *  @throws org.osid.NullArgumentException {@code canonicalUnitProcessorEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId) {
        super.removeCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnablerId);
        return;
    }    
}
