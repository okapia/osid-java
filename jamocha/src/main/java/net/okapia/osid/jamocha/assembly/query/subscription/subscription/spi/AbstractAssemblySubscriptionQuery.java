//
// AbstractAssemblySubscriptionQuery.java
//
//     A SubscriptionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SubscriptionQuery that stores terms.
 */

public abstract class AbstractAssemblySubscriptionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.subscription.SubscriptionQuery,
               org.osid.subscription.SubscriptionQueryInspector,
               org.osid.subscription.SubscriptionSearchOrder {

    private final java.util.Collection<org.osid.subscription.records.SubscriptionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.SubscriptionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.SubscriptionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySubscriptionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySubscriptionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  dispatchId a dispatch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> dispatchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDispatchId(org.osid.id.Id dispatchId, boolean match) {
        getAssembler().addIdTerm(getDispatchIdColumn(), dispatchId, match);
        return;
    }


    /**
     *  Clears the dispatch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDispatchIdTerms() {
        getAssembler().clearTerms(getDispatchIdColumn());
        return;
    }


    /**
     *  Gets the dispatch <code> Id </code> terms. 
     *
     *  @return the dispatch <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDispatchIdTerms() {
        return (getAssembler().getIdTerms(getDispatchIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the dispatch. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDispatch(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDispatchColumn(), style);
        return;
    }


    /**
     *  Gets the DispatchId column name.
     *
     * @return the column name
     */

    protected String getDispatchIdColumn() {
        return ("dispatch_id");
    }


    /**
     *  Tests if a <code> DispatchQuery </code> is available. 
     *
     *  @return <code> true </code> if a dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the dispatch query 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuery getDispatchQuery() {
        throw new org.osid.UnimplementedException("supportsDispatchQuery() is false");
    }


    /**
     *  Clears the dispatch terms. 
     */

    @OSID @Override
    public void clearDispatchTerms() {
        getAssembler().clearTerms(getDispatchColumn());
        return;
    }


    /**
     *  Gets the dispatch terms. 
     *
     *  @return the dispatch terms 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQueryInspector[] getDispatchTerms() {
        return (new org.osid.subscription.DispatchQueryInspector[0]);
    }


    /**
     *  Tests if a dispatch order is available. 
     *
     *  @return <code> true </code> if a dispatch order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSearchOrder() {
        return (false);
    }


    /**
     *  Gets the dispatch order. 
     *
     *  @return the dispatch search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchOrder getDispatchSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDispatchSearchOrder() is false");
    }


    /**
     *  Gets the Dispatch column name.
     *
     * @return the column name
     */

    protected String getDispatchColumn() {
        return ("dispatch");
    }


    /**
     *  Sets a subscriber <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriberId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSubscriberIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the subscriber <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriberIdTerms() {
        getAssembler().clearTerms(getSubscriberIdColumn());
        return;
    }


    /**
     *  Gets the subscriber <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriberIdTerms() {
        return (getAssembler().getIdTerms(getSubscriberIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the subscriber. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubscriber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubscriberColumn(), style);
        return;
    }


    /**
     *  Gets the SubscriberId column name.
     *
     * @return the column name
     */

    protected String getSubscriberIdColumn() {
        return ("subscriber_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriberQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subscriber query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriberQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubscriberQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriberQuery() is false");
    }


    /**
     *  Clears the subscriber terms. 
     */

    @OSID @Override
    public void clearSubscriberTerms() {
        getAssembler().clearTerms(getSubscriberColumn());
        return;
    }


    /**
     *  Gets the subscriber terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSubscriberTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a subscriber order is available. 
     *
     *  @return <code> true </code> if a subscriber order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriberSearchOrder() {
        return (false);
    }


    /**
     *  Gets the subscriber order. 
     *
     *  @return the subscriber search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriberSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSubscriberSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubscriberSearchOrder() is false");
    }


    /**
     *  Gets the Subscriber column name.
     *
     * @return the column name
     */

    protected String getSubscriberColumn() {
        return ("subscriber");
    }


    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        getAssembler().addIdTerm(getAddressIdColumn(), addressId, match);
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        getAssembler().clearTerms(getAddressIdColumn());
        return;
    }


    /**
     *  Gets the address <code> Id </code> terms. 
     *
     *  @return the address <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressIdTerms() {
        return (getAssembler().getIdTerms(getAddressIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddressColumn(), style);
        return;
    }


    /**
     *  Gets the AddressId column name.
     *
     * @return the column name
     */

    protected String getAddressIdColumn() {
        return ("address_id");
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches any address. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        getAssembler().addIdWildcardTerm(getAddressColumn(), match);
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        getAssembler().clearTerms(getAddressColumn());
        return;
    }


    /**
     *  Gets the address terms. 
     *
     *  @return the address terms 
     */

    @OSID @Override
    public org.osid.contact.AddressQueryInspector[] getAddressTerms() {
        return (new org.osid.contact.AddressQueryInspector[0]);
    }


    /**
     *  Tests if a address order is available. 
     *
     *  @return <code> true </code> if an address order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSearchOrder() {
        return (false);
    }


    /**
     *  Gets the address order. 
     *
     *  @return the address search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddressSearchOrder() is false");
    }


    /**
     *  Gets the Address column name.
     *
     * @return the column name
     */

    protected String getAddressColumn() {
        return ("address");
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        getAssembler().addIdTerm(getPublisherIdColumn(), publisherId, match);
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        getAssembler().clearTerms(getPublisherIdColumn());
        return;
    }


    /**
     *  Gets the publisher <code> Id </code> terms. 
     *
     *  @return the publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (getAssembler().getIdTerms(getPublisherIdColumn()));
    }


    /**
     *  Gets the PublisherId column name.
     *
     * @return the column name
     */

    protected String getPublisherIdColumn() {
        return ("publisher_id");
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        getAssembler().clearTerms(getPublisherColumn());
        return;
    }


    /**
     *  Gets the publisher terms. 
     *
     *  @return the publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the Publisher column name.
     *
     * @return the column name
     */

    protected String getPublisherColumn() {
        return ("publisher");
    }


    /**
     *  Tests if this subscription supports the given record
     *  <code>Type</code>.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return <code>true</code> if the subscriptionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type subscriptionRecordType) {
        for (org.osid.subscription.records.SubscriptionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  subscriptionRecordType the subscription record type 
     *  @return the subscription query record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionQueryRecord getSubscriptionQueryRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  subscriptionRecordType the subscription record type 
     *  @return the subscription query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionQueryInspectorRecord getSubscriptionQueryInspectorRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param subscriptionRecordType the subscription record type
     *  @return the subscription search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionSearchOrderRecord getSubscriptionSearchOrderRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this subscription. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param subscriptionQueryRecord the subscription query record
     *  @param subscriptionQueryInspectorRecord the subscription query inspector
     *         record
     *  @param subscriptionSearchOrderRecord the subscription search order record
     *  @param subscriptionRecordType subscription record type
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionQueryRecord</code>,
     *          <code>subscriptionQueryInspectorRecord</code>,
     *          <code>subscriptionSearchOrderRecord</code> or
     *          <code>subscriptionRecordTypesubscription</code> is
     *          <code>null</code>
     */
            
    protected void addSubscriptionRecords(org.osid.subscription.records.SubscriptionQueryRecord subscriptionQueryRecord, 
                                      org.osid.subscription.records.SubscriptionQueryInspectorRecord subscriptionQueryInspectorRecord, 
                                      org.osid.subscription.records.SubscriptionSearchOrderRecord subscriptionSearchOrderRecord, 
                                      org.osid.type.Type subscriptionRecordType) {

        addRecordType(subscriptionRecordType);

        nullarg(subscriptionQueryRecord, "subscription query record");
        nullarg(subscriptionQueryInspectorRecord, "subscription query inspector record");
        nullarg(subscriptionSearchOrderRecord, "subscription search odrer record");

        this.queryRecords.add(subscriptionQueryRecord);
        this.queryInspectorRecords.add(subscriptionQueryInspectorRecord);
        this.searchOrderRecords.add(subscriptionSearchOrderRecord);
        
        return;
    }
}
