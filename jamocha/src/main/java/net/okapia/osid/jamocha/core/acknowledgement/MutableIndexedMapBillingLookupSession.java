//
// MutableIndexedMapBillingLookupSession
//
//    Implements a Billing lookup service backed by a collection of
//    billings indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement;


/**
 *  Implements a Billing lookup service backed by a collection of
 *  billings. The billings are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some billings may be compatible
 *  with more types than are indicated through these billing
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of billings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapBillingLookupSession
    extends net.okapia.osid.jamocha.core.acknowledgement.spi.AbstractIndexedMapBillingLookupSession
    implements org.osid.acknowledgement.BillingLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBillingLookupSession} with no
     *  billings.
     */

    public MutableIndexedMapBillingLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBillingLookupSession} with a
     *  single billing.
     *  
     *  @param  billing a single billing
     *  @throws org.osid.NullArgumentException {@code billing}
     *          is {@code null}
     */

    public MutableIndexedMapBillingLookupSession(org.osid.acknowledgement.Billing billing) {
        putBilling(billing);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBillingLookupSession} using an
     *  array of billings.
     *
     *  @param  billings an array of billings
     *  @throws org.osid.NullArgumentException {@code billings}
     *          is {@code null}
     */

    public MutableIndexedMapBillingLookupSession(org.osid.acknowledgement.Billing[] billings) {
        putBillings(billings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapBillingLookupSession} using a
     *  collection of billings.
     *
     *  @param  billings a collection of billings
     *  @throws org.osid.NullArgumentException {@code billings} is
     *          {@code null}
     */

    public MutableIndexedMapBillingLookupSession(java.util.Collection<? extends org.osid.acknowledgement.Billing> billings) {
        putBillings(billings);
        return;
    }
    

    /**
     *  Makes a {@code Billing} available in this session.
     *
     *  @param  billing a billing
     *  @throws org.osid.NullArgumentException {@code billing{@code  is
     *          {@code null}
     */

    @Override
    public void putBilling(org.osid.acknowledgement.Billing billing) {
        super.putBilling(billing);
        return;
    }


    /**
     *  Makes an array of billings available in this session.
     *
     *  @param  billings an array of billings
     *  @throws org.osid.NullArgumentException {@code billings{@code 
     *          is {@code null}
     */

    @Override
    public void putBillings(org.osid.acknowledgement.Billing[] billings) {
        super.putBillings(billings);
        return;
    }


    /**
     *  Makes collection of billings available in this session.
     *
     *  @param  billings a collection of billings
     *  @throws org.osid.NullArgumentException {@code billing{@code  is
     *          {@code null}
     */

    @Override
    public void putBillings(java.util.Collection<? extends org.osid.acknowledgement.Billing> billings) {
        super.putBillings(billings);
        return;
    }


    /**
     *  Removes a Billing from this session.
     *
     *  @param billingId the {@code Id} of the billing
     *  @throws org.osid.NullArgumentException {@code billingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBilling(org.osid.id.Id billingId) {
        super.removeBilling(billingId);
        return;
    }    
}
