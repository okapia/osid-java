//
// AbstractNodeBankHierarchySession.java
//
//     Defines a Bank hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a bank hierarchy session for delivering a hierarchy
 *  of banks using the BankNode interface.
 */

public abstract class AbstractNodeBankHierarchySession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractBankHierarchySession
    implements org.osid.assessment.BankHierarchySession {

    private java.util.Collection<org.osid.assessment.BankNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root bank <code> Ids </code> in this hierarchy.
     *
     *  @return the root bank <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBankIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.assessment.banknode.BankNodeToIdList(this.roots));
    }


    /**
     *  Gets the root banks in the bank hierarchy. A node
     *  with no parents is an orphan. While all bank <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root banks 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getRootBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.assessment.banknode.BankNodeToBankList(new net.okapia.osid.jamocha.assessment.banknode.ArrayBankNodeList(this.roots)));
    }


    /**
     *  Adds a root bank node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBank(org.osid.assessment.BankNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root bank nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBanks(java.util.Collection<org.osid.assessment.BankNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root bank node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBank(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.assessment.BankNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Bank </code> has any parents. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @return <code> true </code> if the bank has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBanks(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBankNode(bankId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param  bankId the <code> Id </code> of a bank 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> bankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> bankId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBank(org.osid.id.Id id, org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.BankNodeList parents = getBankNode(bankId).getParentBankNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBankNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given bank. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @return the parent <code> Ids </code> of the bank 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBankIds(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.assessment.bank.BankToIdList(getParentBanks(bankId)));
    }


    /**
     *  Gets the parents of the given bank. 
     *
     *  @param  bankId the <code> Id </code> to query 
     *  @return the parents of the bank 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getParentBanks(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.assessment.banknode.BankNodeToBankList(getBankNode(bankId).getParentBankNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param  bankId the Id of a bank 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> bankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBank(org.osid.id.Id id, org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBank(id, bankId)) {
            return (true);
        }

        try (org.osid.assessment.BankList parents = getParentBanks(bankId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBank(id, parents.getNextBank().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a bank has any children. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @return <code> true </code> if the <code> bankId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBanks(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBankNode(bankId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param bankId the <code> Id </code> of a 
     *         bank
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> bankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> bankId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBank(org.osid.id.Id id, org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBank(bankId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  bank.
     *
     *  @param  bankId the <code> Id </code> to query 
     *  @return the children of the bank 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBankIds(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.assessment.bank.BankToIdList(getChildBanks(bankId)));
    }


    /**
     *  Gets the children of the given bank. 
     *
     *  @param  bankId the <code> Id </code> to query 
     *  @return the children of the bank 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankList getChildBanks(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.assessment.banknode.BankNodeToBankList(getBankNode(bankId).getChildBankNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param bankId the <code> Id </code> of a 
     *         bank
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> bankId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBank(org.osid.id.Id id, org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBank(bankId, id)) {
            return (true);
        }

        try (org.osid.assessment.BankList children = getChildBanks(bankId)) {
            while (children.hasNext()) {
                if (isDescendantOfBank(id, children.getNextBank().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  bank.
     *
     *  @param  bankId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified bank node 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBankNodeIds(org.osid.id.Id bankId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.assessment.banknode.BankNodeToNode(getBankNode(bankId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given bank.
     *
     *  @param  bankId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified bank node 
     *  @throws org.osid.NotFoundException <code> bankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.BankNode getBankNodes(org.osid.id.Id bankId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBankNode(bankId));
    }


    /**
     *  Closes this <code>BankHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a bank node.
     *
     *  @param bankId the id of the bank node
     *  @throws org.osid.NotFoundException <code>bankId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>bankId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.assessment.BankNode getBankNode(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(bankId, "bank Id");
        for (org.osid.assessment.BankNode bank : this.roots) {
            if (bank.getId().equals(bankId)) {
                return (bank);
            }

            org.osid.assessment.BankNode r = findBank(bank, bankId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(bankId + " is not found");
    }


    protected org.osid.assessment.BankNode findBank(org.osid.assessment.BankNode node, 
                                                            org.osid.id.Id bankId)
        throws org.osid.OperationFailedException {

        try (org.osid.assessment.BankNodeList children = node.getChildBankNodes()) {
            while (children.hasNext()) {
                org.osid.assessment.BankNode bank = children.getNextBankNode();
                if (bank.getId().equals(bankId)) {
                    return (bank);
                }
                
                bank = findBank(bank, bankId);
                if (bank != null) {
                    return (bank);
                }
            }
        }

        return (null);
    }
}
