//
// AbstractMapInquestLookupSession
//
//    A simple framework for providing an Inquest lookup service
//    backed by a fixed collection of inquests.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Inquest lookup service backed by a
 *  fixed collection of inquests. The inquests are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inquests</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInquestLookupSession
    extends net.okapia.osid.jamocha.inquiry.spi.AbstractInquestLookupSession
    implements org.osid.inquiry.InquestLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.Inquest> inquests = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.Inquest>());


    /**
     *  Makes an <code>Inquest</code> available in this session.
     *
     *  @param  inquest an inquest
     *  @throws org.osid.NullArgumentException <code>inquest<code>
     *          is <code>null</code>
     */

    protected void putInquest(org.osid.inquiry.Inquest inquest) {
        this.inquests.put(inquest.getId(), inquest);
        return;
    }


    /**
     *  Makes an array of inquests available in this session.
     *
     *  @param  inquests an array of inquests
     *  @throws org.osid.NullArgumentException <code>inquests<code>
     *          is <code>null</code>
     */

    protected void putInquests(org.osid.inquiry.Inquest[] inquests) {
        putInquests(java.util.Arrays.asList(inquests));
        return;
    }


    /**
     *  Makes a collection of inquests available in this session.
     *
     *  @param  inquests a collection of inquests
     *  @throws org.osid.NullArgumentException <code>inquests<code>
     *          is <code>null</code>
     */

    protected void putInquests(java.util.Collection<? extends org.osid.inquiry.Inquest> inquests) {
        for (org.osid.inquiry.Inquest inquest : inquests) {
            this.inquests.put(inquest.getId(), inquest);
        }

        return;
    }


    /**
     *  Removes an Inquest from this session.
     *
     *  @param  inquestId the <code>Id</code> of the inquest
     *  @throws org.osid.NullArgumentException <code>inquestId<code> is
     *          <code>null</code>
     */

    protected void removeInquest(org.osid.id.Id inquestId) {
        this.inquests.remove(inquestId);
        return;
    }


    /**
     *  Gets the <code>Inquest</code> specified by its <code>Id</code>.
     *
     *  @param  inquestId <code>Id</code> of the <code>Inquest</code>
     *  @return the inquest
     *  @throws org.osid.NotFoundException <code>inquestId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inquestId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.Inquest inquest = this.inquests.get(inquestId);
        if (inquest == null) {
            throw new org.osid.NotFoundException("inquest not found: " + inquestId);
        }

        return (inquest);
    }


    /**
     *  Gets all <code>Inquests</code>. In plenary mode, the returned
     *  list contains all known inquests or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquests that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Inquests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getInquests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquest.ArrayInquestList(this.inquests.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquests.clear();
        super.close();
        return;
    }
}
