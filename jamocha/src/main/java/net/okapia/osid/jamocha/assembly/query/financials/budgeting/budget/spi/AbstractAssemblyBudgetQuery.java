//
// AbstractAssemblyBudgetQuery.java
//
//     A BudgetQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BudgetQuery that stores terms.
 */

public abstract class AbstractAssemblyBudgetQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.financials.budgeting.BudgetQuery,
               org.osid.financials.budgeting.BudgetQueryInspector,
               org.osid.financials.budgeting.BudgetSearchOrder {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBudgetQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBudgetQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        getAssembler().addIdTerm(getFiscalPeriodIdColumn(), fiscalPeriodId, match);
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        getAssembler().clearTerms(getFiscalPeriodIdColumn());
        return;
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (getAssembler().getIdTerms(getFiscalPeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalPeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFiscalPeriodColumn(), style);
        return;
    }


    /**
     *  Gets the FiscalPeriodId column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodIdColumn() {
        return ("fiscal_period_id");
    }


    /**
     *  Tests if a <code> FiscalPeriod </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches any fiscal period. 
     *
     *  @param  match <code> true </code> to match budgets with any fiscal 
     *          period, <code> false </code> to match budgets with no fiscal 
     *          period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getFiscalPeriodColumn(), match);
        return;
    }


    /**
     *  Clears the fiscal period terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        getAssembler().clearTerms(getFiscalPeriodColumn());
        return;
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Tests if a fiscal period search order is available. 
     *
     *  @return <code> true </code> if a fiscal period search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the fiscal period search order. 
     *
     *  @return the fiscal period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchOrder getFiscalPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodSearchOrder() is false");
    }


    /**
     *  Gets the FiscalPeriod column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodColumn() {
        return ("fiscal_period");
    }


    /**
     *  Sets the budget entry <code> Id </code> for this query. 
     *
     *  @param  budgetEntryId a budget entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> budgetEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetEntryId(org.osid.id.Id budgetEntryId, boolean match) {
        getAssembler().addIdTerm(getBudgetEntryIdColumn(), budgetEntryId, match);
        return;
    }


    /**
     *  Clears the budget entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBudgetEntryIdTerms() {
        getAssembler().clearTerms(getBudgetEntryIdColumn());
        return;
    }


    /**
     *  Gets the budget entry <code> Id </code> query terms. 
     *
     *  @return the budget entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBudgetEntryIdTerms() {
        return (getAssembler().getIdTerms(getBudgetEntryIdColumn()));
    }


    /**
     *  Gets the BudgetEntryId column name.
     *
     * @return the column name
     */

    protected String getBudgetEntryIdColumn() {
        return ("budget_entry_id");
    }


    /**
     *  Tests if an <code> BudgetEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a budget entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a budget entries. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the budget entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuery getBudgetEntryQuery() {
        throw new org.osid.UnimplementedException("supportsBudgetEntryQuery() is false");
    }


    /**
     *  Matches any related budget entries. 
     *
     *  @param  match <code> true </code> to match budgets with any budget 
     *          entry, <code> false </code> to match budgets with no entries 
     */

    @OSID @Override
    public void matchAnyBudgetEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getBudgetEntryColumn(), match);
        return;
    }


    /**
     *  Clears the budget entry terms. 
     */

    @OSID @Override
    public void clearBudgetEntryTerms() {
        getAssembler().clearTerms(getBudgetEntryColumn());
        return;
    }


    /**
     *  Gets the budget entry query terms. 
     *
     *  @return the budget entry query terms 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQueryInspector[] getBudgetEntryTerms() {
        return (new org.osid.financials.budgeting.BudgetEntryQueryInspector[0]);
    }


    /**
     *  Gets the BudgetEntry column name.
     *
     * @return the column name
     */

    protected String getBudgetEntryColumn() {
        return ("budget_entry");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match budgets 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this budget supports the given record
     *  <code>Type</code>.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return <code>true</code> if the budgetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type budgetRecordType) {
        for (org.osid.financials.budgeting.records.BudgetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  budgetRecordType the budget record type 
     *  @return the budget query record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetQueryRecord getBudgetQueryRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  budgetRecordType the budget record type 
     *  @return the budget query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetQueryInspectorRecord getBudgetQueryInspectorRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param budgetRecordType the budget record type
     *  @return the budget search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetSearchOrderRecord getBudgetSearchOrderRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this budget. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param budgetQueryRecord the budget query record
     *  @param budgetQueryInspectorRecord the budget query inspector
     *         record
     *  @param budgetSearchOrderRecord the budget search order record
     *  @param budgetRecordType budget record type
     *  @throws org.osid.NullArgumentException
     *          <code>budgetQueryRecord</code>,
     *          <code>budgetQueryInspectorRecord</code>,
     *          <code>budgetSearchOrderRecord</code> or
     *          <code>budgetRecordTypebudget</code> is
     *          <code>null</code>
     */
            
    protected void addBudgetRecords(org.osid.financials.budgeting.records.BudgetQueryRecord budgetQueryRecord, 
                                      org.osid.financials.budgeting.records.BudgetQueryInspectorRecord budgetQueryInspectorRecord, 
                                      org.osid.financials.budgeting.records.BudgetSearchOrderRecord budgetSearchOrderRecord, 
                                      org.osid.type.Type budgetRecordType) {

        addRecordType(budgetRecordType);

        nullarg(budgetQueryRecord, "budget query record");
        nullarg(budgetQueryInspectorRecord, "budget query inspector record");
        nullarg(budgetSearchOrderRecord, "budget search odrer record");

        this.queryRecords.add(budgetQueryRecord);
        this.queryInspectorRecords.add(budgetQueryInspectorRecord);
        this.searchOrderRecords.add(budgetSearchOrderRecord);
        
        return;
    }
}
