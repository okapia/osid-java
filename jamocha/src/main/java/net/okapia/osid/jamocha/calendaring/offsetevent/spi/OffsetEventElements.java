//
// OffsetEventElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.offsetevent.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OffsetEventElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the OffsetEventElement Id.
     *
     *  @return the offset event element Id
     */

    public static org.osid.id.Id getOffsetEventEntityId() {
        return (makeEntityId("osid.calendaring.OffsetEvent"));
    }


    /**
     *  Gets the FixedStartTime element Id.
     *
     *  @return the FixedStartTime element Id
     */

    public static org.osid.id.Id getFixedStartTime() {
        return (makeElementId("osid.calendaring.offsetevent.FixedStartTime"));
    }


    /**
     *  Gets the StartReferenceEventId element Id.
     *
     *  @return the StartReferenceEventId element Id
     */

    public static org.osid.id.Id getStartReferenceEventId() {
        return (makeElementId("osid.calendaring.offsetevent.StartReferenceEventId"));
    }


    /**
     *  Gets the StartReferenceEvent element Id.
     *
     *  @return the StartReferenceEvent element Id
     */

    public static org.osid.id.Id getStartReferenceEvent() {
        return (makeElementId("osid.calendaring.offsetevent.StartReferenceEvent"));
    }


    /**
     *  Gets the FixedStartOffset element Id.
     *
     *  @return the FixedStartOffset element Id
     */

    public static org.osid.id.Id getFixedStartOffset() {
        return (makeElementId("osid.calendaring.offsetevent.FixedStartOffset"));
    }


    /**
     *  Gets the RelativeWeekdayStartOffset element Id.
     *
     *  @return the RelativeWeekdayStartOffset element Id
     */

    public static org.osid.id.Id getRelativeWeekdayStartOffset() {
        return (makeElementId("osid.calendaring.offsetevent.RelativeWeekdayStartOffset"));
    }


    /**
     *  Gets the RelativeStartWeekday element Id.
     *
     *  @return the RelativeStartWeekday element Id
     */

    public static org.osid.id.Id getRelativeStartWeekday() {
        return (makeElementId("osid.calendaring.offsetevent.RelativeStartWeekday"));
    }


    /**
     *  Gets the EndReferenceEventId element Id.
     *
     *  @return the EndReferenceEventId element Id
     */

    public static org.osid.id.Id getEndReferenceEventId() {
        return (makeElementId("osid.calendaring.offsetevent.EndReferenceEventId"));
    }


    /**
     *  Gets the EndReferenceEvent element Id.
     *
     *  @return the EndReferenceEvent element Id
     */

    public static org.osid.id.Id getEndReferenceEvent() {
        return (makeElementId("osid.calendaring.offsetevent.EndReferenceEvent"));
    }


    /**
     *  Gets the FixedEndOffset element Id.
     *
     *  @return the FixedEndOffset element Id
     */

    public static org.osid.id.Id getFixedEndOffset() {
        return (makeElementId("osid.calendaring.offsetevent.FixedEndOffset"));
    }


    /**
     *  Gets the RelativeWeekdayEndOffset element Id.
     *
     *  @return the RelativeWeekdayEndOffset element Id
     */

    public static org.osid.id.Id getRelativeWeekdayEndOffset() {
        return (makeElementId("osid.calendaring.offsetevent.RelativeWeekdayEndOffset"));
    }


    /**
     *  Gets the RelativeEndWeekday element Id.
     *
     *  @return the RelativeEndWeekday element Id
     */

    public static org.osid.id.Id getRelativeEndWeekday() {
        return (makeElementId("osid.calendaring.offsetevent.RelativeEndWeekday"));
    }


    /**
     *  Gets the LocationDescription element Id.
     *
     *  @return the LocationDescription element Id
     */

    public static org.osid.id.Id getLocationDescription() {
        return (makeElementId("osid.calendaring.offsetevent.LocationDescription"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeElementId("osid.calendaring.offsetevent.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeElementId("osid.calendaring.offsetevent.Location"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.calendaring.offsetevent.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.calendaring.offsetevent.Sponsors"));
    }


    /**
     *  Gets the FixedDuration element Id.
     *
     *  @return the FixedDuration element Id
     */

    public static org.osid.id.Id getFixedDuration() {
        return (makeElementId("osid.calendaring.offsetevent.FixedDuration"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.offsetevent.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.offsetevent.Calendar"));
    }
}
