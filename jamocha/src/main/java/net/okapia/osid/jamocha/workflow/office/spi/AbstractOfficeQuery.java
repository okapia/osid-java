//
// AbstractOfficeQuery.java
//
//     A template for making an Office Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.office.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for offices.
 */

public abstract class AbstractOfficeQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.workflow.OfficeQuery {

    private final java.util.Collection<org.osid.workflow.records.OfficeQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the step <code> Id </code> for this query to match offices 
     *  containing process. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessId(org.osid.id.Id processId, boolean match) {
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getProcessQuery() {
        throw new org.osid.UnimplementedException("supportsProcessQuery() is false");
    }


    /**
     *  Matches offices that have any process. 
     *
     *  @param  match <code> true </code> to match offices with any process, 
     *          <code> false </code> to match offices with no process 
     */

    @OSID @Override
    public void matchAnyProcess(boolean match) {
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearProcessTerms() {
        return;
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if an step query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for an step. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches offices with any step. 
     *
     *  @param  match <code> true </code> to match offices with any step, 
     *          <code> false </code> to match offices with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches offices that have any work. 
     *
     *  @param  match <code> true </code> to match offices with any work, 
     *          <code> false </code> to match offices with no process 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the office <code> Id </code> for this query to match offices that 
     *  have the specified office as an ancestor. 
     *
     *  @param  officeId a office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getAncestorOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOfficeQuery() is false");
    }


    /**
     *  Matches offices with any ancestor. 
     *
     *  @param  match <code> true </code> to match offices with any ancestor, 
     *          <code> false </code> to match root offices 
     */

    @OSID @Override
    public void matchAnyAncestorOffice(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor office query terms. 
     */

    @OSID @Override
    public void clearAncestorOfficeTerms() {
        return;
    }


    /**
     *  Sets the office <code> Id </code> for this query to match offices that 
     *  have the specified office as a descendant. 
     *
     *  @param  officeId a office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOfficeQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getDescendantOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOfficeQuery() is false");
    }


    /**
     *  Matches offices with any descendant. 
     *
     *  @param  match <code> true </code> to match offices with any 
     *          descendant, <code> false </code> to match leaf offices 
     */

    @OSID @Override
    public void matchAnyDescendantOffice(boolean match) {
        return;
    }


    /**
     *  Clears the descendant office query terms. 
     */

    @OSID @Override
    public void clearDescendantOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given office query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an office implementing the requested record.
     *
     *  @param officeRecordType an office record type
     *  @return the office query record
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeQueryRecord getOfficeQueryRecord(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.OfficeQueryRecord record : this.records) {
            if (record.implementsRecordType(officeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this office query. 
     *
     *  @param officeQueryRecord office query record
     *  @param officeRecordType office record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfficeQueryRecord(org.osid.workflow.records.OfficeQueryRecord officeQueryRecord, 
                                          org.osid.type.Type officeRecordType) {

        addRecordType(officeRecordType);
        nullarg(officeQueryRecord, "office query record");
        this.records.add(officeQueryRecord);        
        return;
    }
}
