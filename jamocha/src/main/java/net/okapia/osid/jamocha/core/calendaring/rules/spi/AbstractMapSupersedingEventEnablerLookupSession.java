//
// AbstractMapSupersedingEventEnablerLookupSession
//
//    A simple framework for providing a SupersedingEventEnabler lookup service
//    backed by a fixed collection of superseding event enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SupersedingEventEnabler lookup service backed by a
 *  fixed collection of superseding event enablers. The superseding event enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SupersedingEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSupersedingEventEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractSupersedingEventEnablerLookupSession
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.rules.SupersedingEventEnabler>());


    /**
     *  Makes a <code>SupersedingEventEnabler</code> available in this session.
     *
     *  @param  supersedingEventEnabler a superseding event enabler
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnabler<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEventEnabler(org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler) {
        this.supersedingEventEnablers.put(supersedingEventEnabler.getId(), supersedingEventEnabler);
        return;
    }


    /**
     *  Makes an array of superseding event enablers available in this session.
     *
     *  @param  supersedingEventEnablers an array of superseding event enablers
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEventEnablers(org.osid.calendaring.rules.SupersedingEventEnabler[] supersedingEventEnablers) {
        putSupersedingEventEnablers(java.util.Arrays.asList(supersedingEventEnablers));
        return;
    }


    /**
     *  Makes a collection of superseding event enablers available in this session.
     *
     *  @param  supersedingEventEnablers a collection of superseding event enablers
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablers<code>
     *          is <code>null</code>
     */

    protected void putSupersedingEventEnablers(java.util.Collection<? extends org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablers) {
        for (org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler : supersedingEventEnablers) {
            this.supersedingEventEnablers.put(supersedingEventEnabler.getId(), supersedingEventEnabler);
        }

        return;
    }


    /**
     *  Removes a SupersedingEventEnabler from this session.
     *
     *  @param  supersedingEventEnablerId the <code>Id</code> of the superseding event enabler
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId) {
        this.supersedingEventEnablers.remove(supersedingEventEnablerId);
        return;
    }


    /**
     *  Gets the <code>SupersedingEventEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  supersedingEventEnablerId <code>Id</code> of the <code>SupersedingEventEnabler</code>
     *  @return the supersedingEventEnabler
     *  @throws org.osid.NotFoundException <code>supersedingEventEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnabler getSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler = this.supersedingEventEnablers.get(supersedingEventEnablerId);
        if (supersedingEventEnabler == null) {
            throw new org.osid.NotFoundException("supersedingEventEnabler not found: " + supersedingEventEnablerId);
        }

        return (supersedingEventEnabler);
    }


    /**
     *  Gets all <code>SupersedingEventEnablers</code>. In plenary mode, the returned
     *  list contains all known supersedingEventEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  supersedingEventEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SupersedingEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.ArraySupersedingEventEnablerList(this.supersedingEventEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.supersedingEventEnablers.clear();
        super.close();
        return;
    }
}
