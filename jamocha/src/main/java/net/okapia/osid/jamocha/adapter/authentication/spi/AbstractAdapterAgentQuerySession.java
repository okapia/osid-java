//
// AbstractQueryAgentLookupSession.java
//
//    An AgentQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AgentQuerySession adapter.
 */

public abstract class AbstractAdapterAgentQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authentication.AgentQuerySession {

    private final org.osid.authentication.AgentQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAgentQuerySession.
     *
     *  @param session the underlying agent query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAgentQuerySession(org.osid.authentication.AgentQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeAgency</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeAgency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the {@codeAgency</code> associated with this 
     *  session.
     *
     *  @return the {@codeAgency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform {@codeAgent</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAgents() {
        return (this.session.canSearchAgents());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agents in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this agency only.
     */
    
    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
      
    /**
     *  Gets an agent query. The returned query will not have an
     *  extension query.
     *
     *  @return the agent query 
     */
      
    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        return (this.session.getAgentQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  agentQuery the agent query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code agentQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code agentQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByQuery(org.osid.authentication.AgentQuery agentQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAgentsByQuery(agentQuery));
    }
}
