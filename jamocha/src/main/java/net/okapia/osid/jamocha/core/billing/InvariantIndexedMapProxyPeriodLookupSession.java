//
// InvariantIndexedMapProxyPeriodLookupSession
//
//    Implements a Period lookup service backed by a fixed
//    collection of periods indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements a Period lookup service backed by a fixed
 *  collection of periods. The periods are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some periods may be compatible
 *  with more types than are indicated through these period
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyPeriodLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractIndexedMapPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPeriodLookupSession}
     *  using an array of periods.
     *
     *  @param business the business
     *  @param periods an array of periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code periods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPeriodLookupSession(org.osid.billing.Business business,
                                                         org.osid.billing.Period[] periods, 
                                                         org.osid.proxy.Proxy proxy) {

        setBusiness(business);
        setSessionProxy(proxy);
        putPeriods(periods);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPeriodLookupSession}
     *  using a collection of periods.
     *
     *  @param business the business
     *  @param periods a collection of periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code periods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPeriodLookupSession(org.osid.billing.Business business,
                                                         java.util.Collection<? extends org.osid.billing.Period> periods,
                                                         org.osid.proxy.Proxy proxy) {

        setBusiness(business);
        setSessionProxy(proxy);
        putPeriods(periods);

        return;
    }
}
