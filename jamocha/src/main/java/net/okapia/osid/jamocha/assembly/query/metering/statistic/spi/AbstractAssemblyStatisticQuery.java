//
// AbstractAssemblyStatisticQuery.java
//
//     A StatisticQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StatisticQuery that stores terms.
 */

public abstract class AbstractAssemblyStatisticQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCompendiumQuery
    implements org.osid.metering.StatisticQuery,
               org.osid.metering.StatisticQueryInspector,
               org.osid.metering.StatisticSearchOrder {

    private final java.util.Collection<org.osid.metering.records.StatisticQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.StatisticQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.StatisticSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();
    

    /** 
     *  Constructs a new <code>AbstractAssemblyStatisticQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStatisticQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a meter <code> Id. </code> 
     *
     *  @param  meterId a meter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> meterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeterId(org.osid.id.Id meterId, boolean match) {
        getAssembler().addIdTerm(getMeterIdColumn(), meterId, match);
        return;
    }


    /**
     *  Clears the meter <code> Id </code> query term. 
     */

    @OSID @Override
    public void clearMeterIdTerms() {
        getAssembler().clearTerms(getMeterIdColumn());
        return;
    }


    /**
     *  Gets the meter <code> Id </code> terms. 
     *
     *  @return the meter <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeterIdTerms() {
        return (getAssembler().getIdTerms(getMeterIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the metered object. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMeter(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMeterColumn(), style);
        return;
    }


    /**
     *  Gets the MeterId column name.
     *
     * @return the column name
     */

    protected String getMeterIdColumn() {
        return ("meter_id");
    }


    /**
     *  Tests if a meter query is available. 
     *
     *  @return <code> true </code> if a meter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a meter. 
     *
     *  @return the meter query 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuery getMeterQuery() {
        throw new org.osid.UnimplementedException("supportsMeterQuery() is false");
    }


    /**
     *  Clears the meter query terms. 
     */

    @OSID @Override
    public void clearMeterTerms() {
        getAssembler().clearTerms(getMeterColumn());
        return;
    }


    /**
     *  Gets the meter terms. 
     *
     *  @return the meter terms 
     */

    @OSID @Override
    public org.osid.metering.MeterQueryInspector[] getMeterTerms() {
        return (new org.osid.metering.MeterQueryInspector[0]);
    }


    /**
     *  Tests if a <code> MeterSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a meter search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a meter. 
     *
     *  @return the meter search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchOrder getMeterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsMeterSearchOrder() is false");
    }


    /**
     *  Gets the Meter column name.
     *
     *  @return the column name
     */

    protected String getMeterColumn() {
        return ("meter");
    }


    /**
     *  Sets an object <code> Id. </code> 
     *
     *  @param  objectId an object <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeteredObjectId(org.osid.id.Id objectId, boolean match) {
        getAssembler().addIdTerm(getMeteredObjectIdColumn(), objectId, match);
        return;
    }


    /**
     *  Clears the object query terms. 
     */

    @OSID @Override
    public void clearMeteredObjectIdTerms() {
        getAssembler().clearTerms(getMeteredObjectIdColumn());
        return;
    }


    /**
     *  Gets the object <code> Id </code> terms. 
     *
     *  @return the object <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMeteredObjectIdTerms() {
        return (getAssembler().getIdTerms(getMeteredObjectIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the metered object.
     *
     *  @param  style a search order style
     *  @throws org.osid.NullArgumentException <code> style </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void orderByMeteredObject(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMeteredObjectIdColumn(), style);
        return;
    }


    /**
     *  Gets the ObjectId column name.
     *
     * @return the column name
     */

    protected String getMeteredObjectIdColumn() {
        return ("metered_object_id");
    }


    /**
     *  Sets the time for the statistics to generate. 
     *
     *  @param  start the start time 
     *  @param  end the end time 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void setTimeRange(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end) {
        getAssembler().addDateTimeRangeTerm(getTimeRangeColumn(), start, end, true);
        return;
    }


    /**
     *  Gets the time interval term.
     *
     *  @return the time interval
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeRangeTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeRangeColumn()));
    }


    /**
     *  Gets the Time Range column name.
     *
     *  @return the column name
     */

    protected String getTimeRangeColumn() {
        return ("time_range");
    }


    /**
     *  Matches statistics with the sum between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchSum(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        getAssembler().addDecimalRangeTerm(getSumColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the sum query terms. 
     */

    @OSID @Override
    public void clearSumTerms() {
        getAssembler().clearTerms(getSumColumn());
        return;
    }


    /**
     *  Gets the sum terms. 
     *
     *  @return the sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getSumTerms() {
        return (getAssembler().getDecimalRangeTerms(getSumColumn()));
    }


    /**
     *  Specified a preference for ordering results by the sum. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySum(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSumColumn(), style);
        return;
    }


    /**
     *  Gets the Sum column name.
     *
     * @return the column name
     */

    protected String getSumColumn() {
        return ("sum");
    }


    /**
     *  Matches statistics with the minimum sum value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumSum(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumSumColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum sum query terms. 
     */

    @OSID @Override
    public void clearMinimumSumTerms() {
        getAssembler().clearTerms(getMinimumSumColumn());
        return;
    }


    /**
     *  Gets the minumim sum terms. 
     *
     *  @return the minumim sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumSumTerms() {
        return (getAssembler().getDecimalTerms(getMinimumSumColumn()));
    }


    /**
     *  Gets the MinimumSum column name.
     *
     * @return the column name
     */

    protected String getMinimumSumColumn() {
        return ("minimum_sum");
    }


    /**
     *  Matches statistics with the mean between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMean(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getMeanColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the mean query terms. 
     */

    @OSID @Override
    public void clearMeanTerms() {
        getAssembler().clearTerms(getMeanColumn());
        return;
    }


    /**
     *  Gets the mean terms. 
     *
     *  @return the mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMeanTerms() {
        return (getAssembler().getDecimalRangeTerms(getMeanColumn()));
    }


    /**
     *  Specified a preference for ordering results by the mean. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMean(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMeanColumn(), style);
        return;
    }


    /**
     *  Gets the Mean column name.
     *
     * @return the column name
     */

    protected String getMeanColumn() {
        return ("mean");
    }


    /**
     *  Matches statistics with the minimum mean value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMean(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumMeanColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum mean query terms. 
     */

    @OSID @Override
    public void clearMinimumMeanTerms() {
        getAssembler().clearTerms(getMinimumMeanColumn());
        return;
    }


    /**
     *  Gets the minumim mean terms. 
     *
     *  @return the minumim mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMeanTerms() {
        return (getAssembler().getDecimalTerms(getMinimumMeanColumn()));
    }


    /**
     *  Gets the MinimumMean column name.
     *
     * @return the column name
     */

    protected String getMinimumMeanColumn() {
        return ("minimum_mean");
    }


    /**
     *  Matches statistics with the median between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMedian(java.math.BigDecimal low, 
                            java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getMedianColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the median query terms. 
     */

    @OSID @Override
    public void clearMedianTerms() {
        getAssembler().clearTerms(getMedianColumn());
        return;
    }


    /**
     *  Gets the median terms. 
     *
     *  @return the median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMedianTerms() {
        return (getAssembler().getDecimalRangeTerms(getMedianColumn()));
    }


    /**
     *  Specified a preference for ordering results by the median. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMedian(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMedianColumn(), style);
        return;
    }


    /**
     *  Gets the Median column name.
     *
     * @return the column name
     */

    protected String getMedianColumn() {
        return ("median");
    }


    /**
     *  Matches statistics with the minimum median value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMedian(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumMedianColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum median query terms. 
     */

    @OSID @Override
    public void clearMinimumMedianTerms() {
        getAssembler().clearTerms(getMinimumMedianColumn());
        return;
    }


    /**
     *  Gets the minumim median terms. 
     *
     *  @return the minumim median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMedianTerms() {
        return (getAssembler().getDecimalTerms(getMinimumMedianColumn()));
    }


    /**
     *  Gets the MinimumMedian column name.
     *
     * @return the column name
     */

    protected String getMinimumMedianColumn() {
        return ("minimum_median");
    }


    /**
     *  Matches statistics with the mode between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMode(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        getAssembler().addDecimalRangeTerm(getModeColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the mode query terms. 
     */

    @OSID @Override
    public void clearModeTerms() {
        getAssembler().clearTerms(getModeColumn());
        return;
    }


    /**
     *  Gets the mode terms. 
     *
     *  @return the mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getModeTerms() {
        return (getAssembler().getDecimalRangeTerms(getModeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the mode. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModeColumn(), style);
        return;
    }


    /**
     *  Gets the Mode column name.
     *
     * @return the column name
     */

    protected String getModeColumn() {
        return ("mode");
    }


    /**
     *  Matches statistics with the minimum mode value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMode(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumModeColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum mode query terms. 
     */

    @OSID @Override
    public void clearMinimumModeTerms() {
        getAssembler().clearTerms(getMinimumModeColumn());
        return;
    }


    /**
     *  Gets the minumim mode terms. 
     *
     *  @return the minumim mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumModeTerms() {
        return (getAssembler().getDecimalTerms(getMinimumModeColumn()));
    }


    /**
     *  Gets the MinimumMode column name.
     *
     * @return the column name
     */

    protected String getMinimumModeColumn() {
        return ("minimum_mode");
    }


    /**
     *  Matches statistics with the standard deviation between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchStandardDeviation(java.math.BigDecimal low, 
                                       java.math.BigDecimal high, 
                                       boolean match) {
        getAssembler().addDecimalRangeTerm(getStandardDeviationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the standard deviation query terms. 
     */

    @OSID @Override
    public void clearStandardDeviationTerms() {
        getAssembler().clearTerms(getStandardDeviationColumn());
        return;
    }


    /**
     *  Gets the standard deviation terms. 
     *
     *  @return the standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getStandardDeviationTerms() {
        return (getAssembler().getDecimalRangeTerms(getStandardDeviationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the standard deviation. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStandardDeviation(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStandardDeviationColumn(), style);
        return;
    }


    /**
     *  Gets the StandardDeviation column name.
     *
     * @return the column name
     */

    protected String getStandardDeviationColumn() {
        return ("standard_deviation");
    }


    /**
     *  Matches statistics with the minimum standard deviation value 
     *  inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumStandardDeviation(java.math.BigDecimal value, 
                                              boolean match) {
        getAssembler().addDecimalTerm(getMinimumStandardDeviationColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum standard deviation query terms. 
     */

    @OSID @Override
    public void clearMinimumStandardDeviationTerms() {
        getAssembler().clearTerms(getMinimumStandardDeviationColumn());
        return;
    }


    /**
     *  Gets the minumim standard deviation terms. 
     *
     *  @return the minumim standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumStandardDeviationTerms() {
        return (getAssembler().getDecimalTerms(getMinimumStandardDeviationColumn()));
    }


    /**
     *  Gets the MinimumStandardDeviation column name.
     *
     * @return the column name
     */

    protected String getMinimumStandardDeviationColumn() {
        return ("minimum_standard_deviation");
    }


    /**
     *  Matches statistics with the root mean square between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchRMS(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        getAssembler().addDecimalRangeTerm(getRMSColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the rms query terms. 
     */

    @OSID @Override
    public void clearRMSTerms() {
        getAssembler().clearTerms(getRMSColumn());
        return;
    }


    /**
     *  Gets the root mean square terms. 
     *
     *  @return the root mean square terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getRMSTerms() {
        return (getAssembler().getDecimalRangeTerms(getRMSColumn()));
    }


    /**
     *  Specified a preference for ordering results by the root mean square. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRMS(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRMSColumn(), style);
        return;
    }


    /**
     *  Gets the RMS column name.
     *
     * @return the column name
     */

    protected String getRMSColumn() {
        return ("r_ms");
    }


    /**
     *  Matches statistics with the minimum rms value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumRMS(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumRMSColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum rms query terms. 
     */

    @OSID @Override
    public void clearMinimumRMSTerms() {
        getAssembler().clearTerms(getMinimumRMSColumn());
        return;
    }


    /**
     *  Gets the minumim root mean square terms. 
     *
     *  @return the minumim root mean square terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumRMSTerms() {
        return (getAssembler().getDecimalTerms(getMinimumRMSColumn()));
    }


    /**
     *  Gets the MinimumRMS column name.
     *
     * @return the column name
     */

    protected String getMinimumRMSColumn() {
        return ("minimum_rms");
    }


    /**
     *  Matches statistics with the delta between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchDelta(java.math.BigDecimal low, java.math.BigDecimal high, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getDeltaColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the delta query terms. 
     */

    @OSID @Override
    public void clearDeltaTerms() {
        getAssembler().clearTerms(getDeltaColumn());
        return;
    }


    /**
     *  Gets the delta terms. 
     *
     *  @return the delta terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeltaTerms() {
        return (getAssembler().getDecimalRangeTerms(getDeltaColumn()));
    }


    /**
     *  Specified a preference for ordering results by the delta. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDelta(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeltaColumn(), style);
        return;
    }


    /**
     *  Gets the Delta column name.
     *
     * @return the column name
     */

    protected String getDeltaColumn() {
        return ("delta");
    }


    /**
     *  Matches statistics with the minimum delta value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumDelta(java.math.BigDecimal value, boolean match) {
        getAssembler().addDecimalTerm(getMinimumDeltaColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum delta query terms. 
     */

    @OSID @Override
    public void clearMinimumDeltaTerms() {
        getAssembler().clearTerms(getMinimumDeltaColumn());
        return;
    }


    /**
     *  Gets the minumim delta terms. 
     *
     *  @return the minumim delta terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumDeltaTerms() {
        return (getAssembler().getDecimalTerms(getMinimumDeltaColumn()));
    }


    /**
     *  Gets the MinimumDelta column name.
     *
     * @return the column name
     */

    protected String getMinimumDeltaColumn() {
        return ("minimum_delta");
    }


    /**
     *  Matches statistics with the percentage change between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchPercentChange(java.math.BigDecimal low, 
                                   java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getPercentChangeColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the delta query terms. 
     */

    @OSID @Override
    public void clearPercentChangeTerms() {
        getAssembler().clearTerms(getPercentChangeColumn());
        return;
    }


    /**
     *  Gets the percentage change terms. 
     *
     *  @return the percentage change terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getPercentChangeTerms() {
        return (getAssembler().getDecimalRangeTerms(getPercentChangeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the percent change.
     *
     *  @param  style a search order style
     *  @throws org.osid.NullArgumentException <code> style </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public void orderByPercentChange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPercentChangeColumn(), style);
        return;
    }


    /**
     *  Gets the Percent Change column name.
     *
     * @return the column name
     */

    protected String getPercentChangeColumn() {
        return ("percentage_change");
    }


    /**
     *  Matches statistics with the minimum percentage change value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumPercentChange(java.math.BigDecimal value, 
                                          boolean match) {
        getAssembler().addDecimalTerm(getMinimumPercentChangeColumn(), value, match);
        return;
    }


    /**
     *  Clears the minimum percentage change query terms. 
     */

    @OSID @Override
    public void clearMinimumPercentChangeTerms() {
        getAssembler().clearTerms(getMinimumPercentChangeColumn());
        return;
    }


    /**
     *  Gets the minimum delta terms. 
     *
     *  @return the minumim percentage change terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumPercentChangeTerms() {
        return (getAssembler().getDecimalTerms(getMinimumPercentChangeColumn()));
    }


    /**
     *  Gets the Minimum PercentChange column name.
     *
     * @return the column name
     */

    protected String getMinimumPercentChangeColumn() {
        return ("minimum_percentage_change");
    }


    /**
     *  Matches statistics with the average rate between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchAverageRate(java.math.BigDecimal low, 
                                 java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getAverageRateColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the average rate query terms. 
     */

    @OSID @Override
    public void clearAverageRateTerms() {
        getAssembler().clearTerms(getAverageRateColumn());
        return;
    }


    /**
     *  Gets the average rate terms. 
     *
     *  @return the average rate terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getAverageRateTerms() {
        return (getAssembler().getDecimalRangeTerms(getAverageRateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the average
     *  rate.
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByAverageRate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAverageRateColumn(), style);
        return;
    }


    /**
     *  Gets the AverageRate column name.
     *
     * @return the column name
     */

    protected String getAverageRateColumn() {
        return ("average_rate");
    }


    /**
     *  Tests if this statistic supports the given record
     *  <code>Type</code>.
     *
     *  @param  statisticRecordType a statistic record type 
     *  @return <code>true</code> if the statisticRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type statisticRecordType) {
        for (org.osid.metering.records.StatisticQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  statisticRecordType the statistic record type 
     *  @return the statistic query record 
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticQueryRecord getStatisticQueryRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  statisticRecordType the statistic record type 
     *  @return the statistic query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticQueryInspectorRecord getStatisticQueryInspectorRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param statisticRecordType the statistic record type
     *  @return the statistic search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticSearchOrderRecord getStatisticSearchOrderRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this statistic. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param statisticQueryRecord the statistic query record
     *  @param statisticQueryInspectorRecord the statistic query inspector
     *         record
     *  @param statisticSearchOrderRecord the statistic search order record
     *  @param statisticRecordType statistic record type
     *  @throws org.osid.NullArgumentException
     *          <code>statisticQueryRecord</code>,
     *          <code>statisticQueryInspectorRecord</code>,
     *          <code>statisticSearchOrderRecord</code> or
     *          <code>statisticRecordTypestatistic</code> is
     *          <code>null</code>
     */
            
    protected void addStatisticRecords(org.osid.metering.records.StatisticQueryRecord statisticQueryRecord, 
                                      org.osid.metering.records.StatisticQueryInspectorRecord statisticQueryInspectorRecord, 
                                      org.osid.metering.records.StatisticSearchOrderRecord statisticSearchOrderRecord, 
                                      org.osid.type.Type statisticRecordType) {

        addRecordType(statisticRecordType);

        nullarg(statisticQueryRecord, "statistic query record");
        nullarg(statisticQueryInspectorRecord, "statistic query inspector record");
        nullarg(statisticSearchOrderRecord, "statistic search odrer record");

        this.queryRecords.add(statisticQueryRecord);
        this.queryInspectorRecords.add(statisticQueryInspectorRecord);
        this.searchOrderRecords.add(statisticSearchOrderRecord);
        
        return;
    }
}
