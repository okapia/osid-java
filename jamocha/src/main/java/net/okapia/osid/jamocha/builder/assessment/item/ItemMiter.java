//
// ItemMiter.java
//
//     Defines an Item miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.item;


/**
 *  Defines an <code>Item</code> miter for use with the builders.
 */

public interface ItemMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.assessment.Item {


    /**
     *  Adds a learning objective.
     *
     *  @param learningObjective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjective</code> is <code>null</code>
     */

    public void addLearningObjective(org.osid.learning.Objective learningObjective);


    /**
     *  Sets the question.
     *
     *  @param question the question
     *  @throws org.osid/NullArgumentException <code>question</code>
     *          is <code>null</code>
     */

    public void setQuestion(org.osid.assessment.Question question);


    /**
     *  Adds an answer.
     *
     *  @param answer an answer
     *  @throws org.osid/NullArgumentException <code>answer</code>
     *          is <code>null</code>
     */

    public void addAnswer(org.osid.assessment.Answer answer);


    /**
     *  Sets all the answers.
     *
     *  @param answers a collection of answers
     *  @throws org.osid/NullArgumentException <code>answers</code>
     *          is <code>null</code>
     */

    public void setAnswers(java.util.Collection<org.osid.assessment.Answer> answers);


    /**
     *  Sets all the learning objectives.
     *
     *  @param learningObjectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectives</code> is <code>null</code>
     */

    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> learningObjectives);


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addItemRecord(org.osid.assessment.records.ItemRecord record, org.osid.type.Type recordType);
}       


