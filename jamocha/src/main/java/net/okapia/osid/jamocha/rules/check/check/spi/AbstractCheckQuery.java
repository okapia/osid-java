//
// AbstractCheckQuery.java
//
//     A template for making a Check Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for checks.
 */

public abstract class AbstractCheckQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.rules.check.CheckQuery {

    private final java.util.Collection<org.osid.rules.check.records.CheckQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches fail checks. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFailCheck(boolean match) {
        return;
    }


    /**
     *  Clears the fail check query terms. 
     */

    @OSID @Override
    public void clearFailCheckTerms() {
        return;
    }


    /**
     *  Matches time checks with start dates that fall in the given
     *  range inclusive.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckStartDate(org.osid.calendaring.DateTime from, 
                                        org.osid.calendaring.DateTime to, 
                                        boolean match) {
        return;
    }


    /**
     *  Matches any time check with any start date. 
     *
     *  @param match <code> true </code> to match any time checks with
     *          a start date, <code> false </code> to match checks
     *          with no start date
     */

    @OSID @Override
    public void matchAnyTimeCheckStartDate(boolean match) {
        return;
    }


    /**
     *  Clears the time check start date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckStartDateTerms() {
        return;
    }


    /**
     *  Matches time checks with end dates that fall in the given
     *  range inclusive.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckEndDate(org.osid.calendaring.DateTime from, 
                                      org.osid.calendaring.DateTime to, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches any time check with any end date. 
     *
     *  @param match <code> true </code> to match any time checks with
     *          a end date, <code> false </code> to match checks
     *          with no end date
     */

    @OSID @Override
    public void matchAnyTimeCheckEndDate(boolean match) {
        return;
    }


    /**
     *  Clears the time check end date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEndDateTerms() {
        return;
    }


    /**
     *  Matches time checks with dates overlapping the give date
     *  range.
     *  
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the time check date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckDateTerms() {
        return;
    }


    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId the event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimeCheckEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getTimeCheckEventQuery() {
        throw new org.osid.UnimplementedException("supportsTimeCheckEventQuery() is false");
    }


    /**
     *  Matches any time check with an event. 
     *
     *  @param  match <code> true </code> to match any time checks with an 
     *          event, <code> false </code> to match checks with no events 
     */

    @OSID @Override
    public void matchAnyTimeCheckEvent(boolean match) {
        return;
    }


    /**
     *  Clears the event query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEventTerms() {
        return;
    }


    /**
     *  Sets the cyclic event <code> Id </code> for this query. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckCyclicEventId(org.osid.id.Id cyclicEventId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTimeCheckCyclicEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimeCheckCyclicEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getTimeCheckCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsTimeCheckCyclicEventQuery() is false");
    }


    /**
     *  Matches any time check with a cyclic event. 
     *
     *  @param  match <code> true </code> to match any time checks with a 
     *          cyclic event, <code> false </code> to match checks with no 
     *          cyclic events 
     */

    @OSID @Override
    public void matchAnyTimeCheckCyclicEvent(boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearTimeCheckCyclicEventTerms() {
        return;
    }


    /**
     *  Sets the block <code> Id </code> for this query. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchHoldCheckBlockId(org.osid.id.Id blockId, boolean match) {
        return;
    }


    /**
     *  Clears the block <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldCheckBlockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldCheckBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldCheckBlockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getHoldCheckBlockQuery() {
        throw new org.osid.UnimplementedException("supportsHoldCheckBlockQuery() is false");
    }


    /**
     *  Matches any hold check with a block. 
     *
     *  @param  match <code> true </code> to match any hold checks with a 
     *          block, <code> false </code> to match checks with no blocks 
     */

    @OSID @Override
    public void matchAnyHoldCheckBlock(boolean match) {
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearHoldCheckBlockTerms() {
        return;
    }


    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> audirId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryCheckAuditId(org.osid.id.Id auditId, boolean match) {
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryCheckAuditIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryCheckAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryCheckAuditQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getInquiryCheckAuditQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryCheckAuditQuery() is false");
    }


    /**
     *  Matches any inquiry check with an audit. 
     *
     *  @param  match <code> true </code> to match any inqiiry checks with an 
     *          audit, <code> false </code> to match checks with no audits 
     */

    @OSID @Override
    public void matchAnyInquiryCheckAudit(boolean match) {
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearInquiryCheckAuditTerms() {
        return;
    }


    /**
     *  Sets the agenda <code> Id </code> for this query. 
     *
     *  @param  agendaId the agenda <code> Id </code> 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> agendaId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchProcessCheckAgendaId(org.osid.id.Id agendaId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the agenda <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessCheckAgendaIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgendaQuery </code> is available. 
     *
     *  @return <code> true </code> if an agenda query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsProcessCheckAgendaQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agenda. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the agenda query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessCheckBlockQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuery getProcessCheckAgendaQuery() {
        throw new org.osid.UnimplementedException("supportsProcessCheckAgendaQuery() is false");
    }


    /**
     *  Matches any process check with an agenda. 
     *
     *  @param match <code> true </code> to match any process checks
     *          with an agenda, <code> false </code> to match checks
     *          with no agendas
     */

    @OSID @Override
    public void matchAnyProcessCheckAgenda(boolean match) {
        return;
    }


    /**
     *  Clears the agenda query terms. 
     */

    @OSID @Override
    public void clearProcessCheckAgendaTerms() {
        return;
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match checks 
     *  assigned to engines. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given check query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a check implementing the requested record.
     *
     *  @param checkRecordType a check record type
     *  @return the check query record
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckQueryRecord getCheckQueryRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckQueryRecord record : this.records) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Adds a record to this check query. 
     *
     *  @param checkQueryRecord check query record
     *  @param checkRecordType check record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCheckQueryRecord(org.osid.rules.check.records.CheckQueryRecord checkQueryRecord, 
                                          org.osid.type.Type checkRecordType) {

        addRecordType(checkRecordType);
        nullarg(checkQueryRecord, "check query record");
        this.records.add(checkQueryRecord);        
        return;
    }
}
