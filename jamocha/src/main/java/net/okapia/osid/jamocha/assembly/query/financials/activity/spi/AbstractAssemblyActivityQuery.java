//
// AbstractAssemblyActivityQuery.java
//
//     An ActivityQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.financials.ActivityQuery,
               org.osid.financials.ActivityQueryInspector,
               org.osid.financials.ActivitySearchOrder {

    private final java.util.Collection<org.osid.financials.records.ActivityQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.ActivityQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.ActivitySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the organization <code> Id </code> for this query. 
     *
     *  @param  organizationId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getOrganizationIdColumn(), organizationId, match);
        return;
    }


    /**
     *  Clears the organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        getAssembler().clearTerms(getOrganizationIdColumn());
        return;
    }


    /**
     *  Gets the organization <code> Id </code> query terms. 
     *
     *  @return the organization <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (getAssembler().getIdTerms(getOrganizationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  organization. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOrganization(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOrganizationColumn(), style);
        return;
    }


    /**
     *  Gets the OrganizationId column name.
     *
     * @return the column name
     */

    protected String getOrganizationIdColumn() {
        return ("organization_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Matches an activity that has any organization. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          organization, <code> false </code> to match activities with no 
     *          organization 
     */

    @OSID @Override
    public void matchAnyOrganization(boolean match) {
        getAssembler().addIdWildcardTerm(getOrganizationColumn(), match);
        return;
    }


    /**
     *  Clears the organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        getAssembler().clearTerms(getOrganizationColumn());
        return;
    }


    /**
     *  Gets the organization query terms. 
     *
     *  @return the organization query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getOrganizationTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsOrganizationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getOrganizationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOrganizationSearchOrder() is false");
    }


    /**
     *  Gets the Organization column name.
     *
     * @return the column name
     */

    protected String getOrganizationColumn() {
        return ("organization");
    }


    /**
     *  Sets the supervisor <code> Id </code> for this query. 
     *
     *  @param  supervisorId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supervisorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupervisorId(org.osid.id.Id supervisorId, boolean match) {
        getAssembler().addIdTerm(getSupervisorIdColumn(), supervisorId, match);
        return;
    }


    /**
     *  Clears the supervisor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupervisorIdTerms() {
        getAssembler().clearTerms(getSupervisorIdColumn());
        return;
    }


    /**
     *  Gets the supervisor <code> Id </code> query terms. 
     *
     *  @return the supervisor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupervisorIdTerms() {
        return (getAssembler().getIdTerms(getSupervisorIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the supervisor. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupervisor(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupervisorColumn(), style);
        return;
    }


    /**
     *  Gets the SupervisorId column name.
     *
     * @return the column name
     */

    protected String getSupervisorIdColumn() {
        return ("supervisor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a supervisor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupervisorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a supervisor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupervisorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSupervisorQuery() {
        throw new org.osid.UnimplementedException("supportsSupervisorQuery() is false");
    }


    /**
     *  Matches an activity that has any supervisor. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          supervisor, <code> false </code> to match activities with no 
     *          supervisor 
     */

    @OSID @Override
    public void matchAnySupervisor(boolean match) {
        getAssembler().addIdWildcardTerm(getSupervisorColumn(), match);
        return;
    }


    /**
     *  Clears the supervisor terms. 
     */

    @OSID @Override
    public void clearSupervisorTerms() {
        getAssembler().clearTerms(getSupervisorColumn());
        return;
    }


    /**
     *  Gets the supervisor query terms. 
     *
     *  @return the supervisor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSupervisorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsSupervisorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupervisorSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSupervisorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSupervisorSearchOrder() is false");
    }


    /**
     *  Gets the Supervisor column name.
     *
     * @return the column name
     */

    protected String getSupervisorColumn() {
        return ("supervisor");
    }


    /**
     *  Matches an activity code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches an activity that has any code assigned. 
     *
     *  @param  match <code> true </code> to match activities with any code, 
     *          <code> false </code> to match activities with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCodeColumn(), match);
        return;
    }


    /**
     *  Clears the code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        getAssembler().clearTerms(getCodeColumn());
        return;
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (getAssembler().getStringTerms(getCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the code. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCodeColumn(), style);
        return;
    }


    /**
     *  Gets the Code column name.
     *
     * @return the column name
     */

    protected String getCodeColumn() {
        return ("code");
    }


    /**
     *  Tests if a <code> SummaryQuery </code> is available. 
     *
     *  @return <code> true </code> if a summery query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSummaryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a summary. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the summery query 
     *  @throws org.osid.UnimplementedException <code> supportsSummeryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.SummaryQuery getSummaryQuery() {
        throw new org.osid.UnimplementedException("supportsSummaryQuery() is false");
    }


    /**
     *  Clears the summary terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        getAssembler().clearTerms(getSummaryColumn());
        return;
    }


    /**
     *  Gets the summary query terms. 
     *
     *  @return the summary query terms 
     */

    @OSID @Override
    public org.osid.financials.SummaryQueryInspector[] getSummaryTerms() {
        return (new org.osid.financials.SummaryQueryInspector[0]);
    }


    /**
     *  Gets the Summary column name.
     *
     * @return the column name
     */

    protected String getSummaryColumn() {
        return ("summary");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activities 
     *  that have the specified activity as an ancestor. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorActivityId(org.osid.id.Id activityId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the ancestor activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorActivityIdTerms() {
        getAssembler().clearTerms(getAncestorActivityIdColumn());
        return;
    }


    /**
     *  Gets the ancestor activity <code> Id </code> query terms. 
     *
     *  @return the ancestor activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorActivityIdTerms() {
        return (getAssembler().getIdTerms(getAncestorActivityIdColumn()));
    }


    /**
     *  Gets the AncestorActivityId column name.
     *
     * @return the column name
     */

    protected String getAncestorActivityIdColumn() {
        return ("ancestor_activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getAncestorActivityQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorActivityQuery() is false");
    }


    /**
     *  Matches activities with any activity ancestor. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          ancestor, <code> false </code> to match root activities 
     */

    @OSID @Override
    public void matchAnyAncestorActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorActivityColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor activity query terms. 
     */

    @OSID @Override
    public void clearAncestorActivityTerms() {
        getAssembler().clearTerms(getAncestorActivityColumn());
        return;
    }


    /**
     *  Gets the ancestor activity query terms. 
     *
     *  @return the ancestor activity terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getAncestorActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the AncestorActivity column name.
     *
     * @return the column name
     */

    protected String getAncestorActivityColumn() {
        return ("ancestor_activity");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activities 
     *  that have the specified activity as a descendant. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantActivityId(org.osid.id.Id activityId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the descendant activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantActivityIdTerms() {
        getAssembler().clearTerms(getDescendantActivityIdColumn());
        return;
    }


    /**
     *  Gets the descendant activity <code> Id </code> query terms. 
     *
     *  @return the descendant activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantActivityIdTerms() {
        return (getAssembler().getIdTerms(getDescendantActivityIdColumn()));
    }


    /**
     *  Gets the DescendantActivityId column name.
     *
     * @return the column name
     */

    protected String getDescendantActivityIdColumn() {
        return ("descendant_activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getDescendantActivityQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantActivityQuery() is false");
    }


    /**
     *  Matches activities with any activity descendant. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          descendant, <code> false </code> to match leaf activities 
     */

    @OSID @Override
    public void matchAnyDescendantActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantActivityColumn(), match);
        return;
    }


    /**
     *  Clears the descendant activity query terms. 
     */

    @OSID @Override
    public void clearDescendantActivityTerms() {
        getAssembler().clearTerms(getDescendantActivityColumn());
        return;
    }


    /**
     *  Gets the descendant activity query terms. 
     *
     *  @return the descendant activity terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getDescendantActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the DescendantActivity column name.
     *
     * @return the column name
     */

    protected String getDescendantActivityColumn() {
        return ("descendant_activity");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match activities 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.financials.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivityQueryRecord getActivityQueryRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivityQueryInspectorRecord getActivityQueryInspectorRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivityQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityRecordType the activity record type
     *  @return the activity search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.ActivitySearchOrderRecord getActivitySearchOrderRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.ActivitySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityQueryRecord the activity query record
     *  @param activityQueryInspectorRecord the activity query inspector
     *         record
     *  @param activitySearchOrderRecord the activity search order record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityQueryRecord</code>,
     *          <code>activityQueryInspectorRecord</code>,
     *          <code>activitySearchOrderRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecords(org.osid.financials.records.ActivityQueryRecord activityQueryRecord, 
                                      org.osid.financials.records.ActivityQueryInspectorRecord activityQueryInspectorRecord, 
                                      org.osid.financials.records.ActivitySearchOrderRecord activitySearchOrderRecord, 
                                      org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);

        nullarg(activityQueryRecord, "activity query record");
        nullarg(activityQueryInspectorRecord, "activity query inspector record");
        nullarg(activitySearchOrderRecord, "activity search odrer record");

        this.queryRecords.add(activityQueryRecord);
        this.queryInspectorRecords.add(activityQueryInspectorRecord);
        this.searchOrderRecords.add(activitySearchOrderRecord);
        
        return;
    }
}
