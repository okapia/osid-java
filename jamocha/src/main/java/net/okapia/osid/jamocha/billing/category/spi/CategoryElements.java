//
// CategoryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CategoryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the CategoryElement Id.
     *
     *  @return the category element Id
     */

    public static org.osid.id.Id getCategoryEntityId() {
        return (makeEntityId("osid.billing.Category"));
    }


    /**
     *  Gets the EntryId element Id.
     *
     *  @return the EntryId element Id
     */

    public static org.osid.id.Id getEntryId() {
        return (makeQueryElementId("osid.billing.category.EntryId"));
    }


    /**
     *  Gets the Entry element Id.
     *
     *  @return the Entry element Id
     */

    public static org.osid.id.Id getEntry() {
        return (makeQueryElementId("osid.billing.category.Entry"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.billing.category.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.billing.category.Business"));
    }
}
