//
// AbstractAssessmentOfferedSearchOdrer.java
//
//     Defines an AssessmentOfferedSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssessmentOfferedSearchOrder}.
 */

public abstract class AbstractAssessmentOfferedSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.assessment.AssessmentOfferedSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment search order. 
     *
     *  @return an assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the level of 
     *  difficulty. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLevel(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLevelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getLevelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsLevelSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the sequential 
     *  flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemsSequential(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the shuffle 
     *  flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemsShuffled(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  deadline. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeadline(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for scores. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreSystem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreSystemSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for grades. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment offered. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment offered search order is available. 
     *
     *  @return <code> true </code> if an assessment offered search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment offered search order. 
     *
     *  @return a rubric assessment offered search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assessmentOfferedRecordType an assessment offered record type 
     *  @return {@code true} if the assessmentOfferedRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentOfferedRecordType) {
        for (org.osid.assessment.records.AssessmentOfferedSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assessmentOfferedRecordType the assessment offered record type 
     *  @return the assessment offered search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assessmentOfferedRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedSearchOrderRecord getAssessmentOfferedSearchOrderRecord(org.osid.type.Type assessmentOfferedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentOfferedSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentOfferedRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this assessment offered. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentOfferedRecord the assessment offered search odrer record
     *  @param assessmentOfferedRecordType assessment offered record type
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentOfferedRecord} or
     *          {@code assessmentOfferedRecordTypeassessmentOffered} is
     *          {@code null}
     */
            
    protected void addAssessmentOfferedRecord(org.osid.assessment.records.AssessmentOfferedSearchOrderRecord assessmentOfferedSearchOrderRecord, 
                                     org.osid.type.Type assessmentOfferedRecordType) {

        addRecordType(assessmentOfferedRecordType);
        this.records.add(assessmentOfferedSearchOrderRecord);
        
        return;
    }
}
