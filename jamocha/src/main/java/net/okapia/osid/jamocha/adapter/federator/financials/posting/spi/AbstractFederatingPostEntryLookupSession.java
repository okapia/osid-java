//
// AbstractFederatingPostEntryLookupSession.java
//
//     An abstract federating adapter for a PostEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PostEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPostEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.financials.posting.PostEntryLookupSession>
    implements org.osid.financials.posting.PostEntryLookupSession {

    private boolean parallel = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingPostEntryLookupSession</code>.
     */

    protected AbstractFederatingPostEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.financials.posting.PostEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>PostEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPostEntries() {
        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            if (session.canLookupPostEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>PostEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePostEntryView() {
        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            session.useComparativePostEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>PostEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPostEntryView() {
        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            session.usePlenaryPostEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include post entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>PostEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PostEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PostEntry</code> and
     *  retained for compatibility.
     *
     *  @param  postEntryId <code>Id</code> of the
     *          <code>PostEntry</code>
     *  @return the post entry
     *  @throws org.osid.NotFoundException <code>postEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntry getPostEntry(org.osid.id.Id postEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            try {
                return (session.getPostEntry(postEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(postEntryId + " not found");
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  postEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>PostEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  postEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByIds(org.osid.id.IdList postEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.financials.posting.postentry.MutablePostEntryList ret = new net.okapia.osid.jamocha.financials.posting.postentry.MutablePostEntryList();

        try (org.osid.id.IdList ids = postEntryIds) {
            while (ids.hasNext()) {
                ret.addPostEntry(getPostEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> which does not include
     *  post entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByGenusType(postEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> corresponding to the given
     *  post entry genus <code>Type</code> and include any additional
     *  post entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByParentGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByParentGenusType(postEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> containing the given
     *  post entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryRecordType a postEntry record type 
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByRecordType(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByRecordType(postEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given post.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  postId a post <code>Id /code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>postId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesForPost(postId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> in the given fiscal period.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByFiscalPeriod(fiscalPeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> posted within given date
     *  range inclusive.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByDate(org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> PostEntryList </code> for the given account.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByAccount(accountId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given account in a
     *  fiscal period.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code>
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>accountId</code> or
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndFiscalPeriod(org.osid.id.Id accountId,
                                                                                            org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByAccountAndFiscalPeriod(accountId, fiscalPeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  activityId an activity <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByActivity(activityId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity in a
     *  fiscal period.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  activityId an activity <code>Id</code>
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          or <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivityAndFiscalPeriod(org.osid.id.Id activityId,
                                                                                             org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByActivityAndFiscalPeriod(activityId, fiscalPeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given activity and
     *  account.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code>
     *  @param  activityId an activity <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          or <code>activityId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivity(org.osid.id.Id accountId,
                                                                                        org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByAccountAndActivity(accountId, activityId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PostEntryList</code> for the given account in a fiscal
     *  period.
     *
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account <code>Id</code>
     *  @param  activityId an activity <code>Id</code>
     *  @param  fiscalPeriodId a fiscal period <code>Id</code>
     *  @return the returned <code>PostEntry</code> list
     *  @throws org.osid.NullArgumentException <code>accountId</code>,
     *          <code>activityId</code>, or
     *          <code>fiscalPeriodId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivityAndFiscalPeriod(org.osid.id.Id accountId,
                                                                                                       org.osid.id.Id activityId,
                                                                                                       org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntriesByAccountAndActivityAndFiscalPeriod(accountId, activityId, fiscalPeriodId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>PostEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>PostEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList ret = getPostEntryList();

        for (org.osid.financials.posting.PostEntryLookupSession session : getSessions()) {
            ret.addPostEntryList(session.getPostEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.FederatingPostEntryList getPostEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.ParallelPostEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.posting.postentry.CompositePostEntryList());
        }
    }
}
