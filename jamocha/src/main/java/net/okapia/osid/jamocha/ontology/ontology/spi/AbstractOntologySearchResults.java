//
// AbstractOntologySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOntologySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ontology.OntologySearchResults {

    private org.osid.ontology.OntologyList ontologies;
    private final org.osid.ontology.OntologyQueryInspector inspector;
    private final java.util.Collection<org.osid.ontology.records.OntologySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOntologySearchResults.
     *
     *  @param ontologies the result set
     *  @param ontologyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>ontologies</code>
     *          or <code>ontologyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOntologySearchResults(org.osid.ontology.OntologyList ontologies,
                                            org.osid.ontology.OntologyQueryInspector ontologyQueryInspector) {
        nullarg(ontologies, "ontologies");
        nullarg(ontologyQueryInspector, "ontology query inspectpr");

        this.ontologies = ontologies;
        this.inspector = ontologyQueryInspector;

        return;
    }


    /**
     *  Gets the ontology list resulting from a search.
     *
     *  @return an ontology list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologies() {
        if (this.ontologies == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ontology.OntologyList ontologies = this.ontologies;
        this.ontologies = null;
	return (ontologies);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ontology.OntologyQueryInspector getOntologyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  ontology search record <code> Type. </code> This method must
     *  be used to retrieve an ontology implementing the requested
     *  record.
     *
     *  @param ontologySearchRecordType an ontology search 
     *         record type 
     *  @return the ontology search
     *  @throws org.osid.NullArgumentException
     *          <code>ontologySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(ontologySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologySearchResultsRecord getOntologySearchResultsRecord(org.osid.type.Type ontologySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ontology.records.OntologySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(ontologySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(ontologySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record ontology search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOntologyRecord(org.osid.ontology.records.OntologySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "ontology record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
