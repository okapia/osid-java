//
// AbstractAdapterAssessmentEntryLookupSession.java
//
//    An AssessmentEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AssessmentEntry lookup session adapter.
 */

public abstract class AbstractAdapterAssessmentEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {

    private final org.osid.course.chronicle.AssessmentEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentEntryLookupSession(org.osid.course.chronicle.AssessmentEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code AssessmentEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssessmentEntries() {
        return (this.session.canLookupAssessmentEntries());
    }


    /**
     *  A complete view of the {@code AssessmentEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentEntryView() {
        this.session.useComparativeAssessmentEntryView();
        return;
    }


    /**
     *  A complete view of the {@code AssessmentEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentEntryView() {
        this.session.usePlenaryAssessmentEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only assessment entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAssessmentEntryView() {
        this.session.useEffectiveAssessmentEntryView();
        return;
    }
    

    /**
     *  All assessment entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAssessmentEntryView() {
        this.session.useAnyEffectiveAssessmentEntryView();
        return;
    }

     
    /**
     *  Gets the {@code AssessmentEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AssessmentEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AssessmentEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param assessmentEntryId {@code Id} of the {@code AssessmentEntry}
     *  @return the assessment entry
     *  @throws org.osid.NotFoundException {@code assessmentEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assessmentEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntry getAssessmentEntry(org.osid.id.Id assessmentEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntry(assessmentEntryId));
    }


    /**
     *  Gets an {@code AssessmentEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AssessmentEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AssessmentEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByIds(org.osid.id.IdList assessmentEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesByIds(assessmentEntryIds));
    }


    /**
     *  Gets an {@code AssessmentEntryList} corresponding to the given
     *  assessment entry genus {@code Type} which does not include
     *  assessment entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryGenusType an assessmentEntry genus type 
     *  @return the returned {@code AssessmentEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByGenusType(org.osid.type.Type assessmentEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesByGenusType(assessmentEntryGenusType));
    }


    /**
     *  Gets an {@code AssessmentEntryList} corresponding to the given
     *  assessment entry genus {@code Type} and include any additional
     *  assessment entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryGenusType an assessmentEntry genus type 
     *  @return the returned {@code AssessmentEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByParentGenusType(org.osid.type.Type assessmentEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesByParentGenusType(assessmentEntryGenusType));
    }


    /**
     *  Gets an {@code AssessmentEntryList} containing the given
     *  assessment entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentEntryRecordType an assessmentEntry record type 
     *  @return the returned {@code AssessmentEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByRecordType(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesByRecordType(assessmentEntryRecordType));
    }


    /**
     *  Gets an {@code AssessmentEntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *  
     *  In active mode, assessment entries are returned that are currently
     *  active. In any status mode, active and inactive assessment entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code AssessmentEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of assessment entries corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForStudent(resourceId));
    }


    /**
     *  Gets a list of assessment entries corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of assessment entries corresponding to an
     *  assessment {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  assessmentId the {@code Id} of the assessment
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code assessmentId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForAssessment(assessmentId));
    }


    /**
     *  Gets a list of assessment entries corresponding to an assessment
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  assessmentId the {@code Id} of the assessment
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code assessmentId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForAssessmentOnDate(org.osid.id.Id assessmentId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForAssessmentOnDate(assessmentId, from, to));
    }


    /**
     *  Gets a list of assessment entries corresponding to student and assessment
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  assessment entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  assessmentId the {@code Id} of the assessment
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code assessmentId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentAndAssessment(org.osid.id.Id resourceId,
                                                                                                     org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForStudentAndAssessment(resourceId, assessmentId));
    }


    /**
     *  Gets a list of assessment entries corresponding to student and assessment
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible
     *  through this session.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective. In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @param  assessmentId the {@code Id} of the assessment
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code AssessmentEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code assessmentId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesForStudentAndAssessmentOnDate(org.osid.id.Id resourceId,
                                                                                                           org.osid.id.Id assessmentId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntriesForStudentAndAssessmentOnDate(resourceId, assessmentId, from, to));
    }


    /**
     *  Gets all {@code AssessmentEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessment entries or an error results. Otherwise, the returned list
     *  may contain only those assessment entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, assessment entries are returned that are currently
     *  effective.  In any effective mode, effective assessment entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code AssessmentEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentEntries());
    }
}
