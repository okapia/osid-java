//
// AbstractPostEntry.java
//
//     Defines a PostEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.postentry.spi;


/**
 *  Defines a <code>PostEntry</code> builder.
 */

public abstract class AbstractPostEntryBuilder<T extends AbstractPostEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.posting.postentry.PostEntryMiter postEntry;


    /**
     *  Constructs a new <code>AbstractPostEntryBuilder</code>.
     *
     *  @param postEntry the post entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPostEntryBuilder(net.okapia.osid.jamocha.builder.financials.posting.postentry.PostEntryMiter postEntry) {
        super(postEntry);
        this.postEntry = postEntry;
        return;
    }


    /**
     *  Builds the post entry.
     *
     *  @return the new post entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.posting.PostEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.posting.postentry.PostEntryValidator(getValidations())).validate(this.postEntry);
        return (new net.okapia.osid.jamocha.builder.financials.posting.postentry.ImmutablePostEntry(this.postEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the post entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.posting.postentry.PostEntryMiter getMiter() {
        return (this.postEntry);
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    public T post(org.osid.financials.posting.Post post) {
        getMiter().setPost(post);
        return (self());
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    public T account(org.osid.financials.Account account) {
        getMiter().setAccount(account);
        return (self());
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T activity(org.osid.financials.Activity activity) {
        getMiter().setActivity(activity);
        return (self());
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T amount(org.osid.financials.Currency amount) {
        getMiter().setAmount(amount);
        return (self());
    }


    /**
     *  Sets the debit flag.
     *
     *  @return the builder
     */

    public T debit() {
        getMiter().setDebit(true);
        return (self());
    }


    /**
     *  Unsets the debit flag.
     *
     *  @return the builder
     */

    public T credit() {
        getMiter().setDebit(false);
        return (self());
    }


    /**
     *  Adds a PostEntry record.
     *
     *  @param record a post entry record
     *  @param recordType the type of post entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.posting.records.PostEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addPostEntryRecord(record, recordType);
        return (self());
    }
}       


