//
// AbstractAdapterCredentialLookupSession.java
//
//    A Credential lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Credential lookup session adapter.
 */

public abstract class AbstractAdapterCredentialLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.program.CredentialLookupSession {

    private final org.osid.course.program.CredentialLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCredentialLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCredentialLookupSession(org.osid.course.program.CredentialLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Credential} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCredentials() {
        return (this.session.canLookupCredentials());
    }


    /**
     *  A complete view of the {@code Credential} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCredentialView() {
        this.session.useComparativeCredentialView();
        return;
    }


    /**
     *  A complete view of the {@code Credential} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCredentialView() {
        this.session.usePlenaryCredentialView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credentials in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the {@code Credential} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Credential} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Credential} and
     *  retained for compatibility.
     *
     *  @param credentialId {@code Id} of the {@code Credential}
     *  @return the credential
     *  @throws org.osid.NotFoundException {@code credentialId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code credentialId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential(org.osid.id.Id credentialId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredential(credentialId));
    }


    /**
     *  Gets a {@code CredentialList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credentials specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Credentials} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  credentialIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Credential} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code credentialIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByIds(org.osid.id.IdList credentialIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialsByIds(credentialIds));
    }


    /**
     *  Gets a {@code CredentialList} corresponding to the given
     *  credential genus {@code Type} which does not include
     *  credentials of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialGenusType a credential genus type 
     *  @return the returned {@code Credential} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByGenusType(org.osid.type.Type credentialGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialsByGenusType(credentialGenusType));
    }


    /**
     *  Gets a {@code CredentialList} corresponding to the given
     *  credential genus {@code Type} and include any additional
     *  credentials with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialGenusType a credential genus type 
     *  @return the returned {@code Credential} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByParentGenusType(org.osid.type.Type credentialGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialsByParentGenusType(credentialGenusType));
    }


    /**
     *  Gets a {@code CredentialList} containing the given
     *  credential record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  credentialRecordType a credential record type 
     *  @return the returned {@code Credential} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentialsByRecordType(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialsByRecordType(credentialRecordType));
    }


    /**
     *  Gets all {@code Credentials}. 
     *
     *  In plenary mode, the returned list contains all known
     *  credentials or an error results. Otherwise, the returned list
     *  may contain only those credentials that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Credentials} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentials());
    }
}
