//
// AbstractMapSequenceRuleEnablerLookupSession
//
//    A simple framework for providing a SequenceRuleEnabler lookup service
//    backed by a fixed collection of sequence rule enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a SequenceRuleEnabler lookup service backed by a
 *  fixed collection of sequence rule enablers. The sequence rule enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SequenceRuleEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSequenceRuleEnablerLookupSession
    extends net.okapia.osid.jamocha.assessment.authoring.spi.AbstractSequenceRuleEnablerLookupSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.authoring.SequenceRuleEnabler>());


    /**
     *  Makes a <code>SequenceRuleEnabler</code> available in this session.
     *
     *  @param  sequenceRuleEnabler a sequence rule enabler
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnabler<code>
     *          is <code>null</code>
     */

    protected void putSequenceRuleEnabler(org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler) {
        this.sequenceRuleEnablers.put(sequenceRuleEnabler.getId(), sequenceRuleEnabler);
        return;
    }


    /**
     *  Makes an array of sequence rule enablers available in this session.
     *
     *  @param  sequenceRuleEnablers an array of sequence rule enablers
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablers<code>
     *          is <code>null</code>
     */

    protected void putSequenceRuleEnablers(org.osid.assessment.authoring.SequenceRuleEnabler[] sequenceRuleEnablers) {
        putSequenceRuleEnablers(java.util.Arrays.asList(sequenceRuleEnablers));
        return;
    }


    /**
     *  Makes a collection of sequence rule enablers available in this session.
     *
     *  @param  sequenceRuleEnablers a collection of sequence rule enablers
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablers<code>
     *          is <code>null</code>
     */

    protected void putSequenceRuleEnablers(java.util.Collection<? extends org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablers) {
        for (org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler : sequenceRuleEnablers) {
            this.sequenceRuleEnablers.put(sequenceRuleEnabler.getId(), sequenceRuleEnabler);
        }

        return;
    }


    /**
     *  Removes a SequenceRuleEnabler from this session.
     *
     *  @param  sequenceRuleEnablerId the <code>Id</code> of the sequence rule enabler
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId) {
        this.sequenceRuleEnablers.remove(sequenceRuleEnablerId);
        return;
    }


    /**
     *  Gets the <code>SequenceRuleEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  sequenceRuleEnablerId <code>Id</code> of the <code>SequenceRuleEnabler</code>
     *  @return the sequenceRuleEnabler
     *  @throws org.osid.NotFoundException <code>sequenceRuleEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnabler getSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler = this.sequenceRuleEnablers.get(sequenceRuleEnablerId);
        if (sequenceRuleEnabler == null) {
            throw new org.osid.NotFoundException("sequenceRuleEnabler not found: " + sequenceRuleEnablerId);
        }

        return (sequenceRuleEnabler);
    }


    /**
     *  Gets all <code>SequenceRuleEnablers</code>. In plenary mode, the returned
     *  list contains all known sequenceRuleEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  sequenceRuleEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>SequenceRuleEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.ArraySequenceRuleEnablerList(this.sequenceRuleEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.sequenceRuleEnablers.clear();
        super.close();
        return;
    }
}
