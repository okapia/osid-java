//
// AbstractProvisionableLookupSession.java
//
//    A starter implementation framework for providing a Provisionable
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Provisionable
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProvisionables(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProvisionableLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.ProvisionableLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Provisionable</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProvisionables() {
        return (true);
    }


    /**
     *  A complete view of the <code>Provisionable</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProvisionableView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Provisionable</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProvisionableView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisionables in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only provisionables whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveProvisionableView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All provisionables of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProvisionableView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Provisionable</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Provisionable</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Provisionable</code> and
     *  retained for compatibility.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableId <code>Id</code> of the
     *          <code>Provisionable</code>
     *  @return the provisionable
     *  @throws org.osid.NotFoundException <code>provisionableId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.ProvisionableList provisionables = getProvisionables()) {
            while (provisionables.hasNext()) {
                org.osid.provisioning.Provisionable provisionable = provisionables.getNextProvisionable();
                if (provisionable.getId().equals(provisionableId)) {
                    return (provisionable);
                }
            }
        } 

        throw new org.osid.NotFoundException(provisionableId + " not found");
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisionables specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Provisionables</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, provisionables are returned that are currently effective.
     *  In any effective mode, effective provisionables and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProvisionables()</code>.
     *
     *  @param  provisionableIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByIds(org.osid.id.IdList provisionableIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.Provisionable> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = provisionableIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProvisionable(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("provisionable " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.provisionable.LinkedProvisionableList(ret));
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the given
     *  provisionable genus <code>Type</code> which does not include
     *  provisionables of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently effective.
     *  In any effective mode, effective provisionables and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProvisionables()</code>.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableGenusFilterList(getProvisionables(), provisionableGenusType));
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the
     *  given provisionable genus <code>Type</code> and include any
     *  additional provisionables with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In e mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables
     *  and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProvisionables()</code>.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByParentGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProvisionablesByGenusType(provisionableGenusType));
    }


    /**
     *  Gets a <code>ProvisionableList</code> containing the given
     *  provisionable record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProvisionables()</code>.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByRecordType(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableRecordFilterList(getProvisionables(), provisionableRecordType));
    }


    /**
     *  Gets a <code>ProvisionableList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *  
     *  In active mode, provisionables are returned that are currently
     *  active. In any status mode, active and inactive provisionables
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Provisionable</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesOnDate(org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.TemporalProvisionableFilterList(getProvisionables(), from, to));
    }
        

    /**
     *  Gets a list of provisionables corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionableList getProvisionablesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableFilterList(new ResourceFilter(resourceId), getProvisionables()));
    }


    /**
     *  Gets a list of provisionables corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.TemporalProvisionableFilterList(getProvisionablesForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>poolId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionableList getProvisionablesForPool(org.osid.id.Id poolId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableFilterList(new PoolFilter(poolId), getProvisionables()));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>poolId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForPoolOnDate(org.osid.id.Id poolId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.TemporalProvisionableFilterList(getProvisionablesForPool(poolId), from, to));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and pool
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  poolId the <code>Id</code> of the pool
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>poolId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPool(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id poolId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableFilterList(new PoolFilter(poolId), getProvisionablesForResource(resourceId)));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and pool
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>poolId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPoolOnDate(org.osid.id.Id resourceId,
                                                                                             org.osid.id.Id poolId,
                                                                                             org.osid.calendaring.DateTime from,
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.TemporalProvisionableFilterList(getProvisionablesForResourceAndPool(resourceId, poolId), from, to));
    }


    /**
     *  Gets all <code>Provisionables</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @return a list of <code>Provisionables</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.ProvisionableList getProvisionables()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the provisionable list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of provisionables
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.ProvisionableList filterProvisionablesOnViews(org.osid.provisioning.ProvisionableList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.ProvisionableList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.EffectiveProvisionableFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ProvisionableFilterList to filter the 
         *  provisionable list based on resource.
         *
         *  @param provisionable the provisionable
         *  @return <code>true</code> to pass the provisionable,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provisionable provisionable) {
            return (provisionable.getResourceId().equals(this.resourceId));
        }
    }


    public static class PoolFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provisionable.ProvisionableFilter {
         
        private final org.osid.id.Id poolId;
         
         
        /**
         *  Constructs a new <code>PoolFilter</code>.
         *
         *  @param poolId the pool to filter
         *  @throws org.osid.NullArgumentException
         *          <code>poolId</code> is <code>null</code>
         */
        
        public PoolFilter(org.osid.id.Id poolId) {
            nullarg(poolId, "pool Id");
            this.poolId = poolId;
            return;
        }

         
        /**
         *  Used by the ProvisionableFilterList to filter the 
         *  provisionable list based on pool.
         *
         *  @param provisionable the provisionable
         *  @return <code>true</code> to pass the provisionable,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provisionable provisionable) {
            return (provisionable.getPoolId().equals(this.poolId));
        }
    }
}
