//
// AbstractFloor.java
//
//     Defines a Floor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.floor.spi;


/**
 *  Defines a <code>Floor</code> builder.
 */

public abstract class AbstractFloorBuilder<T extends AbstractFloorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.floor.FloorMiter floor;


    /**
     *  Constructs a new <code>AbstractFloorBuilder</code>.
     *
     *  @param floor the floor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractFloorBuilder(net.okapia.osid.jamocha.builder.room.floor.FloorMiter floor) {
        super(floor);
        this.floor = floor;
        return;
    }


    /**
     *  Builds the floor.
     *
     *  @return the new floor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.Floor build() {
        (new net.okapia.osid.jamocha.builder.validator.room.floor.FloorValidator(getValidations())).validate(this.floor);
        return (new net.okapia.osid.jamocha.builder.room.floor.ImmutableFloor(this.floor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the floor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.floor.FloorMiter getMiter() {
        return (this.floor);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>building</code> is <code>null</code>
     */

    public T building(org.osid.room.Building building) {
        getMiter().setBuilding(building);
        return (self());
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T number(String number) {
        getMiter().setNumber(number);
        return (self());
    }


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    public T grossArea(java.math.BigDecimal area) {
        getMiter().setGrossArea(area);
        return (self());
    }


    /**
     *  Adds a Floor record.
     *
     *  @param record a floor record
     *  @param recordType the type of floor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.records.FloorRecord record, org.osid.type.Type recordType) {
        getMiter().addFloorRecord(record, recordType);
        return (self());
    }
}       


