//
// AbstractImmutableKey.java
//
//     Wraps a mutable Key to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Key</code> to hide modifiers. This
 *  wrapper provides an immutized Key from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying key whose state changes are visible.
 */

public abstract class AbstractImmutableKey
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.authentication.keys.Key {

    private final org.osid.authentication.keys.Key key;


    /**
     *  Constructs a new <code>AbstractImmutableKey</code>.
     *
     *  @param key the key to immutablize
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableKey(org.osid.authentication.keys.Key key) {
        super(key);
        this.key = key;
        return;
    }


    /**
     *  Gets the <code> AgentId </code> corresponding to this key. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.key.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> corresponding to this key. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.key.getAgent());
    }


    /**
     *  Gets the key record corresponding to the given <code> Key </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> keyRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(keyRecordType) </code> is <code> true </code> . 
     *
     *  @param  keyRecordType a key record type 
     *  @return the key record 
     *  @throws org.osid.NullArgumentException <code> keyRecordType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(keyRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyRecord getKeyRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        return (this.key.getKeyRecord(keyRecordType));
    }
}

