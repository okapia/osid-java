//
// AbstractEntry.java
//
//     Defines an Entry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.entry.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Entry</code>.
 */

public abstract class AbstractEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.billing.Entry {

    private org.osid.billing.Customer customer;
    private org.osid.billing.Item item;
    private org.osid.billing.Period period;
    private long quantity;
    private org.osid.financials.Currency amount;
    private boolean debit = true;
    
    private final java.util.Collection<org.osid.billing.records.EntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the customer <code> Id </code> associated with this entry. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.customer.getId());
    }


    /**
     *  Gets the customer associated with this entry. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Customer getCustomer()
        throws org.osid.OperationFailedException {

        return (this.customer);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected void setCustomer(org.osid.billing.Customer customer) {
        nullarg(customer, "customer");
        this.customer = customer;
        return;
    }


    /**
     *  Gets the item <code> Id </code> associated with this entry. 
     *
     *  @return the item <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.item.getId());
    }


    /**
     *  Gets the item associated with this entry. 
     *
     *  @return the item 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Item getItem()
        throws org.osid.OperationFailedException {

        return (this.item);
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    protected void setItem(org.osid.billing.Item item) {
        nullarg(item, "item");
        this.item = item;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Period </code> of this 
     *  offering. 
     *
     *  @return the <code> Period </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPeriodId() {
        return (this.period.getId());
    }


    /**
     *  Gets the <code> Period </code> of this offering. 
     *
     *  @return the period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod()
        throws org.osid.OperationFailedException {

        return (this.period);
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setPeriod(org.osid.billing.Period period) {
        nullarg(period, "billing period");
        this.period = period;
        return;
    }


    /**
     *  Gets the quantity of the item. 
     *
     *  @return the quantity 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    protected void setQuantity(long quantity) {
        cardinalarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Gets the amount of this entry. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this item amount is a debit,
     *          <code> false </code> if it is a credit
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.debit);
    }


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this item amount is a
     *         debit, <code> false </code> if it is a credit
     */

    protected void setDebit(boolean debit) {
        this.debit = debit;
        return;
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.billing.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Entry</code>
     *  record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryRecord the entry record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecord</code> or
     *          <code>entryRecordType</code> is <code>null</code>
     */
            
    protected void addEntryRecord(org.osid.billing.records.EntryRecord entryRecord, 
                                  org.osid.type.Type entryRecordType) {

        nullarg(entryRecord, "entry record");
        addRecordType(entryRecordType);
        this.records.add(entryRecord);
        
        return;
    }
}
