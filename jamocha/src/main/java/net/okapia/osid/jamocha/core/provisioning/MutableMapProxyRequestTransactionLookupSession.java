//
// MutableMapProxyRequestTransactionLookupSession
//
//    Implements a RequestTransaction lookup service backed by a collection of
//    requestTransactions that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a RequestTransaction lookup service backed by a collection of
 *  requestTransactions. The requestTransactions are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of request transactions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyRequestTransactionLookupSession}
     *  with no request transactions.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRequestTransactionLookupSession} with a
     *  single request transaction.
     *
     *  @param distributor the distributor
     *  @param requestTransaction a request transaction
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code requestTransaction}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.RequestTransaction requestTransaction, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRequestTransactionLookupSession} using an
     *  array of request transactions.
     *
     *  @param distributor the distributor
     *  @param requestTransactions an array of request transactions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code requestTransactions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.RequestTransaction[] requestTransactions, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRequestTransactionLookupSession} using a
     *  collection of request transactions.
     *
     *  @param distributor the distributor
     *  @param requestTransactions a collection of request transactions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code requestTransactions}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                                java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions,
                                                org.osid.proxy.Proxy proxy) {
   
        this(distributor, proxy);
        setSessionProxy(proxy);
        putRequestTransactions(requestTransactions);
        return;
    }

    
    /**
     *  Makes a {@code RequestTransaction} available in this session.
     *
     *  @param requestTransaction an request transaction
     *  @throws org.osid.NullArgumentException {@code requestTransaction{@code 
     *          is {@code null}
     */

    @Override
    public void putRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        super.putRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Makes an array of requestTransactions available in this session.
     *
     *  @param requestTransactions an array of request transactions
     *  @throws org.osid.NullArgumentException {@code requestTransactions{@code 
     *          is {@code null}
     */

    @Override
    public void putRequestTransactions(org.osid.provisioning.RequestTransaction[] requestTransactions) {
        super.putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Makes collection of request transactions available in this session.
     *
     *  @param requestTransactions
     *  @throws org.osid.NullArgumentException {@code requestTransaction{@code 
     *          is {@code null}
     */

    @Override
    public void putRequestTransactions(java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions) {
        super.putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Removes a RequestTransaction from this session.
     *
     *  @param requestTransactionId the {@code Id} of the request transaction
     *  @throws org.osid.NullArgumentException {@code requestTransactionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRequestTransaction(org.osid.id.Id requestTransactionId) {
        super.removeRequestTransaction(requestTransactionId);
        return;
    }    
}
