//
// AbstractDictionaryManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;

import net.okapia.osid.torrefacto.collect.Collections;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractDictionaryManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.dictionary.DictionaryManager,
               org.osid.dictionary.DictionaryProxyManager {

    private final MultiMap<org.osid.type.Type, org.osid.type.Type> entryTypes  = Collections.synchronizedMultiMap(new TypeMultiHashMap<org.osid.type.Type>());
    private final Types entryRecordTypes                   = new TypeRefSet();
    private final Types entrySearchRecordTypes             = new TypeRefSet();

    private final Types dictionaryRecordTypes              = new TypeRefSet();
    private final Types dictionarySearchRecordTypes        = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractDictionaryManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractDictionaryManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dictionary federation is exposed. Federation is exposed 
     *  when a specific dictionary may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dictionaries appears as a single dictionary. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if retrieving dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry retrieval is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryRetrieval() {
        return (false);
    }


    /**
     *  Tests if looking up dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching dictionary entries are supported. 
     *
     *  @return <code> true </code> if entry search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (false);
    }


    /**
     *  Tests if a dictionary entry administrative service is supported. 
     *
     *  @return <code> true </code> if dictionary entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if a dictionary entry notification service is supported. 
     *
     *  @return <code> true </code> if dictionary entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (false);
    }


    /**
     *  Tests if retrieving mappings of entry and dictionarys is supported. 
     *
     *  @return <code> true </code> if entry dictionary mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryDictionary() {
        return (false);
    }


    /**
     *  Tests if managing mappings of entry and dictionarys is supported. 
     *
     *  @return <code> true </code> if entry dictionary assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryDictionaryAssignment() {
        return (false);
    }


    /**
     *  Tests if entry smart dictionarys are available. 
     *
     *  @return <code> true </code> if entry smart dictionarys are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartDictionary() {
        return (false);
    }


    /**
     *  Tests if a dictionary lookup service is supported. 
     *
     *  @return <code> true </code> if dictionary lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryLookup() {
        return (false);
    }


    /**
     *  Tests if a dictionary query service is supported. 
     *
     *  @return <code> true </code> if dictionary query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryQuery() {
        return (false);
    }


    /**
     *  Tests if a dictionary search service is supported. 
     *
     *  @return <code> true </code> if dictionary search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionarySearch() {
        return (false);
    }


    /**
     *  Tests if a dictionary administrative service is supported. 
     *
     *  @return <code> true </code> if dictionary administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryAdmin() {
        return (false);
    }


    /**
     *  Tests if a dictionary notification service is supported. 
     *
     *  @return <code> true </code> if dictionary notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryNotification() {
        return (false);
    }


    /**
     *  Tests if a dictionary hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if dictionary hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryHierachyTraversal() {
        return (false);
    }


    /**
     *  Tests if a dictionary hierarchy design service is supported. 
     *
     *  @return <code> true </code> if dictionary hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryHierachyDesign() {
        return (false);
    }


    /**
     *  Tests if a dictionary batch service is supported. 
     *
     *  @return <code> true </code> if dictionary batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Entry </code> key types. 
     *
     *  @return a list containing the supported <code> Entry </code> key types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryKeyTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryTypes.keySet()));
    }


    /**
     *  Tests if the given <code> Entry </code> key type is supported. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryKeyType(org.osid.type.Type entryKeyType) {
        return (this.entryTypes.containsKey(entryKeyType));
    }


    /**
     *  Gets the supported <code> Entry </code> value types. 
     *
     *  @return a list containing the supported <code> Entry </code> value 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryValueTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryTypes.values()));
    }


    /**
     *  Tests if the given <code> Entry </code> value type is supported. 
     *
     *  @param  entryValueType a <code> Type </code> indicating an <code> 
     *          Entry </code> value type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryValueType(org.osid.type.Type entryValueType) {
        return (this.entryTypes.containsValue(entryValueType));
    }


    /**
     *  Adds support for an entry key/value type.
     *
     *  @param entryKeyType an entry key type
     *  @param entryValueType an entry value type
     *  @throws org.osid.NullArgumentException
     *          <code>entryKeyType</code> or
     *          <code>entryValueType</code> is <code>null</code>
     */

     protected void addEntryValueType(org.osid.type.Type entryKeyType, org.osid.type.Type entryValueType) {
        this.entryTypes.put(entryKeyType, entryValueType);
        return;
    }


    /**
     *  Removes support for an entry key/value type.
     *
     *  @param entryKeyType an entry key type
     *  @param entryValueType an entry value type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>entryKeyType</code> or
     *          <code>entryValueType</code> is <code>null</code>
     */

    protected void removeEntryValueType(org.osid.type.Type entryKeyType, org.osid.type.Type entryValueType) {
        this.entryTypes.remove(entryKeyType, entryValueType);
        return;
    }


    /**
     *  Gets the list of value types supported for the given key type. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @return a list of value types 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryValueTypesForKeyType(org.osid.type.Type entryKeyType) {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryTypes.get(entryKeyType)));
    }


    /**
     *  Tests if the given <code> Entry </code> key and value types are 
     *  supported. 
     *
     *  @param  entryKeyType a <code> Type </code> indicating an <code> Entry 
     *          </code> key type 
     *  @param  entryValueType a <code> Type </code> indicating an <code> 
     *          Entry </code> value type 
     *  @return <code> true </code> if the given Types are supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryKeyType </code> or 
     *          <code> entryValueType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryTypes(org.osid.type.Type entryKeyType, org.osid.type.Type entryValueType) {
        return (this.entryTypes.containsKeyValue(entryKeyType, entryValueType));
    }


    /**
     *  Gets the supported <code> Entry </code> record types. 
     *
     *  @return a list containing the supported <code> Entry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> record type is supported. 
     *
     *  @param  entryRecordType a <code> Type </code> indicating an <code> 
     *          Entry </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (this.entryRecordTypes.contains(entryRecordType));
    }


    /**
     *  Adds support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void addEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.add(entryRecordType);
        return;
    }


    /**
     *  Removes support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void removeEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.remove(entryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Entry </code> search record types. 
     *
     *  @return a list containing the supported <code> Entry </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Entry </code> search record type is 
     *  supported. 
     *
     *  @param  entrySearchRecordType a <code> Type </code> indicating an 
     *          <code> Entry </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (this.entrySearchRecordTypes.contains(entrySearchRecordType));
    }


    /**
     *  Adds support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void addEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.add(entrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void removeEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.remove(entrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Dictionary </code> record types. 
     *
     *  @return a list containing the supported <code> Dictionary </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDictionaryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.dictionaryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Dictionary </code> record type is supported. 
     *
     *  @param  dictionaryRecordType a <code> Type </code> indicating a <code> 
     *          Dictionary </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dictionaryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDictionaryRecordType(org.osid.type.Type dictionaryRecordType) {
        return (this.dictionaryRecordTypes.contains(dictionaryRecordType));
    }


    /**
     *  Adds support for a dictionary record type.
     *
     *  @param dictionaryRecordType a dictionary record type
     *  @throws org.osid.NullArgumentException
     *  <code>dictionaryRecordType</code> is <code>null</code>
     */

    protected void addDictionaryRecordType(org.osid.type.Type dictionaryRecordType) {
        this.dictionaryRecordTypes.add(dictionaryRecordType);
        return;
    }


    /**
     *  Removes support for a dictionary record type.
     *
     *  @param dictionaryRecordType a dictionary record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>dictionaryRecordType</code> is <code>null</code>
     */

    protected void removeDictionaryRecordType(org.osid.type.Type dictionaryRecordType) {
        this.dictionaryRecordTypes.remove(dictionaryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Dictionary </code> search record types. 
     *
     *  @return a list containing the supported <code> Dictionary </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDictionarySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.dictionarySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Dictionary </code> search record type is 
     *  supported. 
     *
     *  @param  dictionarySearchRecordType a <code> Type </code> indicating a 
     *          <code> Dictionary </code> search record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          dictionarySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDictionarySearchRecordType(org.osid.type.Type dictionarySearchRecordType) {
        return (this.dictionarySearchRecordTypes.contains(dictionarySearchRecordType));
    }


    /**
     *  Adds support for a dictionary search record type.
     *
     *  @param dictionarySearchRecordType a dictionary search record type
     *  @throws org.osid.NullArgumentException
     *  <code>dictionarySearchRecordType</code> is <code>null</code>
     */

    protected void addDictionarySearchRecordType(org.osid.type.Type dictionarySearchRecordType) {
        this.dictionarySearchRecordTypes.add(dictionarySearchRecordType);
        return;
    }


    /**
     *  Removes support for a dictionary search record type.
     *
     *  @param dictionarySearchRecordType a dictionary search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>dictionarySearchRecordType</code> is <code>null</code>
     */

    protected void removeDictionarySearchRecordType(org.osid.type.Type dictionarySearchRecordType) {
        this.dictionarySearchRecordTypes.remove(dictionarySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries. 
     *
     *  @return the new <code> EntryRetrievalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryRetrievalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryRetrievalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryRetrievalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries for the specified <code> Dictionary. </code> 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntryRetrievalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryRetrievalSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryRetrievalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryRetrievalSession getEntryRetrievalSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryRetrievalSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to retrieve dictionary 
     *  entries. 
     *
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionary entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionary entries 
     *  for the specified <code> Dictionary. </code> 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryLookupSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionary entries 
     *  for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryLookupSession getEntryLookupSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryLookupSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries. 
     *
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries 
     *  for the specified <code> Dictionary. </code> 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryQuerySessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionary entries 
     *  for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQuerySession getEntryQuerySessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryQuerySessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search dictionary entries. 
     *
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search among dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search dictionary entries 
     *  for the specified <code> Dictionary. </code> 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntrySearchSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search among dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySearchSession getEntrySearchSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntrySearchSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries. 
     *
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries for the specified <code> Dictionary. </code> 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> os <code> supportsVisibleFederration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSessionForDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryAdminSessionForDictionary not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionary 
     *  entries for the specified <code> Dictionary </code> . 
     *
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> os <code> supportsVisibleFederration() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryAdminSession getEntryAdminSessionForDictionary(org.osid.id.Id dictionaryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryAdminSessionForDictionary not implemented");
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes within a default <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSession(org.osid.dictionary.EntryReceiver entryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes within a default <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSession(org.osid.dictionary.EntryReceiver entryReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes for a specified <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> dictionaryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSessionForDictionary(org.osid.dictionary.EntryReceiver entryReceiver, 
                                                                                                 org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryNotificationSessionForDictionary not implemented");
    }


    /**
     *  Gets an <code> EntryNotificationSession </code> which is responsible 
     *  for subscribing to entry changes for a specified <code> Dictionary. 
     *  </code> 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  dictionaryId the <code> Id </code> of the <code> Dictionary 
     *          </code> to use 
     *  @param  proxy a proxy 
     *  @return an <code> EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Dictionary </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver, 
     *          dictionaryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryNotificationSession getEntryNotificationSessionForDictionary(org.osid.dictionary.EntryReceiver entryReceiver, 
                                                                                                 org.osid.id.Id dictionaryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryNotificationSessionForDictionary not implemented");
    }


    /**
     *  Gets the session for retrieving entry to dictionary mappings. 
     *
     *  @return a <code> EntryDictionarySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionarySession getEntryDictionarySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryDictionarySession not implemented");
    }


    /**
     *  Gets the session for retrieving entry to dictionary mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> EntryDictionarySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionarySession getEntryDictionarySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryDictionarySession not implemented");
    }


    /**
     *  Gets the session for assigning entry to dictionary mappings. 
     *
     *  @return a <code> EntryDictionaryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionaryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionaryAssignmentSession getEntryDictionaryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntryDictionaryAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning entry to dictionary mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> EntryDictionaryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryDictionaryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntryDictionaryAssignmentSession getEntryDictionaryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntryDictionaryAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic entry dictionarys. 
     *
     *  @param  dictionaryId the <code> Id </code> of the dictionary 
     *  @return a <code> EntrySmartDictionarySession </code> 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySmartDictionarySession getEntrySmartDictionarySession(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getEntrySmartDictionarySession not implemented");
    }


    /**
     *  Gets the session for managing dynamic entry dictionarys. 
     *
     *  @param  dictionaryId the <code> Id </code> of the dictionary 
     *  @param  proxy a proxy 
     *  @return a <code> EntrySmartDictionarySession </code> 
     *  @throws org.osid.NotFoundException <code> dictionaryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> dictionaryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartDictionary() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.EntrySmartDictionarySession getEntrySmartDictionarySession(org.osid.id.Id dictionaryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getEntrySmartDictionarySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionaries. 
     *
     *  @return a <code> DictionaryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryLookupSession getDictionaryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to lookup dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryLookupSession getDictionaryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionaries. 
     *
     *  @return a <code> DictionaryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuerySession getDictionaryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to query dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQuerySession getDictionaryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search dictionaries. 
     *
     *  @return a <code> DictionarySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionarySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionarySearchSession getDictionarySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionarySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to search for dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionarySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionarySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionarySearchSession getDictionarySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionarySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionaries 
     *
     *  @return a <code> DictionaryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryAdminSession getDictionaryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to administer dictionaries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryAdminSession getDictionaryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to receive notifications of 
     *  dictionary changes. 
     *
     *  @param  dictionaryReceiver the notification callback 
     *  @return a <code> DictionaryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryNotificationSession getDictionaryNotificationSession(org.osid.dictionary.DictionaryReceiver dictionaryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> used to subscribe to notifications 
     *  of new, updated or deleted dictionaries dictionaries. 
     *
     *  @param  dictionaryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dictionaryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryNotificationSession getDictionaryNotificationSession(org.osid.dictionary.DictionaryReceiver dictionaryReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryNotificationSession not implemented");
    }


    /**
     *  Gets hierarchy service for traversing the <code> Dictionary </code> 
     *  hierarchy. A parent includes all the dictionary entries of its 
     *  children. 
     *
     *  @return a Dictionary <code> HierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchyTraversal() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchySession getDictionaryHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryHierarchySession not implemented");
    }


    /**
     *  Gets the hierarchy traversing the <code> Dictionary </code> hierarchy. 
     *  The parent includes all dictionary elements of its children. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchySession getDictionaryHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryHierarchySession not implemented");
    }


    /**
     *  Gets hierarchy service for structuring <code> Dictionary </code> 
     *  objects. 
     *
     *  @return a <code> DictionaryHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchyDesignSession getDictionaryHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the hierarchy managing the <code> Dictionary </code> hierarchy. 
     *  The parent includes all dictionary elements of its children. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DictionaryHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryHierarchyDesignSession getDictionaryHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> DictionaryBatchManager. </code> 
     *
     *  @return a <code> DictionaryBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.DictionaryBatchManager getDictionaryBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryManager.getDictionaryBatchManager not implemented");
    }


    /**
     *  Gets a <code> DictionaryBatchProxyManager. </code> 
     *
     *  @return a <code> DictionaryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.batch.DictionaryBatchProxyManager getDictionaryBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.dictionary.DictionaryProxyManager.getDictionaryBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();

        this.entryTypes.clear();

        this.entryRecordTypes.clear();
        this.entryRecordTypes.clear();

        this.entrySearchRecordTypes.clear();
        this.entrySearchRecordTypes.clear();

        this.dictionaryRecordTypes.clear();
        this.dictionaryRecordTypes.clear();

        this.dictionarySearchRecordTypes.clear();
        this.dictionarySearchRecordTypes.clear();

        return;
    }
}
