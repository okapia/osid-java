//
// AbstractCreditQuery.java
//
//     A template for making a Credit Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for credits.
 */

public abstract class AbstractCreditQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.acknowledgement.CreditQuery {

    private final java.util.Collection<org.osid.acknowledgement.records.CreditQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets reference <code> Id. </code> 
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id referenceId, boolean match) {
        return;
    }


    /**
     *  Clears all reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        return;
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears all resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match credits 
     *  assigned to billings. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBillingId(org.osid.id.Id billingId, boolean match) {
        return;
    }


    /**
     *  Clears all billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBillingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> supportsBillingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getBillingQuery() {
        throw new org.osid.UnimplementedException("supportsBillingQuery() is false");
    }


    /**
     *  Clears all billing terms. 
     */

    @OSID @Override
    public void clearBillingTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given credit query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a credit implementing the requested record.
     *
     *  @param creditRecordType a credit record type
     *  @return the credit query record
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditQueryRecord getCreditQueryRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditQueryRecord record : this.records) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credit query. 
     *
     *  @param creditQueryRecord credit query record
     *  @param creditRecordType credit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCreditQueryRecord(org.osid.acknowledgement.records.CreditQueryRecord creditQueryRecord, 
                                          org.osid.type.Type creditRecordType) {

        addRecordType(creditRecordType);
        nullarg(creditQueryRecord, "credit query record");
        this.records.add(creditQueryRecord);        
        return;
    }
}
