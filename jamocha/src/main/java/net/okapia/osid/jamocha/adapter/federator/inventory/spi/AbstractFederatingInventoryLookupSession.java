//
// AbstractFederatingInventoryLookupSession.java
//
//     An abstract federating adapter for an InventoryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  InventoryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingInventoryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inventory.InventoryLookupSession>
    implements org.osid.inventory.InventoryLookupSession {

    private boolean parallel = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Constructs a new <code>AbstractFederatingInventoryLookupSession</code>.
     */

    protected AbstractFederatingInventoryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inventory.InventoryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Inventory</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInventories() {
        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            if (session.canLookupInventories()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Inventory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInventoryView() {
        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            session.useComparativeInventoryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Inventory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInventoryView() {
        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            session.usePlenaryInventoryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inventories in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            session.useFederatedWarehouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            session.useIsolatedWarehouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Inventory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inventory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inventory</code> and
     *  retained for compatibility.
     *
     *  @param  inventoryId <code>Id</code> of the
     *          <code>Inventory</code>
     *  @return the inventory
     *  @throws org.osid.NotFoundException <code>inventoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inventoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Inventory getInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            try {
                return (session.getInventory(inventoryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(inventoryId + " not found");
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inventories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inventories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  inventoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByIds(org.osid.id.IdList inventoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inventory.inventory.MutableInventoryList ret = new net.okapia.osid.jamocha.inventory.inventory.MutableInventoryList();

        try (org.osid.id.IdList ids = inventoryIds) {
            while (ids.hasNext()) {
                ret.addInventory(getInventory(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> which does not include
     *  inventories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesByGenusType(inventoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> and include any additional
     *  inventories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByParentGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesByParentGenusType(inventoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>InventoryList</code> containing the given
     *  inventory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByRecordType(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesByRecordType(inventoryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the most recent <code>Inventories</code>. In plenary
     *  mode, the returned list contains all known inventories or an
     *  error results.  Otherwise, the returned list may contain only
     *  those inventories that are accessible through this session.
     *
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getRecentInventories());
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the most recent <code>Inventories</code> for with the
     *  given <code>Stock</code>, In plenary mode, the returned list
     *  contains all known inventories or an error results. Otherwise,
     *  the returned list may contain only those inventories that are
     *  accessible through this session.
     *
     *  @param  stockId an inventory <code>Id</code>
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getRecentInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesForStock(stockId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets <code>Inventories</code> between the given date range
     *  inclusive.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to< /code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDate(org.osid.calendaring.DateTime from,
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Inventories</code> for the given
     *  <code>Stock</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session.
     *
     *  @param  stockId a stock <code>Id</code>
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesForStock(stockId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets <code>Inventories</code> between the given date range
     *  inclusive for the given stock.  In plenary mode, the returned
     *  list contains all known inventories or an error
     *  results. Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session.
     *
     *  @param  stockId an inventory <code>Id</code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>stockid</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDateForStock(org.osid.id.Id stockId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventoriesByDateForStock(stockId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Inventories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Inventories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList ret = getInventoryList();

        for (org.osid.inventory.InventoryLookupSession session : getSessions()) {
            ret.addInventoryList(session.getInventories());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inventory.inventory.FederatingInventoryList getInventoryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.inventory.ParallelInventoryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.inventory.CompositeInventoryList());
        }
    }
}
