//
// InvariantIndexedMapTimePeriodLookupSession
//
//    Implements a TimePeriod lookup service backed by a fixed
//    collection of timePeriods indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a TimePeriod lookup service backed by a fixed
 *  collection of time periods. The time periods are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some time periods may be compatible
 *  with more types than are indicated through these time period
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapTimePeriodLookupSession} using an
     *  array of timePeriods.
     *
     *  @param calendar the calendar
     *  @param timePeriods an array of time periods
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                    org.osid.calendaring.TimePeriod[] timePeriods) {

        setCalendar(calendar);
        putTimePeriods(timePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapTimePeriodLookupSession} using a
     *  collection of time periods.
     *
     *  @param calendar the calendar
     *  @param timePeriods a collection of time periods
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code timePeriods} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                    java.util.Collection<? extends org.osid.calendaring.TimePeriod> timePeriods) {

        setCalendar(calendar);
        putTimePeriods(timePeriods);
        return;
    }
}
