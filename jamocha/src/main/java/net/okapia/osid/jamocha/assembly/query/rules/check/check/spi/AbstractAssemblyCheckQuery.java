//
// AbstractAssemblyCheckQuery.java
//
//     A CheckQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CheckQuery that stores terms.
 */

public abstract class AbstractAssemblyCheckQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.rules.check.CheckQuery,
               org.osid.rules.check.CheckQueryInspector,
               org.osid.rules.check.CheckSearchOrder {

    private final java.util.Collection<org.osid.rules.check.records.CheckQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.CheckQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.CheckSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCheckQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCheckQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches fail checks. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFailCheck(boolean match) {
        getAssembler().addBooleanTerm(getFailCheckColumn(), match);
        return;
    }


    /**
     *  Clears the fail check query terms. 
     */

    @OSID @Override
    public void clearFailCheckTerms() {
        getAssembler().clearTerms(getFailCheckColumn());
        return;
    }


    /**
     *  Gets the fail check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getFailCheckTerms() {
        return (getAssembler().getBooleanTerms(getFailCheckColumn()));
    }


    /**
     *  Gets the FailCheck column name.
     *
     * @return the column name
     */

    protected String getFailCheckColumn() {
        return ("fail_check");
    }


    /**
     *  Matches checks with start dates within the given date range
     *  inclusive.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckStartDate(org.osid.calendaring.DateTime from, 
                                        org.osid.calendaring.DateTime to, 
                                        boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimeCheckStartDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches any time check with a start date. 
     *
     *  @param match <code> true </code> to match any time checks with
     *         a start date, <code> false </code> to match checks
     *         with no start date
     */

    @OSID @Override
    public void matchAnyTimeCheckStartDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getTimeCheckStartDateColumn(), match);
        return;
    }


    /**
     *  Clears the time check start date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckStartDateTerms() {
        getAssembler().clearTerms(getTimeCheckStartDateColumn());
        return;
    }


    /**
     *  Gets the time check start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckStartDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeCheckStartDateColumn()));
    }


    /**
     *  Orders the results by the time check start date.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckStartDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeCheckStartDateColumn(), style);
        return;
    }


    /**
     *  Gets the TimeCheckStartDate column name.
     *
     * @return the column name
     */

    protected String getTimeCheckStartDateColumn() {
        return ("time_check_start_date");
    }


    /**
     *  Matches checks with end dates within the given date range
     *  inclusive.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckEndDate(org.osid.calendaring.DateTime from, 
                                        org.osid.calendaring.DateTime to, 
                                        boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimeCheckEndDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches any time check with a end date. 
     *
     *  @param match <code> true </code> to match any time checks with
     *         a end date, <code> false </code> to match checks
     *         with no end date
     */

    @OSID @Override
    public void matchAnyTimeCheckEndDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getTimeCheckEndDateColumn(), match);
        return;
    }


    /**
     *  Clears the time check end date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEndDateTerms() {
        getAssembler().clearTerms(getTimeCheckEndDateColumn());
        return;
    }


    /**
     *  Gets the time check end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckEndDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeCheckEndDateColumn()));
    }


    /**
     *  Orders the results by the time check end date.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckEndDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeCheckEndDateColumn(), style);
        return;
    }


    /**
     *  Gets the TimeCheckEndDate column name.
     *
     * @return the column name
     */

    protected String getTimeCheckEndDateColumn() {
        return ("time_check_end_date");
    }


    /**
     *  Matches checks with dates overlapping the given date range
     *  inclusive.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimeCheckDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the time check date query terms. 
     */

    @OSID @Override
    public void clearTimeCheckDateTerms() {
        getAssembler().clearTerms(getTimeCheckDateColumn());
        return;
    }


    /**
     *  Gets the time check date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeCheckDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimeCheckDateColumn()));
    }


    /**
     *  Gets the TimeCheckDate column name.
     *
     * @return the column name
     */

    protected String getTimeCheckDateColumn() {
        return ("time_check_date");
    }

    
    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId the event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getTimeCheckEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEventIdTerms() {
        getAssembler().clearTerms(getTimeCheckEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimeCheckEventIdTerms() {
        return (getAssembler().getIdTerms(getTimeCheckEventIdColumn()));
    }


    /**
     *  Orders the results by the time check event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeCheckEventColumn(), style);
        return;
    }


    /**
     *  Gets the TimeCheckEventId column name.
     *
     * @return the column name
     */

    protected String getTimeCheckEventIdColumn() {
        return ("time_check_event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimeCheckEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getTimeCheckEventQuery() {
        throw new org.osid.UnimplementedException("supportsTimeCheckEventQuery() is false");
    }


    /**
     *  Matches any time check with an event. 
     *
     *  @param  match <code> true </code> to match any time checks with an 
     *          event, <code> false </code> to match checks with no events 
     */

    @OSID @Override
    public void matchAnyTimeCheckEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getTimeCheckEventColumn(), match);
        return;
    }


    /**
     *  Clears the event query terms. 
     */

    @OSID @Override
    public void clearTimeCheckEventTerms() {
        getAssembler().clearTerms(getTimeCheckEventColumn());
        return;
    }


    /**
     *  Gets the event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getTimeCheckEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an event search order is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the event search order. 
     *
     *  @return the event search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTimeCheckEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getTimeCheckEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimeCheckEventSearchOrder() is false");
    }


    /**
     *  Gets the TimeCheckEvent column name.
     *
     * @return the column name
     */

    protected String getTimeCheckEventColumn() {
        return ("time_check_event");
    }


    /**
     *  Sets the cyclic event <code> Id </code> for this query. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimeCheckCyclicEventId(org.osid.id.Id cyclicEventId, 
                                            boolean match) {
        getAssembler().addIdTerm(getTimeCheckCyclicEventIdColumn(), cyclicEventId, match);
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTimeCheckCyclicEventIdTerms() {
        getAssembler().clearTerms(getTimeCheckCyclicEventIdColumn());
        return;
    }


    /**
     *  Gets the cyclic event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimeCheckCyclicEventIdTerms() {
        return (getAssembler().getIdTerms(getTimeCheckCyclicEventIdColumn()));
    }


    /**
     *  Orders the results by cyclic event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeCheckCyclicEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimeCheckCyclicEventColumn(), style);
        return;
    }


    /**
     *  Gets the TimeCheckCyclicEventId column name.
     *
     * @return the column name
     */

    protected String getTimeCheckCyclicEventIdColumn() {
        return ("time_check_cyclic_event_id");
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimeCheckCyclicEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getTimeCheckCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsTimeCheckCyclicEventQuery() is false");
    }


    /**
     *  Matches any time check with a cyclic event. 
     *
     *  @param  match <code> true </code> to match any time checks with a 
     *          cyclic event, <code> false </code> to match checks with no 
     *          cyclic events 
     */

    @OSID @Override
    public void matchAnyTimeCheckCyclicEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getTimeCheckCyclicEventColumn(), match);
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearTimeCheckCyclicEventTerms() {
        getAssembler().clearTerms(getTimeCheckCyclicEventColumn());
        return;
    }


    /**
     *  Gets the cyclic event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getTimeCheckCyclicEventTerms() {
        return (new org.osid.calendaring.cycle.CyclicEventQueryInspector[0]);
    }


    /**
     *  Tests if a cyclic event search order is available. 
     *
     *  @return <code> true </code> if a cyclic event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimeCheckCyclicEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the cyclic event search order. 
     *
     *  @return the cyclic event search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTimeCheckCyclicEventSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchOrder getTimeCheckCyclicEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTimeCheckCyclicEventSearchOrder() is false");
    }


    /**
     *  Gets the TimeCheckCyclicEvent column name.
     *
     * @return the column name
     */

    protected String getTimeCheckCyclicEventColumn() {
        return ("time_check_cyclic_event");
    }


    /**
     *  Sets the block <code> Id </code> for this query. 
     *
     *  @param  blockId the block <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchHoldCheckBlockId(org.osid.id.Id blockId, boolean match) {
        getAssembler().addIdTerm(getHoldCheckBlockIdColumn(), blockId, match);
        return;
    }


    /**
     *  Clears the block <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearHoldCheckBlockIdTerms() {
        getAssembler().clearTerms(getHoldCheckBlockIdColumn());
        return;
    }


    /**
     *  Gets the block <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getHoldCheckBlockIdTerms() {
        return (getAssembler().getIdTerms(getHoldCheckBlockIdColumn()));
    }


    /**
     *  Orders the results by block. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByHoldCheckBlock(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getHoldCheckBlockColumn(), style);
        return;
    }


    /**
     *  Gets the HoldCheckBlockId column name.
     *
     * @return the column name
     */

    protected String getHoldCheckBlockIdColumn() {
        return ("hold_check_block_id");
    }


    /**
     *  Tests if a <code> BlockQuery </code> is available. 
     *
     *  @return <code> true </code> if a block query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldCheckBlockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a block. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the block query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldCheckBlockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuery getHoldCheckBlockQuery() {
        throw new org.osid.UnimplementedException("supportsHoldCheckBlockQuery() is false");
    }


    /**
     *  Matches any hold check with a block. 
     *
     *  @param  match <code> true </code> to match any hold checks with a 
     *          block, <code> false </code> to match checks with no blocks 
     */

    @OSID @Override
    public void matchAnyHoldCheckBlock(boolean match) {
        getAssembler().addIdWildcardTerm(getHoldCheckBlockColumn(), match);
        return;
    }


    /**
     *  Clears the block query terms. 
     */

    @OSID @Override
    public void clearHoldCheckBlockTerms() {
        getAssembler().clearTerms(getHoldCheckBlockColumn());
        return;
    }


    /**
     *  Gets the block query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hold.BlockQueryInspector[] getHoldCheckBlockTerms() {
        return (new org.osid.hold.BlockQueryInspector[0]);
    }


    /**
     *  Tests if a block search order is available. 
     *
     *  @return <code> true </code> if a block search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldCheckBlockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the block search order. 
     *
     *  @return the block search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsHoldCheckBlockSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchOrder getHoldCheckBlockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsHoldCheckBlockSearchOrder() is false");
    }


    /**
     *  Gets the HoldCheckBlock column name.
     *
     * @return the column name
     */

    protected String getHoldCheckBlockColumn() {
        return ("hold_check_block");
    }


    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryCheckAuditId(org.osid.id.Id auditId, boolean match) {
        getAssembler().addIdTerm(getInquiryCheckAuditIdColumn(), auditId, match);
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryCheckAuditIdTerms() {
        getAssembler().clearTerms(getInquiryCheckAuditIdColumn());
        return;
    }


    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryCheckAuditIdTerms() {
        return (getAssembler().getIdTerms(getInquiryCheckAuditIdColumn()));
    }


    /**
     *  Orders the results by audit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInquiryCheckAudit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInquiryCheckAuditColumn(), style);
        return;
    }


    /**
     *  Gets the InquiryCheckAuditId column name.
     *
     * @return the column name
     */

    protected String getInquiryCheckAuditIdColumn() {
        return ("inquiry_check_audit_id");
    }


    /**
     *  Tests if a <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryCheckAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryCheckAuditQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getInquiryCheckAuditQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryCheckAuditQuery() is false");
    }


    /**
     *  Matches any inquiry check with an audit. 
     *
     *  @param  match <code> true </code> to match any inquiry checks with a 
     *          audit, <code> false </code> to match checks with no audits 
     */

    @OSID @Override
    public void matchAnyInquiryCheckAudit(boolean match) {
        getAssembler().addIdWildcardTerm(getInquiryCheckAuditColumn(), match);
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearInquiryCheckAuditTerms() {
        getAssembler().clearTerms(getInquiryCheckAuditColumn());
        return;
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getInquiryCheckAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Tests if an audit search order is available. 
     *
     *  @return <code> true </code> if an audit search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryCheckAuditSearchOrder() {
        return (false);
    }


    /**
     *  Gets the audit search order. 
     *
     *  @return the audit search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInquiryCheckAuditSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditSearchOrder getInquiryCheckAuditSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInquiryCheckAuditSearchOrder() is false");
    }


    /**
     *  Gets the InquiryCheckAudit column name.
     *
     * @return the column name
     */

    protected String getInquiryCheckAuditColumn() {
        return ("inquiry_check_audit");
    }


    /**
     *  Sets the agenda <code> Id </code> for this query. 
     *
     *  @param  agendaId the agenda <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agendaId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcessCheckAgendaId(org.osid.id.Id agendaId, 
                                          boolean match) {
        getAssembler().addIdTerm(getProcessCheckAgendaIdColumn(), agendaId, match);
        return;
    }


    /**
     *  Clears the agenda <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcessCheckAgendaIdTerms() {
        getAssembler().clearTerms(getProcessCheckAgendaIdColumn());
        return;
    }


    /**
     *  Gets the agenda <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessCheckAgendaIdTerms() {
        return (getAssembler().getIdTerms(getProcessCheckAgendaIdColumn()));
    }


    /**
     *  Orders the results by agenda. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByProcessCheckAgenda(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProcessCheckAgendaColumn(), style);
        return;
    }


    /**
     *  Gets the ProcessCheckAgendaId column name.
     *
     * @return the column name
     */

    protected String getProcessCheckAgendaIdColumn() {
        return ("process_check_agenda_id");
    }


    /**
     *  Tests if an <code> AgendaQuery </code> is available. 
     *
     *  @return <code> true </code> if an agenda query is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsProcessCheckAgendaQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agenda. Multiple retrievals produce a
     *  nested <code> OR </code> term.
     *
     *  @return the agenda query 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsProcessCheckAgendaQuery() </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuery getProcessCheckAgendaQuery() {
        throw new org.osid.UnimplementedException("supportsProcessCheckAgendaQuery() is false");
    }


    /**
     *  Matches any process check with an agenda.
     *
     *  @param match <code> true </code> to match any process checks
     *          with an agenda, <code> false </code> to match checks
     *          with no agendas
     */

    @OSID @Override
    public void matchAnyProcessCheckAgenda(boolean match) {
        getAssembler().addIdWildcardTerm(getProcessCheckAgendaColumn(), match);
        return;
    }


    /**
     *  Clears the agenda query terms. 
     */

    @OSID @Override
    public void clearProcessCheckAgendaTerms() {
        getAssembler().clearTerms(getProcessCheckAgendaColumn());
        return;
    }


    /**
     *  Gets the agenda query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQueryInspector[] getProcessCheckAgendaTerms() {
        return (new org.osid.rules.check.AgendaQueryInspector[0]);
    }


    /**
     *  Tests if an agenda search order is available. 
     *
     *  @return <code> true </code> if an agenda search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessCheckAgendaSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agenda search order. 
     *
     *  @return the agenda search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProcessCheckAgendaSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchOrder getProcessCheckAgendaSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProcessCheckAgendaSearchOrder() is false");
    }


    /**
     *  Gets the ProcessCheckAgenda column name.
     *
     * @return the column name
     */

    protected String getProcessCheckAgendaColumn() {
        return ("process_check_agenda");
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match checks 
     *  assigned to engines. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        getAssembler().clearTerms(getEngineIdColumn());
        return;
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (getAssembler().getIdTerms(getEngineIdColumn()));
    }


    /**
     *  Gets the EngineId column name.
     *
     * @return the column name
     */

    protected String getEngineIdColumn() {
        return ("engine_id");
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        getAssembler().clearTerms(getEngineColumn());
        return;
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }


    /**
     *  Gets the Engine column name.
     *
     * @return the column name
     */

    protected String getEngineColumn() {
        return ("engine");
    }


    /**
     *  Tests if this check supports the given record
     *  <code>Type</code>.
     *
     *  @param  checkRecordType a check record type 
     *  @return <code>true</code> if the checkRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type checkRecordType) {
        for (org.osid.rules.check.records.CheckQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(checkRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  checkRecordType the check record type 
     *  @return the check query record 
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckQueryRecord getCheckQueryRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  checkRecordType the check record type 
     *  @return the check query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckQueryInspectorRecord getCheckQueryInspectorRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param checkRecordType the check record type
     *  @return the check search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckSearchOrderRecord getCheckSearchOrderRecord(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.CheckSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(checkRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this check. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param checkQueryRecord the check query record
     *  @param checkQueryInspectorRecord the check query inspector
     *         record
     *  @param checkSearchOrderRecord the check search order record
     *  @param checkRecordType check record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkQueryRecord</code>,
     *          <code>checkQueryInspectorRecord</code>,
     *          <code>checkSearchOrderRecord</code> or
     *          <code>checkRecordTypecheck</code> is
     *          <code>null</code>
     */
            
    protected void addCheckRecords(org.osid.rules.check.records.CheckQueryRecord checkQueryRecord, 
                                      org.osid.rules.check.records.CheckQueryInspectorRecord checkQueryInspectorRecord, 
                                      org.osid.rules.check.records.CheckSearchOrderRecord checkSearchOrderRecord, 
                                      org.osid.type.Type checkRecordType) {

        addRecordType(checkRecordType);

        nullarg(checkQueryRecord, "check query record");
        nullarg(checkQueryInspectorRecord, "check query inspector record");
        nullarg(checkSearchOrderRecord, "check search odrer record");

        this.queryRecords.add(checkQueryRecord);
        this.queryInspectorRecords.add(checkQueryInspectorRecord);
        this.searchOrderRecords.add(checkSearchOrderRecord);
        
        return;
    }
}
