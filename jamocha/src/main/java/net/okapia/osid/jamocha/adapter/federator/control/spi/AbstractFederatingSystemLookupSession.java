//
// AbstractFederatingSystemLookupSession.java
//
//     An abstract federating adapter for a SystemLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SystemLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSystemLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.SystemLookupSession>
    implements org.osid.control.SystemLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingSystemLookupSession</code>.
     */

    protected AbstractFederatingSystemLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.SystemLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>System</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSystems() {
        for (org.osid.control.SystemLookupSession session : getSessions()) {
            if (session.canLookupSystems()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>System</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSystemView() {
        for (org.osid.control.SystemLookupSession session : getSessions()) {
            session.useComparativeSystemView();
        }

        return;
    }


    /**
     *  A complete view of the <code>System</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySystemView() {
        for (org.osid.control.SystemLookupSession session : getSessions()) {
            session.usePlenarySystemView();
        }

        return;
    }

     
    /**
     *  Gets the <code>System</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>System</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>System</code> and
     *  retained for compatibility.
     *
     *  @param  systemId <code>Id</code> of the
     *          <code>System</code>
     *  @return the system
     *  @throws org.osid.NotFoundException <code>systemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>systemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            try {
                return (session.getSystem(systemId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(systemId + " not found");
    }


    /**
     *  Gets a <code>SystemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  systems specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Systems</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSystems()</code>.
     *
     *  @param  systemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>System</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>systemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByIds(org.osid.id.IdList systemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.system.MutableSystemList ret = new net.okapia.osid.jamocha.control.system.MutableSystemList();

        try (org.osid.id.IdList ids = systemIds) {
            while (ids.hasNext()) {
                ret.addSystem(getSystem(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SystemList</code> corresponding to the given
     *  system genus <code>Type</code> which does not include
     *  systems of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSystems()</code>.
     *
     *  @param  systemGenusType a system genus type 
     *  @return the returned <code>System</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>systemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByGenusType(org.osid.type.Type systemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList ret = getSystemList();

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            ret.addSystemList(session.getSystemsByGenusType(systemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SystemList</code> corresponding to the given
     *  system genus <code>Type</code> and include any additional
     *  systems with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSystems()</code>.
     *
     *  @param  systemGenusType a system genus type 
     *  @return the returned <code>System</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>systemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByParentGenusType(org.osid.type.Type systemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList ret = getSystemList();

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            ret.addSystemList(session.getSystemsByParentGenusType(systemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SystemList</code> containing the given
     *  system record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSystems()</code>.
     *
     *  @param  systemRecordType a system record type 
     *  @return the returned <code>System</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByRecordType(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList ret = getSystemList();

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            ret.addSystemList(session.getSystemsByRecordType(systemRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SystemList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known systems or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  systems that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>System</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystemsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList ret = getSystemList();

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            ret.addSystemList(session.getSystemsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Systems</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  systems or an error results. Otherwise, the returned list
     *  may contain only those systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Systems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SystemList getSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList ret = getSystemList();

        for (org.osid.control.SystemLookupSession session : getSessions()) {
            ret.addSystemList(session.getSystems());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.system.FederatingSystemList getSystemList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.system.ParallelSystemList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.system.CompositeSystemList());
        }
    }
}
