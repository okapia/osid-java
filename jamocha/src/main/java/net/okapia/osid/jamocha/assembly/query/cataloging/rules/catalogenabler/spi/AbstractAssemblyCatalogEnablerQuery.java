//
// AbstractAssemblyCatalogEnablerQuery.java
//
//     A CatalogEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.cataloging.rules.catalogenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CatalogEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyCatalogEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.cataloging.rules.CatalogEnablerQuery,
               org.osid.cataloging.rules.CatalogEnablerQueryInspector,
               org.osid.cataloging.rules.CatalogEnablerSearchOrder {

    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCatalogEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCatalogEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the catalog. 
     *
     *  @param  catalogId the catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCatalogId(org.osid.id.Id catalogId, boolean match) {
        getAssembler().addIdTerm(getRuledCatalogIdColumn(), catalogId, match);
        return;
    }


    /**
     *  Clears the catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCatalogIdTerms() {
        getAssembler().clearTerms(getRuledCatalogIdColumn());
        return;
    }


    /**
     *  Gets the catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCatalogIdTerms() {
        return (getAssembler().getIdTerms(getRuledCatalogIdColumn()));
    }


    /**
     *  Gets the RuledCatalogId column name.
     *
     * @return the column name
     */

    protected String getRuledCatalogIdColumn() {
        return ("ruled_catalog_id");
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getRuledCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCatalogQuery() is false");
    }


    /**
     *  Matches enablers mapped to any catalog. 
     *
     *  @param  match <code> true </code> for enablers mapped to any catalog, 
     *          <code> false </code> to match enablers mapped to no catalog 
     */

    @OSID @Override
    public void matchAnyRuledCatalog(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCatalogColumn(), match);
        return;
    }


    /**
     *  Clears the catalog query terms. 
     */

    @OSID @Override
    public void clearRuledCatalogTerms() {
        getAssembler().clearTerms(getRuledCatalogColumn());
        return;
    }


    /**
     *  Gets the catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getRuledCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the RuledCatalog column name.
     *
     * @return the column name
     */

    protected String getRuledCatalogColumn() {
        return ("ruled_catalog");
    }


    /**
     *  Matches enablers mapped to the catalog. 
     *
     *  @param  catalogId the catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogId(org.osid.id.Id catalogId, boolean match) {
        getAssembler().addIdTerm(getCatalogIdColumn(), catalogId, match);
        return;
    }


    /**
     *  Clears the catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogIdTerms() {
        getAssembler().clearTerms(getCatalogIdColumn());
        return;
    }


    /**
     *  Gets the catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCatalogIdColumn()));
    }


    /**
     *  Gets the CatalogId column name.
     *
     * @return the column name
     */

    protected String getCatalogIdColumn() {
        return ("catalog_id");
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogQuery() is false");
    }


    /**
     *  Clears the catalog query terms. 
     */

    @OSID @Override
    public void clearCatalogTerms() {
        getAssembler().clearTerms(getCatalogColumn());
        return;
    }


    /**
     *  Gets the catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the Catalog column name.
     *
     * @return the column name
     */

    protected String getCatalogColumn() {
        return ("catalog");
    }


    /**
     *  Tests if this catalogEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  catalogEnablerRecordType a catalog enabler record type 
     *  @return <code>true</code> if the catalogEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type catalogEnablerRecordType) {
        for (org.osid.cataloging.rules.records.CatalogEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  catalogEnablerRecordType the catalog enabler record type 
     *  @return the catalog enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerQueryRecord getCatalogEnablerQueryRecord(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.rules.records.CatalogEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  catalogEnablerRecordType the catalog enabler record type 
     *  @return the catalog enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord getCatalogEnablerQueryInspectorRecord(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param catalogEnablerRecordType the catalog enabler record type
     *  @return the catalog enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerSearchOrderRecord getCatalogEnablerSearchOrderRecord(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.rules.records.CatalogEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this catalog enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param catalogEnablerQueryRecord the catalog enabler query record
     *  @param catalogEnablerQueryInspectorRecord the catalog enabler query inspector
     *         record
     *  @param catalogEnablerSearchOrderRecord the catalog enabler search order record
     *  @param catalogEnablerRecordType catalog enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerQueryRecord</code>,
     *          <code>catalogEnablerQueryInspectorRecord</code>,
     *          <code>catalogEnablerSearchOrderRecord</code> or
     *          <code>catalogEnablerRecordTypecatalogEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addCatalogEnablerRecords(org.osid.cataloging.rules.records.CatalogEnablerQueryRecord catalogEnablerQueryRecord, 
                                      org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord catalogEnablerQueryInspectorRecord, 
                                      org.osid.cataloging.rules.records.CatalogEnablerSearchOrderRecord catalogEnablerSearchOrderRecord, 
                                      org.osid.type.Type catalogEnablerRecordType) {

        addRecordType(catalogEnablerRecordType);

        nullarg(catalogEnablerQueryRecord, "catalog enabler query record");
        nullarg(catalogEnablerQueryInspectorRecord, "catalog enabler query inspector record");
        nullarg(catalogEnablerSearchOrderRecord, "catalog enabler search odrer record");

        this.queryRecords.add(catalogEnablerQueryRecord);
        this.queryInspectorRecords.add(catalogEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(catalogEnablerSearchOrderRecord);
        
        return;
    }
}
