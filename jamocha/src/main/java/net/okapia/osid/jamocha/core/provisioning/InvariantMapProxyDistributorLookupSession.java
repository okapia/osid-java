//
// InvariantMapProxyDistributorLookupSession
//
//    Implements a Distributor lookup service backed by a fixed
//    collection of distributors. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Distributor lookup service backed by a fixed
 *  collection of distributors. The distributors are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDistributorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapDistributorLookupSession
    implements org.osid.provisioning.DistributorLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDistributorLookupSession} with no
     *  distributors.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyDistributorLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDistributorLookupSession} with a
     *  single distributor.
     *
     *  @param distributor a single distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDistributorLookupSession(org.osid.provisioning.Distributor distributor, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDistributorLookupSession} using
     *  an array of distributors.
     *
     *  @param distributors an array of distributors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributors} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDistributorLookupSession(org.osid.provisioning.Distributor[] distributors, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributors(distributors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDistributorLookupSession} using a
     *  collection of distributors.
     *
     *  @param distributors a collection of distributors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributors} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDistributorLookupSession(java.util.Collection<? extends org.osid.provisioning.Distributor> distributors,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributors(distributors);
        return;
    }
}
