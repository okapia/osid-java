//
// AbstractAssemblyProvisionReturnQuery.java
//
//     A ProvisionReturnQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.provisionreturn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProvisionReturnQuery that stores terms.
 */

public abstract class AbstractAssemblyProvisionReturnQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.provisioning.ProvisionReturnQuery,
               org.osid.provisioning.ProvisionReturnQueryInspector,
               org.osid.provisioning.ProvisionReturnSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProvisionReturnQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProvisionReturnQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches provisions with a return date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReturnDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getReturnDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the return date query terms. 
     */

    @OSID @Override
    public void clearReturnDateTerms() {
        getAssembler().clearTerms(getReturnDateColumn());
        return;
    }


    /**
     *  Gets the return date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReturnDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getReturnDateColumn()));
    }


    /**
     *  Orders the results by the return date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturnDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReturnDateColumn(), style);
        return;
    }


    /**
     *  Gets the ReturnDate column name.
     *
     * @return the column name
     */

    protected String getReturnDateColumn() {
        return ("return_date");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReturnerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getReturnerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReturnerIdTerms() {
        getAssembler().clearTerms(getReturnerIdColumn());
        return;
    }


    /**
     *  Gets the returner <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReturnerIdTerms() {
        return (getAssembler().getIdTerms(getReturnerIdColumn()));
    }


    /**
     *  Orders the results by the returner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturner(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReturnerColumn(), style);
        return;
    }


    /**
     *  Gets the ReturnerId column name.
     *
     * @return the column name
     */

    protected String getReturnerIdColumn() {
        return ("returner_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsReturnerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getReturnerQuery() {
        throw new org.osid.UnimplementedException("supportsReturnerQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearReturnerTerms() {
        getAssembler().clearTerms(getReturnerColumn());
        return;
    }


    /**
     *  Gets the returner query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getReturnerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReturnerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getReturnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReturnerSearchOrder() is false");
    }


    /**
     *  Gets the Returner column name.
     *
     * @return the column name
     */

    protected String getReturnerColumn() {
        return ("returner");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReturningAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getReturningAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReturningAgentIdTerms() {
        getAssembler().clearTerms(getReturningAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReturningAgentIdTerms() {
        return (getAssembler().getIdTerms(getReturningAgentIdColumn()));
    }


    /**
     *  Orders the results by the returning agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturningAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReturningAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ReturningAgentId column name.
     *
     * @return the column name
     */

    protected String getReturningAgentIdColumn() {
        return ("returning_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturningAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReturningAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getReturningAgentQuery() {
        throw new org.osid.UnimplementedException("supportsReturningAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearReturningAgentTerms() {
        getAssembler().clearTerms(getReturningAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getReturningAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturningAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReturningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getReturningAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReturningAgentSearchOrder() is false");
    }


    /**
     *  Gets the ReturningAgent column name.
     *
     * @return the column name
     */

    protected String getReturningAgentColumn() {
        return ("returning_agent");
    }


    /**
     *  Tests if this provisionReturn supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionReturnRecordType a provision return record type 
     *  @return <code>true</code> if the provisionReturnRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionReturnRecordType) {
        for (org.osid.provisioning.records.ProvisionReturnQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  provisionReturnRecordType the provision return record type 
     *  @return the provision return query record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionReturnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnQueryRecord getProvisionReturnQueryRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  provisionReturnRecordType the provision return record type 
     *  @return the provision return query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionReturnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord getProvisionReturnQueryInspectorRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param provisionReturnRecordType the provision return record type
     *  @return the provision return search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionReturnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnSearchOrderRecord getProvisionReturnSearchOrderRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this provision return. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionReturnQueryRecord the provision return query record
     *  @param provisionReturnQueryInspectorRecord the provision return query inspector
     *         record
     *  @param provisionReturnSearchOrderRecord the provision return search order record
     *  @param provisionReturnRecordType provision return record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnQueryRecord</code>,
     *          <code>provisionReturnQueryInspectorRecord</code>,
     *          <code>provisionReturnSearchOrderRecord</code> or
     *          <code>provisionReturnRecordTypeprovisionReturn</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionReturnRecords(org.osid.provisioning.records.ProvisionReturnQueryRecord provisionReturnQueryRecord, 
                                      org.osid.provisioning.records.ProvisionReturnQueryInspectorRecord provisionReturnQueryInspectorRecord, 
                                      org.osid.provisioning.records.ProvisionReturnSearchOrderRecord provisionReturnSearchOrderRecord, 
                                      org.osid.type.Type provisionReturnRecordType) {

        addRecordType(provisionReturnRecordType);

        nullarg(provisionReturnQueryRecord, "provision return query record");
        nullarg(provisionReturnQueryInspectorRecord, "provision return query inspector record");
        nullarg(provisionReturnSearchOrderRecord, "provision return search odrer record");

        this.queryRecords.add(provisionReturnQueryRecord);
        this.queryInspectorRecords.add(provisionReturnQueryInspectorRecord);
        this.searchOrderRecords.add(provisionReturnSearchOrderRecord);
        
        return;
    }
}
