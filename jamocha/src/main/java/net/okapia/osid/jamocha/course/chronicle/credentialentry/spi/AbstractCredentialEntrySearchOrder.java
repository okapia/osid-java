//
// AbstractCredentialEntrySearchOdrer.java
//
//     Defines a CredentialEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CredentialEntrySearchOrder}.
 */

public abstract class AbstractCredentialEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.chronicle.CredentialEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the credential. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCredential(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a credential order is available. 
     *
     *  @return <code> true </code> if a credential order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSearchOrder() {
        return (false);
    }


    /**
     *  Gets the credential order. 
     *
     *  @return the credential search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchOrder getCredentialSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCredentialSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the award date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDateAwarded(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  credentialEntryRecordType a credential entry record type 
     *  @return {@code true} if the credentialEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialEntryRecordType) {
        for (org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  credentialEntryRecordType the credential entry record type 
     *  @return the credential entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(credentialEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord getCredentialEntrySearchOrderRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(credentialEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this credential entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialEntryRecord the credential entry search odrer record
     *  @param credentialEntryRecordType credential entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryRecord} or
     *          {@code credentialEntryRecordTypecredentialEntry} is
     *          {@code null}
     */
            
    protected void addCredentialEntryRecord(org.osid.course.chronicle.records.CredentialEntrySearchOrderRecord credentialEntrySearchOrderRecord, 
                                     org.osid.type.Type credentialEntryRecordType) {

        addRecordType(credentialEntryRecordType);
        this.records.add(credentialEntrySearchOrderRecord);
        
        return;
    }
}
