//
// AbstractSignalSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.signal.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSignalSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.mapping.path.SignalSearchResults {

    private org.osid.mapping.path.SignalList signals;
    private final org.osid.mapping.path.SignalQueryInspector inspector;
    private final java.util.Collection<org.osid.mapping.path.records.SignalSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSignalSearchResults.
     *
     *  @param signals the result set
     *  @param signalQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>signals</code>
     *          or <code>signalQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSignalSearchResults(org.osid.mapping.path.SignalList signals,
                                            org.osid.mapping.path.SignalQueryInspector signalQueryInspector) {
        nullarg(signals, "signals");
        nullarg(signalQueryInspector, "signal query inspectpr");

        this.signals = signals;
        this.inspector = signalQueryInspector;

        return;
    }


    /**
     *  Gets the signal list resulting from a search.
     *
     *  @return a signal list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignals() {
        if (this.signals == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.mapping.path.SignalList signals = this.signals;
        this.signals = null;
	return (signals);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.mapping.path.SignalQueryInspector getSignalQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  signal search record <code> Type. </code> This method must
     *  be used to retrieve a signal implementing the requested
     *  record.
     *
     *  @param signalSearchRecordType a signal search 
     *         record type 
     *  @return the signal search
     *  @throws org.osid.NullArgumentException
     *          <code>signalSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(signalSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalSearchResultsRecord getSignalSearchResultsRecord(org.osid.type.Type signalSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.mapping.path.records.SignalSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(signalSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(signalSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record signal search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSignalRecord(org.osid.mapping.path.records.SignalSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "signal record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
