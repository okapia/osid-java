//
// GradebookColumnElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradebookColumnElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the GradebookColumnElement Id.
     *
     *  @return the gradebook column element Id
     */

    public static org.osid.id.Id getGradebookColumnEntityId() {
        return (makeEntityId("osid.grading.GradebookColumn"));
    }


    /**
     *  Gets the GradeSystemId element Id.
     *
     *  @return the GradeSystemId element Id
     */

    public static org.osid.id.Id getGradeSystemId() {
        return (makeElementId("osid.grading.gradebookcolumn.GradeSystemId"));
    }


    /**
     *  Gets the GradeSystem element Id.
     *
     *  @return the GradeSystem element Id
     */

    public static org.osid.id.Id getGradeSystem() {
        return (makeElementId("osid.grading.gradebookcolumn.GradeSystem"));
    }


    /**
     *  Gets the GradeEntryId element Id.
     *
     *  @return the GradeEntryId element Id
     */

    public static org.osid.id.Id getGradeEntryId() {
        return (makeQueryElementId("osid.grading.gradebookcolumn.GradeEntryId"));
    }


    /**
     *  Gets the GradeEntry element Id.
     *
     *  @return the GradeEntry element Id
     */

    public static org.osid.id.Id getGradeEntry() {
        return (makeQueryElementId("osid.grading.gradebookcolumn.GradeEntry"));
    }


    /**
     *  Gets the GradebookColumnSummary element Id.
     *
     *  @return the GradebookColumnSummary element Id
     */

    public static org.osid.id.Id getGradebookColumnSummary() {
        return (makeQueryElementId("osid.grading.gradebookcolumn.GradebookColumnSummary"));
    }


    /**
     *  Gets the GradebookId element Id.
     *
     *  @return the GradebookId element Id
     */

    public static org.osid.id.Id getGradebookId() {
        return (makeQueryElementId("osid.grading.gradebookcolumn.GradebookId"));
    }


    /**
     *  Gets the Gradebook element Id.
     *
     *  @return the Gradebook element Id
     */

    public static org.osid.id.Id getGradebook() {
        return (makeQueryElementId("osid.grading.gradebookcolumn.Gradebook"));
    }
}
