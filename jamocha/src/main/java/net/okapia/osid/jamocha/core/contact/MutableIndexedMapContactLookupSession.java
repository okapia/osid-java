//
// MutableIndexedMapContactLookupSession
//
//    Implements a Contact lookup service backed by a collection of
//    contacts indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact;


/**
 *  Implements a Contact lookup service backed by a collection of
 *  contacts. The contacts are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some contacts may be compatible
 *  with more types than are indicated through these contact
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of contacts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapContactLookupSession
    extends net.okapia.osid.jamocha.core.contact.spi.AbstractIndexedMapContactLookupSession
    implements org.osid.contact.ContactLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapContactLookupSession} with no contacts.
     *
     *  @param addressBook the address book
     *  @throws org.osid.NullArgumentException {@code addressBook}
     *          is {@code null}
     */

      public MutableIndexedMapContactLookupSession(org.osid.contact.AddressBook addressBook) {
        setAddressBook(addressBook);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapContactLookupSession} with a
     *  single contact.
     *  
     *  @param addressBook the address book
     *  @param  contact a single contact
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contact} is {@code null}
     */

    public MutableIndexedMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                                  org.osid.contact.Contact contact) {
        this(addressBook);
        putContact(contact);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapContactLookupSession} using an
     *  array of contacts.
     *
     *  @param addressBook the address book
     *  @param  contacts an array of contacts
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contacts} is {@code null}
     */

    public MutableIndexedMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                                  org.osid.contact.Contact[] contacts) {
        this(addressBook);
        putContacts(contacts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapContactLookupSession} using a
     *  collection of contacts.
     *
     *  @param addressBook the address book
     *  @param  contacts a collection of contacts
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contacts} is {@code null}
     */

    public MutableIndexedMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                                  java.util.Collection<? extends org.osid.contact.Contact> contacts) {

        this(addressBook);
        putContacts(contacts);
        return;
    }
    

    /**
     *  Makes a {@code Contact} available in this session.
     *
     *  @param  contact a contact
     *  @throws org.osid.NullArgumentException {@code contact{@code  is
     *          {@code null}
     */

    @Override
    public void putContact(org.osid.contact.Contact contact) {
        super.putContact(contact);
        return;
    }


    /**
     *  Makes an array of contacts available in this session.
     *
     *  @param  contacts an array of contacts
     *  @throws org.osid.NullArgumentException {@code contacts{@code 
     *          is {@code null}
     */

    @Override
    public void putContacts(org.osid.contact.Contact[] contacts) {
        super.putContacts(contacts);
        return;
    }


    /**
     *  Makes collection of contacts available in this session.
     *
     *  @param  contacts a collection of contacts
     *  @throws org.osid.NullArgumentException {@code contact{@code  is
     *          {@code null}
     */

    @Override
    public void putContacts(java.util.Collection<? extends org.osid.contact.Contact> contacts) {
        super.putContacts(contacts);
        return;
    }


    /**
     *  Removes a Contact from this session.
     *
     *  @param contactId the {@code Id} of the contact
     *  @throws org.osid.NullArgumentException {@code contactId{@code  is
     *          {@code null}
     */

    @Override
    public void removeContact(org.osid.id.Id contactId) {
        super.removeContact(contactId);
        return;
    }    
}
