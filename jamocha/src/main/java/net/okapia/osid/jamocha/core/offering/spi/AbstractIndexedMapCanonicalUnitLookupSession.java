//
// AbstractIndexedMapCanonicalUnitLookupSession.java
//
//    A simple framework for providing a CanonicalUnit lookup service
//    backed by a fixed collection of canonical units with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CanonicalUnit lookup service backed by a
 *  fixed collection of canonical units. The canonical units are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some canonical units may be compatible
 *  with more types than are indicated through these canonical unit
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCanonicalUnitLookupSession
    extends AbstractMapCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.CanonicalUnit> canonicalUnitsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.CanonicalUnit>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.CanonicalUnit> canonicalUnitsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.CanonicalUnit>());


    /**
     *  Makes a <code>CanonicalUnit</code> available in this session.
     *
     *  @param  canonicalUnit a canonical unit
     *  @throws org.osid.NullArgumentException <code>canonicalUnit<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        super.putCanonicalUnit(canonicalUnit);

        this.canonicalUnitsByGenus.put(canonicalUnit.getGenusType(), canonicalUnit);
        
        try (org.osid.type.TypeList types = canonicalUnit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitsByRecord.put(types.getNextType(), canonicalUnit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of canonical units available in this session.
     *
     *  @param  canonicalUnits an array of canonical units
     *  @throws org.osid.NullArgumentException <code>canonicalUnits<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnits(org.osid.offering.CanonicalUnit[] canonicalUnits) {
        for (org.osid.offering.CanonicalUnit canonicalUnit : canonicalUnits) {
            putCanonicalUnit(canonicalUnit);
        }

        return;
    }


    /**
     *  Makes a collection of canonical units available in this session.
     *
     *  @param  canonicalUnits a collection of canonical units
     *  @throws org.osid.NullArgumentException <code>canonicalUnits<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnits(java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits) {
        for (org.osid.offering.CanonicalUnit canonicalUnit : canonicalUnits) {
            putCanonicalUnit(canonicalUnit);
        }

        return;
    }


    /**
     *  Removes a canonical unit from this session.
     *
     *  @param canonicalUnitId the <code>Id</code> of the canonical unit
     *  @throws org.osid.NullArgumentException <code>canonicalUnitId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCanonicalUnit(org.osid.id.Id canonicalUnitId) {
        org.osid.offering.CanonicalUnit canonicalUnit;
        try {
            canonicalUnit = getCanonicalUnit(canonicalUnitId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.canonicalUnitsByGenus.remove(canonicalUnit.getGenusType());

        try (org.osid.type.TypeList types = canonicalUnit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitsByRecord.remove(types.getNextType(), canonicalUnit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCanonicalUnit(canonicalUnitId);
        return;
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> corresponding to the given
     *  canonical unit genus <code>Type</code> which does not include
     *  canonical units of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known canonical units or an error results. Otherwise,
     *  the returned list may contain only those canonical units that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  canonicalUnitGenusType a canonical unit genus type 
     *  @return the returned <code>CanonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.canonicalunit.ArrayCanonicalUnitList(this.canonicalUnitsByGenus.get(canonicalUnitGenusType)));
    }


    /**
     *  Gets a <code>CanonicalUnitList</code> containing the given
     *  canonical unit record <code>Type</code>. In plenary mode, the
     *  returned list contains all known canonical units or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical units that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  canonicalUnitRecordType a canonical unit record type 
     *  @return the returned <code>canonicalUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByRecordType(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.canonicalunit.ArrayCanonicalUnitList(this.canonicalUnitsByRecord.get(canonicalUnitRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitsByGenus.clear();
        this.canonicalUnitsByRecord.clear();

        super.close();

        return;
    }
}
