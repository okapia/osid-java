//
// AbstractMutableIntersection.java
//
//     Defines a mutable Intersection.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.intersection.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Intersection</code>.
 */

public abstract class AbstractMutableIntersection
    extends net.okapia.osid.jamocha.mapping.path.intersection.spi.AbstractIntersection
    implements org.osid.mapping.path.Intersection,
               net.okapia.osid.jamocha.builder.mapping.path.intersection.IntersectionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this intersection. 
     *
     *  @param record intersection record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addIntersectionRecord(org.osid.mapping.path.records.IntersectionRecord record, org.osid.type.Type recordType) {
        super.addIntersectionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this intersection.
     *
     *  @param displayName the name for this intersection
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this intersection.
     *
     *  @param description the description of this intersection
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    @Override
    public void setCoordinate(org.osid.mapping.Coordinate coordinate) {
        super.setCoordinate(coordinate);
        return;
    }


    /**
     *  Adds a path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    @Override
    public void addPath(org.osid.mapping.path.Path path) {
        super.addPath(path);
        return;
    }


    /**
     *  Sets all the paths.
     *
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException <code>paths</code> is
     *          <code>null</code>
     */

    @Override
    public void setPaths(java.util.Collection<org.osid.mapping.path.Path> paths) {
        super.setPaths(paths);
        return;
    }


    /**
     *  Sets the rotary flag.
     *
     *  @param rotary <code> true </code> if this intersection is a
     *          rotary, <code> false </code> otherwise
     */
    
    @Override
    public void setRotary(boolean rotary) {
        super.setRotary(rotary);
        return;
    }


    /**
     *  Sets the fork flag.
     *
     *  @param fork <code> true </code> if this intersection is a
     *          fork, <code> false </code> otherwise
     */

    @Override
    public void setFork(boolean fork) {
        super.setFork(fork);
        return;
    }
}

