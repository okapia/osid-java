//
// AbstractPoolProcessorEnabler.java
//
//     Defines a PoolProcessorEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessorenabler.spi;


/**
 *  Defines a <code>PoolProcessorEnabler</code> builder.
 */

public abstract class AbstractPoolProcessorEnablerBuilder<T extends AbstractPoolProcessorEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessorenabler.PoolProcessorEnablerMiter poolProcessorEnabler;


    /**
     *  Constructs a new <code>AbstractPoolProcessorEnablerBuilder</code>.
     *
     *  @param poolProcessorEnabler the pool processor enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPoolProcessorEnablerBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessorenabler.PoolProcessorEnablerMiter poolProcessorEnabler) {
        super(poolProcessorEnabler);
        this.poolProcessorEnabler = poolProcessorEnabler;
        return;
    }


    /**
     *  Builds the pool processor enabler.
     *
     *  @return the new pool processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.PoolProcessorEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.poolprocessorenabler.PoolProcessorEnablerValidator(getValidations())).validate(this.poolProcessorEnabler);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessorenabler.ImmutablePoolProcessorEnabler(this.poolProcessorEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the pool processor enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.poolprocessorenabler.PoolProcessorEnablerMiter getMiter() {
        return (this.poolProcessorEnabler);
    }


    /**
     *  Adds a PoolProcessorEnabler record.
     *
     *  @param record a pool processor enabler record
     *  @param recordType the type of pool processor enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.PoolProcessorEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addPoolProcessorEnablerRecord(record, recordType);
        return (self());
    }
}       


