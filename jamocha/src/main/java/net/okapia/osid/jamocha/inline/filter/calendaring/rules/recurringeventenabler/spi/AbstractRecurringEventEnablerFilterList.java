//
// AbstractRecurringEventEnablerList
//
//     Implements a filter for a RecurringEventEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a RecurringEventEnablerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedRecurringEventEnablerList
 *  to improve performance.
 */

public abstract class AbstractRecurringEventEnablerFilterList
    extends net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.spi.AbstractRecurringEventEnablerList
    implements org.osid.calendaring.rules.RecurringEventEnablerList,
               net.okapia.osid.jamocha.inline.filter.calendaring.rules.recurringeventenabler.RecurringEventEnablerFilter {

    private org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler;
    private final org.osid.calendaring.rules.RecurringEventEnablerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractRecurringEventEnablerFilterList</code>.
     *
     *  @param recurringEventEnablerList a <code>RecurringEventEnablerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerList</code> is <code>null</code>
     */

    protected AbstractRecurringEventEnablerFilterList(org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablerList) {
        nullarg(recurringEventEnablerList, "recurring event enabler list");
        this.list = recurringEventEnablerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.recurringEventEnabler == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> RecurringEventEnabler </code> in this list. 
     *
     *  @return the next <code> RecurringEventEnabler </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> RecurringEventEnabler </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnabler getNextRecurringEventEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler = this.recurringEventEnabler;
            this.recurringEventEnabler = null;
            return (recurringEventEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in recurring event enabler list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.recurringEventEnabler = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters RecurringEventEnablers.
     *
     *  @param recurringEventEnabler the recurring event enabler to filter
     *  @return <code>true</code> if the recurring event enabler passes the filter,
     *          <code>false</code> if the recurring event enabler should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler);


    protected void prime() {
        if (this.recurringEventEnabler != null) {
            return;
        }

        org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler = null;

        while (this.list.hasNext()) {
            try {
                recurringEventEnabler = this.list.getNextRecurringEventEnabler();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(recurringEventEnabler)) {
                this.recurringEventEnabler = recurringEventEnabler;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
