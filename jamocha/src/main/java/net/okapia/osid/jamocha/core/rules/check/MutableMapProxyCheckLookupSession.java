//
// MutableMapProxyCheckLookupSession
//
//    Implements a Check lookup service backed by a collection of
//    checks that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements a Check lookup service backed by a collection of
 *  checks. The checks are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of checks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCheckLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractMapCheckLookupSession
    implements org.osid.rules.check.CheckLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCheckLookupSession}
     *  with no checks.
     *
     *  @param engine the engine
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCheckLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.proxy.Proxy proxy) {
        setEngine(engine);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCheckLookupSession} with a
     *  single check.
     *
     *  @param engine the engine
     *  @param check a check
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code check}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCheckLookupSession(org.osid.rules.Engine engine,
                                                org.osid.rules.check.Check check, org.osid.proxy.Proxy proxy) {
        this(engine, proxy);
        putCheck(check);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCheckLookupSession} using an
     *  array of checks.
     *
     *  @param engine the engine
     *  @param checks an array of checks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code checks}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCheckLookupSession(org.osid.rules.Engine engine,
                                                org.osid.rules.check.Check[] checks, org.osid.proxy.Proxy proxy) {
        this(engine, proxy);
        putChecks(checks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCheckLookupSession} using a
     *  collection of checks.
     *
     *  @param engine the engine
     *  @param checks a collection of checks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code checks}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCheckLookupSession(org.osid.rules.Engine engine,
                                                java.util.Collection<? extends org.osid.rules.check.Check> checks,
                                                org.osid.proxy.Proxy proxy) {
   
        this(engine, proxy);
        setSessionProxy(proxy);
        putChecks(checks);
        return;
    }

    
    /**
     *  Makes a {@code Check} available in this session.
     *
     *  @param check an check
     *  @throws org.osid.NullArgumentException {@code check{@code 
     *          is {@code null}
     */

    @Override
    public void putCheck(org.osid.rules.check.Check check) {
        super.putCheck(check);
        return;
    }


    /**
     *  Makes an array of checks available in this session.
     *
     *  @param checks an array of checks
     *  @throws org.osid.NullArgumentException {@code checks{@code 
     *          is {@code null}
     */

    @Override
    public void putChecks(org.osid.rules.check.Check[] checks) {
        super.putChecks(checks);
        return;
    }


    /**
     *  Makes collection of checks available in this session.
     *
     *  @param checks
     *  @throws org.osid.NullArgumentException {@code check{@code 
     *          is {@code null}
     */

    @Override
    public void putChecks(java.util.Collection<? extends org.osid.rules.check.Check> checks) {
        super.putChecks(checks);
        return;
    }


    /**
     *  Removes a Check from this session.
     *
     *  @param checkId the {@code Id} of the check
     *  @throws org.osid.NullArgumentException {@code checkId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCheck(org.osid.id.Id checkId) {
        super.removeCheck(checkId);
        return;
    }    
}
