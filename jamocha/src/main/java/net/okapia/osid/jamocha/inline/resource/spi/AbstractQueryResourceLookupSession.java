//
// AbstractQueryResourceLookupSession.java
//
//    An inline adapter that maps a ResourceLookupSession to
//    a ResourceQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ResourceLookupSession to
 *  a ResourceQuerySession.
 */

public abstract class AbstractQueryResourceLookupSession
    extends net.okapia.osid.jamocha.resource.spi.AbstractResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {

    private final org.osid.resource.ResourceQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryResourceLookupSession.
     *
     *  @param querySession the underlying resource query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryResourceLookupSession(org.osid.resource.ResourceQuerySession querySession) {
        nullarg(querySession, "resource query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bin</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform <code>Resource</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupResources() {
        return (this.session.canSearchResources());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resources in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    
     
    /**
     *  Gets the <code>Resource</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Resource</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Resource</code> and
     *  retained for compatibility.
     *
     *  @param  resourceId <code>Id</code> of the
     *          <code>Resource</code>
     *  @return the resource
     *  @throws org.osid.NotFoundException <code>resourceId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource(org.osid.id.Id resourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();
        query.matchId(resourceId, true);
        org.osid.resource.ResourceList resources = this.session.getResourcesByQuery(query);
        if (resources.hasNext()) {
            return (resources.getNextResource());
        } 
        
        throw new org.osid.NotFoundException(resourceId + " not found");
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  resources specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Resources</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  resourceIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>resourceIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByIds(org.osid.id.IdList resourceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();

        try (org.osid.id.IdList ids = resourceIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getResourcesByQuery(query));
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> which does not include
     *  resources of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();
        query.matchGenusType(resourceGenusType, true);
        return (this.session.getResourcesByQuery(query));
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> and include any additional
     *  resources with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByParentGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();
        query.matchParentGenusType(resourceGenusType, true);
        return (this.session.getResourcesByQuery(query));
    }


    /**
     *  Gets a <code>ResourceList</code> containing the given
     *  resource record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByRecordType(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();
        query.matchRecordType(resourceRecordType, true);
        return (this.session.getResourcesByQuery(query));
    }

    
    /**
     *  Gets all <code>Resources</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resources that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Resources</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resource.ResourceQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getResourcesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resource.ResourceQuery getQuery() {
        org.osid.resource.ResourceQuery query = this.session.getResourceQuery();
        return (query);
    }
}
