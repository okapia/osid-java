//
// AbstractImmutableStatistic.java
//
//     Wraps a mutable Statistic to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Statistic</code> to hide modifiers. This
 *  wrapper provides an immutized Statistic from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying statistic whose state changes are visible.
 */

public abstract class AbstractImmutableStatistic
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCompendium
    implements org.osid.metering.Statistic {

    private final org.osid.metering.Statistic statistic;


    /**
     *  Constructs a new <code>AbstractImmutableStatistic</code>.
     *
     *  @param statistic the statistic to immutablize
     *  @throws org.osid.NullArgumentException <code>statistic</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableStatistic(org.osid.metering.Statistic statistic) {
        super(statistic);
        this.statistic = statistic;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Meter </code> associated with 
     *  this reading. 
     *
     *  @return the <code> Id </code> of the <code> Meter </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeterId() {
        return (this.statistic.getMeterId());
    }


    /**
     *  Gets the <code> Meter </code> associated with this reading. 
     *
     *  @return the <code> Meter </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter()
        throws org.osid.OperationFailedException {

        return (this.statistic.getMeter());
    }


    /**
     *  Gets the metered object associated with this reading. 
     *
     *  @return the metered object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeteredObjectId() {
        return (this.statistic.getMeteredObjectId());
    }


    /**
     *  Gets the total. 
     *
     *  @return the sum 
     */

    @OSID @Override
    public java.math.BigDecimal getSum() {
        return (this.statistic.getSum());
    }


    /**
     *  Gets the mean. 
     *
     *  @return the mean 
     */

    @OSID @Override
    public java.math.BigDecimal getMean() {
        return (this.statistic.getMean());
    }


    /**
     *  Gets the median. 
     *
     *  @return the mean 
     */

    @OSID @Override
    public java.math.BigDecimal getMedian() {
        return (this.statistic.getMedian());
    }


    /**
     *  Gets the mode. 
     *
     *  @return the mode 
     */

    @OSID @Override
    public java.math.BigDecimal getMode() {
        return (this.statistic.getMode());
    }


    /**
     *  Gets the standard deviation. 
     *
     *  @return the standard deviation 
     */

    @OSID @Override
    public java.math.BigDecimal getStandardDeviation() {
        return (this.statistic.getStandardDeviation());
    }


    /**
     *  Gets the root mean square. 
     *
     *  @return the rms 
     */

    @OSID @Override
    public java.math.BigDecimal getRMS() {
        return (this.statistic.getRMS());
    }


    /**
     *  Gets the difference between the end and start values. 
     *
     *  @return the delta 
     */

    @OSID @Override
    public java.math.BigDecimal getDelta() {
        return (this.statistic.getDelta());
    }


    /**
     *  Gets the percent change between the end and start values (e.g. 
     *  50.25%). 
     *
     *  @return the delta 
     */

    @OSID @Override
    public java.math.BigDecimal getPercentChange() {
        return (this.statistic.getPercentChange());
    }


    /**
     *  Gets the average rate of change. 
     *
     *  @param  units the time units 
     *  @return the average rate 
     *  @throws org.osid.NullArgumentException <code> units </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getAverageRate(org.osid.calendaring.DateTimeResolution units) {
        return (this.statistic.getAverageRate(units));
    }


    /**
     *  Gets the statistic record corresponding to the given <code> Statistic 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  statisticRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(statisticRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  statisticRecordType the statistic record type 
     *  @return the statistic record 
     *  @throws org.osid.NullArgumentException <code> statisticRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(statisticRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.records.StatisticRecord getStatisticRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        return (this.statistic.getStatisticRecord(statisticRecordType));
    }
}

