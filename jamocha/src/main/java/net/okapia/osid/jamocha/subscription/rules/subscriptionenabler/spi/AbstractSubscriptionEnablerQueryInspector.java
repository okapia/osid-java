//
// AbstractSubscriptionEnablerQueryInspector.java
//
//     A template for making a SubscriptionEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for subscription enablers.
 */

public abstract class AbstractSubscriptionEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.subscription.rules.SubscriptionEnablerQueryInspector {

    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the subscription <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSubscriptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subscription query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getRuledSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the publisher <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the publisher query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given subscription enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a subscription enabler implementing the requested record.
     *
     *  @param subscriptionEnablerRecordType a subscription enabler record type
     *  @return the subscription enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord getSubscriptionEnablerQueryInspectorRecord(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription enabler query. 
     *
     *  @param subscriptionEnablerQueryInspectorRecord subscription enabler query inspector
     *         record
     *  @param subscriptionEnablerRecordType subscriptionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubscriptionEnablerQueryInspectorRecord(org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord subscriptionEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type subscriptionEnablerRecordType) {

        addRecordType(subscriptionEnablerRecordType);
        nullarg(subscriptionEnablerRecordType, "subscription enabler record type");
        this.records.add(subscriptionEnablerQueryInspectorRecord);        
        return;
    }
}
