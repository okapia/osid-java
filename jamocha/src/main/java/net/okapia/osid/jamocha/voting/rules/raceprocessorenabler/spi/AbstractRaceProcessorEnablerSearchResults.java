//
// AbstractRaceProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRaceProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.rules.RaceProcessorEnablerSearchResults {

    private org.osid.voting.rules.RaceProcessorEnablerList raceProcessorEnablers;
    private final org.osid.voting.rules.RaceProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.rules.records.RaceProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRaceProcessorEnablerSearchResults.
     *
     *  @param raceProcessorEnablers the result set
     *  @param raceProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablers</code>
     *          or <code>raceProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRaceProcessorEnablerSearchResults(org.osid.voting.rules.RaceProcessorEnablerList raceProcessorEnablers,
                                            org.osid.voting.rules.RaceProcessorEnablerQueryInspector raceProcessorEnablerQueryInspector) {
        nullarg(raceProcessorEnablers, "race processor enablers");
        nullarg(raceProcessorEnablerQueryInspector, "race processor enabler query inspectpr");

        this.raceProcessorEnablers = raceProcessorEnablers;
        this.inspector = raceProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the race processor enabler list resulting from a search.
     *
     *  @return a race processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablers() {
        if (this.raceProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.rules.RaceProcessorEnablerList raceProcessorEnablers = this.raceProcessorEnablers;
        this.raceProcessorEnablers = null;
	return (raceProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.rules.RaceProcessorEnablerQueryInspector getRaceProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  race processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a raceProcessorEnabler implementing the requested
     *  record.
     *
     *  @param raceProcessorEnablerSearchRecordType a raceProcessorEnabler search 
     *         record type 
     *  @return the race processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(raceProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceProcessorEnablerSearchResultsRecord getRaceProcessorEnablerSearchResultsRecord(org.osid.type.Type raceProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.rules.records.RaceProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(raceProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(raceProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record race processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRaceProcessorEnablerRecord(org.osid.voting.rules.records.RaceProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "race processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
