//
// AbstractAdapterProfileEntryLookupSession.java
//
//    A ProfileEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ProfileEntry lookup session adapter.
 */

public abstract class AbstractAdapterProfileEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.profile.ProfileEntryLookupSession {

    private final org.osid.profile.ProfileEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProfileEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProfileEntryLookupSession(org.osid.profile.ProfileEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Profile/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Profile Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.session.getProfileId());
    }


    /**
     *  Gets the {@code Profile} associated with this session.
     *
     *  @return the {@code Profile} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProfile());
    }


    /**
     *  Tests if this user can perform {@code ProfileEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProfileEntries() {
        return (this.session.canLookupProfileEntries());
    }


    /**
     *  A complete view of the {@code ProfileEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileEntryView() {
        this.session.useComparativeProfileEntryView();
        return;
    }


    /**
     *  A complete view of the {@code ProfileEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileEntryView() {
        this.session.usePlenaryProfileEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile entries in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.session.useFederatedProfileView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.session.useIsolatedProfileView();
        return;
    }
    

    /**
     *  Only profile entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProfileEntryView() {
        this.session.useEffectiveProfileEntryView();
        return;
    }
    

    /**
     *  All profile entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProfileEntryView() {
        this.session.useAnyEffectiveProfileEntryView();
        return;
    }


    /**
     *  Sets the view for methods in this session to implicit profile
     *  entries.  An implicit view will include profile entries
     *  derived from other entries as a result of the {@code
     *  ProfileItem} or {@code Resource} hierarchies. This method is
     *  the opposite of {@code explicitProfileEntryView()}.
     */

    public void useImplicitProfileEntryView() {
        this.session.useImplicitProfileEntryView();
        return;
    }


    /**
     *  Sets the view for methods in this session to explicit profile
     *  entries.  An explicit view includes only those profile entries
     *  that were explicitly defined and not implied. This method is
     *  the opposite of {@code implicitProfileEntryView()} .
     */

    @OSID @Override
    public void useExplicitProfileEntryView() {
        this.session.useExplicitProfileEntryView();
        return;
    }


    /**
     *  Include profile entries of any agent of a resource when
     *  looking up profile entries by resource.
     */

    public void useImplicitResourceView() {
        this.session.useImplicitResourceView();
        return;
    }


    /**
     *  Only include profile entries explicitly mapped to the given resource 
     *  when looking up profile entries by resource. 
     *
     */

    @OSID @Override
    public void useExplicitResourceView() {
        this.session.useExplicitResourceView();
        return;
    }

     
    /**
     *  Gets the {@code ProfileEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ProfileEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ProfileEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param profileEntryId {@code Id} of the {@code ProfileEntry}
     *  @return the profile entry
     *  @throws org.osid.NotFoundException {@code profileEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code profileEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntry(profileEntryId));
    }


    /**
     *  Gets a {@code ProfileEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ProfileEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ProfileEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByIds(org.osid.id.IdList profileEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesByIds(profileEntryIds));
    }


    /**
     *  Gets a {@code ProfileEntryList} corresponding to the given
     *  profile entry genus {@code Type} which does not include
     *  profile entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned {@code ProfileEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesByGenusType(profileEntryGenusType));
    }


    /**
     *  Gets a {@code ProfileEntryList} corresponding to the given
     *  profile entry genus {@code Type} and include any additional
     *  profile entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryGenusType a profileEntry genus type 
     *  @return the returned {@code ProfileEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByParentGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesByParentGenusType(profileEntryGenusType));
    }


    /**
     *  Gets a {@code ProfileEntryList} containing the given
     *  profile entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @param  profileEntryRecordType a profileEntry record type 
     *  @return the returned {@code ProfileEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByRecordType(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesByRecordType(profileEntryRecordType));
    }


    /**
     *  Gets a {@code ProfileEntryList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *  
     *  In active mode, profile entries are returned that are
     *  currently active. In any status mode, active and inactive
     *  profile entries are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ProfileEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of profile entries corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForResource(resourceId));
    }


    /**
     *  Gets a list of profile entries corresponding to a resource
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  profile entries and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code agentId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgent(org.osid.id.Id agentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForAgent(agentId));
    }


    /**
     *  Gets a list of profile entries corresponding to an agent
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentOnDate(org.osid.id.Id agentId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForAgentOnDate(agentId, from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the {@code Id} of the profile item
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code profileItemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForProfileItem(profileItemId));
    }


    /**
     *  Gets a list of profile entries corresponding to a profile item
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  profileItemId the {@code Id} of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code profileItemId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForProfileItemOnDate(org.osid.id.Id profileItemId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForProfileItemOnDate(profileItemId, from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  profileItemId the {@code Id} of the profile item
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code profileItemId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItem(org.osid.id.Id resourceId,
                                                                                        org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForResourceAndProfileItem(resourceId, profileItemId));
    }


    /**
     *  Gets a list of profile entries corresponding to resource and
     *  profile item {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective. In any effective mode, effective profile
     *  entries and those currently expired are returned.q
     *
     *  @param  profileItemId the {@code Id} of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code profileItemId}, {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForResourceAndProfileItemOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForResourceAndProfileItemOnDate(resourceId, profileItemId, from, to));
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective.  In any effective mode, effective profile
     *  entries and those currently expired are returned.
     *
     *  @param  agentId the {@code Id} of the agent
     *  @param  profileItemId the {@code Id} of the profile item
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code profileItemId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItem(org.osid.id.Id agentId,
                                                                                        org.osid.id.Id profileItemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForAgentAndProfileItem(agentId, profileItemId));
    }


    /**
     *  Gets a list of profile entries corresponding to agent and
     *  profile item {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known profile
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those profile entries that are accessible through
     *  this session.
     *
     *  In effective mode, profile entries are returned that are
     *  currently effective. In any effective mode, effective profile
     *  entries and those currently expired are returned.q
     *
     *  @param  profileItemId the {@code Id} of the profile item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProfileEntryList}
     *  @throws org.osid.NullArgumentException {@code agentId},
     *          {@code profileItemId}, {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesForAgentAndProfileItemOnDate(org.osid.id.Id agentId,
                                                                                              org.osid.id.Id profileItemId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntriesForAgentAndProfileItemOnDate(agentId, profileItemId, from, to));
    }


    /**
     *  Gets the explicit {@code ProfileEntry} that generated the
     *  given implicit profile entry. If the given {@code
     *  ProfileEntry} is explicit, then the same {@code ProfileEntry}
     *  is returned.
     *
     *  @param  profileEntryId a profile entry 
     *  @return the explicit {@code ProfileEntry} 
     *  @throws org.osid.NotFoundException {@code profileEntryId} is 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code profileEntryId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntry getExplicitProfileEntry(org.osid.id.Id profileEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getExplicitProfileEntry(profileEntryId));
    }


    /**
     *  Gets all {@code ProfileEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile entries or an error results. Otherwise, the returned list
     *  may contain only those profile entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, profile entries are returned that are currently
     *  effective.  In any effective mode, effective profile entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code ProfileEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfileEntries());
    }
}
