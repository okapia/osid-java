//
// AbstractImmutableJobConstrainerEnabler.java
//
//     Wraps a mutable JobConstrainerEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.rules.jobconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>JobConstrainerEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized JobConstrainerEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying jobConstrainerEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableJobConstrainerEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.resourcing.rules.JobConstrainerEnabler {

    private final org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableJobConstrainerEnabler</code>.
     *
     *  @param jobConstrainerEnabler the job constrainer enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        super(jobConstrainerEnabler);
        this.jobConstrainerEnabler = jobConstrainerEnabler;
        return;
    }


    /**
     *  Gets the job constrainer enabler record corresponding to the given 
     *  <code> JobConstrainerEnabler </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> jobConstrainerEnablerRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(jobConstrainerEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  jobConstrainerEnablerRecordType the type of job constrainer 
     *          enabler record to retrieve 
     *  @return the job constrainer enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(jobConstrainerEnablerRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerRecord getJobConstrainerEnablerRecord(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.jobConstrainerEnabler.getJobConstrainerEnablerRecord(jobConstrainerEnablerRecordType));
    }
}

