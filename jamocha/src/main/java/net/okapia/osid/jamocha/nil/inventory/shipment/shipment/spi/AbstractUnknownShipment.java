//
// AbstractUnknownShipment.java
//
//     Defines an unknown Shipment.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.inventory.shipment.shipment.spi;


/**
 *  Defines an unknown <code>Shipment</code>.
 */

public abstract class AbstractUnknownShipment
    extends net.okapia.osid.jamocha.inventory.shipment.shipment.spi.AbstractShipment
    implements org.osid.inventory.shipment.Shipment {

    protected static final String OBJECT = "osid.inventory.shipment.Shipment";


    /**
     *  Constructs a new <code>AbstractUnknownShipment</code>.
     */

    public AbstractUnknownShipment() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setSource(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownShipment</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownShipment(boolean optional) {
        this();

        setOrder(new net.okapia.osid.jamocha.nil.ordering.order.UnknownOrder());
        addEntry(new net.okapia.osid.jamocha.nil.inventory.shipment.entry.UnknownEntry());

        return;
    }
}
