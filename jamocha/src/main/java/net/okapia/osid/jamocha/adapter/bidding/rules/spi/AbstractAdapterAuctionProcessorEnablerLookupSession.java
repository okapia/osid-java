//
// AbstractAdapterAuctionProcessorEnablerLookupSession.java
//
//    An AuctionProcessorEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AuctionProcessorEnabler lookup session adapter.
 */

public abstract class AbstractAdapterAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {

    private final org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionProcessorEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionProcessorEnablerLookupSession(org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code AuctionProcessorEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctionProcessorEnablers() {
        return (this.session.canLookupAuctionProcessorEnablers());
    }


    /**
     *  A complete view of the {@code AuctionProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorEnablerView() {
        this.session.useComparativeAuctionProcessorEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code AuctionProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorEnablerView() {
        this.session.usePlenaryAuctionProcessorEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processor enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction processor enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorEnablerView() {
        this.session.useActiveAuctionProcessorEnablerView();
        return;
    }


    /**
     *  Active and inactive auction processor enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorEnablerView() {
        this.session.useAnyStatusAuctionProcessorEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code AuctionProcessorEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AuctionProcessorEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AuctionProcessorEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param auctionProcessorEnablerId {@code Id} of the {@code AuctionProcessorEnabler}
     *  @return the auction processor enabler
     *  @throws org.osid.NotFoundException {@code auctionProcessorEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionProcessorEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnabler getAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnabler(auctionProcessorEnablerId));
    }


    /**
     *  Gets an {@code AuctionProcessorEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessorEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AuctionProcessorEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AuctionProcessorEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByIds(org.osid.id.IdList auctionProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablersByIds(auctionProcessorEnablerIds));
    }


    /**
     *  Gets an {@code AuctionProcessorEnablerList} corresponding to the given
     *  auction processor enabler genus {@code Type} which does not include
     *  auction processor enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned {@code AuctionProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablersByGenusType(auctionProcessorEnablerGenusType));
    }


    /**
     *  Gets an {@code AuctionProcessorEnablerList} corresponding to the given
     *  auction processor enabler genus {@code Type} and include any additional
     *  auction processor enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned {@code AuctionProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByParentGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablersByParentGenusType(auctionProcessorEnablerGenusType));
    }


    /**
     *  Gets an {@code AuctionProcessorEnablerList} containing the given
     *  auction processor enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerRecordType an auctionProcessorEnabler record type 
     *  @return the returned {@code AuctionProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByRecordType(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablersByRecordType(auctionProcessorEnablerRecordType));
    }


    /**
     *  Gets an {@code AuctionProcessorEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code AuctionProcessorEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code AuctionProcessorEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code AuctionProcessorEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getAuctionProcessorEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code AuctionProcessorEnablers}. 
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @return a list of {@code AuctionProcessorEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorEnablers());
    }
}
