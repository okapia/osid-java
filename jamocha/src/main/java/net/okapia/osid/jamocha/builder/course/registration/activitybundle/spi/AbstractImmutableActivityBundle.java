//
// AbstractImmutableActivityBundle.java
//
//     Wraps a mutable ActivityBundle to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ActivityBundle</code> to hide modifiers. This
 *  wrapper provides an immutized ActivityBundle from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activityBundle whose state changes are visible.
 */

public abstract class AbstractImmutableActivityBundle
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.course.registration.ActivityBundle {

    private final org.osid.course.registration.ActivityBundle activityBundle;


    /**
     *  Constructs a new <code>AbstractImmutableActivityBundle</code>.
     *
     *  @param activityBundle the activity bundle to immutablize
     *  @throws org.osid.NullArgumentException <code>activityBundle</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        super(activityBundle);
        this.activityBundle = activityBundle;
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> associated with this 
     *  activity bundle. 
     *
     *  @return the course offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.activityBundle.getCourseOfferingId());
    }


    /**
     *  Gets the course offering associated with this activity bundle. 
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.activityBundle.getCourseOffering());
    }


    /**
     *  Gets the activity <code> Ids </code> in this bundle. 
     *
     *  @return <code> Ids </code> of the <code> l </code> earning objectives 
     */

    @OSID @Override
    public org.osid.id.IdList getActivityIds() {
        return (this.activityBundle.getActivityIds());
    }


    /**
     *  Gets the activities in this bundle. 
     *
     *  @return the activites 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException {

        return (this.activityBundle.getActivities());
    }


    /**
     *  Tests if registration to this bundle results in credits to be earned. 
     *
     *  @return <code> true </code> if this course has credits, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean definesCredits() {
        return (this.activityBundle.definesCredits());
    }


    /**
     *  Gets the number of credits available to register for in this course. 
     *  Each array element is a distinct option. 
     *
     *  @return the number of credits 
     *  @throws org.osid.IllegalStateException <code> definesCredits() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getCredits() {
        return (this.activityBundle.getCredits());
    }


    /**
     *  Tests if a registration to this bundle will be for for grades. 
     *
     *  @return <code> true </code> if this registration is for grades, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.activityBundle.isGraded());
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        return (this.activityBundle.getGradingOptionIds());
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        return (this.activityBundle.getGradingOptions());
    }


    /**
     *  Gets the activity bundle record corresponding to the given <code> 
     *  ActivityBundle </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  activityBundleRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(activityBundleRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  activityBundleRecordType the type of activity bundle record to 
     *          retrieve 
     *  @return the activity bundle record 
     *  @throws org.osid.NullArgumentException <code> activityBundleRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityBundleRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleRecord getActivityBundleRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        return (this.activityBundle.getActivityBundleRecord(activityBundleRecordType));
    }
}

