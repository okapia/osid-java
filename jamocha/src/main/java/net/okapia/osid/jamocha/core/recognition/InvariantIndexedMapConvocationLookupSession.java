//
// InvariantIndexedMapConvocationLookupSession
//
//    Implements a Convocation lookup service backed by a fixed
//    collection of convocations indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Convocation lookup service backed by a fixed
 *  collection of convocations. The convocations are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some convocations may be compatible
 *  with more types than are indicated through these convocation
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapConvocationLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractIndexedMapConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapConvocationLookupSession} using an
     *  array of convocations.
     *
     *  @param academy the academy
     *  @param convocations an array of convocations
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code convocations} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapConvocationLookupSession(org.osid.recognition.Academy academy,
                                                    org.osid.recognition.Convocation[] convocations) {

        setAcademy(academy);
        putConvocations(convocations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapConvocationLookupSession} using a
     *  collection of convocations.
     *
     *  @param academy the academy
     *  @param convocations a collection of convocations
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code convocations} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapConvocationLookupSession(org.osid.recognition.Academy academy,
                                                    java.util.Collection<? extends org.osid.recognition.Convocation> convocations) {

        setAcademy(academy);
        putConvocations(convocations);
        return;
    }
}
