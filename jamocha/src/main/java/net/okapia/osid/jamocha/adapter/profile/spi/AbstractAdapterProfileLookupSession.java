//
// AbstractAdapterProfileLookupSession.java
//
//    A Profile lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.profile.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Profile lookup session adapter.
 */

public abstract class AbstractAdapterProfileLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.profile.ProfileLookupSession {

    private final org.osid.profile.ProfileLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProfileLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProfileLookupSession(org.osid.profile.ProfileLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Profile} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProfiles() {
        return (this.session.canLookupProfiles());
    }


    /**
     *  A complete view of the {@code Profile} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileView() {
        this.session.useComparativeProfileView();
        return;
    }


    /**
     *  A complete view of the {@code Profile} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileView() {
        this.session.usePlenaryProfileView();
        return;
    }

     
    /**
     *  Gets the {@code Profile} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Profile} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Profile} and
     *  retained for compatibility.
     *
     *  @param profileId {@code Id} of the {@code Profile}
     *  @return the profile
     *  @throws org.osid.NotFoundException {@code profileId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code profileId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfile(profileId));
    }


    /**
     *  Gets a {@code ProfileList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profiles specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Profiles} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  profileIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Profile} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code profileIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByIds(org.osid.id.IdList profileIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfilesByIds(profileIds));
    }


    /**
     *  Gets a {@code ProfileList} corresponding to the given
     *  profile genus {@code Type} which does not include
     *  profiles of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileGenusType a profile genus type 
     *  @return the returned {@code Profile} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByGenusType(org.osid.type.Type profileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfilesByGenusType(profileGenusType));
    }


    /**
     *  Gets a {@code ProfileList} corresponding to the given
     *  profile genus {@code Type} and include any additional
     *  profiles with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileGenusType a profile genus type 
     *  @return the returned {@code Profile} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByParentGenusType(org.osid.type.Type profileGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfilesByParentGenusType(profileGenusType));
    }


    /**
     *  Gets a {@code ProfileList} containing the given
     *  profile record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileRecordType a profile record type 
     *  @return the returned {@code Profile} list
     *  @throws org.osid.NullArgumentException
     *          {@code profileRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByRecordType(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfilesByRecordType(profileRecordType));
    }


    /**
     *  Gets a {@code ProfileList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Profile} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfilesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfilesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Profiles}. 
     *
     *  In plenary mode, the returned list contains all known
     *  profiles or an error results. Otherwise, the returned list
     *  may contain only those profiles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Profiles} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProfiles());
    }
}
