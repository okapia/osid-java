//
// AbstractIndexedMapSupersedingEventEnablerLookupSession.java
//
//    A simple framework for providing a SupersedingEventEnabler lookup service
//    backed by a fixed collection of superseding event enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SupersedingEventEnabler lookup service backed by a
 *  fixed collection of superseding event enablers. The superseding event enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some superseding event enablers may be compatible
 *  with more types than are indicated through these superseding event enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SupersedingEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSupersedingEventEnablerLookupSession
    extends AbstractMapSupersedingEventEnablerLookupSession
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.SupersedingEventEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.SupersedingEventEnabler>());


    /**
     *  Makes a <code>SupersedingEventEnabler</code> available in this session.
     *
     *  @param  supersedingEventEnabler a superseding event enabler
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSupersedingEventEnabler(org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler) {
        super.putSupersedingEventEnabler(supersedingEventEnabler);

        this.supersedingEventEnablersByGenus.put(supersedingEventEnabler.getGenusType(), supersedingEventEnabler);
        
        try (org.osid.type.TypeList types = supersedingEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.supersedingEventEnablersByRecord.put(types.getNextType(), supersedingEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a superseding event enabler from this session.
     *
     *  @param supersedingEventEnablerId the <code>Id</code> of the superseding event enabler
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId) {
        org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler;
        try {
            supersedingEventEnabler = getSupersedingEventEnabler(supersedingEventEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.supersedingEventEnablersByGenus.remove(supersedingEventEnabler.getGenusType());

        try (org.osid.type.TypeList types = supersedingEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.supersedingEventEnablersByRecord.remove(types.getNextType(), supersedingEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSupersedingEventEnabler(supersedingEventEnablerId);
        return;
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> corresponding to the given
     *  superseding event enabler genus <code>Type</code> which does not include
     *  superseding event enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known superseding event enablers or an error results. Otherwise,
     *  the returned list may contain only those superseding event enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  supersedingEventEnablerGenusType a superseding event enabler genus type 
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByGenusType(org.osid.type.Type supersedingEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.ArraySupersedingEventEnablerList(this.supersedingEventEnablersByGenus.get(supersedingEventEnablerGenusType)));
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> containing the given
     *  superseding event enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known superseding event enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  superseding event enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  supersedingEventEnablerRecordType a superseding event enabler record type 
     *  @return the returned <code>supersedingEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByRecordType(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.ArraySupersedingEventEnablerList(this.supersedingEventEnablersByRecord.get(supersedingEventEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.supersedingEventEnablersByGenus.clear();
        this.supersedingEventEnablersByRecord.clear();

        super.close();

        return;
    }
}
