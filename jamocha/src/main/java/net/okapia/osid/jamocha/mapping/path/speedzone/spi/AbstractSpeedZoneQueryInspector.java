//
// AbstractSpeedZoneQueryInspector.java
//
//     A template for making a SpeedZoneQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.speedzone.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for speed zones.
 */

public abstract class AbstractSpeedZoneQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.mapping.path.SpeedZoneQueryInspector {

    private final java.util.Collection<org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (new org.osid.search.terms.CoordinateTerm[0]);
    }


    /**
     *  Gets the spatial unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainingSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the implicit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the speed limit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpeedRangeTerm[] getSpeedLimitTerms() {
        return (new org.osid.search.terms.SpeedRangeTerm[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given speed zone query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a speed zone implementing the requested record.
     *
     *  @param speedZoneRecordType a speed zone record type
     *  @return the speed zone query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>speedZoneRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(speedZoneRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord getSpeedZoneQueryInspectorRecord(org.osid.type.Type speedZoneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(speedZoneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(speedZoneRecordType + " is not supported");
    }


    /**
     *  Adds a record to this speed zone query. 
     *
     *  @param speedZoneQueryInspectorRecord speed zone query inspector
     *         record
     *  @param speedZoneRecordType speedZone record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSpeedZoneQueryInspectorRecord(org.osid.mapping.path.records.SpeedZoneQueryInspectorRecord speedZoneQueryInspectorRecord, 
                                                   org.osid.type.Type speedZoneRecordType) {

        addRecordType(speedZoneRecordType);
        nullarg(speedZoneRecordType, "speed zone record type");
        this.records.add(speedZoneQueryInspectorRecord);        
        return;
    }
}
