//
// AbstractVaultSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vault.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractVaultSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authorization.VaultSearchResults {

    private org.osid.authorization.VaultList vaults;
    private final org.osid.authorization.VaultQueryInspector inspector;
    private final java.util.Collection<org.osid.authorization.records.VaultSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractVaultSearchResults.
     *
     *  @param vaults the result set
     *  @param vaultQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>vaults</code>
     *          or <code>vaultQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractVaultSearchResults(org.osid.authorization.VaultList vaults,
                                            org.osid.authorization.VaultQueryInspector vaultQueryInspector) {
        nullarg(vaults, "vaults");
        nullarg(vaultQueryInspector, "vault query inspectpr");

        this.vaults = vaults;
        this.inspector = vaultQueryInspector;

        return;
    }


    /**
     *  Gets the vault list resulting from a search.
     *
     *  @return a vault list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaults() {
        if (this.vaults == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authorization.VaultList vaults = this.vaults;
        this.vaults = null;
	return (vaults);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authorization.VaultQueryInspector getVaultQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  vault search record <code> Type. </code> This method must
     *  be used to retrieve a vault implementing the requested
     *  record.
     *
     *  @param vaultSearchRecordType a vault search 
     *         record type 
     *  @return the vault search
     *  @throws org.osid.NullArgumentException
     *          <code>vaultSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(vaultSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultSearchResultsRecord getVaultSearchResultsRecord(org.osid.type.Type vaultSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authorization.records.VaultSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(vaultSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(vaultSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record vault search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addVaultRecord(org.osid.authorization.records.VaultSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "vault record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
