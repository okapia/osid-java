//
// AbstractAuctionProcessorSearch.java
//
//     A template for making an AuctionProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.rules.AuctionProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.rules.AuctionProcessorSearchOrder auctionProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auction processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionProcessorIds list of auction processors
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctionProcessors(org.osid.id.IdList auctionProcessorIds) {
        while (auctionProcessorIds.hasNext()) {
            try {
                this.ids.add(auctionProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctionProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionProcessorSearchOrder auction processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionProcessorResults(org.osid.bidding.rules.AuctionProcessorSearchOrder auctionProcessorSearchOrder) {
	this.auctionProcessorSearchOrder = auctionProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.rules.AuctionProcessorSearchOrder getAuctionProcessorSearchOrder() {
	return (this.auctionProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction processor implementing the requested record.
     *
     *  @param auctionProcessorSearchRecordType an auction processor search record
     *         type
     *  @return the auction processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorSearchRecord getAuctionProcessorSearchRecord(org.osid.type.Type auctionProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.rules.records.AuctionProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor search. 
     *
     *  @param auctionProcessorSearchRecord auction processor search record
     *  @param auctionProcessorSearchRecordType auctionProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorSearchRecord(org.osid.bidding.rules.records.AuctionProcessorSearchRecord auctionProcessorSearchRecord, 
                                           org.osid.type.Type auctionProcessorSearchRecordType) {

        addRecordType(auctionProcessorSearchRecordType);
        this.records.add(auctionProcessorSearchRecord);        
        return;
    }
}
