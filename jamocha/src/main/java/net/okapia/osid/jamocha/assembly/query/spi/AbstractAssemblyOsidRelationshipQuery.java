//
// AbstractAssemblyOsidRelationshipQuery.java
//
//     An OsidRelationshipQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidRelationshipQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidRelationshipQuery
    extends AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.OsidRelationshipQuery,
               org.osid.OsidRelationshipQueryInspector,
               org.osid.OsidRelationshipSearchOrder {

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidRelationshipQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidRelationshipQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    
    
    /**
     *  Adds a state Id to match. Multiple state Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  stateId state Id to match 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> stateId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchEndReasonId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getEndReasonIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears all state Id terms. 
     */

    @OSID @Override
    public void clearEndReasonIdTerms() {
        getAssembler().clearTerms(getEndReasonIdColumn());
        return;
    }


    /**
     *  Gets the state Id query terms.
     *
     *  @return the state Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndReasonIdTerms() {
        return (getAssembler().getIdTerms(getEndReasonIdColumn()));
    }


    /**
     *  Gets the column name for the state Id field.
     *
     *  @return the column name
     */

    protected String getEndReasonIdColumn() {
        return ("state_id");
    }


    /**
     *  Tests if a <code> EndReasonQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsEndReasonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getEndReasonQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsEndReasonQuery() is false");
    }


    /**
     *  Matches any relationship with a state Id.
     *
     *  @param match <code> true </code> to match any state Id,
     *          <code> false </code> to match relationships with no state
     *          Id
     */

    @OSID @Override
    public void matchAnyEndReason(boolean match) {
        getAssembler().addIdWildcardTerm(getEndReasonColumn(), match);
        return;
    }


    /**
     *  Clears all state terms. 
     */

    @OSID @Override
    public void clearEndReasonTerms() {
        getAssembler().clearTerms(getEndReasonColumn());
        return;
    }


    /**
     *  Gets the state query terms. 
     *
     *  @return the state query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getEndReasonTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Specifies a preference for ordering the results by the end
     *  reason state.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndReason(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndReasonColumn(), style);
        return;
    }

    
    /**
     *  Tests if a <code> StateSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a state. 
     *
     *  @return the state search order 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsEndReasonSearchOrder() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getEndReasonSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndReasonSearchOrder() is false");
    }


    /**
     *  Gets the column name for the state field.
     *
     *  @return the column name
     */

    protected String getEndReasonColumn() {
        return ("end_reason");
    }
}
