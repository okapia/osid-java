//
// AbstractImmutableBook.java
//
//     Wraps a mutable Book to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.commenting.book.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Book</code> to hide modifiers. This
 *  wrapper provides an immutized Book from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying book whose state changes are visible.
 */

public abstract class AbstractImmutableBook
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.commenting.Book {

    private final org.osid.commenting.Book book;


    /**
     *  Constructs a new <code>AbstractImmutableBook</code>.
     *
     *  @param book the book to immutablize
     *  @throws org.osid.NullArgumentException <code>book</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableBook(org.osid.commenting.Book book) {
        super(book);
        this.book = book;
        return;
    }


    /**
     *  Gets the book record corresponding to the given <code> Book </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> bookRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(bookRecordType) </code> is <code> true </code> . 
     *
     *  @param  bookRecordType the type of book record to retrieve 
     *  @return the book record 
     *  @throws org.osid.NullArgumentException <code> bookRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(bookRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.records.BookRecord getBookRecord(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException {

        return (this.book.getBookRecord(bookRecordType));
    }
}

