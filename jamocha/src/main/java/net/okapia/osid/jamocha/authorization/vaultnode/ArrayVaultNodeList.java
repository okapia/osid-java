//
// ArrayVaultNodeList.java
//
//     Implements a VaultNodeList based on an array.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vaultnode;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ObjectList based on an array of VaultNodes.
 */

public final class ArrayVaultNodeList
    extends net.okapia.osid.jamocha.authorization.vaultnode.spi.AbstractVaultNodeList
    implements org.osid.authorization.VaultNodeList {

    private final org.osid.authorization.VaultNode[] list;
    private int index = 0;


    /**
     *  Creates a new empty <code>ArrayVaultNodeList</code>.
     */

    public ArrayVaultNodeList() {
        this.list = new org.osid.authorization.VaultNode[0];
        return;
    }


    /**
     *  Creates a new <code>ArrayVaultNodeList</code>.
     *
     *  @param array an array of vaultnodes
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public ArrayVaultNodeList(org.osid.authorization.VaultNode[] array) {
        nullarg(array, "array");
        this.list = array;
        return;
    }


    /**
     *  Creates a new <code>ArrayVaultNodeList</code>.
     *
     *  @param collection a java.util.Collection of vaultnodes
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public ArrayVaultNodeList(java.util.Collection<? extends org.osid.authorization.VaultNode> collection) {
        nullarg(collection, "collection");
        this.list = collection.toArray(new org.osid.authorization.VaultNode[collection.size()]);
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.index < this.list.length) {
            return (true);
        }

        return (false);
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        return (this.list.length - this.index);
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        this.index += n;
        if (this.index > this.list.length) {
            this.index = this.list.length;
        }

        return;
    }


    /**
     *  Gets the next <code>VaultNode</code> in this list. 
     *
     *  @return the next <code>VaultNode</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>VaultNode</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.VaultNode getNextVaultNode()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            org.osid.authorization.VaultNode ret = this.list[this.index];
            this.list[this.index++] = null;
            return (ret);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in vaultnode list");
        }
    }
}
