//
// AbstractUnknownPosition.java
//
//     Defines an unknown Position.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.personnel.position.spi;


/**
 *  Defines an unknown <code>Position</code>.
 */

public abstract class AbstractUnknownPosition
    extends net.okapia.osid.jamocha.personnel.position.spi.AbstractPosition
    implements org.osid.personnel.Position {

    protected static final String OBJECT = "osid.personnel.Position";


    /**
     *  Constructs a new <code>AbstractUnknownPosition</code>.
     */

    public AbstractUnknownPosition() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setOrganization(new net.okapia.osid.jamocha.nil.personnel.organization.UnknownOrganization());
        setTitle(net.okapia.osid.jamocha.nil.privateutil.Text.valueOf("title"));
        setLevel(new net.okapia.osid.jamocha.nil.grading.grade.UnknownGrade());
        setCompensationFrequency(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        setBenefitsType(new net.okapia.osid.primordium.type.BasicType("okapia.net", "benefits", "unknown"));
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownPosition</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownPosition(boolean optional) {
        this();
        
        addQualification(new net.okapia.osid.jamocha.nil.learning.objective.UnknownObjective());
        setLowSalaryRange(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setMidpointSalaryRange(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setHighSalaryRange(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        
        return;
    }
}
