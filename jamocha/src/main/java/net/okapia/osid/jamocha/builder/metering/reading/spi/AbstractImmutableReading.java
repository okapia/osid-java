//
// AbstractImmutableReading.java
//
//     Wraps a mutable Reading to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.metering.reading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Reading</code> to hide modifiers. This
 *  wrapper provides an immutized Reading from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying reading whose state changes are visible.
 */

public abstract class AbstractImmutableReading
    implements org.osid.metering.Reading {

    private final org.osid.metering.Reading reading;


    /**
     *  Constructs a new <code>AbstractImmutableReading</code>.
     *
     *  @param reading the reading to immutablize
     *  @throws org.osid.NullArgumentException <code>reading</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableReading(org.osid.metering.Reading reading) {
        this.reading = reading;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Meter </code> associated with 
     *  this reading. 
     *
     *  @return gets the <code> Id </code> of the <code> Meter </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeterId() {
        return (this.reading.getMeterId());
    }


    /**
     *  Gets the <code> Meter </code> associated with this reading. 
     *
     *  @return gets the <code> Meter </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.metering.Meter getMeter()
        throws org.osid.OperationFailedException {

        return (this.reading.getMeter());
    }


    /**
     *  Gets the metered object associated with this reading. 
     *
     *  @return gets the metered object <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMeteredObjectId() {
        return (this.reading.getMeteredObjectId());
    }


    /**
     *  Gets the amount of this reading. 
     *
     *  @return gets the amount of this reading 
     */

    @OSID @Override
    public java.math.BigDecimal getAmount() {
        return (this.reading.getAmount());
    }
}

