//
// InvariantMapParameterProcessorLookupSession
//
//    Implements a ParameterProcessor lookup service backed by a fixed collection of
//    parameterProcessors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessor lookup service backed by a fixed
 *  collection of parameter processors. The parameter processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractMapParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorLookupSession</code> with no
     *  parameter processors.
     *  
     *  @param configuration the configuration
     *  @throws org.osid.NullArgumnetException {@code configuration} is
     *          {@code null}
     */

    public InvariantMapParameterProcessorLookupSession(org.osid.configuration.Configuration configuration) {
        setConfiguration(configuration);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorLookupSession</code> with a single
     *  parameter processor.
     *  
     *  @param configuration the configuration
     *  @param parameterProcessor a single parameter processor
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessor} is <code>null</code>
     */

      public InvariantMapParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.rules.ParameterProcessor parameterProcessor) {
        this(configuration);
        putParameterProcessor(parameterProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorLookupSession</code> using an array
     *  of parameter processors.
     *  
     *  @param configuration the configuration
     *  @param parameterProcessors an array of parameter processors
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessors} is <code>null</code>
     */

      public InvariantMapParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                               org.osid.configuration.rules.ParameterProcessor[] parameterProcessors) {
        this(configuration);
        putParameterProcessors(parameterProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapParameterProcessorLookupSession</code> using a
     *  collection of parameter processors.
     *
     *  @param configuration the configuration
     *  @param parameterProcessors a collection of parameter processors
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code parameterProcessors} is <code>null</code>
     */

      public InvariantMapParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                               java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessor> parameterProcessors) {
        this(configuration);
        putParameterProcessors(parameterProcessors);
        return;
    }
}
