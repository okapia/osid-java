//
// AbstractActivityRegistrationSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractActivityRegistrationSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.registration.ActivityRegistrationSearchResults {

    private org.osid.course.registration.ActivityRegistrationList activityRegistrations;
    private final org.osid.course.registration.ActivityRegistrationQueryInspector inspector;
    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractActivityRegistrationSearchResults.
     *
     *  @param activityRegistrations the result set
     *  @param activityRegistrationQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>activityRegistrations</code>
     *          or <code>activityRegistrationQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractActivityRegistrationSearchResults(org.osid.course.registration.ActivityRegistrationList activityRegistrations,
                                            org.osid.course.registration.ActivityRegistrationQueryInspector activityRegistrationQueryInspector) {
        nullarg(activityRegistrations, "activity registrations");
        nullarg(activityRegistrationQueryInspector, "activity registration query inspectpr");

        this.activityRegistrations = activityRegistrations;
        this.inspector = activityRegistrationQueryInspector;

        return;
    }


    /**
     *  Gets the activity registration list resulting from a search.
     *
     *  @return an activity registration list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrations() {
        if (this.activityRegistrations == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.registration.ActivityRegistrationList activityRegistrations = this.activityRegistrations;
        this.activityRegistrations = null;
	return (activityRegistrations);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.registration.ActivityRegistrationQueryInspector getActivityRegistrationQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  activity registration search record <code> Type. </code> This method must
     *  be used to retrieve an activityRegistration implementing the requested
     *  record.
     *
     *  @param activityRegistrationSearchRecordType an activityRegistration search 
     *         record type 
     *  @return the activity registration search
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(activityRegistrationSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationSearchResultsRecord getActivityRegistrationSearchResultsRecord(org.osid.type.Type activityRegistrationSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.registration.records.ActivityRegistrationSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(activityRegistrationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record activity registration search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addActivityRegistrationRecord(org.osid.course.registration.records.ActivityRegistrationSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "activity registration record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
