//
// AbstractActivityBundleValidator.java
//
//     Validates an ActivityBundle.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.registration.activitybundle.spi;


/**
 *  Validates an ActivityBundle.
 */

public abstract class AbstractActivityBundleValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractActivityBundleValidator</code>.
     */

    protected AbstractActivityBundleValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractActivityBundleValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractActivityBundleValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an ActivityBundle.
     *
     *  @param activityBundle an activity bundle to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>activityBundle</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.registration.ActivityBundle activityBundle) {
        super.validate(activityBundle);

        testNestedObject(activityBundle, "getCourseOffering");
        testNestedObjects(activityBundle, "getActivityIds", "getActivities");

        testConditionalMethod(activityBundle, "getCredits", activityBundle.definesCredits(), "definesCredits()");

        testConditionalMethod(activityBundle, "getGradingOptionIds", activityBundle.isGraded(), "isGraded()");
        testConditionalMethod(activityBundle, "getGradingOptions", activityBundle.isGraded(), "isGraded()");
        if (activityBundle.isGraded()) {
            testNestedObjects(activityBundle, "getGradingOptionIds", "getGradingOptions");
        }

        return;
    }
}
