//
// InvariantMapProxyQueueLookupSession
//
//    Implements a Queue lookup service backed by a fixed
//    collection of queues. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking;


/**
 *  Implements a Queue lookup service backed by a fixed
 *  collection of queues. The queues are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyQueueLookupSession
    extends net.okapia.osid.jamocha.core.tracking.spi.AbstractMapQueueLookupSession
    implements org.osid.tracking.QueueLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyQueueLookupSession} with no
     *  queues.
     *
     *  @param frontOffice the front office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.proxy.Proxy proxy) {
        setFrontOffice(frontOffice);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyQueueLookupSession} with a single
     *  queue.
     *
     *  @param frontOffice the front office
     *  @param queue a single queue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queue} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.tracking.Queue queue, org.osid.proxy.Proxy proxy) {

        this(frontOffice, proxy);
        putQueue(queue);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyQueueLookupSession} using
     *  an array of queues.
     *
     *  @param frontOffice the front office
     *  @param queues an array of queues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queues} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.tracking.Queue[] queues, org.osid.proxy.Proxy proxy) {

        this(frontOffice, proxy);
        putQueues(queues);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyQueueLookupSession} using a
     *  collection of queues.
     *
     *  @param frontOffice the front office
     *  @param queues a collection of queues
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queues} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyQueueLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  java.util.Collection<? extends org.osid.tracking.Queue> queues,
                                                  org.osid.proxy.Proxy proxy) {

        this(frontOffice, proxy);
        putQueues(queues);
        return;
    }
}
