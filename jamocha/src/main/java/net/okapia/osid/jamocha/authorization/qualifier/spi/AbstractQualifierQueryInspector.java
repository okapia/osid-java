//
// AbstractQualifierQueryInspector.java
//
//     A template for making a QualifierQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.qualifier.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for qualifiers.
 */

public abstract class AbstractQualifierQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.authorization.QualifierQueryInspector {

    private final java.util.Collection<org.osid.authorization.records.QualifierQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the qualifier hierarchy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierHierarchyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the qualifier hierarchy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQueryInspector[] getQualifierHierarchyTerms() {
        return (new org.osid.hierarchy.HierarchyQueryInspector[0]);
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the ancestor qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorQualifierIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getAncestorQualifierTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the descendant qualifier <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantQualifierIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant qualifier query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQueryInspector[] getDescendantQualifierTerms() {
        return (new org.osid.authorization.FunctionQueryInspector[0]);
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given qualifier query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a qualifier implementing the requested record.
     *
     *  @param qualifierRecordType a qualifier record type
     *  @return the qualifier query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierQueryInspectorRecord getQualifierQueryInspectorRecord(org.osid.type.Type qualifierRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.QualifierQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(qualifierRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierRecordType + " is not supported");
    }


    /**
     *  Adds a record to this qualifier query. 
     *
     *  @param qualifierQueryInspectorRecord qualifier query inspector
     *         record
     *  @param qualifierRecordType qualifier record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQualifierQueryInspectorRecord(org.osid.authorization.records.QualifierQueryInspectorRecord qualifierQueryInspectorRecord, 
                                                   org.osid.type.Type qualifierRecordType) {

        addRecordType(qualifierRecordType);
        nullarg(qualifierRecordType, "qualifier record type");
        this.records.add(qualifierQueryInspectorRecord);        
        return;
    }
}
