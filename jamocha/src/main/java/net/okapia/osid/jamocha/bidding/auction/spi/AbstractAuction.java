//
// AbstractAuction.java
//
//     Defines an Auction.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Auction</code>.
 */

public abstract class AbstractAuction
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.bidding.Auction {

    private org.osid.type.Type currencyType;
    private long minimumBidders = 1;
    private boolean sealed;
    private org.osid.resource.Resource seller;
    private org.osid.resource.Resource item;

    private long lotSize        = 0;
    private long remainingItems = 0;
    private long itemLimit      = -1;

    private org.osid.financials.Currency startingPrice;
    private org.osid.financials.Currency priceIncrement;
    private org.osid.financials.Currency reservePrice;
    private org.osid.financials.Currency buyoutPrice;

    private final java.util.Collection<org.osid.bidding.records.AuctionRecord> records = new java.util.LinkedHashSet<>();

    private final Temporal temporal = new Temporal();
    

    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        this.temporal.setStartDate(date);
        return;
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        this.temporal.setEndDate(date);
        return;
    }


    /**
     *  Gets the currency type used in this auction. 
     *
     *  @return currency type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyType() {
        return (this.currencyType);
    }


    /**
     *  Sets the currency type.
     *
     *  @param currencyType a currency type
     *  @throws org.osid.NullArgumentException
     *          <code>currencyType</code> is <code>null</code>
     */

    protected void setCurrencyType(org.osid.type.Type currencyType) {
        nullarg(currencyType, "currency type");
        this.currencyType = currencyType;
        return;
    }


    /**
     *  Gets the minimum number of bidders required for this auction to 
     *  complete. 
     *
     *  @return the minimum number of bidders 
     */

    @OSID @Override
    public long getMinimumBidders() {
        return (this.minimumBidders);
    }


    /**
     *  Sets the minimum bidders.
     *
     *  @param bidders the minimum bidders
     *  @throws org.osid.InvalidArgumentException <code>bidders</code>
     *          is negative
     */

    protected void setMinimumBidders(long bidders) {
        cardinalarg(bidders, "minimum bidders");
        this.minimumBidders = bidders;
        return;
    }


    /**
     *  Tests if bids in this auction are visible. 
     *
     *  @return <code> true </code> if this auction is sealed, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isSealed() {
        return (this.sealed);
    }


    /**
     *  Sets the sealed flag.
     *
     *  @param sealed <code> true </code> if this auction is sealed,
     *         <code> false </code> otherwise
     */

    protected void setSealed(boolean sealed) {
        this.sealed = sealed;
        return;
    }


    /**
     *  Gets the seller <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSellerId() {
        return (this.seller.getId());
    }


    /**
     *  Gets the seller. 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSeller()
        throws org.osid.OperationFailedException {

        return (this.seller);
    }


    /**
     *  Sets the seller.
     *
     *  @param seller a seller
     *  @throws org.osid.NullArgumentException
     *          <code>seller</code> is <code>null</code>
     */

    protected void setSeller(org.osid.resource.Resource seller) {
        nullarg(seller, "seller");
        this.seller = seller;
        return;
    }


    /**
     *  Gets the item <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.item.getId());
    }


    /**
     *  Gets the item. 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getItem()
        throws org.osid.OperationFailedException {

        return (this.item);
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    protected void setItem(org.osid.resource.Resource item) {
        nullarg(item, "item");
        this.item = item;
        return;
    }


    /**
     *  Gets the number of items up for bid. 
     *
     *  @return the initial number of items 
     */

    @OSID @Override
    public long getLotSize() {
        return (this.lotSize);
    }


    /**
     *  Sets the lot size.
     *
     *  @param size a lot size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    protected void setLotSize(long size) {
        cardinalarg(size, "lot size");
        this.lotSize = size;
        return;
    }


    /**
     *  Gets the number of items remaining. 
     *
     *  @return the number of items remaining 
     */

    @OSID @Override
    public long getRemainingItems() {
        return (this.remainingItems);
    }


    /**
     *  Sets the remaining items.
     *
     *  @param number the remaining items
     *  @throws org.osid.InvalidArgumentException <code>number</code>
     *          is negative
     */

    protected void setRemainingItems(long number) {
        cardinalarg(number, "remaining items");
        this.remainingItems = number;
        return;
    }


    /**
     *  Tests if this auction has a maxmimum number of items that can
     *  be bought.
     *
     *  @return <code> true </code> if the auction has an item limit,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean limitsItems() {
        return (this.itemLimit >= 0);
    }


    /**
     *  Gets the limit on the number of items that can be bought. 
     *
     *  @return the item limit 
     *  @throws org.osid.IllegalStateException <code> limitsItems()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public long getItemLimit() {
        if (!limitsItems()) {
            throw new org.osid.IllegalStateException("limitsItems() is null");
        }

        return (this.itemLimit);
    }


    /**
     *  Sets the item limit.
     *
     *  @param limit an item limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    protected void setItemLimit(long limit) {
        cardinalarg(limit, "item limit");
        this.itemLimit = limit;
        return;
    }


    /**
     *  Gets the starting price per item. 
     *
     *  @return the starting price 
     */

    @OSID @Override
    public org.osid.financials.Currency getStartingPrice() {
        return (this.startingPrice);
    }


    /**
     *  Sets the starting price.
     *
     *  @param price a starting price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    protected void setStartingPrice(org.osid.financials.Currency price) {
        nullarg(price, "starting price");
        this.startingPrice = price;
        return;
    }


    /**
     *  Gets the minimum increment amount for successive bids. 
     *
     *  @return the increment 
     */

    @OSID @Override
    public org.osid.financials.Currency getPriceIncrement() {
        return (this.priceIncrement);
    }


    /**
     *  Sets the price increment.
     *
     *  @param increment a price increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    protected void setPriceIncrement(org.osid.financials.Currency increment) {
        nullarg(increment, "price increment");
        this.priceIncrement = increment;
        return;
    }


    /**
     *  Tests if a buyout price is available. 
     *
     *  @return <code> true </code> if the auction has a reserve
     *          price, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasReservePrice() {
        return (this.reservePrice != null);
    }


    /**
     *  Gets the reserve price. 
     *
     *  @return the reserve price 
     *  @throws org.osid.IllegalStateException <code> hasReservePrice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getReservePrice() {
        if (!hasReservePrice()) {
            throw new org.osid.IllegalStateException("hasReservePrice() is false");
        }

        return (this.reservePrice);
    }


    /**
     *  Sets the reserve price.
     *
     *  @param price a reserve price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    protected void setReservePrice(org.osid.financials.Currency price) {
        nullarg(price, "reserve price");
        this.reservePrice = price;
        return;
    }


    /**
     *  Tests if a buyout price is available. 
     *
     *  @return <code> true </code> if the item can be bought out,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasBuyoutPrice() {
        return (this.buyoutPrice != null);
    }


    /**
     *  Gets the buyout price. 
     *
     *  @return the buyout price 
     *  @throws org.osid.IllegalStateException <code> hasBuyoutPrice()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.financials.Currency getBuyoutPrice() {
        if (!hasBuyoutPrice()) {
            throw new org.osid.IllegalStateException("hasBuyoutPrice() is false");
        }

        return (this.buyoutPrice);
    }


    /**
     *  Sets the buyout price.
     *
     *  @param price a buyout price
     *  @throws org.osid.NullArgumentException
     *          <code>price</code> is <code>null</code>
     */

    protected void setBuyoutPrice(org.osid.financials.Currency price) {
        nullarg(price, "buyout price");
        this.buyoutPrice = price;
        return;
    }


    /**
     *  Tests if this auction supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return <code>true</code> if the auctionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionRecordType) {
        for (org.osid.bidding.records.AuctionRecord record : this.records) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Auction</code> record <code>Type</code>.
     *
     *  @param  auctionRecordType the auction record type 
     *  @return the auction record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionRecord getAuctionRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionRecord record : this.records) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionRecord the auction record
     *  @param auctionRecordType auction record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecord</code> or
     *          <code>auctionRecordType</code> is <code>null</code>
     */
            
    protected void addAuctionRecord(org.osid.bidding.records.AuctionRecord auctionRecord, 
                                    org.osid.type.Type auctionRecordType) {

        nullarg(auctionRecord, "auction record");
        addRecordType(auctionRecordType);
        this.records.add(auctionRecord);
        
        return;
    }


    protected class Temporal
        extends net.okapia.osid.jamocha.spi.AbstractTemporal
        implements org.osid.Temporal {

        
        /**
         *  Sets the start date.
         *
         *  @param date the start date
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void setStartDate(org.osid.calendaring.DateTime date) {
            super.setStartDate(date);
            return;
        }


        /**
         *  Sets the end date.
         *
         *  @param date the end date
         *  @throws org.osid.NullArgumentException <code>date</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void setEndDate(org.osid.calendaring.DateTime date) {
            super.setEndDate(date);
            return;
        }
    }        
}
