//
// AbstractBallot.java
//
//     Defines a Ballot.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Ballot</code>.
 */

public abstract class AbstractBallot
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.voting.Ballot {

    private boolean canRevote = false;
    private final java.util.Collection<org.osid.voting.records.BallotRecord> records = new java.util.LinkedHashSet<>();

    private final Temporal temporal = new Temporal();


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        this.temporal.setStartDate(date);
        return;
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        this.temporal.setEndDate(date);
        return;
    }


    /**
     *  Tests if races on this ballot can be modified before the election 
     *  ends. 
     *
     *  @return <code> true </code> if votes can be modified, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean canRevote() {
        return (this.canRevote);
    }


    /**
     *  Sets the can revote.
     *
     *  @param canRevote a can revote
     *  @throws org.osid.NullArgumentException
     *          <code>canRevote</code> is <code>null</code>
     */

    protected void setCanRevote(boolean canRevote) {
        this.canRevote = canRevote;
        return;
    }


    /**
     *  Tests if this ballot supports the given record
     *  <code>Type</code>.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return <code>true</code> if the ballotRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ballotRecordType) {
        for (org.osid.voting.records.BallotRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Ballot</code>
     *  record <code>Type</code>.
     *
     *  @param  ballotRecordType the ballot record type 
     *  @return the ballot record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotRecord getBallotRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ballotRecord the ballot record
     *  @param ballotRecordType ballot record type
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecord</code> or
     *          <code>ballotRecordTypeballot</code> is
     *          <code>null</code>
     */
            
    protected void addBallotRecord(org.osid.voting.records.BallotRecord ballotRecord, 
                                   org.osid.type.Type ballotRecordType) {

        nullarg(ballotRecord, "ballot record");
        addRecordType(ballotRecordType);
        this.records.add(ballotRecord);
        
        return;
    }


    protected class Temporal
        extends net.okapia.osid.jamocha.spi.AbstractTemporal
        implements org.osid.Temporal {

        
        /**
         *  Sets the start date.
         *
         *  @param date the start date
         *  @throws org.osid.NullArgumentException <code>date</code> is
         *          <code>null</code>
         */
        
        @Override
        protected void setStartDate(org.osid.calendaring.DateTime date) {
            super.setStartDate(date);
            return;
        }


        /**
         *  Sets the end date.
         *
         *  @param date the end date
         *  @throws org.osid.NullArgumentException <code>date</code> is
         *          <code>null</code>
         */
        
        @Override
        protected void setEndDate(org.osid.calendaring.DateTime date) {
            super.setEndDate(date);
            return;
        }
    }        
}
