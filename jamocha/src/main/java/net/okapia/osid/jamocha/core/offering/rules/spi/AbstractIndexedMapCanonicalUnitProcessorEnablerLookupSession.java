//
// AbstractIndexedMapCanonicalUnitProcessorEnablerLookupSession.java
//
//    A simple framework for providing a CanonicalUnitProcessorEnabler lookup service
//    backed by a fixed collection of canonical unit processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CanonicalUnitProcessorEnabler lookup service backed by a
 *  fixed collection of canonical unit processor enablers. The canonical unit processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some canonical unit processor enablers may be compatible
 *  with more types than are indicated through these canonical unit processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CanonicalUnitProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCanonicalUnitProcessorEnablerLookupSession
    extends AbstractMapCanonicalUnitProcessorEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.rules.CanonicalUnitProcessorEnabler>());


    /**
     *  Makes a <code>CanonicalUnitProcessorEnabler</code> available in this session.
     *
     *  @param  canonicalUnitProcessorEnabler a canonical unit processor enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessorEnabler(org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler) {
        super.putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);

        this.canonicalUnitProcessorEnablersByGenus.put(canonicalUnitProcessorEnabler.getGenusType(), canonicalUnitProcessorEnabler);
        
        try (org.osid.type.TypeList types = canonicalUnitProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitProcessorEnablersByRecord.put(types.getNextType(), canonicalUnitProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of canonical unit processor enablers available in this session.
     *
     *  @param  canonicalUnitProcessorEnablers an array of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessorEnablers(org.osid.offering.rules.CanonicalUnitProcessorEnabler[] canonicalUnitProcessorEnablers) {
        for (org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler : canonicalUnitProcessorEnablers) {
            putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of canonical unit processor enablers available in this session.
     *
     *  @param  canonicalUnitProcessorEnablers a collection of canonical unit processor enablers
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putCanonicalUnitProcessorEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers) {
        for (org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler : canonicalUnitProcessorEnablers) {
            putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a canonical unit processor enabler from this session.
     *
     *  @param canonicalUnitProcessorEnablerId the <code>Id</code> of the canonical unit processor enabler
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCanonicalUnitProcessorEnabler(org.osid.id.Id canonicalUnitProcessorEnablerId) {
        org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler;
        try {
            canonicalUnitProcessorEnabler = getCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.canonicalUnitProcessorEnablersByGenus.remove(canonicalUnitProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = canonicalUnitProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.canonicalUnitProcessorEnablersByRecord.remove(types.getNextType(), canonicalUnitProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> corresponding to the given
     *  canonical unit processor enabler genus <code>Type</code> which does not include
     *  canonical unit processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known canonical unit processor enablers or an error results. Otherwise,
     *  the returned list may contain only those canonical unit processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  canonicalUnitProcessorEnablerGenusType a canonical unit processor enabler genus type 
     *  @return the returned <code>CanonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByGenusType(org.osid.type.Type canonicalUnitProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.ArrayCanonicalUnitProcessorEnablerList(this.canonicalUnitProcessorEnablersByGenus.get(canonicalUnitProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>CanonicalUnitProcessorEnablerList</code> containing the given
     *  canonical unit processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known canonical unit processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  canonical unit processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type 
     *  @return the returned <code>canonicalUnitProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablersByRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.ArrayCanonicalUnitProcessorEnablerList(this.canonicalUnitProcessorEnablersByRecord.get(canonicalUnitProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.canonicalUnitProcessorEnablersByGenus.clear();
        this.canonicalUnitProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
