//
// InvariantMapSequenceRuleLookupSession
//
//    Implements a SequenceRule lookup service backed by a fixed collection of
//    sequenceRules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRule lookup service backed by a fixed
 *  collection of sequence rules. The sequence rules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSequenceRuleLookupSession</code> with no
     *  sequence rules.
     *  
     *  @param bank the bank
     *  @throws org.osid.NullArgumnetException {@code bank} is
     *          {@code null}
     */

    public InvariantMapSequenceRuleLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSequenceRuleLookupSession</code> with a single
     *  sequence rule.
     *  
     *  @param bank the bank
     *  @param sequenceRule a single sequence rule
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRule} is <code>null</code>
     */

      public InvariantMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.authoring.SequenceRule sequenceRule) {
        this(bank);
        putSequenceRule(sequenceRule);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSequenceRuleLookupSession</code> using an array
     *  of sequence rules.
     *  
     *  @param bank the bank
     *  @param sequenceRules an array of sequence rules
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRules} is <code>null</code>
     */

      public InvariantMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.authoring.SequenceRule[] sequenceRules) {
        this(bank);
        putSequenceRules(sequenceRules);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSequenceRuleLookupSession</code> using a
     *  collection of sequence rules.
     *
     *  @param bank the bank
     *  @param sequenceRules a collection of sequence rules
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code sequenceRules} is <code>null</code>
     */

      public InvariantMapSequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                               java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules) {
        this(bank);
        putSequenceRules(sequenceRules);
        return;
    }
}
