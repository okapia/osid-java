//
// MutableMapProxyCampusLookupSession
//
//    Implements a Campus lookup service backed by a collection of
//    campuses that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room;


/**
 *  Implements a Campus lookup service backed by a collection of
 *  campuses. The campuses are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of campuses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCampusLookupSession
    extends net.okapia.osid.jamocha.core.room.spi.AbstractMapCampusLookupSession
    implements org.osid.room.CampusLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCampusLookupSession} with no
     *  campuses.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyCampusLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCampusLookupSession} with a
     *  single campus.
     *
     *  @param campus a campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCampusLookupSession(org.osid.room.Campus campus, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCampus(campus);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCampusLookupSession} using an
     *  array of campuses.
     *
     *  @param campuses an array of campuses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campuses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCampusLookupSession(org.osid.room.Campus[] campuses, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCampuses(campuses);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCampusLookupSession} using
     *  a collection of campuses.
     *
     *  @param campuses a collection of campuses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campuses} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyCampusLookupSession(java.util.Collection<? extends org.osid.room.Campus> campuses,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putCampuses(campuses);
        return;
    }

    
    /**
     *  Makes a {@code Campus} available in this session.
     *
     *  @param campus an campus
     *  @throws org.osid.NullArgumentException {@code campus{@code 
     *          is {@code null}
     */

    @Override
    public void putCampus(org.osid.room.Campus campus) {
        super.putCampus(campus);
        return;
    }


    /**
     *  Makes an array of campuses available in this session.
     *
     *  @param campuses an array of campuses
     *  @throws org.osid.NullArgumentException {@code campuses{@code 
     *          is {@code null}
     */

    @Override
    public void putCampuses(org.osid.room.Campus[] campuses) {
        super.putCampuses(campuses);
        return;
    }


    /**
     *  Makes collection of campuses available in this session.
     *
     *  @param campuses
     *  @throws org.osid.NullArgumentException {@code campus{@code 
     *          is {@code null}
     */

    @Override
    public void putCampuses(java.util.Collection<? extends org.osid.room.Campus> campuses) {
        super.putCampuses(campuses);
        return;
    }


    /**
     *  Removes a Campus from this session.
     *
     *  @param campusId the {@code Id} of the campus
     *  @throws org.osid.NullArgumentException {@code campusId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCampus(org.osid.id.Id campusId) {
        super.removeCampus(campusId);
        return;
    }    
}
