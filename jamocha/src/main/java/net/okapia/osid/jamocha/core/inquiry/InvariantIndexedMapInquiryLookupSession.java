//
// InvariantIndexedMapInquiryLookupSession
//
//    Implements an Inquiry lookup service backed by a fixed
//    collection of inquiries indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Inquiry lookup service backed by a fixed
 *  collection of inquiries. The inquiries are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some inquiries may be compatible
 *  with more types than are indicated through these inquiry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapInquiryLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractIndexedMapInquiryLookupSession
    implements org.osid.inquiry.InquiryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInquiryLookupSession} using an
     *  array of inquiries.
     *
     *  @param inquest the inquest
     *  @param inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                    org.osid.inquiry.Inquiry[] inquiries) {

        setInquest(inquest);
        putInquiries(inquiries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInquiryLookupSession} using a
     *  collection of inquiries.
     *
     *  @param inquest the inquest
     *  @param inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                    java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {

        setInquest(inquest);
        putInquiries(inquiries);
        return;
    }
}
