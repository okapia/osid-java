//
// AbstractCourseQueryInspector.java
//
//     A template for making a CourseQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for courses.
 */

public abstract class AbstractCourseQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.course.CourseQueryInspector {

    private final java.util.Collection<org.osid.course.records.CourseQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditAmountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade query terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getCreditAmountTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the prerequisite query terms. 
     *
     *  @return the prereq query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPrerequisitesInfoTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the requisite <code> Id </code> query terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPrerequisitesIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the requisite query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getPrerequisitesTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the grade level <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLevelIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade level query terms. 
     *
     *  @return the grade query terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getLevelTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the activity unit <code> Id </code> query terms. 
     *
     *  @return the activity unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity unit query terms. 
     *
     *  @return the activity unit query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given course query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a course implementing the requested record.
     *
     *  @param courseRecordType a course record type
     *  @return the course query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseQueryInspectorRecord getCourseQueryInspectorRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(courseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course query. 
     *
     *  @param courseQueryInspectorRecord course query inspector
     *         record
     *  @param courseRecordType course record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseQueryInspectorRecord(org.osid.course.records.CourseQueryInspectorRecord courseQueryInspectorRecord, 
                                                   org.osid.type.Type courseRecordType) {

        addRecordType(courseRecordType);
        nullarg(courseRecordType, "course record type");
        this.records.add(courseQueryInspectorRecord);        
        return;
    }
}
