//
// AuctionHouseProviderFilterList.java
//
//     Implements a filter for providers.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.bidding.auctionhouse;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for providers.
 */

public final class AuctionHouseProviderFilterList
    extends net.okapia.osid.jamocha.inline.filter.bidding.auctionhouse.spi.AbstractAuctionHouseFilterList
    implements org.osid.bidding.AuctionHouseList,
               AuctionHouseFilter {

    private final org.osid.id.Id resourceId;


    /**
     *  Creates a new <code>AuctionHouseProviderFilterList</code>.
     *
     *  @param list an <code>AuctionHouseList</code>
     *  @param resourceId the provider Id
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>resourceId</code> is <code>null</code>
     */

    public AuctionHouseProviderFilterList(org.osid.bidding.AuctionHouseList list, org.osid.id.Id resourceId) {
        super(list);
        this.resourceId = resourceId;
        return;
    }    

    
    /**
     *  Filters AuctionHouses.
     *
     *  @param auctionHouse the auction house to filter
     *  @return <code>true</code> if the auction house passes the filter,
     *          <code>false</code> if the auction house should be filtered
     */

    @Override
    public boolean pass(org.osid.bidding.AuctionHouse auctionHouse) {
        return (auctionHouse.getProviderId().equals(this.resourceId));
    }
}
