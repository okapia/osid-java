//
// AbstractQueryEnrollmentLookupSession.java
//
//    An inline adapter that maps an EnrollmentLookupSession to
//    an EnrollmentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EnrollmentLookupSession to
 *  an EnrollmentQuerySession.
 */

public abstract class AbstractQueryEnrollmentLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractEnrollmentLookupSession
    implements org.osid.course.program.EnrollmentLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.program.EnrollmentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEnrollmentLookupSession.
     *
     *  @param querySession the underlying enrollment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEnrollmentLookupSession(org.osid.course.program.EnrollmentQuerySession querySession) {
        nullarg(querySession, "enrollment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Enrollment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEnrollments() {
        return (this.session.canSearchEnrollments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include enrollments in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only enrollments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEnrollmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All enrollments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEnrollmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Enrollment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Enrollment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Enrollment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentId <code>Id</code> of the
     *          <code>Enrollment</code>
     *  @return the enrollment
     *  @throws org.osid.NotFoundException <code>enrollmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>enrollmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Enrollment getEnrollment(org.osid.id.Id enrollmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchId(enrollmentId, true);
        org.osid.course.program.EnrollmentList enrollments = this.session.getEnrollmentsByQuery(query);
        if (enrollments.hasNext()) {
            return (enrollments.getNextEnrollment());
        } 
        
        throw new org.osid.NotFoundException(enrollmentId + " not found");
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  enrollments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Enrollments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, enrollments are returned that are currently effective.
     *  In any effective mode, effective enrollments and those currently expired
     *  are returned.
     *
     *  @param  enrollmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByIds(org.osid.id.IdList enrollmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();

        try (org.osid.id.IdList ids = enrollmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> which does not include
     *  enrollments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently effective.
     *  In any effective mode, effective enrollments and those currently expired
     *  are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchGenusType(enrollmentGenusType, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets an <code>EnrollmentList</code> corresponding to the given
     *  enrollment genus <code>Type</code> and include any additional
     *  enrollments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentGenusType an enrollment genus type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByParentGenusType(org.osid.type.Type enrollmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchParentGenusType(enrollmentGenusType, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets an <code>EnrollmentList</code> containing the given
     *  enrollment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  enrollmentRecordType an enrollment record type 
     *  @return the returned <code>Enrollment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsByRecordType(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchRecordType(enrollmentRecordType, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets an <code>EnrollmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *  
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Enrollment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getEnrollmentsByQuery(query));
    }
        

    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOffering(org.osid.id.Id programOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchProgramOfferingId(programOfferingId, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets a list of enrollments corresponding to a program offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @param programOfferingId the <code>Id</code> of the program
     *         offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingOnDate(org.osid.id.Id programOfferingId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchProgramOfferingId(programOfferingId, true);
        query.matchDate(from, to, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.program.EnrollmentList getEnrollmentsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets a list of enrollments corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering
     *  and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudent(org.osid.id.Id programOfferingId,
                                                                                             org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchProgramOfferingId(programOfferingId, true);
        query.matchStudentId(resourceId, true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets a list of enrollments corresponding to program offering and student
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible
     *  through this session.
     *
     *  In effective mode, enrollments are returned that are
     *  currently effective.  In any effective mode, effective
     *  enrollments and those currently expired are returned.
     *
     *  @param  programOfferingId the <code>Id</code> of the program offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EnrollmentList</code>
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollmentsForProgramOfferingAndStudentOnDate(org.osid.id.Id programOfferingId,
                                                                                                   org.osid.id.Id resourceId,
                                                                                                   org.osid.calendaring.DateTime from,
                                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchProgramOfferingId(programOfferingId, true);
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getEnrollmentsByQuery(query));
    }

    
    /**
     *  Gets all <code>Enrollments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  enrollments or an error results. Otherwise, the returned list
     *  may contain only those enrollments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, enrollments are returned that are currently
     *  effective.  In any effective mode, effective enrollments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Enrollments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.program.EnrollmentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEnrollmentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.program.EnrollmentQuery getQuery() {
        org.osid.course.program.EnrollmentQuery query = this.session.getEnrollmentQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
