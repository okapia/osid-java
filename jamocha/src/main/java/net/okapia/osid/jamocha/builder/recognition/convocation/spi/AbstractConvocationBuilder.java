//
// AbstractConvocation.java
//
//     Defines a Convocation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.convocation.spi;


/**
 *  Defines a <code>Convocation</code> builder.
 */

public abstract class AbstractConvocationBuilder<T extends AbstractConvocationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.recognition.convocation.ConvocationMiter convocation;


    /**
     *  Constructs a new <code>AbstractConvocationBuilder</code>.
     *
     *  @param convocation the convocation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractConvocationBuilder(net.okapia.osid.jamocha.builder.recognition.convocation.ConvocationMiter convocation) {
        super(convocation);
        this.convocation = convocation;
        return;
    }


    /**
     *  Builds the convocation.
     *
     *  @return the new convocation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.recognition.Convocation build() {
        (new net.okapia.osid.jamocha.builder.validator.recognition.convocation.ConvocationValidator(getValidations())).validate(this.convocation);
        return (new net.okapia.osid.jamocha.builder.recognition.convocation.ImmutableConvocation(this.convocation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the convocation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.recognition.convocation.ConvocationMiter getMiter() {
        return (this.convocation);
    }


    /**
     *  Adds an award.
     *
     *  @param award an award
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    public T award(org.osid.recognition.Award award) {
        getMiter().addAward(award);
        return (self());
    }


    /**
     *  Sets all the awards.
     *
     *  @param awards a collection of awards
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>awards</code> is
     *          <code>null</code>
     */

    public T awards(java.util.Collection<org.osid.recognition.Award> awards) {
        getMiter().setAwards(awards);
        return (self());
    }


    /**
     *  Sets the time period.
     *
     *  @param period a time period
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public T timePeriod(org.osid.calendaring.TimePeriod period) {
        getMiter().setTimePeriod(period);
        return (self());
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T date(org.osid.calendaring.DateTime date) {
        getMiter().setDate(date);
        return (self());
    }


    /**
     *  Adds a Convocation record.
     *
     *  @param record a convocation record
     *  @param recordType the type of convocation record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.recognition.records.ConvocationRecord record, org.osid.type.Type recordType) {
        getMiter().addConvocationRecord(record, recordType);
        return (self());
    }
}       


