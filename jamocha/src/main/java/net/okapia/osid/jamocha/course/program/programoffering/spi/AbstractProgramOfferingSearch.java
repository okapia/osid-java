//
// AbstractProgramOfferingSearch.java
//
//     A template for making a ProgramOffering Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.programoffering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing program offering searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProgramOfferingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.program.ProgramOfferingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.program.records.ProgramOfferingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.program.ProgramOfferingSearchOrder programOfferingSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of program offerings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  programOfferingIds list of program offerings
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProgramOfferings(org.osid.id.IdList programOfferingIds) {
        while (programOfferingIds.hasNext()) {
            try {
                this.ids.add(programOfferingIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProgramOfferings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of program offering Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProgramOfferingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  programOfferingSearchOrder program offering search order 
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>programOfferingSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProgramOfferingResults(org.osid.course.program.ProgramOfferingSearchOrder programOfferingSearchOrder) {
	this.programOfferingSearchOrder = programOfferingSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.program.ProgramOfferingSearchOrder getProgramOfferingSearchOrder() {
	return (this.programOfferingSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given program offering search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program offering implementing the requested record.
     *
     *  @param programOfferingSearchRecordType a program offering search record
     *         type
     *  @return the program offering search record
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programOfferingSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramOfferingSearchRecord getProgramOfferingSearchRecord(org.osid.type.Type programOfferingSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.program.records.ProgramOfferingSearchRecord record : this.records) {
            if (record.implementsRecordType(programOfferingSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programOfferingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program offering search. 
     *
     *  @param programOfferingSearchRecord program offering search record
     *  @param programOfferingSearchRecordType programOffering search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramOfferingSearchRecord(org.osid.course.program.records.ProgramOfferingSearchRecord programOfferingSearchRecord, 
                                           org.osid.type.Type programOfferingSearchRecordType) {

        addRecordType(programOfferingSearchRecordType);
        this.records.add(programOfferingSearchRecord);        
        return;
    }
}
