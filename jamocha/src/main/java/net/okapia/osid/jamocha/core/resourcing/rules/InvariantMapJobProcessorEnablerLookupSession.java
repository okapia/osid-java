//
// InvariantMapJobProcessorEnablerLookupSession
//
//    Implements a JobProcessorEnabler lookup service backed by a fixed collection of
//    jobProcessorEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobProcessorEnabler lookup service backed by a fixed
 *  collection of job processor enablers. The job processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapJobProcessorEnablerLookupSession</code> with no
     *  job processor enablers.
     *  
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumnetException {@code foundry} is
     *          {@code null}
     */

    public InvariantMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobProcessorEnablerLookupSession</code> with a single
     *  job processor enabler.
     *  
     *  @param foundry the foundry
     *  @param jobProcessorEnabler a single job processor enabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnabler} is <code>null</code>
     */

      public InvariantMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        this(foundry);
        putJobProcessorEnabler(jobProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobProcessorEnablerLookupSession</code> using an array
     *  of job processor enablers.
     *  
     *  @param foundry the foundry
     *  @param jobProcessorEnablers an array of job processor enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnablers} is <code>null</code>
     */

      public InvariantMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers) {
        this(foundry);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapJobProcessorEnablerLookupSession</code> using a
     *  collection of job processor enablers.
     *
     *  @param foundry the foundry
     *  @param jobProcessorEnablers a collection of job processor enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnablers} is <code>null</code>
     */

      public InvariantMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                               java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers) {
        this(foundry);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }
}
