//
// MutableIndexedMapProxyParameterProcessorLookupSession
//
//    Implements a ParameterProcessor lookup service backed by a collection of
//    parameterProcessors indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessor lookup service backed by a collection of
 *  parameterProcessors. The parameter processors are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some parameterProcessors may be compatible
 *  with more types than are indicated through these parameterProcessor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of parameter processors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractIndexedMapParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyParameterProcessorLookupSession} with
     *  no parameter processor.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyParameterProcessorLookupSession} with
     *  a single parameter processor.
     *
     *  @param configuration the configuration
     *  @param  parameterProcessor an parameter processor
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessor}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.configuration.rules.ParameterProcessor parameterProcessor, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessor(parameterProcessor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyParameterProcessorLookupSession} using
     *  an array of parameter processors.
     *
     *  @param configuration the configuration
     *  @param  parameterProcessors an array of parameter processors
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessors}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                       org.osid.configuration.rules.ParameterProcessor[] parameterProcessors, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessors(parameterProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyParameterProcessorLookupSession} using
     *  a collection of parameter processors.
     *
     *  @param configuration the configuration
     *  @param  parameterProcessors a collection of parameter processors
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessors}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyParameterProcessorLookupSession(org.osid.configuration.Configuration configuration,
                                                       java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessor> parameterProcessors,
                                                       org.osid.proxy.Proxy proxy) {
        this(configuration, proxy);
        putParameterProcessors(parameterProcessors);
        return;
    }

    
    /**
     *  Makes a {@code ParameterProcessor} available in this session.
     *
     *  @param  parameterProcessor a parameter processor
     *  @throws org.osid.NullArgumentException {@code parameterProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putParameterProcessor(org.osid.configuration.rules.ParameterProcessor parameterProcessor) {
        super.putParameterProcessor(parameterProcessor);
        return;
    }


    /**
     *  Makes an array of parameter processors available in this session.
     *
     *  @param  parameterProcessors an array of parameter processors
     *  @throws org.osid.NullArgumentException {@code parameterProcessors{@code 
     *          is {@code null}
     */

    @Override
    public void putParameterProcessors(org.osid.configuration.rules.ParameterProcessor[] parameterProcessors) {
        super.putParameterProcessors(parameterProcessors);
        return;
    }


    /**
     *  Makes collection of parameter processors available in this session.
     *
     *  @param  parameterProcessors a collection of parameter processors
     *  @throws org.osid.NullArgumentException {@code parameterProcessor{@code 
     *          is {@code null}
     */

    @Override
    public void putParameterProcessors(java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessor> parameterProcessors) {
        super.putParameterProcessors(parameterProcessors);
        return;
    }


    /**
     *  Removes a ParameterProcessor from this session.
     *
     *  @param parameterProcessorId the {@code Id} of the parameter processor
     *  @throws org.osid.NullArgumentException {@code parameterProcessorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeParameterProcessor(org.osid.id.Id parameterProcessorId) {
        super.removeParameterProcessor(parameterProcessorId);
        return;
    }    
}
