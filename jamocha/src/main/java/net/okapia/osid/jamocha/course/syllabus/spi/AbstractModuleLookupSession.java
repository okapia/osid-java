//
// AbstractModuleLookupSession.java
//
//    A starter implementation framework for providing a Module lookup
//    service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Module
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getModules(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractModuleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.syllabus.ModuleLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Module</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModules() {
        return (true);
    }


    /**
     *  A complete view of the <code>Module</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModuleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Module</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModuleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include modules in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active modules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveModuleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive modules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusModuleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Module</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Module</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Module</code> and
     *  retained for compatibility.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleId <code>Id</code> of the
     *          <code>Module</code>
     *  @return the module
     *  @throws org.osid.NotFoundException <code>moduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>moduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule(org.osid.id.Id moduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.syllabus.ModuleList modules = getModules()) {
            while (modules.hasNext()) {
                org.osid.course.syllabus.Module module = modules.getNextModule();
                if (module.getId().equals(moduleId)) {
                    return (module);
                }
            }
        } 

        throw new org.osid.NotFoundException(moduleId + " not found");
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  modules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Modules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getModules()</code>.
     *
     *  @param  moduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>moduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByIds(org.osid.id.IdList moduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.syllabus.Module> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = moduleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getModule(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("module " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.syllabus.module.LinkedModuleList(ret));
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> which does not include
     *  modules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getModules()</code>.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ModuleGenusFilterList(getModules(), moduleGenusType));
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> and include any additional
     *  modules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getModules()</code>.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByParentGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getModulesByGenusType(moduleGenusType));
    }


    /**
     *  Gets a <code>ModuleList</code> containing the given
     *  module record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getModules()</code>.
     *
     *  @param  moduleRecordType a module record type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByRecordType(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ModuleRecordFilterList(getModules(), moduleRecordType));
    }


    /**
     *  Gets a <code>ModuleList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known modules
     *  or an error results. Otherwise, the returned list may contain
     *  only those modules that are accessible through this session.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules are
     *  returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Module</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ModuleProviderFilterList(getModules(), resourceId));
    }


    /**
     *  Gets a <code> ModuleList </code> for the given syllabus <code>
     *  Id.</code>
     *  
     *  In plenary mode, the returned list contains all known modules
     *  or an error results. Otherwise, the returned list may contain
     *  only those modules that are accessible through this session.
     *  
     *  In active mode, modeules are returned that are currently
     *  active. In any status mode, active and inactive modules are
     *  returned.
     *
     *  @param  syllabusId a syllabus <code> Id </code> 
     *  @return the returned <code> Module </code> list 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesForSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ModuleFilterList(new SyllabusFilter(syllabusId), getModules()));
    }        


    /**
     *  Gets all <code>Modules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @return a list of <code>Modules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the module list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of modules
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.syllabus.ModuleList filterModulesOnViews(org.osid.course.syllabus.ModuleList list)
        throws org.osid.OperationFailedException {

        org.osid.course.syllabus.ModuleList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ActiveModuleFilterList(ret);
        }

        return (ret);
    }


    public static class SyllabusFilter
        implements net.okapia.osid.jamocha.inline.filter.course.syllabus.module.ModuleFilter {
         
        private final org.osid.id.Id syllabusId;
         
         
        /**
         *  Constructs a new <code>SyllabusFilter</code>.
         *
         *  @param syllabusId the syllabus to filter
         *  @throws org.osid.NullArgumentException
         *          <code>syllabusId</code> is <code>null</code>
         */
        
        public SyllabusFilter(org.osid.id.Id syllabusId) {
            nullarg(syllabusId, "syllabus Id");
            this.syllabusId = syllabusId;
            return;
        }

         
        /**
         *  Used by the ModuleFilterList to filter the 
         *  module list based on syllabus.
         *
         *  @param module the module
         *  @return <code>true</code> to pass the module,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.syllabus.Module module) {
            return (module.getSyllabusId().equals(this.syllabusId));
        }
    }
}
