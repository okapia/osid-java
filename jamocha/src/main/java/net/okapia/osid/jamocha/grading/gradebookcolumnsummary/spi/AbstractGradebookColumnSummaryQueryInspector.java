//
// AbstractGradebookColumnSummaryQueryInspector.java
//
//     A template for making a GradebookColumnSummaryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for gradebook column summaries.
 */

public abstract class AbstractGradebookColumnSummaryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.grading.GradebookColumnSummaryQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebookc column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the mean terms. 
     *
     *  @return the mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMeanTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum mean terms. 
     *
     *  @return the minimum mean terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMeanTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the median terms. 
     *
     *  @return the median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getMedianTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum median terms. 
     *
     *  @return the minimum median terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumMedianTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the mode terms. 
     *
     *  @return the mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getModeTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum mode terms. 
     *
     *  @return the minimum mode terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumModeTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the rms terms. 
     *
     *  @return the rms terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getRMSTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum rms terms. 
     *
     *  @return the minimum rms terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumRMSTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the standard deviation terms. 
     *
     *  @return the standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getStandardDeviationTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum standard deviation terms. 
     *
     *  @return the minimum standard deviation terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumStandardDeviationTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the sum terms. 
     *
     *  @return the sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getSumTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the minimum sum terms. 
     *
     *  @return the minimum sum terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getMinimumSumTerms() {
        return (new org.osid.search.terms.DecimalTerm[0]);
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given gradebook column summary query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a gradebook column summary implementing the requested record.
     *
     *  @param gradebookColumnSummaryRecordType a gradebook column summary record type
     *  @return the gradebook column summary query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord getGradebookColumnSummaryQueryInspectorRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column summary query. 
     *
     *  @param gradebookColumnSummaryQueryInspectorRecord gradebook column summary query inspector
     *         record
     *  @param gradebookColumnSummaryRecordType gradebookColumnSummary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnSummaryQueryInspectorRecord(org.osid.grading.records.GradebookColumnSummaryQueryInspectorRecord gradebookColumnSummaryQueryInspectorRecord, 
                                                   org.osid.type.Type gradebookColumnSummaryRecordType) {

        addRecordType(gradebookColumnSummaryRecordType);
        nullarg(gradebookColumnSummaryRecordType, "gradebook column summary record type");
        this.records.add(gradebookColumnSummaryQueryInspectorRecord);        
        return;
    }
}
