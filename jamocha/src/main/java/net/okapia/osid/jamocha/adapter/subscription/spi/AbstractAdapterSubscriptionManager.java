//
// AbstractSubscriptionManager.java
//
//     An adapter for a SubscriptionManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SubscriptionManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSubscriptionManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.subscription.SubscriptionManager>
    implements org.osid.subscription.SubscriptionManager {


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSubscriptionManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any dispatch federation is exposed. Federation is exposed 
     *  when a specific dispatch may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of dispatches appears as a single dispatch. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a my subscription lookup service. 
     *
     *  @return <code> true </code> if my subscription lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySubscription() {
        return (getAdapteeManager().supportsMySubscription());
    }


    /**
     *  Tests for the availability of a my subscription administrative 
     *  service. 
     *
     *  @return <code> true </code> if my subscription admin is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySubscriptionAdmin() {
        return (getAdapteeManager().supportsMySubscriptionAdmin());
    }


    /**
     *  Tests for the availability of a subscription lookup service. 
     *
     *  @return <code> true </code> if subscription lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionLookup() {
        return (getAdapteeManager().supportsSubscriptionLookup());
    }


    /**
     *  Tests if querying subscriptions is available. 
     *
     *  @return <code> true </code> if subscription query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (getAdapteeManager().supportsSubscriptionQuery());
    }


    /**
     *  Tests if searching for subscriptions is available. 
     *
     *  @return <code> true </code> if subscription search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionSearch() {
        return (getAdapteeManager().supportsSubscriptionSearch());
    }


    /**
     *  Tests if searching for subscriptions is available. 
     *
     *  @return <code> true </code> if subscription search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionAdmin() {
        return (getAdapteeManager().supportsSubscriptionAdmin());
    }


    /**
     *  Tests if subscription notification is available. 
     *
     *  @return <code> true </code> if subscription notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionNotification() {
        return (getAdapteeManager().supportsSubscriptionNotification());
    }


    /**
     *  Tests if a subscription to publisher lookup session is available. 
     *
     *  @return <code> true </code> if subscription publisher lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionPublisher() {
        return (getAdapteeManager().supportsSubscriptionPublisher());
    }


    /**
     *  Tests if a subscription to publisher assignment session is available. 
     *
     *  @return <code> true </code> if subscription publisher assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionPublisherAssignment() {
        return (getAdapteeManager().supportsSubscriptionPublisherAssignment());
    }


    /**
     *  Tests if a subscription smart publisher session is available. 
     *
     *  @return <code> true </code> if subscription smart publisher is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionSmartPublisher() {
        return (getAdapteeManager().supportsSubscriptionSmartPublisher());
    }


    /**
     *  Tests for the availability of an dispatch lookup service. 
     *
     *  @return <code> true </code> if dispatch lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchLookup() {
        return (getAdapteeManager().supportsDispatchLookup());
    }


    /**
     *  Tests if querying dispatches is available. 
     *
     *  @return <code> true </code> if dispatch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchQuery() {
        return (getAdapteeManager().supportsDispatchQuery());
    }


    /**
     *  Tests if searching for dispatches is available. 
     *
     *  @return <code> true </code> if dispatch search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSearch() {
        return (getAdapteeManager().supportsDispatchSearch());
    }


    /**
     *  Tests for the availability of a dispatch administrative service for 
     *  creating and deleting dispatches. 
     *
     *  @return <code> true </code> if dispatch administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchAdmin() {
        return (getAdapteeManager().supportsDispatchAdmin());
    }


    /**
     *  Tests for the availability of a dispatch notification service. 
     *
     *  @return <code> true </code> if dispatch notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchNotification() {
        return (getAdapteeManager().supportsDispatchNotification());
    }


    /**
     *  Tests if a dispatch to publisher lookup session is available. 
     *
     *  @return <code> true </code> if dispatch publisher lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchPublisher() {
        return (getAdapteeManager().supportsDispatchPublisher());
    }


    /**
     *  Tests if a dispatch to publisher assignment session is available. 
     *
     *  @return <code> true </code> if dispatch publisher assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchPublisherAssignment() {
        return (getAdapteeManager().supportsDispatchPublisherAssignment());
    }


    /**
     *  Tests if a dispatch smart publisher session is available. 
     *
     *  @return <code> true </code> if dispatch smart publisher is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchSmartPublisher() {
        return (getAdapteeManager().supportsDispatchSmartPublisher());
    }


    /**
     *  Tests for the availability of an publisher lookup service. 
     *
     *  @return <code> true </code> if publisher lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherLookup() {
        return (getAdapteeManager().supportsPublisherLookup());
    }


    /**
     *  Tests if querying publishers is available. 
     *
     *  @return <code> true </code> if publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (getAdapteeManager().supportsPublisherQuery());
    }


    /**
     *  Tests if searching for publishers is available. 
     *
     *  @return <code> true </code> if publisher search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherSearch() {
        return (getAdapteeManager().supportsPublisherSearch());
    }


    /**
     *  Tests for the availability of a publisher administrative service for 
     *  creating and deleting publishers. 
     *
     *  @return <code> true </code> if publisher administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherAdmin() {
        return (getAdapteeManager().supportsPublisherAdmin());
    }


    /**
     *  Tests for the availability of a publisher notification service. 
     *
     *  @return <code> true </code> if publisher notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherNotification() {
        return (getAdapteeManager().supportsPublisherNotification());
    }


    /**
     *  Tests for the availability of a publisher hierarchy traversal service. 
     *
     *  @return <code> true </code> if publisher hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherHierarchy() {
        return (getAdapteeManager().supportsPublisherHierarchy());
    }


    /**
     *  Tests for the availability of a publisher hierarchy design service. 
     *
     *  @return <code> true </code> if publisher hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherHierarchyDesign() {
        return (getAdapteeManager().supportsPublisherHierarchyDesign());
    }


    /**
     *  Tests for the availability of a subscription batch service. 
     *
     *  @return <code> true </code> if a subscription batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionBatch() {
        return (getAdapteeManager().supportsSubscriptionBatch());
    }


    /**
     *  Tests for the availability of a subscription rules service. 
     *
     *  @return <code> true </code> if a subscription rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionRules() {
        return (getAdapteeManager().supportsSubscriptionRules());
    }


    /**
     *  Gets the supported <code> Subscription </code> record types. 
     *
     *  @return a list containing the supported subscription record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionRecordTypes() {
        return (getAdapteeManager().getSubscriptionRecordTypes());
    }


    /**
     *  Tests if the given <code> Subscription </code> record type is 
     *  supported. 
     *
     *  @param  subscriptionRecordType a <code> Type </code> indicating a 
     *          <code> Subscription </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> subscriptionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionRecordType(org.osid.type.Type subscriptionRecordType) {
        return (getAdapteeManager().supportsSubscriptionRecordType(subscriptionRecordType));
    }


    /**
     *  Gets the supported subscription search record types. 
     *
     *  @return a list containing the supported subscription search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSubscriptionSearchRecordTypes() {
        return (getAdapteeManager().getSubscriptionSearchRecordTypes());
    }


    /**
     *  Tests if the given subscription search record type is supported. 
     *
     *  @param  subscriptionSearchRecordType a <code> Type </code> indicating 
     *          a subscription record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          subscriptionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSubscriptionSearchRecordType(org.osid.type.Type subscriptionSearchRecordType) {
        return (getAdapteeManager().supportsSubscriptionSearchRecordType(subscriptionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Dispatch </code> record types. 
     *
     *  @return a list containing the supported dispatch record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDispatchRecordTypes() {
        return (getAdapteeManager().getDispatchRecordTypes());
    }


    /**
     *  Tests if the given <code> Dispatch </code> record type is supported. 
     *
     *  @param  dispatchRecordType a <code> Type </code> indicating a <code> 
     *          Dispatch </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dispatchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDispatchRecordType(org.osid.type.Type dispatchRecordType) {
        return (getAdapteeManager().supportsDispatchRecordType(dispatchRecordType));
    }


    /**
     *  Gets the supported dispatch search record types. 
     *
     *  @return a list containing the supported dispatch search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDispatchSearchRecordTypes() {
        return (getAdapteeManager().getDispatchSearchRecordTypes());
    }


    /**
     *  Tests if the given dispatch search record type is supported. 
     *
     *  @param  dispatchSearchRecordType a <code> Type </code> indicating a 
     *          dispatch record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> dispatchSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDispatchSearchRecordType(org.osid.type.Type dispatchSearchRecordType) {
        return (getAdapteeManager().supportsDispatchSearchRecordType(dispatchSearchRecordType));
    }


    /**
     *  Gets the supported <code> Publisher </code> record types. 
     *
     *  @return a list containing the supported publisher record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPublisherRecordTypes() {
        return (getAdapteeManager().getPublisherRecordTypes());
    }


    /**
     *  Tests if the given <code> Publisher </code> record type is supported. 
     *
     *  @param  publisherRecordType a <code> Type </code> indicating a <code> 
     *          Publisher </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> publisherRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPublisherRecordType(org.osid.type.Type publisherRecordType) {
        return (getAdapteeManager().supportsPublisherRecordType(publisherRecordType));
    }


    /**
     *  Gets the supported publisher search record types. 
     *
     *  @return a list containing the supported publisher search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPublisherSearchRecordTypes() {
        return (getAdapteeManager().getPublisherSearchRecordTypes());
    }


    /**
     *  Tests if the given publisher search record type is supported. 
     *
     *  @param  publisherSearchRecordType a <code> Type </code> indicating a 
     *          publisher record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          publisherSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPublisherSearchRecordType(org.osid.type.Type publisherSearchRecordType) {
        return (getAdapteeManager().supportsPublisherSearchRecordType(publisherSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent. 
     *
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMySubscriptionSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the authenticated agent for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscription() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionSession getMySubscriptionSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMySubscriptionSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent. 
     *
     *  @return a <code> My </code> S <code> ubscriptionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMySubscriptionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administrative service for the authenticated agent for the given 
     *  publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> MySubscriptionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMySubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.MySubscriptionAdminSession getMySubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMySubscriptionAdminSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service. 
     *
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionLookupSession getSubscriptionLookupSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionLookupSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service. 
     *
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  query service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuerySession getSubscriptionQuerySessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionQuerySessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service. 
     *
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSearchSession getSubscriptionSearchSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionSearchSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service. 
     *
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionAdminSession getSubscriptionAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionAdminSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSession(org.osid.subscription.SubscriptionReceiver subscriptionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionNotificationSession(subscriptionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subscription 
     *  notification service for the given publisher. 
     *
     *  @param  subscriptionReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> SubscriptionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> subscriptionReceiver 
     *          </code> or <code> publisherId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionNotificationSession getSubscriptionNotificationSessionForPublisher(org.osid.subscription.SubscriptionReceiver subscriptionReceiver, 
                                                                                                                org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionNotificationSessionForPublisher(subscriptionReceiver, publisherId));
    }


    /**
     *  Gets the session for retrieving subscription to publisher mappings. 
     *
     *  @return a <code> SubscriptionPublisherSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherSession getSubscriptionPublisherSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionPublisherSession());
    }


    /**
     *  Gets the session for assigning subscription to publisher mappings. 
     *
     *  @return a <code> SubscriptionPublisherAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionPublisherAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionPublisherAssignmentSession getSubscriptionPublisherAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionPublisherAssignmentSession());
    }


    /**
     *  Gets the session associated with the subscription smart publisher for 
     *  the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the publisher 
     *  @return a <code> SubscriptionSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionSmartPublisherSession getSubscriptionSmartPublisherSession(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionSmartPublisherSession(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service. 
     *
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  lookup service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchLookupSession getDispatchLookupSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchLookupSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service. 
     *
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch query 
     *  service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQuerySession getDispatchQuerySessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchQuerySessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service. 
     *
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  search service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSearchSession getDispatchSearchSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchSearchSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administrative service. 
     *
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  administrative service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Dispatch 
     *          </code> 
     *  @return a <code> DispatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDispatchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchAdminSession getDispatchAdminSessionForPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchAdminSessionForPublisher(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSession(org.osid.subscription.DispatchReceiver dispatchReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchNotificationSession(dispatchReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the dispatch 
     *  notification service for the given publisher. 
     *
     *  @param  dispatchReceiver the receiver 
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @return a <code> DispatchNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> dispatchReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchNotificationSession getDispatchNotificationSessionForPublisher(org.osid.subscription.DispatchReceiver dispatchReceiver, 
                                                                                                        org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchNotificationSessionForPublisher(dispatchReceiver, publisherId));
    }


    /**
     *  Gets the session for retrieving dispatch to publisher mappings. 
     *
     *  @return a <code> DispatchPublisherSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisher() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherSession getDispatchPublisherSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchPublisherSession());
    }


    /**
     *  Gets the session for assigning dispatch to publisher mappings. 
     *
     *  @return a <code> DispatchPublisherAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchPublisherAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchPublisherAssignmentSession getDispatchPublisherAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchPublisherAssignmentSession());
    }


    /**
     *  Gets the session associated with the dispatch smart publisher for the 
     *  given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the publisher 
     *  @return a <code> DispatchSmartPublisherSession </code> 
     *  @throws org.osid.NotFoundException <code> publisherId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchSmartPublisher() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.DispatchSmartPublisherSession getDispatchSmartPublisherSession(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchSmartPublisherSession(publisherId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  lookup service. 
     *
     *  @return a <code> PublisherLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherLookupSession getPublisherLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  query service. 
     *
     *  @return a <code> PublisherQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuerySession getPublisherQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  search service. 
     *
     *  @return a <code> PublisherSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherSearchSession getPublisherSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  administrative service. 
     *
     *  @return a <code> PublisherAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherAdminSession getPublisherAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  notification service. 
     *
     *  @param  publisherReceiver the receiver 
     *  @return a <code> PublisherNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> publisherReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherNotificationSession getPublisherNotificationSession(org.osid.subscription.PublisherReceiver publisherReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherNotificationSession(publisherReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy service. 
     *
     *  @return a <code> PublisherHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchySession getPublisherHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the publisher 
     *  hierarchy design service. 
     *
     *  @return a <code> PublisherHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherHierarchyDesignSession getPublisherHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherHierarchyDesignSession());
    }


    /**
     *  Gets the <code> SubscriptionBatchManager. </code> 
     *
     *  @return a <code> SubscriptionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionbatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.SubscriptionBatchManager getSubscriptionBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionBatchManager());
    }


    /**
     *  Gets the <code> SubscriptionRulesManager. </code> 
     *
     *  @return a <code> SubscriptionRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.rules.SubscriptionRulesManager getSubscriptionRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
