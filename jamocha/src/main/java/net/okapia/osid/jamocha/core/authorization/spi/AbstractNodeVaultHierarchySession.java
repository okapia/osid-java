//
// AbstractNodeVaultHierarchySession.java
//
//     Defines a Vault hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a vault hierarchy session for delivering a hierarchy
 *  of vaults using the VaultNode interface.
 */

public abstract class AbstractNodeVaultHierarchySession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractVaultHierarchySession
    implements org.osid.authorization.VaultHierarchySession {

    private java.util.Collection<org.osid.authorization.VaultNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root vault <code> Ids </code> in this hierarchy.
     *
     *  @return the root vault <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootVaultIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vaultnode.VaultNodeToIdList(this.roots));
    }


    /**
     *  Gets the root vaults in the vault hierarchy. A node
     *  with no parents is an orphan. While all vault <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root vaults 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getRootVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vaultnode.VaultNodeToVaultList(new net.okapia.osid.jamocha.authorization.vaultnode.ArrayVaultNodeList(this.roots)));
    }


    /**
     *  Adds a root vault node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootVault(org.osid.authorization.VaultNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root vault nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootVaults(java.util.Collection<org.osid.authorization.VaultNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root vault node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootVault(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.authorization.VaultNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Vault </code> has any parents. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @return <code> true </code> if the vault has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentVaults(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getVaultNode(vaultId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  vault.
     *
     *  @param  id an <code> Id </code> 
     *  @param  vaultId the <code> Id </code> of a vault 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> vaultId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> vaultId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfVault(org.osid.id.Id id, org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authorization.VaultNodeList parents = getVaultNode(vaultId).getParentVaultNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextVaultNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given vault. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @return the parent <code> Ids </code> of the vault 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentVaultIds(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vault.VaultToIdList(getParentVaults(vaultId)));
    }


    /**
     *  Gets the parents of the given vault. 
     *
     *  @param  vaultId the <code> Id </code> to query 
     *  @return the parents of the vault 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getParentVaults(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vaultnode.VaultNodeToVaultList(getVaultNode(vaultId).getParentVaultNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  vault.
     *
     *  @param  id an <code> Id </code> 
     *  @param  vaultId the Id of a vault 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> vaultId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfVault(org.osid.id.Id id, org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfVault(id, vaultId)) {
            return (true);
        }

        try (org.osid.authorization.VaultList parents = getParentVaults(vaultId)) {
            while (parents.hasNext()) {
                if (isAncestorOfVault(id, parents.getNextVault().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a vault has any children. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @return <code> true </code> if the <code> vaultId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildVaults(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getVaultNode(vaultId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  vault.
     *
     *  @param  id an <code> Id </code> 
     *  @param vaultId the <code> Id </code> of a 
     *         vault
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> vaultId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> vaultId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfVault(org.osid.id.Id id, org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfVault(vaultId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  vault.
     *
     *  @param  vaultId the <code> Id </code> to query 
     *  @return the children of the vault 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildVaultIds(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vault.VaultToIdList(getChildVaults(vaultId)));
    }


    /**
     *  Gets the children of the given vault. 
     *
     *  @param  vaultId the <code> Id </code> to query 
     *  @return the children of the vault 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getChildVaults(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vaultnode.VaultNodeToVaultList(getVaultNode(vaultId).getChildVaultNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  vault.
     *
     *  @param  id an <code> Id </code> 
     *  @param vaultId the <code> Id </code> of a 
     *         vault
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> vaultId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfVault(org.osid.id.Id id, org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfVault(vaultId, id)) {
            return (true);
        }

        try (org.osid.authorization.VaultList children = getChildVaults(vaultId)) {
            while (children.hasNext()) {
                if (isDescendantOfVault(id, children.getNextVault().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  vault.
     *
     *  @param  vaultId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified vault node 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getVaultNodeIds(org.osid.id.Id vaultId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.authorization.vaultnode.VaultNodeToNode(getVaultNode(vaultId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given vault.
     *
     *  @param  vaultId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified vault node 
     *  @throws org.osid.NotFoundException <code> vaultId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> vaultId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultNode getVaultNodes(org.osid.id.Id vaultId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getVaultNode(vaultId));
    }


    /**
     *  Closes this <code>VaultHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a vault node.
     *
     *  @param vaultId the id of the vault node
     *  @throws org.osid.NotFoundException <code>vaultId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>vaultId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.authorization.VaultNode getVaultNode(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(vaultId, "vault Id");
        for (org.osid.authorization.VaultNode vault : this.roots) {
            if (vault.getId().equals(vaultId)) {
                return (vault);
            }

            org.osid.authorization.VaultNode r = findVault(vault, vaultId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(vaultId + " is not found");
    }


    protected org.osid.authorization.VaultNode findVault(org.osid.authorization.VaultNode node, 
                                                            org.osid.id.Id vaultId)
        throws org.osid.OperationFailedException {

        try (org.osid.authorization.VaultNodeList children = node.getChildVaultNodes()) {
            while (children.hasNext()) {
                org.osid.authorization.VaultNode vault = children.getNextVaultNode();
                if (vault.getId().equals(vaultId)) {
                    return (vault);
                }
                
                vault = findVault(vault, vaultId);
                if (vault != null) {
                    return (vault);
                }
            }
        }

        return (null);
    }
}
