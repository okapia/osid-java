//
// AbstractMapBrokerProcessorLookupSession
//
//    A simple framework for providing a BrokerProcessor lookup service
//    backed by a fixed collection of broker processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a BrokerProcessor lookup service backed by a
 *  fixed collection of broker processors. The broker processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBrokerProcessorLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerProcessorLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.BrokerProcessor> brokerProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.BrokerProcessor>());


    /**
     *  Makes a <code>BrokerProcessor</code> available in this session.
     *
     *  @param  brokerProcessor a broker processor
     *  @throws org.osid.NullArgumentException <code>brokerProcessor<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessor(org.osid.provisioning.rules.BrokerProcessor brokerProcessor) {
        this.brokerProcessors.put(brokerProcessor.getId(), brokerProcessor);
        return;
    }


    /**
     *  Makes an array of broker processors available in this session.
     *
     *  @param  brokerProcessors an array of broker processors
     *  @throws org.osid.NullArgumentException <code>brokerProcessors<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessors(org.osid.provisioning.rules.BrokerProcessor[] brokerProcessors) {
        putBrokerProcessors(java.util.Arrays.asList(brokerProcessors));
        return;
    }


    /**
     *  Makes a collection of broker processors available in this session.
     *
     *  @param  brokerProcessors a collection of broker processors
     *  @throws org.osid.NullArgumentException <code>brokerProcessors<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessors(java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessor> brokerProcessors) {
        for (org.osid.provisioning.rules.BrokerProcessor brokerProcessor : brokerProcessors) {
            this.brokerProcessors.put(brokerProcessor.getId(), brokerProcessor);
        }

        return;
    }


    /**
     *  Removes a BrokerProcessor from this session.
     *
     *  @param  brokerProcessorId the <code>Id</code> of the broker processor
     *  @throws org.osid.NullArgumentException <code>brokerProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeBrokerProcessor(org.osid.id.Id brokerProcessorId) {
        this.brokerProcessors.remove(brokerProcessorId);
        return;
    }


    /**
     *  Gets the <code>BrokerProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  brokerProcessorId <code>Id</code> of the <code>BrokerProcessor</code>
     *  @return the brokerProcessor
     *  @throws org.osid.NotFoundException <code>brokerProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>brokerProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessor getBrokerProcessor(org.osid.id.Id brokerProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.BrokerProcessor brokerProcessor = this.brokerProcessors.get(brokerProcessorId);
        if (brokerProcessor == null) {
            throw new org.osid.NotFoundException("brokerProcessor not found: " + brokerProcessorId);
        }

        return (brokerProcessor);
    }


    /**
     *  Gets all <code>BrokerProcessors</code>. In plenary mode, the returned
     *  list contains all known brokerProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  brokerProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>BrokerProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.ArrayBrokerProcessorList(this.brokerProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessors.clear();
        super.close();
        return;
    }
}
