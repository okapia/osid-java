//
// InvariantMapProxyInquestLookupSession
//
//    Implements an Inquest lookup service backed by a fixed
//    collection of inquests. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Inquest lookup service backed by a fixed
 *  collection of inquests. The inquests are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyInquestLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapInquestLookupSession
    implements org.osid.inquiry.InquestLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyInquestLookupSession} with no
     *  inquests.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyInquestLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyInquestLookupSession} with a
     *  single inquest.
     *
     *  @param inquest an single inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquestLookupSession(org.osid.inquiry.Inquest inquest, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putInquest(inquest);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyInquestLookupSession} using
     *  an array of inquests.
     *
     *  @param inquests an array of inquests
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquests} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquestLookupSession(org.osid.inquiry.Inquest[] inquests, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putInquests(inquests);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyInquestLookupSession} using a
     *  collection of inquests.
     *
     *  @param inquests a collection of inquests
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquests} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquestLookupSession(java.util.Collection<? extends org.osid.inquiry.Inquest> inquests,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putInquests(inquests);
        return;
    }
}
