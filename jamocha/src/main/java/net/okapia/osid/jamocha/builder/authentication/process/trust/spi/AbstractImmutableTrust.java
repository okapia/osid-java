//
// AbstractImmutableTrust.java
//
//     Wraps a mutable Trust to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.trust.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Trust</code> to hide modifiers. This
 *  wrapper provides an immutized Trust from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying trust whose state changes are visible.
 */

public abstract class AbstractImmutableTrust
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.authentication.process.Trust {

    private final org.osid.authentication.process.Trust trust;


    /**
     *  Constructs a new <code>AbstractImmutableTrust</code>.
     *
     *  @param trust the trust to immutablize
     *  @throws org.osid.NullArgumentException <code>trust</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTrust(org.osid.authentication.process.Trust trust) {
        super(trust);
        this.trust = trust;
        return;
    }


    /**
     *  Gets the trust record corresponding to the given trust record <code> 
     *  Type. </code> This method is used to retrieve an object implementing 
     *  the requested. The <code> trustRecordType </code> may be the <code> 
     *  Type </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(trustRecordType) </code> is <code> true </code> . 
     *
     *  @param  trustRecordType the type of trust record to retrieve 
     *  @return the trust record 
     *  @throws org.osid.NullArgumentException <code> trustRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(trustRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.records.TrustRecord getTrustRecord(org.osid.type.Type trustRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.trust.getTrustRecord(trustRecordType));
    }
}

