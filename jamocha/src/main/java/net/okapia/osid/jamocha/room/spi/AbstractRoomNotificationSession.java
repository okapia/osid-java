//
// AbstractRoomNotificationSession.java
//
//     A template for making RoomNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Room} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Room} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for room entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractRoomNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.RoomNotificationSession {

    private boolean federated = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();


    /**
     *  Gets the {@code Campus/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }

    
    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the {@code Campus}.
     *
     *  @param campus the campus for this session
     *  @throws org.osid.NullArgumentException {@code campus}
     *          is {@code null}
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can register for {@code Room}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForRoomNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rooms in campi which are children of
     *  this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new rooms. {@code
     *  RoomReceiver.newRoom()} is invoked when a new {@code Room} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new rooms of the given genus
     *  type.  {@code RoomReceiver.newRoom()} is invoked when a new
     *  {@code Room} is created.
     *
     *  @param  roomGenusType a room genus type 
     *  @throws org.osid.NullArgumentException {@code roomGenusType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new rooms for the given building
     *  {@code Id}. {@code RoomReceiver.newRoom()} is invoked when a
     *  new {@code Room} is created.
     *
     *  @param  buildingId the {@code Id} of the building to monitor
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewRoomsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated rooms. {@code
     *  RoomReceiver.changedRoom()} is invoked when a room is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated rooms of the given genus
     *  type.  {@code RoomReceiver.changedRoom()} is invoked when a
     *  room is changed.
     *
     *  @param  roomGenusType a room genus type 
     *  @throws org.osid.NullArgumentException {@code roomGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated rooms for the given
     *  building {@code Id}. {@code RoomReceiver.changedRoom()} is
     *  invoked when a {@code Room} in this campus is changed.
     *
     *  @param  buildingId the {@code Id} of the building to monitor
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedRoomsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated room. {@code
     *  RoomReceiver.changedRoom()} is invoked when the specified room
     *  is changed.
     *
     *  @param roomId the {@code Id} of the {@code Room} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code roomId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted rooms. {@code
     *  RoomReceiver.deletedRoom()} is invoked when a room is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted rooms of the given genus
     *  type.  {@code RoomReceiver.deletedRoom()} is invoked when a
     *  room is deleted.
     *
     *  @param  roomGenusType a room genus type 
     *  @throws org.osid.NullArgumentException {@code roomGenusType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted rooms for the given
     *  building {@code Id}. {@code RoomReceiver.deletedRoom()} is
     *  invoked when a {@code Room} is deleted or removed from this
     *  campus.
     *
     *  @param  buildingId the {@code Id} of the building to monitor
     *  @throws org.osid.NullArgumentException {@code buildingId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedRoomsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted room. {@code
     *  RoomReceiver.deletedRoom()} is invoked when the specified room
     *  is deleted.
     *
     *  @param roomId the {@code Id} of the
     *          {@code Room} to monitor
     *  @throws org.osid.NullArgumentException {@code roomId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
