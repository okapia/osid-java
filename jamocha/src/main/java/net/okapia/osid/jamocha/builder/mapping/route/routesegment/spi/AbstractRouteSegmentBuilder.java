//
// AbstractRouteSegment.java
//
//     Defines a RouteSegment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routesegment.spi;


/**
 *  Defines a <code>RouteSegment</code> builder.
 */

public abstract class AbstractRouteSegmentBuilder<T extends AbstractRouteSegmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.route.routesegment.RouteSegmentMiter routeSegment;


    /**
     *  Constructs a new <code>AbstractRouteSegmentBuilder</code>.
     *
     *  @param routeSegment the route segment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRouteSegmentBuilder(net.okapia.osid.jamocha.builder.mapping.route.routesegment.RouteSegmentMiter routeSegment) {
        super(routeSegment);
        this.routeSegment = routeSegment;
        return;
    }


    /**
     *  Builds the route segment.
     *
     *  @return the new route segment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.route.RouteSegment build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.route.routesegment.RouteSegmentValidator(getValidations())).validate(this.routeSegment);
        return (new net.okapia.osid.jamocha.builder.mapping.route.routesegment.ImmutableRouteSegment(this.routeSegment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the route segment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.route.routesegment.RouteSegmentMiter getMiter() {
        return (this.routeSegment);
    }


    /**
     *  Sets the starting instructions.
     *
     *  @param instructions starting instructions
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    public T startingInstructions(org.osid.locale.DisplayText instructions) {
        getMiter().setStartingInstructions(instructions);
        return (self());
    }


    /**
     *  Sets the ending instructions.
     *
     *  @param instructions ending instructions
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    public T endingInstructions(org.osid.locale.DisplayText instructions) {
        getMiter().setEndingInstructions(instructions);
        return (self());
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public T distance(org.osid.mapping.Distance distance) {
        getMiter().setDistance(distance);
        return (self());
    }


    /**
     *  Sets the eta.
     *
     *  @param eta the estimated time of arrival
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    public T eta(org.osid.calendaring.Duration eta) {
        getMiter().setETA(eta);
        return (self());
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(org.osid.mapping.path.Path path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Adds a RouteSegment record.
     *
     *  @param record a route segment record
     *  @param recordType the type of route segment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.route.records.RouteSegmentRecord record, org.osid.type.Type recordType) {
        getMiter().addRouteSegmentRecord(record, recordType);
        return (self());
    }
}       


