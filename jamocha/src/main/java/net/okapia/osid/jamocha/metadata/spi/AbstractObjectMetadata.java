//
// AbstractObjectMetadata.java
//
//     Defines an object Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an object Metadata.
 */

public abstract class AbstractObjectMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types types = new TypeSet();

    private final java.util.Collection<java.lang.Object> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<java.lang.Object> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<java.lang.Object> existing = new java.util.LinkedHashSet<>();    


    /**
     *  Constructs a new {@code AbstractObjectMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractObjectMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.SPATIALUNIT, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractObjectMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractObjectMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.SPATIALUNIT, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable object types. 
     *
     *  @return the set of object types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIALUNIT or SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getObjectTypes() {
        return (this.types.toArray());
    }


    /**
     *  Tests if the given object type is supported. 
     *
     *  @param  objectType an object Type 
     *  @return <code> true </code> if the type is supported, <code>
     *          false </code> otherwise
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPATIALUNIT </code> 
     *  @throws org.osid.NullArgumentException <code> objectType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public boolean supportsObjectType(org.osid.type.Type objectType) {
        return (this.types.contains(objectType));
    }


    /**
     *  Add support for an object type.
     *
     *  @param objectType the type of object
     *  @throws org.osid.NullArgumentException {@code
     *          objectType} is {@code null}
     */

    protected void addObjectType(org.osid.type.Type objectType) {
        this.types.add(objectType);
        return;
    }



    /**
     *  Gets the set of acceptable object values. 
     *
     *  @return a set of objects or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          OBJECT </code>
     */

    @OSID @Override
    public java.lang.Object[] getObjectSet() {
        return (this.set.toArray(new java.lang.Object[this.set.size()]));
    }

    
    /**
     *  Sets the object set.
     *
     *  @param values a collection of accepted object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setObjectSet(java.util.Collection<java.lang.Object> values) {
        this.set.clear();
        addToObjectSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the object set.
     *
     *  @param values a collection of accepted object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToObjectSet(java.util.Collection<java.lang.Object> values) {
        nullarg(values, "object set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the object set.
     *
     *  @param value an object value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToObjectSet(java.lang.Object value) {
        nullarg(value, "object value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the object set.
     *
     *  @param value an object value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromObjectSet(java.lang.Object value) {
        nullarg(value, "object value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the object set.
     */

    protected void clearObjectSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default object values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default object values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          OBJECT </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public java.lang.Object[] getDefaultObjectValues() {
        return (this.defvals.toArray(new java.lang.Object[this.defvals.size()]));
    }


    /**
     *  Sets the default object set.
     *
     *  @param values a collection of default object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultObjectValues(java.util.Collection<java.lang.Object> values) {
        clearDefaultObjectValues();
        addDefaultObjectValues(values);
        return;
    }


    /**
     *  Adds a collection of default object values.
     *
     *  @param values a collection of default object values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultObjectValues(java.util.Collection<java.lang.Object> values) {
        nullarg(values, "default object values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultObjectValue(java.lang.Object value) {
        nullarg(value, "default object value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultObjectValue(java.lang.Object value) {
        nullarg(value, "default object value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default object values.
     */

    protected void clearDefaultObjectValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing object values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing object values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          OBJECT </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public java.lang.Object[] getExistingObjectValues() {
        return (this.existing.toArray(new java.lang.Object[this.existing.size()]));
    }


    /**
     *  Sets the existing object set.
     *
     *  @param values a collection of existing object values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingObjectValues(java.util.Collection<java.lang.Object> values) {
        clearExistingObjectValues();
        addExistingObjectValues(values);
        return;
    }


    /**
     *  Adds a collection of existing object values.
     *
     *  @param values a collection of existing object values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingObjectValues(java.util.Collection<java.lang.Object> values) {
        nullarg(values, "existing object values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingObjectValue(java.lang.Object value) {
        nullarg(value, "existing object value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingObjectValue(java.lang.Object value) {
        nullarg(value, "existing object value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing object values.
     */

    protected void clearExistingObjectValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }            
}