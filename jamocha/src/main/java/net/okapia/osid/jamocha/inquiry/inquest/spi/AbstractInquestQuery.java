//
// AbstractInquestQuery.java
//
//     A template for making an Inquest Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquest.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for inquests.
 */

public abstract class AbstractInquestQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.inquiry.InquestQuery {

    private final java.util.Collection<org.osid.inquiry.records.InquestQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the inquiry <code> Id </code> for this query to match inquests 
     *  that have a related response. 
     *
     *  @param  inquiryId an inquiry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquiryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryId(org.osid.id.Id inquiryId, boolean match) {
        return;
    }


    /**
     *  Clears the inquiry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquiryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquiry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquiry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquiry query 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuery getInquiryQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryQuery() is false");
    }


    /**
     *  Matches inquests that have any inquiry. 
     *
     *  @param  match <code> true </code> to match inquests with any inquiry, 
     *          <code> false </code> to match inquests with no inquiry 
     */

    @OSID @Override
    public void matchAnyInquiry(boolean match) {
        return;
    }


    /**
     *  Clears the inquiry query terms. 
     */

    @OSID @Override
    public void clearInquiryTerms() {
        return;
    }


    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuditId(org.osid.id.Id auditId, boolean match) {
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuditIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getAuditQuery() {
        throw new org.osid.UnimplementedException("supportsAuditQuery() is false");
    }


    /**
     *  Matches inquests with any audit. 
     *
     *  @param  match <code> true </code> to match inquests with any audit, 
     *          <code> false </code> to match inquests with no audit 
     */

    @OSID @Override
    public void matchAnyAudit(boolean match) {
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearAuditTerms() {
        return;
    }


    /**
     *  Sets the response <code> Id </code> for this query. 
     *
     *  @param  responseId the response <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> responseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResponseId(org.osid.id.Id responseId, boolean match) {
        return;
    }


    /**
     *  Clears the response <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResponseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResponseQuery </code> is available. 
     *
     *  @return <code> true </code> if a response query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a response. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the response query 
     *  @throws org.osid.UnimplementedException <code> supportsResponseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseQuery getResponseQuery() {
        throw new org.osid.UnimplementedException("supportsResponseQuery() is false");
    }


    /**
     *  Matches inquests with any response. 
     *
     *  @param  match <code> true </code> to match inquests with any response, 
     *          <code> false </code> to match inquests with no response 
     */

    @OSID @Override
    public void matchAnyResponse(boolean match) {
        return;
    }


    /**
     *  Clears the response query terms. 
     */

    @OSID @Override
    public void clearResponseTerms() {
        return;
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquests 
     *  that have the specified inquest as an ancestor. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorInquestId(org.osid.id.Id inquestId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorInquestIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorInquestQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getAncestorInquestQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorInquestQuery() is false");
    }


    /**
     *  Matches inquests with any ancestor. 
     *
     *  @param  match <code> true </code> to match inquests with any ancestor, 
     *          <code> false </code> to match root inquests 
     */

    @OSID @Override
    public void matchAnyAncestorInquest(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor inquest query terms. 
     */

    @OSID @Override
    public void clearAncestorInquestTerms() {
        return;
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquests 
     *  that have the specified inquest as a descendant. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantInquestId(org.osid.id.Id inquestId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantInquestIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest/ Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantInquestQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getDescendantInquestQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantInquestQuery() is false");
    }


    /**
     *  Matches inquests with any descendant. 
     *
     *  @param  match <code> true </code> to match inquests with any 
     *          descendant, <code> false </code> to match leaf inquests 
     */

    @OSID @Override
    public void matchAnyDescendantInquest(boolean match) {
        return;
    }


    /**
     *  Clears the descendant inquest query terms. 
     */

    @OSID @Override
    public void clearDescendantInquestTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given inquest query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an inquest implementing the requested record.
     *
     *  @param inquestRecordType an inquest record type
     *  @return the inquest query record
     *  @throws org.osid.NullArgumentException
     *          <code>inquestRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestQueryRecord getInquestQueryRecord(org.osid.type.Type inquestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquestQueryRecord record : this.records) {
            if (record.implementsRecordType(inquestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquest query. 
     *
     *  @param inquestQueryRecord inquest query record
     *  @param inquestRecordType inquest record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquestQueryRecord(org.osid.inquiry.records.InquestQueryRecord inquestQueryRecord, 
                                          org.osid.type.Type inquestRecordType) {

        addRecordType(inquestRecordType);
        nullarg(inquestQueryRecord, "inquest query record");
        this.records.add(inquestQueryRecord);        
        return;
    }
}
