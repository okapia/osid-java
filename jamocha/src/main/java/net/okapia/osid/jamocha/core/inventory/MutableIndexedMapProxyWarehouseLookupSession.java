//
// MutableIndexedMapProxyWarehouseLookupSession
//
//    Implements a Warehouse lookup service backed by a collection of
//    warehouses indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Warehouse lookup service backed by a collection of
 *  warehouses. The warehouses are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some warehouses may be compatible
 *  with more types than are indicated through these warehouse
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of warehouses can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyWarehouseLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractIndexedMapWarehouseLookupSession
    implements org.osid.inventory.WarehouseLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyWarehouseLookupSession} with
     *  no warehouse.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyWarehouseLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyWarehouseLookupSession} with
     *  a single warehouse.
     *
     *  @param  warehouse an warehouse
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyWarehouseLookupSession(org.osid.inventory.Warehouse warehouse, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putWarehouse(warehouse);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyWarehouseLookupSession} using
     *  an array of warehouses.
     *
     *  @param  warehouses an array of warehouses
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouses} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyWarehouseLookupSession(org.osid.inventory.Warehouse[] warehouses, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putWarehouses(warehouses);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyWarehouseLookupSession} using
     *  a collection of warehouses.
     *
     *  @param  warehouses a collection of warehouses
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouses} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyWarehouseLookupSession(java.util.Collection<? extends org.osid.inventory.Warehouse> warehouses,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putWarehouses(warehouses);
        return;
    }

    
    /**
     *  Makes a {@code Warehouse} available in this session.
     *
     *  @param  warehouse a warehouse
     *  @throws org.osid.NullArgumentException {@code warehouse{@code 
     *          is {@code null}
     */

    @Override
    public void putWarehouse(org.osid.inventory.Warehouse warehouse) {
        super.putWarehouse(warehouse);
        return;
    }


    /**
     *  Makes an array of warehouses available in this session.
     *
     *  @param  warehouses an array of warehouses
     *  @throws org.osid.NullArgumentException {@code warehouses{@code 
     *          is {@code null}
     */

    @Override
    public void putWarehouses(org.osid.inventory.Warehouse[] warehouses) {
        super.putWarehouses(warehouses);
        return;
    }


    /**
     *  Makes collection of warehouses available in this session.
     *
     *  @param  warehouses a collection of warehouses
     *  @throws org.osid.NullArgumentException {@code warehouse{@code 
     *          is {@code null}
     */

    @Override
    public void putWarehouses(java.util.Collection<? extends org.osid.inventory.Warehouse> warehouses) {
        super.putWarehouses(warehouses);
        return;
    }


    /**
     *  Removes a Warehouse from this session.
     *
     *  @param warehouseId the {@code Id} of the warehouse
     *  @throws org.osid.NullArgumentException {@code warehouseId{@code  is
     *          {@code null}
     */

    @Override
    public void removeWarehouse(org.osid.id.Id warehouseId) {
        super.removeWarehouse(warehouseId);
        return;
    }    
}
