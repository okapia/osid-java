//
// AbstractMutableGradebookColumnCalculation.java
//
//     Defines a mutable GradebookColumnCalculation.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>GradebookColumnCalculation</code>.
 */

public abstract class AbstractMutableGradebookColumnCalculation
    extends net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.spi.AbstractGradebookColumnCalculation
    implements org.osid.grading.calculation.GradebookColumnCalculation,
               net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.GradebookColumnCalculationMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this gradebook column calculation. 
     *
     *  @param record gradebook column calculation record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addGradebookColumnCalculationRecord(org.osid.grading.calculation.records.GradebookColumnCalculationRecord record, org.osid.type.Type recordType) {
        super.addGradebookColumnCalculationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this gradebook column calculation. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this gradebook column calculation. Disabling an
     *  operable overrides any enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this gradebook column calculation.
     *
     *  @param displayName the name for this gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this gradebook column calculation.
     *
     *  @param description the description of this gradebook column calculation
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException
     *          <code>rule</code> is <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    @Override
    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.setGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Adds an input gradebook column.
     *
     *  @param gradebookColumn an input gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    @Override
    public void addInputGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.addInputGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Sets all the input gradebook columns.
     *
     *  @param gradebookColumns a collection of input gradebook columns
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumns</code> is <code>null</code>
     */

    @Override
    public void setInputGradebookColumns(java.util.Collection<org.osid.grading.GradebookColumn> gradebookColumns) {
        super.setInputGradebookColumns(gradebookColumns);
        return;
    }


    /**
     *  Sets the operation.
     *
     *  @param operation an operation
     *  @throws org.osid.NullArgumentException <code>operation</code>
     *          is <code>null</code>
     */

    @Override
    public void setOperation(org.osid.grading.calculation.CalculationOperation operation) {
        super.setOperation(operation);
        return;
    }


    /**
     *  Sets the tweaked center.
     *
     *  @param center a tweaked center
     *  @throws org.osid.NullArgumentException <code>center</code> is
     *          <code>null</code>
     */

    @Override
    public void setTweakedCenter(java.math.BigDecimal center) {
        super.setTweakedCenter(center);
        return;
    }


    /**
     *  Sets the tweaked standard deviation.
     *
     *  @param standardDeviation a tweaked standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    @Override
    public void setTweakedStandardDeviation(java.math.BigDecimal standardDeviation) {
        super.setTweakedStandardDeviation(standardDeviation);
        return;
    }
}

