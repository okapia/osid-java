//
// AbstractTimePeriodQueryInspector.java
//
//     A template for making a TimePeriodQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for time periods.
 */

public abstract class AbstractTimePeriodQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.calendaring.TimePeriodQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.TimePeriodQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the start terms. 
     *
     *  @return the start terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getStartTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the end terms. 
     *
     *  @return the end terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getEndTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the time terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getTimeTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the inclusive time terms. 
     *
     *  @return the time range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeInclusiveTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationTerm[0]);
    }


    /**
     *  Gets the exception event <code> Id </code> terms. 
     *
     *  @return the exception event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExceptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the exception event terms. 
     *
     *  @return the exception event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getExceptionTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the event <code> Id </code> terms. 
     *
     *  @return the event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the event terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given time period query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a time period implementing the requested record.
     *
     *  @param timePeriodRecordType a time period record type
     *  @return the time period query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodQueryInspectorRecord getTimePeriodQueryInspectorRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this time period query. 
     *
     *  @param timePeriodQueryInspectorRecord time period query inspector
     *         record
     *  @param timePeriodRecordType timePeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTimePeriodQueryInspectorRecord(org.osid.calendaring.records.TimePeriodQueryInspectorRecord timePeriodQueryInspectorRecord, 
                                                   org.osid.type.Type timePeriodRecordType) {

        addRecordType(timePeriodRecordType);
        nullarg(timePeriodRecordType, "time period record type");
        this.records.add(timePeriodQueryInspectorRecord);        
        return;
    }
}
