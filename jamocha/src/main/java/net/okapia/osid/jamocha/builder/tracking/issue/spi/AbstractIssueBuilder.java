//
// AbstractIssue.java
//
//     Defines an Issue builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.issue.spi;


/**
 *  Defines an <code>Issue</code> builder.
 */

public abstract class AbstractIssueBuilder<T extends AbstractIssueBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.tracking.issue.IssueMiter issue;


    /**
     *  Constructs a new <code>AbstractIssueBuilder</code>.
     *
     *  @param issue the issue to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractIssueBuilder(net.okapia.osid.jamocha.builder.tracking.issue.IssueMiter issue) {
        super(issue);
        this.issue = issue;
        return;
    }


    /**
     *  Builds the issue.
     *
     *  @return the new issue
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.tracking.Issue build() {
        (new net.okapia.osid.jamocha.builder.validator.tracking.issue.IssueValidator(getValidations())).validate(this.issue);
        return (new net.okapia.osid.jamocha.builder.tracking.issue.ImmutableIssue(this.issue));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the issue miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.tracking.issue.IssueMiter getMiter() {
        return (this.issue);
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    public T queue(org.osid.tracking.Queue queue) {
        getMiter().setQueue(queue);
        return (self());
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    public T customer(org.osid.resource.Resource customer) {
        getMiter().setCustomer(customer);
        return (self());
    }


    /**
     *  Sets the topic.
     *
     *  @param topic a topic
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>topic</code> is
     *          <code>null</code>
     */

    public T topic(org.osid.ontology.Subject topic) {
        getMiter().setTopic(topic);
        return (self());
    }


    /**
     *  Sets the master issue.
     *
     *  @param issue a master issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T masterIssue(org.osid.tracking.Issue issue) {
        getMiter().setMasterIssue(issue);
        return (self());
    }


    /**
     *  Adds a duplicate issue.
     *
     *  @param issue a duplicate issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T duplicateIssue(org.osid.tracking.Issue issue) {
        getMiter().addDuplicateIssue(issue);
        return (self());
    }


    /**
     *  Sets all the duplicate issues.
     *
     *  @param issues a collection of duplicate issues
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issues</code> is
     *          <code>null</code>
     */

    public T duplicateIssues(java.util.Collection<org.osid.tracking.Issue> issues) {
        getMiter().setDuplicateIssues(issues);
        return (self());
    }


    /**
     *  Sets the branched issue.
     *
     *  @param issue a branched issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T branchedIssue(org.osid.tracking.Issue issue) {
        getMiter().setBranchedIssue(issue);
        return (self());
    }


    /**
     *  Sets the root issue.
     *
     *  @param issue a root issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T rootIssue(org.osid.tracking.Issue issue) {
        getMiter().setRootIssue(issue);
        return (self());
    }


    /**
     *  Sets the priority type.
     *
     *  @param priorityType a priority type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     */

    public T priorityType(org.osid.type.Type priorityType) {
        getMiter().setPriorityType(priorityType);
        return (self());
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    public T creator(org.osid.resource.Resource creator) {
        getMiter().setCreator(creator);
        return (self());
    }


    /**
     *  Sets the creating agent.
     *
     *  @param agent a creating agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T creatingAgent(org.osid.authentication.Agent agent) {
        getMiter().setCreatingAgent(agent);
        return (self());
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T createdDate(org.osid.calendaring.DateTime date) {
        getMiter().setCreatedDate(date);
        return (self());
    }


    /**
     *  Sets the reopener.
     *
     *  @param reopener a reopener
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>reopener</code>
     *          is <code>null</code>
     */

    public T reopener(org.osid.resource.Resource reopener) {
        getMiter().setReopener(reopener);
        return (self());
    }


    /**
     *  Sets the reopening agent.
     *
     *  @param agent a reopening agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T reopeningAgent(org.osid.authentication.Agent agent) {
        getMiter().setReopeningAgent(agent);
        return (self());
    }


    /**
     *  Sets the reopened date.
     *
     *  @param date a reopened date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T reopenedDate(org.osid.calendaring.DateTime date) {
        getMiter().setReopenedDate(date);
        return (self());
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dueDate(org.osid.calendaring.DateTime date) {
        getMiter().setDueDate(date);
        return (self());
    }


    /**
     *  Adds a blocker.
     *
     *  @param blocker a blocker
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>blocker</code> is
     *          <code>null</code>
     */

    public T blocker(org.osid.tracking.Issue blocker) {
        getMiter().addBlocker(blocker);
        return (self());
    }


    /**
     *  Sets all the blockers.
     *
     *  @param blockers a collection of blockers
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>blockers</code>
     *          is <code>null</code>
     */

    public T blockers(java.util.Collection<org.osid.tracking.Issue> blockers) {
        getMiter().setBlockers(blockers);
        return (self());
    }


    /**
     *  Sets the resolver.
     *
     *  @param resolver a resolver
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resolver</code>
     *          is <code>null</code>
     */

    public T resolver(org.osid.resource.Resource resolver) {
        getMiter().setResolver(resolver);
        return (self());
    }


    /**
     *  Sets the resolving agent.
     *
     *  @param agent a resolving agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T resolvingAgent(org.osid.authentication.Agent agent) {
        getMiter().setResolvingAgent(agent);
        return (self());
    }


    /**
     *  Sets the resolved date.
     *
     *  @param date a resolved date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T resolvedDate(org.osid.calendaring.DateTime date) {
        getMiter().setResolvedDate(date);
        return (self());
    }


    /**
     *  Sets the resolution type.
     *
     *  @param resolutionType a resolution type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resolutionType</code> is <code>null</code>
     */

    public T resolutionType(org.osid.type.Type resolutionType) {
        getMiter().setResolutionType(resolutionType);
        return (self());
    }


    /**
     *  Sets the closer.
     *
     *  @param closer a closer
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>closer</code> is
     *          <code>null</code>
     */

    public T closer(org.osid.resource.Resource closer) {
        getMiter().setCloser(closer);
        return (self());
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent a closing agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T closingAgent(org.osid.authentication.Agent agent) {
        getMiter().setClosingAgent(agent);
        return (self());
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T closedDate(org.osid.calendaring.DateTime date) {
        getMiter().setClosedDate(date);
        return (self());
    }


    /**
     *  Sets the assigned resource.
     *
     *  @param resource an assigned resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T assignedResource(org.osid.resource.Resource resource) {
        getMiter().setAssignedResource(resource);
        return (self());
    }


    /**
     *  Adds an Issue record.
     *
     *  @param record an issue record
     *  @param recordType the type of issue record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.tracking.records.IssueRecord record, org.osid.type.Type recordType) {
        getMiter().addIssueRecord(record, recordType);
        return (self());
    }
}       


