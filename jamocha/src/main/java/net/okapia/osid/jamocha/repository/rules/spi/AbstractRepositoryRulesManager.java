//
// AbstractRepositoryRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRepositoryRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.repository.rules.RepositoryRulesManager,
               org.osid.repository.rules.RepositoryRulesProxyManager {

    private final Types compositionEnablerRecordTypes      = new TypeRefSet();
    private final Types compositionEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRepositoryRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRepositoryRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching composition enablers is supported. 
     *
     *  @return <code> true </code> if composition enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a composition enabler administrative service is supported. 
     *
     *  @return <code> true </code> if composition enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a composition enabler notification service is supported. 
     *
     *  @return <code> true </code> if composition enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a composition enabler repository lookup service is supported. 
     *
     *  @return <code> true </code> if a repository enabler composition lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRepository() {
        return (false);
    }


    /**
     *  Tests if a composition enabler repository service is supported. 
     *
     *  @return <code> true </code> if composition enabler repository 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRepositoryAssignment() {
        return (false);
    }


    /**
     *  Tests if a composition enabler repository lookup service is supported. 
     *
     *  @return <code> true </code> if a composition enabler repository 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSmartRepository() {
        return (false);
    }


    /**
     *  Tests if a composition enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a composition enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a composition enabler rule application service is supported. 
     *
     *  @return <code> true </code> if composition enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> CompositionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CompositionEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.compositionEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CompositionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  compositionEnablerRecordType a <code> Type </code> indicating 
     *          a <code> CompositionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerRecordType(org.osid.type.Type compositionEnablerRecordType) {
        return (this.compositionEnablerRecordTypes.contains(compositionEnablerRecordType));
    }


    /**
     *  Adds support for a composition enabler record type.
     *
     *  @param compositionEnablerRecordType a composition enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>compositionEnablerRecordType</code> is <code>null</code>
     */

    protected void addCompositionEnablerRecordType(org.osid.type.Type compositionEnablerRecordType) {
        this.compositionEnablerRecordTypes.add(compositionEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a composition enabler record type.
     *
     *  @param compositionEnablerRecordType a composition enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>compositionEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCompositionEnablerRecordType(org.osid.type.Type compositionEnablerRecordType) {
        this.compositionEnablerRecordTypes.remove(compositionEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CompositionEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CompositionEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.compositionEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CompositionEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  compositionEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CompositionEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCompositionEnablerSearchRecordType(org.osid.type.Type compositionEnablerSearchRecordType) {
        return (this.compositionEnablerSearchRecordTypes.contains(compositionEnablerSearchRecordType));
    }


    /**
     *  Adds support for a composition enabler search record type.
     *
     *  @param compositionEnablerSearchRecordType a composition enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>compositionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCompositionEnablerSearchRecordType(org.osid.type.Type compositionEnablerSearchRecordType) {
        this.compositionEnablerSearchRecordTypes.add(compositionEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a composition enabler search record type.
     *
     *  @param compositionEnablerSearchRecordType a composition enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>compositionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCompositionEnablerSearchRecordType(org.osid.type.Type compositionEnablerSearchRecordType) {
        this.compositionEnablerSearchRecordTypes.remove(compositionEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service. 
     *
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerLookupSession getCompositionEnablerLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service. 
     *
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerQuerySessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler query service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerQuerySession getCompositionEnablerQuerySessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerQuerySessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler search service. 
     *
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enablers earch service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerSearchSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enablers earch service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSearchSession getCompositionEnablerSearchSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerSearchSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service. 
     *
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler administration service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerAdminSession getCompositionEnablerAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSession(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSession(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service for the given repository. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no repository found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver </code> or <code> repositoryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSessionForRepository(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver, 
                                                                                                                                 org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler notification service for the given repository. 
     *
     *  @param  compositionEnablerReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no repository found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionEnablerReceiver, repositoryId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerNotificationSession getCompositionEnablerNotificationSessionForRepository(org.osid.repository.rules.CompositionEnablerReceiver compositionEnablerReceiver, 
                                                                                                                                 org.osid.id.Id repositoryId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup composition enabler 
     *  repository mappings for composition enablers. 
     *
     *  @return a <code> CompositionEnablerCompositionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositorySession getCompositionEnablerRepositorySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRepositorySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup composition 
     *  enabler/composition mappings for composition enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRepositorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositorySession getCompositionEnablerRepositorySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRepositorySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  composition enablers to repositories. 
     *
     *  @return a <code> CompositionEnablerRepositoryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerCompositionAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositoryAssignmentSession getCompositionEnablerRepositoryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  composition enablers to repositories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRepositoryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerCompositionAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRepositoryAssignmentSession getCompositionEnablerRepositoryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage composition enabler 
     *  smart repositories. 
     *
     *  @param  repositoryId the Id of the <code> Repository </code> 
     *  @return a <code> CompositionEnablerSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSmartRepository() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSmartRepositorySession getCompositionEnablerSmartRepositorySession(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerSmartRepositorySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage composition enabler 
     *  smart repositories. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerSmartRepository() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerSmartRepositorySession getCompositionEnablerSmartRepositorySession(org.osid.id.Id repositoryId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerSmartRepositorySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for looking up the rules applied to a 
     *  composition. 
     *
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  composition. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for the given composition for looking 
     *  up rules applied to a composition. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRuleLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler mapping lookup service for the given composition for looking 
     *  up rules applied to a composition. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleLookupSession getCompositionEnablerRuleLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRuleLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service to apply enablers to compositions. 
     *
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service to apply enablers to compositions. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service for the given composition to apply enablers 
     *  to compositions. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesManager.getCompositionEnablerRuleApplicationSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  enabler assignment service for the given composition to apply enablers 
     *  to compositions. 
     *
     *  @param  repositoryId the <code> Id </code> of the <code> Repository 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Repository </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerRuleApplicationSession getCompositionEnablerRuleApplicationSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.rules.RepositoryRulesProxyManager.getCompositionEnablerRuleApplicationSessionForRepository not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.compositionEnablerRecordTypes.clear();
        this.compositionEnablerRecordTypes.clear();

        this.compositionEnablerSearchRecordTypes.clear();
        this.compositionEnablerSearchRecordTypes.clear();

        return;
    }
}
