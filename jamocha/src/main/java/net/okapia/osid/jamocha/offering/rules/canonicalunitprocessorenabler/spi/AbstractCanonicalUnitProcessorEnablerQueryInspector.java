//
// AbstractCanonicalUnitProcessorEnablerQueryInspector.java
//
//     A template for making a CanonicalUnitProcessorEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for canonical unit processor enablers.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerQueryInspector {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the canonical unit processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCanonicalUnitProcessorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the canonical unit processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQueryInspector[] getRuledCanonicalUnitProcessorTerms() {
        return (new org.osid.offering.rules.CanonicalUnitProcessorQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given canonical unit processor enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a canonical unit processor enabler implementing the requested record.
     *
     *  @param canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type
     *  @return the canonical unit processor enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord getCanonicalUnitProcessorEnablerQueryInspectorRecord(org.osid.type.Type canonicalUnitProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor enabler query. 
     *
     *  @param canonicalUnitProcessorEnablerQueryInspectorRecord canonical unit processor enabler query inspector
     *         record
     *  @param canonicalUnitProcessorEnablerRecordType canonicalUnitProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorEnablerQueryInspectorRecord(org.osid.offering.rules.records.CanonicalUnitProcessorEnablerQueryInspectorRecord canonicalUnitProcessorEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {

        addRecordType(canonicalUnitProcessorEnablerRecordType);
        nullarg(canonicalUnitProcessorEnablerRecordType, "canonical unit processor enabler record type");
        this.records.add(canonicalUnitProcessorEnablerQueryInspectorRecord);        
        return;
    }
}
