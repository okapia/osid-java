//
// AbstractImmutableReceipt.java
//
//     Wraps a mutable Receipt to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.receipt.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Receipt</code> to hide modifiers. This
 *  wrapper provides an immutized Receipt from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying receipt whose state changes are visible.
 */

public abstract class AbstractImmutableReceipt
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.messaging.Receipt {

    private final org.osid.messaging.Receipt receipt;


    /**
     *  Constructs a new <code>AbstractImmutableReceipt</code>.
     *
     *  @param receipt the receipt to immutablize
     *  @throws org.osid.NullArgumentException <code>receipt</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableReceipt(org.osid.messaging.Receipt receipt) {
        super(receipt);
        this.receipt = receipt;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Message. </code> 
     *
     *  @return the message <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMessageId() {
        return (this.receipt.getMessageId());
    }


    /**
     *  Gets the <code> Message. </code> 
     *
     *  @return the message 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage()
        throws org.osid.OperationFailedException {

        return (this.receipt.getMessage());
    }


    /**
     *  Gets the time the message was received. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReceivedTime() {
        return (this.receipt.getReceivedTime());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that received 
     *  this message at this endpoint. 
     *
     *  @return the receiving agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReceivingAgentId() {
        return (this.receipt.getReceivingAgentId());
    }


    /**
     *  Gets the <code> Agent </code> that received this message at this 
     *  endpoint. 
     *
     *  @return the receiving agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getReceivingAgent()
        throws org.osid.OperationFailedException {

        return (this.receipt.getReceivingAgent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Resource </code> that 
     *  received this message at this endpoint. 
     *
     *  @return the recipient <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.receipt.getRecipientId());
    }


    /**
     *  Gets the <code> Resource </code> that received this message at this 
     *  endpoint. 
     *
     *  @return the recipient 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.receipt.getRecipient());
    }


    /**
     *  Gets the receipt record corresponding to the given <code> Receipt 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> receiptRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(receiptRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  receiptRecordType the receipt record type 
     *  @return the receipt record 
     *  @throws org.osid.NullArgumentException <code> receiptRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(receiptRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptRecord getReceiptRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        return (this.receipt.getReceiptRecord(receiptRecordType));
    }
}

