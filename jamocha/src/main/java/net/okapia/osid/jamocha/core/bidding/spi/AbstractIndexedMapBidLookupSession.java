//
// AbstractIndexedMapBidLookupSession.java
//
//    A simple framework for providing a Bid lookup service
//    backed by a fixed collection of bids with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Bid lookup service backed by a
 *  fixed collection of bids. The bids are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some bids may be compatible
 *  with more types than are indicated through these bid
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Bids</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBidLookupSession
    extends AbstractMapBidLookupSession
    implements org.osid.bidding.BidLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.Bid> bidsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.Bid>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.Bid> bidsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.Bid>());


    /**
     *  Makes a <code>Bid</code> available in this session.
     *
     *  @param  bid a bid
     *  @throws org.osid.NullArgumentException <code>bid<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBid(org.osid.bidding.Bid bid) {
        super.putBid(bid);

        this.bidsByGenus.put(bid.getGenusType(), bid);
        
        try (org.osid.type.TypeList types = bid.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.bidsByRecord.put(types.getNextType(), bid);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a bid from this session.
     *
     *  @param bidId the <code>Id</code> of the bid
     *  @throws org.osid.NullArgumentException <code>bidId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBid(org.osid.id.Id bidId) {
        org.osid.bidding.Bid bid;
        try {
            bid = getBid(bidId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.bidsByGenus.remove(bid.getGenusType());

        try (org.osid.type.TypeList types = bid.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.bidsByRecord.remove(types.getNextType(), bid);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBid(bidId);
        return;
    }


    /**
     *  Gets a <code>BidList</code> corresponding to the given
     *  bid genus <code>Type</code> which does not include
     *  bids of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known bids or an error results. Otherwise,
     *  the returned list may contain only those bids that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  bidGenusType a bid genus type 
     *  @return the returned <code>Bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByGenusType(org.osid.type.Type bidGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.bid.ArrayBidList(this.bidsByGenus.get(bidGenusType)));
    }


    /**
     *  Gets a <code>BidList</code> containing the given
     *  bid record <code>Type</code>. In plenary mode, the
     *  returned list contains all known bids or an error
     *  results. Otherwise, the returned list may contain only those
     *  bids that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  bidRecordType a bid record type 
     *  @return the returned <code>bid</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.BidList getBidsByRecordType(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.bid.ArrayBidList(this.bidsByRecord.get(bidRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.bidsByGenus.clear();
        this.bidsByRecord.clear();

        super.close();

        return;
    }
}
