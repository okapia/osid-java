//
// AbstractGradeEntryNotificationSession.java
//
//     A template for making GradeEntryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code GradeEntry} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code GradeEntry} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for grade entry entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractGradeEntryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.grading.GradeEntryNotificationSession {

    private boolean federated = false;
    private org.osid.grading.Gradebook gradebook = new net.okapia.osid.jamocha.nil.grading.gradebook.UnknownGradebook();


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.gradebook.getId());
    }

    
    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.gradebook);
    }


    /**
     *  Sets the {@code Gradebook}.
     *
     *  @param gradebook the gradebook for this session
     *  @throws org.osid.NullArgumentException {@code gradebook}
     *          is {@code null}
     */

    protected void setGradebook(org.osid.grading.Gradebook gradebook) {
        nullarg(gradebook, "gradebook");
        this.gradebook = gradebook;
        return;
    }


    /**
     *  Tests if this user can register for {@code GradeEntry}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForGradeEntryNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for entries in gradebooks which
     *  are children of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new grade entries. {@code
     *  GradeEntryReceiver.newGradeEntry()} is invoked when a new
     *  {@code GradeEntry} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new gradeEntries for the given
     *  gradebook column {@code Id}. {@code
     *  GradeEntryReceiver.newGradeEntry()} is invoked when a new
     *  {@code GradeEntry} is created.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new gradeEntries for the given
     *  resource {@code Id}. {@code
     *  GradeEntryReceiver.newGradeEntry()} is invoked when a new
     *  {@code GradeEntry} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewGradeEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a new grade entry for the
     *  specified grader agent. {@code
     *  GradeEntryReceiver.newGradeEntry()} is invoked when a new
     *  entry for the grader is created.
     *
     *  @param resourceId the {@code Id} of the {@code Agent} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code agentId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated grade entries. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when a
     *  grade entry is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated gradeEntries for the
     *  given gradebook column {@code Id}. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when a
     *  {@code GradeEntry} in this gradebook is changed.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated gradeEntries for the
     *  given resource {@code Id}. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when a
     *  {@code GradeEntry} in this gradebook is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedGradeEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated grade entry for the
     *  specified grader. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when an
     *  entry for the agent is updated.
     *
     *  @param resourceId the {@code Id} of the {@code Agent} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code agentId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated grade entry. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when the
     *  specified grade entry is changed.
     *
     *  @param gradeEntryId the {@code Id} of the {@code GradeEntry} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code gradeEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted grade entries. {@code
     *  GradeEntryReceiver.deletedGradeEntry()} is invoked when a
     *  grade entry is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradeEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted gradeEntries for the
     *  given gradebook column {@code Id}. {@code
     *  GradeEntryReceiver.deletedGradeEntry()} is invoked when a
     *  {@code GradeEntry} is deleted or removed from this gradebook.
     *
     *  @param  gradebookColumnId the {@code Id} of the gradebook column to monitor
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedGradeEntriesForGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted gradeEntries for the
     *  given resource {@code Id}. {@code
     *  GradeEntryReceiver.deletedGradeEntry()} is invoked when a
     *  {@code GradeEntry} is deleted or removed from this gradebook.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedGradeEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted grade entry for the
     *  specified grader. {@code
     *  GradeEntryReceiver.changedGradeEntry()} is invoked when an
     *  entry for the agent is removed from this gradebook.
     *
     *  @param resourceId the {@code Id} of the {@code Agent} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code agentId is null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedGradeEntriesByGrader(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted grade entry. {@code
     *  GradeEntryReceiver.deletedGradeEntry()} is invoked when the
     *  specified grade entry is deleted.
     *
     *  @param gradeEntryId the {@code Id} of the
     *          {@code GradeEntry} to monitor
     *  @throws org.osid.NullArgumentException {@code gradeEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedGradeEntry(org.osid.id.Id gradeEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
