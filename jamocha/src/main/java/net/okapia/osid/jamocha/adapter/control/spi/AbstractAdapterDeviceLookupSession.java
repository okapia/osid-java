//
// AbstractAdapterDeviceLookupSession.java
//
//    A Device lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Device lookup session adapter.
 */

public abstract class AbstractAdapterDeviceLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.DeviceLookupSession {

    private final org.osid.control.DeviceLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDeviceLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDeviceLookupSession(org.osid.control.DeviceLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code Device} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDevices() {
        return (this.session.canLookupDevices());
    }


    /**
     *  A complete view of the {@code Device} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDeviceView() {
        this.session.useComparativeDeviceView();
        return;
    }


    /**
     *  A complete view of the {@code Device} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDeviceView() {
        this.session.usePlenaryDeviceView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include devices in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    
     
    /**
     *  Gets the {@code Device} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Device} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Device} and
     *  retained for compatibility.
     *
     *  @param deviceId {@code Id} of the {@code Device}
     *  @return the device
     *  @throws org.osid.NotFoundException {@code deviceId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code deviceId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Device getDevice(org.osid.id.Id deviceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevice(deviceId));
    }


    /**
     *  Gets a {@code DeviceList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  devices specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Devices} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  deviceIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Device} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code deviceIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByIds(org.osid.id.IdList deviceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevicesByIds(deviceIds));
    }


    /**
     *  Gets a {@code DeviceList} corresponding to the given
     *  device genus {@code Type} which does not include
     *  devices of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  devices or an error results. Otherwise, the returned list
     *  may contain only those devices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  deviceGenusType a device genus type 
     *  @return the returned {@code Device} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByGenusType(org.osid.type.Type deviceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevicesByGenusType(deviceGenusType));
    }


    /**
     *  Gets a {@code DeviceList} corresponding to the given
     *  device genus {@code Type} and include any additional
     *  devices with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  devices or an error results. Otherwise, the returned list
     *  may contain only those devices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  deviceGenusType a device genus type 
     *  @return the returned {@code Device} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByParentGenusType(org.osid.type.Type deviceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevicesByParentGenusType(deviceGenusType));
    }


    /**
     *  Gets a {@code DeviceList} containing the given
     *  device record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  devices or an error results. Otherwise, the returned list
     *  may contain only those devices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  deviceRecordType a device record type 
     *  @return the returned {@code Device} list
     *  @throws org.osid.NullArgumentException
     *          {@code deviceRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevicesByRecordType(org.osid.type.Type deviceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevicesByRecordType(deviceRecordType));
    }


    /**
     *  Gets all {@code Devices}. 
     *
     *  In plenary mode, the returned list contains all known
     *  devices or an error results. Otherwise, the returned list
     *  may contain only those devices that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Devices} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDevices());
    }
}
