//
// AbstractMapInquiryLookupSession
//
//    A simple framework for providing an Inquiry lookup service
//    backed by a fixed collection of inquiries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Inquiry lookup service backed by a
 *  fixed collection of inquiries. The inquiries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inquiries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInquiryLookupSession
    extends net.okapia.osid.jamocha.inquiry.spi.AbstractInquiryLookupSession
    implements org.osid.inquiry.InquiryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inquiry.Inquiry> inquiries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inquiry.Inquiry>());


    /**
     *  Makes an <code>Inquiry</code> available in this session.
     *
     *  @param  inquiry an inquiry
     *  @throws org.osid.NullArgumentException <code>inquiry<code>
     *          is <code>null</code>
     */

    protected void putInquiry(org.osid.inquiry.Inquiry inquiry) {
        this.inquiries.put(inquiry.getId(), inquiry);
        return;
    }


    /**
     *  Makes an array of inquiries available in this session.
     *
     *  @param  inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException <code>inquiries<code>
     *          is <code>null</code>
     */

    protected void putInquiries(org.osid.inquiry.Inquiry[] inquiries) {
        putInquiries(java.util.Arrays.asList(inquiries));
        return;
    }


    /**
     *  Makes a collection of inquiries available in this session.
     *
     *  @param  inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException <code>inquiries<code>
     *          is <code>null</code>
     */

    protected void putInquiries(java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {
        for (org.osid.inquiry.Inquiry inquiry : inquiries) {
            this.inquiries.put(inquiry.getId(), inquiry);
        }

        return;
    }


    /**
     *  Removes an Inquiry from this session.
     *
     *  @param  inquiryId the <code>Id</code> of the inquiry
     *  @throws org.osid.NullArgumentException <code>inquiryId<code> is
     *          <code>null</code>
     */

    protected void removeInquiry(org.osid.id.Id inquiryId) {
        this.inquiries.remove(inquiryId);
        return;
    }


    /**
     *  Gets the <code>Inquiry</code> specified by its <code>Id</code>.
     *
     *  @param  inquiryId <code>Id</code> of the <code>Inquiry</code>
     *  @return the inquiry
     *  @throws org.osid.NotFoundException <code>inquiryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inquiryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquiry getInquiry(org.osid.id.Id inquiryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inquiry.Inquiry inquiry = this.inquiries.get(inquiryId);
        if (inquiry == null) {
            throw new org.osid.NotFoundException("inquiry not found: " + inquiryId);
        }

        return (inquiry);
    }


    /**
     *  Gets all <code>Inquiries</code>. In plenary mode, the returned
     *  list contains all known inquiries or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquiries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Inquiries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquiry.ArrayInquiryList(this.inquiries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquiries.clear();
        super.close();
        return;
    }
}
