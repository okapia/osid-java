//
// AbstractQueueValidator.java
//
//     Validates a Queue.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.provisioning.queue.spi;


/**
 *  Validates a Queue.
 */

public abstract class AbstractQueueValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidGovernatorValidator {


    /**
     *  Constructs a new <code>AbstractQueueValidator</code>.
     */

    protected AbstractQueueValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractQueueValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractQueueValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Queue.
     *
     *  @param queue a queue to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>queue</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.provisioning.Queue queue) {
        super.validate(queue);

        testNestedObject(queue, "getBroker");

        testConditionalMethod(queue, "getSize", queue.hasSize(), "hasSize()");
        if (queue.hasSize()) {
            testCardinal(queue.getSize(), "getSize()");
        }

        testConditionalMethod(queue, "getEWA", queue.hasEWA(), "hasEWA()");
        if (queue.hasEWA()) {
            test(queue.getEWA(), "getEWA()");
        }

        return;
    }
}
