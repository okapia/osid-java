//
// AbstractAssessmentPartSearch.java
//
//     A template for making an AssessmentPart Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing assessment part searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssessmentPartSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.assessment.authoring.AssessmentPartSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.assessment.authoring.AssessmentPartSearchOrder assessmentPartSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assessment parts. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assessmentPartIds list of assessment parts
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssessmentParts(org.osid.id.IdList assessmentPartIds) {
        while (assessmentPartIds.hasNext()) {
            try {
                this.ids.add(assessmentPartIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssessmentParts</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of assessment part Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssessmentPartIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assessmentPartSearchOrder assessment part search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assessmentPartSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssessmentPartResults(org.osid.assessment.authoring.AssessmentPartSearchOrder assessmentPartSearchOrder) {
	this.assessmentPartSearchOrder = assessmentPartSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.assessment.authoring.AssessmentPartSearchOrder getAssessmentPartSearchOrder() {
	return (this.assessmentPartSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given assessment part search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment part implementing the requested record.
     *
     *  @param assessmentPartSearchRecordType an assessment part search record
     *         type
     *  @return the assessment part search record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartSearchRecord getAssessmentPartSearchRecord(org.osid.type.Type assessmentPartSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.assessment.authoring.records.AssessmentPartSearchRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment part search. 
     *
     *  @param assessmentPartSearchRecord assessment part search record
     *  @param assessmentPartSearchRecordType assessmentPart search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentPartSearchRecord(org.osid.assessment.authoring.records.AssessmentPartSearchRecord assessmentPartSearchRecord, 
                                           org.osid.type.Type assessmentPartSearchRecordType) {

        addRecordType(assessmentPartSearchRecordType);
        this.records.add(assessmentPartSearchRecord);        
        return;
    }
}
