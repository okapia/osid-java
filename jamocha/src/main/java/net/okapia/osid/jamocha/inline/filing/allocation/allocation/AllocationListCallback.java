//
// AllocationListCallback
//
//     Defines a callback for AllocationList events.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filing.allocation.allocation;


/**
 *  Defines a callback for AllocationList events.
 */

public interface AllocationListCallback {


    /**
     *  Notification for a new allocation.
     *
     *  @param list the list that originated the callback
     */

    public void newAllocationInList(org.osid.filing.allocation.AllocationList list);


    /**
     *  Notification for an error.
     *
     *  @param list the list that originated the callback
     */

    public void errorInList(org.osid.filing.allocation.AllocationList list);


    /**
     *  Notification for a invocation of <code>done()</code>
     *
     *  @param list the list that originated the callback
     */

    public void doneForList(org.osid.filing.allocation.AllocationList list);
}
