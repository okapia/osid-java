//
// AbstractIndexedMapBallotConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a BallotConstrainerEnabler lookup service
//    backed by a fixed collection of ballot constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BallotConstrainerEnabler lookup service backed by a
 *  fixed collection of ballot constrainer enablers. The ballot constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some ballot constrainer enablers may be compatible
 *  with more types than are indicated through these ballot constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BallotConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBallotConstrainerEnablerLookupSession
    extends AbstractMapBallotConstrainerEnablerLookupSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.BallotConstrainerEnabler> ballotConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.BallotConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.BallotConstrainerEnabler> ballotConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.BallotConstrainerEnabler>());


    /**
     *  Makes a <code>BallotConstrainerEnabler</code> available in this session.
     *
     *  @param  ballotConstrainerEnabler a ballot constrainer enabler
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBallotConstrainerEnabler(org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler) {
        super.putBallotConstrainerEnabler(ballotConstrainerEnabler);

        this.ballotConstrainerEnablersByGenus.put(ballotConstrainerEnabler.getGenusType(), ballotConstrainerEnabler);
        
        try (org.osid.type.TypeList types = ballotConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotConstrainerEnablersByRecord.put(types.getNextType(), ballotConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a ballot constrainer enabler from this session.
     *
     *  @param ballotConstrainerEnablerId the <code>Id</code> of the ballot constrainer enabler
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBallotConstrainerEnabler(org.osid.id.Id ballotConstrainerEnablerId) {
        org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler;
        try {
            ballotConstrainerEnabler = getBallotConstrainerEnabler(ballotConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.ballotConstrainerEnablersByGenus.remove(ballotConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = ballotConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ballotConstrainerEnablersByRecord.remove(types.getNextType(), ballotConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBallotConstrainerEnabler(ballotConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding to the given
     *  ballot constrainer enabler genus <code>Type</code> which does not include
     *  ballot constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known ballot constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those ballot constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballot constrainer enabler genus type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainerenabler.ArrayBallotConstrainerEnablerList(this.ballotConstrainerEnablersByGenus.get(ballotConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> containing the given
     *  ballot constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known ballot constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  ballot constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  ballotConstrainerEnablerRecordType a ballot constrainer enabler record type 
     *  @return the returned <code>ballotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.ballotconstrainerenabler.ArrayBallotConstrainerEnablerList(this.ballotConstrainerEnablersByRecord.get(ballotConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ballotConstrainerEnablersByGenus.clear();
        this.ballotConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
