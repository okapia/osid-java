//
// AbstractLoggingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractLoggingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.logging.LoggingManager,
               org.osid.logging.LoggingProxyManager {

    private final Types logEntryRecordTypes                = new TypeRefSet();
    private final Types logEntrySearchRecordTypes          = new TypeRefSet();

    private final Types logRecordTypes                     = new TypeRefSet();
    private final Types logSearchRecordTypes               = new TypeRefSet();

    private final Types priorityTypes                      = new TypeRefSet();
    private final Types contentTypes                       = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractLoggingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractLoggingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if logging is supported. 
     *
     *  @return <code> true </code> if logging is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogging() {
        return (false);
    }


    /**
     *  Tests if reading logs is supported. 
     *
     *  @return <code> true </code> if reading logs is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying log entries is supported. 
     *
     *  @return <code> true </code> if querying log entries is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching log entries is supported. 
     *
     *  @return <code> true </code> if searching log entries is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntrySearch() {
        return (false);
    }


    /**
     *  Tests if log entry notification is supported,. 
     *
     *  @return <code> true </code> if log entry notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryNotification() {
        return (false);
    }


    /**
     *  Tests if looking up log entry log mappings is supported. 
     *
     *  @return <code> true </code> if log entry logs is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLog() {
        return (false);
    }


    /**
     *  Tests if managing log entry log mappings is supported. 
     *
     *  @return <code> true </code> if log entry logs mapping assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLogAssignment() {
        return (false);
    }


    /**
     *  Tests if smart logs is supported. 
     *
     *  @return <code> true </code> if smart logs is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntrySmartLog() {
        return (false);
    }


    /**
     *  Tests for the availability of a log lookup service. 
     *
     *  @return <code> true </code> if log lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogLookup() {
        return (false);
    }


    /**
     *  Tests if querying logs is available. 
     *
     *  @return <code> true </code> if log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogQuery() {
        return (false);
    }


    /**
     *  Tests if searching for logs is available. 
     *
     *  @return <code> true </code> if log search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a log administrative service for 
     *  creating and deleting logs. 
     *
     *  @return <code> true </code> if log administration is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a log notification service. 
     *
     *  @return <code> true </code> if log notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a log hierarchy traversal service. 
     *
     *  @return <code> true </code> if log hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a log hierarchy design service. 
     *
     *  @return <code> true </code> if log hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a logging batch service. 
     *
     *  @return <code> true </code> if loggin batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Log </code> record types. 
     *
     *  @return a list containing the supported log record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.logEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> LogEntry </code> record type is supported. 
     *
     *  @param  logEntryRecordType a <code> Type </code> indicating a <code> 
     *          LogEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        return (this.logEntryRecordTypes.contains(logEntryRecordType));
    }


    /**
     *  Adds support for a log entry record type.
     *
     *  @param logEntryRecordType a log entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>logEntryRecordType</code> is <code>null</code>
     */

    protected void addLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        this.logEntryRecordTypes.add(logEntryRecordType);
        return;
    }


    /**
     *  Removes support for a log entry record type.
     *
     *  @param logEntryRecordType a log entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>logEntryRecordType</code> is <code>null</code>
     */

    protected void removeLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        this.logEntryRecordTypes.remove(logEntryRecordType);
        return;
    }


    /**
     *  Gets the supported log entry search record types. 
     *
     *  @return a list containing the supported log entry search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.logEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given log entry search record type is supported. 
     *
     *  @param  logEntrySearchRecordType a <code> Type </code> indicating a 
     *          log entry record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntrySearchRecordType(org.osid.type.Type logEntrySearchRecordType) {
        return (this.logEntrySearchRecordTypes.contains(logEntrySearchRecordType));
    }


    /**
     *  Adds support for a log entry search record type.
     *
     *  @param logEntrySearchRecordType a log entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>logEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addLogEntrySearchRecordType(org.osid.type.Type logEntrySearchRecordType) {
        this.logEntrySearchRecordTypes.add(logEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a log entry search record type.
     *
     *  @param logEntrySearchRecordType a log entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>logEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeLogEntrySearchRecordType(org.osid.type.Type logEntrySearchRecordType) {
        this.logEntrySearchRecordTypes.remove(logEntrySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Log </code> record types. 
     *
     *  @return a list containing the supported log record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.logRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Log </code> record type is supported. 
     *
     *  @param  logRecordType a <code> Type </code> indicating a <code> Log 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogRecordType(org.osid.type.Type logRecordType) {
        return (this.logRecordTypes.contains(logRecordType));
    }


    /**
     *  Adds support for a log record type.
     *
     *  @param logRecordType a log record type
     *  @throws org.osid.NullArgumentException
     *  <code>logRecordType</code> is <code>null</code>
     */

    protected void addLogRecordType(org.osid.type.Type logRecordType) {
        this.logRecordTypes.add(logRecordType);
        return;
    }


    /**
     *  Removes support for a log record type.
     *
     *  @param logRecordType a log record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>logRecordType</code> is <code>null</code>
     */

    protected void removeLogRecordType(org.osid.type.Type logRecordType) {
        this.logRecordTypes.remove(logRecordType);
        return;
    }


    /**
     *  Gets the supported log search record types. 
     *
     *  @return a list containing the supported log search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.logSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given log search record type is supported. 
     *
     *  @param  logSearchRecordType a <code> Type </code> indicating a log 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogSearchRecordType(org.osid.type.Type logSearchRecordType) {
        return (this.logSearchRecordTypes.contains(logSearchRecordType));
    }


    /**
     *  Adds support for a log search record type.
     *
     *  @param logSearchRecordType a log search record type
     *  @throws org.osid.NullArgumentException
     *  <code>logSearchRecordType</code> is <code>null</code>
     */

    protected void addLogSearchRecordType(org.osid.type.Type logSearchRecordType) {
        this.logSearchRecordTypes.add(logSearchRecordType);
        return;
    }


    /**
     *  Removes support for a log search record type.
     *
     *  @param logSearchRecordType a log search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>logSearchRecordType</code> is <code>null</code>
     */

    protected void removeLogSearchRecordType(org.osid.type.Type logSearchRecordType) {
        this.logSearchRecordTypes.remove(logSearchRecordType);
        return;
    }


    /**
     *  Gets the priority types supported, in ascending order of the priority 
     *  level. 
     *
     *  @return a list containing the supported priority types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriorityTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.priorityTypes.toCollection()));
    }


    /**
     *  Tests if the priority type is supported. 
     *
     *  @param  priorityType a <code> Type </code> indicating a priority type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriorityType(org.osid.type.Type priorityType) {
        return (this.priorityTypes.contains(priorityType));
    }


    /**
     *  Adds support for a priority type.
     *
     *  @param priorityType a priority type
     *  @throws org.osid.NullArgumentException
     *  <code>priorityType</code> is <code>null</code>
     */

    protected void addPriorityType(org.osid.type.Type priorityType) {
        this.priorityTypes.add(priorityType);
        return;
    }


    /**
     *  Removes support for a priority type.
     *
     *  @param priorityType a priority type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>priorityType</code> is <code>null</code>
     */

    protected void removePriorityType(org.osid.type.Type priorityType) {
        this.priorityTypes.remove(priorityType);
        return;
    }


    /**
     *  Gets the content types supported. 
     *
     *  @return a list containing the supported content types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContentTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.contentTypes.toCollection()));
    }


    /**
     *  Tests if the content type is supported. 
     *
     *  @param  contentType a <code> Type </code> indicating a content type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contentType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContentType(org.osid.type.Type contentType) {
        return (this.contentTypes.contains(contentType));
    }


    /**
     *  Adds support for a content type.
     *
     *  @param contentType a content type
     *  @throws org.osid.NullArgumentException
     *  <code>contentType</code> is <code>null</code>
     */

    protected void addContentType(org.osid.type.Type contentType) {
        this.contentTypes.add(contentType);
        return;
    }


    /**
     *  Removes support for a content type.
     *
     *  @param contentType a content type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>contentType</code> is <code>null</code>
     */

    protected void removeContentType(org.osid.type.Type contentType) {
        this.contentTypes.remove(contentType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service. 
     *
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLoggingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLoggingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLoggingSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSessionForLog(org.osid.id.Id logId, 
                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLoggingSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log reading 
     *  service. 
     *
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  reading service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log reading 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryLookupSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log reading 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSessionForLog(org.osid.id.Id logId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryLookupSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  query service. 
     *
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  query service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryQuerySessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  query service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySessionForLog(org.osid.id.Id logId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryQuerySessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  search service. 
     *
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  search service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntrySearchSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  search service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSessionForLog(org.osid.id.Id logId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntrySearchSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  administrative service. 
     *
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  administrative service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryAdminSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  administrative service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSessionForLog(org.osid.id.Id logId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryAdminSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.logging.LogEntryReceiver logEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.logging.LogEntryReceiver logEntryReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given log. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> logId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSessionForLog(org.osid.logging.LogEntryReceiver logEntryReceiver, 
                                                                                             org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryNotificationSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given log. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver, logId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSessionForLog(org.osid.logging.LogEntryReceiver logEntryReceiver, 
                                                                                             org.osid.id.Id logId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryNotificationSessionForLog not implemented");
    }


    /**
     *  Gets the session for retrieving log entry to log mappings. 
     *
     *  @return a <code> LogEntryLogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryLog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogSession getLogEntryLogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryLogSession not implemented");
    }


    /**
     *  Gets the session for retrieving log entry to log mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryLog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogSession getLogEntryLogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryLogSession not implemented");
    }


    /**
     *  Gets the session for assigning log entry to logs mappings. 
     *
     *  @return a <code> LogEntryLogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogAssignmentSession getLogEntryLogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntryLogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning log entry to log mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogAssignmentSession getLogEntryLogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntryLogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic logEntry log. 
     *
     *  @param  logId the <code> Id </code> of the log 
     *  @return a <code> LogEntrySmartLogSession </code> 
     *  @throws org.osid.NotFoundException <code> logId </code> not found 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySmartLog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySmartLogSession getLogEntrySmartLogSession(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogEntrySmartLogSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic log entry logs. 
     *
     *  @param  logId the <code> Id </code> of the log 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntrySmartLogSession </code> 
     *  @throws org.osid.NotFoundException <code> logId </code> not found 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySmartLog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySmartLogSession getLogEntrySmartLogSession(org.osid.id.Id logId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogEntrySmartLogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log lookup 
     *  service. 
     *
     *  @return a <code> LogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogLookupSession getLogLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogLookupSession getLogLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log query 
     *  service. 
     *
     *  @return a <code> LogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuerySession getLogQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuerySession getLogQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log search 
     *  service. 
     *
     *  @return a <code> LogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogSearchSession getLogSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogSearchSession getLogSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  administrative service. 
     *
     *  @return a <code> LogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogAdminSession getLogAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogAdminSession getLogAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  notification service. 
     *
     *  @param  logReceiver the receiver 
     *  @return a <code> LogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogNotificationSession getLogNotificationSession(org.osid.logging.LogReceiver logReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  notification service. 
     *
     *  @param  logReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> LogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogNotificationSession getLogNotificationSession(org.osid.logging.LogReceiver logReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  service. 
     *
     *  @return a <code> LogHierarchySession </code> for logs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchySession getLogHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogHierarchySession </code> for logs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchySession getLogHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for logs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchyDesignSession getLogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for logs 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchyDesignSession getLogHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> LoggingBatchManager. </code> 
     *
     *  @return a <code> LoggingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLoggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LoggingBatchManager getLoggingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingManager.getLoggingBatchManager not implemented");
    }


    /**
     *  Gets a <code> LoggingBatchProxyManager. </code> 
     *
     *  @return a <code> LoggingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLoggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LoggingBatchProxyManager getLoggingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.LoggingProxyManager.getLoggingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.logEntryRecordTypes.clear();
        this.logEntryRecordTypes.clear();

        this.logEntrySearchRecordTypes.clear();
        this.logEntrySearchRecordTypes.clear();

        this.logRecordTypes.clear();
        this.logRecordTypes.clear();

        this.logSearchRecordTypes.clear();
        this.logSearchRecordTypes.clear();

        this.priorityTypes.clear();
        this.priorityTypes.clear();

        this.contentTypes.clear();
        this.contentTypes.clear();

        return;
    }
}
