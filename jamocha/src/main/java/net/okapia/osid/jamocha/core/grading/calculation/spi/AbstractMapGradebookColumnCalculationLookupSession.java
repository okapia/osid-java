//
// AbstractMapGradebookColumnCalculationLookupSession
//
//    A simple framework for providing a GradebookColumnCalculation lookup service
//    backed by a fixed collection of gradebook column calculations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a GradebookColumnCalculation lookup service backed by a
 *  fixed collection of gradebook column calculations. The gradebook column calculations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradebookColumnCalculations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapGradebookColumnCalculationLookupSession
    extends net.okapia.osid.jamocha.grading.calculation.spi.AbstractGradebookColumnCalculationLookupSession
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.grading.calculation.GradebookColumnCalculation>());


    /**
     *  Makes a <code>GradebookColumnCalculation</code> available in this session.
     *
     *  @param  gradebookColumnCalculation a gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculation<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        this.gradebookColumnCalculations.put(gradebookColumnCalculation.getId(), gradebookColumnCalculation);
        return;
    }


    /**
     *  Makes an array of gradebook column calculations available in this session.
     *
     *  @param  gradebookColumnCalculations an array of gradebook column calculations
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculations<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumnCalculations(org.osid.grading.calculation.GradebookColumnCalculation[] gradebookColumnCalculations) {
        putGradebookColumnCalculations(java.util.Arrays.asList(gradebookColumnCalculations));
        return;
    }


    /**
     *  Makes a collection of gradebook column calculations available in this session.
     *
     *  @param  gradebookColumnCalculations a collection of gradebook column calculations
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculations<code>
     *          is <code>null</code>
     */

    protected void putGradebookColumnCalculations(java.util.Collection<? extends org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculations) {
        for (org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation : gradebookColumnCalculations) {
            this.gradebookColumnCalculations.put(gradebookColumnCalculation.getId(), gradebookColumnCalculation);
        }

        return;
    }


    /**
     *  Removes a GradebookColumnCalculation from this session.
     *
     *  @param  gradebookColumnCalculationId the <code>Id</code> of the gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculationId<code> is
     *          <code>null</code>
     */

    protected void removeGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId) {
        this.gradebookColumnCalculations.remove(gradebookColumnCalculationId);
        return;
    }


    /**
     *  Gets the <code>GradebookColumnCalculation</code> specified by its <code>Id</code>.
     *
     *  @param  gradebookColumnCalculationId <code>Id</code> of the <code>GradebookColumnCalculation</code>
     *  @return the gradebookColumnCalculation
     *  @throws org.osid.NotFoundException <code>gradebookColumnCalculationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculation getGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation = this.gradebookColumnCalculations.get(gradebookColumnCalculationId);
        if (gradebookColumnCalculation == null) {
            throw new org.osid.NotFoundException("gradebookColumnCalculation not found: " + gradebookColumnCalculationId);
        }

        return (gradebookColumnCalculation);
    }


    /**
     *  Gets all <code>GradebookColumnCalculations</code>. In plenary mode, the returned
     *  list contains all known gradebookColumnCalculations or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebookColumnCalculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>GradebookColumnCalculations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.ArrayGradebookColumnCalculationList(this.gradebookColumnCalculations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebookColumnCalculations.clear();
        super.close();
        return;
    }
}
