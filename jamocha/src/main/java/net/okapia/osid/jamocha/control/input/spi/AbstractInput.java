//
// AbstractInput.java
//
//     Defines an Input.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Input</code>.
 */

public abstract class AbstractInput
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.control.Input {

    private org.osid.control.Device device;
    private org.osid.control.Controller controller;

    private final java.util.Collection<org.osid.control.records.InputRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the device <code> Id. </code> 
     *
     *  @return the device <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDeviceId() {
        return (this.device.getId());
    }


    /**
     *  Gets the device. 
     *
     *  @return the device 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Device getDevice()
        throws org.osid.OperationFailedException {

        return (this.device);
    }


    /**
     *  Sets the device.
     *
     *  @param device a device
     *  @throws org.osid.NullArgumentException <code>device</code> is
     *          <code>null</code>
     */

    protected void setDevice(org.osid.control.Device device) {
        nullarg(device, "device");
        this.device = device;
        return;
    }


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.controller.getId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.controller);
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    protected void setController(org.osid.control.Controller controller) {
        nullarg(controller, "controller");
        this.controller = controller;
        return;
    }


    /**
     *  Tests if this input supports the given record
     *  <code>Type</code>.
     *
     *  @param  inputRecordType an input record type 
     *  @return <code>true</code> if the inputRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type inputRecordType) {
        for (org.osid.control.records.InputRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  inputRecordType the input record type 
     *  @return the input record 
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputRecord getInputRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Adds a record to this input. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param inputRecord the input record
     *  @param inputRecordType input record type
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecord</code> or
     *          <code>inputRecordTypeinput</code> is
     *          <code>null</code>
     */
            
    protected void addInputRecord(org.osid.control.records.InputRecord inputRecord, 
                                     org.osid.type.Type inputRecordType) {

        addRecordType(inputRecordType);
        this.records.add(inputRecord);
        
        return;
    }
}
