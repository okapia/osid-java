//
// AbstractQueryActivityLookupSession.java
//
//    An ActivityQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityQuerySession adapter.
 */

public abstract class AbstractAdapterActivityQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.learning.ActivityQuerySession {

    private final org.osid.learning.ActivityQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterActivityQuerySession.
     *
     *  @param session the underlying activity query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityQuerySession(org.osid.learning.ActivityQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeObjectiveBank</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the {@codeObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the {@codeObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform {@codeActivity</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchActivities() {
        return (this.session.canSearchActivities());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this objective bank only.
     */
    
    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    
      
    /**
     *  Gets an activity query. The returned query will not have an
     *  extension query.
     *
     *  @return the activity query 
     */
      
    @OSID @Override
    public org.osid.learning.ActivityQuery getActivityQuery() {
        return (this.session.getActivityQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  activityQuery the activity query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code activityQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code activityQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByQuery(org.osid.learning.ActivityQuery activityQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getActivitiesByQuery(activityQuery));
    }
}
