//
// AbstractAdapterPollsLookupSession.java
//
//    A Polls lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Polls lookup session adapter.
 */

public abstract class AbstractAdapterPollsLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.PollsLookupSession {

    private final org.osid.voting.PollsLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPollsLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPollsLookupSession(org.osid.voting.PollsLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Polls} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPolls() {
        return (this.session.canLookupPolls());
    }


    /**
     *  A complete view of the {@code Polls} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePollsView() {
        this.session.useComparativePollsView();
        return;
    }


    /**
     *  A complete view of the {@code Polls} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPollsView() {
        this.session.usePlenaryPollsView();
        return;
    }

     
    /**
     *  Gets the {@code Polls} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Polls} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Polls} and
     *  retained for compatibility.
     *
     *  @param pollsId {@code Id} of the {@code Polls}
     *  @return the polls
     *  @throws org.osid.NotFoundException {@code pollsId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code pollsId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPolls(pollsId));
    }


    /**
     *  Gets a {@code PollsList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  polls specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Polls} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  pollsIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Polls} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code pollsIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByIds(org.osid.id.IdList pollsIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPollsByIds(pollsIds));
    }


    /**
     *  Gets a {@code PollsList} corresponding to the given
     *  polls genus {@code Type} which does not include
     *  polls of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned {@code Polls} list
     *  @throws org.osid.NullArgumentException
     *          {@code pollsGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPollsByGenusType(pollsGenusType));
    }


    /**
     *  Gets a {@code PollsList} corresponding to the given
     *  polls genus {@code Type} and include any additional
     *  polls with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned {@code Polls} list
     *  @throws org.osid.NullArgumentException
     *          {@code pollsGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByParentGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPollsByParentGenusType(pollsGenusType));
    }


    /**
     *  Gets a {@code PollsList} containing the given
     *  polls record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned {@code Polls} list
     *  @throws org.osid.NullArgumentException
     *          {@code pollsRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPollsByRecordType(pollsRecordType));
    }


    /**
     *  Gets a {@code PollsList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Polls} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPollsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Polls}. 
     *
     *  In plenary mode, the returned list contains all known
     *  polls or an error results. Otherwise, the returned list
     *  may contain only those polls that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Polls} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getAllPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAllPolls());
    }
}
