//
// MutableIndexedMapSyllabusLookupSession
//
//    Implements a Syllabus lookup service backed by a collection of
//    syllabi indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Syllabus lookup service backed by a collection of
 *  syllabi. The syllabi are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some syllabi may be compatible
 *  with more types than are indicated through these syllabus
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of syllabi can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapSyllabusLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractIndexedMapSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapSyllabusLookupSession} with no syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

      public MutableIndexedMapSyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSyllabusLookupSession} with a
     *  single syllabus.
     *  
     *  @param courseCatalog the course catalog
     *  @param  syllabus a single syllabus
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code syllabus} is {@code null}
     */

    public MutableIndexedMapSyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Syllabus syllabus) {
        this(courseCatalog);
        putSyllabus(syllabus);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSyllabusLookupSession} using an
     *  array of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param  syllabi an array of syllabi
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code syllabi} is {@code null}
     */

    public MutableIndexedMapSyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Syllabus[] syllabi) {
        this(courseCatalog);
        putSyllabi(syllabi);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSyllabusLookupSession} using a
     *  collection of syllabi.
     *
     *  @param courseCatalog the course catalog
     *  @param  syllabi a collection of syllabi
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code syllabi} is {@code null}
     */

    public MutableIndexedMapSyllabusLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi) {

        this(courseCatalog);
        putSyllabi(syllabi);
        return;
    }
    

    /**
     *  Makes a {@code Syllabus} available in this session.
     *
     *  @param  syllabus a syllabus
     *  @throws org.osid.NullArgumentException {@code syllabus{@code  is
     *          {@code null}
     */

    @Override
    public void putSyllabus(org.osid.course.syllabus.Syllabus syllabus) {
        super.putSyllabus(syllabus);
        return;
    }


    /**
     *  Makes an array of syllabi available in this session.
     *
     *  @param  syllabi an array of syllabi
     *  @throws org.osid.NullArgumentException {@code syllabi{@code 
     *          is {@code null}
     */

    @Override
    public void putSyllabi(org.osid.course.syllabus.Syllabus[] syllabi) {
        super.putSyllabi(syllabi);
        return;
    }


    /**
     *  Makes collection of syllabi available in this session.
     *
     *  @param  syllabi a collection of syllabi
     *  @throws org.osid.NullArgumentException {@code syllabus{@code  is
     *          {@code null}
     */

    @Override
    public void putSyllabi(java.util.Collection<? extends org.osid.course.syllabus.Syllabus> syllabi) {
        super.putSyllabi(syllabi);
        return;
    }


    /**
     *  Removes a Syllabus from this session.
     *
     *  @param syllabusId the {@code Id} of the syllabus
     *  @throws org.osid.NullArgumentException {@code syllabusId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSyllabus(org.osid.id.Id syllabusId) {
        super.removeSyllabus(syllabusId);
        return;
    }    
}
