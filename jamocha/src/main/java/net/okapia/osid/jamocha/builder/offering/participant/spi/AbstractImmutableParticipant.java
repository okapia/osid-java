//
// AbstractImmutableParticipant.java
//
//     Wraps a mutable Participant to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.participant.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Participant</code> to hide modifiers. This
 *  wrapper provides an immutized Participant from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying participant whose state changes are visible.
 */

public abstract class AbstractImmutableParticipant
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.offering.Participant {

    private final org.osid.offering.Participant participant;


    /**
     *  Constructs a new <code>AbstractImmutableParticipant</code>.
     *
     *  @param participant the participant to immutablize
     *  @throws org.osid.NullArgumentException <code>participant</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableParticipant(org.osid.offering.Participant participant) {
        super(participant);
        this.participant = participant;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the offering to which this participant 
     *  is assigned. 
     *
     *  @return the <code> Offering </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOfferingId() {
        return (this.participant.getOfferingId());
    }


    /**
     *  Gets the offering for this participant. 
     *
     *  @return the <code> Offering </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.Offering getOffering()
        throws org.osid.OperationFailedException {

        return (this.participant.getOffering());
    }


    /**
     *  Gets the <code> Id </code> of the resource. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.participant.getResourceId());
    }


    /**
     *  Gets the resource for this participant. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.participant.getResource());
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the <code> TimePeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.participant.getTimePeriodId());
    }


    /**
     *  Gets the time period for this participant. 
     *
     *  @return the <code> TimePeriod </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.participant.getTimePeriod());
    }


    /**
     *  Tests if ththere are result options that constrain results.
     *
     *  @return <code> true </code> if there are results results,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.participant.hasResults());
    }


    /**
     *  Gets the various result option <code> Ids </code> applied to
     *  this participation.
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code>
     *          hasResults()</code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        return (this.participant.getResultOptionIds());
    }


    /**
     *  Gets the various result option <code> Ids </code> applied to
     *  this participation.
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException
     *          <code>hasResults()</code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        return (this.participant.getResultOptions());
    }


    /**
     *  Gets the record corresponding to the given <code> Participant </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> participantRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(participantRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  participantRecordType the type of participant record to 
     *          retrieve 
     *  @return the participant record 
     *  @throws org.osid.NullArgumentException <code> participantRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(participantRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.records.ParticipantRecord getParticipantRecord(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException {

        return (this.participant.getParticipantRecord(participantRecordType));
    }
}

