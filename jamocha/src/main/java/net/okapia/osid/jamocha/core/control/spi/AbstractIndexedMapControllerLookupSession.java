//
// AbstractIndexedMapControllerLookupSession.java
//
//    A simple framework for providing a Controller lookup service
//    backed by a fixed collection of controllers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Controller lookup service backed by a
 *  fixed collection of controllers. The controllers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some controllers may be compatible
 *  with more types than are indicated through these controller
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Controllers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapControllerLookupSession
    extends AbstractMapControllerLookupSession
    implements org.osid.control.ControllerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Controller> controllersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Controller>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Controller> controllersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Controller>());


    /**
     *  Makes a <code>Controller</code> available in this session.
     *
     *  @param  controller a controller
     *  @throws org.osid.NullArgumentException <code>controller<code> is
     *          <code>null</code>
     */

    @Override
    protected void putController(org.osid.control.Controller controller) {
        super.putController(controller);

        this.controllersByGenus.put(controller.getGenusType(), controller);
        
        try (org.osid.type.TypeList types = controller.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.controllersByRecord.put(types.getNextType(), controller);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of controllers available in this session.
     *
     *  @param  controllers an array of controllers
     *  @throws org.osid.NullArgumentException <code>controllers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putControllers(org.osid.control.Controller[] controllers) {
        for (org.osid.control.Controller controller : controllers) {
            putController(controller);
        }

        return;
    }


    /**
     *  Makes a collection of controllers available in this session.
     *
     *  @param  controllers a collection of controllers
     *  @throws org.osid.NullArgumentException <code>controllers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putControllers(java.util.Collection<? extends org.osid.control.Controller> controllers) {
        for (org.osid.control.Controller controller : controllers) {
            putController(controller);
        }

        return;
    }


    /**
     *  Removes a controller from this session.
     *
     *  @param controllerId the <code>Id</code> of the controller
     *  @throws org.osid.NullArgumentException <code>controllerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeController(org.osid.id.Id controllerId) {
        org.osid.control.Controller controller;
        try {
            controller = getController(controllerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.controllersByGenus.remove(controller.getGenusType());

        try (org.osid.type.TypeList types = controller.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.controllersByRecord.remove(types.getNextType(), controller);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeController(controllerId);
        return;
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  controller genus <code>Type</code> which does not include
     *  controllers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known controllers or an error results. Otherwise,
     *  the returned list may contain only those controllers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.controller.ArrayControllerList(this.controllersByGenus.get(controllerGenusType)));
    }


    /**
     *  Gets a <code>ControllerList</code> containing the given
     *  controller record <code>Type</code>. In plenary mode, the
     *  returned list contains all known controllers or an error
     *  results. Otherwise, the returned list may contain only those
     *  controllers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return the returned <code>controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByRecordType(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.controller.ArrayControllerList(this.controllersByRecord.get(controllerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.controllersByGenus.clear();
        this.controllersByRecord.clear();

        super.close();

        return;
    }
}
