//
// AbstractAdapterInquiryEnablerLookupSession.java
//
//    An InquiryEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An InquiryEnabler lookup session adapter.
 */

public abstract class AbstractAdapterInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {

    private final org.osid.inquiry.rules.InquiryEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterInquiryEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInquiryEnablerLookupSession(org.osid.inquiry.rules.InquiryEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Inquest/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.session.getInquestId());
    }


    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getInquest());
    }


    /**
     *  Tests if this user can perform {@code InquiryEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupInquiryEnablers() {
        return (this.session.canLookupInquiryEnablers());
    }


    /**
     *  A complete view of the {@code InquiryEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeInquiryEnablerView() {
        this.session.useComparativeInquiryEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code InquiryEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryInquiryEnablerView() {
        this.session.usePlenaryInquiryEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inquiry enablers in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.session.useFederatedInquestView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.session.useIsolatedInquestView();
        return;
    }
    

    /**
     *  Only active inquiry enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveInquiryEnablerView() {
        this.session.useActiveInquiryEnablerView();
        return;
    }


    /**
     *  Active and inactive inquiry enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusInquiryEnablerView() {
        this.session.useAnyStatusInquiryEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code InquiryEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code InquiryEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code InquiryEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param inquiryEnablerId {@code Id} of the {@code InquiryEnabler}
     *  @return the inquiry enabler
     *  @throws org.osid.NotFoundException {@code inquiryEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code inquiryEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnabler getInquiryEnabler(org.osid.id.Id inquiryEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnabler(inquiryEnablerId));
    }


    /**
     *  Gets an {@code InquiryEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inquiryEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code InquiryEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  inquiryEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code InquiryEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByIds(org.osid.id.IdList inquiryEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablersByIds(inquiryEnablerIds));
    }


    /**
     *  Gets an {@code InquiryEnablerList} corresponding to the given
     *  inquiry enabler genus {@code Type} which does not include
     *  inquiry enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  inquiryEnablerGenusType an inquiryEnabler genus type 
     *  @return the returned {@code InquiryEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByGenusType(org.osid.type.Type inquiryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablersByGenusType(inquiryEnablerGenusType));
    }


    /**
     *  Gets an {@code InquiryEnablerList} corresponding to the given
     *  inquiry enabler genus {@code Type} and include any additional
     *  inquiry enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  inquiryEnablerGenusType an inquiryEnabler genus type 
     *  @return the returned {@code InquiryEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByParentGenusType(org.osid.type.Type inquiryEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablersByParentGenusType(inquiryEnablerGenusType));
    }


    /**
     *  Gets an {@code InquiryEnablerList} containing the given
     *  inquiry enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  inquiryEnablerRecordType an inquiryEnabler record type 
     *  @return the returned {@code InquiryEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code inquiryEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersByRecordType(org.osid.type.Type inquiryEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablersByRecordType(inquiryEnablerRecordType));
    }


    /**
     *  Gets an {@code InquiryEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible
     *  through this session.
     *  
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code InquiryEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code InquiryEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible
     *  through this session.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code InquiryEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getInquiryEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code InquiryEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  inquiry enablers or an error results. Otherwise, the returned list
     *  may contain only those inquiry enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, inquiry enablers are returned that are currently
     *  active. In any status mode, active and inactive inquiry enablers
     *  are returned.
     *
     *  @return a list of {@code InquiryEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerList getInquiryEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getInquiryEnablers());
    }
}
