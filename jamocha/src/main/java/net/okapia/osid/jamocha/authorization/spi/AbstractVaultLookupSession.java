//
// AbstractVaultLookupSession.java
//
//    A starter implementation framework for providing a Vault
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Vault
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getVaults(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractVaultLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authorization.VaultLookupSession {

    private boolean pedantic = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();
    


    /**
     *  Tests if this user can perform <code>Vault</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupVaults() {
        return (true);
    }


    /**
     *  A complete view of the <code>Vault</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVaultView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Vault</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVaultView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Vault</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Vault</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Vault</code> and
     *  retained for compatibility.
     *
     *  @param  vaultId <code>Id</code> of the
     *          <code>Vault</code>
     *  @return the vault
     *  @throws org.osid.NotFoundException <code>vaultId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>vaultId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authorization.VaultList vaults = getVaults()) {
            while (vaults.hasNext()) {
                org.osid.authorization.Vault vault = vaults.getNextVault();
                if (vault.getId().equals(vaultId)) {
                    return (vault);
                }
            }
        } 

        throw new org.osid.NotFoundException(vaultId + " not found");
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  vaults specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Vaults</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getVaults()</code>.
     *
     *  @param  vaultIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>vaultIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByIds(org.osid.id.IdList vaultIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.authorization.Vault> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = vaultIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getVault(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("vault " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.authorization.vault.LinkedVaultList(ret));
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> which does not include
     *  vaults of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getVaults()</code>.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.vault.VaultGenusFilterList(getVaults(), vaultGenusType));
    }


    /**
     *  Gets a <code>VaultList</code> corresponding to the given
     *  vault genus <code>Type</code> and include any additional
     *  vaults with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getVaults()</code>.
     *
     *  @param  vaultGenusType a vault genus type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByParentGenusType(org.osid.type.Type vaultGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getVaultsByGenusType(vaultGenusType));
    }


    /**
     *  Gets a <code>VaultList</code> containing the given
     *  vault record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getVaults()</code>.
     *
     *  @param  vaultRecordType a vault record type 
     *  @return the returned <code>Vault</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>vaultRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByRecordType(org.osid.type.Type vaultRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authorization.vault.VaultRecordFilterList(getVaults(), vaultRecordType));
    }


    /**
     *  Gets a <code>VaultList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known vaults or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  vaults that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Vault</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authorization.VaultList getVaultsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.authorization.vault.VaultProviderFilterList(getVaults(), resourceId));
    }


    /**
     *  Gets all <code>Vaults</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  vaults or an error results. Otherwise, the returned list
     *  may contain only those vaults that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Vaults</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.authorization.VaultList getVaults()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the vault list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of vaults
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.authorization.VaultList filterVaultsOnViews(org.osid.authorization.VaultList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
