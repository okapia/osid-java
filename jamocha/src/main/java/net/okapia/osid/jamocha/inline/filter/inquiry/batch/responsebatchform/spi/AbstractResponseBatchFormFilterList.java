//
// AbstractResponseBatchFormList
//
//     Implements a filter for a ResponseBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.inquiry.batch.responsebatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ResponseBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedResponseBatchFormList
 *  to improve performance.
 */

public abstract class AbstractResponseBatchFormFilterList
    extends net.okapia.osid.jamocha.inquiry.batch.responsebatchform.spi.AbstractResponseBatchFormList
    implements org.osid.inquiry.batch.ResponseBatchFormList,
               net.okapia.osid.jamocha.inline.filter.inquiry.batch.responsebatchform.ResponseBatchFormFilter {

    private org.osid.inquiry.batch.ResponseBatchForm responseBatchForm;
    private final org.osid.inquiry.batch.ResponseBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractResponseBatchFormFilterList</code>.
     *
     *  @param responseBatchFormList a <code>ResponseBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>responseBatchFormList</code> is <code>null</code>
     */

    protected AbstractResponseBatchFormFilterList(org.osid.inquiry.batch.ResponseBatchFormList responseBatchFormList) {
        nullarg(responseBatchFormList, "response batch form list");
        this.list = responseBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.responseBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ResponseBatchForm </code> in this list. 
     *
     *  @return the next <code> ResponseBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ResponseBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inquiry.batch.ResponseBatchForm getNextResponseBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.inquiry.batch.ResponseBatchForm responseBatchForm = this.responseBatchForm;
            this.responseBatchForm = null;
            return (responseBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in response batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.responseBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ResponseBatchForms.
     *
     *  @param responseBatchForm the response batch form to filter
     *  @return <code>true</code> if the response batch form passes the filter,
     *          <code>false</code> if the response batch form should be filtered
     */

    public abstract boolean pass(org.osid.inquiry.batch.ResponseBatchForm responseBatchForm);


    protected void prime() {
        if (this.responseBatchForm != null) {
            return;
        }

        org.osid.inquiry.batch.ResponseBatchForm responseBatchForm = null;

        while (this.list.hasNext()) {
            try {
                responseBatchForm = this.list.getNextResponseBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(responseBatchForm)) {
                this.responseBatchForm = responseBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
