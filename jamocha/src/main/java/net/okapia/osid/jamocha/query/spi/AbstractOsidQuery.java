//
// AbstractOsidQuery.java
//
//     An OsidQuery with stored terms.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OsidQuery with stored terms.
 */

public abstract class AbstractOsidQuery
    implements org.osid.OsidQuery {

    private boolean any = false;
    private boolean none = false;
    private final Types stringMatchTypes = new TypeSet();
    private final java.util.Collection<org.osid.search.terms.StringTerm> keywordTerms = new java.util.LinkedHashSet<>();
    private final net.okapia.osid.jamocha.query.TermFactory factory;


    /**
     *  Constructs a new <code>AbstractOsidQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractOsidQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        nullarg(factory, "term factory");
        this.factory = factory;
        return;
    }


    /**
     *  Gets the term factory.
     *
     *  @return the term factory
     */

    protected net.okapia.osid.jamocha.query.TermFactory getTermFactory() {
        return (this.factory);
    }


    /**
     *  Gets the string matching types supported. A string match type
     *  specifies the syntax of the string query, such as matching a
     *  word or including a wildcard or regular expression.
     *
     *  @return a list containing the supported string match types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStringMatchTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stringMatchTypes.toCollection()));
    }


    /**
     *  Tests if the given string matching type is supported. 
     *
     *  @param stringMatchType a <code> Type </code> indicating a
     *         string match type
     *  @return <code> true </code> if the given Type is supported,
     *          <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        if (this.stringMatchTypes.contains(stringMatchType)) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Adds a string match type.
     *
     *  @param stringMatchType
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addStringMatchType(org.osid.type.Type stringMatchType) {
        nullarg(stringMatchType, "string match type");
        this.stringMatchTypes.add(stringMatchType);
        return;
    }


    /**
     *  Adds a keyword to match. Multiple keywords can be added to
     *  perform a boolean <code> OR </code> among them. A keyword may
     *  be applied to any of the elements defined in this object such
     *  as the display name, description or any method defined in an
     *  interface implemented by this object.
     *
     *  @param  keyword keyword to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> keyword </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchKeyword(String keyword, org.osid.type.Type stringMatchType, boolean match) {
        if (!supportsStringMatchType(stringMatchType)) {
            throw new org.osid.UnsupportedException(stringMatchType + " is not supported");
        }

        this.keywordTerms.add(this.factory.createStringTerm(keyword, stringMatchType, match));
        return;
    }


    /**
     *  Clears all keyword terms. 
     */

    @OSID @Override
    public void clearKeywordTerms() {
        this.keywordTerms.clear();
        return;
    }

    
    /**
     *  Gets all the keyword query terms.
     *
     *  @return a collection of the keyword query terms
     */

    protected java.util.Collection<org.osid.search.terms.StringTerm> getKeywordTerms() {
        return (java.util.Collections.unmodifiableCollection(this.keywordTerms));
    }


    /**
     *  Matches any object. 
     *
     *  @param  match <code> true </code> to match any object
     *          <code> false </code> to match no objects 
     */

    @OSID @Override
    public void matchAny(boolean match) {
        if (match) {
            this.any = true;
        } else {
            this.none = true;
        }

        return;
    }


    /**
     *  Clears the match any terms. 
     */

    @OSID @Override
    public void clearAnyTerms() {
        this.any = false;
        this.none = false;
        return;
    }


    /**
     *  Tests if this query matches nay object.
     *
     *  @return <code>true</code> if the any term has been set,
     *          <code>false</code> otherwise
     */

    protected boolean hasAnyTerm() {
        return (this.any);
    }


    /**
     *  Tests if this query matches no objects.
     *
     *  @return <code>true</code> if the none term has been set,
     *          <code>false</code> otherwise
     */

    protected boolean hasNoneTerm() {
        return (this.none);
    }


    /**
     *  Clears wildcard terms from a list of terms.
     *
     *  @param terms the list of terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is
     *          <code>null</code>
     */

    protected void clearWildcardTerms(java.util.Collection<? extends org.osid.search.QueryTerm> terms) {
        for (org.osid.search.QueryTerm term : terms) {
            if (term.isWildcard()) {
                terms.remove(term);
            }
        }

        return;
    }
}
