//
// AbstractMapFloorLookupSession
//
//    A simple framework for providing a Floor lookup service
//    backed by a fixed collection of floors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Floor lookup service backed by a
 *  fixed collection of floors. The floors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Floors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapFloorLookupSession
    extends net.okapia.osid.jamocha.room.spi.AbstractFloorLookupSession
    implements org.osid.room.FloorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.Floor> floors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.Floor>());


    /**
     *  Makes a <code>Floor</code> available in this session.
     *
     *  @param  floor a floor
     *  @throws org.osid.NullArgumentException <code>floor<code>
     *          is <code>null</code>
     */

    protected void putFloor(org.osid.room.Floor floor) {
        this.floors.put(floor.getId(), floor);
        return;
    }


    /**
     *  Makes an array of floors available in this session.
     *
     *  @param  floors an array of floors
     *  @throws org.osid.NullArgumentException <code>floors<code>
     *          is <code>null</code>
     */

    protected void putFloors(org.osid.room.Floor[] floors) {
        putFloors(java.util.Arrays.asList(floors));
        return;
    }


    /**
     *  Makes a collection of floors available in this session.
     *
     *  @param  floors a collection of floors
     *  @throws org.osid.NullArgumentException <code>floors<code>
     *          is <code>null</code>
     */

    protected void putFloors(java.util.Collection<? extends org.osid.room.Floor> floors) {
        for (org.osid.room.Floor floor : floors) {
            this.floors.put(floor.getId(), floor);
        }

        return;
    }


    /**
     *  Removes a Floor from this session.
     *
     *  @param  floorId the <code>Id</code> of the floor
     *  @throws org.osid.NullArgumentException <code>floorId<code> is
     *          <code>null</code>
     */

    protected void removeFloor(org.osid.id.Id floorId) {
        this.floors.remove(floorId);
        return;
    }


    /**
     *  Gets the <code>Floor</code> specified by its <code>Id</code>.
     *
     *  @param  floorId <code>Id</code> of the <code>Floor</code>
     *  @return the floor
     *  @throws org.osid.NotFoundException <code>floorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>floorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor(org.osid.id.Id floorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.Floor floor = this.floors.get(floorId);
        if (floor == null) {
            throw new org.osid.NotFoundException("floor not found: " + floorId);
        }

        return (floor);
    }


    /**
     *  Gets all <code>Floors</code>. In plenary mode, the returned
     *  list contains all known floors or an error
     *  results. Otherwise, the returned list may contain only those
     *  floors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Floors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.FloorList getFloors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.floor.ArrayFloorList(this.floors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.floors.clear();
        super.close();
        return;
    }
}
