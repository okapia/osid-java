//
// AbstractGradebookColumn.java
//
//     Defines a GradebookColumn builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumn.spi;


/**
 *  Defines a <code>GradebookColumn</code> builder.
 */

public abstract class AbstractGradebookColumnBuilder<T extends AbstractGradebookColumnBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.gradebookcolumn.GradebookColumnMiter gradebookColumn;


    /**
     *  Constructs a new <code>AbstractGradebookColumnBuilder</code>.
     *
     *  @param gradebookColumn the gradebook column to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradebookColumnBuilder(net.okapia.osid.jamocha.builder.grading.gradebookcolumn.GradebookColumnMiter gradebookColumn) {
        super(gradebookColumn);
        this.gradebookColumn = gradebookColumn;
        return;
    }


    /**
     *  Builds the gradebook column.
     *
     *  @return the new gradebook column
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.GradebookColumn build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.gradebookcolumn.GradebookColumnValidator(getValidations())).validate(this.gradebookColumn);
        return (new net.okapia.osid.jamocha.builder.grading.gradebookcolumn.ImmutableGradebookColumn(this.gradebookColumn));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the gradebook column miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.gradebookcolumn.GradebookColumnMiter getMiter() {
        return (this.gradebookColumn);
    }


    /**
     *  Sets the grade system.
     *
     *  @param system a grade system
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public T gradeSystem(org.osid.grading.GradeSystem system) {
        getMiter().setGradeSystem(system);
        return (self());
    }


    /**
     *  Adds a GradebookColumn record.
     *
     *  @param record a gradebook column record
     *  @param recordType the type of gradebook column record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.records.GradebookColumnRecord record, org.osid.type.Type recordType) {
        getMiter().addGradebookColumnRecord(record, recordType);
        return (self());
    }
}       


