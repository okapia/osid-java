//
// AbstractBranchQuery.java
//
//     A template for making a Branch Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for branches.
 */

public abstract class AbstractBranchQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.journaling.BranchQuery {

    private final java.util.Collection<org.osid.journaling.records.BranchQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the origin journal entry <code> Id </code> for this query. 
     *
     *  @param  journalEntryId a journal entry Id <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOriginJournalEntryId(org.osid.id.Id journalEntryId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the origin journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOriginJournalEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an origin journal entry. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the origin journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getOriginJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsOriginJournalEntryQuery() is false");
    }


    /**
     *  Clears the origin journal entry terms. 
     */

    @OSID @Override
    public void clearOriginJournalEntryTerms() {
        return;
    }


    /**
     *  Sets the latest journal entry <code> Id </code> for this query. 
     *
     *  @param  journalEntryId a journal entry Id <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchLatestJournalEntryId(org.osid.id.Id journalEntryId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the latest journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLatestJournalEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> JournalEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLatestJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a latest journal entry. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the latest journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getLatestJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLatestJournalEntryQuery() is false");
    }


    /**
     *  Clears the latest journal entry terms. 
     */

    @OSID @Override
    public void clearLatestJournalEntryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given branch query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a branch implementing the requested record.
     *
     *  @param branchRecordType a branch record type
     *  @return the branch query record
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchQueryRecord getBranchQueryRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchQueryRecord record : this.records) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this branch query. 
     *
     *  @param branchQueryRecord branch query record
     *  @param branchRecordType branch record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBranchQueryRecord(org.osid.journaling.records.BranchQueryRecord branchQueryRecord, 
                                          org.osid.type.Type branchRecordType) {

        addRecordType(branchRecordType);
        nullarg(branchQueryRecord, "branch query record");
        this.records.add(branchQueryRecord);        
        return;
    }
}
