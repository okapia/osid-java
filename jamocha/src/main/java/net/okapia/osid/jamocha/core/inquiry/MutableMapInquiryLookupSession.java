//
// MutableMapInquiryLookupSession
//
//    Implements an Inquiry lookup service backed by a collection of
//    inquiries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Inquiry lookup service backed by a collection of
 *  inquiries. The inquiries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of inquiries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapInquiryLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapInquiryLookupSession
    implements org.osid.inquiry.InquiryLookupSession {


    /**
     *  Constructs a new {@code MutableMapInquiryLookupSession}
     *  with no inquiries.
     *
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumentException {@code inquest} is
     *          {@code null}
     */

      public MutableMapInquiryLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquiryLookupSession} with a
     *  single inquiry.
     *
     *  @param inquest the inquest  
     *  @param inquiry an inquiry
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiry} is {@code null}
     */

    public MutableMapInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                           org.osid.inquiry.Inquiry inquiry) {
        this(inquest);
        putInquiry(inquiry);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquiryLookupSession}
     *  using an array of inquiries.
     *
     *  @param inquest the inquest
     *  @param inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiries} is {@code null}
     */

    public MutableMapInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                           org.osid.inquiry.Inquiry[] inquiries) {
        this(inquest);
        putInquiries(inquiries);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInquiryLookupSession}
     *  using a collection of inquiries.
     *
     *  @param inquest the inquest
     *  @param inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code inquiries} is {@code null}
     */

    public MutableMapInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                           java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {

        this(inquest);
        putInquiries(inquiries);
        return;
    }

    
    /**
     *  Makes an {@code Inquiry} available in this session.
     *
     *  @param inquiry an inquiry
     *  @throws org.osid.NullArgumentException {@code inquiry{@code  is
     *          {@code null}
     */

    @Override
    public void putInquiry(org.osid.inquiry.Inquiry inquiry) {
        super.putInquiry(inquiry);
        return;
    }


    /**
     *  Makes an array of inquiries available in this session.
     *
     *  @param inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException {@code inquiries{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiries(org.osid.inquiry.Inquiry[] inquiries) {
        super.putInquiries(inquiries);
        return;
    }


    /**
     *  Makes collection of inquiries available in this session.
     *
     *  @param inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException {@code inquiries{@code  is
     *          {@code null}
     */

    @Override
    public void putInquiries(java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {
        super.putInquiries(inquiries);
        return;
    }


    /**
     *  Removes an Inquiry from this session.
     *
     *  @param inquiryId the {@code Id} of the inquiry
     *  @throws org.osid.NullArgumentException {@code inquiryId{@code 
     *          is {@code null}
     */

    @Override
    public void removeInquiry(org.osid.id.Id inquiryId) {
        super.removeInquiry(inquiryId);
        return;
    }    
}
