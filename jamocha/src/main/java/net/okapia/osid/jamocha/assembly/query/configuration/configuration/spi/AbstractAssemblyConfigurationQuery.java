//
// AbstractAssemblyConfigurationQuery.java
//
//     A ConfigurationQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ConfigurationQuery that stores terms.
 */

public abstract class AbstractAssemblyConfigurationQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.configuration.ConfigurationQuery,
               org.osid.configuration.ConfigurationQueryInspector,
               org.osid.configuration.ConfigurationSearchOrder {

    private final java.util.Collection<org.osid.configuration.records.ConfigurationQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ConfigurationQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ConfigurationSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyConfigurationQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyConfigurationQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches configurations which are parameter registries. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public void matchRegistry(boolean match) {
        getAssembler().addBooleanTerm(getRegistryColumn(), match);
        return;
    }


    /**
     *  Clears the registry terms. 
     */

    @OSID @Override
    public void clearRegistryTerms() {
        getAssembler().clearTerms(getRegistryColumn());
        return;
    }


    /**
     *  Gets the registry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRegistryTerms() {
        return (getAssembler().getBooleanTerms(getRegistryColumn()));
    }


    /**
     *  Gets the Registry column name.
     *
     * @return the column name
     */

    protected String getRegistryColumn() {
        return ("registry");
    }


    /**
     *  Adds a parameter <code> Id </code> for this query. 
     *
     *  @param  parameterId a parameter <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> parameterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchParameterId(org.osid.id.Id parameterId, boolean match) {
        getAssembler().addIdTerm(getParameterIdColumn(), parameterId, match);
        return;
    }


    /**
     *  Clears the parameter <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParameterIdTerms() {
        getAssembler().clearTerms(getParameterIdColumn());
        return;
    }


    /**
     *  Gets the parameter <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParameterIdTerms() {
        return (getAssembler().getIdTerms(getParameterIdColumn()));
    }


    /**
     *  Gets the ParameterId column name.
     *
     * @return the column name
     */

    protected String getParameterIdColumn() {
        return ("parameter_id");
    }


    /**
     *  Tests if a <code> ParameterQuery </code> is available. 
     *
     *  @return <code> true </code> if a parameter query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a parameter. 
     *
     *  @return the parameter query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuery getParameterQuery() {
        throw new org.osid.UnimplementedException("supportsParameterQuery() is false");
    }


    /**
     *  Matches configurations that have any parameter. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          parameter, <code> false </code> to match configurations with 
     *          no parameter 
     */

    @OSID @Override
    public void matchAnyParameter(boolean match) {
        getAssembler().addIdWildcardTerm(getParameterColumn(), match);
        return;
    }


    /**
     *  Clears the parameter terms. 
     */

    @OSID @Override
    public void clearParameterTerms() {
        getAssembler().clearTerms(getParameterColumn());
        return;
    }


    /**
     *  Gets the parameter query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQueryInspector[] getParameterTerms() {
        return (new org.osid.configuration.ParameterQueryInspector[0]);
    }


    /**
     *  Gets the Parameter column name.
     *
     * @return the column name
     */

    protected String getParameterColumn() {
        return ("parameter");
    }


    /**
     *  Adds a configuration <code> Id </code> for this query to match 
     *  configurations which have as an ancestor the specified configuration. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorConfigurationId(org.osid.id.Id configurationId, 
                                             boolean match) {
        getAssembler().addIdTerm(getAncestorConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the ancestor configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorConfigurationIdTerms() {
        getAssembler().clearTerms(getAncestorConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the ancestor configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getAncestorConfigurationIdColumn()));
    }


    /**
     *  Gets the AncestorConfigurationId column name.
     *
     * @return the column name
     */

    protected String getAncestorConfigurationIdColumn() {
        return ("ancestor_configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorConfigurationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getAncestorConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorConfigurationQuery() is false");
    }


    /**
     *  Matches configurations that have any ancestor. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          ancestor, <code> false </code> to match root configurations 
     */

    @OSID @Override
    public void matchAnyAncestorConfiguration(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorConfigurationColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor configuration terms. 
     */

    @OSID @Override
    public void clearAncestorConfigurationTerms() {
        getAssembler().clearTerms(getAncestorConfigurationColumn());
        return;
    }


    /**
     *  Gets the ancestor configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getAncestorConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the AncestorConfiguration column name.
     *
     * @return the column name
     */

    protected String getAncestorConfigurationColumn() {
        return ("ancestor_configuration");
    }


    /**
     *  Adds a configuration <code> Id </code> for this query to match 
     *  configurations which have as a descendant the specified configuration. 
     *
     *  @param  configurationId a configuration <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantConfigurationId(org.osid.id.Id configurationId, 
                                               boolean match) {
        getAssembler().addIdTerm(getDescendantConfigurationIdColumn(), configurationId, match);
        return;
    }


    /**
     *  Clears the descendant configuration <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantConfigurationIdTerms() {
        getAssembler().clearTerms(getDescendantConfigurationIdColumn());
        return;
    }


    /**
     *  Gets the descendant configuration <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantConfigurationIdTerms() {
        return (getAssembler().getIdTerms(getDescendantConfigurationIdColumn()));
    }


    /**
     *  Gets the DescendantConfigurationId column name.
     *
     * @return the column name
     */

    protected String getDescendantConfigurationIdColumn() {
        return ("descendant_configuration_id");
    }


    /**
     *  Tests if a <code> ConfigurationQuery </code> is available. 
     *
     *  @return <code> true </code> if a configuration query interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantConfigurationQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a configuration. 
     *
     *  @return the configuration query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantConfigurationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuery getDescendantConfigurationQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantConfigurationQuery() is false");
    }


    /**
     *  Matches configurations that have any descendant. 
     *
     *  @param  match <code> true </code> to match configurations with any 
     *          descendant, <code> false </code> to match leaf configurations 
     */

    @OSID @Override
    public void matchAnyDescendantConfiguration(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantConfigurationColumn(), match);
        return;
    }


    /**
     *  Clears the descendant configuration terms. 
     */

    @OSID @Override
    public void clearDescendantConfigurationTerms() {
        getAssembler().clearTerms(getDescendantConfigurationColumn());
        return;
    }


    /**
     *  Gets the descendant configuration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQueryInspector[] getDescendantConfigurationTerms() {
        return (new org.osid.configuration.ConfigurationQueryInspector[0]);
    }


    /**
     *  Gets the DescendantConfiguration column name.
     *
     * @return the column name
     */

    protected String getDescendantConfigurationColumn() {
        return ("descendant_configuration");
    }


    /**
     *  Tests if this configuration supports the given record
     *  <code>Type</code>.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return <code>true</code> if the configurationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type configurationRecordType) {
        for (org.osid.configuration.records.ConfigurationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  configurationRecordType the configuration record type 
     *  @return the configuration query record 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationQueryRecord getConfigurationQueryRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  configurationRecordType the configuration record type 
     *  @return the configuration query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationQueryInspectorRecord getConfigurationQueryInspectorRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param configurationRecordType the configuration record type
     *  @return the configuration search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>configurationRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(configurationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationSearchOrderRecord getConfigurationSearchOrderRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ConfigurationSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(configurationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(configurationRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this configuration. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param configurationQueryRecord the configuration query record
     *  @param configurationQueryInspectorRecord the configuration query inspector
     *         record
     *  @param configurationSearchOrderRecord the configuration search order record
     *  @param configurationRecordType configuration record type
     *  @throws org.osid.NullArgumentException
     *          <code>configurationQueryRecord</code>,
     *          <code>configurationQueryInspectorRecord</code>,
     *          <code>configurationSearchOrderRecord</code> or
     *          <code>configurationRecordTypeconfiguration</code> is
     *          <code>null</code>
     */
            
    protected void addConfigurationRecords(org.osid.configuration.records.ConfigurationQueryRecord configurationQueryRecord, 
                                      org.osid.configuration.records.ConfigurationQueryInspectorRecord configurationQueryInspectorRecord, 
                                      org.osid.configuration.records.ConfigurationSearchOrderRecord configurationSearchOrderRecord, 
                                      org.osid.type.Type configurationRecordType) {

        addRecordType(configurationRecordType);

        nullarg(configurationQueryRecord, "configuration query record");
        nullarg(configurationQueryInspectorRecord, "configuration query inspector record");
        nullarg(configurationSearchOrderRecord, "configuration search odrer record");

        this.queryRecords.add(configurationQueryRecord);
        this.queryInspectorRecords.add(configurationQueryInspectorRecord);
        this.searchOrderRecords.add(configurationSearchOrderRecord);
        
        return;
    }
}
