//
// AbstractCreditLookupSession.java
//
//    A starter implementation framework for providing a Credit
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Credit
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCredits(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCreditLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.acknowledgement.CreditLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.acknowledgement.Billing billing = new net.okapia.osid.jamocha.nil.acknowledgement.billing.UnknownBilling();
    

    /**
     *  Gets the <code>Billing/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Billing Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBillingId() {
        return (this.billing.getId());
    }


    /**
     *  Gets the <code>Billing</code> associated with this 
     *  session.
     *
     *  @return the <code>Billing</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.billing);
    }


    /**
     *  Sets the <code>Billing</code>.
     *
     *  @param  billing the billing for this session
     *  @throws org.osid.NullArgumentException <code>billing</code>
     *          is <code>null</code>
     */

    protected void setBilling(org.osid.acknowledgement.Billing billing) {
        nullarg(billing, "billing");
        this.billing = billing;
        return;
    }

    /**
     *  Tests if this user can perform <code>Credit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCredits() {
        return (true);
    }


    /**
     *  A complete view of the <code>Credit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCreditView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Credit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCreditView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credits in billings which are children of
     *  this billing in the billing hierarchy.
     */

    @OSID @Override
    public void useFederatedBillingView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this billing only.
     */

    @OSID @Override
    public void useIsolatedBillingView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only credits whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCreditView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All creditss of any effective dates are returned by all methods in
     *  this session.
     */

    @OSID @Override
    public void useAnyEffectiveCreditView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Credit</code> specified by its
     *  <code>Id</code>.
     *
     *  <code> </code> In plenary mode, the exact <code> Id </code> is found
     *  or a <code> NOT_FOUND </code> results. Otherwise, the returned <code>
     *  Credit </code> may have a different <code> Id </code> than requested,
     *  such as the case where a duplicate <code> Id </code> was assigned to a
     *  <code>Credit</code> and retained for compatibility.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective creditss and those currently expired
     *  are returned.
     *
     *  @param  creditId <code>Id</code> of the
     *          <code>Credit</code>
     *  @return the credit
     *  @throws org.osid.NotFoundException <code>creditId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>creditId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Credit getCredit(org.osid.id.Id creditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.acknowledgement.CreditList credits = getCredits()) {
            while (credits.hasNext()) {
                org.osid.acknowledgement.Credit credit = credits.getNextCredit();
                if (credit.getId().equals(creditId)) {
                    return (credit);
                }
            }
        } 

        throw new org.osid.NotFoundException(creditId + " not found");
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Credits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective credits and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCredits()</code>.
     *
     *  @param  creditIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found 
     *  @throws org.osid.NullArgumentException
     *          <code>creditIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByIds(org.osid.id.IdList creditIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.acknowledgement.Credit> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = creditIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCredit(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("credit " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.acknowledgement.credit.LinkedCreditList(ret));
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> which does not include
     *  credits of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective credits and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCredits()</code>.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditGenusFilterList(getCredits(), creditGenusType));
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> and include any additional
     *  credits with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective credits and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredits()</code>.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByParentGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCreditsByGenusType(creditGenusType));
    }


    /**
     *  Gets a <code>CreditList</code> containing the given
     *  credit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently effective.
     *  In any effective mode, effective credits and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredits()</code>.
     *
     *  @param  creditRecordType a credit record type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByRecordType(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditRecordFilterList(getCredits(), creditRecordType));
    }


    /**
     *  Gets a <code> CreditList </code> effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits or an
     *  error results. Otherwise, the returned list may contain only those
     *  credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently effective
     *  in addition to being effective in the given date range. In any
     *  effective mode, effective credits and those currently expired are
     *  returned.
     *
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from or to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    public org.osid.acknowledgement.CreditList getCreditsOnDate(org.osid.calendaring.DateTime from,
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCredits(), from, to));
    }


    /**
     *  Gets a list of credits of the given genus type and effective entire 
     *  given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known credits or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  credits that are accessible through this session. 
     *  
     *  In effective mode, credits are returned that are currently effective 
     *  in addition to being effective in the given date range. In any 
     *  effective mode, effective credits and those currently expired are 
     *  returned. 
     *
     *  @param  creditGenusType a credit genus <code> Type </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> CreditList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> creditGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeOnDate(org.osid.type.Type creditGenusType, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsByGenusType(creditGenusType), from, to));
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.acknowledgement.CreditList getCreditsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ResourceFilter(resourceId), getCredits()));
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resource</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceOnDate(org.osid.id.Id resourceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of credits by a genus type for a resource.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException <code> creditGenusType </code>
     *          or <code> resourceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ResourceFilter(resourceId), getCreditsByGenusType(creditGenusType)));
    }


    /**
     *  Gets a list of credits by genus type for a resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.type.Type creditGenusType,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsByGenusTypeForResource(resourceId, creditGenusType), from, to));
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>reference</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.acknowledgement.CreditList getCreditsForReference(org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

         return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ReferenceFilter(referenceId), getCredits()));
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>reference</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsForReference(referenceId), from, to));
    }


    /**
     *  Gets a list of credits by a genus type for a reference.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> or
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReference(org.osid.id.Id referenceId,
                                                                                 org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ReferenceFilter(referenceId), getCreditsByGenusType(creditGenusType)));
    }


    /**
     *  Gets a list of credits of the given genus type for a reference
     *  and effective entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> referenceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId,
                                                                                       org.osid.type.Type creditGenusType,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsByGenusTypeForReference(referenceId, creditGenusType), from, to));
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  referenceId the <code> Id </code> of the reference
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> or
     *          <code>referenceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReference(org.osid.id.Id resourceId,
                                                                                 org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ReferenceFilter(resourceId), getCreditsForResource(resourceId)));
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code> Id </code> of the resource
     *  @param  referenceId the <code> Id </code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code>
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id referenceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsForResourceAndReference(resourceId, referenceId), from, to));
    }


    /**
     *  Gets a <code>CreditList</code> of the given genus type for
     *  the given resource and reference.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given resource and reference, including
     *  duplicates, or an error results if a credit is inaccessible.
     *  Otherwise, inaccessible <code> Credits </code> may be omitted
     *  from the list.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code> or
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReference(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id referenceId,
                                                                                            org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilterList(new ReferenceFilter(resourceId), getCreditsByGenusTypeForResource(resourceId, creditGenusType)));
    }


    /**
     *  Gets a <code>CreditList</code> of the given genus type
     *  corresponding to the given resource and reference and
     *  effective entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given peer, including duplicates, or an
     *  error results if a credit is inaccessible. Otherwise,
     *  inaccessible <code> Credits </code> may be omitted from the
     *  list.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code>,
     *          <code>creditGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id referenceId,
                                                                                                  org.osid.type.Type creditGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.TemporalCreditFilterList(getCreditsByGenusTypeForResourceAndReference(resourceId, referenceId, creditGenusType), from, to));
    }


    /**
     *  Gets all <code>Credits</code>. 
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective. In any effective mode, effective creditss and those
     *  currently expired are returned.
     *
     *  @return a list of <code>Credits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.acknowledgement.CreditList getCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the credit list for various views.  Should be called
     *  by <code>getObjects()</code> if no filtering is already
     *  performed.
     *
     *  @param list the list of credits
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.acknowledgement.CreditList filterCreditsOnViews(org.osid.acknowledgement.CreditList list)
        throws org.osid.OperationFailedException {
         
        org.osid.acknowledgement.CreditList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.EffectiveCreditFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the CreditFilterList to filter the credit list
         *  based on resource.
         *
         *  @param credit the credit
         *  @return <code>true</code> to pass the credit,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.acknowledgement.Credit credit) {
            return (credit.getResourceId().equals(this.resourceId));
        }
    }


    public static class ReferenceFilter
        implements net.okapia.osid.jamocha.inline.filter.acknowledgement.credit.CreditFilter {

        private final org.osid.id.Id referenceId;

        
        /**
         *  Constructs a new <code>ReferenceFilterList</code>.
         *
         *  @param referenceId the reference to filter
         *  @throws org.osid.NullArgumentException
         *          <code>referenceId</code> is <code>null</code>
         */

        public ReferenceFilter(org.osid.id.Id referenceId) {
            nullarg(referenceId, "reference Id");
            this.referenceId = referenceId;
            return;
        }


        /**
         *  Used by the CreditFilterList to filter the credit list
         *  based on reference.
         *
         *  @param credit the credit
         *  @return <code>true</code> to pass the credit,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.acknowledgement.Credit credit) {
            return (credit.getReferenceId().equals(this.referenceId));
        }
    }
}
