//
// AbstractNodeInquestHierarchySession.java
//
//     Defines an Inquest hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an inquest hierarchy session for delivering a hierarchy
 *  of inquests using the InquestNode interface.
 */

public abstract class AbstractNodeInquestHierarchySession
    extends net.okapia.osid.jamocha.inquiry.spi.AbstractInquestHierarchySession
    implements org.osid.inquiry.InquestHierarchySession {

    private java.util.Collection<org.osid.inquiry.InquestNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root inquest <code> Ids </code> in this hierarchy.
     *
     *  @return the root inquest <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootInquestIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquestnode.InquestNodeToIdList(this.roots));
    }


    /**
     *  Gets the root inquests in the inquest hierarchy. A node
     *  with no parents is an orphan. While all inquest <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root inquests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getRootInquests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquestnode.InquestNodeToInquestList(new net.okapia.osid.jamocha.inquiry.inquestnode.ArrayInquestNodeList(this.roots)));
    }


    /**
     *  Adds a root inquest node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootInquest(org.osid.inquiry.InquestNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root inquest nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootInquests(java.util.Collection<org.osid.inquiry.InquestNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root inquest node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootInquest(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.inquiry.InquestNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Inquest </code> has any parents. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @return <code> true </code> if the inquest has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentInquests(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getInquestNode(inquestId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  inquest.
     *
     *  @param  id an <code> Id </code> 
     *  @param  inquestId the <code> Id </code> of an inquest 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> inquestId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> inquestId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfInquest(org.osid.id.Id id, org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inquiry.InquestNodeList parents = getInquestNode(inquestId).getParentInquestNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextInquestNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given inquest. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @return the parent <code> Ids </code> of the inquest 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentInquestIds(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquest.InquestToIdList(getParentInquests(inquestId)));
    }


    /**
     *  Gets the parents of the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> to query 
     *  @return the parents of the inquest 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getParentInquests(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquestnode.InquestNodeToInquestList(getInquestNode(inquestId).getParentInquestNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  inquest.
     *
     *  @param  id an <code> Id </code> 
     *  @param  inquestId the Id of an inquest 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> inquestId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfInquest(org.osid.id.Id id, org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfInquest(id, inquestId)) {
            return (true);
        }

        try (org.osid.inquiry.InquestList parents = getParentInquests(inquestId)) {
            while (parents.hasNext()) {
                if (isAncestorOfInquest(id, parents.getNextInquest().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an inquest has any children. 
     *
     *  @param  inquestId an inquest <code> Id </code> 
     *  @return <code> true </code> if the <code> inquestId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildInquests(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInquestNode(inquestId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  inquest.
     *
     *  @param  id an <code> Id </code> 
     *  @param inquestId the <code> Id </code> of an 
     *         inquest
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> inquestId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> inquestId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfInquest(org.osid.id.Id id, org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfInquest(inquestId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  inquest.
     *
     *  @param  inquestId the <code> Id </code> to query 
     *  @return the children of the inquest 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildInquestIds(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquest.InquestToIdList(getChildInquests(inquestId)));
    }


    /**
     *  Gets the children of the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> to query 
     *  @return the children of the inquest 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestList getChildInquests(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquestnode.InquestNodeToInquestList(getInquestNode(inquestId).getChildInquestNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  inquest.
     *
     *  @param  id an <code> Id </code> 
     *  @param inquestId the <code> Id </code> of an 
     *         inquest
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> inquestId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfInquest(org.osid.id.Id id, org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfInquest(inquestId, id)) {
            return (true);
        }

        try (org.osid.inquiry.InquestList children = getChildInquests(inquestId)) {
            while (children.hasNext()) {
                if (isDescendantOfInquest(id, children.getNextInquest().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  inquest.
     *
     *  @param  inquestId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified inquest node 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getInquestNodeIds(org.osid.id.Id inquestId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.inquiry.inquestnode.InquestNodeToNode(getInquestNode(inquestId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given inquest.
     *
     *  @param  inquestId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified inquest node 
     *  @throws org.osid.NotFoundException <code> inquestId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> inquestId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquestNode getInquestNodes(org.osid.id.Id inquestId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getInquestNode(inquestId));
    }


    /**
     *  Closes this <code>InquestHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an inquest node.
     *
     *  @param inquestId the id of the inquest node
     *  @throws org.osid.NotFoundException <code>inquestId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>inquestId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.inquiry.InquestNode getInquestNode(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(inquestId, "inquest Id");
        for (org.osid.inquiry.InquestNode inquest : this.roots) {
            if (inquest.getId().equals(inquestId)) {
                return (inquest);
            }

            org.osid.inquiry.InquestNode r = findInquest(inquest, inquestId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(inquestId + " is not found");
    }


    protected org.osid.inquiry.InquestNode findInquest(org.osid.inquiry.InquestNode node, 
                                                       org.osid.id.Id inquestId) 
        throws org.osid.OperationFailedException {

        try (org.osid.inquiry.InquestNodeList children = node.getChildInquestNodes()) {
            while (children.hasNext()) {
                org.osid.inquiry.InquestNode inquest = children.getNextInquestNode();
                if (inquest.getId().equals(inquestId)) {
                    return (inquest);
                }
                
                inquest = findInquest(inquest, inquestId);
                if (inquest != null) {
                    return (inquest);
                }
            }
        }

        return (null);
    }
}
