//
// AbstractAdapterChainLookupSession.java
//
//    A Chain lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.sequencing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Chain lookup session adapter.
 */

public abstract class AbstractAdapterChainLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.sequencing.ChainLookupSession {

    private final org.osid.sequencing.ChainLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterChainLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterChainLookupSession(org.osid.sequencing.ChainLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Antimatroid/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Antimatroid Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAntimatroidId() {
        return (this.session.getAntimatroidId());
    }


    /**
     *  Gets the {@code Antimatroid} associated with this session.
     *
     *  @return the {@code Antimatroid} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Antimatroid getAntimatroid()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAntimatroid());
    }


    /**
     *  Tests if this user can perform {@code Chain} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupChains() {
        return (this.session.canLookupChains());
    }


    /**
     *  A complete view of the {@code Chain} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeChainView() {
        this.session.useComparativeChainView();
        return;
    }


    /**
     *  A complete view of the {@code Chain} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryChainView() {
        this.session.usePlenaryChainView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include chains in antimatroids which are children
     *  of this antimatroid in the antimatroid hierarchy.
     */

    @OSID @Override
    public void useFederatedAntimatroidView() {
        this.session.useFederatedAntimatroidView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this antimatroid only.
     */

    @OSID @Override
    public void useIsolatedAntimatroidView() {
        this.session.useIsolatedAntimatroidView();
        return;
    }
    
     
    /**
     *  Gets the {@code Chain} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Chain} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Chain} and
     *  retained for compatibility.
     *
     *  @param chainId {@code Id} of the {@code Chain}
     *  @return the chain
     *  @throws org.osid.NotFoundException {@code chainId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code chainId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.Chain getChain(org.osid.id.Id chainId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChain(chainId));
    }


    /**
     *  Gets a {@code ChainList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  chains specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Chains} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  chainIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Chain} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code chainIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByIds(org.osid.id.IdList chainIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChainsByIds(chainIds));
    }


    /**
     *  Gets a {@code ChainList} corresponding to the given
     *  chain genus {@code Type} which does not include
     *  chains of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  chainGenusType a chain genus type 
     *  @return the returned {@code Chain} list
     *  @throws org.osid.NullArgumentException
     *          {@code chainGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByGenusType(org.osid.type.Type chainGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChainsByGenusType(chainGenusType));
    }


    /**
     *  Gets a {@code ChainList} corresponding to the given
     *  chain genus {@code Type} and include any additional
     *  chains with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  chainGenusType a chain genus type 
     *  @return the returned {@code Chain} list
     *  @throws org.osid.NullArgumentException
     *          {@code chainGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByParentGenusType(org.osid.type.Type chainGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChainsByParentGenusType(chainGenusType));
    }


    /**
     *  Gets a {@code ChainList} containing the given
     *  chain record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  chainRecordType a chain record type 
     *  @return the returned {@code Chain} list
     *  @throws org.osid.NullArgumentException
     *          {@code chainRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChainsByRecordType(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChainsByRecordType(chainRecordType));
    }


    /**
     *  Gets all {@code Chains}. 
     *
     *  In plenary mode, the returned list contains all known
     *  chains or an error results. Otherwise, the returned list
     *  may contain only those chains that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Chains} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.sequencing.ChainList getChains()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getChains());
    }
}
