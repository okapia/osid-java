//
// AbstractCatalogueQueryInspector.java
//
//     A template for making a CatalogueQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.catalogue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for catalogues.
 */

public abstract class AbstractCatalogueQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.offering.CatalogueQueryInspector {

    private final java.util.Collection<org.osid.offering.records.CatalogueQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the canonical unit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCanonicalUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the canonical unit terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the offering <code> Id </code> query terms. 
     *
     *  @return the offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the offering query terms. 
     *
     *  @return the offering terms 
     */

    @OSID @Override
    public org.osid.offering.OfferingQueryInspector[] getOfferingTerms() {
        return (new org.osid.offering.OfferingQueryInspector[0]);
    }


    /**
     *  Gets the participant <code> Id </code> query terms. 
     *
     *  @return the participant <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getParticipantIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the participant query terms. 
     *
     *  @return the participant terms 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQueryInspector[] getParticipantTerms() {
        return (new org.osid.offering.ParticipantQueryInspector[0]);
    }


    /**
     *  Gets the result <code> Id </code> query terms. 
     *
     *  @return the result <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the result query terms. 
     *
     *  @return the result terms 
     */

    @OSID @Override
    public org.osid.offering.ResultQueryInspector[] getResultTerms() {
        return (new org.osid.offering.ResultQueryInspector[0]);
    }


    /**
     *  Gets the ancestor catalogue <code> Id </code> query terms. 
     *
     *  @return the ancestor catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor catalogue query terms. 
     *
     *  @return the ancestor catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getAncestorCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the descendant catalogue <code> Id </code> query terms. 
     *
     *  @return the descendant catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant catalogue query terms. 
     *
     *  @return the descendant catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getDescendantCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given catalogue query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a catalogue implementing the requested record.
     *
     *  @param catalogueRecordType a catalogue record type
     *  @return the catalogue query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CatalogueQueryInspectorRecord getCatalogueQueryInspectorRecord(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CatalogueQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(catalogueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalogue query. 
     *
     *  @param catalogueQueryInspectorRecord catalogue query inspector
     *         record
     *  @param catalogueRecordType catalogue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogueQueryInspectorRecord(org.osid.offering.records.CatalogueQueryInspectorRecord catalogueQueryInspectorRecord, 
                                                   org.osid.type.Type catalogueRecordType) {

        addRecordType(catalogueRecordType);
        nullarg(catalogueRecordType, "catalogue record type");
        this.records.add(catalogueQueryInspectorRecord);        
        return;
    }
}
