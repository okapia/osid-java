//
// AbstractImmutableComposition.java
//
//     Wraps a mutable Composition to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Composition</code> to hide modifiers. This
 *  wrapper provides an immutized Composition from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying composition whose state changes are visible.
 */

public abstract class AbstractImmutableComposition
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableSourceableOsidObject
    implements org.osid.repository.Composition {

    private final org.osid.repository.Composition composition;


    /**
     *  Constructs a new <code>AbstractImmutableComposition</code>.
     *
     *  @param composition the composition to immutablize
     *  @throws org.osid.NullArgumentException <code>composition</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableComposition(org.osid.repository.Composition composition) {
        super(composition);
        this.composition = composition;
        return;
    }


    /**
     *  Tests if this rule is active. <code> isActive() </code> is
     *  <code> true </code> if <code> isEnabled() </code> and <code>
     *  isOperational() </code> are <code> true </code> and <code>
     *  isDisabled() </code> is <code> false. </code>
     *
     *  @return <code> true </code> if this rule is active, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isActive() {
        return (this.composition.isActive());
    }


    /**
     *  Tests if this rule is administravely enabled. Administratively
     *  enabling overrides any enabling rule which may exist. If this
     *  method returns <code> true </code> then <code> isDisabled()
     *  </code> must return <code> false. </code>
     *
     *  @return <code> true </code> if this rule is enabled, <code>
     *          false </code> is the active status is determined by
     *          other rules
     */

    @OSID @Override
    public boolean isEnabled() {
        return (this.composition.isEnabled());
    }


    /**
     *  Tests if this rule is administravely
     *  disabled. Administratively disabling overrides any disabling
     *  rule which may exist. If this method returns <code> true
     *  </code> then <code> isDisabled() </code> must return <code>
     *  false. </code>
     *
     *  @return <code> true </code> if this rule is disabled, <code>
     *          false </code> is the active status is determined by
     *          other rules
     */

    @OSID @Override
    public boolean isDisabled() {
        return (this.composition.isDisabled());
    }


    /**
     *  Tests if this rule is operational in that all rules pertaining
     *  to this operation except for an administrative disable are
     *  <code> true.  </code>
     *
     *  @return <code> true </code> if this rule is operational, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isOperational() {
        return (this.composition.isOperational());
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.composition.isSequestered());
    }


    /**
     *  Gets the child <code> Ids </code> of this composition. 
     *
     *  @return the composition child <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getChildrenIds() {
        return (this.composition.getChildrenIds());
    }


    /**
     *  Gets the children of this composition. 
     *
     *  @return the composition children 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getChildren()
        throws org.osid.OperationFailedException {

        return (this.composition.getChildren());
    }


    /**
     *  Gets the composition record corresponding to the given <code> 
     *  Composition </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  compositionRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(compositionRecordType) </code> is <code> true </code> . 
     *
     *  @param  compositionRecordType a composition record type 
     *  @return the composition record 
     *  @throws org.osid.NullArgumentException <code> compositionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(compositionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.records.CompositionRecord getCompositionRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        return (this.composition.getCompositionRecord(compositionRecordType));
    }
}

