//
// AbstractNodeAuctionHouseHierarchySession.java
//
//     Defines an AuctionHouse hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an auction house hierarchy session for delivering a hierarchy
 *  of auction houses using the AuctionHouseNode interface.
 */

public abstract class AbstractNodeAuctionHouseHierarchySession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractAuctionHouseHierarchySession
    implements org.osid.bidding.AuctionHouseHierarchySession {

    private java.util.Collection<org.osid.bidding.AuctionHouseNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root auction house <code> Ids </code> in this hierarchy.
     *
     *  @return the root auction house <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAuctionHouseIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToIdList(this.roots));
    }


    /**
     *  Gets the root auction houses in the auction house hierarchy. A node
     *  with no parents is an orphan. While all auction house <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root auction houses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getRootAuctionHouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToAuctionHouseList(new net.okapia.osid.jamocha.bidding.auctionhousenode.ArrayAuctionHouseNodeList(this.roots)));
    }


    /**
     *  Adds a root auction house node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAuctionHouse(org.osid.bidding.AuctionHouseNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root auction house nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAuctionHouses(java.util.Collection<org.osid.bidding.AuctionHouseNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root auction house node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAuctionHouse(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.bidding.AuctionHouseNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> AuctionHouse </code> has any parents. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @return <code> true </code> if the auction house has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAuctionHouses(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAuctionHouseNode(auctionHouseId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  auction house.
     *
     *  @param  id an <code> Id </code> 
     *  @param  auctionHouseId the <code> Id </code> of an auction house 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> auctionHouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> auctionHouseId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAuctionHouse(org.osid.id.Id id, org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.AuctionHouseNodeList parents = getAuctionHouseNode(auctionHouseId).getParentAuctionHouseNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAuctionHouseNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given auction house. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @return the parent <code> Ids </code> of the auction house 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAuctionHouseIds(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhouse.AuctionHouseToIdList(getParentAuctionHouses(auctionHouseId)));
    }


    /**
     *  Gets the parents of the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> to query 
     *  @return the parents of the auction house 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getParentAuctionHouses(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToAuctionHouseList(getAuctionHouseNode(auctionHouseId).getParentAuctionHouseNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  auction house.
     *
     *  @param  id an <code> Id </code> 
     *  @param  auctionHouseId the Id of an auction house 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> auctionHouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAuctionHouse(org.osid.id.Id id, org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAuctionHouse(id, auctionHouseId)) {
            return (true);
        }

        try (org.osid.bidding.AuctionHouseList parents = getParentAuctionHouses(auctionHouseId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAuctionHouse(id, parents.getNextAuctionHouse().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an auction house has any children. 
     *
     *  @param  auctionHouseId an auction house <code> Id </code> 
     *  @return <code> true </code> if the <code> auctionHouseId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAuctionHouses(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionHouseNode(auctionHouseId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  auction house.
     *
     *  @param  id an <code> Id </code> 
     *  @param auctionHouseId the <code> Id </code> of an 
     *         auction house
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> auctionHouseId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> auctionHouseId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAuctionHouse(org.osid.id.Id id, org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAuctionHouse(auctionHouseId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  auction house.
     *
     *  @param  auctionHouseId the <code> Id </code> to query 
     *  @return the children of the auction house 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAuctionHouseIds(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhouse.AuctionHouseToIdList(getChildAuctionHouses(auctionHouseId)));
    }


    /**
     *  Gets the children of the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> to query 
     *  @return the children of the auction house 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getChildAuctionHouses(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToAuctionHouseList(getAuctionHouseNode(auctionHouseId).getChildAuctionHouseNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  auction house.
     *
     *  @param  id an <code> Id </code> 
     *  @param auctionHouseId the <code> Id </code> of an 
     *         auction house
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> auctionHouseId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAuctionHouse(org.osid.id.Id id, org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAuctionHouse(auctionHouseId, id)) {
            return (true);
        }

        try (org.osid.bidding.AuctionHouseList children = getChildAuctionHouses(auctionHouseId)) {
            while (children.hasNext()) {
                if (isDescendantOfAuctionHouse(id, children.getNextAuctionHouse().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  auction house.
     *
     *  @param  auctionHouseId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified auction house node 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAuctionHouseNodeIds(org.osid.id.Id auctionHouseId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.bidding.auctionhousenode.AuctionHouseNodeToNode(getAuctionHouseNode(auctionHouseId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given auction house.
     *
     *  @param  auctionHouseId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified auction house node 
     *  @throws org.osid.NotFoundException <code> auctionHouseId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> auctionHouseId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNode getAuctionHouseNodes(org.osid.id.Id auctionHouseId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionHouseNode(auctionHouseId));
    }


    /**
     *  Closes this <code>AuctionHouseHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an auction house node.
     *
     *  @param auctionHouseId the id of the auction house node
     *  @throws org.osid.NotFoundException <code>auctionHouseId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>auctionHouseId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.bidding.AuctionHouseNode getAuctionHouseNode(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(auctionHouseId, "auction house Id");
        for (org.osid.bidding.AuctionHouseNode auctionHouse : this.roots) {
            if (auctionHouse.getId().equals(auctionHouseId)) {
                return (auctionHouse);
            }

            org.osid.bidding.AuctionHouseNode r = findAuctionHouse(auctionHouse, auctionHouseId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(auctionHouseId + " is not found");
    }


    protected org.osid.bidding.AuctionHouseNode findAuctionHouse(org.osid.bidding.AuctionHouseNode node, 
                                                            org.osid.id.Id auctionHouseId)
        throws org.osid.OperationFailedException {

        try (org.osid.bidding.AuctionHouseNodeList children = node.getChildAuctionHouseNodes()) {
            while (children.hasNext()) {
                org.osid.bidding.AuctionHouseNode auctionHouse = children.getNextAuctionHouseNode();
                if (auctionHouse.getId().equals(auctionHouseId)) {
                    return (auctionHouse);
                }
                
                auctionHouse = findAuctionHouse(auctionHouse, auctionHouseId);
                if (auctionHouse != null) {
                    return (auctionHouse);
                }
            }
        }

        return (null);
    }
}
