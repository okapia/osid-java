//
// AbstractAdapterTriggerEnablerLookupSession.java
//
//    A TriggerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A TriggerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {

    private final org.osid.control.rules.TriggerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTriggerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTriggerEnablerLookupSession(org.osid.control.rules.TriggerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code TriggerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTriggerEnablers() {
        return (this.session.canLookupTriggerEnablers());
    }


    /**
     *  A complete view of the {@code TriggerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTriggerEnablerView() {
        this.session.useComparativeTriggerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code TriggerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTriggerEnablerView() {
        this.session.usePlenaryTriggerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include trigger enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    

    /**
     *  Only active trigger enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveTriggerEnablerView() {
        this.session.useActiveTriggerEnablerView();
        return;
    }


    /**
     *  Active and inactive trigger enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusTriggerEnablerView() {
        this.session.useAnyStatusTriggerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code TriggerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code TriggerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code TriggerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param triggerEnablerId {@code Id} of the {@code TriggerEnabler}
     *  @return the trigger enabler
     *  @throws org.osid.NotFoundException {@code triggerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code triggerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnabler getTriggerEnabler(org.osid.id.Id triggerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnabler(triggerEnablerId));
    }


    /**
     *  Gets a {@code TriggerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  triggerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code TriggerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  triggerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code TriggerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code triggerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByIds(org.osid.id.IdList triggerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablersByIds(triggerEnablerIds));
    }


    /**
     *  Gets a {@code TriggerEnablerList} corresponding to the given
     *  trigger enabler genus {@code Type} which does not include
     *  trigger enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  triggerEnablerGenusType a triggerEnabler genus type 
     *  @return the returned {@code TriggerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByGenusType(org.osid.type.Type triggerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablersByGenusType(triggerEnablerGenusType));
    }


    /**
     *  Gets a {@code TriggerEnablerList} corresponding to the given
     *  trigger enabler genus {@code Type} and include any additional
     *  trigger enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  triggerEnablerGenusType a triggerEnabler genus type 
     *  @return the returned {@code TriggerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByParentGenusType(org.osid.type.Type triggerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablersByParentGenusType(triggerEnablerGenusType));
    }


    /**
     *  Gets a {@code TriggerEnablerList} containing the given
     *  trigger enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  triggerEnablerRecordType a triggerEnabler record type 
     *  @return the returned {@code TriggerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code triggerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByRecordType(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablersByRecordType(triggerEnablerRecordType));
    }


    /**
     *  Gets a {@code TriggerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible
     *  through this session.
     *  
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code TriggerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code TriggerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible
     *  through this session.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code TriggerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getTriggerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code TriggerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  trigger enablers or an error results. Otherwise, the returned list
     *  may contain only those trigger enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, trigger enablers are returned that are currently
     *  active. In any status mode, active and inactive trigger enablers
     *  are returned.
     *
     *  @return a list of {@code TriggerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTriggerEnablers());
    }
}
