//
// AbstractImmutableLesson.java
//
//     Wraps a mutable Lesson to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Lesson</code> to hide modifiers. This
 *  wrapper provides an immutized Lesson from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying lesson whose state changes are visible.
 */

public abstract class AbstractImmutableLesson
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.plan.Lesson {

    private final org.osid.course.plan.Lesson lesson;


    /**
     *  Constructs a new <code>AbstractImmutableLesson</code>.
     *
     *  @param lesson the lesson to immutablize
     *  @throws org.osid.NullArgumentException <code>lesson</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLesson(org.osid.course.plan.Lesson lesson) {
        super(lesson);
        this.lesson = lesson;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the plan. 
     *
     *  @return the plan <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPlanId() {
        return (this.lesson.getPlanId());
    }


    /**
     *  Gets the plan. 
     *
     *  @return the plan 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.plan.Plan getPlan()
        throws org.osid.OperationFailedException {

        return (this.lesson.getPlan());
    }


    /**
     *  Gets the <code> Id </code> of the docet. 
     *
     *  @return the lesson <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDocetId() {
        return (this.lesson.getDocetId());
    }


    /**
     *  Gets the docet. 
     *
     *  @return the docet 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet()
        throws org.osid.OperationFailedException {

        return (this.lesson.getDocet());
    }


    /**
     *  Gets the <code> Ids </code> of the activities to which this lesson 
     *  applies. 
     *
     *  @return the activity <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActivityIds() {
        return (this.lesson.getActivityIds());
    }


    /**
     *  Gets the activities to which this lesson applies. A Lesson may span 
     *  multiple scheduled activities. 
     *
     *  @return the activities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException {

        return (this.lesson.getActivities());
    }


    /**
     *  Gets the planned start time within the first activity as deteremined 
     *  by the <code> Lesson </code> scheduling. The time expressed as a 
     *  duration relative to the starting time of the first activity. 
     *
     *  @return the starting time 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getPlannedStartTime() {
        return (this.lesson.getPlannedStartTime());
    }


    /**
     *  Tests if this lesson has begun. <code> hasBegun() </code> is <code> 
     *  true </code> for completed lessons. A lesson in progress is one where 
     *  <code> hasBegun() </code> is <code> true </code> and <code> 
     *  isComplete() </code> or <code> isSkipped() </code> is <code> false. 
     *  </code> 
     *
     *  @return <code> true </code> if this lesson has begun, <code> false 
     *          </code> if not yet begun 
     */

    @OSID @Override
    public boolean hasBegun() {
        return (this.lesson.hasBegun());
    }


    /**
     *  Gets the actual start time. The time expressed as a duration relative 
     *  to the starting time of the actual starting activity. 
     *
     *  @return the actual starting time 
     *  @throws org.osid.IllegalStateException <code> hasBegun() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualStartTime() {
        return (this.lesson.getActualStartTime());
    }


    /**
     *  Gets the <code> Id </code> of the activity when this lesson actually 
     *  began. 
     *
     *  @return the starting activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasBegun() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActualStartingActivityId() {
        return (this.lesson.getActualStartingActivityId());
    }


    /**
     *  Gets the activity when this lesson actually began. 
     *
     *  @return the starting activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.IllegalStateException <code> hasBegun() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.Activity getActualStartingActivity()
        throws org.osid.OperationFailedException {

        return (this.lesson.getActualStartingActivity());
    }


    /**
     *  Tests if this lesson has been marked as completed. 
     *
     *  @return <code> true </code> if this lesson is complete, <code> false 
     *          </code> if not completed 
     *  @throws org.osid.IllegalStateException <code> hasBegun() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.lesson.isComplete());
    }


    /**
     *  Tests if this lesson has been marked as skipped. A skipped lesson may 
     *  have been partially undertaken but <code> isComplete() </code> remains 
     *  <code> false. </code> 
     *
     *  @return <code> true </code> if this lesson is skipped, <code> false 
     *          </code> if not completed 
     */

    @OSID @Override
    public boolean isSkipped() {
        return (this.lesson.isSkipped());
    }


    /**
     *  Gets the actual completion time. The time expressed as a duration 
     *  relative to the starting time of the ending activity. 
     *
     *  @return the actual end time 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> and 
     *          <code> isSkipped() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualEndTime() {
        return (this.lesson.getActualEndTime());
    }


    /**
     *  Gets the <code> Id </code> of the activity when this lesson was 
     *  completed or skipped. 
     *
     *  @return the ending activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> and 
     *          <code> isSkipped() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActualEndingActivityId() {
        return (this.lesson.getActualEndingActivityId());
    }


    /**
     *  Gets the activity when this lesson was completed or skipped. 
     *
     *  @return the ending activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> and 
     *          <code> isSkipped() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.Activity getActualEndingActivity()
        throws org.osid.OperationFailedException {

        return (this.lesson.getActualEndingActivity());
    }


    /**
     *  Gets the actual duration of this lesson if it has completed, in 
     *  progress, or skipped. 
     *
     *  @return the actual time spent 
     *  @throws org.osid.IllegalStateException <code> hasBegun() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getActualTimeSpent() {
        return (this.lesson.getActualTimeSpent());
    }


    /**
     *  Gets the lesson record corresponding to the given <code> Lesson 
     *  </code> record <code> Type. </code> This method must be used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  lessonRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(lessonRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  lessonRecordType the type of lesson record to retrieve 
     *  @return the lesson record 
     *  @throws org.osid.NullArgumentException <code> lessonRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(planRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonRecord getLessonRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        return (this.lesson.getLessonRecord(lessonRecordType));
    }
}

