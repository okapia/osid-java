//
// AbstractSite.java
//
//     Defines a Site builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.site.spi;


/**
 *  Defines a <code>Site</code> builder.
 */

public abstract class AbstractSiteBuilder<T extends AbstractSiteBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.installation.site.SiteMiter site;


    /**
     *  Constructs a new <code>AbstractSiteBuilder</code>.
     *
     *  @param site the site to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSiteBuilder(net.okapia.osid.jamocha.builder.installation.site.SiteMiter site) {
        super(site);
        this.site = site;
        return;
    }


    /**
     *  Builds the site.
     *
     *  @return the new site
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.installation.Site build() {
        (new net.okapia.osid.jamocha.builder.validator.installation.site.SiteValidator(getValidations())).validate(this.site);
        return (new net.okapia.osid.jamocha.builder.installation.site.ImmutableSite(this.site));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the site miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.installation.site.SiteMiter getMiter() {
        return (this.site);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(String path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Adds a Site record.
     *
     *  @param record a site record
     *  @param recordType the type of site record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.installation.records.SiteRecord record, org.osid.type.Type recordType) {
        getMiter().addSiteRecord(record, recordType);
        return (self());
    }
}       


