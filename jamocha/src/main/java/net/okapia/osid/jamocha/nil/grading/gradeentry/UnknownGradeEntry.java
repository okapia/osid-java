//
// UnknownGradeEntry.java
//
//     Defines an unknown GradeEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.grading.gradeentry;


/**
 *  Defines an unknown <code>GradeEntry</code>.
 */

public final class UnknownGradeEntry
    extends net.okapia.osid.jamocha.nil.grading.gradeentry.spi.AbstractUnknownGradeEntry
    implements org.osid.grading.GradeEntry {


    /**
     *  Constructs a new <code>UnknownGradeEntry</code>.
     */

    public UnknownGradeEntry() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownGradeEntry</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownGradeEntry(boolean optional) {
        super(optional);
        addGradeEntryRecord(new GradeEntryRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown GradeEntry.
     *
     *  @return an unknown GradeEntry
     */

    public static org.osid.grading.GradeEntry create() {
        return (net.okapia.osid.jamocha.builder.validator.grading.gradeentry.GradeEntryValidator.validateGradeEntry(new UnknownGradeEntry()));
    }


    public class GradeEntryRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.grading.records.GradeEntryRecord {

        
        protected GradeEntryRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
