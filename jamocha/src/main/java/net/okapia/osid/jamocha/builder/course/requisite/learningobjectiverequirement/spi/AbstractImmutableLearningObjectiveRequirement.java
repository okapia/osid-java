//
// AbstractImmutableLearningObjectiveRequirement.java
//
//     Wraps a mutable LearningObjectiveRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.learningobjectiverequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>LearningObjectiveRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized LearningObjectiveRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying learningObjectiveRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableLearningObjectiveRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.LearningObjectiveRequirement {

    private final org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableLearningObjectiveRequirement</code>.
     *
     *  @param learningObjectiveRequirement the learning objective requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>learningObjectiveRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLearningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement) {
        super(learningObjectiveRequirement);
        this.learningObjectiveRequirement = learningObjectiveRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> LearningObjectiveRequirement. </code> All <code> 
     *  Requisites </code> must be satisifed to be a substitute for this 
     *  learning objective requirement. Inactive <code> Requisites </code> are 
     *  not evaluated but if no applicable requisite exists, then the 
     *  alternate requisite is not satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.learningObjectiveRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> LearningObjective. </code> 
     *
     *  @return the learning objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLearningObjectiveId() {
        return (this.learningObjectiveRequirement.getLearningObjectiveId());
    }


    /**
     *  Gets the <code> LearningObjective. </code> 
     *
     *  @return the learning objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getLearningObjective()
        throws org.osid.OperationFailedException {

        return (this.learningObjectiveRequirement.getLearningObjective());
    }


    /**
     *  Tests if a minimum grade in proficency is required for this learning 
     *  objective. 
     *
     *  @return <code> true </code> if a minimum proficiency is required, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasMinimumProficiency() {
        return (this.learningObjectiveRequirement.hasMinimumProficiency());
    }


    /**
     *  Gets the minimum proficiency expressed as a <code> Grade. </code> 
     *
     *  @return the minimum proficiency 
     *  @throws org.osid.IllegalStateException <code> hasMinimumProficiency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumProficiencyId() {
        return (this.learningObjectiveRequirement.getMinimumProficiencyId());
    }


    /**
     *  Gets the minimum proficiency expressed as a <code> Grade. </code> 
     *
     *  @return the minimum proficiency 
     *  @throws org.osid.IllegalStateException <code> hasMinimumProficiency() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getMinimumProficiency()
        throws org.osid.OperationFailedException {

        return (this.learningObjectiveRequirement.getMinimumProficiency());
    }


    /**
     *  Gets the learning objective requirement record corresponding to the 
     *  given <code> LearningObjectiveRequirement </code> record <code> Type. 
     *  </code> This method is used to retrieve an object implementing the 
     *  requested record. The <code> learningObjectiveRequirementRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> 
     *  hasRecordType(learningObjectiveRequirementRecordType) </code> is 
     *  <code> true </code> . 
     *
     *  @param  learningObjectiveRequirementRecordType the type of learning 
     *          objective requirement record to retrieve 
     *  @return the learning objective requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          learningObjectiveRequirementRecordType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(learningObjectiveRequirementRecordType) </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.LearningObjectiveRequirementRecord getLearningObjectiveRequirementRecord(org.osid.type.Type learningObjectiveRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.learningObjectiveRequirement.getLearningObjectiveRequirementRecord(learningObjectiveRequirementRecordType));
    }
}

