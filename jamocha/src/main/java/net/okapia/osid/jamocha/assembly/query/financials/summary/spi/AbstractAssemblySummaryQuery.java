//
// AbstractAssemblySummaryQuery.java
//
//     A SummaryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SummaryQuery that stores terms.
 */

public abstract class AbstractAssemblySummaryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCompendiumQuery
    implements org.osid.financials.SummaryQuery,
               org.osid.financials.SummaryQueryInspector,
               org.osid.financials.SummarySearchOrder {

    private final java.util.Collection<org.osid.financials.records.SummaryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.SummaryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.records.SummarySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySummaryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySummaryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches an account <code> Id. </code> 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        getAssembler().addIdTerm(getAccountIdColumn(), accountId, match);
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        getAssembler().clearTerms(getAccountIdColumn());
        return;
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (getAssembler().getIdTerms(getAccountIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAccountColumn(), style);
        return;
    }


    /**
     *  Gets the AccountId column name.
     *
     * @return the column name
     */

    protected String getAccountIdColumn() {
        return ("account_id");
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        getAssembler().clearTerms(getAccountColumn());
        return;
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Tests if an <code> AccountSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an account search order is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Gets the account search order. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Gets the Account column name.
     *
     * @return the column name
     */

    protected String getAccountColumn() {
        return ("account");
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> fiscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        getAssembler().addIdTerm(getFiscalPeriodIdColumn(), fiscalPeriodId, match);
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        getAssembler().clearTerms(getFiscalPeriodIdColumn());
        return;
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (getAssembler().getIdTerms(getFiscalPeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the fiscal 
     *  period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalPeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFiscalPeriodColumn(), style);
        return;
    }


    /**
     *  Gets the FiscalPeriodId column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodIdColumn() {
        return ("fiscal_period_id");
    }


    /**
     *  Tests if a <code> FiscalPeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Clears the fiscal period query terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        getAssembler().clearTerms(getFiscalPeriodColumn());
        return;
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Tests if a <code> FiscalPeriodSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period search order is 
     *          available, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the fiscal period search order. 
     *
     *  @return the fiscal period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchOrder getFiscalPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodSearchOrder() is false");
    }


    /**
     *  Gets the FiscalPeriod column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodColumn() {
        return ("fiscal_period");
    }


    /**
     *  Matches credits within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCredits(org.osid.financials.Currency from, 
                             org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getCreditsColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the credits terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        getAssembler().clearTerms(getCreditsColumn());
        return;
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCreditsTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCreditsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the credits. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCredits(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditsColumn(), style);
        return;
    }


    /**
     *  Gets the Credits column name.
     *
     * @return the column name
     */

    protected String getCreditsColumn() {
        return ("credits");
    }


    /**
     *  Matches debits within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDebits(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getDebitsColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the debits terms. 
     */

    @OSID @Override
    public void clearDebitsTerms() {
        getAssembler().clearTerms(getDebitsColumn());
        return;
    }


    /**
     *  Gets the debits query terms. 
     *
     *  @return the debits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getDebitsTerms() {
        return (getAssembler().getCurrencyRangeTerms(getDebitsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the debits. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebits(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDebitsColumn(), style);
        return;
    }


    /**
     *  Gets the Debits column name.
     *
     * @return the column name
     */

    protected String getDebitsColumn() {
        return ("debits");
    }


    /**
     *  Matches summaries with a delta between debits and credits. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchBalance(java.math.BigDecimal from, 
                             java.math.BigDecimal to, boolean match) {
        getAssembler().addDecimalRangeTerm(getBalanceColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the balance terms. 
     */

    @OSID @Override
    public void clearBalanceTerms() {
        getAssembler().clearTerms(getBalanceColumn());
        return;
    }


    /**
     *  Gets the balacnce query terms. 
     *
     *  @return the balance query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getBalanceTerms() {
        return (getAssembler().getDecimalRangeTerms(getBalanceColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the balance. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBalance(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBalanceColumn(), style);
        return;
    }


    /**
     *  Gets the Balance column name.
     *
     * @return the column name
     */

    protected String getBalanceColumn() {
        return ("balance");
    }


    /**
     *  Matches a budget within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBudget(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getBudgetColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the budget terms. 
     */

    @OSID @Override
    public void clearBudgetTerms() {
        getAssembler().clearTerms(getBudgetColumn());
        return;
    }


    /**
     *  Gets the budget query terms. 
     *
     *  @return the budget query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getBudgetTerms() {
        return (getAssembler().getCurrencyRangeTerms(getBudgetColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the budget 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBudget(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBudgetColumn(), style);
        return;
    }


    /**
     *  Gets the Budget column name.
     *
     * @return the column name
     */

    protected String getBudgetColumn() {
        return ("budget");
    }


    /**
     *  Matches summaries with a delta between the balance and the budget. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchDelta(java.math.BigDecimal from, java.math.BigDecimal to, 
                           boolean match) {
        getAssembler().addDecimalRangeTerm(getDeltaColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the delta terms. 
     */

    @OSID @Override
    public void clearDeltaTerms() {
        getAssembler().clearTerms(getDeltaColumn());
        return;
    }


    /**
     *  Gets the delta query terms. 
     *
     *  @return the delta query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeltaTerms() {
        return (getAssembler().getDecimalRangeTerms(getDeltaColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the delta. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDelta(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDeltaColumn(), style);
        return;
    }


    /**
     *  Gets the Delta column name.
     *
     * @return the column name
     */

    protected String getDeltaColumn() {
        return ("delta");
    }


    /**
     *  Matches a forecast within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchForecast(org.osid.financials.Currency from, 
                              org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getForecastColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the forecast terms. 
     */

    @OSID @Override
    public void clearForecastTerms() {
        getAssembler().clearTerms(getForecastColumn());
        return;
    }


    /**
     *  Gets the forecast query terms. 
     *
     *  @return the forecast query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getForecastTerms() {
        return (getAssembler().getCurrencyRangeTerms(getForecastColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the forecast. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByForecast(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getForecastColumn(), style);
        return;
    }


    /**
     *  Gets the Forecast column name.
     *
     * @return the column name
     */

    protected String getForecastColumn() {
        return ("forecast");
    }


    /**
     *  Tests if this summary supports the given record
     *  <code>Type</code>.
     *
     *  @param  summaryRecordType a summary record type 
     *  @return <code>true</code> if the summaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type summaryRecordType) {
        for (org.osid.financials.records.SummaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary query record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryQueryRecord getSummaryQueryRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummaryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryQueryInspectorRecord getSummaryQueryInspectorRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummaryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param summaryRecordType the summary record type
     *  @return the summary search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummarySearchOrderRecord getSummarySearchOrderRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummarySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this summary. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param summaryQueryRecord the summary query record
     *  @param summaryQueryInspectorRecord the summary query inspector
     *         record
     *  @param summarySearchOrderRecord the summary search order record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException
     *          <code>summaryQueryRecord</code>,
     *          <code>summaryQueryInspectorRecord</code>,
     *          <code>summarySearchOrderRecord</code> or
     *          <code>summaryRecordTypesummary</code> is
     *          <code>null</code>
     */
            
    protected void addSummaryRecords(org.osid.financials.records.SummaryQueryRecord summaryQueryRecord, 
                                      org.osid.financials.records.SummaryQueryInspectorRecord summaryQueryInspectorRecord, 
                                      org.osid.financials.records.SummarySearchOrderRecord summarySearchOrderRecord, 
                                      org.osid.type.Type summaryRecordType) {

        addRecordType(summaryRecordType);

        nullarg(summaryQueryRecord, "summary query record");
        nullarg(summaryQueryInspectorRecord, "summary query inspector record");
        nullarg(summarySearchOrderRecord, "summary search odrer record");

        this.queryRecords.add(summaryQueryRecord);
        this.queryInspectorRecords.add(summaryQueryInspectorRecord);
        this.searchOrderRecords.add(summarySearchOrderRecord);
        
        return;
    }
}
