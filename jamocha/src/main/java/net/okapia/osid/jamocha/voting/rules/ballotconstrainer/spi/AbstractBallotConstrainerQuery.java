//
// AbstractBallotConstrainerQuery.java
//
//     A template for making a BallotConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.ballotconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for ballot constrainers.
 */

public abstract class AbstractBallotConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.voting.rules.BallotConstrainerQuery {

    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches rules mapped to a ballot. 
     *
     *  @param  ballotId the ballot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ballotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBallotId(org.osid.id.Id ballotId, boolean match) {
        return;
    }


    /**
     *  Clears the ballot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBallotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BallotQuery </code> is available. 
     *
     *  @return <code> true </code> if a ballot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBallotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ballot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ballot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBallotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuery getRuledBallotQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBallotQuery() is false");
    }


    /**
     *  Matches rules mapped to any ballot. 
     *
     *  @param  match <code> true </code> for rules mapped to any ballot, 
     *          <code> false </code> to match rules mapped to no ballots 
     */

    @OSID @Override
    public void matchAnyRuledBallot(boolean match) {
        return;
    }


    /**
     *  Clears the ballot query terms. 
     */

    @OSID @Override
    public void clearRuledBallotTerms() {
        return;
    }


    /**
     *  Matches rules mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given ballot constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a ballot constrainer implementing the requested record.
     *
     *  @param ballotConstrainerRecordType a ballot constrainer record type
     *  @return the ballot constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerQueryRecord getBallotConstrainerQueryRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot constrainer query. 
     *
     *  @param ballotConstrainerQueryRecord ballot constrainer query record
     *  @param ballotConstrainerRecordType ballotConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotConstrainerQueryRecord(org.osid.voting.rules.records.BallotConstrainerQueryRecord ballotConstrainerQueryRecord, 
                                          org.osid.type.Type ballotConstrainerRecordType) {

        addRecordType(ballotConstrainerRecordType);
        nullarg(ballotConstrainerQueryRecord, "ballot constrainer query record");
        this.records.add(ballotConstrainerQueryRecord);        
        return;
    }
}
