//
// AbstractAdapterAuctionProcessorLookupSession.java
//
//    An AuctionProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AuctionProcessor lookup session adapter.
 */

public abstract class AbstractAdapterAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private final org.osid.bidding.rules.AuctionProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAuctionProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionProcessorLookupSession(org.osid.bidding.rules.AuctionProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code AuctionHouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code AuctionHouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the {@code AuctionHouse} associated with this session.
     *
     *  @return the {@code AuctionHouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform {@code AuctionProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAuctionProcessors() {
        return (this.session.canLookupAuctionProcessors());
    }


    /**
     *  A complete view of the {@code AuctionProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorView() {
        this.session.useComparativeAuctionProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code AuctionProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorView() {
        this.session.usePlenaryAuctionProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processors in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorView() {
        this.session.useActiveAuctionProcessorView();
        return;
    }


    /**
     *  Active and inactive auction processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorView() {
        this.session.useAnyStatusAuctionProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code AuctionProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AuctionProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AuctionProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param auctionProcessorId {@code Id} of the {@code AuctionProcessor}
     *  @return the auction processor
     *  @throws org.osid.NotFoundException {@code auctionProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code auctionProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessor getAuctionProcessor(org.osid.id.Id auctionProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessor(auctionProcessorId));
    }


    /**
     *  Gets an {@code AuctionProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AuctionProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AuctionProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByIds(org.osid.id.IdList auctionProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorsByIds(auctionProcessorIds));
    }


    /**
     *  Gets an {@code AuctionProcessorList} corresponding to the given
     *  auction processor genus {@code Type} which does not include
     *  auction processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known auction
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned {@code AuctionProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorsByGenusType(auctionProcessorGenusType));
    }


    /**
     *  Gets an {@code AuctionProcessorList} corresponding to the given
     *  auction processor genus {@code Type} and include any additional
     *  auction processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned {@code AuctionProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByParentGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorsByParentGenusType(auctionProcessorGenusType));
    }


    /**
     *  Gets an {@code AuctionProcessorList} containing the given
     *  auction processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorRecordType an auctionProcessor record type 
     *  @return the returned {@code AuctionProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code auctionProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByRecordType(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessorsByRecordType(auctionProcessorRecordType));
    }


    /**
     *  Gets all {@code AuctionProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @return a list of {@code AuctionProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAuctionProcessors());
    }
}
