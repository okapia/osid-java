//
// AbstractAssemblyAuctionConstrainerEnablerQuery.java
//
//     An AuctionConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.rules.auctionconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyAuctionConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.bidding.rules.AuctionConstrainerEnablerQuery,
               org.osid.bidding.rules.AuctionConstrainerEnablerQueryInspector,
               org.osid.bidding.rules.AuctionConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAuctionConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAuctionConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the auction constrainer. 
     *
     *  @param  auctionConstrainerId the auction constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionConstrainerId(org.osid.id.Id auctionConstrainerId, 
                                               boolean match) {
        getAssembler().addIdTerm(getRuledAuctionConstrainerIdColumn(), auctionConstrainerId, match);
        return;
    }


    /**
     *  Clears the auction constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledAuctionConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the auction constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuctionConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledAuctionConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledAuctionConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionConstrainerIdColumn() {
        return ("ruled_auction_constrainer_id");
    }


    /**
     *  Tests if an <code> AuctionConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction constrainer. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the auction constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuery getRuledAuctionConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any auction constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any auction 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no auction constrainers 
     */

    @OSID @Override
    public void matchAnyRuledAuctionConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAuctionConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the auction constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionConstrainerTerms() {
        getAssembler().clearTerms(getRuledAuctionConstrainerColumn());
        return;
    }


    /**
     *  Gets the auction constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQueryInspector[] getRuledAuctionConstrainerTerms() {
        return (new org.osid.bidding.rules.AuctionConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledAuctionConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledAuctionConstrainerColumn() {
        return ("ruled_auction_constrainer");
    }


    /**
     *  Matches enablers mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseIdColumn() {
        return ("auction_house_id");
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        getAssembler().clearTerms(getAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseColumn() {
        return ("auction_house");
    }


    /**
     *  Tests if this auctionConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  auctionConstrainerEnablerRecordType an auction constrainer enabler record type 
     *  @return <code>true</code> if the auctionConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType) {
        for (org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  auctionConstrainerEnablerRecordType the auction constrainer enabler record type 
     *  @return the auction constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord getAuctionConstrainerEnablerQueryRecord(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(auctionConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  auctionConstrainerEnablerRecordType the auction constrainer enabler record type 
     *  @return the auction constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryInspectorRecord getAuctionConstrainerEnablerQueryInspectorRecord(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(auctionConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param auctionConstrainerEnablerRecordType the auction constrainer enabler record type
     *  @return the auction constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchOrderRecord getAuctionConstrainerEnablerSearchOrderRecord(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(auctionConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this auction constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionConstrainerEnablerQueryRecord the auction constrainer enabler query record
     *  @param auctionConstrainerEnablerQueryInspectorRecord the auction constrainer enabler query inspector
     *         record
     *  @param auctionConstrainerEnablerSearchOrderRecord the auction constrainer enabler search order record
     *  @param auctionConstrainerEnablerRecordType auction constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerQueryRecord</code>,
     *          <code>auctionConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>auctionConstrainerEnablerSearchOrderRecord</code> or
     *          <code>auctionConstrainerEnablerRecordTypeauctionConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addAuctionConstrainerEnablerRecords(org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord auctionConstrainerEnablerQueryRecord, 
                                      org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryInspectorRecord auctionConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchOrderRecord auctionConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type auctionConstrainerEnablerRecordType) {

        addRecordType(auctionConstrainerEnablerRecordType);

        nullarg(auctionConstrainerEnablerQueryRecord, "auction constrainer enabler query record");
        nullarg(auctionConstrainerEnablerQueryInspectorRecord, "auction constrainer enabler query inspector record");
        nullarg(auctionConstrainerEnablerSearchOrderRecord, "auction constrainer enabler search odrer record");

        this.queryRecords.add(auctionConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(auctionConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(auctionConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
