//
// AbstractImmutableRoom.java
//
//     Wraps a mutable Room to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Room</code> to hide modifiers. This
 *  wrapper provides an immutized Room from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying room whose state changes are visible.
 */

public abstract class AbstractImmutableRoom
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.room.Room {

    private final org.osid.room.Room room;


    /**
     *  Constructs a new <code>AbstractImmutableRoom</code>.
     *
     *  @param room the room to immutablize
     *  @throws org.osid.NullArgumentException <code>room</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRoom(org.osid.room.Room room) {
        super(room);
        this.room = room;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.room.getBuildingId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.room.getBuilding());
    }


    /**
     *  Gets the floor <code> Id. </code> 
     *
     *  @return the floor <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFloorId() {
        return (this.room.getFloorId());
    }


    /**
     *  Gets the floor. 
     *
     *  @return the floor 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Floor getFloor()
        throws org.osid.OperationFailedException {

        return (this.room.getFloor());
    }


    /**
     *  Tests if this room is a subdivision of another room. 
     *
     *  @return <code> true </code> if this room is a subdivisiaion, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSubdivision() {
        return (this.room.isSubdivision());
    }


    /**
     *  Gets the enclosing room <code> Id </code> if this room is a 
     *  subdivision. 
     *
     *  @return the enclosing room <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEnclosingRoomId() {
        return (this.room.getEnclosingRoomId());
    }


    /**
     *  Gets the enclosing room if this room is a subdivision. 
     *
     *  @return the enlcosing room 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Room getEnclosingRoom()
        throws org.osid.OperationFailedException {

        return (this.room.getEnclosingRoom());
    }


    /**
     *  Gets the subdivision room <code> Ids </code> if this room is 
     *  subdivided. 
     *
     *  @return the subdivision room <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSubdivisionIds() {
        return (this.room.getSubdivisionIds());
    }


    /**
     *  Gets the subdivision rooms if this room is subdivided. 
     *
     *  @return the subdivision rooms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.RoomList getSubdivisions()
        throws org.osid.OperationFailedException {

        return (this.room.getSubdivisions());
    }


    /**
     *  Gets the designated or formal name of the room (e.g. Twenty Chimneys). 
     *  The display name may be mapped to this designated name, room number, 
     *  or some other room label. 
     *
     *  @return the room number 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDesignatedName() {
        return (this.room.getDesignatedName());
    }


    /**
     *  Gets the complete room number including the building and floor (e.g. 
     *  W20-306). 
     *
     *  @return the room number 
     */

    @OSID @Override
    public String getRoomNumber() {
        return (this.room.getRoomNumber());
    }


    /**
     *  Gets the room number within a floor (e.g. 06). 
     *
     *  @return the room code 
     */

    @OSID @Override
    public String getCode() {
        return (this.room.getCode());
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.room.hasArea());
    }


    /**
     *  Gets the area of this room in square feet. 
     *
     *  @return the area in square feet 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getArea() {
        return (this.room.getArea());
    }


    /**
     *  Gets the limit on occupancy of this room. 
     *
     *  @return the occupancy limit 
     */

    @OSID @Override
    public long getOccupancyLimit() {
        return (this.room.getOccupancyLimit());
    }


    /**
     *  Gets the <code> Ids </code> of any resources in this room such as 
     *  projectors, white boards, or other classes of equiptment. 
     *
     *  @return the resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResourceIds() {
        return (this.room.getResourceIds());
    }


    /**
     *  Gets a list of resources in this room such as projectors, white 
     *  boards, or other classes of equiptment. These classes of equipment are 
     *  expressed as resources although they may be managed at the individual 
     *  item level in the Inventory OSID. These resources are to facilitate 
     *  searches among equipped rooms. 
     *
     *  @return a list of resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException {

        return (this.room.getResources());
    }


    /**
     *  Gets the room record corresponding to the given <code> Room </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> roomRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(roomRecordType) </code> is <code> true </code> . 
     *
     *  @param  roomRecordType the type of room record to retrieve 
     *  @return the room record 
     *  @throws org.osid.NullArgumentException <code> roomRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(roomRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.records.RoomRecord getRoomRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        return (this.room.getRoomRecord(roomRecordType));
    }
}

