//
// AbstractCampusQueryInspector.java
//
//     A template for making a CampusQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.campus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for campuses.
 */

public abstract class AbstractCampusQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.room.CampusQueryInspector {

    private final java.util.Collection<org.osid.room.records.CampusQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the floor <code> Id </code> terms. 
     *
     *  @return the floor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFloorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the floor terms. 
     *
     *  @return the floor terms 
     */

    @OSID @Override
    public org.osid.room.FloorQueryInspector[] getFloorTerms() {
        return (new org.osid.room.FloorQueryInspector[0]);
    }


    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the ancestor campus <code> Id </code> terms. 
     *
     *  @return the ancestor campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor campus terms. 
     *
     *  @return the ancestor campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getAncestorCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }


    /**
     *  Gets the descendant campus <code> Id </code> terms. 
     *
     *  @return the descendant campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant campus terms. 
     *
     *  @return the descendant campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getDescendantCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given campus query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a campus implementing the requested record.
     *
     *  @param campusRecordType a campus record type
     *  @return the campus query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(campusRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.CampusQueryInspectorRecord getCampusQueryInspectorRecord(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.CampusQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(campusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(campusRecordType + " is not supported");
    }


    /**
     *  Adds a record to this campus query. 
     *
     *  @param campusQueryInspectorRecord campus query inspector
     *         record
     *  @param campusRecordType campus record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCampusQueryInspectorRecord(org.osid.room.records.CampusQueryInspectorRecord campusQueryInspectorRecord, 
                                                   org.osid.type.Type campusRecordType) {

        addRecordType(campusRecordType);
        nullarg(campusRecordType, "campus record type");
        this.records.add(campusQueryInspectorRecord);        
        return;
    }
}
