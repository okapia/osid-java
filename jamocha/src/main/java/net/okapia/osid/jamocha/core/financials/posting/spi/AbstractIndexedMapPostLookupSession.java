//
// AbstractIndexedMapPostLookupSession.java
//
//    A simple framework for providing a Post lookup service
//    backed by a fixed collection of posts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Post lookup service backed by a
 *  fixed collection of posts. The posts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some posts may be compatible
 *  with more types than are indicated through these post
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Posts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPostLookupSession
    extends AbstractMapPostLookupSession
    implements org.osid.financials.posting.PostLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.posting.Post> postsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.posting.Post>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.posting.Post> postsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.posting.Post>());


    /**
     *  Makes a <code>Post</code> available in this session.
     *
     *  @param  post a post
     *  @throws org.osid.NullArgumentException <code>post<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPost(org.osid.financials.posting.Post post) {
        super.putPost(post);

        this.postsByGenus.put(post.getGenusType(), post);
        
        try (org.osid.type.TypeList types = post.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.postsByRecord.put(types.getNextType(), post);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a post from this session.
     *
     *  @param postId the <code>Id</code> of the post
     *  @throws org.osid.NullArgumentException <code>postId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePost(org.osid.id.Id postId) {
        org.osid.financials.posting.Post post;
        try {
            post = getPost(postId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.postsByGenus.remove(post.getGenusType());

        try (org.osid.type.TypeList types = post.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.postsByRecord.remove(types.getNextType(), post);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePost(postId);
        return;
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> which does not include
     *  posts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known posts or an error results. Otherwise,
     *  the returned list may contain only those posts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.post.ArrayPostList(this.postsByGenus.get(postGenusType)));
    }


    /**
     *  Gets a <code>PostList</code> containing the given
     *  post record <code>Type</code>. In plenary mode, the
     *  returned list contains all known posts or an error
     *  results. Otherwise, the returned list may contain only those
     *  posts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  postRecordType a post record type 
     *  @return the returned <code>post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostList getPostsByRecordType(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.posting.post.ArrayPostList(this.postsByRecord.get(postRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.postsByGenus.clear();
        this.postsByRecord.clear();

        super.close();

        return;
    }
}
