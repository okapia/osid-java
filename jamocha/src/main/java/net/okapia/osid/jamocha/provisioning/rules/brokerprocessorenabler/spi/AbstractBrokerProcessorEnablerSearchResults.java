//
// AbstractBrokerProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBrokerProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.BrokerProcessorEnablerSearchResults {

    private org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablers;
    private final org.osid.provisioning.rules.BrokerProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBrokerProcessorEnablerSearchResults.
     *
     *  @param brokerProcessorEnablers the result set
     *  @param brokerProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablers</code>
     *          or <code>brokerProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBrokerProcessorEnablerSearchResults(org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablers,
                                            org.osid.provisioning.rules.BrokerProcessorEnablerQueryInspector brokerProcessorEnablerQueryInspector) {
        nullarg(brokerProcessorEnablers, "broker processor enablers");
        nullarg(brokerProcessorEnablerQueryInspector, "broker processor enabler query inspectpr");

        this.brokerProcessorEnablers = brokerProcessorEnablers;
        this.inspector = brokerProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the broker processor enabler list resulting from a search.
     *
     *  @return a broker processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablers() {
        if (this.brokerProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablers = this.brokerProcessorEnablers;
        this.brokerProcessorEnablers = null;
	return (brokerProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.BrokerProcessorEnablerQueryInspector getBrokerProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  broker processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a brokerProcessorEnabler implementing the requested
     *  record.
     *
     *  @param brokerProcessorEnablerSearchRecordType a brokerProcessorEnabler search 
     *         record type 
     *  @return the broker processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(brokerProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchResultsRecord getBrokerProcessorEnablerSearchResultsRecord(org.osid.type.Type brokerProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(brokerProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record broker processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBrokerProcessorEnablerRecord(org.osid.provisioning.rules.records.BrokerProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "broker processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
