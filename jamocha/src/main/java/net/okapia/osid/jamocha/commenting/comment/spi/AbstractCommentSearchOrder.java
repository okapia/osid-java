//
// AbstractCommentSearchOdrer.java
//
//     Defines a CommentSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code CommentSearchOrder}.
 */

public abstract class AbstractCommentSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.commenting.CommentSearchOrder {

    private final java.util.Collection<org.osid.commenting.records.CommentSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommentor(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order interface is available. 
     *
     *  @return <code> true </code> if a resource order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order interface. 
     *
     *  @return the resource search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCommentorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCommentorSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommentingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent order interface is available. 
     *
     *  @return <code> true </code> if an agent order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent order interface. 
     *
     *  @return the agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCommentingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCommentingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the rating. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRating(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a rating order interface is available. 
     *
     *  @return <code> true </code> if a rating order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the rating order interface. 
     *
     *  @return the rating search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRatingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getRatingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRatingSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  commentRecordType a comment record type 
     *  @return {@code true} if the commentRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code commentRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commentRecordType) {
        for (org.osid.commenting.records.CommentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  commentRecordType the comment record type 
     *  @return the comment search order record
     *  @throws org.osid.NullArgumentException
     *          {@code commentRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(commentRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.commenting.records.CommentSearchOrderRecord getCommentSearchOrderRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this comment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commentRecord the comment search odrer record
     *  @param commentRecordType comment record type
     *  @throws org.osid.NullArgumentException
     *          {@code commentRecord} or
     *          {@code commentRecordTypecomment} is
     *          {@code null}
     */
            
    protected void addCommentRecord(org.osid.commenting.records.CommentSearchOrderRecord commentSearchOrderRecord, 
                                     org.osid.type.Type commentRecordType) {

        addRecordType(commentRecordType);
        this.records.add(commentSearchOrderRecord);
        
        return;
    }
}
