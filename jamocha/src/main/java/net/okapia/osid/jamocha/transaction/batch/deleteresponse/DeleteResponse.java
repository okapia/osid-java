//
// DeleteResponse.java
//
//     Defines a DeleteResponse.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transaction.batch.deleteresponse;


/**
 *  Defines a <code>DeleteResponse</code>.
 */

public final class DeleteResponse
    extends net.okapia.osid.jamocha.transaction.batch.deleteresponse.spi.AbstractDeleteResponse
    implements org.osid.transaction.batch.DeleteResponse {


    /**
     *  Constructs a new successful <code>DeleteResponse</code>.
     *
     *  @param deletedId the deleted Id
     *  @throws org.osid.NullArgumentException <code>deletedId</code>
     *          is <code>null</code>
     */

    public DeleteResponse(org.osid.id.Id deletedId) {
        setDeletedId(deletedId);
        return;
    }


    /**
     *  Constructs a new unsuccessful <code>DeleteResponse</code>.
     *
     *  @param deletedId the deleted Id
     *  @param message the error message
     *  @throws org.osid.NullArgumentException <code>deletedId</code>
     *          or <code>message</code> is <code>null</code>
     */

    public DeleteResponse(org.osid.id.Id deletedId, org.osid.locale.DisplayText message) {

        this(deletedId);
        setErrorMessage(message);
        return;
    }
}
