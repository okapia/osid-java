//
// AbstractMapRegistrationLookupSession
//
//    A simple framework for providing a Registration lookup service
//    backed by a fixed collection of registrations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Registration lookup service backed by a
 *  fixed collection of registrations. The registrations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Registrations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRegistrationLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractRegistrationLookupSession
    implements org.osid.course.registration.RegistrationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.registration.Registration> registrations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.registration.Registration>());


    /**
     *  Makes a <code>Registration</code> available in this session.
     *
     *  @param  registration a registration
     *  @throws org.osid.NullArgumentException <code>registration<code>
     *          is <code>null</code>
     */

    protected void putRegistration(org.osid.course.registration.Registration registration) {
        this.registrations.put(registration.getId(), registration);
        return;
    }


    /**
     *  Makes an array of registrations available in this session.
     *
     *  @param  registrations an array of registrations
     *  @throws org.osid.NullArgumentException <code>registrations<code>
     *          is <code>null</code>
     */

    protected void putRegistrations(org.osid.course.registration.Registration[] registrations) {
        putRegistrations(java.util.Arrays.asList(registrations));
        return;
    }


    /**
     *  Makes a collection of registrations available in this session.
     *
     *  @param  registrations a collection of registrations
     *  @throws org.osid.NullArgumentException <code>registrations<code>
     *          is <code>null</code>
     */

    protected void putRegistrations(java.util.Collection<? extends org.osid.course.registration.Registration> registrations) {
        for (org.osid.course.registration.Registration registration : registrations) {
            this.registrations.put(registration.getId(), registration);
        }

        return;
    }


    /**
     *  Removes a Registration from this session.
     *
     *  @param  registrationId the <code>Id</code> of the registration
     *  @throws org.osid.NullArgumentException <code>registrationId<code> is
     *          <code>null</code>
     */

    protected void removeRegistration(org.osid.id.Id registrationId) {
        this.registrations.remove(registrationId);
        return;
    }


    /**
     *  Gets the <code>Registration</code> specified by its <code>Id</code>.
     *
     *  @param  registrationId <code>Id</code> of the <code>Registration</code>
     *  @return the registration
     *  @throws org.osid.NotFoundException <code>registrationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>registrationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration(org.osid.id.Id registrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.Registration registration = this.registrations.get(registrationId);
        if (registration == null) {
            throw new org.osid.NotFoundException("registration not found: " + registrationId);
        }

        return (registration);
    }


    /**
     *  Gets all <code>Registrations</code>. In plenary mode, the returned
     *  list contains all known registrations or an error
     *  results. Otherwise, the returned list may contain only those
     *  registrations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Registrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.registration.registration.ArrayRegistrationList(this.registrations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.registrations.clear();
        super.close();
        return;
    }
}
