//
// AbstractRouteSearchOdrer.java
//
//     Defines a RouteSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RouteSearchOrder}.
 */

public abstract class AbstractRouteSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.mapping.route.RouteSearchOrder {

    private final java.util.Collection<org.osid.mapping.route.records.RouteSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the route 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the route travel 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByETA(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  routeRecordType a route record type 
     *  @return {@code true} if the routeRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code routeRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type routeRecordType) {
        for (org.osid.mapping.route.records.RouteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(routeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  routeRecordType the route record type 
     *  @return the route search order record
     *  @throws org.osid.NullArgumentException
     *          {@code routeRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(routeRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteSearchOrderRecord getRouteSearchOrderRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this route. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param routeRecord the route search odrer record
     *  @param routeRecordType route record type
     *  @throws org.osid.NullArgumentException
     *          {@code routeRecord} or
     *          {@code routeRecordTyperoute} is
     *          {@code null}
     */
            
    protected void addRouteRecord(org.osid.mapping.route.records.RouteSearchOrderRecord routeSearchOrderRecord, 
                                     org.osid.type.Type routeRecordType) {

        addRecordType(routeRecordType);
        this.records.add(routeSearchOrderRecord);
        
        return;
    }
}
