//
// AbstractQueryActivityBundleLookupSession.java
//
//    An inline adapter that maps an ActivityBundleLookupSession to
//    an ActivityBundleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityBundleLookupSession to
 *  an ActivityBundleQuerySession.
 */

public abstract class AbstractQueryActivityBundleLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {

    private final org.osid.course.registration.ActivityBundleQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityBundleLookupSession.
     *
     *  @param querySession the underlying activity bundle query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityBundleLookupSession(org.osid.course.registration.ActivityBundleQuerySession querySession) {
        nullarg(querySession, "activity bundle query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>ActivityBundle</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityBundles() {
        return (this.session.canSearchActivityBundles());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity bundles in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the <code>ActivityBundle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityBundle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActivityBundle</code> and
     *  retained for compatibility.
     *
     *  @param  activityBundleId <code>Id</code> of the
     *          <code>ActivityBundle</code>
     *  @return the activity bundle
     *  @throws org.osid.NotFoundException <code>activityBundleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundle getActivityBundle(org.osid.id.Id activityBundleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchId(activityBundleId, true);
        org.osid.course.registration.ActivityBundleList activityBundles = this.session.getActivityBundlesByQuery(query);
        if (activityBundles.hasNext()) {
            return (activityBundles.getNextActivityBundle());
        } 
        
        throw new org.osid.NotFoundException(activityBundleId + " not found");
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityBundles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActivityBundles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  activityBundleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByIds(org.osid.id.IdList activityBundleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();

        try (org.osid.id.IdList ids = activityBundleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> which does not include
     *  activity bundles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchGenusType(activityBundleGenusType, true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> corresponding to the given
     *  activity bundle genus <code>Type</code> and include any additional
     *  activity bundles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleGenusType an activityBundle genus type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByParentGenusType(org.osid.type.Type activityBundleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchParentGenusType(activityBundleGenusType, true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityBundleList</code> containing the given
     *  activity bundle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityBundleRecordType an activityBundle record type 
     *  @return the returned <code>ActivityBundle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByRecordType(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchRecordType(activityBundleRecordType, true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given course
     *  offering <code> . </code> In plenary mode, the returned list
     *  contains all known courses or an error results. Otherwise, the
     *  returned list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @return the returned <code> ActivityBundle </code> list 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchCourseOfferingId(courseOfferingId, true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets an <code> ActivityBundleList </code> for a given
     *  activity. In plenary mode, the returned list contains all
     *  known courses or an error results. Otherwise, the returned
     *  list may contain only those activity bundles that are
     *  accessible through this session.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return the returned <code> ActivityBundle </code> list 
     *  @throws org.osid.NullArgumentException <code> activityId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundlesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchActivityId(activityId, true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets all <code>ActivityBundles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activity bundles or an error results. Otherwise, the returned list
     *  may contain only those activity bundles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ActivityBundles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityBundleList getActivityBundles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityBundleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivityBundlesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.registration.ActivityBundleQuery getQuery() {
        org.osid.course.registration.ActivityBundleQuery query = this.session.getActivityBundleQuery();
        
        return (query);
    }
}
