//
// AbstractCyclicTimePeriodQuery.java
//
//     A template for making a CyclicTimePeriod Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for cyclic time periods.
 */

public abstract class AbstractCyclicTimePeriodQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.calendaring.cycle.CyclicTimePeriodQuery {

    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the time period <code> Id </code> for this query to match cyclic 
     *  time periods with the given mapped time period. 
     *
     *  @param  timePeriodId a tiem period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the cyclic time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available for querying 
     *  time periods. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a cyclic time period that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match cyclic time periods with 
     *          any tiem period, <code> false </code> to match cyclic time 
     *          periods with no tiem periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given cyclic time period query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a cyclic time period implementing the requested record.
     *
     *  @param cyclicTimePeriodRecordType a cyclic time period record type
     *  @return the cyclic time period query record
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord getCyclicTimePeriodQueryRecord(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord record : this.records) {
            if (record.implementsRecordType(cyclicTimePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cyclic time period query. 
     *
     *  @param cyclicTimePeriodQueryRecord cyclic time period query record
     *  @param cyclicTimePeriodRecordType cyclicTimePeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCyclicTimePeriodQueryRecord(org.osid.calendaring.cycle.records.CyclicTimePeriodQueryRecord cyclicTimePeriodQueryRecord, 
                                          org.osid.type.Type cyclicTimePeriodRecordType) {

        addRecordType(cyclicTimePeriodRecordType);
        nullarg(cyclicTimePeriodQueryRecord, "cyclic time period query record");
        this.records.add(cyclicTimePeriodQueryRecord);        
        return;
    }
}
