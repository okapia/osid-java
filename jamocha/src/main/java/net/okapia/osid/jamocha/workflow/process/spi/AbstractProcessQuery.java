//
// AbstractProcessQuery.java
//
//     A template for making a Process Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for processes.
 */

public abstract class AbstractProcessQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.workflow.ProcessQuery {

    private final java.util.Collection<org.osid.workflow.records.ProcessQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enabled processes. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        return;
    }


    /**
     *  Clears the enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        return;
    }


    /**
     *  Sets the initial step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchInitialStepId(org.osid.id.Id stepId, boolean match) {
        return;
    }


    /**
     *  Clears the initial stap <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInitialStepIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInitialStepQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getInitialStepQuery() {
        throw new org.osid.UnimplementedException("supportsInitialStepQuery() is false");
    }


    /**
     *  Clears the initial step terms. 
     */

    @OSID @Override
    public void clearInitialStepTerms() {
        return;
    }


    /**
     *  Sets the initial state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInitialStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the initial state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInitialStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInitialStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getInitialStateQuery() {
        throw new org.osid.UnimplementedException("supportsInitialStateQuery() is false");
    }


    /**
     *  Clears the state terms. 
     */

    @OSID @Override
    public void clearInitialStateTerms() {
        return;
    }


    /**
     *  Sets the step <code> Id </code> for this query. 
     *
     *  @param  stepId the step <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStepId(org.osid.id.Id stepId, boolean match) {
        return;
    }


    /**
     *  Clears the step <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStepIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepQuery </code> is available. 
     *
     *  @return <code> true </code> if a step query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the step query 
     *  @throws org.osid.UnimplementedException <code> supportsStepyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepQuery getStepQuery() {
        throw new org.osid.UnimplementedException("supportsStepQuery() is false");
    }


    /**
     *  Matches processes that have any step. 
     *
     *  @param  match <code> true </code> to match processes with any step, 
     *          <code> false </code> to match processes with no step 
     */

    @OSID @Override
    public void matchAnyStep(boolean match) {
        return;
    }


    /**
     *  Clears the step query terms. 
     */

    @OSID @Override
    public void clearStepTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Matches processs that have any work. 
     *
     *  @param  match <code> true </code> to match processes with any work, 
     *          <code> false </code> to match processes with no work 
     */

    @OSID @Override
    public void matchAnyWork(boolean match) {
        return;
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the office <code> Id </code> for this query to match process 
     *  assigned to offices. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given process query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a process implementing the requested record.
     *
     *  @param processRecordType a process record type
     *  @return the process query record
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessQueryRecord getProcessQueryRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessQueryRecord record : this.records) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process query. 
     *
     *  @param processQueryRecord process query record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessQueryRecord(org.osid.workflow.records.ProcessQueryRecord processQueryRecord, 
                                          org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);
        nullarg(processQueryRecord, "process query record");
        this.records.add(processQueryRecord);        
        return;
    }
}
