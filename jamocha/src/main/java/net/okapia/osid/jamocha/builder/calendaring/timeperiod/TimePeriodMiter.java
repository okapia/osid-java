//
// TimePeriodMiter.java
//
//     Defines a TimePeriod miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.timeperiod;


/**
 *  Defines a <code>TimePeriod</code> miter for use with the builders.
 */

public interface TimePeriodMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.calendaring.TimePeriod {


    /**
     *  Sets the start.
     *
     *  @param start a start
     *  @throws org.osid.NullArgumentException <code>start</code> is
     *          <code>null</code>
     */

    public void setStart(org.osid.calendaring.DateTime start);


    /**
     *  Sets the end.
     *
     *  @param end an end
     *  @throws org.osid.NullArgumentException <code>end</code> is
     *          <code>null</code>
     */

    public void setEnd(org.osid.calendaring.DateTime end);


    /**
     *  Adds an exception.
     *
     *  @param exception an exception
     *  @throws org.osid.NullArgumentException <code>exception</code>
     *          is <code>null</code>
     */

    public void addException(org.osid.calendaring.Event exception);


    /**
     *  Sets all the exceptions.
     *
     *  @param exceptions a collection of exceptions
     *  @throws org.osid.NullArgumentException <code>exceptions</code>
     *          is <code>null</code>
     */

    public void setExceptions(java.util.Collection<org.osid.calendaring.Event> exceptions);


    /**
     *  Adds a TimePeriod record.
     *
     *  @param record a timePeriod record
     *  @param recordType the type of timePeriod record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addTimePeriodRecord(org.osid.calendaring.records.TimePeriodRecord record, org.osid.type.Type recordType);
}       


