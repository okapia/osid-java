//
// AbstractAuthorizationAgentPeerList
//
//     Implements a filter for an AuthorizationAgentPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.authorization.batch.authorizationagentpeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an AuthorizationAgentPeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedAuthorizationAgentPeerList
 *  to improve performance.
 */

public abstract class AbstractAuthorizationAgentPeerFilterList
    extends net.okapia.osid.jamocha.authorization.batch.authorizationagentpeer.spi.AbstractAuthorizationAgentPeerList
    implements org.osid.authorization.batch.AuthorizationAgentPeerList,
               net.okapia.osid.jamocha.inline.filter.authorization.batch.authorizationagentpeer.AuthorizationAgentPeerFilter {

    private org.osid.authorization.batch.AuthorizationAgentPeer authorizationAgentPeer;
    private final org.osid.authorization.batch.AuthorizationAgentPeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractAuthorizationAgentPeerFilterList</code>.
     *
     *  @param authorizationAgentPeerList an <code>AuthorizationAgentPeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationAgentPeerList</code> is <code>null</code>
     */

    protected AbstractAuthorizationAgentPeerFilterList(org.osid.authorization.batch.AuthorizationAgentPeerList authorizationAgentPeerList) {
        nullarg(authorizationAgentPeerList, "authorization agent peer list");
        this.list = authorizationAgentPeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.authorizationAgentPeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> AuthorizationAgentPeer </code> in this list. 
     *
     *  @return the next <code> AuthorizationAgentPeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> AuthorizationAgentPeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationAgentPeer getNextAuthorizationAgentPeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.authorization.batch.AuthorizationAgentPeer authorizationAgentPeer = this.authorizationAgentPeer;
            this.authorizationAgentPeer = null;
            return (authorizationAgentPeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in authorization agent peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizationAgentPeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters AuthorizationAgentPeers.
     *
     *  @param authorizationAgentPeer the authorization agent peer to filter
     *  @return <code>true</code> if the authorization agent peer passes the filter,
     *          <code>false</code> if the authorization agent peer should be filtered
     */

    public abstract boolean pass(org.osid.authorization.batch.AuthorizationAgentPeer authorizationAgentPeer);


    protected void prime() {
        if (this.authorizationAgentPeer != null) {
            return;
        }

        org.osid.authorization.batch.AuthorizationAgentPeer authorizationAgentPeer = null;

        while (this.list.hasNext()) {
            try {
                authorizationAgentPeer = this.list.getNextAuthorizationAgentPeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(authorizationAgentPeer)) {
                this.authorizationAgentPeer = authorizationAgentPeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
