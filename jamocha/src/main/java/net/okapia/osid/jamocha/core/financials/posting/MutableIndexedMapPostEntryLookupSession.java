//
// MutableIndexedMapPostEntryLookupSession
//
//    Implements a PostEntry lookup service backed by a collection of
//    postEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.posting;


/**
 *  Implements a PostEntry lookup service backed by a collection of
 *  post entries. The post entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some post entries may be compatible
 *  with more types than are indicated through these post entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of post entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPostEntryLookupSession
    extends net.okapia.osid.jamocha.core.financials.posting.spi.AbstractIndexedMapPostEntryLookupSession
    implements org.osid.financials.posting.PostEntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPostEntryLookupSession} with no post entries.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

      public MutableIndexedMapPostEntryLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPostEntryLookupSession} with a
     *  single post entry.
     *  
     *  @param business the business
     *  @param  postEntry a single postEntry
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntry} is {@code null}
     */

    public MutableIndexedMapPostEntryLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.posting.PostEntry postEntry) {
        this(business);
        putPostEntry(postEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPostEntryLookupSession} using an
     *  array of post entries.
     *
     *  @param business the business
     *  @param  postEntries an array of post entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntries} is {@code null}
     */

    public MutableIndexedMapPostEntryLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.posting.PostEntry[] postEntries) {
        this(business);
        putPostEntries(postEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPostEntryLookupSession} using a
     *  collection of post entries.
     *
     *  @param business the business
     *  @param  postEntries a collection of post entries
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code postEntries} is {@code null}
     */

    public MutableIndexedMapPostEntryLookupSession(org.osid.financials.Business business,
                                                  java.util.Collection<? extends org.osid.financials.posting.PostEntry> postEntries) {

        this(business);
        putPostEntries(postEntries);
        return;
    }
    

    /**
     *  Makes a {@code PostEntry} available in this session.
     *
     *  @param  postEntry a post entry
     *  @throws org.osid.NullArgumentException {@code postEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putPostEntry(org.osid.financials.posting.PostEntry postEntry) {
        super.putPostEntry(postEntry);
        return;
    }


    /**
     *  Makes an array of post entries available in this session.
     *
     *  @param  postEntries an array of post entries
     *  @throws org.osid.NullArgumentException {@code postEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putPostEntries(org.osid.financials.posting.PostEntry[] postEntries) {
        super.putPostEntries(postEntries);
        return;
    }


    /**
     *  Makes collection of post entries available in this session.
     *
     *  @param  postEntries a collection of post entries
     *  @throws org.osid.NullArgumentException {@code postEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putPostEntries(java.util.Collection<? extends org.osid.financials.posting.PostEntry> postEntries) {
        super.putPostEntries(postEntries);
        return;
    }


    /**
     *  Removes a PostEntry from this session.
     *
     *  @param postEntryId the {@code Id} of the post entry
     *  @throws org.osid.NullArgumentException {@code postEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removePostEntry(org.osid.id.Id postEntryId) {
        super.removePostEntry(postEntryId);
        return;
    }    
}
