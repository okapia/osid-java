//
// AbstractLogSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.log.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractLogSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.logging.LogSearchResults {

    private org.osid.logging.LogList logs;
    private final org.osid.logging.LogQueryInspector inspector;
    private final java.util.Collection<org.osid.logging.records.LogSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractLogSearchResults.
     *
     *  @param logs the result set
     *  @param logQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>logs</code>
     *          or <code>logQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractLogSearchResults(org.osid.logging.LogList logs,
                                            org.osid.logging.LogQueryInspector logQueryInspector) {
        nullarg(logs, "logs");
        nullarg(logQueryInspector, "log query inspectpr");

        this.logs = logs;
        this.inspector = logQueryInspector;

        return;
    }


    /**
     *  Gets the log list resulting from a search.
     *
     *  @return a log list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.logging.LogList getLogs() {
        if (this.logs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.logging.LogList logs = this.logs;
        this.logs = null;
	return (logs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.logging.LogQueryInspector getLogQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  log search record <code> Type. </code> This method must
     *  be used to retrieve a log implementing the requested
     *  record.
     *
     *  @param logSearchRecordType a log search 
     *         record type 
     *  @return the log search
     *  @throws org.osid.NullArgumentException
     *          <code>logSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(logSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogSearchResultsRecord getLogSearchResultsRecord(org.osid.type.Type logSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.logging.records.LogSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(logSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(logSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record log search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addLogRecord(org.osid.logging.records.LogSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "log record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
