//
// AbstractIndexedMapModelLookupSession.java
//
//    A simple framework for providing a Model lookup service
//    backed by a fixed collection of models with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Model lookup service backed by a
 *  fixed collection of models. The models are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some models may be compatible
 *  with more types than are indicated through these model
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Models</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapModelLookupSession
    extends AbstractMapModelLookupSession
    implements org.osid.inventory.ModelLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inventory.Model> modelsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Model>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.Model> modelsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Model>());


    /**
     *  Makes a <code>Model</code> available in this session.
     *
     *  @param  model a model
     *  @throws org.osid.NullArgumentException <code>model<code> is
     *          <code>null</code>
     */

    @Override
    protected void putModel(org.osid.inventory.Model model) {
        super.putModel(model);

        this.modelsByGenus.put(model.getGenusType(), model);
        
        try (org.osid.type.TypeList types = model.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.modelsByRecord.put(types.getNextType(), model);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a model from this session.
     *
     *  @param modelId the <code>Id</code> of the model
     *  @throws org.osid.NullArgumentException <code>modelId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeModel(org.osid.id.Id modelId) {
        org.osid.inventory.Model model;
        try {
            model = getModel(modelId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.modelsByGenus.remove(model.getGenusType());

        try (org.osid.type.TypeList types = model.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.modelsByRecord.remove(types.getNextType(), model);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeModel(modelId);
        return;
    }


    /**
     *  Gets a <code>ModelList</code> corresponding to the given
     *  model genus <code>Type</code> which does not include
     *  models of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known models or an error results. Otherwise,
     *  the returned list may contain only those models that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  modelGenusType a model genus type 
     *  @return the returned <code>Model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByGenusType(org.osid.type.Type modelGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.model.ArrayModelList(this.modelsByGenus.get(modelGenusType)));
    }


    /**
     *  Gets a <code>ModelList</code> containing the given
     *  model record <code>Type</code>. In plenary mode, the
     *  returned list contains all known models or an error
     *  results. Otherwise, the returned list may contain only those
     *  models that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  modelRecordType a model record type 
     *  @return the returned <code>model</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>modelRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModelsByRecordType(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.model.ArrayModelList(this.modelsByRecord.get(modelRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.modelsByGenus.clear();
        this.modelsByRecord.clear();

        super.close();

        return;
    }
}
