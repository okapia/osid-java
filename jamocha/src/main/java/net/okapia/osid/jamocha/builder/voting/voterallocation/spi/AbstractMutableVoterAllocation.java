//
// AbstractMutableVoterAllocation.java
//
//     Defines a mutable VoterAllocation.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.voterallocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>VoterAllocation</code>.
 */

public abstract class AbstractMutableVoterAllocation
    extends net.okapia.osid.jamocha.voting.voterallocation.spi.AbstractVoterAllocation
    implements org.osid.voting.VoterAllocation,
               net.okapia.osid.jamocha.builder.voting.voterallocation.VoterAllocationMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this voter allocation. 
     *
     *  @param record voter allocation record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addVoterAllocationRecord(org.osid.voting.records.VoterAllocationRecord record, org.osid.type.Type recordType) {
        super.addVoterAllocationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this voter allocation.
     *
     *  @param displayName the name for this voter allocation
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this voter allocation.
     *
     *  @param description the description of this voter allocation
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @throws org.osid.NullArgumentException <code>race</code> is
     *          <code>null</code>
     */

    @Override
    public void setRace(org.osid.voting.Race race) {
        super.setRace(race);
        return;
    }


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    @Override
    public void setVoter(org.osid.resource.Resource voter) {
        super.setVoter(voter);
        return;
    }


    /**
     *  Sets the total votes.
     *
     *  @param votes a total votes
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    @Override
    public void setTotalVotes(long votes) {
        super.setTotalVotes(votes);
        return;
    }


    /**
     *  Sets the max votes per candidate.
     *
     *  @param votes a max votes per candidate
     *  @throws org.osid.InvalidArgumentException <code>vote</code> is
     *          negative
     */

    @Override
    public void setMaxVotesPerCandidate(long votes) {
        super.setMaxVotesPerCandidate(votes);
        return;
    }


    /**
     *  Sets the max candidates.
     *
     *  @param candidates a max candidates
     *  @throws org.osid.InvalidArgumentException
     *          <code>candidates</code> is negative
     */

    @Override
    public void setMaxCandidates(long candidates) {
        super.setMaxCandidates(candidates);
        return;
    }


    /**
     *  Sets the reallocates votes when candidate drops.
     *
     *  @param reallocate <code> true </code> if votes are returned to
     *         the resource when the candidate drops from the race,
     *         <code> false </code> otherwise
     */

    @Override
    public void setReallocatesVotesWhenCandidateDrops(boolean reallocate) {
        super.setReallocatesVotesWhenCandidateDrops(reallocate);
        return;
    }
}

