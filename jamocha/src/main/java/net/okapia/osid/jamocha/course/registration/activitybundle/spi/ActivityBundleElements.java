//
// ActivityBundleElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activitybundle.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityBundleElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ActivityBundleElement Id.
     *
     *  @return the activity bundle element Id
     */

    public static org.osid.id.Id getActivityBundleEntityId() {
        return (makeEntityId("osid.course.registration.ActivityBundle"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeElementId("osid.course.registration.activitybundle.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeElementId("osid.course.registration.activitybundle.CourseOffering"));
    }


    /**
     *  Gets the ActivityIds element Id.
     *
     *  @return the ActivityIds element Id
     */

    public static org.osid.id.Id getActivityIds() {
        return (makeElementId("osid.course.registration.activitybundle.ActivityIds"));
    }


    /**
     *  Gets the Activities element Id.
     *
     *  @return the Activities element Id
     */

    public static org.osid.id.Id getActivities() {
        return (makeElementId("osid.course.registration.activitybundle.Activities"));
    }


    /**
     *  Gets the Credits element Id.
     *
     *  @return the Credits element Id
     */

    public static org.osid.id.Id getCredits() {
        return (makeElementId("osid.course.registration.activitybundle.Credits"));
    }


    /**
     *  Gets the GradingOptionIds element Id.
     *
     *  @return the GradingOptionIds element Id
     */

    public static org.osid.id.Id getGradingOptionIds() {
        return (makeElementId("osid.course.registration.activitybundle.GradingOptionIds"));
    }


    /**
     *  Gets the GradingOptions element Id.
     *
     *  @return the GradingOptions element Id
     */

    public static org.osid.id.Id getGradingOptions() {
        return (makeElementId("osid.course.registration.activitybundle.GradingOptions"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.registration.activitybundle.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.registration.activitybundle.CourseCatalog"));
    }
}
