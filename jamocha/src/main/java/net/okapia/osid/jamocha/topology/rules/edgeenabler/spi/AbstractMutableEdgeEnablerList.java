//
// AbstractMutableEdgeEnablerList.java
//
//     Implements a mutable EdgeEnablerList. This list allows EdgeEnablers
//     to be added after this object has been created.
//
//
// Tom Coppeto
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a mutable EdgeEnablerList. This list allows EdgeEnablers
 *  to be added after this edgeEnabler has been created. One this list
 *  has been returned to the consumer, all subsequent additions occur
 *  in a separate processing thread.  The creator of this edgeEnabler
 *  must invoke <code>eol()</code> when there are no more edgeEnablers to
 *  be added.</p>
 *
 *  <p> If the consumer of the <code>EdgeEnablerList</code> interface reaches
 *  the end of the internal buffer before <code>eol()</code>, then
 *  methods will block until more edgeEnablers are added or <code>eol()</code>
 *  is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more edgeEnablers to be added.</p>
 */

public abstract class AbstractMutableEdgeEnablerList
    extends AbstractEdgeEnablerList
    implements org.osid.topology.rules.EdgeEnablerList {

    private final java.util.Queue<org.osid.topology.rules.EdgeEnabler> stack = new java.util.concurrent.ConcurrentLinkedQueue<>();
    private volatile boolean eol = false;
    private volatile boolean closed = false;
    private volatile org.osid.OsidException error;
    

    /**
     *  Creates a new empty <code>AbstractMutableEdgeEnablerList</code>.
     */

    protected AbstractMutableEdgeEnablerList() {
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableEdgeEnablerList</code>.
     *
     *  @param edgeEnabler an <code>EdgeEnabler</code>
     *  @throws org.osid.NullArgumentException <code>edgeEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractMutableEdgeEnablerList(org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        nullarg(edgeEnabler, "edgeEnabler");
        addEdgeEnabler(edgeEnabler);
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableEdgeEnablerList</code>.
     *
     *  @param array an array of edgeenablers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    protected AbstractMutableEdgeEnablerList(org.osid.topology.rules.EdgeEnabler[] array) {
        nullarg(array, "array");
        addEdgeEnablers(array);
        return;
    }


    /**
     *  Creates a new <code>AbstractMutableEdgeEnablerList</code>.
     *
     *  @param collection a java.util.Collection of edgeEnablers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    protected AbstractMutableEdgeEnablerList(java.util.Collection<org.osid.topology.rules.EdgeEnabler> collection) {
        nullarg(collection, "collection");
        addEdgeEnablerCollection(collection);
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        synchronized (this.stack) {
            while (true) {
                if (this.stack.size() > 0) {
                    return (true);
                }

                if (hasError()) {
                    return (true);
                }

                if (this.eol) {
                    return (false);
                }

                try {
                    this.stack.wait();
                } catch (InterruptedException ie) {}
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        synchronized (this.stack) {
            return (this.stack.size());
        }
    }


    /**
     *  Skip the specified number of elements in the list. If the number 
     *  skipped is greater than the number of elements in the list, hasNext() 
     *  becomes false and available() returns zero as there are no more 
     *  elements to retrieve. 
     *
     *  @param  n the number of elements to skip 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public void skip(long n) {
        synchronized (this.stack) {
            while (n > 0) {
                if (hasError()) {
                    return;
                }

                /* blasted protected removeRange */
                if (this.stack.size() > 0) {
                    this.stack.remove();
                    --n;
                } else if (this.eol) {
                    return;
                } else {
                    try {
                        this.stack.wait();
                    } catch (InterruptedException ie) {}
                }
            }
            
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Gets the next <code>EdgeEnabler</code> in this list. 
     *
     *  @return the next <code>EdgeEnabler</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>EdgeEnabler</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler getNextEdgeEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(getError());
        }

        if (hasNext()) {
            synchronized (this.stack) {
                org.osid.topology.rules.EdgeEnabler edgeEnabler = this.stack.remove();
                this.stack.notifyAll();
                return (edgeEnabler);
            }
        } else {
            throw new org.osid.IllegalStateException("no more elements available in edgeenabler list");
        }
    }

        
    /**
     *  Gets the next set of <code>EdgeEnabler</code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param  n the number of <code>EdgeEnabler</code> elements requested which 
     *          must be less than or equal to <code> available() </code> 
     *  @return an array of <code>EdgeEnabler</code> elements. <code> </code> The 
     *          length of the array is less than or equal to the number 
     *          specified. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is less
     *          than zero
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler[] getNextEdgeEnablers(long n)
        throws org.osid.OperationFailedException {
        
        if (n > available()) {
            throw new org.osid.IllegalStateException("insufficient elements available");
        }

        if (hasError()) {
            throw new org.osid.OperationFailedException(getError());
        }

        org.osid.topology.rules.EdgeEnabler[] ret = new org.osid.topology.rules.EdgeEnabler[(int) n];

        synchronized (this.stack) {
            for (int i = 0; i < n; i++) {
                ret[i] = this.stack.remove();
            }

            this.stack.notifyAll();
        }

        return (ret);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        synchronized (this.stack) {
            this.stack.notifyAll();
            this.stack.clear();
            this.closed = true;
        }

        return;
    }


    /**
     *  Tests if this list has been closed.
     *
     *  @return <code>true</code> if this list has been closed,
     *          <code>false</code> if still active
     */

    public boolean isClosed() {
        return (this.closed);
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>done()</code> has not been invoked and no error has
     *  occurred.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational() {
        if (hasError()) {
            return (false);
        }

        if (isClosed()) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty() {
        synchronized (this.stack) {
            if (this.stack == null) {
                return (true);
            }
            
            if (this.stack.size() == 0) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Adds a <code>EdgeEnabler</code> to this <code>EdgeEnablerList</code>.
     *
     *  @param edgeEnabler the <code>EdgeEnabler</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>edgeEnabler</code>
     *          is <code>null</code>
     */

    public void addEdgeEnabler(org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        nullarg(edgeEnabler, "edgeEnabler");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.add(edgeEnabler);
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Adds a <code>EdgeEnabler</code> to this <code>EdgeEnablerList</code>.
     *
     *  @param edgeEnablers <code>EdgeEnabler</code> array to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>edgeEnablers</code>
     *          is <code>null</code>
     */

    public void addEdgeEnablers(org.osid.topology.rules.EdgeEnabler[] edgeEnablers) {
        nullarg(edgeEnablers, "edgeEnablers");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            for (org.osid.topology.rules.EdgeEnabler edgeEnabler : edgeEnablers) {
                this.stack.add(edgeEnabler);
            }

            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Adds a <code>EdgeEnabler</code> collection to this
     *  <code>EdgeEnablerList</code>.
     *
     *  @param collection an <code>EdgeEnabler</code> collection
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public void addEdgeEnablerCollection(java.util.Collection<org.osid.topology.rules.EdgeEnabler> collection) {
        nullarg(collection, "collection");

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.addAll(collection);
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    public void error(org.osid.OsidException error) {
        this.error = error;

        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.notifyAll();
        }

        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    public boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the error.
     *
     *  @return an error or <code>null</code> if none
     */

    protected org.osid.OsidException getError() {
        return (this.error);
    }


    /**
     *  There are no more elements to be added to this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    public void eol() {
        this.eol = true;
        
        if (isClosed()) {
            return;
        }

        synchronized (this.stack) {
            this.stack.notifyAll();
        }

        return;
    }
}
