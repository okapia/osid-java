//
// AbstractProjectSearchOdrer.java
//
//     Defines a ProjectSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProjectSearchOrder}.
 */

public abstract class AbstractProjectSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.room.construction.ProjectSearchOrder {

    private final java.util.Collection<org.osid.room.construction.records.ProjectSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the building. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuilding(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBuildingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  projectRecordType a project record type 
     *  @return {@code true} if the projectRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code projectRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type projectRecordType) {
        for (org.osid.room.construction.records.ProjectSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(projectRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  projectRecordType the project record type 
     *  @return the project search order record
     *  @throws org.osid.NullArgumentException
     *          {@code projectRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(projectRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectSearchOrderRecord getProjectSearchOrderRecord(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.ProjectSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(projectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(projectRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this project. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param projectRecord the project search odrer record
     *  @param projectRecordType project record type
     *  @throws org.osid.NullArgumentException
     *          {@code projectRecord} or
     *          {@code projectRecordTypeproject} is
     *          {@code null}
     */
            
    protected void addProjectRecord(org.osid.room.construction.records.ProjectSearchOrderRecord projectSearchOrderRecord, 
                                     org.osid.type.Type projectRecordType) {

        addRecordType(projectRecordType);
        this.records.add(projectSearchOrderRecord);
        
        return;
    }
}
