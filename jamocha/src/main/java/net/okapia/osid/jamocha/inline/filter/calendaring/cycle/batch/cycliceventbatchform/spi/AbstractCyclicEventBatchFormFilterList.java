//
// AbstractCyclicEventBatchFormList
//
//     Implements a filter for a CyclicEventBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.cycle.batch.cycliceventbatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CyclicEventBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCyclicEventBatchFormList
 *  to improve performance.
 */

public abstract class AbstractCyclicEventBatchFormFilterList
    extends net.okapia.osid.jamocha.calendaring.cycle.batch.cycliceventbatchform.spi.AbstractCyclicEventBatchFormList
    implements org.osid.calendaring.cycle.batch.CyclicEventBatchFormList,
               net.okapia.osid.jamocha.inline.filter.calendaring.cycle.batch.cycliceventbatchform.CyclicEventBatchFormFilter {

    private org.osid.calendaring.cycle.batch.CyclicEventBatchForm cyclicEventBatchForm;
    private final org.osid.calendaring.cycle.batch.CyclicEventBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCyclicEventBatchFormFilterList</code>.
     *
     *  @param cyclicEventBatchFormList a <code>CyclicEventBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventBatchFormList</code> is <code>null</code>
     */

    protected AbstractCyclicEventBatchFormFilterList(org.osid.calendaring.cycle.batch.CyclicEventBatchFormList cyclicEventBatchFormList) {
        nullarg(cyclicEventBatchFormList, "cyclic event batch form list");
        this.list = cyclicEventBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.cyclicEventBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> CyclicEventBatchForm </code> in this list. 
     *
     *  @return the next <code> CyclicEventBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> CyclicEventBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchForm getNextCyclicEventBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.cycle.batch.CyclicEventBatchForm cyclicEventBatchForm = this.cyclicEventBatchForm;
            this.cyclicEventBatchForm = null;
            return (cyclicEventBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in cyclic event batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicEventBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters CyclicEventBatchForms.
     *
     *  @param cyclicEventBatchForm the cyclic event batch form to filter
     *  @return <code>true</code> if the cyclic event batch form passes the filter,
     *          <code>false</code> if the cyclic event batch form should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.cycle.batch.CyclicEventBatchForm cyclicEventBatchForm);


    protected void prime() {
        if (this.cyclicEventBatchForm != null) {
            return;
        }

        org.osid.calendaring.cycle.batch.CyclicEventBatchForm cyclicEventBatchForm = null;

        while (this.list.hasNext()) {
            try {
                cyclicEventBatchForm = this.list.getNextCyclicEventBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(cyclicEventBatchForm)) {
                this.cyclicEventBatchForm = cyclicEventBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
