//
// AbstractMapProfileLookupSession
//
//    A simple framework for providing a Profile lookup service
//    backed by a fixed collection of profiles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Profile lookup service backed by a
 *  fixed collection of profiles. The profiles are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Profiles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProfileLookupSession
    extends net.okapia.osid.jamocha.profile.spi.AbstractProfileLookupSession
    implements org.osid.profile.ProfileLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.profile.Profile> profiles = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.profile.Profile>());


    /**
     *  Makes a <code>Profile</code> available in this session.
     *
     *  @param  profile a profile
     *  @throws org.osid.NullArgumentException <code>profile<code>
     *          is <code>null</code>
     */

    protected void putProfile(org.osid.profile.Profile profile) {
        this.profiles.put(profile.getId(), profile);
        return;
    }


    /**
     *  Makes an array of profiles available in this session.
     *
     *  @param  profiles an array of profiles
     *  @throws org.osid.NullArgumentException <code>profiles<code>
     *          is <code>null</code>
     */

    protected void putProfiles(org.osid.profile.Profile[] profiles) {
        putProfiles(java.util.Arrays.asList(profiles));
        return;
    }


    /**
     *  Makes a collection of profiles available in this session.
     *
     *  @param  profiles a collection of profiles
     *  @throws org.osid.NullArgumentException <code>profiles<code>
     *          is <code>null</code>
     */

    protected void putProfiles(java.util.Collection<? extends org.osid.profile.Profile> profiles) {
        for (org.osid.profile.Profile profile : profiles) {
            this.profiles.put(profile.getId(), profile);
        }

        return;
    }


    /**
     *  Removes a Profile from this session.
     *
     *  @param  profileId the <code>Id</code> of the profile
     *  @throws org.osid.NullArgumentException <code>profileId<code> is
     *          <code>null</code>
     */

    protected void removeProfile(org.osid.id.Id profileId) {
        this.profiles.remove(profileId);
        return;
    }


    /**
     *  Gets the <code>Profile</code> specified by its <code>Id</code>.
     *
     *  @param  profileId <code>Id</code> of the <code>Profile</code>
     *  @return the profile
     *  @throws org.osid.NotFoundException <code>profileId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>profileId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile(org.osid.id.Id profileId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.profile.Profile profile = this.profiles.get(profileId);
        if (profile == null) {
            throw new org.osid.NotFoundException("profile not found: " + profileId);
        }

        return (profile);
    }


    /**
     *  Gets all <code>Profiles</code>. In plenary mode, the returned
     *  list contains all known profiles or an error
     *  results. Otherwise, the returned list may contain only those
     *  profiles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Profiles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileList getProfiles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profile.ArrayProfileList(this.profiles.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profiles.clear();
        super.close();
        return;
    }
}
