//
// AbstractNodeLookupSession.java
//
//    A starter implementation framework for providing a Node
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Node
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getNodes(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractNodeLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.NodeLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();
    

    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this session.
     *
     *  @return the <code>Graph</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Node</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupNodes() {
        return (true);
    }


    /**
     *  A complete view of the <code>Node</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeNodeView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Node</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryNodeView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include nodes in graphs which are children of this
     *  graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Node</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Node</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Node</code> and retained for
     *  compatibility.
     *
     *  @param  nodeId <code>Id</code> of the
     *          <code>Node</code>
     *  @return the node
     *  @throws org.osid.NotFoundException <code>nodeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>nodeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Node getNode(org.osid.id.Id nodeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.topology.NodeList nodes = getNodes()) {
            while (nodes.hasNext()) {
                org.osid.topology.Node node = nodes.getNextNode();
                if (node.getId().equals(nodeId)) {
                    return (node);
                }
            }
        } 

        throw new org.osid.NotFoundException(nodeId + " not found");
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the nodes
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Nodes</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getNodes()</code>.
     *
     *  @param  nodeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>nodeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByIds(org.osid.id.IdList nodeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.topology.Node> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = nodeIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getNode(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("node " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.topology.node.LinkedNodeList(ret));
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given node
     *  genus <code>Type</code> which does not include nodes of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known nodes or
     *  an error results. Otherwise, the returned list may contain
     *  only those nodes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getNodes()</code>.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.node.NodeGenusFilterList(getNodes(), nodeGenusType));
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given node
     *  genus <code>Type</code> and include any additional nodes with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known nodes or
     *  an error results. Otherwise, the returned list may contain
     *  only those nodes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getNodes()</code>.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByParentGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getNodesByGenusType(nodeGenusType));
    }


    /**
     *  Gets a <code>NodeList</code> containing the given node record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known nodes or
     *  an error results. Otherwise, the returned list may contain
     *  only those nodes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getNodes()</code>.
     *
     *  @param  nodeRecordType a node record type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByRecordType(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.node.NodeRecordFilterList(getNodes(), nodeRecordType));
    }


    /**
     *  Gets all <code>Nodes</code>.
     *
     *  In plenary mode, the returned list contains all known nodes or
     *  an error results. Otherwise, the returned list may contain
     *  only those nodes that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Nodes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.topology.NodeList getNodes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the node list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of nodes
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.topology.NodeList filterNodesOnViews(org.osid.topology.NodeList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
