//
// AbstractAdapterRaceProcessorLookupSession.java
//
//    A RaceProcessor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RaceProcessor lookup session adapter.
 */

public abstract class AbstractAdapterRaceProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.RaceProcessorLookupSession {

    private final org.osid.voting.rules.RaceProcessorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRaceProcessorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRaceProcessorLookupSession(org.osid.voting.rules.RaceProcessorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code RaceProcessor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRaceProcessors() {
        return (this.session.canLookupRaceProcessors());
    }


    /**
     *  A complete view of the {@code RaceProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceProcessorView() {
        this.session.useComparativeRaceProcessorView();
        return;
    }


    /**
     *  A complete view of the {@code RaceProcessor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceProcessorView() {
        this.session.usePlenaryRaceProcessorView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processors in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceProcessorView() {
        this.session.useActiveRaceProcessorView();
        return;
    }


    /**
     *  Active and inactive race processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorView() {
        this.session.useAnyStatusRaceProcessorView();
        return;
    }
    
     
    /**
     *  Gets the {@code RaceProcessor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RaceProcessor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RaceProcessor} and
     *  retained for compatibility.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param raceProcessorId {@code Id} of the {@code RaceProcessor}
     *  @return the race processor
     *  @throws org.osid.NotFoundException {@code raceProcessorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code raceProcessorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessor getRaceProcessor(org.osid.id.Id raceProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessor(raceProcessorId));
    }


    /**
     *  Gets a {@code RaceProcessorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RaceProcessors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RaceProcessor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByIds(org.osid.id.IdList raceProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorsByIds(raceProcessorIds));
    }


    /**
     *  Gets a {@code RaceProcessorList} corresponding to the given
     *  race processor genus {@code Type} which does not include
     *  race processors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned {@code RaceProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorsByGenusType(raceProcessorGenusType));
    }


    /**
     *  Gets a {@code RaceProcessorList} corresponding to the given
     *  race processor genus {@code Type} and include any additional
     *  race processors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorGenusType a raceProcessor genus type 
     *  @return the returned {@code RaceProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByParentGenusType(org.osid.type.Type raceProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorsByParentGenusType(raceProcessorGenusType));
    }


    /**
     *  Gets a {@code RaceProcessorList} containing the given
     *  race processor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @param  raceProcessorRecordType a raceProcessor record type 
     *  @return the returned {@code RaceProcessor} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceProcessorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByRecordType(org.osid.type.Type raceProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessorsByRecordType(raceProcessorRecordType));
    }


    /**
     *  Gets all {@code RaceProcessors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  race processors or an error results. Otherwise, the returned list
     *  may contain only those race processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race processors are returned that are currently
     *  active. In any status mode, active and inactive race processors
     *  are returned.
     *
     *  @return a list of {@code RaceProcessors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceProcessors());
    }
}
