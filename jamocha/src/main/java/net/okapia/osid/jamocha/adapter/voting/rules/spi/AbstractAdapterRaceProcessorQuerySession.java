//
// AbstractQueryRaceProcessorLookupSession.java
//
//    A RaceProcessorQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceProcessorQuerySession adapter.
 */

public abstract class AbstractAdapterRaceProcessorQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.RaceProcessorQuerySession {

    private final org.osid.voting.rules.RaceProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterRaceProcessorQuerySession.
     *
     *  @param session the underlying race processor query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRaceProcessorQuerySession(org.osid.voting.rules.RaceProcessorQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codePolls</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codePolls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@codePolls</code> associated with this 
     *  session.
     *
     *  @return the {@codePolls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@codeRaceProcessor</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchRaceProcessors() {
        return (this.session.canSearchRaceProcessors());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processors in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this polls only.
     */
    
    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    
      
    /**
     *  Gets a race processor query. The returned query will not have an
     *  extension query.
     *
     *  @return the race processor query 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuery getRaceProcessorQuery() {
        return (this.session.getRaceProcessorQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  raceProcessorQuery the race processor query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code raceProcessorQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code raceProcessorQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorList getRaceProcessorsByQuery(org.osid.voting.rules.RaceProcessorQuery raceProcessorQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getRaceProcessorsByQuery(raceProcessorQuery));
    }
}
