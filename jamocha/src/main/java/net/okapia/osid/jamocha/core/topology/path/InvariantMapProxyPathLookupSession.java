//
// InvariantMapProxyPathLookupSession
//
//    Implements a Path lookup service backed by a fixed
//    collection of paths. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.path;


/**
 *  Implements a Path lookup service backed by a fixed
 *  collection of paths. The paths are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPathLookupSession
    extends net.okapia.osid.jamocha.core.topology.path.spi.AbstractMapPathLookupSession
    implements org.osid.topology.path.PathLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPathLookupSession} with no
     *  paths.
     *
     *  @param graph the graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPathLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.proxy.Proxy proxy) {
        setGraph(graph);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPathLookupSession} with a single
     *  path.
     *
     *  @param graph the graph
     *  @param path a single path
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code path} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPathLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.path.Path path, org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putPath(path);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPathLookupSession} using
     *  an array of paths.
     *
     *  @param graph the graph
     *  @param paths an array of paths
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code paths} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPathLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.topology.path.Path[] paths, org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPathLookupSession} using a
     *  collection of paths.
     *
     *  @param graph the graph
     *  @param paths a collection of paths
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code paths} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPathLookupSession(org.osid.topology.Graph graph,
                                                  java.util.Collection<? extends org.osid.topology.path.Path> paths,
                                                  org.osid.proxy.Proxy proxy) {

        this(graph, proxy);
        putPaths(paths);
        return;
    }
}
