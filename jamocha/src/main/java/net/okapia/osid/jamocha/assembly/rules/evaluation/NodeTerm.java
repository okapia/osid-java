//
// NodeTerm.java
//
//    A term for a boolean operation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.rules.evaluation;


/**
 *  A term for a boolean opertation.
 */


public final class NodeTerm    
    extends net.okapia.osid.jamocha.assembly.rules.evaluation.spi.AbstractTerm
    implements Term {

    private boolean and;
    private boolean negate;


    /**
     *  Constructs a new <code>NodeTerm</code>.
     *
     *  @param op "and" or "or"
     *  @param negate <code>true</code> for a positive,
     *         <code>false</code> to negate the result of the child
     *         terms
     */

    public NodeTerm(String op, boolean negate) {
        if (op.equalsIgnoreCase("and")) {
            this.and = true;
        } else {
            this.and = false;
        }

        this.negate = negate;
        return;
    }


    /**
     *  Evaluates the term.
     *
     *  @param condition
     *  @return result
     */

    @Override
    public boolean eval(org.osid.rules.Condition condition) {
        boolean result;

        if (this.and) {
            result = evalAnd(condition);
        } else {
            result = evalOr(condition);
        }

        if (this.negate) {
            return (!result);
        } else {
            return (result);
        }
    }
    

    private boolean evalAnd(org.osid.rules.Condition condition) {
        for (Term term : getTerms()) {
            if (!term.eval(condition)) {
                return (false);
            }
        }

        return (true);
    }


    private boolean evalOr(org.osid.rules.Condition condition) {
        for (Term term : getTerms()) {
            if (term.eval(condition)) {
                return (true);
            }
        }

        return (false);
    }
}
