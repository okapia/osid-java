//
// AbstractInquestSearch.java
//
//     A template for making an Inquest Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquest.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing inquest searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractInquestSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.inquiry.InquestSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.inquiry.records.InquestSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.inquiry.InquestSearchOrder inquestSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of inquests. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  inquestIds list of inquests
     *  @throws org.osid.NullArgumentException
     *          <code>inquestIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongInquests(org.osid.id.IdList inquestIds) {
        while (inquestIds.hasNext()) {
            try {
                this.ids.add(inquestIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongInquests</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of inquest Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getInquestIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  inquestSearchOrder inquest search order 
     *  @throws org.osid.NullArgumentException
     *          <code>inquestSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>inquestSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderInquestResults(org.osid.inquiry.InquestSearchOrder inquestSearchOrder) {
	this.inquestSearchOrder = inquestSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.inquiry.InquestSearchOrder getInquestSearchOrder() {
	return (this.inquestSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given inquest search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an inquest implementing the requested record.
     *
     *  @param inquestSearchRecordType an inquest search record
     *         type
     *  @return the inquest search record
     *  @throws org.osid.NullArgumentException
     *          <code>inquestSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquestSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquestSearchRecord getInquestSearchRecord(org.osid.type.Type inquestSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.inquiry.records.InquestSearchRecord record : this.records) {
            if (record.implementsRecordType(inquestSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquestSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquest search. 
     *
     *  @param inquestSearchRecord inquest search record
     *  @param inquestSearchRecordType inquest search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquestSearchRecord(org.osid.inquiry.records.InquestSearchRecord inquestSearchRecord, 
                                           org.osid.type.Type inquestSearchRecordType) {

        addRecordType(inquestSearchRecordType);
        this.records.add(inquestSearchRecord);        
        return;
    }
}
