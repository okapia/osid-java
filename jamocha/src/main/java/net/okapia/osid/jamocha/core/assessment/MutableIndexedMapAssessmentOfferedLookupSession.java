//
// MutableIndexedMapAssessmentOfferedLookupSession
//
//    Implements an AssessmentOffered lookup service backed by a collection of
//    assessmentsOffered indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an AssessmentOffered lookup service backed by a collection of
 *  assessments offered. The assessments offered are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some assessments offered may be compatible
 *  with more types than are indicated through these assessment offered
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of assessments offered can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractIndexedMapAssessmentOfferedLookupSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAssessmentOfferedLookupSession} with no assessments offered.
     *
     *  @param bank the bank
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is {@code null}
     */

      public MutableIndexedMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentOfferedLookupSession} with a
     *  single assessment offered.
     *  
     *  @param bank the bank
     *  @param  assessmentOffered an single assessmentOffered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentOffered} is {@code null}
     */

    public MutableIndexedMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.AssessmentOffered assessmentOffered) {
        this(bank);
        putAssessmentOffered(assessmentOffered);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentOfferedLookupSession} using an
     *  array of assessments offered.
     *
     *  @param bank the bank
     *  @param  assessmentsOffered an array of assessments offered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsOffered} is {@code null}
     */

    public MutableIndexedMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.assessment.AssessmentOffered[] assessmentsOffered) {
        this(bank);
        putAssessmentsOffered(assessmentsOffered);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAssessmentOfferedLookupSession} using a
     *  collection of assessments offered.
     *
     *  @param bank the bank
     *  @param  assessmentsOffered a collection of assessments offered
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsOffered} is {@code null}
     */

    public MutableIndexedMapAssessmentOfferedLookupSession(org.osid.assessment.Bank bank,
                                                  java.util.Collection<? extends org.osid.assessment.AssessmentOffered> assessmentsOffered) {

        this(bank);
        putAssessmentsOffered(assessmentsOffered);
        return;
    }
    

    /**
     *  Makes an {@code AssessmentOffered} available in this session.
     *
     *  @param  assessmentOffered an assessment offered
     *  @throws org.osid.NullArgumentException {@code assessmentOffered{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        super.putAssessmentOffered(assessmentOffered);
        return;
    }


    /**
     *  Makes an array of assessments offered available in this session.
     *
     *  @param  assessmentsOffered an array of assessments offered
     *  @throws org.osid.NullArgumentException {@code assessmentsOffered{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentsOffered(org.osid.assessment.AssessmentOffered[] assessmentsOffered) {
        super.putAssessmentsOffered(assessmentsOffered);
        return;
    }


    /**
     *  Makes collection of assessments offered available in this session.
     *
     *  @param  assessmentsOffered a collection of assessments offered
     *  @throws org.osid.NullArgumentException {@code assessmentOffered{@code  is
     *          {@code null}
     */

    @Override
    public void putAssessmentsOffered(java.util.Collection<? extends org.osid.assessment.AssessmentOffered> assessmentsOffered) {
        super.putAssessmentsOffered(assessmentsOffered);
        return;
    }


    /**
     *  Removes an AssessmentOffered from this session.
     *
     *  @param assessmentOfferedId the {@code Id} of the assessment offered
     *  @throws org.osid.NullArgumentException {@code assessmentOfferedId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentOffered(org.osid.id.Id assessmentOfferedId) {
        super.removeAssessmentOffered(assessmentOfferedId);
        return;
    }    
}
