//
// AbstractMapCandidateLookupSession
//
//    A simple framework for providing a Candidate lookup service
//    backed by a fixed collection of candidates.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Candidate lookup service backed by a
 *  fixed collection of candidates. The candidates are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Candidates</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCandidateLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.Candidate> candidates = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.Candidate>());


    /**
     *  Makes a <code>Candidate</code> available in this session.
     *
     *  @param  candidate a candidate
     *  @throws org.osid.NullArgumentException <code>candidate<code>
     *          is <code>null</code>
     */

    protected void putCandidate(org.osid.voting.Candidate candidate) {
        this.candidates.put(candidate.getId(), candidate);
        return;
    }


    /**
     *  Makes an array of candidates available in this session.
     *
     *  @param  candidates an array of candidates
     *  @throws org.osid.NullArgumentException <code>candidates<code>
     *          is <code>null</code>
     */

    protected void putCandidates(org.osid.voting.Candidate[] candidates) {
        putCandidates(java.util.Arrays.asList(candidates));
        return;
    }


    /**
     *  Makes a collection of candidates available in this session.
     *
     *  @param  candidates a collection of candidates
     *  @throws org.osid.NullArgumentException <code>candidates<code>
     *          is <code>null</code>
     */

    protected void putCandidates(java.util.Collection<? extends org.osid.voting.Candidate> candidates) {
        for (org.osid.voting.Candidate candidate : candidates) {
            this.candidates.put(candidate.getId(), candidate);
        }

        return;
    }


    /**
     *  Removes a Candidate from this session.
     *
     *  @param  candidateId the <code>Id</code> of the candidate
     *  @throws org.osid.NullArgumentException <code>candidateId<code> is
     *          <code>null</code>
     */

    protected void removeCandidate(org.osid.id.Id candidateId) {
        this.candidates.remove(candidateId);
        return;
    }


    /**
     *  Gets the <code>Candidate</code> specified by its <code>Id</code>.
     *
     *  @param  candidateId <code>Id</code> of the <code>Candidate</code>
     *  @return the candidate
     *  @throws org.osid.NotFoundException <code>candidateId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>candidateId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate(org.osid.id.Id candidateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.Candidate candidate = this.candidates.get(candidateId);
        if (candidate == null) {
            throw new org.osid.NotFoundException("candidate not found: " + candidateId);
        }

        return (candidate);
    }


    /**
     *  Gets all <code>Candidates</code>. In plenary mode, the returned
     *  list contains all known candidates or an error
     *  results. Otherwise, the returned list may contain only those
     *  candidates that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Candidates</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.candidate.ArrayCandidateList(this.candidates.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.candidates.clear();
        super.close();
        return;
    }
}
