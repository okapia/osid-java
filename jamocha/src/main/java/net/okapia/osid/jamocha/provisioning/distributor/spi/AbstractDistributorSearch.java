//
// AbstractDistributorSearch.java
//
//     A template for making a Distributor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.distributor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing distributor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDistributorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.DistributorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.DistributorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.DistributorSearchOrder distributorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of distributors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  distributorIds list of distributors
     *  @throws org.osid.NullArgumentException
     *          <code>distributorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDistributors(org.osid.id.IdList distributorIds) {
        while (distributorIds.hasNext()) {
            try {
                this.ids.add(distributorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDistributors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of distributor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDistributorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  distributorSearchOrder distributor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>distributorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>distributorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDistributorResults(org.osid.provisioning.DistributorSearchOrder distributorSearchOrder) {
	this.distributorSearchOrder = distributorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.DistributorSearchOrder getDistributorSearchOrder() {
	return (this.distributorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given distributor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a distributor implementing the requested record.
     *
     *  @param distributorSearchRecordType a distributor search record
     *         type
     *  @return the distributor search record
     *  @throws org.osid.NullArgumentException
     *          <code>distributorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(distributorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.DistributorSearchRecord getDistributorSearchRecord(org.osid.type.Type distributorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.records.DistributorSearchRecord record : this.records) {
            if (record.implementsRecordType(distributorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(distributorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this distributor search. 
     *
     *  @param distributorSearchRecord distributor search record
     *  @param distributorSearchRecordType distributor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDistributorSearchRecord(org.osid.provisioning.records.DistributorSearchRecord distributorSearchRecord, 
                                           org.osid.type.Type distributorSearchRecordType) {

        addRecordType(distributorSearchRecordType);
        this.records.add(distributorSearchRecord);        
        return;
    }
}
