//
// AbstractRecipeQueryInspector.java
//
//     A template for making a RecipeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for recipes.
 */

public abstract class AbstractRecipeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQueryInspector
    implements org.osid.recipe.RecipeQueryInspector {

    private final java.util.Collection<org.osid.recipe.records.RecipeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the estimated duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalEstimatedDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the direction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDirectionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the direction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQueryInspector[] getDirectionTerms() {
        return (new org.osid.recipe.DirectionQueryInspector[0]);
    }


    /**
     *  Gets the total duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the cook book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the cook book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given recipe query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a recipe implementing the requested record.
     *
     *  @param recipeRecordType a recipe record type
     *  @return the recipe query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeQueryInspectorRecord getRecipeQueryInspectorRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recipe query. 
     *
     *  @param recipeQueryInspectorRecord recipe query inspector
     *         record
     *  @param recipeRecordType recipe record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecipeQueryInspectorRecord(org.osid.recipe.records.RecipeQueryInspectorRecord recipeQueryInspectorRecord, 
                                                   org.osid.type.Type recipeRecordType) {

        addRecordType(recipeRecordType);
        nullarg(recipeRecordType, "recipe record type");
        this.records.add(recipeQueryInspectorRecord);        
        return;
    }
}
