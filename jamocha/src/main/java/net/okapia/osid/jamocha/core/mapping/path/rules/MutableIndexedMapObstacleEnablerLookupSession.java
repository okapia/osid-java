//
// MutableIndexedMapObstacleEnablerLookupSession
//
//    Implements an ObstacleEnabler lookup service backed by a collection of
//    obstacleEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules;


/**
 *  Implements an ObstacleEnabler lookup service backed by a collection of
 *  obstacle enablers. The obstacle enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some obstacle enablers may be compatible
 *  with more types than are indicated through these obstacle enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of obstacle enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.rules.spi.AbstractIndexedMapObstacleEnablerLookupSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapObstacleEnablerLookupSession} with no obstacle enablers.
     *
     *  @param map the map
     *  @throws org.osid.NullArgumentException {@code map}
     *          is {@code null}
     */

      public MutableIndexedMapObstacleEnablerLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObstacleEnablerLookupSession} with a
     *  single obstacle enabler.
     *  
     *  @param map the map
     *  @param  obstacleEnabler an single obstacleEnabler
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnabler} is {@code null}
     */

    public MutableIndexedMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        this(map);
        putObstacleEnabler(obstacleEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObstacleEnablerLookupSession} using an
     *  array of obstacle enablers.
     *
     *  @param map the map
     *  @param  obstacleEnablers an array of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnablers} is {@code null}
     */

    public MutableIndexedMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.rules.ObstacleEnabler[] obstacleEnablers) {
        this(map);
        putObstacleEnablers(obstacleEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapObstacleEnablerLookupSession} using a
     *  collection of obstacle enablers.
     *
     *  @param map the map
     *  @param  obstacleEnablers a collection of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code obstacleEnablers} is {@code null}
     */

    public MutableIndexedMapObstacleEnablerLookupSession(org.osid.mapping.Map map,
                                                  java.util.Collection<? extends org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers) {

        this(map);
        putObstacleEnablers(obstacleEnablers);
        return;
    }
    

    /**
     *  Makes an {@code ObstacleEnabler} available in this session.
     *
     *  @param  obstacleEnabler an obstacle enabler
     *  @throws org.osid.NullArgumentException {@code obstacleEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putObstacleEnabler(org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        super.putObstacleEnabler(obstacleEnabler);
        return;
    }


    /**
     *  Makes an array of obstacle enablers available in this session.
     *
     *  @param  obstacleEnablers an array of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code obstacleEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putObstacleEnablers(org.osid.mapping.path.rules.ObstacleEnabler[] obstacleEnablers) {
        super.putObstacleEnablers(obstacleEnablers);
        return;
    }


    /**
     *  Makes collection of obstacle enablers available in this session.
     *
     *  @param  obstacleEnablers a collection of obstacle enablers
     *  @throws org.osid.NullArgumentException {@code obstacleEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putObstacleEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers) {
        super.putObstacleEnablers(obstacleEnablers);
        return;
    }


    /**
     *  Removes an ObstacleEnabler from this session.
     *
     *  @param obstacleEnablerId the {@code Id} of the obstacle enabler
     *  @throws org.osid.NullArgumentException {@code obstacleEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeObstacleEnabler(org.osid.id.Id obstacleEnablerId) {
        super.removeObstacleEnabler(obstacleEnablerId);
        return;
    }    
}
