//
// AbstractForumManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractForumManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.forum.ForumManager,
               org.osid.forum.ForumProxyManager {

    private final Types postRecordTypes                    = new TypeRefSet();
    private final Types postSearchRecordTypes              = new TypeRefSet();

    private final Types replyRecordTypes                   = new TypeRefSet();
    private final Types replySearchRecordTypes             = new TypeRefSet();

    private final Types forumRecordTypes                   = new TypeRefSet();
    private final Types forumSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractForumManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractForumManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any post federation is exposed. Federation is exposed when a 
     *  specific post may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of posts 
     *  appears as a single post. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of an post lookup service. 
     *
     *  @return <code> true </code> if post lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostLookup() {
        return (false);
    }


    /**
     *  Tests if querying posts is available. 
     *
     *  @return <code> true </code> if post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Tests if searching for posts is available. 
     *
     *  @return <code> true </code> if post search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a post administrative service for 
     *  creating and deleting posts. 
     *
     *  @return <code> true </code> if post administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a post notification service. 
     *
     *  @return <code> true </code> if post notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostNotification() {
        return (false);
    }


    /**
     *  Tests if a post to forum lookup session is available. 
     *
     *  @return <code> true </code> if post forum lookup session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostForum() {
        return (false);
    }


    /**
     *  Tests if a post to forum assignment session is available. 
     *
     *  @return <code> true </code> if post forum assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostForumAssignment() {
        return (false);
    }


    /**
     *  Tests if a post smart foruming session is available. 
     *
     *  @return <code> true </code> if post smart foruming is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostSmartForum() {
        return (false);
    }


    /**
     *  Tests for the availability of a reply lookup service. 
     *
     *  @return <code> true </code> if reply lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyLookup() {
        return (false);
    }


    /**
     *  Tests if searching for replies is available. 
     *
     *  @return <code> true </code> if reply search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyAdmin() {
        return (false);
    }


    /**
     *  Tests if reply notification is available. 
     *
     *  @return <code> true </code> if reply notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an forum lookup service. 
     *
     *  @return <code> true </code> if forum lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumLookup() {
        return (false);
    }


    /**
     *  Tests if querying forums is available. 
     *
     *  @return <code> true </code> if forum query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumQuery() {
        return (false);
    }


    /**
     *  Tests if searching for forums is available. 
     *
     *  @return <code> true </code> if forum search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a forum administrative service for 
     *  creating and deleting forums. 
     *
     *  @return <code> true </code> if forum administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a forum notification service. 
     *
     *  @return <code> true </code> if forum notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a forum hierarchy traversal service. 
     *
     *  @return <code> true </code> if forum hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a forum hierarchy design service. 
     *
     *  @return <code> true </code> if forum hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if forum batch service is available. 
     *
     *  @return <code> true </code> if forum batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Post </code> record types. 
     *
     *  @return a list containing the supported post record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Post </code> record type is supported. 
     *
     *  @param  postRecordType a <code> Type </code> indicating a <code> Post 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostRecordType(org.osid.type.Type postRecordType) {
        return (this.postRecordTypes.contains(postRecordType));
    }


    /**
     *  Adds support for a post record type.
     *
     *  @param postRecordType a post record type
     *  @throws org.osid.NullArgumentException
     *  <code>postRecordType</code> is <code>null</code>
     */

    protected void addPostRecordType(org.osid.type.Type postRecordType) {
        this.postRecordTypes.add(postRecordType);
        return;
    }


    /**
     *  Removes support for a post record type.
     *
     *  @param postRecordType a post record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postRecordType</code> is <code>null</code>
     */

    protected void removePostRecordType(org.osid.type.Type postRecordType) {
        this.postRecordTypes.remove(postRecordType);
        return;
    }


    /**
     *  Gets the supported post search record types. 
     *
     *  @return a list containing the supported post search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPostSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.postSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given post search record type is supported. 
     *
     *  @param  postSearchRecordType a <code> Type </code> indicating a post 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> postSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        return (this.postSearchRecordTypes.contains(postSearchRecordType));
    }


    /**
     *  Adds support for a post search record type.
     *
     *  @param postSearchRecordType a post search record type
     *  @throws org.osid.NullArgumentException
     *  <code>postSearchRecordType</code> is <code>null</code>
     */

    protected void addPostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        this.postSearchRecordTypes.add(postSearchRecordType);
        return;
    }


    /**
     *  Removes support for a post search record type.
     *
     *  @param postSearchRecordType a post search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>postSearchRecordType</code> is <code>null</code>
     */

    protected void removePostSearchRecordType(org.osid.type.Type postSearchRecordType) {
        this.postSearchRecordTypes.remove(postSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Reply </code> record types. 
     *
     *  @return a list containing the supported reply record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReplyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.replyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Reply </code> record type is supported. 
     *
     *  @param  replyRecordType a <code> Type </code> indicating a <code> 
     *          Reply </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> replyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReplyRecordType(org.osid.type.Type replyRecordType) {
        return (this.replyRecordTypes.contains(replyRecordType));
    }


    /**
     *  Adds support for a reply record type.
     *
     *  @param replyRecordType a reply record type
     *  @throws org.osid.NullArgumentException
     *  <code>replyRecordType</code> is <code>null</code>
     */

    protected void addReplyRecordType(org.osid.type.Type replyRecordType) {
        this.replyRecordTypes.add(replyRecordType);
        return;
    }


    /**
     *  Removes support for a reply record type.
     *
     *  @param replyRecordType a reply record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>replyRecordType</code> is <code>null</code>
     */

    protected void removeReplyRecordType(org.osid.type.Type replyRecordType) {
        this.replyRecordTypes.remove(replyRecordType);
        return;
    }


    /**
     *  Gets the supported reply search record types. 
     *
     *  @return a list containing the supported reply search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReplySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.replySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given reply search record type is supported. 
     *
     *  @param  replySearchRecordType a <code> Type </code> indicating a reply 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> replySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReplySearchRecordType(org.osid.type.Type replySearchRecordType) {
        return (this.replySearchRecordTypes.contains(replySearchRecordType));
    }


    /**
     *  Adds support for a reply search record type.
     *
     *  @param replySearchRecordType a reply search record type
     *  @throws org.osid.NullArgumentException
     *  <code>replySearchRecordType</code> is <code>null</code>
     */

    protected void addReplySearchRecordType(org.osid.type.Type replySearchRecordType) {
        this.replySearchRecordTypes.add(replySearchRecordType);
        return;
    }


    /**
     *  Removes support for a reply search record type.
     *
     *  @param replySearchRecordType a reply search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>replySearchRecordType</code> is <code>null</code>
     */

    protected void removeReplySearchRecordType(org.osid.type.Type replySearchRecordType) {
        this.replySearchRecordTypes.remove(replySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Forum </code> record types. 
     *
     *  @return a list containing the supported forum record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getForumRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.forumRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Forum </code> record type is supported. 
     *
     *  @param  forumRecordType a <code> Type </code> indicating a <code> 
     *          Forum </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> forumRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsForumRecordType(org.osid.type.Type forumRecordType) {
        return (this.forumRecordTypes.contains(forumRecordType));
    }


    /**
     *  Adds support for a forum record type.
     *
     *  @param forumRecordType a forum record type
     *  @throws org.osid.NullArgumentException
     *  <code>forumRecordType</code> is <code>null</code>
     */

    protected void addForumRecordType(org.osid.type.Type forumRecordType) {
        this.forumRecordTypes.add(forumRecordType);
        return;
    }


    /**
     *  Removes support for a forum record type.
     *
     *  @param forumRecordType a forum record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>forumRecordType</code> is <code>null</code>
     */

    protected void removeForumRecordType(org.osid.type.Type forumRecordType) {
        this.forumRecordTypes.remove(forumRecordType);
        return;
    }


    /**
     *  Gets the supported forum search record types. 
     *
     *  @return a list containing the supported forum search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getForumSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.forumSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given forum search record type is supported. 
     *
     *  @param  forumSearchRecordType a <code> Type </code> indicating a forum 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> forumSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsForumSearchRecordType(org.osid.type.Type forumSearchRecordType) {
        return (this.forumSearchRecordTypes.contains(forumSearchRecordType));
    }


    /**
     *  Adds support for a forum search record type.
     *
     *  @param forumSearchRecordType a forum search record type
     *  @throws org.osid.NullArgumentException
     *  <code>forumSearchRecordType</code> is <code>null</code>
     */

    protected void addForumSearchRecordType(org.osid.type.Type forumSearchRecordType) {
        this.forumSearchRecordTypes.add(forumSearchRecordType);
        return;
    }


    /**
     *  Removes support for a forum search record type.
     *
     *  @param forumSearchRecordType a forum search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>forumSearchRecordType</code> is <code>null</code>
     */

    protected void removeForumSearchRecordType(org.osid.type.Type forumSearchRecordType) {
        this.forumSearchRecordTypes.remove(forumSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostLookupSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PostLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostLookupSession getPostLookupSessionForForum(org.osid.id.Id forumId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostLookupSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostQuerySessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post query 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PostQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Post </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuerySession getPostQuerySessionForForum(org.osid.id.Id forumId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostQuerySessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostSearchSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post search 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PostSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Post </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSearchSession getPostSearchSessionForForum(org.osid.id.Id forumId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostSearchSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administrative service. 
     *
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administrative service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Post </code> 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostAdminSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PostAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Post </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostAdminSession getPostAdminSessionForForum(org.osid.id.Id forumId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostAdminSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the receiver 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSession(org.osid.forum.PostReceiver postReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service. 
     *
     *  @param  postReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSession(org.osid.forum.PostReceiver postReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given forum. 
     *
     *  @param  postReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSessionForForum(org.osid.forum.PostReceiver postReceiver, 
                                                                                     org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostNotificationSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the post 
     *  notification service for the given forum. 
     *
     *  @param  postReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PostNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Post </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> postReceiver, forumId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostNotificationSession getPostNotificationSessionForForum(org.osid.forum.PostReceiver postReceiver, 
                                                                                     org.osid.id.Id forumId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostNotificationSessionForForum not implemented");
    }


    /**
     *  Gets the session for retrieving post to forum mappings. 
     *
     *  @return a <code> PostForumSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostForum() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumSession getPostForumSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostForumSession not implemented");
    }


    /**
     *  Gets the session for retrieving post to forum mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostForumSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPostForum() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumSession getPostForumSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostForumSession not implemented");
    }


    /**
     *  Gets the session for assigning post to forum mappings. 
     *
     *  @return a <code> PostForumAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostForumAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumAssignmentSession getPostForumAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostForumAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning post to forum mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PostForumAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostForumAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostForumAssignmentSession getPostForumAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostForumAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the post smart forum for the given 
     *  forum. 
     *
     *  @param  forumId the <code> Id </code> of the forum 
     *  @return a <code> PostSmartForumSession </code> 
     *  @throws org.osid.NotFoundException <code> forumId </code> not found 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartForum() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSmartForumSession getPostSmartForumSession(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getPostSmartForumSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic post forums for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of a forum 
     *  @param  proxy a proxy 
     *  @return <code> forumId </code> not found 
     *  @throws org.osid.NotFoundException <code> forumId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostSmartForum() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostSmartForumSession getPostSmartForumSession(org.osid.id.Id forumId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getPostSmartForumSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service. 
     *
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyLookupSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply lookup 
     *  service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ReplyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyLookupSession getReplyLookupSessionForForum(org.osid.id.Id forumId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyLookupSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service. 
     *
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyAdminSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ReplyAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReplyAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyAdminSession getReplyAdminSessionForForum(org.osid.id.Id forumId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyAdminSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service. 
     *
     *  @param  replyReceiver the receiver 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSession(org.osid.forum.ReplyReceiver replyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service. 
     *
     *  @param  replyReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSession(org.osid.forum.ReplyReceiver replyReceiver, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service for the given forum. 
     *
     *  @param  replyReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver </code> or 
     *          <code> forumId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSessionForForum(org.osid.forum.ReplyReceiver replyReceiver, 
                                                                                       org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getReplyNotificationSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the reply 
     *  notification service for the given forum. 
     *
     *  @param  replyReceiver the receiver 
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ReplyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> replyReceiver, forumId, 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyNotificationSession getReplyNotificationSessionForForum(org.osid.forum.ReplyReceiver replyReceiver, 
                                                                                       org.osid.id.Id forumId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getReplyNotificationSessionForForum not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum lookup 
     *  service. 
     *
     *  @return a <code> ForumLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumLookupSession getForumLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumLookupSession getForumLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum query 
     *  service. 
     *
     *  @return a <code> ForumQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuerySession getForumQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuerySession getForumQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum search 
     *  service. 
     *
     *  @return a <code> ForumSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumSearchSession getForumSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumSearchSession getForumSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  administrative service. 
     *
     *  @return a <code> ForumAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumAdminSession getForumAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumAdminSession getForumAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  notification service. 
     *
     *  @param  forumReceiver the receiver 
     *  @return a <code> ForumNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> forumReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumNotificationSession getForumNotificationSession(org.osid.forum.ForumReceiver forumReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  notification service. 
     *
     *  @param  forumReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ForumNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> forumReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumNotificationSession getForumNotificationSession(org.osid.forum.ForumReceiver forumReceiver, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy service. 
     *
     *  @return a <code> ForumHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchySession getForumHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchySession getForumHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy design service. 
     *
     *  @return a <code> ForumHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchyDesignSession getForumHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the forum 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ForumHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumHierarchyDesignSession getForumHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> ForumBatchManager. </code> 
     *
     *  @return a <code> ForumBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ForumBatchManager getForumBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumManager.getForumBatchManager not implemented");
    }


    /**
     *  Gets a <code> ForumBatchProxyManager. </code> 
     *
     *  @return a <code> ForumBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ForumBatchProxyManager getForumBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.forum.ForumProxyManager.getForumBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.postRecordTypes.clear();
        this.postRecordTypes.clear();

        this.postSearchRecordTypes.clear();
        this.postSearchRecordTypes.clear();

        this.replyRecordTypes.clear();
        this.replyRecordTypes.clear();

        this.replySearchRecordTypes.clear();
        this.replySearchRecordTypes.clear();

        this.forumRecordTypes.clear();
        this.forumRecordTypes.clear();

        this.forumSearchRecordTypes.clear();
        this.forumSearchRecordTypes.clear();

        return;
    }
}
