//
// AbstractAdapterSettingLookupSession.java
//
//    A Setting lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Setting lookup session adapter.
 */

public abstract class AbstractAdapterSettingLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.SettingLookupSession {

    private final org.osid.control.SettingLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSettingLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSettingLookupSession(org.osid.control.SettingLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code Setting} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSettings() {
        return (this.session.canLookupSettings());
    }


    /**
     *  A complete view of the {@code Setting} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSettingView() {
        this.session.useComparativeSettingView();
        return;
    }


    /**
     *  A complete view of the {@code Setting} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySettingView() {
        this.session.usePlenarySettingView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include settings in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    
     
    /**
     *  Gets the {@code Setting} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Setting} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Setting} and
     *  retained for compatibility.
     *
     *  @param settingId {@code Id} of the {@code Setting}
     *  @return the setting
     *  @throws org.osid.NotFoundException {@code settingId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code settingId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting(org.osid.id.Id settingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSetting(settingId));
    }


    /**
     *  Gets a {@code SettingList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  settings specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Settings} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  settingIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Setting} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code settingIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByIds(org.osid.id.IdList settingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettingsByIds(settingIds));
    }


    /**
     *  Gets a {@code SettingList} corresponding to the given
     *  setting genus {@code Type} which does not include
     *  settings of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned {@code Setting} list
     *  @throws org.osid.NullArgumentException
     *          {@code settingGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettingsByGenusType(settingGenusType));
    }


    /**
     *  Gets a {@code SettingList} corresponding to the given
     *  setting genus {@code Type} and include any additional
     *  settings with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned {@code Setting} list
     *  @throws org.osid.NullArgumentException
     *          {@code settingGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByParentGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettingsByParentGenusType(settingGenusType));
    }


    /**
     *  Gets a {@code SettingList} containing the given
     *  setting record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  settingRecordType a setting record type 
     *  @return the returned {@code Setting} list
     *  @throws org.osid.NullArgumentException
     *          {@code settingRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByRecordType(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettingsByRecordType(settingRecordType));
    }


    /**
     *  Gets a list of settings for a controller.
     *  
     *  In plenary mode, the returned list contains all known settings
     *  or an error results. Otherwise, the returned list may contain
     *  only those settings that are accessible through this session.
     *
     *  @param  controllerId a controller {@code Id} 
     *  @return the returned {@code Setting} list 
     *  @throws org.osid.NullArgumentException {@code controllerId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsForController(org.osid.id.Id controllerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettingsForController(controllerId));
    }


    /**
     *  Gets all {@code Settings}. 
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Settings} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSettings());
    }
}
