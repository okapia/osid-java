//
// MutableMapCookbookLookupSession
//
//    Implements a Cookbook lookup service backed by a collection of
//    cookbooks that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Cookbook lookup service backed by a collection of
 *  cookbooks. The cookbooks are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of cookbooks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapCookbookLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapCookbookLookupSession
    implements org.osid.recipe.CookbookLookupSession {


    /**
     *  Constructs a new {@code MutableMapCookbookLookupSession}
     *  with no cookbooks.
     */

    public MutableMapCookbookLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCookbookLookupSession} with a
     *  single cookbook.
     *  
     *  @param cookbook a cookbook
     *  @throws org.osid.NullArgumentException {@code cookbook}
     *          is {@code null}
     */

    public MutableMapCookbookLookupSession(org.osid.recipe.Cookbook cookbook) {
        putCookbook(cookbook);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCookbookLookupSession}
     *  using an array of cookbooks.
     *
     *  @param cookbooks an array of cookbooks
     *  @throws org.osid.NullArgumentException {@code cookbooks}
     *          is {@code null}
     */

    public MutableMapCookbookLookupSession(org.osid.recipe.Cookbook[] cookbooks) {
        putCookbooks(cookbooks);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapCookbookLookupSession}
     *  using a collection of cookbooks.
     *
     *  @param cookbooks a collection of cookbooks
     *  @throws org.osid.NullArgumentException {@code cookbooks}
     *          is {@code null}
     */

    public MutableMapCookbookLookupSession(java.util.Collection<? extends org.osid.recipe.Cookbook> cookbooks) {
        putCookbooks(cookbooks);
        return;
    }

    
    /**
     *  Makes a {@code Cookbook} available in this session.
     *
     *  @param cookbook a cookbook
     *  @throws org.osid.NullArgumentException {@code cookbook{@code  is
     *          {@code null}
     */

    @Override
    public void putCookbook(org.osid.recipe.Cookbook cookbook) {
        super.putCookbook(cookbook);
        return;
    }


    /**
     *  Makes an array of cookbooks available in this session.
     *
     *  @param cookbooks an array of cookbooks
     *  @throws org.osid.NullArgumentException {@code cookbooks{@code 
     *          is {@code null}
     */

    @Override
    public void putCookbooks(org.osid.recipe.Cookbook[] cookbooks) {
        super.putCookbooks(cookbooks);
        return;
    }


    /**
     *  Makes collection of cookbooks available in this session.
     *
     *  @param cookbooks a collection of cookbooks
     *  @throws org.osid.NullArgumentException {@code cookbooks{@code  is
     *          {@code null}
     */

    @Override
    public void putCookbooks(java.util.Collection<? extends org.osid.recipe.Cookbook> cookbooks) {
        super.putCookbooks(cookbooks);
        return;
    }


    /**
     *  Removes a Cookbook from this session.
     *
     *  @param cookbookId the {@code Id} of the cookbook
     *  @throws org.osid.NullArgumentException {@code cookbookId{@code 
     *          is {@code null}
     */

    @Override
    public void removeCookbook(org.osid.id.Id cookbookId) {
        super.removeCookbook(cookbookId);
        return;
    }    
}
