//
// AbstractAssemblyCustomerQuery.java
//
//     A CustomerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CustomerQuery that stores terms.
 */

public abstract class AbstractAssemblyCustomerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyTemporalOsidObjectQuery
    implements org.osid.billing.CustomerQuery,
               org.osid.billing.CustomerQueryInspector,
               org.osid.billing.CustomerSearchOrder {

    private final java.util.Collection<org.osid.billing.records.CustomerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.CustomerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.records.CustomerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCustomerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCustomerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches customer numbers. 
     *
     *  @param  number a customer number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerNumber(String number, 
                                    org.osid.type.Type stringMatchType, 
                                    boolean match) {
        getAssembler().addStringTerm(getCustomerNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches customers with any number. 
     *
     *  @param  match <code> true </code> to match customers with any number, 
     *          <code> false </code> to match customers with no number 
     */

    @OSID @Override
    public void matchAnyCustomerNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getCustomerNumberColumn(), match);
        return;
    }


    /**
     *  Clears the customer number. 
     */

    @OSID @Override
    public void clearCustomerNumberTerms() {
        getAssembler().clearTerms(getCustomerNumberColumn());
        return;
    }


    /**
     *  Gets the customer number terms. 
     *
     *  @return the customer number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCustomerNumberTerms() {
        return (getAssembler().getStringTerms(getCustomerNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the customer 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomerNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerNumberColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerNumber column name.
     *
     * @return the column name
     */

    protected String getCustomerNumberColumn() {
        return ("customer_number");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match customers 
     *  that have a financial activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return an activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches customers that have any financial activity. 
     *
     *  @param  match <code> true </code> to match customers with any 
     *          activity, <code> false </code> to match customers with no 
     *          activity 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the activity, 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match customers 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this customer supports the given record
     *  <code>Type</code>.
     *
     *  @param  customerRecordType a customer record type 
     *  @return <code>true</code> if the customerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type customerRecordType) {
        for (org.osid.billing.records.CustomerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(customerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  customerRecordType the customer record type 
     *  @return the customer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerQueryRecord getCustomerQueryRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CustomerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(customerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  customerRecordType the customer record type 
     *  @return the customer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerQueryInspectorRecord getCustomerQueryInspectorRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CustomerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(customerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param customerRecordType the customer record type
     *  @return the customer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(customerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CustomerSearchOrderRecord getCustomerSearchOrderRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CustomerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(customerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(customerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this customer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param customerQueryRecord the customer query record
     *  @param customerQueryInspectorRecord the customer query inspector
     *         record
     *  @param customerSearchOrderRecord the customer search order record
     *  @param customerRecordType customer record type
     *  @throws org.osid.NullArgumentException
     *          <code>customerQueryRecord</code>,
     *          <code>customerQueryInspectorRecord</code>,
     *          <code>customerSearchOrderRecord</code> or
     *          <code>customerRecordTypecustomer</code> is
     *          <code>null</code>
     */
            
    protected void addCustomerRecords(org.osid.billing.records.CustomerQueryRecord customerQueryRecord, 
                                      org.osid.billing.records.CustomerQueryInspectorRecord customerQueryInspectorRecord, 
                                      org.osid.billing.records.CustomerSearchOrderRecord customerSearchOrderRecord, 
                                      org.osid.type.Type customerRecordType) {

        addRecordType(customerRecordType);

        nullarg(customerQueryRecord, "customer query record");
        nullarg(customerQueryInspectorRecord, "customer query inspector record");
        nullarg(customerSearchOrderRecord, "customer search odrer record");

        this.queryRecords.add(customerQueryRecord);
        this.queryInspectorRecords.add(customerQueryInspectorRecord);
        this.searchOrderRecords.add(customerSearchOrderRecord);
        
        return;
    }
}
