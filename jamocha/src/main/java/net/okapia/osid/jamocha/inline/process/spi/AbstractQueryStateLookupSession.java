//
// AbstractQueryStateLookupSession.java
//
//    An inline adapter that maps a StateLookupSession to
//    a StateQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StateLookupSession to
 *  a StateQuerySession.
 */

public abstract class AbstractQueryStateLookupSession
    extends net.okapia.osid.jamocha.process.spi.AbstractStateLookupSession
    implements org.osid.process.StateLookupSession {

    private final org.osid.process.StateQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStateLookupSession.
     *
     *  @param querySession the underlying state query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStateLookupSession(org.osid.process.StateQuerySession querySession) {
        nullarg(querySession, "state query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Process</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Process Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.session.getProcessId());
    }


    /**
     *  Gets the <code>Process</code> associated with this 
     *  session.
     *
     *  @return the <code>Process</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getProcess());
    }


    /**
     *  Tests if this user can perform <code>State</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStates() {
        return (this.session.canSearchStates());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include states in processes which are children
     *  of this process in the process hierarchy.
     */

    @OSID @Override
    public void useFederatedProcessView() {
        this.session.useFederatedProcessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this process only.
     */

    @OSID @Override
    public void useIsolatedProcessView() {
        this.session.useIsolatedProcessView();
        return;
    }
    
     
    /**
     *  Gets the <code>State</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>State</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>State</code> and
     *  retained for compatibility.
     *
     *  @param  stateId <code>Id</code> of the
     *          <code>State</code>
     *  @return the state
     *  @throws org.osid.NotFoundException <code>stateId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stateId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.State getState(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.process.StateQuery query = getQuery();
        query.matchId(stateId, true);
        org.osid.process.StateList states = this.session.getStatesByQuery(query);
        if (states.hasNext()) {
            return (states.getNextState());
        } 
        
        throw new org.osid.NotFoundException(stateId + " not found");
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  states specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>States</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  stateIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stateIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByIds(org.osid.id.IdList stateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.process.StateQuery query = getQuery();

        try (org.osid.id.IdList ids = stateIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStatesByQuery(query));
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> which does not include
     *  states of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.process.StateQuery query = getQuery();
        query.matchGenusType(stateGenusType, true);
        return (this.session.getStatesByQuery(query));
    }


    /**
     *  Gets a <code>StateList</code> corresponding to the given
     *  state genus <code>Type</code> and include any additional
     *  states with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateGenusType a state genus type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByParentGenusType(org.osid.type.Type stateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.process.StateQuery query = getQuery();
        query.matchParentGenusType(stateGenusType, true);
        return (this.session.getStatesByQuery(query));
    }


    /**
     *  Gets a <code>StateList</code> containing the given
     *  state record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stateRecordType a state record type 
     *  @return the returned <code>State</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stateRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStatesByRecordType(org.osid.type.Type stateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.process.StateQuery query = getQuery();
        query.matchRecordType(stateRecordType, true);
        return (this.session.getStatesByQuery(query));
    }

    
    /**
     *  Gets all <code>States</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  states or an error results. Otherwise, the returned list
     *  may contain only those states that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>States</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.process.StateQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStatesByQuery(query));
    }


    /**
     *  Gets the next valid states for the given state. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @return the valid next <code> States </code> 
     *  @throws org.osid.NotFoundException <code> stateId </code> is not found 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.process.StateList getValidNextStates(org.osid.id.Id stateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.nil.process.state.EmptyStateList());
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.process.StateQuery getQuery() {
        org.osid.process.StateQuery query = this.session.getStateQuery();
        
        return (query);
    }
}
