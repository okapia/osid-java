//
// AbstractWorkflowEventLookupSession.java
//
//    A starter implementation framework for providing a WorkflowEvent
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a WorkflowEvent
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getWorkflowEvents(), this other methods may need to be overridden
 *  for better performance.
 */

public abstract class AbstractWorkflowEventLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.WorkflowEventLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this session.
     *
     *  @return the <code>Office</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>WorkflowEvent</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorkflowEvents() {
        return (true);
    }


    /**
     *  A complete view of the <code>WorkflowEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkflowEventView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>WorkflowEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkflowEventView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include workflow events in offices which are
     *  children of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>WorkflowEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>WorkflowEvent</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>WorkflowEvent</code> and retained for compatibility.
     *
     *  @param workflowEventId <code>Id</code> of the
     *          <code>WorkflowEvent</code>
     *  @return the workflow event
     *  @throws org.osid.NotFoundException <code>workflowEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workflowEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEvent getWorkflowEvent(org.osid.id.Id workflowEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.WorkflowEventList workflowEvents = getWorkflowEvents()) {
            while (workflowEvents.hasNext()) {
                org.osid.workflow.WorkflowEvent workflowEvent = workflowEvents.getNextWorkflowEvent();
                if (workflowEvent.getId().equals(workflowEventId)) {
                    return (workflowEvent);
                }
            }
        } 

        throw new org.osid.NotFoundException(workflowEventId + " not found");
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  workflowEvents specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>WorkflowEvents</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getWorkflowEvents()</code>.
     *
     *  @param  workflowEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByIds(org.osid.id.IdList workflowEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.WorkflowEvent> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = workflowEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getWorkflowEvent(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("workflow event " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.workflowevent.LinkedWorkflowEventList(ret));
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the
     *  given workflow event genus <code>Type</code> which does not
     *  include workflow events of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getWorkflowEvents()</code>.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventGenusFilterList(getWorkflowEvents(), workflowEventGenusType));
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the
     *  given workflow event genus <code>Type</code> and include any
     *  additional workflow events with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorkflowEvents()</code>.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByParentGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWorkflowEventsByGenusType(workflowEventGenusType));
    }


    /**
     *  Gets a <code>WorkflowEventList</code> containing the given
     *  workflow event record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorkflowEvents()</code>.
     *
     *  @param  workflowEventRecordType a workflowEvent record type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByRecordType(org.osid.type.Type workflowEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventRecordFilterList(getWorkflowEvents(), workflowEventRecordType));
    }


    /**
     *  Gets the entire workflow log within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code> from</code> is 
     *          greater than <code> to</code> 
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDate(org.osid.calendaring.DateTime from, 
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEvents()));
    }


    /**
     *  Gets the entire workflow log for a process. In plenary mode,
     *  the returned list contains all known workflow events or an
     *  error results.  Otherwise, the returned list may contain only
     *  those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>processId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new ProcessFilter(processId), getWorkflowEvents()));
    }



    /**
     *  Gets the entire workflow log for this process within the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  processId a process <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>processId</code>,
     *         <code>from</code>, or <code>to</code> is
     *         <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForProcess(org.osid.id.Id processId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForProcess(processId)));
    }


    /**
     *  Gets the workflow log for a step. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>stepId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStep(org.osid.id.Id stepId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new StepFilter(stepId), getWorkflowEvents()));
    }


    /**
     *  Gets the workflow log for a step within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  stepId a step <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>stepId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStep(org.osid.id.Id stepId, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForStep(stepId)));
    }


    /**
     *  Gets the workflow log for a work. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  workId a work <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWork(org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new WorkFilter(workId), getWorkflowEvents()));
    }



    /**
     *  Gets the workflow log for a work within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  workId a work <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWork(org.osid.id.Id workId, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForWork(workId)));
    }



    /**
     *  Gets the workflow log for a work in a process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process <code>Id</code> 
     *  @param  workId a work <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkAndProcess(org.osid.id.Id processId, 
                                                                                  org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new ProcessFilter(processId), getWorkflowEventsForWork(workId)));
    }



    /**
     *  Gets the workflow log for a work in a process within the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  processId a process <code>Id</code> 
     *  @param  workId a work <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkAndProcess(org.osid.id.Id processId, 
                                                                                        org.osid.id.Id workId, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForWorkAndProcess(workId, processId)));
    }


    /**
     *  Gets the workflow log for a work in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  stepId a step <code>Id</code> 
     *  @param  workId a work <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>stepId</code> or
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStepAndWork(org.osid.id.Id stepId, 
                                                                               org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new StepFilter(stepId), getWorkflowEventsForWork(workId)));
    }


    /**
     *  Gets the workflow log for a work in this process within the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step <code>Id</code> 
     *  @param  workId a work <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>stepId</code>,
     *          <code>workId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStepAndWork(org.osid.id.Id stepId, 
                                                                                     org.osid.id.Id workId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForStepAndWork(stepId, workId)));
    }



    /**
     *  Gets the workflow log by an agent in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results. Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new WorkerFilter(resourceId), getWorkflowEvents()));
    }


    /**
     *  Gets the workflow log by the resource in this process within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException <code>resourceId</code>, <code>from</code>
     *        > or <code>to</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorker(org.osid.id.Id resourceId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForWorker(resourceId)));
    }


    /**
     *  Gets the workflow log by an agent in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results. Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  processId a process <code>Id</code> 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException <code>resourceId</code> or 
     *          <code>processId</code> is <code>null</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkerAndProcess(org.osid.id.Id resourceId, 
                                                                                    org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new WorkerFilter(resourceId), getWorkflowEventsForProcess(processId)));
    }


    /**
     *  Gets the workflow log by the resource in this process within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  processId a process <code>Id</code> 
     *  @param  from start range 
     *  @param  to end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is 
     *          greater than <code>to</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>processId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkerAndProcess(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id processId, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilterList(new TimestampFilter(from, to), getWorkflowEventsForWorkerAndProcess(resourceId, processId)));
    }


    /**
     *  Gets all <code>WorkflowEvents</code>.
     *
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>WorkflowEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.WorkflowEventList getWorkflowEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the workflow event list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of workflow events
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.WorkflowEventList filterWorkflowEventsOnViews(org.osid.workflow.WorkflowEventList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ProcessFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilter {
         
        private final org.osid.id.Id processId;
         
         
        /**
         *  Constructs a new <code>ProcessFilter</code>.
         *
         *  @param processId the process to filter
         *  @throws org.osid.NullArgumentException
         *          <code>processId</code> is <code>null</code>
         */
        
        public ProcessFilter(org.osid.id.Id processId) {
            nullarg(processId, "process Id");
            this.processId = processId;
            return;
        }

         
        /**
         *  Used by the WorkflowEventFilterList to filter the workflow
         *  event list based on process.
         *
         *  @param workflowEvent the workflow event
         *  @return <code>true</code> to pass the workflow event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.WorkflowEvent workflowEvent) {
            return (workflowEvent.getProcessId().equals(this.processId));
        }
    }


    public static class WorkFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilter {
         
        private final org.osid.id.Id workId;
         
         
        /**
         *  Constructs a new <code>WorkFilter</code>.
         *
         *  @param workId the work to filter
         *  @throws org.osid.NullArgumentException
         *          <code>workId</code> is <code>null</code>
         */
        
        public WorkFilter(org.osid.id.Id workId) {
            nullarg(workId, "work Id");
            this.workId = workId;
            return;
        }

         
        /**
         *  Used by the WorkflowEventFilterList to filter the workflow
         *  event list based on work.
         *
         *  @param workflowEvent the workflow event
         *  @return <code>true</code> to pass the workflow event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.WorkflowEvent workflowEvent) {
            return (workflowEvent.getWorkId().equals(this.workId));
        }
    }


    public static class StepFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilter {
         
        private final org.osid.id.Id stepId;
         
         
        /**
         *  Constructs a new <code>StepFilter</code>.
         *
         *  @param stepId the step to filter
         *  @throws org.osid.NullArgumentException
         *          <code>stepId</code> is <code>null</code>
         */
        
        public StepFilter(org.osid.id.Id stepId) {
            nullarg(stepId, "step Id");
            this.stepId = stepId;
            return;
        }

         
        /**
         *  Used by the WorkflowEventFilterList to filter the workflow
         *  event list based on step.
         *
         *  @param workflowEvent the workflow event
         *  @return <code>true</code> to pass the workflow event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.WorkflowEvent workflowEvent) {
            return (workflowEvent.getStepId().equals(this.stepId));
        }
    }


    public static class WorkerFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>WorkerFilter</code>.
         *
         *  @param resourceId the worker to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public WorkerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "worker Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the WorkflowEventFilterList to filter the workflow
         *  event list based on worker.
         *
         *  @param workflowEvent the workflow event
         *  @return <code>true</code> to pass the workflow event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.WorkflowEvent workflowEvent) {
            return (workflowEvent.getWorkerId().equals(this.resourceId));
        }
    }


    public static class TimestampFilter
        implements net.okapia.osid.jamocha.inline.filter.workflow.workflowevent.WorkflowEventFilter {
         
        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;
         
         
        /**
         *  Constructs a new <code>TimestampFilter</code>.
         *
         *  @param from the start date
         *  @param to the end date
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        public TimestampFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "start date");
            nullarg(to, "end date");

            this.from = from;
            this.to = to;

            return;
        }

         
        /**
         *  Used by the WorkflowEventFilterList to filter the workflow
         *  event list based on date.
         *
         *  @param workflowEvent the workflow event
         *  @return <code>true</code> to pass the workflow event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.workflow.WorkflowEvent workflowEvent) {
            if (workflowEvent.getTimestamp().isLess(this.from)) {
                return (false);
            }

            if (workflowEvent.getTimestamp().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }
}
