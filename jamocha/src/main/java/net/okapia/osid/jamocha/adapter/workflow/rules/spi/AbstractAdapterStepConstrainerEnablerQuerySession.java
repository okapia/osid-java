//
// AbstractQueryStepConstrainerEnablerLookupSession.java
//
//    A StepConstrainerEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StepConstrainerEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterStepConstrainerEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.rules.StepConstrainerEnablerQuerySession {

    private final org.osid.workflow.rules.StepConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterStepConstrainerEnablerQuerySession.
     *
     *  @param session the underlying step constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepConstrainerEnablerQuerySession(org.osid.workflow.rules.StepConstrainerEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeOffice</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@codeOffice</code> associated with this 
     *  session.
     *
     *  @return the {@codeOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@codeStepConstrainerEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchStepConstrainerEnablers() {
        return (this.session.canSearchStepConstrainerEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainer enablers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this office only.
     */
    
    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    
      
    /**
     *  Gets a step constrainer enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the step constrainer enabler query 
     */
      
    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuery getStepConstrainerEnablerQuery() {
        return (this.session.getStepConstrainerEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  stepConstrainerEnablerQuery the step constrainer enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code stepConstrainerEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code stepConstrainerEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByQuery(org.osid.workflow.rules.StepConstrainerEnablerQuery stepConstrainerEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getStepConstrainerEnablersByQuery(stepConstrainerEnablerQuery));
    }
}
