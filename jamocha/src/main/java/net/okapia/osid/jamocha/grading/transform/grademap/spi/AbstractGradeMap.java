//
// AbstractGradeMap.java
//
//     Defines a GradeMap.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.grademap.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradeMap</code>.
 */

public abstract class AbstractGradeMap
    implements org.osid.grading.transform.GradeMap {

    private org.osid.grading.Grade source;
    private org.osid.grading.Grade target;
    
    
    /**
     *  Gets the source <code> Grade Id. </code> 
     *
     *  @return the source grade <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceGradeId() {
        return (this.source.getId());
    }


    /**
     *  Gets the source <code> Grade. </code> 
     *
     *  @return the source grade 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getSourceGrade()
        throws org.osid.OperationFailedException {
        
        return (this.source);
    }


    /**
     *  Sets the source grade.
     * 
     *  @param grade the source grade
     *  @throws org.osid.NUllArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    protected void setSourceGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "source grade");
        this.source = grade;
        return;
    }


    /**
     *  Gets the target <code> Grade Id. </code> 
     *
     *  @return the target grade <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTargetGradeId() {
        return (this.target.getId());
    }


    /**
     *  Gets the target <code> Grade. </code> 
     *
     *  @return the target grade 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getTargetGrade()
        throws org.osid.OperationFailedException {
        
        return (this.target);
    }


    /**
     *  Sets the target grade.
     * 
     *  @param grade the target grade
     *  @throws org.osid.NUllArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    protected void setTargetGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "target grade");
        this.target = grade;
        return;
    }


    /**
     *  Determines if the given <code> GradeMap </code> is equal to
     *  this one. Two <code>GradeMaps</code> are equal if their source
     *  and target grades are equal.
     *
     *  If <code>equals()</code> is <code>true</code>, then
     *  <code>compareTo()</code> must be zero and their hash codes
     *  must also be equal for consistent behavior. For orderings that
     *  may yield inconsistent behavior, an external
     *  <code>Comparator</code> should be used.
     *
     *  If <code>obj</code> is null or if a different interface, this
     *  method returns <code>false</code>.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>GradeMap</code>, <code> false </code>
     *          otherwise
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return (true);
        }

        if (obj == null) {
            return (false);
        }

        if (!(obj instanceof org.osid.grading.transform.GradeMap)) {
            return (false);
        }

        org.osid.grading.transform.GradeMap gm = (org.osid.grading.transform.GradeMap) obj;
        return (getSourceGradeId().equals(gm.getSourceGradeId()) && 
                getTargetGradeId().equals(gm.getTargetGradeId()));
    }


    /**
     *  Returns a hash code value for this <code>GradeMap</code>.  The
     *  hash code is determined by the source and target grade Ids.
     *
     *  @return a hash code value for this object
     */

    @Override
    public int hashCode() {
        return (this.source.hashCode() * 31 + this.target.hashCode());
    }
}
