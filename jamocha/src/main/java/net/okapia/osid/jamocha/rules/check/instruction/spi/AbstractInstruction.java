//
// AbstractInstruction.java
//
//     Defines an Instruction.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Instruction</code>.
 */

public abstract class AbstractInstruction
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.rules.check.Instruction {

    private org.osid.rules.check.Agenda agenda;
    private org.osid.rules.check.Check check;
    private org.osid.locale.DisplayText message = Plain.valueOf("");

    private boolean warning = false;
    private boolean continueOnFail = false;

    private final java.util.Collection<org.osid.rules.check.records.InstructionRecord> records = new java.util.LinkedHashSet<>();

    private final OsidRelationship relationship = new OsidRelationship();


    /**
     *  Tests if a reason this relationship came to an end is known. 
     *
     *  @return <code> true </code> if an end reason is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code>isEffective</code> is
     *          <code>true</code>
     */

    @OSID @Override
    public boolean hasEndReason() {
        return (this.relationship.hasEndReason());
    }


    /**
     *  Gets a state <code> Id </code> indicating why this relationship has 
     *  ended. 
     *
     *  @return a state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReasonId() {
        return (this.relationship.getEndReasonId());
    }


    /**
     *  Gets a state indicating why this relationship has ended. 
     *
     *  @return a state 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getEndReason()
        throws org.osid.OperationFailedException {

        return (this.relationship.getEndReason());
    }


    /**
     *  Sets the end state.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */
    
    protected void setEndReason(org.osid.process.State state) {
        this.relationship.setEndReason(state);
        return;
    }


    /**
     *  Gets the <code> Id </code> of the agenda. 
     *
     *  @return the agenda <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgendaId() {
        return (this.agenda.getId());
    }


    /**
     *  Gets the agenda. 
     *
     *  @return the agenda 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda()
        throws org.osid.OperationFailedException {

        return (this.agenda);
    }


    /**
     *  Sets the agenda.
     *
     *  @param agenda an agenda
     *  @throws org.osid.NullArgumentException
     *          <code>agenda</code> is <code>null</code>
     */

    protected void setAgenda(org.osid.rules.check.Agenda agenda) {
        nullarg(agenda, "agenda");
        this.agenda = agenda;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the check. 
     *
     *  @return the check <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCheckId() {
        return (this.check.getId());
    }


    /**
     *  Gets the check. 
     *
     *  @return the check 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Check getCheck()
        throws org.osid.OperationFailedException {

        return (this.check);
    }


    /**
     *  Sets the check.
     *
     *  @param check a check
     *  @throws org.osid.NullArgumentException
     *          <code>check</code> is <code>null</code>
     */

    protected void setCheck(org.osid.rules.check.Check check) {
        nullarg(check, "check");
        this.check = check;
        return;
    }


    /**
     *  Gets the message to be returned upon failure of the check evaluation. 
     *
     *  @return the message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.message);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    protected void setMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "message");
        this.message = message;
        return;
    }


    /**
     *  Tests if failure of the check should be interpreted as a warning and 
     *  not a failure. 
     *
     *  @return <code> true </code> if this is a warning, <code> false </code> 
     *          if an error 
     */

    @OSID @Override
    public boolean isWarning() {
        return (this.warning);
    }


    /**
     *  Sets the warning flag.
     *
     *  @param warning <code> true </code> if this is a warning,
     *          <code> false </code> if an error
     */

    protected void setWarning(boolean warning) {
        this.warning = warning;
        return;
    }


    /**
     *  Tests if evaluation of the next instruction should continue if
     *  the check in this instruction fails. While the overall
     *  evaluation of the agenda still fails, if true, allows for
     *  other messages to be gathered.
     *
     *  @return <code> true </code> if this processing should continue
     *          on failure, <code> false </code> if processing should
     *          cease upon failure
     */

    @OSID @Override
    public boolean continueOnFail() {
        return (this.continueOnFail);
    }


    /**
     *  Sets the continue on fail flag.
     *
     *  @param continueOnFail <code> true </code> if this processing
     *          should continue on failure, <code> false </code> if
     *          processing should cease upon failure
     */

    protected void setContinueOnFail(boolean continueOnFail) {
        this.continueOnFail = continueOnFail;
        return;
    }


    /**
     *  Tests if this instruction supports the given record
     *  <code>Type</code>.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return <code>true</code> if the instructionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type instructionRecordType) {
        for (org.osid.rules.check.records.InstructionRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Instruction</code> record <code>Type</code>.
     *
     *  @param  instructionRecordType the instruction record type 
     *  @return the instruction record 
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionRecord getInstructionRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionRecord record : this.records) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this instruction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param instructionRecord the instruction record
     *  @param instructionRecordType instruction record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecord</code> or
     *          <code>instructionRecordTypeinstruction</code> is
     *          <code>null</code>
     */
            
    protected void addInstructionRecord(org.osid.rules.check.records.InstructionRecord instructionRecord, 
                                        org.osid.type.Type instructionRecordType) {

        nullarg(instructionRecord, "instruction record");
        addRecordType(instructionRecordType);
        this.records.add(instructionRecord);
        
        return;
    }


    protected class OsidRelationship
        extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship {


        /**
         *  Sets the end state.
         *
         *  @param state the end state
         *  @throws org.osid.NullArgumentException <code>state</code> is
         *          <code>null</code>
         */
        
        protected void setEndReason(org.osid.process.State state) {
            super.setEndReason(state);
            return;
        }
    }
}
