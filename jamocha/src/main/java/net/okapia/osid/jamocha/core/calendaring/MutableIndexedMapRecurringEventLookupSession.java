//
// MutableIndexedMapRecurringEventLookupSession
//
//    Implements a RecurringEvent lookup service backed by a collection of
//    recurringEvents indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a RecurringEvent lookup service backed by a collection of
 *  recurring events. The recurring events are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some recurring events may be compatible
 *  with more types than are indicated through these recurring event
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of recurring events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRecurringEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRecurringEventLookupSession} with no recurring events.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

      public MutableIndexedMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventLookupSession} with a
     *  single recurring event.
     *  
     *  @param calendar the calendar
     *  @param  recurringEvent a single recurringEvent
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvent} is {@code null}
     */

    public MutableIndexedMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.RecurringEvent recurringEvent) {
        this(calendar);
        putRecurringEvent(recurringEvent);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventLookupSession} using an
     *  array of recurring events.
     *
     *  @param calendar the calendar
     *  @param  recurringEvents an array of recurring events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvents} is {@code null}
     */

    public MutableIndexedMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.RecurringEvent[] recurringEvents) {
        this(calendar);
        putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRecurringEventLookupSession} using a
     *  collection of recurring events.
     *
     *  @param calendar the calendar
     *  @param  recurringEvents a collection of recurring events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvents} is {@code null}
     */

    public MutableIndexedMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents) {

        this(calendar);
        putRecurringEvents(recurringEvents);
        return;
    }
    

    /**
     *  Makes a {@code RecurringEvent} available in this session.
     *
     *  @param  recurringEvent a recurring event
     *  @throws org.osid.NullArgumentException {@code recurringEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        super.putRecurringEvent(recurringEvent);
        return;
    }


    /**
     *  Makes an array of recurring events available in this session.
     *
     *  @param  recurringEvents an array of recurring events
     *  @throws org.osid.NullArgumentException {@code recurringEvents{@code 
     *          is {@code null}
     */

    @Override
    public void putRecurringEvents(org.osid.calendaring.RecurringEvent[] recurringEvents) {
        super.putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Makes collection of recurring events available in this session.
     *
     *  @param  recurringEvents a collection of recurring events
     *  @throws org.osid.NullArgumentException {@code recurringEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putRecurringEvents(java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents) {
        super.putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Removes a RecurringEvent from this session.
     *
     *  @param recurringEventId the {@code Id} of the recurring event
     *  @throws org.osid.NullArgumentException {@code recurringEventId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRecurringEvent(org.osid.id.Id recurringEventId) {
        super.removeRecurringEvent(recurringEventId);
        return;
    }    
}
