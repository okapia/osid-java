//
// SpatialUnitMetadata.java
//
//     Defines a spatial unit Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a spatial unit Metadata.
 */

public final class SpatialUnitMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractSpatialUnitMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code SpatialUnitMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public SpatialUnitMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code SpatialUnitMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public SpatialUnitMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code SpatialUnitMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public SpatialUnitMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Add support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType the type of spatial unit record
     *  @throws org.osid.NullArgumentException {@code
     *          spatialUnitRecordType} is {@code null}
     */

    public void addSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        super.addSpatialUnitRecordType(spatialUnitRecordType);
        return;
    }

    
    /**
     *  Sets the spatial unit set.
     *
     *  @param values a collection of accepted spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setSpatialUnitSet(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.setSpatialUnitSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the spatial unit set.
     *
     *  @param values a collection of accepted spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToSpatialUnitSet(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.addToSpatialUnitSet(values);
        return;
    }


    /**
     *  Adds a value to the spatial unit set.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToSpatialUnitSet(org.osid.mapping.SpatialUnit value) {
        super.addToSpatialUnitSet(value);
        return;
    }


    /**
     *  Removes a value from the spatial unit set.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromSpatialUnitSet(org.osid.mapping.SpatialUnit value) {
        super.removeFromSpatialUnitSet(value);
        return;
    }


    /**
     *  Clears the spatial unit set.
     */

    public void clearSpatialUnitSet() {
        super.clearSpatialUnitSet();
        return;
    }


    /**
     *  Sets the default spatial unit set.
     *
     *  @param values a collection of default spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.setDefaultSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a collection of default spatial unit values.
     *
     *  @param values a collection of default spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.addDefaultSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a default spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        super.addDefaultSpatialUnitValue(value);
        return;
    }


    /**
     *  Removes a default spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        super.removeDefaultSpatialUnitValue(value);
        return;
    }


    /**
     *  Clears the default spatial unit values.
     */

    public void clearDefaultSpatialUnitValues() {
        super.clearDefaultSpatialUnitValues();
        return;
    }


    /**
     *  Sets the existing spatial unit set.
     *
     *  @param values a collection of existing spatial unit values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.setExistingSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a collection of existing spatial unit values.
     *
     *  @param values a collection of existing spatial unit values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingSpatialUnitValues(java.util.Collection<org.osid.mapping.SpatialUnit> values) {
        super.addExistingSpatialUnitValues(values);
        return;
    }


    /**
     *  Adds a existing spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        super.addExistingSpatialUnitValue(value);
        return;
    }


    /**
     *  Removes a existing spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        super.removeExistingSpatialUnitValue(value);
        return;
    }


    /**
     *  Clears the existing spatial unit values.
     */

    public void clearExistingSpatialUnitValues() {
        super.clearExistingSpatialUnitValues();
        return;
    }    
}
