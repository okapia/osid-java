//
// AbstractQueryPathLookupSession.java
//
//    An inline adapter that maps a PathLookupSession to
//    a PathQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PathLookupSession to
 *  a PathQuerySession.
 */

public abstract class AbstractQueryPathLookupSession
    extends net.okapia.osid.jamocha.mapping.path.spi.AbstractPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {

    private final org.osid.mapping.path.PathQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPathLookupSession.
     *
     *  @param querySession the underlying path query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPathLookupSession(org.osid.mapping.path.PathQuerySession querySession) {
        nullarg(querySession, "path query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Map</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform <code>Path</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (this.session.canSearchPaths());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    
     
    /**
     *  Gets the <code>Path</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Path</code> and
     *  retained for compatibility.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();
        query.matchId(pathId, true);
        org.osid.mapping.path.PathList paths = this.session.getPathsByQuery(query);
        if (paths.hasNext()) {
            return (paths.getNextPath());
        } 
        
        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pathIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();
        query.matchGenusType(pathGenusType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> and include any additional
     *  paths with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();
        query.matchParentGenusType(pathGenusType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();
        query.matchRecordType(pathRecordType, true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets a <code> PathList </code> connected to all the given <code> 
     *  Locations. </code> In plenary mode, the returned list contains all of 
     *  the paths along the locations, or an error results if a path connected 
     *  to the location is not found or inaccessible. Otherwise, inaccessible 
     *  <code> Paths </code> may be omitted from the list. 
     *
     *  @param  locationIds the list of <code> Ids </code> to retrieve 
     *  @return the returned <code> Path </code> list 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.nil.mapping.path.path.EmptyPathList());
    }

    
    /**
     *  Gets all <code>Paths</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.mapping.path.PathQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPathsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.mapping.path.PathQuery getQuery() {
        org.osid.mapping.path.PathQuery query = this.session.getPathQuery();
        return (query);
    }
}
