//
// AbstractInputQuery.java
//
//     A template for making an Input Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.input.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for inputs.
 */

public abstract class AbstractInputQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.control.InputQuery {

    private final java.util.Collection<org.osid.control.records.InputQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the device <code> Id </code> for this query. 
     *
     *  @param  deviceId the device <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> deviceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDeviceId(org.osid.id.Id deviceId, boolean match) {
        return;
    }


    /**
     *  Clears the device <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDeviceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DeviceQuery </code> is available. 
     *
     *  @return <code> true </code> if a device query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a device. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the device query 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuery getDeviceQuery() {
        throw new org.osid.UnimplementedException("supportsDeviceQuery() is false");
    }


    /**
     *  Clears the device query terms. 
     */

    @OSID @Override
    public void clearDeviceTerms() {
        return;
    }


    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId the controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> ControllerQuery. </code> Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query to match inputs 
     *  assigned to systems. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sustemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given input query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an input implementing the requested record.
     *
     *  @param inputRecordType an input record type
     *  @return the input query record
     *  @throws org.osid.NullArgumentException
     *          <code>inputRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inputRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.InputQueryRecord getInputQueryRecord(org.osid.type.Type inputRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.InputQueryRecord record : this.records) {
            if (record.implementsRecordType(inputRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inputRecordType + " is not supported");
    }


    /**
     *  Adds a record to this input query. 
     *
     *  @param inputQueryRecord input query record
     *  @param inputRecordType input record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInputQueryRecord(org.osid.control.records.InputQueryRecord inputQueryRecord, 
                                          org.osid.type.Type inputRecordType) {

        addRecordType(inputRecordType);
        nullarg(inputQueryRecord, "input query record");
        this.records.add(inputQueryRecord);        
        return;
    }
}
