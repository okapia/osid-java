//
// AbstractQueryAppointmentLookupSession.java
//
//    An inline adapter that maps an AppointmentLookupSession to
//    an AppointmentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AppointmentLookupSession to
 *  an AppointmentQuerySession.
 */

public abstract class AbstractQueryAppointmentLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractAppointmentLookupSession
    implements org.osid.personnel.AppointmentLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.personnel.AppointmentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAppointmentLookupSession.
     *
     *  @param querySession the underlying appointment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAppointmentLookupSession(org.osid.personnel.AppointmentQuerySession querySession) {
        nullarg(querySession, "appointment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Realm</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Realm Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the <code>Realm</code> associated with this session.
     *
     *  @return the <code>Realm</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform <code>Appointment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAppointments() {
        return (this.session.canSearchAppointments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include appointments in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    

    /**
     *  Only appointments whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveAppointmentView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All appointments of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveAppointmentView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Appointment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Appointment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Appointment</code> and
     *  retained for compatibility.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentId <code>Id</code> of the
     *          <code>Appointment</code>
     *  @return the appointment
     *  @throws org.osid.NotFoundException <code>appointmentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>appointmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Appointment getAppointment(org.osid.id.Id appointmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchId(appointmentId, true);
        org.osid.personnel.AppointmentList appointments = this.session.getAppointmentsByQuery(query);
        if (appointments.hasNext()) {
            return (appointments.getNextAppointment());
        } 
        
        throw new org.osid.NotFoundException(appointmentId + " not found");
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  appointments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Appointments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, appointments are returned that are currently effective.
     *  In any effective mode, effective appointments and those currently expired
     *  are returned.
     *
     *  @param  appointmentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByIds(org.osid.id.IdList appointmentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();

        try (org.osid.id.IdList ids = appointmentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  appointment genus <code>Type</code> which does not include
     *  appointments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently effective.
     *  In any effective mode, effective appointments and those currently expired
     *  are returned.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchGenusType(appointmentGenusType, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets an <code>AppointmentList</code> corresponding to the given
     *  appointment genus <code>Type</code> and include any additional
     *  appointments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentGenusType an appointment genus type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByParentGenusType(org.osid.type.Type appointmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchParentGenusType(appointmentGenusType, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets an <code>AppointmentList</code> containing the given
     *  appointment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  appointmentRecordType an appointment record type 
     *  @return the returned <code>Appointment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsByRecordType(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchRecordType(appointmentRecordType, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets an <code>AppointmentList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *  
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Appointment</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAppointmentsByQuery(query));
    }
        

    /**
     *  Gets a list of appointments corresponding to a person
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.personnel.AppointmentList getAppointmentsForPerson(org.osid.id.Id personId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPersonId(personId, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets a list of appointments corresponding to a person
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonOnDate(org.osid.id.Id personId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPersonId(personId, true);
        query.matchDate(from, to, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets a list of appointments corresponding to a position
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>positionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.personnel.AppointmentList getAppointmentsForPosition(org.osid.id.Id positionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPositionId(positionId, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets a list of appointments corresponding to a position
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  positionId the <code>Id</code> of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>positionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPositionOnDate(org.osid.id.Id positionId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPositionId(positionId, true);
        query.matchDate(from, to, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets a list of appointments corresponding to person and position
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @param  positionId the <code>Id</code> of the position
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>positionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPosition(org.osid.id.Id personId,
                                                                                  org.osid.id.Id positionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPersonId(personId, true);
        query.matchPositionId(positionId, true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets a list of appointments corresponding to person and position
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible
     *  through this session.
     *
     *  In effective mode, appointments are returned that are
     *  currently effective.  In any effective mode, effective
     *  appointments and those currently expired are returned.
     *
     *  @param  personId the <code>Id</code> of the person
     *  @param  positionId the <code>Id</code> of the position
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AppointmentList</code>
     *  @throws org.osid.NullArgumentException <code>personId</code>,
     *          <code>positionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointmentsForPersonAndPositionOnDate(org.osid.id.Id personId,
                                                                                        org.osid.id.Id positionId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchPersonId(personId, true);
        query.matchPositionId(positionId, true);
        query.matchDate(from, to, true);
        return (this.session.getAppointmentsByQuery(query));
    }

    
    /**
     *  Gets all <code>Appointments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  appointments or an error results. Otherwise, the returned list
     *  may contain only those appointments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, appointments are returned that are currently
     *  effective.  In any effective mode, effective appointments and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Appointments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.personnel.AppointmentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAppointmentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.personnel.AppointmentQuery getQuery() {
        org.osid.personnel.AppointmentQuery query = this.session.getAppointmentQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
