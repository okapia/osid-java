//
// AbstractMutableIssue.java
//
//     Defines a mutable Issue.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Issue</code>.
 */

public abstract class AbstractMutableIssue
    extends net.okapia.osid.jamocha.tracking.issue.spi.AbstractIssue
    implements org.osid.tracking.Issue,
               net.okapia.osid.jamocha.builder.tracking.issue.IssueMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this issue. 
     *
     *  @param record issue record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addIssueRecord(org.osid.tracking.records.IssueRecord record, org.osid.type.Type recordType) {
        super.addIssueRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this issue is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this issue ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this issue.
     *
     *  @param displayName the name for this issue
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this issue.
     *
     *  @param description the description of this issue
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this issue
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException <code>queue</code> is
     *          <code>null</code>
     */

    @Override
    public void setQueue(org.osid.tracking.Queue queue) {
        super.setQueue(queue);
        return;
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    @Override
    public void setCustomer(org.osid.resource.Resource customer) {
        super.setCustomer(customer);
        return;
    }


    /**
     *  Sets the topic.
     *
     *  @param topic a topic
     *  @throws org.osid.NullArgumentException <code>topic</code> is
     *          <code>null</code>
     */

    @Override
    public void setTopic(org.osid.ontology.Subject topic) {
        super.setTopic(topic);
        return;
    }


    /**
     *  Sets the master issue.
     *
     *  @param issue a master issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    @Override
    public void setMasterIssue(org.osid.tracking.Issue issue) {
        super.setMasterIssue(issue);
        return;
    }


    /**
     *  Adds a duplicate issue.
     *
     *  @param issue a duplicate issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    public void addDuplicateIssue(org.osid.tracking.Issue issue) {
        super.addDuplicateIssue(issue);
        return;
    }


    /**
     *  Sets all the duplicate issues.
     *
     *  @param issues a collection of duplicate issues
     *  @throws org.osid.NullArgumentException <code>issues</code> is
     *          <code>null</code>
     */

    @Override
    public void setDuplicateIssues(java.util.Collection<org.osid.tracking.Issue> issues) {
        super.setDuplicateIssues(issues);
        return;
    }


    /**
     *  Sets the branched issue.
     *
     *  @param issue a branched issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    @Override
    public void setBranchedIssue(org.osid.tracking.Issue issue) {
        super.setBranchedIssue(issue);
        return;
    }


    /**
     *  Sets the root issue.
     *
     *  @param issue a root issue
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    @Override
    public void setRootIssue(org.osid.tracking.Issue issue) {
        super.setRootIssue(issue);
        return;
    }


    /**
     *  Sets the priority type.
     *
     *  @param priorityType a priority type
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     */

    @Override
    public void setPriorityType(org.osid.type.Type priorityType) {
        super.setPriorityType(priorityType);
        return;
    }


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreator(org.osid.resource.Resource creator) {
        super.setCreator(creator);
        return;
    }


    /**
     *  Sets the creating agent.
     *
     *  @param agent a creating agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreatingAgent(org.osid.authentication.Agent agent) {
        super.setCreatingAgent(agent);
        return;
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreatedDate(org.osid.calendaring.DateTime date) {
        super.setCreatedDate(date);
        return;
    }


    /**
     *  Sets the reopener.
     *
     *  @param reopener a reopener
     *  @throws org.osid.NullArgumentException <code>reopener</code>
     *          is <code>null</code>
     */

    @Override
    public void setReopener(org.osid.resource.Resource reopener) {
        super.setReopener(reopener);
        return;
    }


    /**
     *  Sets the reopening agent.
     *
     *  @param agent a reopening agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setReopeningAgent(org.osid.authentication.Agent agent) {
        super.setReopeningAgent(agent);
        return;
    }


    /**
     *  Sets the reopened date.
     *
     *  @param date a reopened date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setReopenedDate(org.osid.calendaring.DateTime date) {
        super.setReopenedDate(date);
        return;
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDueDate(org.osid.calendaring.DateTime date) {
        super.setDueDate(date);
        return;
    }


    /**
     *  Adds a blocker.
     *
     *  @param blocker a blocker
     *  @throws org.osid.NullArgumentException <code>blocker</code> is
     *          <code>null</code>
     */

    @Override
    public void addBlocker(org.osid.tracking.Issue blocker) {
        super.addBlocker(blocker);
        return;
    }


    /**
     *  Sets all the blockers.
     *
     *  @param blockers a collection of blockers
     *  @throws org.osid.NullArgumentException <code>blockers</code>
     *          is <code>null</code>
     */

    @Override
    public void setBlockers(java.util.Collection<org.osid.tracking.Issue> blockers) {
        super.setBlockers(blockers);
        return;
    }


    /**
     *  Sets the resolver.
     *
     *  @param resolver a resolver
     *  @throws org.osid.NullArgumentException <code>resolver</code>
     *          is <code>null</code>
     */

    @Override
    public void setResolver(org.osid.resource.Resource resolver) {
        super.setResolver(resolver);
        return;
    }


    /**
     *  Sets the resolving agent.
     *
     *  @param agent a resolving agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setResolvingAgent(org.osid.authentication.Agent agent) {
        super.setResolvingAgent(agent);
        return;
    }


    /**
     *  Sets the resolved date.
     *
     *  @param date a resolved date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setResolvedDate(org.osid.calendaring.DateTime date) {
        super.setResolvedDate(date);
        return;
    }


    /**
     *  Sets the resolution type.
     *
     *  @param resolutionType a resolution type
     *  @throws org.osid.NullArgumentException
     *          <code>resolutionType</code> is <code>null</code>
     */

    @Override
    public void setResolutionType(org.osid.type.Type resolutionType) {
        super.setResolutionType(resolutionType);
        return;
    }


    /**
     *  Sets the closer.
     *
     *  @param closer a closer
     *  @throws org.osid.NullArgumentException <code>closer</code> is
     *          <code>null</code>
     */

    @Override
    public void setCloser(org.osid.resource.Resource closer) {
        super.setCloser(closer);
        return;
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent a closing agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setClosingAgent(org.osid.authentication.Agent agent) {
        super.setClosingAgent(agent);
        return;
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setClosedDate(org.osid.calendaring.DateTime date) {
        super.setClosedDate(date);
        return;
    }


    /**
     *  Sets the assigned resource.
     *
     *  @param resource an assigned resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void setAssignedResource(org.osid.resource.Resource resource) {
        super.setAssignedResource(resource);
        return;
    }
}

