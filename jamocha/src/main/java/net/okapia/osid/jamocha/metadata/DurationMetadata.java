//
// DurationMetadata.java
//
//     Defines a duration Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a duration Metadata.
 */

public final class DurationMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractDurationMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code DurationMetadata}.
     *
     *  @param elementId the Id of the element    
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DurationMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code DurationMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DurationMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code DurationMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DurationMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
	return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *  	is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }


    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }
   
    
    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    public void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        super.setResolution(resolution);
        return;
    }


    /**
     *  Add support for a calendar type.
     *
     *  @param calendarType the type of calendar
     *  @throws org.osid.NullArgumentException {@code calendarType} is
     *          {@code null}
     */

    public void addCalendarType(org.osid.type.Type calendarType) {
        super.addCalendarType(calendarType);
        return;
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    public void addTimeType(org.osid.type.Type timeType) {
        super.addTimeType(timeType);
        return;
    }


    /**
     *  Sets the duration range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    public void setDurationRange(org.osid.calendaring.Duration min, org.osid.calendaring.Duration max) {
        super.setDurationRange(min, max);
        return;
    }


    /**
     *  Sets the duration set.
     *
     *  @param values a collection of accepted duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDurationSet(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.setDurationSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the duration set.
     *
     *  @param values a collection of accepted duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToDurationSet(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.addToDurationSet(values);
        return;
    }


    /**
     *  Adds a value to the duration set.
     *
     *  @param value a duration value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToDurationSet(org.osid.calendaring.Duration value) {
        super.addToDurationSet(value);
        return;
    }


    /**
     *  Removes a value from the duration set.
     *
     *  @param value a duration value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromDurationSet(org.osid.calendaring.Duration value) {
        super.removeFromDurationSet(value);
        return;
    }


    /**
     *  Clears the duration set.
     */

    public void clearDurationSet() {
        super.clearDurationSet();
        return;
    }


    /**
     *  Sets the default duration set.
     *
     *  @param values a collection of default duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.setDefaultDurationValues(values);
        return;
    }


    /**
     *  Adds a collection of default duration values.
     *
     *  @param values a collection of default duration values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.addDefaultDurationValues(values);
        return;
    }


    /**
     *  Adds a default duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultDurationValue(org.osid.calendaring.Duration value) {
        super.addDefaultDurationValue(value);
        return;
    }


    /**
     *  Removes a default duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultDurationValue(org.osid.calendaring.Duration value) {
        super.removeDefaultDurationValue(value);
        return;
    }


    /**
     *  Clears the default duration values.
     */

    public void clearDefaultDurationValues() {
        super.clearDefaultDurationValues();
        return;
    }


    /**
     *  Sets the existing duration set.
     *
     *  @param values a collection of existing duration values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.setExistingDurationValues(values);
        return;
    }


    /**
     *  Adds a collection of existing duration values.
     *
     *  @param values a collection of existing duration values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingDurationValues(java.util.Collection<org.osid.calendaring.Duration> values) {
        super.addExistingDurationValues(values);
        return;
    }


    /**
     *  Adds a existing duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingDurationValue(org.osid.calendaring.Duration value) {
        super.addExistingDurationValue(value);
        return;
    }


    /**
     *  Removes a existing duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingDurationValue(org.osid.calendaring.Duration value) {
        super.removeExistingDurationValue(value);
        return;
    }


    /**
     *  Clears the existing duration values.
     */

    public void clearExistingDurationValues() {
        super.clearExistingDurationValues();
        return;
    }        
}
