//
// AbstractImmutableStock.java
//
//     Wraps a mutable Stock to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Stock</code> to hide modifiers. This
 *  wrapper provides an immutized Stock from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying stock whose state changes are visible.
 */

public abstract class AbstractImmutableStock
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.inventory.Stock {

    private final org.osid.inventory.Stock stock;


    /**
     *  Constructs a new <code>AbstractImmutableStock</code>.
     *
     *  @param stock the stock to immutablize
     *  @throws org.osid.NullArgumentException <code>stock</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableStock(org.osid.inventory.Stock stock) {
        super(stock);
        this.stock = stock;
        return;
    }


    /**
     *  Gets the sku for this class of items. 
     *
     *  @return a stock keeping unit 
     */

    @OSID @Override
    public String getSKU() {
        return (this.stock.getSKU());
    }


    /**
     *  Gets the model <code> Ids </code> of the items in this stock. 
     *
     *  @return model <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getModelIds() {
        return (this.stock.getModelIds());
    }


    /**
     *  Gets the models of the items in this stock. 
     *
     *  @return a model list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException {

        return (this.stock.getModels());
    }


    /**
     *  Gets the location description in which this stock is located. 
     *
     *  @return a location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.stock.getLocationDescription());
    }


    /**
     *  Gets the location <code> Id </code> to which this stock is located. 
     *
     *  @return a location <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLocationIds() {
        return (this.stock.getLocationIds());
    }


    /**
     *  Gets the location to which this stock is located. 
     *
     *  @return a location list 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.LocationList getLocations()
        throws org.osid.OperationFailedException {

        return (this.stock.getLocations());
    }


    /**
     *  Gets the stock record corresponding to the given <code> Stock </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> stockRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(stockRecordType) </code> is <code> true </code> . 
     *
     *  @param  stockRecordType the type of stock record to retrieve 
     *  @return the stock record 
     *  @throws org.osid.NullArgumentException <code> stockRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(stockRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.records.StockRecord getStockRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        return (this.stock.getStockRecord(stockRecordType));
    }
}

