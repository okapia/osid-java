//
// MutableIndexedMapProxyBudgetEntryLookupSession
//
//    Implements a BudgetEntry lookup service backed by a collection of
//    budgetEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting;


/**
 *  Implements a BudgetEntry lookup service backed by a collection of
 *  budgetEntries. The budget entries are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some budgetEntries may be compatible
 *  with more types than are indicated through these budgetEntry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of budget entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyBudgetEntryLookupSession
    extends net.okapia.osid.jamocha.core.financials.budgeting.spi.AbstractIndexedMapBudgetEntryLookupSession
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBudgetEntryLookupSession} with
     *  no budget entry.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBudgetEntryLookupSession(org.osid.financials.Business business,
                                                       org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBudgetEntryLookupSession} with
     *  a single budget entry.
     *
     *  @param business the business
     *  @param  budgetEntry an budget entry
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budgetEntry}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBudgetEntryLookupSession(org.osid.financials.Business business,
                                                       org.osid.financials.budgeting.BudgetEntry budgetEntry, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putBudgetEntry(budgetEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBudgetEntryLookupSession} using
     *  an array of budget entries.
     *
     *  @param business the business
     *  @param  budgetEntries an array of budget entries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budgetEntries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBudgetEntryLookupSession(org.osid.financials.Business business,
                                                       org.osid.financials.budgeting.BudgetEntry[] budgetEntries, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putBudgetEntries(budgetEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBudgetEntryLookupSession} using
     *  a collection of budget entries.
     *
     *  @param business the business
     *  @param  budgetEntries a collection of budget entries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code budgetEntries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyBudgetEntryLookupSession(org.osid.financials.Business business,
                                                       java.util.Collection<? extends org.osid.financials.budgeting.BudgetEntry> budgetEntries,
                                                       org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putBudgetEntries(budgetEntries);
        return;
    }

    
    /**
     *  Makes a {@code BudgetEntry} available in this session.
     *
     *  @param  budgetEntry a budget entry
     *  @throws org.osid.NullArgumentException {@code budgetEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putBudgetEntry(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
        super.putBudgetEntry(budgetEntry);
        return;
    }


    /**
     *  Makes an array of budget entries available in this session.
     *
     *  @param  budgetEntries an array of budget entries
     *  @throws org.osid.NullArgumentException {@code budgetEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putBudgetEntries(org.osid.financials.budgeting.BudgetEntry[] budgetEntries) {
        super.putBudgetEntries(budgetEntries);
        return;
    }


    /**
     *  Makes collection of budget entries available in this session.
     *
     *  @param  budgetEntries a collection of budget entries
     *  @throws org.osid.NullArgumentException {@code budgetEntry{@code 
     *          is {@code null}
     */

    @Override
    public void putBudgetEntries(java.util.Collection<? extends org.osid.financials.budgeting.BudgetEntry> budgetEntries) {
        super.putBudgetEntries(budgetEntries);
        return;
    }


    /**
     *  Removes a BudgetEntry from this session.
     *
     *  @param budgetEntryId the {@code Id} of the budget entry
     *  @throws org.osid.NullArgumentException {@code budgetEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBudgetEntry(org.osid.id.Id budgetEntryId) {
        super.removeBudgetEntry(budgetEntryId);
        return;
    }    
}
