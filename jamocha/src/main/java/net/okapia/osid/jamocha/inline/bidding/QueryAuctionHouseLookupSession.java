//
// QueryAuctionHouseLookupSession.java
//
//    An inline adapter that maps an AuctionHouseLookupSession to
//    an AuctionHouseQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding;


/**
 *  An inline adapter that maps an AuctionHouseLookupSession to
 *  an AuctionHouseQuerySession.
 */

public final class QueryAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.inline.bidding.spi.AbstractQueryAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {


    /**
     *  Constructs a new QueryAuctionHouseLookupSession.
     *
     *  @param querySession the underlying auction house query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    public QueryAuctionHouseLookupSession(org.osid.bidding.AuctionHouseQuerySession querySession) {
        super(querySession);
        return;
    }


    /**
     *  Constructs a new QueryAuctionHouseLookupSession.
     *
     *  @param querySession the underlying auction house query session
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code querySession} or
     *          {@code proxy} is {@code null}
     */

    public QueryAuctionHouseLookupSession(org.osid.bidding.AuctionHouseQuerySession querySession,
                                      org.osid.proxy.Proxy proxy) {
        super(querySession);
        setSessionProxy(proxy);
        return;
    }
}
