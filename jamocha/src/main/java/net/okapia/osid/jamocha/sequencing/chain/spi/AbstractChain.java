//
// AbstractChain.java
//
//     Defines a Chain.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Chain</code>.
 */

public abstract class AbstractChain
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.sequencing.Chain {

    private boolean fifo = true;
    private final java.util.Collection<org.osid.sequencing.records.ChainRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if elements are appended to the end of the chain. 
     *
     *  @return <code> true </code> if elements are appended to the
     *          end of the chain, <code> false </code> if elements are
     *          added to the top of the chain
     */

    @OSID @Override
    public boolean isFifo() {
        return (this.fifo);
    }


    /**
     *  Sets the fifo flag.
     *
     *  @param fifo {@code true} if new items appended, {@code false}
     *         if new items put in front of list
     */

    protected void setFifo(boolean fifo) {
        this.fifo = fifo;
        return;
    }


    /**
     *  Tests if this chain supports the given record
     *  <code>Type</code>.
     *
     *  @param  chainRecordType a chain record type 
     *  @return <code>true</code> if the chainRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type chainRecordType) {
        for (org.osid.sequencing.records.ChainRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  chainRecordType the chain record type 
     *  @return the chain record 
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainRecord getChainRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Adds a record to this chain. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param chainRecord the chain record
     *  @param chainRecordType chain record type
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecord</code> or
     *          <code>chainRecordTypechain</code> is
     *          <code>null</code>
     */
            
    protected void addChainRecord(org.osid.sequencing.records.ChainRecord chainRecord, 
                                     org.osid.type.Type chainRecordType) {

        addRecordType(chainRecordType);
        this.records.add(chainRecord);
        
        return;
    }
}
