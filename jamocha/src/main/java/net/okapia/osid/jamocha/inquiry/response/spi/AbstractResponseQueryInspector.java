//
// AbstractResponseQueryInspector.java
//
//     A template for making a ResponseQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for responses.
 */

public abstract class AbstractResponseQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.inquiry.ResponseQueryInspector {

    private final java.util.Collection<org.osid.inquiry.records.ResponseQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the inquiry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Gets the responder resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResponderIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the responder resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResponderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the responding agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRespondingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the responding agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getRespondingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the affirmative response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAffirmativeTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given response query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a response implementing the requested record.
     *
     *  @param responseRecordType a response record type
     *  @return the response query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseQueryInspectorRecord getResponseQueryInspectorRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this response query. 
     *
     *  @param responseQueryInspectorRecord response query inspector
     *         record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResponseQueryInspectorRecord(org.osid.inquiry.records.ResponseQueryInspectorRecord responseQueryInspectorRecord, 
                                                   org.osid.type.Type responseRecordType) {

        addRecordType(responseRecordType);
        nullarg(responseRecordType, "response record type");
        this.records.add(responseQueryInspectorRecord);        
        return;
    }
}
