//
// AbstractQueryCompositionEnablerLookupSession.java
//
//    An inline adapter that maps a CompositionEnablerLookupSession to
//    a CompositionEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CompositionEnablerLookupSession to
 *  a CompositionEnablerQuerySession.
 */

public abstract class AbstractQueryCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.repository.rules.spi.AbstractCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;    
    private final org.osid.repository.rules.CompositionEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCompositionEnablerLookupSession.
     *
     *  @param querySession the underlying composition enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCompositionEnablerLookupSession(org.osid.repository.rules.CompositionEnablerQuerySession querySession) {
        nullarg(querySession, "composition enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Repository</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Repository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the <code>Repository</code> associated with this 
     *  session.
     *
     *  @return the <code>Repository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform <code>CompositionEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompositionEnablers() {
        return (this.session.canSearchCompositionEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include composition enablers in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    

    /**
     *  Only active composition enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveCompositionEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive composition enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusCompositionEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CompositionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CompositionEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CompositionEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  compositionEnablerId <code>Id</code> of the
     *          <code>CompositionEnabler</code>
     *  @return the composition enabler
     *  @throws org.osid.NotFoundException <code>compositionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>compositionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnabler getCompositionEnabler(org.osid.id.Id compositionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchId(compositionEnablerId, true);
        org.osid.repository.rules.CompositionEnablerList compositionEnablers = this.session.getCompositionEnablersByQuery(query);
        if (compositionEnablers.hasNext()) {
            return (compositionEnablers.getNextCompositionEnabler());
        } 
        
        throw new org.osid.NotFoundException(compositionEnablerId + " not found");
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  compositionEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>CompositionEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  compositionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CompositionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByIds(org.osid.id.IdList compositionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = compositionEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCompositionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> corresponding to
     *  the given composition enabler genus <code>Type</code> which
     *  does not include composition enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the
     *  returned list may contain only those composition enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  compositionEnablerGenusType a compositionEnabler genus type 
     *  @return the returned <code>CompositionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByGenusType(org.osid.type.Type compositionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchGenusType(compositionEnablerGenusType, true);
        return (this.session.getCompositionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> corresponding to
     *  the given composition enabler genus <code>Type</code> and
     *  include any additional composition enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the
     *  returned list may contain only those composition enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  compositionEnablerGenusType a compositionEnabler genus type 
     *  @return the returned <code>CompositionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByParentGenusType(org.osid.type.Type compositionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchParentGenusType(compositionEnablerGenusType, true);
        return (this.session.getCompositionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> containing the
     *  given composition enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the
     *  returned list may contain only those composition enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  compositionEnablerRecordType a compositionEnabler record type 
     *  @return the returned <code>CompositionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByRecordType(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchRecordType(compositionEnablerRecordType, true);
        return (this.session.getCompositionEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the
     *  returned list may contain only those composition enablers that
     *  are accessible through this session.
     *  
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CompositionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCompositionEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>CompositionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the
     *  returned list may contain only those composition enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, composition enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  composition enablers are returned.
     *
     *  @return a list of <code>CompositionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.repository.rules.CompositionEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCompositionEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.repository.rules.CompositionEnablerQuery getQuery() {
        org.osid.repository.rules.CompositionEnablerQuery query = this.session.getCompositionEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
