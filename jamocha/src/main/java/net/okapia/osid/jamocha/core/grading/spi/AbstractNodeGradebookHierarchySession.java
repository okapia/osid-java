//
// AbstractNodeGradebookHierarchySession.java
//
//     Defines a Gradebook hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a gradebook hierarchy session for delivering a hierarchy
 *  of gradebooks using the GradebookNode interface.
 */

public abstract class AbstractNodeGradebookHierarchySession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradebookHierarchySession
    implements org.osid.grading.GradebookHierarchySession {

    private java.util.Collection<org.osid.grading.GradebookNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root gradebook <code> Ids </code> in this hierarchy.
     *
     *  @return the root gradebook <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootGradebookIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToIdList(this.roots));
    }


    /**
     *  Gets the root gradebooks in the gradebook hierarchy. A node
     *  with no parents is an orphan. While all gradebook <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root gradebooks 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getRootGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToGradebookList(new net.okapia.osid.jamocha.grading.gradebooknode.ArrayGradebookNodeList(this.roots)));
    }


    /**
     *  Adds a root gradebook node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootGradebook(org.osid.grading.GradebookNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root gradebook nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootGradebooks(java.util.Collection<org.osid.grading.GradebookNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root gradebook node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootGradebook(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.grading.GradebookNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Gradebook </code> has any parents. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @return <code> true </code> if the gradebook has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentGradebooks(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getGradebookNode(gradebookId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  gradebook.
     *
     *  @param  id an <code> Id </code> 
     *  @param  gradebookId the <code> Id </code> of a gradebook 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> gradebookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> gradebookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfGradebook(org.osid.id.Id id, org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.grading.GradebookNodeList parents = getGradebookNode(gradebookId).getParentGradebookNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextGradebookNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given gradebook. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @return the parent <code> Ids </code> of the gradebook 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentGradebookIds(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebook.GradebookToIdList(getParentGradebooks(gradebookId)));
    }


    /**
     *  Gets the parents of the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> to query 
     *  @return the parents of the gradebook 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getParentGradebooks(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToGradebookList(getGradebookNode(gradebookId).getParentGradebookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  gradebook.
     *
     *  @param  id an <code> Id </code> 
     *  @param  gradebookId the Id of a gradebook 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> gradebookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfGradebook(org.osid.id.Id id, org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfGradebook(id, gradebookId)) {
            return (true);
        }

        try (org.osid.grading.GradebookList parents = getParentGradebooks(gradebookId)) {
            while (parents.hasNext()) {
                if (isAncestorOfGradebook(id, parents.getNextGradebook().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a gradebook has any children. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @return <code> true </code> if the <code> gradebookId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildGradebooks(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradebookNode(gradebookId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  gradebook.
     *
     *  @param  id an <code> Id </code> 
     *  @param gradebookId the <code> Id </code> of a 
     *         gradebook
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> gradebookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> gradebookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfGradebook(org.osid.id.Id id, org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfGradebook(gradebookId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  gradebook.
     *
     *  @param  gradebookId the <code> Id </code> to query 
     *  @return the children of the gradebook 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildGradebookIds(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebook.GradebookToIdList(getChildGradebooks(gradebookId)));
    }


    /**
     *  Gets the children of the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> to query 
     *  @return the children of the gradebook 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getChildGradebooks(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToGradebookList(getGradebookNode(gradebookId).getChildGradebookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  gradebook.
     *
     *  @param  id an <code> Id </code> 
     *  @param gradebookId the <code> Id </code> of a 
     *         gradebook
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> gradebookId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfGradebook(org.osid.id.Id id, org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfGradebook(gradebookId, id)) {
            return (true);
        }

        try (org.osid.grading.GradebookList children = getChildGradebooks(gradebookId)) {
            while (children.hasNext()) {
                if (isDescendantOfGradebook(id, children.getNextGradebook().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  gradebook.
     *
     *  @param  gradebookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified gradebook node 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getGradebookNodeIds(org.osid.id.Id gradebookId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.grading.gradebooknode.GradebookNodeToNode(getGradebookNode(gradebookId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given gradebook.
     *
     *  @param  gradebookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified gradebook node 
     *  @throws org.osid.NotFoundException <code> gradebookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> gradebookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookNode getGradebookNodes(org.osid.id.Id gradebookId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGradebookNode(gradebookId));
    }


    /**
     *  Closes this <code>GradebookHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a gradebook node.
     *
     *  @param gradebookId the id of the gradebook node
     *  @throws org.osid.NotFoundException <code>gradebookId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>gradebookId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.grading.GradebookNode getGradebookNode(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(gradebookId, "gradebook Id");
        for (org.osid.grading.GradebookNode gradebook : this.roots) {
            if (gradebook.getId().equals(gradebookId)) {
                return (gradebook);
            }

            org.osid.grading.GradebookNode r = findGradebook(gradebook, gradebookId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(gradebookId + " is not found");
    }


    protected org.osid.grading.GradebookNode findGradebook(org.osid.grading.GradebookNode node, 
                                                           org.osid.id.Id gradebookId) 
	throws org.osid.OperationFailedException {

        try (org.osid.grading.GradebookNodeList children = node.getChildGradebookNodes()) {
            while (children.hasNext()) {
                org.osid.grading.GradebookNode gradebook = children.getNextGradebookNode();
                if (gradebook.getId().equals(gradebookId)) {
                    return (gradebook);
                }
                
                gradebook = findGradebook(gradebook, gradebookId);
                if (gradebook != null) {
                    return (gradebook);
                }
            }
        }

        return (null);
    }
}
