//
// AbstractProcessSearchOdrer.java
//
//     Defines a ProcessSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProcessSearchOrder}.
 */

public abstract class AbstractProcessSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.workflow.ProcessSearchOrder {

    private final java.util.Collection<org.osid.workflow.records.ProcessSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by enabled. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by initial step. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitialStep(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an initial step search order is available. 
     *
     *  @return <code> true </code> if a step search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStepSearchOrder() {
        return (false);
    }


    /**
     *  Gets the initial step search order. 
     *
     *  @return the step search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInitialStepSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.StepSearchOrder getInitialStepSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInitialStepSearchOrder() is false");
    }


    /**
     *  Orders the results by initial state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInitialState(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an initial state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInitialStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the initial state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsInitialStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getInitialStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsInitialStateSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  processRecordType a process record type 
     *  @return {@code true} if the processRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code processRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type processRecordType) {
        for (org.osid.workflow.records.ProcessSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(processRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  processRecordType the process record type 
     *  @return the process search order record
     *  @throws org.osid.NullArgumentException
     *          {@code processRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(processRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.workflow.records.ProcessSearchOrderRecord getProcessSearchOrderRecord(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.ProcessSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(processRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this process. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param processRecord the process search odrer record
     *  @param processRecordType process record type
     *  @throws org.osid.NullArgumentException
     *          {@code processRecord} or
     *          {@code processRecordTypeprocess} is
     *          {@code null}
     */
            
    protected void addProcessRecord(org.osid.workflow.records.ProcessSearchOrderRecord processSearchOrderRecord, 
                                     org.osid.type.Type processRecordType) {

        addRecordType(processRecordType);
        this.records.add(processSearchOrderRecord);
        
        return;
    }
}
