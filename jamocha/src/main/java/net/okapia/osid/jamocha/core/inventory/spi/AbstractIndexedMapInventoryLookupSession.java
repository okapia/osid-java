//
// AbstractIndexedMapInventoryLookupSession.java
//
//    A simple framework for providing an Inventory lookup service
//    backed by a fixed collection of inventories with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Inventory lookup service backed by a
 *  fixed collection of inventories. The inventories are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some inventories may be compatible
 *  with more types than are indicated through these inventory
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inventories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInventoryLookupSession
    extends AbstractMapInventoryLookupSession
    implements org.osid.inventory.InventoryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inventory.Inventory> inventoriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Inventory>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.Inventory> inventoriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Inventory>());


    /**
     *  Makes an <code>Inventory</code> available in this session.
     *
     *  @param  inventory an inventory
     *  @throws org.osid.NullArgumentException <code>inventory<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInventory(org.osid.inventory.Inventory inventory) {
        super.putInventory(inventory);

        this.inventoriesByGenus.put(inventory.getGenusType(), inventory);
        
        try (org.osid.type.TypeList types = inventory.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inventoriesByRecord.put(types.getNextType(), inventory);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an inventory from this session.
     *
     *  @param inventoryId the <code>Id</code> of the inventory
     *  @throws org.osid.NullArgumentException <code>inventoryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInventory(org.osid.id.Id inventoryId) {
        org.osid.inventory.Inventory inventory;
        try {
            inventory = getInventory(inventoryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inventoriesByGenus.remove(inventory.getGenusType());

        try (org.osid.type.TypeList types = inventory.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inventoriesByRecord.remove(types.getNextType(), inventory);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInventory(inventoryId);
        return;
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> which does not include
     *  inventories of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known inventories or an error results. Otherwise,
     *  the returned list may contain only those inventories that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.inventory.ArrayInventoryList(this.inventoriesByGenus.get(inventoryGenusType)));
    }


    /**
     *  Gets an <code>InventoryList</code> containing the given
     *  inventory record <code>Type</code>. In plenary mode, the
     *  returned list contains all known inventories or an error
     *  results. Otherwise, the returned list may contain only those
     *  inventories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return the returned <code>inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByRecordType(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.inventory.ArrayInventoryList(this.inventoriesByRecord.get(inventoryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inventoriesByGenus.clear();
        this.inventoriesByRecord.clear();

        super.close();

        return;
    }
}
