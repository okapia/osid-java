//
// AbstractAssemblyProfileItemQuery.java
//
//     A ProfileItemQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.profile.profileitem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileItemQuery that stores terms.
 */

public abstract class AbstractAssemblyProfileItemQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.profile.ProfileItemQuery,
               org.osid.profile.ProfileItemQueryInspector,
               org.osid.profile.ProfileItemSearchOrder {

    private final java.util.Collection<org.osid.profile.records.ProfileItemQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileItemQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileItemSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProfileItemQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProfileItemQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the pofile entry <code> Id </code> for this query. 
     *
     *  @param  profileEntryId a profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProfileEntryId(org.osid.id.Id profileEntryId, 
                                    boolean match) {
        getAssembler().addIdTerm(getProfileEntryIdColumn(), profileEntryId, match);
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileEntryIdTerms() {
        getAssembler().clearTerms(getProfileEntryIdColumn());
        return;
    }


    /**
     *  Gets the profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileEntryIdTerms() {
        return (getAssembler().getIdTerms(getProfileEntryIdColumn()));
    }


    /**
     *  Gets the ProfileEntryId column name.
     *
     * @return the column name
     */

    protected String getProfileEntryIdColumn() {
        return ("profile_entry_id");
    }


    /**
     *  Tests if an <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsProfileEntryQuery() is false");
    }


    /**
     *  Matches profile items that have any profile entry mapping. 
     *
     *  @param  match <code> true </code> to match items with any entry 
     *          mapping, <code> false </code> to match items with no entry 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getProfileEntryColumn(), match);
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileEntryTerms() {
        getAssembler().clearTerms(getProfileEntryColumn());
        return;
    }


    /**
     *  Gets the profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the ProfileEntry column name.
     *
     * @return the column name
     */

    protected String getProfileEntryColumn() {
        return ("profile_entry");
    }


    /**
     *  Sets the profile <code> Id </code> for this query. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        getAssembler().addIdTerm(getProfileIdColumn(), profileId, match);
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        getAssembler().clearTerms(getProfileIdColumn());
        return;
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (getAssembler().getIdTerms(getProfileIdColumn()));
    }


    /**
     *  Gets the ProfileId column name.
     *
     * @return the column name
     */

    protected String getProfileIdColumn() {
        return ("profile_id");
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        getAssembler().clearTerms(getProfileColumn());
        return;
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the Profile column name.
     *
     * @return the column name
     */

    protected String getProfileColumn() {
        return ("profile");
    }


    /**
     *  Tests if this profileItem supports the given record
     *  <code>Type</code>.
     *
     *  @param  profileItemRecordType a profile item record type 
     *  @return <code>true</code> if the profileItemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileItemRecordType) {
        for (org.osid.profile.records.ProfileItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  profileItemRecordType the profile item record type 
     *  @return the profile item query record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileItemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemQueryRecord getProfileItemQueryRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileItemQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileItemRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  profileItemRecordType the profile item record type 
     *  @return the profile item query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileItemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemQueryInspectorRecord getProfileItemQueryInspectorRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileItemQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileItemRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param profileItemRecordType the profile item record type
     *  @return the profile item search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileItemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemSearchOrderRecord getProfileItemSearchOrderRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileItemSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileItemRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this profile item. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileItemQueryRecord the profile item query record
     *  @param profileItemQueryInspectorRecord the profile item query inspector
     *         record
     *  @param profileItemSearchOrderRecord the profile item search order record
     *  @param profileItemRecordType profile item record type
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemQueryRecord</code>,
     *          <code>profileItemQueryInspectorRecord</code>,
     *          <code>profileItemSearchOrderRecord</code> or
     *          <code>profileItemRecordTypeprofileItem</code> is
     *          <code>null</code>
     */
            
    protected void addProfileItemRecords(org.osid.profile.records.ProfileItemQueryRecord profileItemQueryRecord, 
                                      org.osid.profile.records.ProfileItemQueryInspectorRecord profileItemQueryInspectorRecord, 
                                      org.osid.profile.records.ProfileItemSearchOrderRecord profileItemSearchOrderRecord, 
                                      org.osid.type.Type profileItemRecordType) {

        addRecordType(profileItemRecordType);

        nullarg(profileItemQueryRecord, "profile item query record");
        nullarg(profileItemQueryInspectorRecord, "profile item query inspector record");
        nullarg(profileItemSearchOrderRecord, "profile item search odrer record");

        this.queryRecords.add(profileItemQueryRecord);
        this.queryInspectorRecords.add(profileItemQueryInspectorRecord);
        this.searchOrderRecords.add(profileItemSearchOrderRecord);
        
        return;
    }
}
