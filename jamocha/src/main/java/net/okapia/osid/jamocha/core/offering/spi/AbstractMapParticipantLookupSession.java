//
// AbstractMapParticipantLookupSession
//
//    A simple framework for providing a Participant lookup service
//    backed by a fixed collection of participants.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Participant lookup service backed by a
 *  fixed collection of participants. The participants are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Participants</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapParticipantLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.Participant> participants = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.Participant>());


    /**
     *  Makes a <code>Participant</code> available in this session.
     *
     *  @param  participant a participant
     *  @throws org.osid.NullArgumentException <code>participant<code>
     *          is <code>null</code>
     */

    protected void putParticipant(org.osid.offering.Participant participant) {
        this.participants.put(participant.getId(), participant);
        return;
    }


    /**
     *  Makes an array of participants available in this session.
     *
     *  @param  participants an array of participants
     *  @throws org.osid.NullArgumentException <code>participants<code>
     *          is <code>null</code>
     */

    protected void putParticipants(org.osid.offering.Participant[] participants) {
        putParticipants(java.util.Arrays.asList(participants));
        return;
    }


    /**
     *  Makes a collection of participants available in this session.
     *
     *  @param  participants a collection of participants
     *  @throws org.osid.NullArgumentException <code>participants<code>
     *          is <code>null</code>
     */

    protected void putParticipants(java.util.Collection<? extends org.osid.offering.Participant> participants) {
        for (org.osid.offering.Participant participant : participants) {
            this.participants.put(participant.getId(), participant);
        }

        return;
    }


    /**
     *  Removes a Participant from this session.
     *
     *  @param  participantId the <code>Id</code> of the participant
     *  @throws org.osid.NullArgumentException <code>participantId<code> is
     *          <code>null</code>
     */

    protected void removeParticipant(org.osid.id.Id participantId) {
        this.participants.remove(participantId);
        return;
    }


    /**
     *  Gets the <code>Participant</code> specified by its <code>Id</code>.
     *
     *  @param  participantId <code>Id</code> of the <code>Participant</code>
     *  @return the participant
     *  @throws org.osid.NotFoundException <code>participantId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>participantId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant(org.osid.id.Id participantId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.Participant participant = this.participants.get(participantId);
        if (participant == null) {
            throw new org.osid.NotFoundException("participant not found: " + participantId);
        }

        return (participant);
    }


    /**
     *  Gets all <code>Participants</code>. In plenary mode, the returned
     *  list contains all known participants or an error
     *  results. Otherwise, the returned list may contain only those
     *  participants that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Participants</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipants()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.participant.ArrayParticipantList(this.participants.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.participants.clear();
        super.close();
        return;
    }
}
