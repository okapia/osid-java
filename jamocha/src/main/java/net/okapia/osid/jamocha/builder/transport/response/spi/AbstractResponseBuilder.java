//
// AbstractResponse.java
//
//     Defines a Response builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transport.response.spi;


/**
 *  Defines a <code>Response</code> builder.
 */

public abstract class AbstractResponseBuilder<T extends AbstractResponseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.transport.response.ResponseMiter response;


    /**
     *  Constructs a new <code>AbstractResponseBuilder</code>.
     *
     *  @param response the response to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResponseBuilder(net.okapia.osid.jamocha.builder.transport.response.ResponseMiter response) {
        super(response);
        this.response = response;
        return;
    }


    /**
     *  Builds the response.
     *
     *  @return the new response
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.transport.Response build() {
        (new net.okapia.osid.jamocha.builder.validator.transport.response.ResponseValidator(getValidations())).validate(this.response);
        return (new net.okapia.osid.jamocha.builder.transport.response.ImmutableResponse(this.response));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the response miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.transport.response.ResponseMiter getMiter() {
        return (this.response);
    }


    /**
     *  Sets the status message.
     *
     *  @param statusMessage a status message
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>statusMessage</code> is <code>null</code>
     */

    public T statusMessage(org.osid.locale.DisplayText statusMessage) {
        getMiter().setStatusMessage(statusMessage);
        return (self());
    }


    /**
     *  Adds a Response record.
     *
     *  @param record a response record
     *  @param recordType the type of response record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.transport.records.ResponseRecord record, org.osid.type.Type recordType) {
        getMiter().addResponseRecord(record, recordType);
        return (self());
    }
}       


