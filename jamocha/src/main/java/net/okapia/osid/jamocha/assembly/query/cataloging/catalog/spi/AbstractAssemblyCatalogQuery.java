//
// AbstractAssemblyCatalogQuery.java
//
//     A CatalogQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.cataloging.catalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CatalogQuery that stores terms.
 */

public abstract class AbstractAssemblyCatalogQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.cataloging.CatalogQuery,
               org.osid.cataloging.CatalogQueryInspector,
               org.osid.cataloging.CatalogSearchOrder {

    private final java.util.Collection<org.osid.cataloging.records.CatalogQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.cataloging.records.CatalogQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.cataloging.records.CatalogSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCatalogQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCatalogQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches an <code> Id </code> in this catalog. Multiple Ids are treated 
     *  as a boolean <code> OR. </code> 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getIdColumn(), id, match);
        return;
    }


    /**
     *  Matches catalogs that have any <code> Id </code> mapping. 
     *
     *  @param  match <code> true </code> to match catalogs with any <code> Id 
     *          </code> mapping, <code> false </code> to match catalogs with 
     *          no <code> Id </code> mapping 
     */

    @OSID @Override
    public void matchAnyId(boolean match) {
        getAssembler().addIdWildcardTerm(getIdColumn(), match);
        return;
    }


    /**
     *  Clears the <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIdTerms() {
        getAssembler().clearTerms(getIdColumn());
        return;
    }


    /**
     *  Gets the <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIdTerms() {
        return (getAssembler().getIdTerms(getIdColumn()));
    }


    /**
     *  Gets the Id column name.
     *
     * @return the column name
     */

    protected String getIdColumn() {
        return ("id");
    }


    /**
     *  Sets the catalog <code> Id </code> for this query to match catalogs 
     *  that have the specified catalog as an ancestor. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCatalogId(org.osid.id.Id catalogId, boolean match) {
        getAssembler().addIdTerm(getAncestorCatalogIdColumn(), catalogId, match);
        return;
    }


    /**
     *  Clears the ancestor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogIdTerms() {
        getAssembler().clearTerms(getAncestorCatalogIdColumn());
        return;
    }


    /**
     *  Gets the ancestor catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCatalogIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCatalogIdColumn()));
    }


    /**
     *  Gets the AncestorCatalogId column name.
     *
     * @return the column name
     */

    protected String getAncestorCatalogIdColumn() {
        return ("ancestor_catalog_id");
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getAncestorCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCatalogQuery() is false");
    }


    /**
     *  Matches catalogs with any ancestor. 
     *
     *  @param  match <code> true </code> to match catalogs with any ancestor, 
     *          <code> false </code> to match root catalogs 
     */

    @OSID @Override
    public void matchAnyAncestorCatalog(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCatalogColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor query terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogTerms() {
        getAssembler().clearTerms(getAncestorCatalogColumn());
        return;
    }


    /**
     *  Gets the ancestor catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getAncestorCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCatalog column name.
     *
     * @return the column name
     */

    protected String getAncestorCatalogColumn() {
        return ("ancestor_catalog");
    }


    /**
     *  Sets the catalog <code> Id </code> for this query to match catalogs 
     *  that have the specified catalog as an descendant. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCatalogId(org.osid.id.Id catalogId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantCatalogIdColumn(), catalogId, match);
        return;
    }


    /**
     *  Clears the descendant <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogIdTerms() {
        getAssembler().clearTerms(getDescendantCatalogIdColumn());
        return;
    }


    /**
     *  Gets the descendant catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCatalogIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCatalogIdColumn()));
    }


    /**
     *  Gets the DescendantCatalogId column name.
     *
     * @return the column name
     */

    protected String getDescendantCatalogIdColumn() {
        return ("descendant_catalog_id");
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getDescendantCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCatalogQuery() is false");
    }


    /**
     *  Matches catalogs with any descendant. 
     *
     *  @param  match <code> true </code> to match catalogs with any 
     *          descendant, <code> false </code> to match leaf catalogs 
     */

    @OSID @Override
    public void matchAnyDescendantCatalog(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCatalogColumn(), match);
        return;
    }


    /**
     *  Clears the descendant query terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogTerms() {
        getAssembler().clearTerms(getDescendantCatalogColumn());
        return;
    }


    /**
     *  Gets the descendant catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getDescendantCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCatalog column name.
     *
     * @return the column name
     */

    protected String getDescendantCatalogColumn() {
        return ("descendant_catalog");
    }


    /**
     *  Tests if this catalog supports the given record
     *  <code>Type</code>.
     *
     *  @param  catalogRecordType a catalog record type 
     *  @return <code>true</code> if the catalogRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type catalogRecordType) {
        for (org.osid.cataloging.records.CatalogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  catalogRecordType the catalog record type 
     *  @return the catalog query record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogQueryRecord getCatalogQueryRecord(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.records.CatalogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  catalogRecordType the catalog record type 
     *  @return the catalog query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogQueryInspectorRecord getCatalogQueryInspectorRecord(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.records.CatalogQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param catalogRecordType the catalog record type
     *  @return the catalog search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogSearchOrderRecord getCatalogSearchOrderRecord(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.records.CatalogSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this catalog. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param catalogQueryRecord the catalog query record
     *  @param catalogQueryInspectorRecord the catalog query inspector
     *         record
     *  @param catalogSearchOrderRecord the catalog search order record
     *  @param catalogRecordType catalog record type
     *  @throws org.osid.NullArgumentException
     *          <code>catalogQueryRecord</code>,
     *          <code>catalogQueryInspectorRecord</code>,
     *          <code>catalogSearchOrderRecord</code> or
     *          <code>catalogRecordTypecatalog</code> is
     *          <code>null</code>
     */
            
    protected void addCatalogRecords(org.osid.cataloging.records.CatalogQueryRecord catalogQueryRecord, 
                                      org.osid.cataloging.records.CatalogQueryInspectorRecord catalogQueryInspectorRecord, 
                                      org.osid.cataloging.records.CatalogSearchOrderRecord catalogSearchOrderRecord, 
                                      org.osid.type.Type catalogRecordType) {

        addRecordType(catalogRecordType);

        nullarg(catalogQueryRecord, "catalog query record");
        nullarg(catalogQueryInspectorRecord, "catalog query inspector record");
        nullarg(catalogSearchOrderRecord, "catalog search odrer record");

        this.queryRecords.add(catalogQueryRecord);
        this.queryInspectorRecords.add(catalogQueryInspectorRecord);
        this.searchOrderRecords.add(catalogSearchOrderRecord);
        
        return;
    }
}
