//
// MutableMapRealmLookupSession
//
//    Implements a Realm lookup service backed by a collection of
//    realms that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Realm lookup service backed by a collection of
 *  realms. The realms are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of realms can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRealmLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapRealmLookupSession
    implements org.osid.personnel.RealmLookupSession {


    /**
     *  Constructs a new {@code MutableMapRealmLookupSession}
     *  with no realms.
     */

    public MutableMapRealmLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRealmLookupSession} with a
     *  single realm.
     *  
     *  @param realm a realm
     *  @throws org.osid.NullArgumentException {@code realm}
     *          is {@code null}
     */

    public MutableMapRealmLookupSession(org.osid.personnel.Realm realm) {
        putRealm(realm);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRealmLookupSession}
     *  using an array of realms.
     *
     *  @param realms an array of realms
     *  @throws org.osid.NullArgumentException {@code realms}
     *          is {@code null}
     */

    public MutableMapRealmLookupSession(org.osid.personnel.Realm[] realms) {
        putRealms(realms);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRealmLookupSession}
     *  using a collection of realms.
     *
     *  @param realms a collection of realms
     *  @throws org.osid.NullArgumentException {@code realms}
     *          is {@code null}
     */

    public MutableMapRealmLookupSession(java.util.Collection<? extends org.osid.personnel.Realm> realms) {
        putRealms(realms);
        return;
    }

    
    /**
     *  Makes a {@code Realm} available in this session.
     *
     *  @param realm a realm
     *  @throws org.osid.NullArgumentException {@code realm{@code  is
     *          {@code null}
     */

    @Override
    public void putRealm(org.osid.personnel.Realm realm) {
        super.putRealm(realm);
        return;
    }


    /**
     *  Makes an array of realms available in this session.
     *
     *  @param realms an array of realms
     *  @throws org.osid.NullArgumentException {@code realms{@code 
     *          is {@code null}
     */

    @Override
    public void putRealms(org.osid.personnel.Realm[] realms) {
        super.putRealms(realms);
        return;
    }


    /**
     *  Makes collection of realms available in this session.
     *
     *  @param realms a collection of realms
     *  @throws org.osid.NullArgumentException {@code realms{@code  is
     *          {@code null}
     */

    @Override
    public void putRealms(java.util.Collection<? extends org.osid.personnel.Realm> realms) {
        super.putRealms(realms);
        return;
    }


    /**
     *  Removes a Realm from this session.
     *
     *  @param realmId the {@code Id} of the realm
     *  @throws org.osid.NullArgumentException {@code realmId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRealm(org.osid.id.Id realmId) {
        super.removeRealm(realmId);
        return;
    }    
}
