//
// AbstractSupersedingEventSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSupersedingEventSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.SupersedingEventSearchResults {

    private org.osid.calendaring.SupersedingEventList supersedingEvents;
    private final org.osid.calendaring.SupersedingEventQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSupersedingEventSearchResults.
     *
     *  @param supersedingEvents the result set
     *  @param supersedingEventQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>supersedingEvents</code>
     *          or <code>supersedingEventQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSupersedingEventSearchResults(org.osid.calendaring.SupersedingEventList supersedingEvents,
                                            org.osid.calendaring.SupersedingEventQueryInspector supersedingEventQueryInspector) {
        nullarg(supersedingEvents, "superseding events");
        nullarg(supersedingEventQueryInspector, "superseding event query inspectpr");

        this.supersedingEvents = supersedingEvents;
        this.inspector = supersedingEventQueryInspector;

        return;
    }


    /**
     *  Gets the superseding event list resulting from a search.
     *
     *  @return a superseding event list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents() {
        if (this.supersedingEvents == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.SupersedingEventList supersedingEvents = this.supersedingEvents;
        this.supersedingEvents = null;
	return (supersedingEvents);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.SupersedingEventQueryInspector getSupersedingEventQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  superseding event search record <code> Type. </code> This method must
     *  be used to retrieve a supersedingEvent implementing the requested
     *  record.
     *
     *  @param supersedingEventSearchRecordType a supersedingEvent search 
     *         record type 
     *  @return the superseding event search
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(supersedingEventSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventSearchResultsRecord getSupersedingEventSearchResultsRecord(org.osid.type.Type supersedingEventSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.SupersedingEventSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(supersedingEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record superseding event search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSupersedingEventRecord(org.osid.calendaring.records.SupersedingEventSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "superseding event record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
