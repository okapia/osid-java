//
// InvariantMapEnrollmentLookupSession
//
//    Implements an Enrollment lookup service backed by a fixed collection of
//    enrollments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements an Enrollment lookup service backed by a fixed
 *  collection of enrollments. The enrollments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapEnrollmentLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapEnrollmentLookupSession
    implements org.osid.course.program.EnrollmentLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapEnrollmentLookupSession</code> with no
     *  enrollments.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEnrollmentLookupSession</code> with a single
     *  enrollment.
     *  
     *  @param courseCatalog the course catalog
     *  @param enrollment an single enrollment
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code enrollment} is <code>null</code>
     */

      public InvariantMapEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Enrollment enrollment) {
        this(courseCatalog);
        putEnrollment(enrollment);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEnrollmentLookupSession</code> using an array
     *  of enrollments.
     *  
     *  @param courseCatalog the course catalog
     *  @param enrollments an array of enrollments
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code enrollments} is <code>null</code>
     */

      public InvariantMapEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Enrollment[] enrollments) {
        this(courseCatalog);
        putEnrollments(enrollments);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEnrollmentLookupSession</code> using a
     *  collection of enrollments.
     *
     *  @param courseCatalog the course catalog
     *  @param enrollments a collection of enrollments
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code enrollments} is <code>null</code>
     */

      public InvariantMapEnrollmentLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.program.Enrollment> enrollments) {
        this(courseCatalog);
        putEnrollments(enrollments);
        return;
    }
}
