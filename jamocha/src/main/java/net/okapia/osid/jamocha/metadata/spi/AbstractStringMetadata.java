//
// AbstractStringMetadata.java
//
//     Defines a string Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeHashMap;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a string Metadata.
 */

public abstract class AbstractStringMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private long minimum = 0;
    private long maximum = 1024;

    private final TypeHashMap<String> stringMatchTypes = new TypeHashMap<>();
    private final Types stringFormatTypes = new TypeSet();

    private final java.util.Collection<String> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<String> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<String> existing = new java.util.LinkedHashSet<>();    
    

    /**
     *  Constructs a new {@code AbstractStringMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractStringMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.STRING, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractStringMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractStringMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.STRING, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the minimum string length. 
     *
     *  @return the minimum string length
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code>
     */

    @OSID @Override
    public long getMinimumStringLength() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum string length. 
     *
     *  @return the maximum string length
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code>
     */

    @OSID @Override
    public long getMaximumStringLength() {
        return (this.maximum);
    }


    /**
     *  Sets the min and max string length.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    protected void setStringLengthRange(long min, long max) {
        cardinalarg(min, "min string length");
        cardinalarg(max, "max string length");

        if (min > max) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.maximum = min;
        this.maximum = min;

        return;
    }


    /**
     *  Gets the set of acceptable string match types. 
     *
     *  @return a set of string match types or an empty array if not
     *          restricted
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>STRING</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getStringMatchTypes() {
        return (this.stringMatchTypes.keySet().toArray(new org.osid.type.Type[this.stringMatchTypes.size()]));
    }


    /**
     *  Tests if the given string type is supported. 
     *
     *  @param  stringMatchType a string match Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>STRING</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> stringMatchType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        return (this.stringMatchTypes.containsKey(stringMatchType));
    }


    /**
     *  Add support for a string match type.
     *
     *  @param stringMatchType the type of string match
     *  @param expression the expression used to validate string
     *  @throws org.osid.NullArgumentException {@code stringMatchType}
     *          or {@code expression} is {@code null}
     */

    protected void addStringMatchType(org.osid.type.Type stringMatchType, String expression) {
        this.stringMatchTypes.put(stringMatchType, expression);
        return;
    }


    /**
     *  Gets the regular expression of an acceptable string for the given 
     *  string match type. 
     *
     *  @param  stringMatchType a string match type 
     *  @return the regular expression 
     *  @throws org.osid.NullArgumentException <code> stringMatchType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType </code> ) is <code> 
     *          false </code> 
     */

    @OSID @Override
    public String getStringExpression(org.osid.type.Type stringMatchType) {
        if (this.stringMatchTypes.containsKey(stringMatchType)) {
            return (this.stringMatchTypes.get(stringMatchType));
        } else {
            throw new org.osid.UnsupportedException("supportsStringMatchType(" + stringMatchType + 
                                                    ") is false");
        }
    }


    /**
     *  Gets the set of valid string formats. 
     *
     *  @return the set of valid text format types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getStringFormatTypes() {
        return (this.stringFormatTypes.toArray());
    }


    /**
     *  Add support for a string format type.
     *
     *  @param stringFormatType the type of string format
     *  @throws org.osid.NullArgumentException {@code stringFormatType}
     *          is {@code null}
     */

    protected void addStringFormatType(org.osid.type.Type stringFormatType) {
        this.stringFormatTypes.add(stringFormatType);
        return;
    }    


    /**
     *  Gets the set of acceptable string values. 
     *
     *  @return a set of strings or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code>
     */

    @OSID @Override
    public String[] getStringSet() {
        return (this.set.toArray(new String[this.set.size()]));
    }

    
    /**
     *  Sets the string set.
     *
     *  @param values a collection of accepted string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setStringSet(java.util.Collection<String> values) {
        this.set.clear();
        addToStringSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the string set.
     *
     *  @param values a collection of accepted string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToStringSet(java.util.Collection<String> values) {
        nullarg(values, "string set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the string set.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToStringSet(String value) {
        nullarg(value, "string value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the string set.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromStringSet(String value) {
        nullarg(value, "string value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the string set.
     */

    protected void clearStringSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default string values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default string values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          STRING </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public String[] getDefaultStringValues() {
        return (this.defvals.toArray(new String[this.defvals.size()]));
    }


    /**
     *  Sets the default string set.
     *
     *  @param values a collection of default string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultStringValues(java.util.Collection<String> values) {
        clearDefaultStringValues();
        addDefaultStringValues(values);
        return;
    }


    /**
     *  Adds a collection of default string values.
     *
     *  @param values a collection of default string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultStringValues(java.util.Collection<String> values) {
        nullarg(values, "default string values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultStringValue(String value) {
        nullarg(value, "default string value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultStringValue(String value) {
        nullarg(value, "default string value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default string values.
     */

    protected void clearDefaultStringValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing string values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing string values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          STRING </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public String[] getExistingStringValues() {
        return (this.existing.toArray(new String[this.existing.size()]));
    }


    /**
     *  Sets the existing string set.
     *
     *  @param values a collection of existing string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingStringValues(java.util.Collection<String> values) {
        clearExistingStringValues();
        addExistingStringValues(values);
        return;
    }


    /**
     *  Adds a collection of existing string values.
     *
     *  @param values a collection of existing string values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingStringValues(java.util.Collection<String> values) {
        nullarg(values, "existing string values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingStringValue(String value) {
        nullarg(value, "existing string value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingStringValue(String value) {
        nullarg(value, "existing string value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing string values.
     */

    protected void clearExistingStringValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }            
}