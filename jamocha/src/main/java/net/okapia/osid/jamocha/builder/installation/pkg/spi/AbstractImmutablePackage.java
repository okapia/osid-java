//
// AbstractImmutablePackage.java
//
//     Wraps a mutable Package to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Package</code> to hide modifiers. This
 *  wrapper provides an immutized Package from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying pkg whose state changes are visible.
 */

public abstract class AbstractImmutablePackage
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableSourceableOsidObject
    implements org.osid.installation.Package {

    private final org.osid.installation.Package pkg;


    /**
     *  Constructs a new <code>AbstractImmutablePackage</code>.
     *
     *  @param pkg the package to immutablize
     *  @throws org.osid.NullArgumentException <code>pkg</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePackage(org.osid.installation.Package pkg) {
        super(pkg);
        this.pkg = pkg;
        return;
    }


    /**
     *  Gets the version of this package. 
     *
     *  @return the package version 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.pkg.getVersion());
    }


    /**
     *  Gets the copyright of this package. 
     *
     *  @return the copyright 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        return (this.pkg.getCopyright());
    }


    /**
     *  Tests if the provider requests acknowledgement of the license. 
     *
     *  @return <code> true </code> if the consumer should acknowledge the 
     *          terms in the license, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requestsLicenseAcknowledgement() {
        return (this.pkg.requestsLicenseAcknowledgement());
    }


    /**
     *  Gets the creator or author of this package. 
     *
     *  @return the creator <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreatorId() {
        return (this.pkg.getCreatorId());
    }


    /**
     *  Gets the creator <code> Id </code> of this package. 
     *
     *  @return the creator 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCreator()
        throws org.osid.OperationFailedException {

        return (this.pkg.getCreator());
    }


    /**
     *  Gets the release date of this package. 
     *
     *  @return the timestamp of this package 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReleaseDate() {
        return (this.pkg.getReleaseDate());
    }


    /**
     *  Gets the package <code> Ids </code> on which this package
     *  directly depends.
     *
     *  @return the package dependency <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getDependencyIds() {
        return (this.pkg.getDependencyIds());
    }


    /**
     *  Gets the packages on which this package directly depends.
     *
     *  @return the package dependencies 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependencies()
        throws org.osid.OperationFailedException {

        return (this.pkg.getDependencies());
    }


    /**
     *  Gets a url for this package. The url may point to an external project 
     *  or product site. If no url is available an empty string is returned. 
     *
     *  @return the url for this package 
     */

    @OSID @Override
    public String getURL() {
        return (this.pkg.getURL());
    }


    /**
     *  Gets the installation content <code> Ids. </code> 
     *
     *  @return the installation content <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.IdList getInstallationContentIds()
        throws org.osid.OperationFailedException {

        return (this.pkg.getInstallationContentIds());
    }


    /**
     *  Gets the installation contents. 
     *
     *  @return the installation contents 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.InstallationContentList getInstallationContents()
        throws org.osid.OperationFailedException {

        return (this.pkg.getInstallationContents());
    }


    /**
     *  Gets the package record corresponding to the given <code> Package 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> packageRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(packageRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  packageRecordType the type of the record to retrieve 
     *  @return the package record 
     *  @throws org.osid.NullArgumentException <code> packageRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(packageRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.records.PackageRecord getPackageRecord(org.osid.type.Type packageRecordType)
        throws org.osid.OperationFailedException {

        return (this.pkg.getPackageRecord(packageRecordType));
    }
}

