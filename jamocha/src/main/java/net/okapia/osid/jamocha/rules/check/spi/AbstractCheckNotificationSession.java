//
// AbstractCheckNotificationSession.java
//
//     A template for making CheckNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Check} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Check} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for check entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCheckNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.rules.check.CheckNotificationSession {

    private boolean federated = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();


    /**
     *  Gets the {@code Engine/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }

    
    /**
     *  Gets the {@code Engine} associated with this 
     *  session.
     *
     *  @return the {@code Engine} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the {@code Engine}.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can register for {@code Check}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a
     *  {@code PERMISSION_DENIED}. This is intended as a hint to
     *  an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCheckNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include checks in engines which are children of this
     *  engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new
     *  checks. {@code CheckReceiver.newCheck()} is
     *  invoked when a new {@code Check} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewChecks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated
     *  checks. {@code CheckReceiver.changedCheck()} is
     *  invoked when a check is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedChecks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated
     *  check. {@code CheckReceiver.changedCheck()} is
     *  invoked when the specified check is changed.
     *
     *  @param checkId the {@code Id} of the {@code Check} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code checkId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCheck(org.osid.id.Id checkId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted
     *  checks. {@code CheckReceiver.deletedCheck()} is
     *  invoked when a check is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedChecks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted
     *  check. {@code CheckReceiver.deletedCheck()} is
     *  invoked when the specified check is deleted.
     *
     *  @param checkId the {@code Id} of the
     *          {@code Check} to monitor
     *  @throws org.osid.NullArgumentException {@code checkId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCheck(org.osid.id.Id checkId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }

}
