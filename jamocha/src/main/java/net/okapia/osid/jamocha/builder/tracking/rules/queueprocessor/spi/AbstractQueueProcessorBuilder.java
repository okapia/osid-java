//
// AbstractQueueProcessor.java
//
//     Defines a QueueProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.rules.queueprocessor.spi;


/**
 *  Defines a <code>QueueProcessor</code> builder.
 */

public abstract class AbstractQueueProcessorBuilder<T extends AbstractQueueProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.tracking.rules.queueprocessor.QueueProcessorMiter queueProcessor;


    /**
     *  Constructs a new <code>AbstractQueueProcessorBuilder</code>.
     *
     *  @param queueProcessor the queue processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractQueueProcessorBuilder(net.okapia.osid.jamocha.builder.tracking.rules.queueprocessor.QueueProcessorMiter queueProcessor) {
        super(queueProcessor);
        this.queueProcessor = queueProcessor;
        return;
    }


    /**
     *  Builds the queue processor.
     *
     *  @return the new queue processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.tracking.rules.QueueProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.tracking.rules.queueprocessor.QueueProcessorValidator(getValidations())).validate(this.queueProcessor);
        return (new net.okapia.osid.jamocha.builder.tracking.rules.queueprocessor.ImmutableQueueProcessor(this.queueProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the queue processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.tracking.rules.queueprocessor.QueueProcessorMiter getMiter() {
        return (this.queueProcessor);
    }


    /**
     *  Adds a QueueProcessor record.
     *
     *  @param record a queue processor record
     *  @param recordType the type of queue processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.tracking.rules.records.QueueProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addQueueProcessorRecord(record, recordType);
        return (self());
    }
}       


