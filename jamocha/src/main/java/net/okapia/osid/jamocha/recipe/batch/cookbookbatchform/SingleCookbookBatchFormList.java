//
// SingleCookbookBatchFormList.java
//
//     Implements a CookbookBatchFormList based on a single CookbookBatchForm.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.batch.cookbookbatchform;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a ObjectList based on a single CookbookBatchForm.
 */

public final class SingleCookbookBatchFormList
    extends net.okapia.osid.jamocha.recipe.batch.cookbookbatchform.spi.AbstractCookbookBatchFormList
    implements org.osid.recipe.batch.CookbookBatchFormList {

    private final org.osid.recipe.batch.CookbookBatchForm cookbookBatchForm;
    private boolean read = false;


    /**
     *  Creates a new <code>SingleCookbookBatchFormList</code>.
     *
     *  @param cookbookBatchForm a cookbookbatchform
     *  @throws org.osid.NullArgumentException <code>cookbookBatchForm</code>
     *          is <code>null</code>
     */

    public SingleCookbookBatchFormList(org.osid.recipe.batch.CookbookBatchForm cookbookBatchForm) {
        nullarg(cookbookBatchForm, "cookbookbatchform");
        this.cookbookBatchForm = cookbookBatchForm;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code>true</code> if more elements are available in
     *          this list, <code>false</code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.read) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code>CookbookBatchForm</code> in this list. 
     *
     *  @return the next <code>CookbookBatchForm</code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code>CookbookBatchForm</code> is available before
     *          calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.batch.CookbookBatchForm getNextCookbookBatchForm()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.read = true;
            return (this.cookbookBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in cookbookbatchform list");
        }
    }
}
