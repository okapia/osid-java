//
// AbstractAdapterPersonLookupSession.java
//
//    A Person lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Person lookup session adapter.
 */

public abstract class AbstractAdapterPersonLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.PersonLookupSession {

    private final org.osid.personnel.PersonLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPersonLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPersonLookupSession(org.osid.personnel.PersonLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Realm/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Realm Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@code Realm} associated with this session.
     *
     *  @return the {@code Realm} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@code Person} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPersons() {
        return (this.session.canLookupPersons());
    }


    /**
     *  A complete view of the {@code Person} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePersonView() {
        this.session.useComparativePersonView();
        return;
    }


    /**
     *  A complete view of the {@code Person} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPersonView() {
        this.session.usePlenaryPersonView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include persons in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    
     
    /**
     *  Gets the {@code Person} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Person} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Person} and
     *  retained for compatibility.
     *
     *  @param personId {@code Id} of the {@code Person}
     *  @return the person
     *  @throws org.osid.NotFoundException {@code personId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code personId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Person getPerson(org.osid.id.Id personId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPerson(personId));
    }


    /**
     *  Gets a {@code PersonList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  persons specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Persons} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  personIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Person} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code personIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByIds(org.osid.id.IdList personIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPersonsByIds(personIds));
    }


    /**
     *  Gets a {@code PersonList} corresponding to the given
     *  person genus {@code Type} which does not include
     *  persons of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned {@code Person} list
     *  @throws org.osid.NullArgumentException
     *          {@code personGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPersonsByGenusType(personGenusType));
    }


    /**
     *  Gets a {@code PersonList} corresponding to the given
     *  person genus {@code Type} and include any additional
     *  persons with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned {@code Person} list
     *  @throws org.osid.NullArgumentException
     *          {@code personGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByParentGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPersonsByParentGenusType(personGenusType));
    }


    /**
     *  Gets a {@code PersonList} containing the given
     *  person record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  personRecordType a person record type 
     *  @return the returned {@code Person} list
     *  @throws org.osid.NullArgumentException
     *          {@code personRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByRecordType(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPersonsByRecordType(personRecordType));
    }


    /**
     *  Gets all {@code Persons}. 
     *
     *  In plenary mode, the returned list contains all known
     *  persons or an error results. Otherwise, the returned list
     *  may contain only those persons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Persons} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPersons());
    }
}
