//
// MutableGradebookList.java
//
//     Implements a GradebookList. This list allows Gradebooks to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook;


/**
 *  <p>Implements a GradebookList. This list allows Gradebooks to be
 *  added after this gradebook has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this gradebook must
 *  invoke <code>eol()</code> when there are no more gradebooks to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>GradebookList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more gradebooks are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more gradebooks to be added.</p>
 */

public final class MutableGradebookList
    extends net.okapia.osid.jamocha.grading.gradebook.spi.AbstractMutableGradebookList
    implements org.osid.grading.GradebookList {


    /**
     *  Creates a new empty <code>MutableGradebookList</code>.
     */

    public MutableGradebookList() {
        super();
    }


    /**
     *  Creates a new <code>MutableGradebookList</code>.
     *
     *  @param gradebook a <code>Gradebook</code>
     *  @throws org.osid.NullArgumentException <code>gradebook</code>
     *          is <code>null</code>
     */

    public MutableGradebookList(org.osid.grading.Gradebook gradebook) {
        super(gradebook);
        return;
    }


    /**
     *  Creates a new <code>MutableGradebookList</code>.
     *
     *  @param array an array of gradebooks
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableGradebookList(org.osid.grading.Gradebook[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableGradebookList</code>.
     *
     *  @param collection a java.util.Collection of gradebooks
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableGradebookList(java.util.Collection<org.osid.grading.Gradebook> collection) {
        super(collection);
        return;
    }
}
