//
// AbstractCanonicalUnitProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCanonicalUnitProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchResults {

    private org.osid.offering.rules.CanonicalUnitProcessorEnablerList canonicalUnitProcessorEnablers;
    private final org.osid.offering.rules.CanonicalUnitProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCanonicalUnitProcessorEnablerSearchResults.
     *
     *  @param canonicalUnitProcessorEnablers the result set
     *  @param canonicalUnitProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>canonicalUnitProcessorEnablers</code>
     *          or <code>canonicalUnitProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCanonicalUnitProcessorEnablerSearchResults(org.osid.offering.rules.CanonicalUnitProcessorEnablerList canonicalUnitProcessorEnablers,
                                            org.osid.offering.rules.CanonicalUnitProcessorEnablerQueryInspector canonicalUnitProcessorEnablerQueryInspector) {
        nullarg(canonicalUnitProcessorEnablers, "canonical unit processor enablers");
        nullarg(canonicalUnitProcessorEnablerQueryInspector, "canonical unit processor enabler query inspectpr");

        this.canonicalUnitProcessorEnablers = canonicalUnitProcessorEnablers;
        this.inspector = canonicalUnitProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the canonical unit processor enabler list resulting from a search.
     *
     *  @return a canonical unit processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerList getCanonicalUnitProcessorEnablers() {
        if (this.canonicalUnitProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.rules.CanonicalUnitProcessorEnablerList canonicalUnitProcessorEnablers = this.canonicalUnitProcessorEnablers;
        this.canonicalUnitProcessorEnablers = null;
	return (canonicalUnitProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQueryInspector getCanonicalUnitProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  canonical unit processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a canonicalUnitProcessorEnabler implementing the requested
     *  record.
     *
     *  @param canonicalUnitProcessorEnablerSearchRecordType a canonicalUnitProcessorEnabler search 
     *         record type 
     *  @return the canonical unit processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(canonicalUnitProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchResultsRecord getCanonicalUnitProcessorEnablerSearchResultsRecord(org.osid.type.Type canonicalUnitProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(canonicalUnitProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record canonical unit processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCanonicalUnitProcessorEnablerRecord(org.osid.offering.rules.records.CanonicalUnitProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "canonical unit processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
