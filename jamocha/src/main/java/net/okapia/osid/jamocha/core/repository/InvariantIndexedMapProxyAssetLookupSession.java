//
// InvariantIndexedMapProxyAssetLookupSession
//
//    Implements an Asset lookup service backed by a fixed
//    collection of assets indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Asset lookup service backed by a fixed
 *  collection of assets. The assets are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some assets may be compatible
 *  with more types than are indicated through these asset
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyAssetLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractIndexedMapAssetLookupSession
    implements org.osid.repository.AssetLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAssetLookupSession}
     *  using an array of assets.
     *
     *  @param repository the repository
     *  @param assets an array of assets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code assets} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                         org.osid.repository.Asset[] assets, 
                                                         org.osid.proxy.Proxy proxy) {

        setRepository(repository);
        setSessionProxy(proxy);
        putAssets(assets);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAssetLookupSession}
     *  using a collection of assets.
     *
     *  @param repository the repository
     *  @param assets a collection of assets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code assets} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                         java.util.Collection<? extends org.osid.repository.Asset> assets,
                                                         org.osid.proxy.Proxy proxy) {

        setRepository(repository);
        setSessionProxy(proxy);
        putAssets(assets);

        return;
    }
}
