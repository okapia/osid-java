//
// AbstractQueryProductLookupSession.java
//
//    An inline adapter that maps a ProductLookupSession to
//    a ProductQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProductLookupSession to
 *  a ProductQuerySession.
 */

public abstract class AbstractQueryProductLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractProductLookupSession
    implements org.osid.ordering.ProductLookupSession {

    private final org.osid.ordering.ProductQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProductLookupSession.
     *
     *  @param querySession the underlying product query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProductLookupSession(org.osid.ordering.ProductQuerySession querySession) {
        nullarg(querySession, "product query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Store</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform <code>Product</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProducts() {
        return (this.session.canSearchProducts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include products in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the <code>Product</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Product</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Product</code> and
     *  retained for compatibility.
     *
     *  @param  productId <code>Id</code> of the
     *          <code>Product</code>
     *  @return the product
     *  @throws org.osid.NotFoundException <code>productId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>productId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct(org.osid.id.Id productId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchId(productId, true);
        org.osid.ordering.ProductList products = this.session.getProductsByQuery(query);
        if (products.hasNext()) {
            return (products.getNextProduct());
        } 
        
        throw new org.osid.NotFoundException(productId + " not found");
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  products specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Products</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  productIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>productIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByIds(org.osid.id.IdList productIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();

        try (org.osid.id.IdList ids = productIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  product genus <code>Type</code> which does not include
     *  products of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchGenusType(productGenusType, true);
        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  product genus <code>Type</code> and include any additional
     *  products with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByParentGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchParentGenusType(productGenusType, true);
        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets a <code>ProductList</code> containing the given
     *  product record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  productRecordType a product record type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByRecordType(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchRecordType(productRecordType, true);
        return (this.session.getProductsByQuery(query));
    }

    
    /**
     *  Gets the <code> Product </code> specified by its product
     *  code.
     *
     *  @param  code a product code 
     *  @return the product 
     *  @throws org.osid.NotFoundException <code> code </code> not found 
     *  @throws org.osid.NullArgumentException <code> code </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByCode(String code)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchCode(code, getStringMatchType(), true);
        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets a <code> ProductList </code> containing the given price
     *  schedule.  In plenary mode, the returned list contains all
     *  known products or an error results. Otherwise, the returned
     *  list may contain only those products that are accessible
     *  through this session.
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @return the product list 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.ProductQuery query = getQuery();
        query.matchPriceScheduleId(priceScheduleId, true);
        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets all <code>Products</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Products</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProducts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.ProductQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProductsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ordering.ProductQuery getQuery() {
        org.osid.ordering.ProductQuery query = this.session.getProductQuery();
        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
