//
// AbstractAdapterLogEntryLookupSession.java
//
//    A LogEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.logging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A LogEntry lookup session adapter.
 */

public abstract class AbstractAdapterLogEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.logging.LogEntryLookupSession {

    private final org.osid.logging.LogEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLogEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLogEntryLookupSession(org.osid.logging.LogEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Log/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Log Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getLogId() {
        return (this.session.getLogId());
    }


    /**
     *  Gets the {@code Log} associated with this session.
     *
     *  @return the {@code Log} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLog());
    }


    /**
     *  Tests if this user can perform {@code LogEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canReadLog() {
        return (this.session.canReadLog());
    }


    /**
     *  A complete view of the {@code LogEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLogEntryView() {
        this.session.useComparativeLogEntryView();
        return;
    }


    /**
     *  A complete view of the {@code LogEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLogEntryView() {
        this.session.usePlenaryLogEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in logs which are children
     *  of this log in the log hierarchy.
     */

    @OSID @Override
    public void useFederatedLogView() {
        this.session.useFederatedLogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this log only.
     */

    @OSID @Override
    public void useIsolatedLogView() {
        this.session.useIsolatedLogView();
        return;
    }
    
     
    /**
     *  Gets the {@code LogEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code LogEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code LogEntry} and
     *  retained for compatibility.
     *
     *  @param logEntryId {@code Id} of the {@code LogEntry}
     *  @return the log entry
     *  @throws org.osid.NotFoundException {@code logEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code logEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntry(logEntryId));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code LogEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  logEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByIds(org.osid.id.IdList logEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByIds(logEntryIds));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  log entry genus {@code Type} which does not include
     *  log entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByGenusType(logEntryGenusType));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given
     *  log entry genus {@code Type} and include any additional
     *  log entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByParentGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByParentGenusType(logEntryGenusType));
    }


    /**
     *  Gets a {@code LogEntryList} containing the given
     *  log entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryRecordType a logEntry record type 
     *  @return the returned {@code LogEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code logEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByRecordType(logEntryRecordType));
    }

    
    /**
     *  Gets a {@code LogEntryList} filtering the list to log entries
     *  including and above the given priority {@code Type.} In
     *  plenary mode, the returned list contains all known entries or
     *  an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  @param  priorityType a log entry priority type 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.NullArgumentException {@code priorityType} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityType(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesByPriorityType(priorityType));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given time
     *  interval inclusive. {@code} In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code start} or {@code
     *          end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByDate(org.osid.calendaring.DateTime start, 
                                                             org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesByDate(start, end));
    }
               
               
    /**
     *  Gets a {@code LogEntryList} corresponding to the given time
     *  interval inclusive filtering the list to log entries including
     *  and above the given priority {@code Type.} In plenary mode,
     *  the returned list contains all known entries or an error
     *  results.  Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  priorityType a log entry priority type 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code priorityType,
     *         start}, or {@code end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityTypeAndDate(org.osid.type.Type priorityType, 
                                                                            org.osid.calendaring.DateTime start, 
                                                                            org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesByPriorityTypeAndDate(priorityType, start, end));
    }


    /**
     *  Gets a {@code LogEntryList} for an agent associated with the
     *  given resource. In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesForResource(resourceId));
    }

    
    /**
     *  Gets a {@code LogEntryList} corresponding to the given time
     *  interval inclusive for an agent associated with the given
     *  resource.  In plenary mode, the returned list contains all
     *  known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          start} or {@code end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByDateForResource(org.osid.id.Id resourceId, 
                                                                        org.osid.calendaring.DateTime start, 
                                                                        org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getLogEntriesByDateForResource(resourceId, start, end));
    }


    /**
     *  Gets a {@code LogEntryList} corresponding to the given time
     *  interval inclusive for an agent associated with the given
     *  resource filtering the list to log entries including and above
     *  the given priority {@code Type.} In plenary mode, the returned
     *  list contains all known entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  priorityType a log entry priority type 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned {@code LogEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          priorityType, start} or {@code end} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityTypeAndDateForResource(org.osid.id.Id resourceId, 
                                                                                       org.osid.type.Type priorityType, 
                                                                                       org.osid.calendaring.DateTime start, 
                                                                                       org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntriesByPriorityTypeAndDateForResource(resourceId, priorityType, start, end));
    }


    /**
     *  Gets all {@code LogEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code LogEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLogEntries());
    }
}
