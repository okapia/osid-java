//
// MutableMapProxyChecklistLookupSession
//
//    Implements a Checklist lookup service backed by a collection of
//    checklists that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Checklist lookup service backed by a collection of
 *  checklists. The checklists are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of checklists can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyChecklistLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractMapChecklistLookupSession
    implements org.osid.checklist.ChecklistLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyChecklistLookupSession} with no
     *  checklists.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyChecklistLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyChecklistLookupSession} with a
     *  single checklist.
     *
     *  @param checklist a checklist
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyChecklistLookupSession(org.osid.checklist.Checklist checklist, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyChecklistLookupSession} using an
     *  array of checklists.
     *
     *  @param checklists an array of checklists
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklists} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyChecklistLookupSession(org.osid.checklist.Checklist[] checklists, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklists(checklists);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyChecklistLookupSession} using
     *  a collection of checklists.
     *
     *  @param checklists a collection of checklists
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklists} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyChecklistLookupSession(java.util.Collection<? extends org.osid.checklist.Checklist> checklists,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putChecklists(checklists);
        return;
    }

    
    /**
     *  Makes a {@code Checklist} available in this session.
     *
     *  @param checklist an checklist
     *  @throws org.osid.NullArgumentException {@code checklist{@code 
     *          is {@code null}
     */

    @Override
    public void putChecklist(org.osid.checklist.Checklist checklist) {
        super.putChecklist(checklist);
        return;
    }


    /**
     *  Makes an array of checklists available in this session.
     *
     *  @param checklists an array of checklists
     *  @throws org.osid.NullArgumentException {@code checklists{@code 
     *          is {@code null}
     */

    @Override
    public void putChecklists(org.osid.checklist.Checklist[] checklists) {
        super.putChecklists(checklists);
        return;
    }


    /**
     *  Makes collection of checklists available in this session.
     *
     *  @param checklists
     *  @throws org.osid.NullArgumentException {@code checklist{@code 
     *          is {@code null}
     */

    @Override
    public void putChecklists(java.util.Collection<? extends org.osid.checklist.Checklist> checklists) {
        super.putChecklists(checklists);
        return;
    }


    /**
     *  Removes a Checklist from this session.
     *
     *  @param checklistId the {@code Id} of the checklist
     *  @throws org.osid.NullArgumentException {@code checklistId{@code  is
     *          {@code null}
     */

    @Override
    public void removeChecklist(org.osid.id.Id checklistId) {
        super.removeChecklist(checklistId);
        return;
    }    
}
