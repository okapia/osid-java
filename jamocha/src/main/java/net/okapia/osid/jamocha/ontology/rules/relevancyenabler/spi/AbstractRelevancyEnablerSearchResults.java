//
// AbstractRelevancyEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRelevancyEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ontology.rules.RelevancyEnablerSearchResults {

    private org.osid.ontology.rules.RelevancyEnablerList relevancyEnablers;
    private final org.osid.ontology.rules.RelevancyEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRelevancyEnablerSearchResults.
     *
     *  @param relevancyEnablers the result set
     *  @param relevancyEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>relevancyEnablers</code>
     *          or <code>relevancyEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRelevancyEnablerSearchResults(org.osid.ontology.rules.RelevancyEnablerList relevancyEnablers,
                                            org.osid.ontology.rules.RelevancyEnablerQueryInspector relevancyEnablerQueryInspector) {
        nullarg(relevancyEnablers, "relevancy enablers");
        nullarg(relevancyEnablerQueryInspector, "relevancy enabler query inspectpr");

        this.relevancyEnablers = relevancyEnablers;
        this.inspector = relevancyEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the relevancy enabler list resulting from a search.
     *
     *  @return a relevancy enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablers() {
        if (this.relevancyEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ontology.rules.RelevancyEnablerList relevancyEnablers = this.relevancyEnablers;
        this.relevancyEnablers = null;
	return (relevancyEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ontology.rules.RelevancyEnablerQueryInspector getRelevancyEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  relevancy enabler search record <code> Type. </code> This method must
     *  be used to retrieve a relevancyEnabler implementing the requested
     *  record.
     *
     *  @param relevancyEnablerSearchRecordType a relevancyEnabler search 
     *         record type 
     *  @return the relevancy enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(relevancyEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerSearchResultsRecord getRelevancyEnablerSearchResultsRecord(org.osid.type.Type relevancyEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ontology.rules.records.RelevancyEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(relevancyEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record relevancy enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRelevancyEnablerRecord(org.osid.ontology.rules.records.RelevancyEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "relevancy enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
