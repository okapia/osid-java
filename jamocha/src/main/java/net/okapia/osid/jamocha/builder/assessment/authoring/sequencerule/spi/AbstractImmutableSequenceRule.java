//
// AbstractImmutableSequenceRule.java
//
//     Wraps a mutable SequenceRule to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>SequenceRule</code> to hide modifiers. This
 *  wrapper provides an immutized SequenceRule from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying sequenceRule whose state changes are visible.
 */

public abstract class AbstractImmutableSequenceRule
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.assessment.authoring.SequenceRule {

    private final org.osid.assessment.authoring.SequenceRule sequenceRule;


    /**
     *  Constructs a new <code>AbstractImmutableSequenceRule</code>.
     *
     *  @param sequenceRule the sequence rule to immutablize
     *  @throws org.osid.NullArgumentException <code>sequenceRule</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSequenceRule(org.osid.assessment.authoring.SequenceRule sequenceRule) {
        super(sequenceRule);
        this.sequenceRule = sequenceRule;
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> to which this rule belongs. 
     *
     *  @return <code> Id </code> of an assessment part 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentPartId() {
        return (this.sequenceRule.getAssessmentPartId());
    }


    /**
     *  Gets the assessment part to which this rule belongs. 
     *
     *  @return an assessment part 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart()
        throws org.osid.OperationFailedException {

        return (this.sequenceRule.getAssessmentPart());
    }


    /**
     *  Gets the next assessment part <code> Id </code> for success of this 
     *  rule. 
     *
     *  @return <code> Id </code> of an assessment part 
     */

    @OSID @Override
    public org.osid.id.Id getNextAssessmentPartId() {
        return (this.sequenceRule.getNextAssessmentPartId());
    }


    /**
     *  Gets the next assessment part for success of this rule. 
     *
     *  @return an assessment part 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getNextAssessmentPart()
        throws org.osid.OperationFailedException {

        return (this.sequenceRule.getNextAssessmentPart());
    }


    /**
     *  Gets the minimum score expressed as an integer (0-100) for this rule. 
     *
     *  @return minimum score 
     */

    @OSID @Override
    public long getMinimumScore() {
        return (this.sequenceRule.getMinimumScore());
    }


    /**
     *  Gets the maximum score expressed as an integer (0-100) for this rule. 
     *
     *  @return maximum score 
     */

    @OSID @Override
    public long getMaximumScore() {
        return (this.sequenceRule.getMaximumScore());
    }


    /**
     *  Tests if the score is applied to all previous assessment parts. 
     *
     *  @return <code> true </code> if the score is applied to all previous 
     *          assessment parts, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isCumulative() {
        return (this.sequenceRule.isCumulative());
    }


    /**
     *  Gets the list of assessment part <code> Ids </code> the minimum and 
     *  maximum score is applied to. If <code> isCumulative() </code> is 
     *  <code> true, </code> this method may return an empty list. 
     *
     *  @return list of assessment parts 
     */

    @OSID @Override
    public org.osid.id.IdList getAppliedAssessmentPartIds() {
        return (this.sequenceRule.getAppliedAssessmentPartIds());
    }


    /**
     *  Gets the list of assessment parts the minimum and maximum score is 
     *  applied to. If <code> isCumulative() </code> is <code> true, </code> 
     *  this method may return an empty list. 
     *
     *  @return list of assessment parts 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAppliedAssessmentParts()
        throws org.osid.OperationFailedException {

        return (this.sequenceRule.getAppliedAssessmentParts());
    }


    /**
     *  Gets the assessment sequence rule record corresponding to the given 
     *  <code> SequenceRule </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> sequenceRuleRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(sequenceRuleRecordType) </code> is <code> true </code> . 
     *
     *  @param  sequenceRuleRecordType the type of the record to retrieve 
     *  @return the assessment sequence rule record 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(sequenceRuleRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleRecord getSequenceRuleRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        return (this.sequenceRule.getSequenceRuleRecord(sequenceRuleRecordType));
    }
}

