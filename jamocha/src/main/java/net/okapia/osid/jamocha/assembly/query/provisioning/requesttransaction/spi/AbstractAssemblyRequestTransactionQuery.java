//
// AbstractAssemblyRequestTransactionQuery.java
//
//     A RequestTransactionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.requesttransaction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RequestTransactionQuery that stores terms.
 */

public abstract class AbstractAssemblyRequestTransactionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.provisioning.RequestTransactionQuery,
               org.osid.provisioning.RequestTransactionQueryInspector,
               org.osid.provisioning.RequestTransactionSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.RequestTransactionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.RequestTransactionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.RequestTransactionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRequestTransactionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRequestTransactionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getBrokerIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        getAssembler().clearTerms(getBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (getAssembler().getIdTerms(getBrokerIdColumn()));
    }


    /**
     *  Orders the results by the broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBrokerColumn(), style);
        return;
    }


    /**
     *  Gets the BrokerId column name.
     *
     * @return the column name
     */

    protected String getBrokerIdColumn() {
        return ("broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        getAssembler().clearTerms(getBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Gets the Broker column name.
     *
     * @return the column name
     */

    protected String getBrokerColumn() {
        return ("broker");
    }


    /**
     *  Matches transactions with a submit date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getSubmitDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the submit date query terms. 
     */

    @OSID @Override
    public void clearSubmitDateTerms() {
        getAssembler().clearTerms(getSubmitDateColumn());
        return;
    }


    /**
     *  Gets the submit date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSubmitDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getSubmitDateColumn()));
    }


    /**
     *  Orders the results by the submit date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmitDateColumn(), style);
        return;
    }


    /**
     *  Gets the SubmitDate column name.
     *
     * @return the column name
     */

    protected String getSubmitDateColumn() {
        return ("submit_date");
    }


    /**
     *  Sets the submitter <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmitterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSubmitterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the submitter <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubmitterIdTerms() {
        getAssembler().clearTerms(getSubmitterIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmitterIdTerms() {
        return (getAssembler().getIdTerms(getSubmitterIdColumn()));
    }


    /**
     *  Orders the results by the submitter. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitter(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmitterColumn(), style);
        return;
    }


    /**
     *  Gets the SubmitterId column name.
     *
     * @return the column name
     */

    protected String getSubmitterIdColumn() {
        return ("submitter_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSubmitterQuery() {
        throw new org.osid.UnimplementedException("supportsSubmitterQuery() is false");
    }


    /**
     *  Clears the submitter query terms. 
     */

    @OSID @Override
    public void clearSubmitterTerms() {
        getAssembler().clearTerms(getSubmitterColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSubmitterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsSubmitterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSubmitterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmitterSearchOrder() is false");
    }


    /**
     *  Gets the Submitter column name.
     *
     * @return the column name
     */

    protected String getSubmitterColumn() {
        return ("submitter");
    }


    /**
     *  Sets the submitting agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubmittingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getSubmittingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the submitting agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentIdTerms() {
        getAssembler().clearTerms(getSubmittingAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubmittingAgentIdTerms() {
        return (getAssembler().getIdTerms(getSubmittingAgentIdColumn()));
    }


    /**
     *  Orders the results by submitting agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmittingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSubmittingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the SubmittingAgentId column name.
     *
     * @return the column name
     */

    protected String getSubmittingAgentIdColumn() {
        return ("submitting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a submitting agent. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getSubmittingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentQuery() is false");
    }


    /**
     *  Clears the submitting agent query terms. 
     */

    @OSID @Override
    public void clearSubmittingAgentTerms() {
        getAssembler().clearTerms(getSubmittingAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getSubmittingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsSubmittingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getSubmittingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentSearchOrder() is false");
    }


    /**
     *  Gets the SubmittingAgent column name.
     *
     * @return the column name
     */

    protected String getSubmittingAgentColumn() {
        return ("submitting_agent");
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        getAssembler().addIdTerm(getRequestIdColumn(), requestId, match);
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        getAssembler().clearTerms(getRequestIdColumn());
        return;
    }


    /**
     *  Gets the request <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestIdTerms() {
        return (getAssembler().getIdTerms(getRequestIdColumn()));
    }


    /**
     *  Gets the RequestId column name.
     *
     * @return the column name
     */

    protected String getRequestIdColumn() {
        return ("request_id");
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches transactions with any request. 
     *
     *  @param  match <code> true </code> for to match transactions with any 
     *          request, match, <code> false </code> to match transaction with 
     *          no requests 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        getAssembler().addIdWildcardTerm(getRequestColumn(), match);
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        getAssembler().clearTerms(getRequestColumn());
        return;
    }


    /**
     *  Gets the request query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQueryInspector[] getRequestTerms() {
        return (new org.osid.provisioning.RequestQueryInspector[0]);
    }


    /**
     *  Gets the Request column name.
     *
     * @return the column name
     */

    protected String getRequestColumn() {
        return ("request");
    }


    /**
     *  Tests if this requestTransaction supports the given record
     *  <code>Type</code>.
     *
     *  @param  requestTransactionRecordType a request transaction record type 
     *  @return <code>true</code> if the requestTransactionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requestTransactionRecordType) {
        for (org.osid.provisioning.records.RequestTransactionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  requestTransactionRecordType the request transaction record type 
     *  @return the request transaction query record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestTransactionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionQueryRecord getRequestTransactionQueryRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestTransactionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestTransactionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  requestTransactionRecordType the request transaction record type 
     *  @return the request transaction query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestTransactionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionQueryInspectorRecord getRequestTransactionQueryInspectorRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestTransactionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestTransactionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param requestTransactionRecordType the request transaction record type
     *  @return the request transaction search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestTransactionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionSearchOrderRecord getRequestTransactionSearchOrderRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestTransactionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestTransactionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this request transaction. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requestTransactionQueryRecord the request transaction query record
     *  @param requestTransactionQueryInspectorRecord the request transaction query inspector
     *         record
     *  @param requestTransactionSearchOrderRecord the request transaction search order record
     *  @param requestTransactionRecordType request transaction record type
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionQueryRecord</code>,
     *          <code>requestTransactionQueryInspectorRecord</code>,
     *          <code>requestTransactionSearchOrderRecord</code> or
     *          <code>requestTransactionRecordTyperequestTransaction</code> is
     *          <code>null</code>
     */
            
    protected void addRequestTransactionRecords(org.osid.provisioning.records.RequestTransactionQueryRecord requestTransactionQueryRecord, 
                                      org.osid.provisioning.records.RequestTransactionQueryInspectorRecord requestTransactionQueryInspectorRecord, 
                                      org.osid.provisioning.records.RequestTransactionSearchOrderRecord requestTransactionSearchOrderRecord, 
                                      org.osid.type.Type requestTransactionRecordType) {

        addRecordType(requestTransactionRecordType);

        nullarg(requestTransactionQueryRecord, "request transaction query record");
        nullarg(requestTransactionQueryInspectorRecord, "request transaction query inspector record");
        nullarg(requestTransactionSearchOrderRecord, "request transaction search odrer record");

        this.queryRecords.add(requestTransactionQueryRecord);
        this.queryInspectorRecords.add(requestTransactionQueryInspectorRecord);
        this.searchOrderRecords.add(requestTransactionSearchOrderRecord);
        
        return;
    }
}
