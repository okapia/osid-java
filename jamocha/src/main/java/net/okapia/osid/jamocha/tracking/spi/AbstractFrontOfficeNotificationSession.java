//
// AbstractFrontOfficeNotificationSession.java
//
//     A template for making FrontOfficeNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code FrontOffice} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code FrontOffice} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for front office entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractFrontOfficeNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.tracking.FrontOfficeNotificationSession {


    /**
     *  Tests if this user can register for {@code FrontOffice}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForFrontOfficeNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new front offices. {@code
     *  FrontOfficeReceiver.newFrontOffice()} is invoked when a new
     *  {@code FrontOffice} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified front
     *  office. {@code FrontOfficeReceiver.newAncestorFrontOffice()}
     *  is invoked when the specified front office node gets a new
     *  ancestor.
     *
     *  @param frontOfficeId the {@code Id} of the
     *         {@code FrontOffice} node to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewFrontOfficeAncestors(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified front
     *  office. {@code FrontOfficeReceiver.newDescendantFrontOffice()}
     *  is invoked when the specified front office node gets a new
     *  descendant.
     *
     *  @param frontOfficeId the {@code Id} of the
     *         {@code FrontOffice} node to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewFrontOfficeDescendants(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated front offices. {@code
     *  FrontOfficeReceiver.changedFrontOffice()} is invoked when a
     *  front office is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated front office. {@code
     *  FrontOfficeReceiver.changedFrontOffice()} is invoked when the
     *  specified front office is changed.
     *
     *  @param frontOfficeId the {@code Id} of the {@code FrontOffice} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted front offices. {@code
     *  FrontOfficeReceiver.deletedFrontOffice()} is invoked when a
     *  front office is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFrontOffices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted front office. {@code
     *  FrontOfficeReceiver.deletedFrontOffice()} is invoked when the
     *  specified front office is deleted.
     *
     *  @param frontOfficeId the {@code Id} of the
     *          {@code FrontOffice} to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified front office. {@code
     *  FrontOfficeReceiver.deletedAncestor()} is invoked when the
     *  specified front office node loses an ancestor.
     *
     *  @param frontOfficeId the {@code Id} of the
     *         {@code FrontOffice} node to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFrontOfficeAncestors(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified front
     *  office. {@code FrontOfficeReceiver.deletedDescendant()} is
     *  invoked when the specified front office node loses a
     *  descendant.
     *
     *  @param frontOfficeId the {@code Id} of the
     *          {@code FrontOffice} node to monitor
     *  @throws org.osid.NullArgumentException {@code frontOfficeId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedFrontOfficeDescendants(org.osid.id.Id frontOfficeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
