//
// AbstractVaultSearch.java
//
//     A template for making a Vault Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.vault.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing vault searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractVaultSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authorization.VaultSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authorization.records.VaultSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authorization.VaultSearchOrder vaultSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of vaults. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  vaultIds list of vaults
     *  @throws org.osid.NullArgumentException
     *          <code>vaultIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongVaults(org.osid.id.IdList vaultIds) {
        while (vaultIds.hasNext()) {
            try {
                this.ids.add(vaultIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongVaults</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of vault Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getVaultIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  vaultSearchOrder vault search order 
     *  @throws org.osid.NullArgumentException
     *          <code>vaultSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>vaultSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderVaultResults(org.osid.authorization.VaultSearchOrder vaultSearchOrder) {
	this.vaultSearchOrder = vaultSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authorization.VaultSearchOrder getVaultSearchOrder() {
	return (this.vaultSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given vault search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a vault implementing the requested record.
     *
     *  @param vaultSearchRecordType a vault search record
     *         type
     *  @return the vault search record
     *  @throws org.osid.NullArgumentException
     *          <code>vaultSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(vaultSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.VaultSearchRecord getVaultSearchRecord(org.osid.type.Type vaultSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authorization.records.VaultSearchRecord record : this.records) {
            if (record.implementsRecordType(vaultSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(vaultSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vault search. 
     *
     *  @param vaultSearchRecord vault search record
     *  @param vaultSearchRecordType vault search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addVaultSearchRecord(org.osid.authorization.records.VaultSearchRecord vaultSearchRecord, 
                                           org.osid.type.Type vaultSearchRecordType) {

        addRecordType(vaultSearchRecordType);
        this.records.add(vaultSearchRecord);        
        return;
    }
}
