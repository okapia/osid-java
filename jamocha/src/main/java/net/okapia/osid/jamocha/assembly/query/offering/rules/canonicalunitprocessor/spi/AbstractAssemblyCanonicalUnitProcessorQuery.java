//
// AbstractAssemblyCanonicalUnitProcessorQuery.java
//
//     A CanonicalUnitProcessorQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CanonicalUnitProcessorQuery that stores terms.
 */

public abstract class AbstractAssemblyCanonicalUnitProcessorQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidProcessorQuery
    implements org.osid.offering.rules.CanonicalUnitProcessorQuery,
               org.osid.offering.rules.CanonicalUnitProcessorQueryInspector,
               org.osid.offering.rules.CanonicalUnitProcessorSearchOrder {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCanonicalUnitProcessorQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCanonicalUnitProcessorQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to the canonical unit. 
     *
     *  @param  canonicalUnitId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledCanonicalUnitIdColumn(), canonicalUnitId, match);
        return;
    }


    /**
     *  Clears the canonical unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitIdTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitIdColumn());
        return;
    }


    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCanonicalUnitIdTerms() {
        return (getAssembler().getIdTerms(getRuledCanonicalUnitIdColumn()));
    }


    /**
     *  Gets the RuledCanonicalUnitId column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitIdColumn() {
        return ("ruled_canonical_unit_id");
    }


    /**
     *  Tests if a <code> CanonicalUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getRuledCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitQuery() is false");
    }


    /**
     *  Matches mapped to any canonical unit. 
     *
     *  @param  match <code> true </code> for mapped to any canonical unit, 
     *          <code> false </code> to match mapped to no canonicalUnit 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnit(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCanonicalUnitColumn(), match);
        return;
    }


    /**
     *  Clears the canonical unit query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitColumn());
        return;
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getRuledCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the RuledCanonicalUnit column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitColumn() {
        return ("ruled_canonical_unit");
    }


    /**
     *  Matches mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this canonicalUnitProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorRecordType a canonical unit processor record type 
     *  @return <code>true</code> if the canonicalUnitProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        for (org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorRecordType the canonical unit processor record type 
     *  @return the canonical unit processor query record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord getCanonicalUnitProcessorQueryRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  canonicalUnitProcessorRecordType the canonical unit processor record type 
     *  @return the canonical unit processor query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord getCanonicalUnitProcessorQueryInspectorRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param canonicalUnitProcessorRecordType the canonical unit processor record type
     *  @return the canonical unit processor search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorSearchOrderRecord getCanonicalUnitProcessorSearchOrderRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this canonical unit processor. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitProcessorQueryRecord the canonical unit processor query record
     *  @param canonicalUnitProcessorQueryInspectorRecord the canonical unit processor query inspector
     *         record
     *  @param canonicalUnitProcessorSearchOrderRecord the canonical unit processor search order record
     *  @param canonicalUnitProcessorRecordType canonical unit processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorQueryRecord</code>,
     *          <code>canonicalUnitProcessorQueryInspectorRecord</code>,
     *          <code>canonicalUnitProcessorSearchOrderRecord</code> or
     *          <code>canonicalUnitProcessorRecordTypecanonicalUnitProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorRecords(org.osid.offering.rules.records.CanonicalUnitProcessorQueryRecord canonicalUnitProcessorQueryRecord, 
                                      org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord canonicalUnitProcessorQueryInspectorRecord, 
                                      org.osid.offering.rules.records.CanonicalUnitProcessorSearchOrderRecord canonicalUnitProcessorSearchOrderRecord, 
                                      org.osid.type.Type canonicalUnitProcessorRecordType) {

        addRecordType(canonicalUnitProcessorRecordType);

        nullarg(canonicalUnitProcessorQueryRecord, "canonical unit processor query record");
        nullarg(canonicalUnitProcessorQueryInspectorRecord, "canonical unit processor query inspector record");
        nullarg(canonicalUnitProcessorSearchOrderRecord, "canonical unit processor search odrer record");

        this.queryRecords.add(canonicalUnitProcessorQueryRecord);
        this.queryInspectorRecords.add(canonicalUnitProcessorQueryInspectorRecord);
        this.searchOrderRecords.add(canonicalUnitProcessorSearchOrderRecord);
        
        return;
    }
}
