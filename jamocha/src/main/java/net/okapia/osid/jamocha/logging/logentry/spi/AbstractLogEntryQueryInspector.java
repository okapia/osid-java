//
// AbstractLogEntryQueryInspector.java
//
//     A template for making a LogEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for log entries.
 */

public abstract class AbstractLogEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.logging.LogEntryQueryInspector {

    private final java.util.Collection<org.osid.logging.records.LogEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the prirority query terms. 
     *
     *  @return the priority terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getPriorityTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the minimum prirority query terms. 
     *
     *  @return the minimum priority terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getMinimumPriorityTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the timestamp query terms. 
     *
     *  @return the timestamp terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimestampTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the log <code> Id </code> query terms. 
     *
     *  @return the log <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the log query terms. 
     *
     *  @return the log terms 
     */

    @OSID @Override
    public org.osid.logging.LogQueryInspector[] getLogTerms() {
        return (new org.osid.logging.LogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given log entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a log entry implementing the requested record.
     *
     *  @param logEntryRecordType a log entry record type
     *  @return the log entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryQueryInspectorRecord getLogEntryQueryInspectorRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.logging.records.LogEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry query. 
     *
     *  @param logEntryQueryInspectorRecord log entry query inspector
     *         record
     *  @param logEntryRecordType logEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLogEntryQueryInspectorRecord(org.osid.logging.records.LogEntryQueryInspectorRecord logEntryQueryInspectorRecord, 
                                                   org.osid.type.Type logEntryRecordType) {

        addRecordType(logEntryRecordType);
        nullarg(logEntryRecordType, "log entry record type");
        this.records.add(logEntryQueryInspectorRecord);        
        return;
    }
}
