//
// AbstractCalendaringCycleProxyManager.java
//
//     An adapter for a CalendaringCycleProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CalendaringCycleProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCalendaringCycleProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.calendaring.cycle.CalendaringCycleProxyManager>
    implements org.osid.calendaring.cycle.CalendaringCycleProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringCycleProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCalendaringCycleProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringCycleProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCalendaringCycleProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a cyclic event lookup service is supported. a cyclic event 
     *  lookup service defines methods to access cyclic events. 
     *
     *  @return true if cyclic event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventLookup() {
        return (getAdapteeManager().supportsCyclicEventLookup());
    }


    /**
     *  Tests if a cyclic event query service is supported. 
     *
     *  @return <code> true </code> if cyclic event query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (getAdapteeManager().supportsCyclicEventQuery());
    }


    /**
     *  Tests if a cyclic event search service is supported. 
     *
     *  @return <code> true </code> if cyclic event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearch() {
        return (getAdapteeManager().supportsCyclicEventSearch());
    }


    /**
     *  Tests if a cyclic event administrative service is supported. 
     *
     *  @return <code> true </code> if cyclic event admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventAdmin() {
        return (getAdapteeManager().supportsCyclicEventAdmin());
    }


    /**
     *  Tests if cyclic event notification is supported. Messages may be sent 
     *  when cyclic events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if cyclic event notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventNotification() {
        return (getAdapteeManager().supportsCyclicEventNotification());
    }


    /**
     *  Tests if a cyclic event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if cyclic event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventCalendar() {
        return (getAdapteeManager().supportsCyclicEventCalendar());
    }


    /**
     *  Tests if a cyclic event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if cyclic event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventCalendarAssignment() {
        return (getAdapteeManager().supportsCyclicEventCalendarAssignment());
    }


    /**
     *  Tests if cyclic event smart calendaring is available. 
     *
     *  @return <code> true </code> if cyclic event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSmartCalendar() {
        return (getAdapteeManager().supportsCyclicEventSmartCalendar());
    }


    /**
     *  Tests if a session to look up associations between events and cyclic 
     *  events is available. 
     *
     *  @return <code> true </code> if an event to cyclic event lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCyclicEventLookup() {
        return (getAdapteeManager().supportsEventCyclicEventLookup());
    }


    /**
     *  Tests if a session for manually assigning events to cyclic events is 
     *  available. 
     *
     *  @return <code> true </code> if an event to cyclic event assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCyclicEventAssignment() {
        return (getAdapteeManager().supportsEventCyclicEventAssignment());
    }


    /**
     *  Tests if a cyclic time period lookup service is supported. 
     *
     *  @return <code> true </code> if cyclic time period lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodLookup() {
        return (getAdapteeManager().supportsCyclicTimePeriodLookup());
    }


    /**
     *  Tests if a cyclic time period search service is supported. 
     *
     *  @return <code> true </code> if cyclic time period search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSearch() {
        return (getAdapteeManager().supportsCyclicTimePeriodSearch());
    }


    /**
     *  Tests if a cyclic time period administrative service is supported. 
     *
     *  @return <code> true </code> if cyclic time period admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodAdmin() {
        return (getAdapteeManager().supportsCyclicTimePeriodAdmin());
    }


    /**
     *  Tests if cyclic time period notification is supported. Messages may be 
     *  sent when cyclic time periods are created, modified, or deleted. 
     *
     *  @return <code> true </code> if cyclic time period notification is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodNotification() {
        return (getAdapteeManager().supportsCyclicTimePeriodNotification());
    }


    /**
     *  Tests if a cyclic time period to calendar lookup session is available. 
     *
     *  @return <code> true </code> if cyclic time period calendar lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodCalendar() {
        return (getAdapteeManager().supportsCyclicTimePeriodCalendar());
    }


    /**
     *  Tests if a cyclic time period to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if cyclic time period calendar assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodCalendarAssignment() {
        return (getAdapteeManager().supportsCyclicTimePeriodCalendarAssignment());
    }


    /**
     *  Tests if cyclic time period smart calendaring is available. 
     *
     *  @return <code> true </code> if cyclic time period smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSmartCalendar() {
        return (getAdapteeManager().supportsCyclicTimePeriodSmartCalendar());
    }


    /**
     *  Tests if a session to look up associations between time periods and 
     *  cyclic time periods is available. 
     *
     *  @return <code> true </code> if a time period to cyclic time period 
     *          lookup session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCyclicTimePeriodLookup() {
        return (getAdapteeManager().supportsTimePeriodCyclicTimePeriodLookup());
    }


    /**
     *  Tests if a session for manually assigning time periods to cyclic time 
     *  periods is available. 
     *
     *  @return <code> true </code> if a time period to cyclic time period 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCyclicTimePeriodAssignment() {
        return (getAdapteeManager().supportsTimePeriodCyclicTimePeriodAssignment());
    }


    /**
     *  Tests if a calendaring cycle batch service is available. 
     *
     *  @return <code> true </code> if a calndaring cycle batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringCycleBatch() {
        return (getAdapteeManager().supportsCalendaringCycleBatch());
    }


    /**
     *  Gets the supported <code> CyclicEvent </code> record types. 
     *
     *  @return a list containing the supported <code> CyclicEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicEventRecordTypes() {
        return (getAdapteeManager().getCyclicEventRecordTypes());
    }


    /**
     *  Tests if the given <code> CyclicEvent </code> record type is 
     *  supported. 
     *
     *  @param  cyclicEventRecordType a <code> Type </code> indicating a 
     *          <code> CyclicEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cyclicEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicEventRecordType(org.osid.type.Type cyclicEventRecordType) {
        return (getAdapteeManager().supportsCyclicEventRecordType(cyclicEventRecordType));
    }


    /**
     *  Gets the supported <code> CyclicEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> CyclicEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicEventSearchRecordTypes() {
        return (getAdapteeManager().getCyclicEventSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CyclicEvent </code> search record type is 
     *  supported. 
     *
     *  @param  cyclicEventSearchRecordType a <code> Type </code> indicating a 
     *          <code> CyclicEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearchRecordType(org.osid.type.Type cyclicEventSearchRecordType) {
        return (getAdapteeManager().supportsCyclicEventSearchRecordType(cyclicEventSearchRecordType));
    }


    /**
     *  Gets the supported <code> CyclicTimePeriod </code> record types. 
     *
     *  @return a list containing the supported <code> CyclicTimePeriod 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicTimePeriodRecordTypes() {
        return (getAdapteeManager().getCyclicTimePeriodRecordTypes());
    }


    /**
     *  Tests if the given <code> CyclicTimePeriod </code> record type is 
     *  supported. 
     *
     *  @param  cyclicTimePeriodRecordType a <code> Type </code> indicating a 
     *          <code> CyclicTimePeriod </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicTimePeriodRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodRecordType(org.osid.type.Type cyclicTimePeriodRecordType) {
        return (getAdapteeManager().supportsCyclicTimePeriodRecordType(cyclicTimePeriodRecordType));
    }


    /**
     *  Gets the supported <code> CyclicTimePeriod </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CyclicTimePeriod 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCyclicTimePeriodSearchRecordTypes() {
        return (getAdapteeManager().getCyclicTimePeriodSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CyclicTimePeriod </code> search record type 
     *  is supported. 
     *
     *  @param  cyclicTimePeriodSearchRecordType a <code> Type </code> 
     *          indicating a <code> CyclicTimePeriod </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          cyclicTimePeriodSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodSearchRecordType(org.osid.type.Type cyclicTimePeriodSearchRecordType) {
        return (getAdapteeManager().supportsCyclicTimePeriodSearchRecordType(cyclicTimePeriodSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventLookupSession getCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuerySession getCyclicEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchSession getCyclicEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventAdminSession getCyclicEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to event 
     *  changes. 
     *
     *  @param  eventReceiver the cyclic event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSession(org.osid.calendaring.cycle.CyclicEventReceiver eventReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventNotificationSession(eventReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic event 
     *  notification service for the given calendar. 
     *
     *  @param  eventReceiver the cyclic event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> CyclicEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> eventReceiver, </code> 
     *          <code> calendarId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventNotificationSession getCyclicEventNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicEventReceiver eventReceiver, 
                                                                                                                  org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventNotificationSessionForCalendar(eventReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventCalendar() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarSession getCyclicEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventCalendarAssignmentSession getCyclicEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the cyclic event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSmartCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSmartCalendarSession getCyclicEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving event to cyclic event associations. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCyclicEventLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventLookupSession getEventCyclicEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCyclicEventLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the session for manually making event to cyclic event 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCyclicEventAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event cyclic 
     *  event assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventCyclicEventAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCyclicEventAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.EventCyclicEventAssignmentSession getEventCyclicEventAssignmentSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEventCyclicEventAssignmentSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodLookupSession getCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuerySession getCyclicTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodQuerySessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSearchSession getCyclicTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodSearchSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> calendarId </code> not found 
     *  @throws org.osid.NotFoundException a <code> 
     *          CyclicTimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodAdminSession getCyclicTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to cyclic 
     *  time period changes. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSession(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodNotificationSession(cyclicTimePeriodReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cyclic time 
     *  period notification service for the given calendar. 
     *
     *  @param  cyclicTimePeriodReceiver the cyclic time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a CyclicTimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodReceiver 
     *          </code> or <code> calendarId </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodNotificationSession getCyclicTimePeriodNotificationSessionForCalendar(org.osid.calendaring.cycle.CyclicTimePeriodReceiver cyclicTimePeriodReceiver, 
                                                                                                                            org.osid.id.Id calendarId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodNotificationSessionForCalendar(cyclicTimePeriodReceiver, calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving cyclic time period to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarSession getCyclicTimePeriodCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodCalendarSession(proxy));
    }


    /**
     *  Gets the session for assigning cyclic time period to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodCalendarAssignmentSession getCyclicTimePeriodCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodCalendarAssignmentSession(proxy));
    }


    /**
     *  Gets the session associated with the cyclic time period smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodSmartCalendarSession getCyclicTimePeriodSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodSmartCalendarSession(calendarId, proxy));
    }


    /**
     *  Gets the session for retrieving time period to cyclic time period 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCyclicTimePeriodLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodLookupSession getTimePeriodCyclicTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCyclicTimePeriodLookupSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the session for manually making time period to cyclic time period 
     *  associations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCyclicTimePeriodAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  cyclic time period assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCyclicTimePeriodAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCyclicTimePeriodAssignment() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.TimePeriodCyclicTimePeriodAssignmentSession getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTimePeriodCyclicTimePeriodAssignmentSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets a <code> CalendaringCycleBatchProxyManager. </code> 
     *
     *  @return a <code> CalendaringCycleBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycleBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager getCalendaringCycleBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendaringCycleBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
