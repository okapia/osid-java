//
// AbstractAccount.java
//
//     Defines an Account builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.account.spi;


/**
 *  Defines an <code>Account</code> builder.
 */

public abstract class AbstractAccountBuilder<T extends AbstractAccountBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.financials.account.AccountMiter account;


    /**
     *  Constructs a new <code>AbstractAccountBuilder</code>.
     *
     *  @param account the account to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAccountBuilder(net.okapia.osid.jamocha.builder.financials.account.AccountMiter account) {
        super(account);
        this.account = account;
        return;
    }


    /**
     *  Builds the account.
     *
     *  @return the new account
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.financials.Account build() {
        (new net.okapia.osid.jamocha.builder.validator.financials.account.AccountValidator(getValidations())).validate(this.account);
        return (new net.okapia.osid.jamocha.builder.financials.account.ImmutableAccount(this.account));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the account miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.financials.account.AccountMiter getMiter() {
        return (this.account);
    }


    /**
     *  Credits increase balance.
     *
     *  @return the builder
     */

    public T creditIncreaseBalance() {
        getMiter().setCreditBalance(true);
        return (self());
    }


    /**
     *  Credits decrease balance.
     *
     *  @return the builder
     */

    public T creditDecreaseBalance() {
        getMiter().setCreditBalance(false);
        return (self());
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Adds an Account record.
     *
     *  @param record an account record
     *  @param recordType the type of account record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.financials.records.AccountRecord record, org.osid.type.Type recordType) {
        getMiter().addAccountRecord(record, recordType);
        return (self());
    }
}       


