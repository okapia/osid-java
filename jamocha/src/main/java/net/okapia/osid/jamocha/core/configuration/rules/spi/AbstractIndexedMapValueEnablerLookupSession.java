//
// AbstractIndexedMapValueEnablerLookupSession.java
//
//    A simple framework for providing a ValueEnabler lookup service
//    backed by a fixed collection of value enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ValueEnabler lookup service backed by a
 *  fixed collection of value enablers. The value enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some value enablers may be compatible
 *  with more types than are indicated through these value enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ValueEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapValueEnablerLookupSession
    extends AbstractMapValueEnablerLookupSession
    implements org.osid.configuration.rules.ValueEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ValueEnabler> valueEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ValueEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.configuration.rules.ValueEnabler> valueEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.rules.ValueEnabler>());


    /**
     *  Makes a <code>ValueEnabler</code> available in this session.
     *
     *  @param  valueEnabler a value enabler
     *  @throws org.osid.NullArgumentException <code>valueEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putValueEnabler(org.osid.configuration.rules.ValueEnabler valueEnabler) {
        super.putValueEnabler(valueEnabler);

        this.valueEnablersByGenus.put(valueEnabler.getGenusType(), valueEnabler);
        
        try (org.osid.type.TypeList types = valueEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.valueEnablersByRecord.put(types.getNextType(), valueEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a value enabler from this session.
     *
     *  @param valueEnablerId the <code>Id</code> of the value enabler
     *  @throws org.osid.NullArgumentException <code>valueEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeValueEnabler(org.osid.id.Id valueEnablerId) {
        org.osid.configuration.rules.ValueEnabler valueEnabler;
        try {
            valueEnabler = getValueEnabler(valueEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.valueEnablersByGenus.remove(valueEnabler.getGenusType());

        try (org.osid.type.TypeList types = valueEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.valueEnablersByRecord.remove(types.getNextType(), valueEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeValueEnabler(valueEnablerId);
        return;
    }


    /**
     *  Gets a <code>ValueEnablerList</code> corresponding to the given
     *  value enabler genus <code>Type</code> which does not include
     *  value enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known value enablers or an error results. Otherwise,
     *  the returned list may contain only those value enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  valueEnablerGenusType a value enabler genus type 
     *  @return the returned <code>ValueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByGenusType(org.osid.type.Type valueEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.valueenabler.ArrayValueEnablerList(this.valueEnablersByGenus.get(valueEnablerGenusType)));
    }


    /**
     *  Gets a <code>ValueEnablerList</code> containing the given
     *  value enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known value enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  value enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  valueEnablerRecordType a value enabler record type 
     *  @return the returned <code>valueEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>valueEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablersByRecordType(org.osid.type.Type valueEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.valueenabler.ArrayValueEnablerList(this.valueEnablersByRecord.get(valueEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.valueEnablersByGenus.clear();
        this.valueEnablersByRecord.clear();

        super.close();

        return;
    }
}
