//
// AbstractParameterSearch.java
//
//     A template for making a Parameter Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing parameter searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractParameterSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.configuration.ParameterSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.configuration.records.ParameterSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.configuration.ParameterSearchOrder parameterSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of parameters. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  parameterIds list of parameters
     *  @throws org.osid.NullArgumentException
     *          <code>parameterIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongParameters(org.osid.id.IdList parameterIds) {
        while (parameterIds.hasNext()) {
            try {
                this.ids.add(parameterIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongParameters</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of parameter Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getParameterIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  parameterSearchOrder parameter search order 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>parameterSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderParameterResults(org.osid.configuration.ParameterSearchOrder parameterSearchOrder) {
	this.parameterSearchOrder = parameterSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.configuration.ParameterSearchOrder getParameterSearchOrder() {
	return (this.parameterSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given parameter search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter implementing the requested record.
     *
     *  @param parameterSearchRecordType a parameter search record
     *         type
     *  @return the parameter search record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterSearchRecord getParameterSearchRecord(org.osid.type.Type parameterSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.configuration.records.ParameterSearchRecord record : this.records) {
            if (record.implementsRecordType(parameterSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter search. 
     *
     *  @param parameterSearchRecord parameter search record
     *  @param parameterSearchRecordType parameter search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterSearchRecord(org.osid.configuration.records.ParameterSearchRecord parameterSearchRecord, 
                                           org.osid.type.Type parameterSearchRecordType) {

        addRecordType(parameterSearchRecordType);
        this.records.add(parameterSearchRecord);        
        return;
    }
}
