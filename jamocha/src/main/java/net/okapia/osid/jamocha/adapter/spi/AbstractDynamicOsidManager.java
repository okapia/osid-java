//
// AbstractDynamicOsidManager.java
//
//     An adapter for an OSID Provider.
//
//
// Tom Coppeto
// Okapia
// 30 January 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A dynamic Java adapter template for an OSID Provider. A single
 *  OSID Provider is adapted through overriding the
 *  <code>invoke()</code> method.
 */

public abstract class AbstractDynamicOsidManager 
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager 
    implements java.lang.reflect.InvocationHandler,
               org.osid.binding.java.DynamicManager {
    
    private Object delegate;
    private boolean stealth = false;

    private final java.util.Map<Class<? extends Object>, AdapterHandler> argHandlers = new java.util.HashMap<Class<? extends Object>, AdapterHandler>();
    private final java.util.Map<Class<?extends Object>, AdapterHandler> retHandlers = new java.util.HashMap<Class<? extends Object>, AdapterHandler>();
    

    /**
     *  Constructs a new <code>AbstractDynamicOsidManager</code>.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractDynamicOsidManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Starts this adapter. 
     *
     *  @param  osid the OSID interface
     *  @param  implementation the name of the OSID Provider
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> implementation </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected void start(org.osid.OSID osid, String implementation)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(osid, "osid");
        nullarg(implementation, "implementation");
        
        try {
            this.delegate = getRuntime().getManager(osid, implementation, 
                                                    net.okapia.osid.primordium.installation.SoftwareVersion.valueOf("3.0.0")); 
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("could not instantiate provider " + implementation, e);
        }

        return;
    }


    /**
     *  Start this adapter for OSID Proxy managers. 
     *
     *  @param  osid the OSID interface
     *  @param  implementation the name of the OSID Provider
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> implementation </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected void startProxy(org.osid.OSID osid, String implementation)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(osid, "osid");
        nullarg(implementation, "implementation");
        
        try {
            this.delegate = getRuntime().getProxyManager(osid, implementation, 
                                                         net.okapia.osid.primordium.installation.SoftwareVersion.valueOf("3.0.0")); 
        } catch (Exception e) {
            throw new org.osid.OperationFailedException("could not instantiate provider " + implementation, e);
        }

        return;
    }


    /**
     *  Sets stealth mode. In stealth mode, the
     *  <code>OsidProfile</code> identity methods are passed through
     *  to the underlying provider.
     */

    protected void setStealth() {
        this.stealth = true;
        return;
    }


    /**
     *  Adds an argument handler for this adapter. An argument handler
     *  is called for every argument implementing the given interface.
     *
     *  @param  handler handler interface
     *  @param  c interface
     *  @throws org.osid.NullArgumentException <code>handler</code> or
     *          <code>c</code> is <code>null</code>
     */

    protected void addArgumentHandler(AdapterHandler handler, Class<? extends Object> c) {
        nullarg(handler, "handler");
        nullarg(c, "class");
        this.argHandlers.put(c, handler);
        return;
    }


    /**
     *  Adds a return handler for this adapter. A return handler
     *  is called for every return value implementing the given interface.
     *
     *  @param  handler handler interface
     *  @param  c interface
     *  @throws org.osid.NullArgumentException <code>handler</code> or
     *          <code>c</code> is <code>null</code>
     */

    protected void addReturnHandler(AdapterHandler handler, Class<? extends Object> c) {
        nullarg(handler, "handler");
        nullarg(c, "class");
        this.retHandlers.put(c, handler);
        return;
    }
    

    /**
     *  Gets the delegate under this adapter.
     *
     *  @return the delegate or <code>null</code> if none
     */

    protected Object getDelegate() {
        return (this.delegate);
    }


    /**
     *  Invokes a method. Part of the InvocationHandler interface.
     *  This method uses the manager identity methods of this adapter
     *  including Id, displayName, description, version, release date,
     *  license and provider. This behavior may be used by calling
     *  this method or replaced by overriding it completely.
     *
     *  @param proxy the instance of the object on which the method
     *         was invoked
     *  @param method the method invoked
     *  @param args the arguments to the method
     *  @return result of the method
     *  @throws Throwable
     */

    public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args)
        throws Throwable {
        
        if (!stealth && (proxy instanceof org.osid.OsidProfile) &&
            ((args == null) || (args.length == 0))) {

            if (method.getName().equals("getId")) {
                return (getId());
            }
            
            if (method.getName().equals("getDisplayName")) {
                return (getDisplayName());
            }
            
            if (method.getName().equals("getDescription")) {
                return (getDescription());
            }
            
            if (method.getName().equals("getVersion")) {
                return (getVersion());
            }
            
            if (method.getName().equals("getReleaseDate")) {
                return (getReleaseDate());
            }
            
            if (method.getName().equals("getLicense")) {
                return (getLicense());
            }
            
            if (method.getName().equals("getProviderId")) {
                return (getProviderId());
            }
            
            if (method.getName().equals("getProvider")) {
                return (getProvider());
            }
            
            if (method.getName().equals("getBranding")) {
                return (getBranding());
            }
        }

        /*
         * These methods may get invoked before the delegate is set.
         * Since this is an adapter for an OsidManager, every instance
         * of an OsidManager is unique so this object's hashCode and
         * equals should suffice.
         */

        if ((args == null) || (args.length == 0)) {
            if (method.getName().equals("hashCode")) {
                return (hashCode());
            }

            if (method.getName().equals("toString")) {
                if (this.delegate == null) {
                    return (toString());
                } else {
                    return (this.delegate.toString());
                }
            }
        }

        if ((args != null) && (args.length == 1) && (method.getName().equals("equals"))) {
            return (equals(args[0]));
        }

        if (method.getName().equals("initialize")) {
            if ((args.length == 1) && (args[0] instanceof org.osid.OsidRuntimeManager)) {
                initialize((org.osid.OsidRuntimeManager) args[0]);
                return (null);
            }
        }

        if (method.getName().equals("close")) {
            close();
            return (null);
        }

        if (this.delegate == null) {
            throw new org.osid.IllegalStateException("adapter not running");
        }

        try {
            return (processReturn(method.invoke(this.delegate, processArguments(args))));
        } catch (java.lang.reflect.InvocationTargetException ite) {
            throw ite.getTargetException();
        }
    }


    private Object[] processArguments(Object[] args) {
        if (args == null) {
            return (args);
        }

        Object[] ret = args.clone();

        for (int i = 0; i < args.length; i++) {
            for (Class<? extends Object> c : this.argHandlers.keySet()) {
                ret[i] = processArgument(c, args[i]);
            }
        }
        
        return (ret);
    }


    private Object processArgument(Class<? extends Object> c, Object arg) {
        if (arg.getClass().isArray()) {
            Object[] array;
            array = (Object[]) arg;
            Object[] na = new Object[array.length];
            for (int a = 0; a < array.length; a++) {
                if (c.isAssignableFrom(array[a].getClass().asSubclass(array[a].getClass()))) {
                    na[a] = this.argHandlers.get(c).handleArgument(array[a]);
                } else {
                    na[a] = array[a];
                }
            }

            arg = na;
        } else if (c.isAssignableFrom(arg.getClass().asSubclass(arg.getClass()))) {
            arg = this.argHandlers.get(c).handleArgument(arg);
        }
        
        return (arg);
    }


    private Object processReturn(Object value) {
        if (value == null) {
            return (null);
        }
        
        for (Class<? extends Object> c : this.retHandlers.keySet()) {
            if (value.getClass().isArray()) {
                Object[] array;
                array = (Object[]) value;
                for (int a = 0; a < array.length; a++) {
                    if (c.isAssignableFrom(array[a].getClass().asSubclass(array[a].getClass()))) {
                        array[a] = this.retHandlers.get(c).handleReturn(array[a]);
                    } 
                }
            } else if (c.isAssignableFrom(value.getClass().asSubclass(value.getClass()))) {
                value = this.retHandlers.get(c).handleReturn(value);
            }
        }

        return (value);
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidLoader </code> or this manager 
     *          has been shut down 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {
        
        super.initialize(runtime);
        return;
    }


    /**
     *  Closes this <code>osid.OsidManager</code>
     *
     *  @throws org.osid.IllegalStateException This manager has been closed.
     */

    public void close() {
        if (this.delegate instanceof org.osid.OsidManager) {
            ((org.osid.OsidManager) this.delegate).close();
        } else if (this.delegate instanceof org.osid.OsidProxyManager) {
            ((org.osid.OsidProxyManager) this.delegate).close();
        }

        super.close();
        return;
    }
}
    
