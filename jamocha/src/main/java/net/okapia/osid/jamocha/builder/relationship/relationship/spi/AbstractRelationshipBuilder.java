//
// AbstractRelationship.java
//
//     Defines a Relationship builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.relationship.relationship.spi;


/**
 *  Defines a <code>Relationship</code> builder.
 */

public abstract class AbstractRelationshipBuilder<T extends AbstractRelationshipBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.relationship.relationship.RelationshipMiter relationship;


    /**
     *  Constructs a new <code>AbstractRelationshipBuilder</code>.
     *
     *  @param relationship the relationship to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRelationshipBuilder(net.okapia.osid.jamocha.builder.relationship.relationship.RelationshipMiter relationship) {
        super(relationship);
        this.relationship = relationship;
        return;
    }


    /**
     *  Builds the relationship.
     *
     *  @return the new relationship
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.relationship.Relationship build() {
        (new net.okapia.osid.jamocha.builder.validator.relationship.relationship.RelationshipValidator(getValidations())).validate(this.relationship);
        return (new net.okapia.osid.jamocha.builder.relationship.relationship.ImmutableRelationship(this.relationship));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the relationship miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.relationship.relationship.RelationshipMiter getMiter() {
        return (this.relationship);
    }


    /**
     *  Sets the source id.
     *
     *  @param sourceId a source id
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          is <code>null</code>
     */

    public T sourceId(org.osid.id.Id sourceId) {
        getMiter().setSourceId(sourceId);
        return (self());
    }


    /**
     *  Sets the destination id.
     *
     *  @param destinationId a destination id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>destinationId</code> is <code>null</code>
     */

    public T destinationId(org.osid.id.Id destinationId) {
        getMiter().setDestinationId(destinationId);
        return (self());
    }


    /**
     *  Adds a Relationship record.
     *
     *  @param record a relationship record
     *  @param recordType the type of relationship record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.relationship.records.RelationshipRecord record, org.osid.type.Type recordType) {
        getMiter().addRelationshipRecord(record, recordType);
        return (self());
    }
}       


