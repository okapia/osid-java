//
// AbstractImmutableProfileItem.java
//
//     Wraps a mutable ProfileItem to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.profile.profileitem.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProfileItem</code> to hide modifiers. This
 *  wrapper provides an immutized ProfileItem from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying profileItem whose state changes are visible.
 */

public abstract class AbstractImmutableProfileItem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.profile.ProfileItem {

    private final org.osid.profile.ProfileItem profileItem;


    /**
     *  Constructs a new <code>AbstractImmutableProfileItem</code>.
     *
     *  @param profileItem the profile item to immutablize
     *  @throws org.osid.NullArgumentException <code>profileItem</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProfileItem(org.osid.profile.ProfileItem profileItem) {
        super(profileItem);
        this.profileItem = profileItem;
        return;
    }


    /**
     *  Gets the profile item record corresponding to the given <code> 
     *  ProfileItem </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  profileItemRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(profileItemRecordType) </code> is <code> true </code> . 
     *
     *  @param  profileItemRecordType the type of the record to retrieve 
     *  @return the profile item record 
     *  @throws org.osid.NullArgumentException <code> profileItemRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(profileItemRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemRecord getProfileItemRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        return (this.profileItem.getProfileItemRecord(profileItemRecordType));
    }
}

