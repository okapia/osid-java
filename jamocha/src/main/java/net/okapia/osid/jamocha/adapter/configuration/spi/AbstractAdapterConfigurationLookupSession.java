//
// AbstractAdapterConfigurationLookupSession.java
//
//    A Configuration lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Configuration lookup session adapter.
 */

public abstract class AbstractAdapterConfigurationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.ConfigurationLookupSession {

    private final org.osid.configuration.ConfigurationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterConfigurationLookupSession(org.osid.configuration.ConfigurationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Configuration} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupConfigurations() {
        return (this.session.canLookupConfigurations());
    }


    /**
     *  A complete view of the {@code Configuration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConfigurationView() {
        this.session.useComparativeConfigurationView();
        return;
    }


    /**
     *  A complete view of the {@code Configuration} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConfigurationView() {
        this.session.usePlenaryConfigurationView();
        return;
    }

     
    /**
     *  Gets the {@code Configuration} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Configuration} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Configuration} and
     *  retained for compatibility.
     *
     *  @param configurationId {@code Id} of the {@code Configuration}
     *  @return the configuration
     *  @throws org.osid.NotFoundException {@code configurationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code configurationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfiguration(configurationId));
    }


    /**
     *  Gets a {@code ConfigurationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  configurations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Configurations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  configurationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Configuration} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code configurationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByIds(org.osid.id.IdList configurationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurationsByIds(configurationIds));
    }


    /**
     *  Gets a {@code ConfigurationList} corresponding to the given
     *  configuration genus {@code Type} which does not include
     *  configurations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned {@code Configuration} list
     *  @throws org.osid.NullArgumentException
     *          {@code configurationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurationsByGenusType(configurationGenusType));
    }


    /**
     *  Gets a {@code ConfigurationList} corresponding to the given
     *  configuration genus {@code Type} and include any additional
     *  configurations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationGenusType a configuration genus type 
     *  @return the returned {@code Configuration} list
     *  @throws org.osid.NullArgumentException
     *          {@code configurationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByParentGenusType(org.osid.type.Type configurationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurationsByParentGenusType(configurationGenusType));
    }


    /**
     *  Gets a {@code ConfigurationList} containing the given
     *  configuration record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return the returned {@code Configuration} list
     *  @throws org.osid.NullArgumentException
     *          {@code configurationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByRecordType(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurationsByRecordType(configurationRecordType));
    }


    /**
     *  Gets a {@code ConfigurationList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Configuration} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurationsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurationsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Configurations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  configurations or an error results. Otherwise, the returned list
     *  may contain only those configurations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Configurations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConfigurations());
    }
}
