//
// AbstractIndexedMapRelevancyEnablerLookupSession.java
//
//    A simple framework for providing a RelevancyEnabler lookup service
//    backed by a fixed collection of relevancy enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RelevancyEnabler lookup service backed by a
 *  fixed collection of relevancy enablers. The relevancy enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some relevancy enablers may be compatible
 *  with more types than are indicated through these relevancy enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RelevancyEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRelevancyEnablerLookupSession
    extends AbstractMapRelevancyEnablerLookupSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ontology.rules.RelevancyEnabler> relevancyEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.rules.RelevancyEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.ontology.rules.RelevancyEnabler> relevancyEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.rules.RelevancyEnabler>());


    /**
     *  Makes a <code>RelevancyEnabler</code> available in this session.
     *
     *  @param  relevancyEnabler a relevancy enabler
     *  @throws org.osid.NullArgumentException <code>relevancyEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRelevancyEnabler(org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        super.putRelevancyEnabler(relevancyEnabler);

        this.relevancyEnablersByGenus.put(relevancyEnabler.getGenusType(), relevancyEnabler);
        
        try (org.osid.type.TypeList types = relevancyEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relevancyEnablersByRecord.put(types.getNextType(), relevancyEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a relevancy enabler from this session.
     *
     *  @param relevancyEnablerId the <code>Id</code> of the relevancy enabler
     *  @throws org.osid.NullArgumentException <code>relevancyEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRelevancyEnabler(org.osid.id.Id relevancyEnablerId) {
        org.osid.ontology.rules.RelevancyEnabler relevancyEnabler;
        try {
            relevancyEnabler = getRelevancyEnabler(relevancyEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.relevancyEnablersByGenus.remove(relevancyEnabler.getGenusType());

        try (org.osid.type.TypeList types = relevancyEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relevancyEnablersByRecord.remove(types.getNextType(), relevancyEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRelevancyEnabler(relevancyEnablerId);
        return;
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> corresponding to the given
     *  relevancy enabler genus <code>Type</code> which does not include
     *  relevancy enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known relevancy enablers or an error results. Otherwise,
     *  the returned list may contain only those relevancy enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  relevancyEnablerGenusType a relevancy enabler genus type 
     *  @return the returned <code>RelevancyEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByGenusType(org.osid.type.Type relevancyEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.rules.relevancyenabler.ArrayRelevancyEnablerList(this.relevancyEnablersByGenus.get(relevancyEnablerGenusType)));
    }


    /**
     *  Gets a <code>RelevancyEnablerList</code> containing the given
     *  relevancy enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known relevancy enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  relevancy enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  relevancyEnablerRecordType a relevancy enabler record type 
     *  @return the returned <code>relevancyEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.rules.RelevancyEnablerList getRelevancyEnablersByRecordType(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.rules.relevancyenabler.ArrayRelevancyEnablerList(this.relevancyEnablersByRecord.get(relevancyEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relevancyEnablersByGenus.clear();
        this.relevancyEnablersByRecord.clear();

        super.close();

        return;
    }
}
