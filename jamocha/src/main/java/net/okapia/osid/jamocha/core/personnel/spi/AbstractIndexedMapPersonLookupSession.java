//
// AbstractIndexedMapPersonLookupSession.java
//
//    A simple framework for providing a Person lookup service
//    backed by a fixed collection of persons with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Person lookup service backed by a
 *  fixed collection of persons. The persons are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some persons may be compatible
 *  with more types than are indicated through these person
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Persons</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPersonLookupSession
    extends AbstractMapPersonLookupSession
    implements org.osid.personnel.PersonLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.personnel.Person> personsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Person>());
    private final MultiMap<org.osid.type.Type, org.osid.personnel.Person> personsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Person>());


    /**
     *  Makes a <code>Person</code> available in this session.
     *
     *  @param  person a person
     *  @throws org.osid.NullArgumentException <code>person<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPerson(org.osid.personnel.Person person) {
        super.putPerson(person);

        this.personsByGenus.put(person.getGenusType(), person);
        
        try (org.osid.type.TypeList types = person.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.personsByRecord.put(types.getNextType(), person);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a person from this session.
     *
     *  @param personId the <code>Id</code> of the person
     *  @throws org.osid.NullArgumentException <code>personId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePerson(org.osid.id.Id personId) {
        org.osid.personnel.Person person;
        try {
            person = getPerson(personId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.personsByGenus.remove(person.getGenusType());

        try (org.osid.type.TypeList types = person.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.personsByRecord.remove(types.getNextType(), person);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePerson(personId);
        return;
    }


    /**
     *  Gets a <code>PersonList</code> corresponding to the given
     *  person genus <code>Type</code> which does not include
     *  persons of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known persons or an error results. Otherwise,
     *  the returned list may contain only those persons that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  personGenusType a person genus type 
     *  @return the returned <code>Person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByGenusType(org.osid.type.Type personGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.person.ArrayPersonList(this.personsByGenus.get(personGenusType)));
    }


    /**
     *  Gets a <code>PersonList</code> containing the given
     *  person record <code>Type</code>. In plenary mode, the
     *  returned list contains all known persons or an error
     *  results. Otherwise, the returned list may contain only those
     *  persons that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  personRecordType a person record type 
     *  @return the returned <code>person</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>personRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PersonList getPersonsByRecordType(org.osid.type.Type personRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.person.ArrayPersonList(this.personsByRecord.get(personRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.personsByGenus.clear();
        this.personsByRecord.clear();

        super.close();

        return;
    }
}
