//
// AbstractScene.java
//
//     Defines a Scene.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.scene.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Scene</code>.
 */

public abstract class AbstractScene
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.control.Scene {

    private final java.util.Collection<org.osid.control.Setting> settings = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.control.records.SceneRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the setting item <code> Ids. </code> 
     *
     *  @return the setting item <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSettingIds() {
        try (org.osid.control.SettingList settings = getSettings()) {
            return (new net.okapia.osid.jamocha.adapter.converter.control.setting.SettingToIdList(settings));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the settings. 
     *
     *  @return list of settings part of this scene 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.control.setting.ArraySettingList(this.settings));
    }


    /**
     *  Adds a setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException
     *          <code>setting</code> is <code>null</code>
     */

    protected void addSetting(org.osid.control.Setting setting) {
        nullarg(setting, "setting");
        this.settings.add(setting);
        return;
    }


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException
     *          <code>settings</code> is <code>null</code>
     */

    protected void setSettings(java.util.Collection<org.osid.control.Setting> settings) {
        nullarg(settings, "settings");
        this.settings.clear();
        this.settings.addAll(settings);
        return;
    }


    /**
     *  Tests if this scene supports the given record
     *  <code>Type</code>.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return <code>true</code> if the sceneRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sceneRecordType) {
        for (org.osid.control.records.SceneRecord record : this.records) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  sceneRecordType the scene record type 
     *  @return the scene record 
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sceneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneRecord getSceneRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SceneRecord record : this.records) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sceneRecordType + " is not supported");
    }


    /**
     *  Adds a record to this scene. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sceneRecord the scene record
     *  @param sceneRecordType scene record type
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecord</code> or
     *          <code>sceneRecordTypescene</code> is
     *          <code>null</code>
     */
            
    protected void addSceneRecord(org.osid.control.records.SceneRecord sceneRecord, 
                                     org.osid.type.Type sceneRecordType) {

        addRecordType(sceneRecordType);
        this.records.add(sceneRecord);
        
        return;
    }
}
