//
// AbstractHoldBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractHoldBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.hold.batch.HoldBatchManager,
               org.osid.hold.batch.HoldBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractHoldBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractHoldBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of blocks is available. 
     *
     *  @return <code> true </code> if a block bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of issues is available. 
     *
     *  @return <code> true </code> if an issue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of holds is available. 
     *
     *  @return <code> true </code> if a hold bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of oubliettes is available. 
     *
     *  @return <code> true </code> if a oubliette bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service. 
     *
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getBlockBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getBlockBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getBlockBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.BlockBatchAdminSession getBlockBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getBlockBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service. 
     *
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getIssueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getIssueBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.IssueBatchAdminSession getIssueBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getIssueBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service. 
     *
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getHoldBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getHoldBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getHoldBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk hold 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchAdminSession getHoldBatchAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getHoldBatchAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk oubliette 
     *  administration service. 
     *
     *  @return a <code> OublietteBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.OublietteBatchAdminSession getOublietteBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchManager.getOublietteBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk oubliette 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.OublietteBatchAdminSession getOublietteBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.batch.HoldBatchProxyManager.getOublietteBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
