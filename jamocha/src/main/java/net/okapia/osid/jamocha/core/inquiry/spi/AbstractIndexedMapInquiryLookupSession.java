//
// AbstractIndexedMapInquiryLookupSession.java
//
//    A simple framework for providing an Inquiry lookup service
//    backed by a fixed collection of inquiries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Inquiry lookup service backed by a
 *  fixed collection of inquiries. The inquiries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some inquiries may be compatible
 *  with more types than are indicated through these inquiry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inquiries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInquiryLookupSession
    extends AbstractMapInquiryLookupSession
    implements org.osid.inquiry.InquiryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Inquiry> inquiriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Inquiry>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Inquiry> inquiriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Inquiry>());


    /**
     *  Makes an <code>Inquiry</code> available in this session.
     *
     *  @param  inquiry an inquiry
     *  @throws org.osid.NullArgumentException <code>inquiry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInquiry(org.osid.inquiry.Inquiry inquiry) {
        super.putInquiry(inquiry);

        this.inquiriesByGenus.put(inquiry.getGenusType(), inquiry);
        
        try (org.osid.type.TypeList types = inquiry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquiriesByRecord.put(types.getNextType(), inquiry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of inquiries available in this session.
     *
     *  @param  inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException <code>inquiries<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquiries(org.osid.inquiry.Inquiry[] inquiries) {
        for (org.osid.inquiry.Inquiry inquiry : inquiries) {
            putInquiry(inquiry);
        }

        return;
    }


    /**
     *  Makes a collection of inquiries available in this session.
     *
     *  @param  inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException <code>inquiries<code>
     *          is <code>null</code>
     */

    @Override
    protected void putInquiries(java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {
        for (org.osid.inquiry.Inquiry inquiry : inquiries) {
            putInquiry(inquiry);
        }

        return;
    }


    /**
     *  Removes an inquiry from this session.
     *
     *  @param inquiryId the <code>Id</code> of the inquiry
     *  @throws org.osid.NullArgumentException <code>inquiryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInquiry(org.osid.id.Id inquiryId) {
        org.osid.inquiry.Inquiry inquiry;
        try {
            inquiry = getInquiry(inquiryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.inquiriesByGenus.remove(inquiry.getGenusType());

        try (org.osid.type.TypeList types = inquiry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.inquiriesByRecord.remove(types.getNextType(), inquiry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInquiry(inquiryId);
        return;
    }


    /**
     *  Gets an <code>InquiryList</code> corresponding to the given
     *  inquiry genus <code>Type</code> which does not include
     *  inquiries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known inquiries or an error results. Otherwise,
     *  the returned list may contain only those inquiries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  inquiryGenusType an inquiry genus type 
     *  @return the returned <code>Inquiry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByGenusType(org.osid.type.Type inquiryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquiry.ArrayInquiryList(this.inquiriesByGenus.get(inquiryGenusType)));
    }


    /**
     *  Gets an <code>InquiryList</code> containing the given
     *  inquiry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known inquiries or an error
     *  results. Otherwise, the returned list may contain only those
     *  inquiries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  inquiryRecordType an inquiry record type 
     *  @return the returned <code>inquiry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryList getInquiriesByRecordType(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.inquiry.ArrayInquiryList(this.inquiriesByRecord.get(inquiryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inquiriesByGenus.clear();
        this.inquiriesByRecord.clear();

        super.close();

        return;
    }
}
