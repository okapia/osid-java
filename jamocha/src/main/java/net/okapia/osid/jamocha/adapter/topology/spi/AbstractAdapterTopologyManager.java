//
// AbstractTopologyManager.java
//
//     An adapter for a TopologyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TopologyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTopologyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.topology.TopologyManager>
    implements org.osid.topology.TopologyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTopologyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTopologyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTopologyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTopologyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any graph federation is exposed. Federation is exposed when a 
     *  specific graph may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of graphs 
     *  appears as a single graph. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if traversing topologies is supported. 
     *
     *  @return <code> true </code> if topology traversal is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyTraversal() {
        return (getAdapteeManager().supportsTopologyTraversal());
    }


    /**
     *  Tests if topology routing is supported. 
     *
     *  @return <code> true </code> if topology routing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyRouting() {
        return (getAdapteeManager().supportsTopologyRouting());
    }


    /**
     *  Tests if looking up nodes is supported. 
     *
     *  @return <code> true </code> if node lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeLookup() {
        return (getAdapteeManager().supportsNodeLookup());
    }


    /**
     *  Tests if querying nodes is supported. 
     *
     *  @return <code> true </code> if node query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (getAdapteeManager().supportsNodeQuery());
    }


    /**
     *  Tests if searching nodes is supported. 
     *
     *  @return <code> true </code> if node search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeSearch() {
        return (getAdapteeManager().supportsNodeSearch());
    }


    /**
     *  Tests if node <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if node administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeAdmin() {
        return (getAdapteeManager().supportsNodeAdmin());
    }


    /**
     *  Tests if a node <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if node notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeNotification() {
        return (getAdapteeManager().supportsNodeNotification());
    }


    /**
     *  Tests if a node graph mapping lookup service is supported. 
     *
     *  @return <code> true </code> if a node graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeGraph() {
        return (getAdapteeManager().supportsNodeGraph());
    }


    /**
     *  Tests if a node graph mapping service is supported. 
     *
     *  @return <code> true </code> if node to graph mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeGraphAssignment() {
        return (getAdapteeManager().supportsNodeGraphAssignment());
    }


    /**
     *  Tests if a node smart graph cataloging service is supported. 
     *
     *  @return <code> true </code> if node smart graphs are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeSmartGraph() {
        return (getAdapteeManager().supportsNodeSmartGraph());
    }


    /**
     *  Tests if looking up edges is supported. 
     *
     *  @return <code> true </code> if edge lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeLookup() {
        return (getAdapteeManager().supportsEdgeLookup());
    }


    /**
     *  Tests if searching edges is supported. 
     *
     *  @return <code> true </code> if edge search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeSearch() {
        return (getAdapteeManager().supportsEdgeSearch());
    }


    /**
     *  Tests if edge <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if edge administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeAdmin() {
        return (getAdapteeManager().supportsEdgeAdmin());
    }


    /**
     *  Tests if an edge <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if edge notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeNotification() {
        return (getAdapteeManager().supportsEdgeNotification());
    }


    /**
     *  Tests if an edge graph mapping lookup service is supported. 
     *
     *  @return <code> true </code> if an edge graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeGraph() {
        return (getAdapteeManager().supportsEdgeGraph());
    }


    /**
     *  Tests if an edge graph mapping service is supported. 
     *
     *  @return <code> true </code> if edge to graph mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeGraphAssignment() {
        return (getAdapteeManager().supportsEdgeGraphAssignment());
    }


    /**
     *  Tests if an edgesmart graph cataloging service is supported. 
     *
     *  @return <code> true </code> if edge smart graphs are supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeSmartGraph() {
        return (getAdapteeManager().supportsEdgeSmartGraph());
    }


    /**
     *  Tests if looking up graphs is supported. 
     *
     *  @return <code> true </code> if graph lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphLookup() {
        return (getAdapteeManager().supportsGraphLookup());
    }


    /**
     *  Tests if querying graphs is supported. 
     *
     *  @return <code> true </code> if graph query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (getAdapteeManager().supportsGraphQuery());
    }


    /**
     *  Tests if searching graphs is supported. 
     *
     *  @return <code> true </code> if graph search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphSearch() {
        return (getAdapteeManager().supportsGraphSearch());
    }


    /**
     *  Tests if graph administrative service is supported. 
     *
     *  @return <code> true </code> if graph administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphAdmin() {
        return (getAdapteeManager().supportsGraphAdmin());
    }


    /**
     *  Tests if a graph <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if graph notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphNotification() {
        return (getAdapteeManager().supportsGraphNotification());
    }


    /**
     *  Tests for the availability of a graph hierarchy traversal service. 
     *
     *  @return <code> true </code> if graph hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphHierarchy() {
        return (getAdapteeManager().supportsGraphHierarchy());
    }


    /**
     *  Tests for the availability of a graph hierarchy design service. 
     *
     *  @return <code> true </code> if graph hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphHierarchyDesign() {
        return (getAdapteeManager().supportsGraphHierarchyDesign());
    }


    /**
     *  Tests for the availability of a topology pbatchath service. 
     *
     *  @return <code> true </code> if a topology batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyBatch() {
        return (getAdapteeManager().supportsTopologyBatch());
    }


    /**
     *  Tests for the availability of a topology path service. 
     *
     *  @return <code> true </code> if a topology path service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyPath() {
        return (getAdapteeManager().supportsTopologyPath());
    }


    /**
     *  Tests for the availability of a topology rules service. 
     *
     *  @return <code> true </code> if a topology rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyRules() {
        return (getAdapteeManager().supportsTopologyRules());
    }


    /**
     *  Gets the supported <code> Node </code> record types. 
     *
     *  @return a list containing the supported <code> Node </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNodeRecordTypes() {
        return (getAdapteeManager().getNodeRecordTypes());
    }


    /**
     *  Tests if the given <code> Node </code> record type is supported. 
     *
     *  @param  nodeRecordType a <code> Type </code> indicating a <code> Node 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> nodeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNodeRecordType(org.osid.type.Type nodeRecordType) {
        return (getAdapteeManager().supportsNodeRecordType(nodeRecordType));
    }


    /**
     *  Gets the supported <code> Node </code> search types. 
     *
     *  @return a list containing the supported <code> Node </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNodeSearchRecordTypes() {
        return (getAdapteeManager().getNodeSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Node </code> search type is supported. 
     *
     *  @param  nodeSearchRecordType a <code> Type </code> indicating a <code> 
     *          Node </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> nodeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNodeSearchRecordType(org.osid.type.Type nodeSearchRecordType) {
        return (getAdapteeManager().supportsNodeSearchRecordType(nodeSearchRecordType));
    }


    /**
     *  Gets the supported <code> Edge </code> record types. 
     *
     *  @return a list containing the supported <code> Edge </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeRecordTypes() {
        return (getAdapteeManager().getEdgeRecordTypes());
    }


    /**
     *  Tests if the given <code> Edge </code> record type is supported. 
     *
     *  @param  edgeRecordType a <code> Type </code> indicating an <code> Edge 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeRecordType(org.osid.type.Type edgeRecordType) {
        return (getAdapteeManager().supportsEdgeRecordType(edgeRecordType));
    }


    /**
     *  Gets the supported <code> Edge </code> search record types. 
     *
     *  @return a list containing the supported <code> Edge </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEdgeSearchRecordTypes() {
        return (getAdapteeManager().getEdgeSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Edge </code> search record type is 
     *  supported. 
     *
     *  @param  edgeSearchRecordType a <code> Type </code> indicating an 
     *          <code> Edge </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> edgeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEdgeSearchRecordType(org.osid.type.Type edgeSearchRecordType) {
        return (getAdapteeManager().supportsEdgeSearchRecordType(edgeSearchRecordType));
    }


    /**
     *  Gets the supported <code> Graph </code> record types. 
     *
     *  @return a list containing the supported <code> Graph </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGraphRecordTypes() {
        return (getAdapteeManager().getGraphRecordTypes());
    }


    /**
     *  Tests if the given <code> Graph </code> record type is supported. 
     *
     *  @param  graphRecordType a <code> Type </code> indicating a <code> 
     *          Graph </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> graphRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGraphRecordType(org.osid.type.Type graphRecordType) {
        return (getAdapteeManager().supportsGraphRecordType(graphRecordType));
    }


    /**
     *  Gets the supported <code> Graph </code> search record types. 
     *
     *  @return a list containing the supported <code> Graph </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGraphSearchRecordTypes() {
        return (getAdapteeManager().getGraphSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Graph </code> search record type is 
     *  supported. 
     *
     *  @param  graphSearchRecordType a <code> Type </code> indicating a 
     *          <code> Graph </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> graphSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGraphSearchRecordType(org.osid.type.Type graphSearchRecordType) {
        return (getAdapteeManager().supportsGraphSearchRecordType(graphSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service. 
     *
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyTraversalSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  traversal service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> TopologyTraversalSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyTraversal() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyTraversalSession getTopologyTraversalForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyTraversalForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service. 
     *
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyRoutingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  routing service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> TopologyRoutingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyRouting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyRoutingSession getTopologyRoutingForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyRoutingForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service. 
     *
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> NodeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeLookupSession getNodeLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeLookupSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service. 
     *
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return a <code> NodeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuerySession getNodeQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeQuerySessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service. 
     *
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchSession getNodeSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeSearchSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service. 
     *
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeAdminSession getNodeAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeAdminSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSession(org.osid.topology.NodeReceiver nodeReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeNotificationSession(nodeReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the node 
     *  notification service for the given graph. 
     *
     *  @param  nodeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> nodeReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeNotificationSession getNodeNotificationSessionForGraph(org.osid.topology.NodeReceiver nodeReceiver, 
                                                                                        org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeNotificationSessionForGraph(nodeReceiver, graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup node/graph mappings. 
     *
     *  @return a <code> NodeGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsNodeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphSession getNodeGraphSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeGraphSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning nodes to 
     *  graphs. 
     *
     *  @return a <code> NodeGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeGraphAssignmentSession getNodeGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeGraphAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> NodeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNodeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSmartGraphSession getNodeSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getNodeSmartGraphSession(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service. 
     *
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeLookupSession getEdgeLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeLookupSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service. 
     *
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the graph 
     *  @return an <code> EdgeQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuerySession getEdgeQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeQuerySessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service. 
     *
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSearchSession getEdgeSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeSearchSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service. 
     *
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeAdminSession getEdgeAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeAdminSessionForGraph(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSession(org.osid.topology.EdgeReceiver edgeReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeNotificationSession(edgeReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the edge 
     *  notification service for the given graph. 
     *
     *  @param  edgeReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> edgeReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeNotificationSession getEdgeNotificationSessionForGraph(org.osid.topology.EdgeReceiver edgeReceiver, 
                                                                                        org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeNotificationSessionForGraph(edgeReceiver, graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup edge/graph mappings. 
     *
     *  @return an <code> EdgeGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphSession getEdgeGraphSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeGraphSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning edges to 
     *  graphs. 
     *
     *  @return an <code> EdgeGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeGraphAssignmentSession getEdgeGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeGraphAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic graphs of 
     *  retlationships. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return an <code> EdgeSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEdgeSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeSmartGraphSession getEdgeSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEdgeSmartGraphSession(graphId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph lookup 
     *  service. 
     *
     *  @return a <code> GraphLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphLookupSession getGraphLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph query 
     *  service. 
     *
     *  @return a <code> GraphQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuerySession getGraphQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph search 
     *  service. 
     *
     *  @return a <code> GraphSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphSearchSession getGraphSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  administrative service. 
     *
     *  @return a <code> GraphAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGraphAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphAdminSession getGraphAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  notification service. 
     *
     *  @param  graphReceiver the notification callback 
     *  @return a <code> GraphNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> graphReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphNotificationSession getGraphNotificationSession(org.osid.topology.GraphReceiver graphReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphNotificationSession(graphReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy service. 
     *
     *  @return a <code> GraphHierarchySession </code> for graphs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchySession getGraphHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the graph 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for graphs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGraphHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphHierarchyDesignSession getGraphHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGraphHierarchyDesignSession());
    }


    /**
     *  Gets a <code> TopologyBatchManager. </code> 
     *
     *  @return a <code> TopologyBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.batch.TopologyBatchManager getTopologyBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyBatchManager());
    }


    /**
     *  Gets a <code> TopologyPathManager. </code> 
     *
     *  @return a <code> TopologyPathManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.TopologyPathManager getTopologyPathManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyPathManager());
    }


    /**
     *  Gets a <code> TopologyRulesManager. </code> 
     *
     *  @return a <code> TopologyRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTopologyRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.rules.TopologyRulesManager getTopologyRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
