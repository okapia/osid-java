//
// CourseEntryMiter.java
//
//     Defines a CourseEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.courseentry;


/**
 *  Defines a <code>CourseEntry</code> miter for use with the builders.
 */

public interface CourseEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.chronicle.CourseEntry {


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public void setStudent(org.osid.resource.Resource student);


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public void setCourse(org.osid.course.Course course);


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public void setTerm(org.osid.course.Term term);


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the course has been
     *          completed, <code> false </code> otherwise
     */

    public void setComplete(boolean complete);


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public void setCreditScale(org.osid.grading.GradeSystem scale);


    /**
     *  Sets the credits earned.
     *
     *  @param earned a credits earned
     *  @throws org.osid.NullArgumentException <code>earned</code> is
     *          <code>null</code>
     */

    public void setCreditsEarned(java.math.BigDecimal earned);


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public void setGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the score scale.
     *
     *  @param scale a score scale
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public void setScoreScale(org.osid.grading.GradeSystem scale);


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setScore(java.math.BigDecimal score);


    /**
     *  Adds a registration.
     *
     *  @param registration a registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    public void addRegistration(org.osid.course.registration.Registration registration);


    /**
     *  Sets all the registrations.
     *
     *  @param registrations a collection of registrations
     *  @throws org.osid.NullArgumentException
     *          <code>registrations</code> is <code>null</code>
     */

    public void setRegistrations(java.util.Collection<org.osid.course.registration.Registration> registrations);


    /**
     *  Adds a CourseEntry record.
     *
     *  @param record a courseEntry record
     *  @param recordType the type of courseEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCourseEntryRecord(org.osid.course.chronicle.records.CourseEntryRecord record, org.osid.type.Type recordType);
}       


