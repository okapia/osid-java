//
// AbstractIndexedMapRuleLookupSession.java
//
//    A simple framework for providing a Rule lookup service
//    backed by a fixed collection of rules with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Rule lookup service backed by a
 *  fixed collection of rules. The rules are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some rules may be compatible
 *  with more types than are indicated through these rule
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Rules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRuleLookupSession
    extends AbstractMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.rules.Rule> rulesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.Rule>());
    private final MultiMap<org.osid.type.Type, org.osid.rules.Rule> rulesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.Rule>());


    /**
     *  Makes a <code>Rule</code> available in this session.
     *
     *  @param  rule a rule
     *  @throws org.osid.NullArgumentException <code>rule<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRule(org.osid.rules.Rule rule) {
        super.putRule(rule);

        this.rulesByGenus.put(rule.getGenusType(), rule);
        
        try (org.osid.type.TypeList types = rule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.rulesByRecord.put(types.getNextType(), rule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a rule from this session.
     *
     *  @param ruleId the <code>Id</code> of the rule
     *  @throws org.osid.NullArgumentException <code>ruleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRule(org.osid.id.Id ruleId) {
        org.osid.rules.Rule rule;
        try {
            rule = getRule(ruleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.rulesByGenus.remove(rule.getGenusType());

        try (org.osid.type.TypeList types = rule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.rulesByRecord.remove(types.getNextType(), rule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRule(ruleId);
        return;
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  rule genus <code>Type</code> which does not include
     *  rules of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known rules or an error results. Otherwise,
     *  the returned list may contain only those rules that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.rule.ArrayRuleList(this.rulesByGenus.get(ruleGenusType)));
    }


    /**
     *  Gets a <code>RuleList</code> containing the given
     *  rule record <code>Type</code>. In plenary mode, the
     *  returned list contains all known rules or an error
     *  results. Otherwise, the returned list may contain only those
     *  rules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  ruleRecordType a rule record type 
     *  @return the returned <code>rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByRecordType(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.rule.ArrayRuleList(this.rulesByRecord.get(ruleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.rulesByGenus.clear();
        this.rulesByRecord.clear();

        super.close();

        return;
    }
}
