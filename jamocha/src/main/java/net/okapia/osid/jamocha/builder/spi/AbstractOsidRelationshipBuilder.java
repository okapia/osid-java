//
// AbstractOsidRelationshipBuilder.java
//
//     Defines a builder for an OSID Relationship.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      relationships:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the OsidRelationship builder.
 */

public abstract class AbstractOsidRelationshipBuilder<T extends AbstractOsidRelationshipBuilder<? extends T>>
    extends AbstractOsidObjectBuilder<T> {

    private final OsidRelationshipMiter relationship;


    /**
     *  Creates a new <code>AbstractOsidRelationshipBuilder</code>.
     *
     *  @param relationship a relationship miter interface
     *  @throws org.osid.NullArgumentException <code>relationship</code> is
     *          <code>null</code>
     */

    protected AbstractOsidRelationshipBuilder(OsidRelationshipMiter relationship) {
        super(relationship);
        this.relationship = relationship;
        return;
    }


    /**
     *  Sets the end state.
     *
     *  @param state the end state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T endReason(org.osid.process.State state) {
        this.relationship.setEndReason(state);
        return (self());
    }

    
    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T startDate(org.osid.calendaring.DateTime date) {
        this.relationship.setStartDate(date);
        return (self());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T endDate(org.osid.calendaring.DateTime date) {
        this.relationship.setEndDate(date);
        return (self());
    }
}
