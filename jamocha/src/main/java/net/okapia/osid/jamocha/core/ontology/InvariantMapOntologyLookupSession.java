//
// InvariantMapOntologyLookupSession
//
//    Implements an Ontology lookup service backed by a fixed collection of
//    ontologies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements an Ontology lookup service backed by a fixed
 *  collection of ontologies. The ontologies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapOntologyLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractMapOntologyLookupSession
    implements org.osid.ontology.OntologyLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapOntologyLookupSession</code> with no
     *  ontologies.
     */

    public InvariantMapOntologyLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOntologyLookupSession</code> with a single
     *  ontology.
     *  
     *  @throws org.osid.NullArgumentException {@code ontology}
     *          is <code>null</code>
     */

    public InvariantMapOntologyLookupSession(org.osid.ontology.Ontology ontology) {
        putOntology(ontology);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOntologyLookupSession</code> using an array
     *  of ontologies.
     *  
     *  @throws org.osid.NullArgumentException {@code ontologies}
     *          is <code>null</code>
     */

    public InvariantMapOntologyLookupSession(org.osid.ontology.Ontology[] ontologies) {
        putOntologies(ontologies);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapOntologyLookupSession</code> using a
     *  collection of ontologies.
     *
     *  @throws org.osid.NullArgumentException {@code ontologies}
     *          is <code>null</code>
     */

    public InvariantMapOntologyLookupSession(java.util.Collection<? extends org.osid.ontology.Ontology> ontologies) {
        putOntologies(ontologies);
        return;
    }
}
