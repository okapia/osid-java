//
// AbstractRoomSquattingManager.java
//
//     An adapter for a RoomSquattingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RoomSquattingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRoomSquattingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.room.squatting.RoomSquattingManager>
    implements org.osid.room.squatting.RoomSquattingManager {


    /**
     *  Constructs a new {@code AbstractAdapterRoomSquattingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRoomSquattingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRoomSquattingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRoomSquattingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any deed federation is exposed. Federation is exposed when a 
     *  specific deed may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of deeds 
     *  appears as a single deed. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of an lease lookup service. 
     *
     *  @return <code> true </code> if lease lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseLookup() {
        return (getAdapteeManager().supportsLeaseLookup());
    }


    /**
     *  Tests if querying leasees is available. 
     *
     *  @return <code> true </code> if lease query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseQuery() {
        return (getAdapteeManager().supportsLeaseQuery());
    }


    /**
     *  Tests if searching for leasees is available. 
     *
     *  @return <code> true </code> if lease search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseSearch() {
        return (getAdapteeManager().supportsLeaseSearch());
    }


    /**
     *  Tests for the availability of a lease administrative service for 
     *  creating and deleting leasees. 
     *
     *  @return <code> true </code> if lease administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseAdmin() {
        return (getAdapteeManager().supportsLeaseAdmin());
    }


    /**
     *  Tests for the availability of a lease notification service. 
     *
     *  @return <code> true </code> if lease notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseNotification() {
        return (getAdapteeManager().supportsLeaseNotification());
    }


    /**
     *  Tests if a lease to campus lookup session is available. 
     *
     *  @return <code> true </code> if lease campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseCampus() {
        return (getAdapteeManager().supportsLeaseCampus());
    }


    /**
     *  Tests if a lease to campus assignment session is available. 
     *
     *  @return <code> true </code> if lease campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseCampusAssignment() {
        return (getAdapteeManager().supportsLeaseCampusAssignment());
    }


    /**
     *  Tests if a lease smart campus session is available. 
     *
     *  @return <code> true </code> if lease smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseSmartCampus() {
        return (getAdapteeManager().supportsLeaseSmartCampus());
    }


    /**
     *  Tests for the availability of an deed lookup service. 
     *
     *  @return <code> true </code> if deed lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedLookup() {
        return (getAdapteeManager().supportsDeedLookup());
    }


    /**
     *  Tests if querying deeds is available. 
     *
     *  @return <code> true </code> if deed query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedQuery() {
        return (getAdapteeManager().supportsDeedQuery());
    }


    /**
     *  Tests if searching for deeds is available. 
     *
     *  @return <code> true </code> if deed search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedSearch() {
        return (getAdapteeManager().supportsDeedSearch());
    }


    /**
     *  Tests for the availability of a deed administrative service for 
     *  creating and deleting deeds. 
     *
     *  @return <code> true </code> if deed administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedAdmin() {
        return (getAdapteeManager().supportsDeedAdmin());
    }


    /**
     *  Tests for the availability of a deed notification service. 
     *
     *  @return <code> true </code> if deed notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedNotification() {
        return (getAdapteeManager().supportsDeedNotification());
    }


    /**
     *  Tests if a deed to campus lookup session is available. 
     *
     *  @return <code> true </code> if deed campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedCampus() {
        return (getAdapteeManager().supportsDeedCampus());
    }


    /**
     *  Tests if a deed to campus assignment session is available. 
     *
     *  @return <code> true </code> if deed campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedCampusAssignment() {
        return (getAdapteeManager().supportsDeedCampusAssignment());
    }


    /**
     *  Tests if a deed smart campus session is available. 
     *
     *  @return <code> true </code> if deed smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedSmartCampus() {
        return (getAdapteeManager().supportsDeedSmartCampus());
    }


    /**
     *  Tests if a service to manage squatters in bulk is available. 
     *
     *  @return <code> true </code> if a room batch squatting service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSquattingBatch() {
        return (getAdapteeManager().supportsRoomSquattingBatch());
    }


    /**
     *  Gets the supported <code> Lease </code> record types. 
     *
     *  @return a list containing the supported lease record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLeaseRecordTypes() {
        return (getAdapteeManager().getLeaseRecordTypes());
    }


    /**
     *  Tests if the given <code> Lease </code> record type is supported. 
     *
     *  @param  leaseRecordType a <code> Type </code> indicating a <code> 
     *          Lease </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> leaseRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLeaseRecordType(org.osid.type.Type leaseRecordType) {
        return (getAdapteeManager().supportsLeaseRecordType(leaseRecordType));
    }


    /**
     *  Gets the supported lease search record types. 
     *
     *  @return a list containing the supported lease search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLeaseSearchRecordTypes() {
        return (getAdapteeManager().getLeaseSearchRecordTypes());
    }


    /**
     *  Tests if the given lease search record type is supported. 
     *
     *  @param  leaseSearchRecordType a <code> Type </code> indicating a lease 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> leaseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLeaseSearchRecordType(org.osid.type.Type leaseSearchRecordType) {
        return (getAdapteeManager().supportsLeaseSearchRecordType(leaseSearchRecordType));
    }


    /**
     *  Gets the supported <code> Deed </code> record types. 
     *
     *  @return a list containing the supported deed record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeedRecordTypes() {
        return (getAdapteeManager().getDeedRecordTypes());
    }


    /**
     *  Tests if the given <code> Deed </code> record type is supported. 
     *
     *  @param  deedRecordType a <code> Type </code> indicating a <code> Deed 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deedRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeedRecordType(org.osid.type.Type deedRecordType) {
        return (getAdapteeManager().supportsDeedRecordType(deedRecordType));
    }


    /**
     *  Gets the supported deed search record types. 
     *
     *  @return a list containing the supported deed search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeedSearchRecordTypes() {
        return (getAdapteeManager().getDeedSearchRecordTypes());
    }


    /**
     *  Tests if the given deed search record type is supported. 
     *
     *  @param  deedSearchRecordType a <code> Type </code> indicating a deed 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deedSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeedSearchRecordType(org.osid.type.Type deedSearchRecordType) {
        return (getAdapteeManager().supportsDeedSearchRecordType(deedSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service. 
     *
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseLookupSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service. 
     *
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseQuerySessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service. 
     *
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseSearchSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administrative service. 
     *
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseAdminSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service. 
     *
     *  @param  leaseReceiver the receiver 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSession(org.osid.room.squatting.LeaseReceiver leaseReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseNotificationSession(leaseReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service for the given campus. 
     *
     *  @param  leaseReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSessionForCampus(org.osid.room.squatting.LeaseReceiver leaseReceiver, 
                                                                                                 org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseNotificationSessionForCampus(leaseReceiver, campusId));
    }


    /**
     *  Gets the session for retrieving lease to campus mappings. 
     *
     *  @return a <code> LeaseCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusSession getLeaseCampusSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseCampusSession());
    }


    /**
     *  Gets the session for assigning lease to campus mappings. 
     *
     *  @return a <code> LeaseCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusAssignmentSession getLeaseCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseCampusAssignmentSession());
    }


    /**
     *  Gets the session associated with the lease smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> LeaseSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSmartCampusSession getLeaseSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLeaseSmartCampusSession(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service. 
     *
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedLookupSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service. 
     *
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedQuerySessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service. 
     *
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedSearchSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administrative service. 
     *
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedAdminSessionForCampus(campusId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service. 
     *
     *  @param  DeedReceiver the receiver 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSession(org.osid.room.squatting.DeedReceiver DeedReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedNotificationSession(DeedReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service for the given campus. 
     *
     *  @param  DeedReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSessionForCampus(org.osid.room.squatting.DeedReceiver DeedReceiver, 
                                                                                               org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedNotificationSessionForCampus(DeedReceiver, campusId));
    }


    /**
     *  Gets the session for retrieving deed to campus mappings. 
     *
     *  @return a <code> DeedCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusSession getDeedCampusSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedCampusSession());
    }


    /**
     *  Gets the session for assigning deed to campus mappings. 
     *
     *  @return a <code> DeedCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusAssignmentSession getDeedCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedCampusAssignmentSession());
    }


    /**
     *  Gets the session associated with the deed smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> DeedSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSmartCampusSession getDeedSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeedSmartCampusSession(campusId));
    }


    /**
     *  Gets a <code> RoomSquattingBatchManager. </code> 
     *
     *  @return a <code> RoomSquattingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSquattingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.RoomSquattingBatchManager getRoomSquattingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoomSquattingBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
