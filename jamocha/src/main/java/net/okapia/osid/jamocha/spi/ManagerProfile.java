//
// ManagerProfile.java
//
//     Manages profile information.
//
//
// Tom Coppeto
// Okapia
// 27 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Manages profile information.
 */

public class ManagerProfile {

    private final Types proxyRecordTypes = new TypeRefSet();
    private final java.util.Collection<org.osid.locale.Locale> locales = new java.util.ArrayList<>();
    private final java.util.Collection<org.osid.installation.Version> osidVersions = new java.util.HashSet<>();


    /**
     *  Creates a new {@code ManagePprofile}
     */
    
    public ManagerProfile() {
        this.osidVersions.add(net.okapia.osid.OsidVersions.V3_0_0.getVersion());        
        return;
    }


    /**
     *  Test for support of an OSID version. 
     *
     *  @param  version the version string to test 
     *  @return <code> true </code> if this manager supports the given 
     *          version, <code> false </code> otherwise 
     */

    public boolean supportsOSIDVersion(org.osid.installation.Version version) {
        for (org.osid.installation.Version v : this.osidVersions) {
            if (v.equals(version)) {
                return (true);
            }
        } 
        
        return (false);
    }


    /**
     *  Adds support for an OSID version.
     *
     *  @param version an OSID version
     *  @throws org.osid.NullargumentException {@code version} is
     *          {@code null}
     */

    public void addOSIDVersion(org.osid.installation.Version version) {
        nullarg(version, "OSID version");
        this.osidVersions.add(version);
        return;
    }


    /**
     *  Gets the locales supported in this service. 
     *
     *  @return list of locales supported 
     */

    public org.osid.locale.LocaleList getLocales() {
        return (new net.okapia.osid.jamocha.locale.locale.ArrayLocaleList(this.locales));
    }


    /**
     *  Adds support for a locale.
     *
     *  @param locale the locale to add
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code>
     */

    public void addLocale(org.osid.locale.Locale locale) {
        nullarg(locale, "locale");
        this.locales.add(locale);
        return;
    }


    /**
     *  Gets the proxy record <code> Types </code> supported in this
     *  service.  If no proxy manager is available, an empty list is
     *  returned.
     *
     *  @return list of proxy record types supported 
     *  @throws org.osid.IllegalStateException this manager has been shut down 
     */

    public org.osid.type.TypeList getProxyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.proxyRecordTypes.toCollection()));
    }


    /**
     *  Test for support of a proxy type. 
     *
     *  @param  proxyRecordType a proxy record type 
     *  @return <code> true </code> if this service supports the given proxy 
     *          record type, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proxyRecordType </code> 
     *          is <code> null </code> 
     */

    public boolean supportsProxyRecordType(org.osid.type.Type proxyRecordType) {
        return (this.proxyRecordTypes.contains(proxyRecordType));
    }


    /**
     *  Adds support for a proxy record type.
     *
     *  @param type a proxy type to add
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public void addProxyRecordType(org.osid.type.Type type) {
        this.proxyRecordTypes.add(type);
        return;
    }


    /**
     *  Removes support for a proxy record type.
     *
     *  @param type
     *  @throws org.osid.NullArgumentException
     *          <code>type</code> is <code>null</code>
     */

    public void removeProxyRecordType(org.osid.type.Type type) {
        this.proxyRecordTypes.remove(type);
        return;
    }
}
