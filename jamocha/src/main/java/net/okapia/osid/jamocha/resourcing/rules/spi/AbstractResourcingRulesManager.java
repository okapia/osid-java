//
// AbstractResourcingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractResourcingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.resourcing.rules.ResourcingRulesManager,
               org.osid.resourcing.rules.ResourcingRulesProxyManager {

    private final Types availabilityEnablerRecordTypes     = new TypeRefSet();
    private final Types availabilityEnablerSearchRecordTypes= new TypeRefSet();

    private final Types commissionEnablerRecordTypes       = new TypeRefSet();
    private final Types commissionEnablerSearchRecordTypes = new TypeRefSet();

    private final Types jobConstrainerRecordTypes          = new TypeRefSet();
    private final Types jobConstrainerSearchRecordTypes    = new TypeRefSet();

    private final Types jobConstrainerEnablerRecordTypes   = new TypeRefSet();
    private final Types jobConstrainerEnablerSearchRecordTypes= new TypeRefSet();

    private final Types jobProcessorRecordTypes            = new TypeRefSet();
    private final Types jobProcessorSearchRecordTypes      = new TypeRefSet();

    private final Types jobProcessorEnablerRecordTypes     = new TypeRefSet();
    private final Types jobProcessorEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractResourcingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractResourcingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any job federation is exposed. Federation is exposed when a 
     *  specific job may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of jobs appears 
     *  as a single job. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching availability enabler is supported. 
     *
     *  @return <code> true </code> if availability enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an availability enabler administrative service is supported. 
     *
     *  @return <code> true </code> if availability enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an availability enabler notification service is supported. 
     *
     *  @return <code> true </code> if availability enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an availability enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerFoundry() {
        return (false);
    }


    /**
     *  Tests if an availability enabler foundry service is supported. 
     *
     *  @return <code> true </code> if availability enabler foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if an availability enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if an availability enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an availability enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an availability enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if an availability enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching commission enabler is supported. 
     *
     *  @return <code> true </code> if commission enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a commission enabler administrative service is supported. 
     *
     *  @return <code> true </code> if commission enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a commission enabler notification service is supported. 
     *
     *  @return <code> true </code> if commission enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a commission enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerFoundry() {
        return (false);
    }


    /**
     *  Tests if a commission enabler foundry service is supported. 
     *
     *  @return <code> true </code> if commission enabler foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a commission enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if a commission enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a commission enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a commission enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a commission enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching job constrainer is supported. 
     *
     *  @return <code> true </code> if job constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if a job constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if job constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if a job constrainer notification service is supported. 
     *
     *  @return <code> true </code> if job constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if a job constrainer foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerFoundry() {
        return (false);
    }


    /**
     *  Tests if a job constrainer foundry service is supported. 
     *
     *  @return <code> true </code> if job constrainer foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a job constrainer foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if a job constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a job constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a job constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching job constrainer enablers is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if job constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler foundry lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler foundry 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerFoundry() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler foundry service is supported. 
     *
     *  @return <code> true </code> if job constrainer enabler foundry 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler foundry lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler foundry 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a job constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if job constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up job processor is supported. 
     *
     *  @return <code> true </code> if job processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying job processor is supported. 
     *
     *  @return <code> true </code> if job processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching job processor is supported. 
     *
     *  @return <code> true </code> if job processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a job processor administrative service is supported. 
     *
     *  @return <code> true </code> if job processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a job processor notification service is supported. 
     *
     *  @return <code> true </code> if job processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a job processor foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor foundry lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorFoundry() {
        return (false);
    }


    /**
     *  Tests if a job processor foundry service is supported. 
     *
     *  @return <code> true </code> if job processor foundry assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a job processor foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor foundry service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if a job processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a job processor rule application service is supported. 
     *
     *  @return <code> true </code> if job processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching job processor enablers is supported. 
     *
     *  @return <code> true </code> if job processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if job processor enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if job processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor enabler foundry lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerFoundry() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler foundry service is supported. 
     *
     *  @return <code> true </code> if job processor enabler foundry 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerFoundryAssignment() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler foundry lookup service is supported. 
     *
     *  @return <code> true </code> if a job processor enabler foundry service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSmartFoundry() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a job processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if job processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> AvailabilityEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> AvailabilityEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.availabilityEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AvailabilityEnabler </code> record type is 
     *  supported. 
     *
     *  @param  availabilityEnablerRecordType a <code> Type </code> indicating 
     *          an <code> AvailabilityEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAvailabilityEnablerRecordType(org.osid.type.Type availabilityEnablerRecordType) {
        return (this.availabilityEnablerRecordTypes.contains(availabilityEnablerRecordType));
    }


    /**
     *  Adds support for an availability enabler record type.
     *
     *  @param availabilityEnablerRecordType an availability enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>availabilityEnablerRecordType</code> is <code>null</code>
     */

    protected void addAvailabilityEnablerRecordType(org.osid.type.Type availabilityEnablerRecordType) {
        this.availabilityEnablerRecordTypes.add(availabilityEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an availability enabler record type.
     *
     *  @param availabilityEnablerRecordType an availability enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>availabilityEnablerRecordType</code> is <code>null</code>
     */

    protected void removeAvailabilityEnablerRecordType(org.osid.type.Type availabilityEnablerRecordType) {
        this.availabilityEnablerRecordTypes.remove(availabilityEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AvailabilityEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AvailabilityEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAvailabilityEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.availabilityEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Gets the supported <code> CommissionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CommissionEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commissionEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CommissionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  commissionEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CommissionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerRecordType(org.osid.type.Type commissionEnablerRecordType) {
        return (this.commissionEnablerRecordTypes.contains(commissionEnablerRecordType));
    }


    /**
     *  Adds support for a commission enabler record type.
     *
     *  @param commissionEnablerRecordType a commission enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>commissionEnablerRecordType</code> is <code>null</code>
     */

    protected void addCommissionEnablerRecordType(org.osid.type.Type commissionEnablerRecordType) {
        this.commissionEnablerRecordTypes.add(commissionEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a commission enabler record type.
     *
     *  @param commissionEnablerRecordType a commission enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commissionEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCommissionEnablerRecordType(org.osid.type.Type commissionEnablerRecordType) {
        this.commissionEnablerRecordTypes.remove(commissionEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CommissionEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CommissionEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommissionEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commissionEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CommissionEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  commissionEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CommissionEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCommissionEnablerSearchRecordType(org.osid.type.Type commissionEnablerSearchRecordType) {
        return (this.commissionEnablerSearchRecordTypes.contains(commissionEnablerSearchRecordType));
    }


    /**
     *  Adds support for a commission enabler search record type.
     *
     *  @param commissionEnablerSearchRecordType a commission enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>commissionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCommissionEnablerSearchRecordType(org.osid.type.Type commissionEnablerSearchRecordType) {
        this.commissionEnablerSearchRecordTypes.add(commissionEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a commission enabler search record type.
     *
     *  @param commissionEnablerSearchRecordType a commission enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commissionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCommissionEnablerSearchRecordType(org.osid.type.Type commissionEnablerSearchRecordType) {
        this.commissionEnablerSearchRecordTypes.remove(commissionEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> JobConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  jobConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> JobConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerRecordType(org.osid.type.Type jobConstrainerRecordType) {
        return (this.jobConstrainerRecordTypes.contains(jobConstrainerRecordType));
    }


    /**
     *  Adds support for a job constrainer record type.
     *
     *  @param jobConstrainerRecordType a job constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerRecordType</code> is <code>null</code>
     */

    protected void addJobConstrainerRecordType(org.osid.type.Type jobConstrainerRecordType) {
        this.jobConstrainerRecordTypes.add(jobConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for a job constrainer record type.
     *
     *  @param jobConstrainerRecordType a job constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeJobConstrainerRecordType(org.osid.type.Type jobConstrainerRecordType) {
        this.jobConstrainerRecordTypes.remove(jobConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> JobConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobConstrainer </code> search record type is 
     *  supported. 
     *
     *  @param  jobConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerSearchRecordType(org.osid.type.Type jobConstrainerSearchRecordType) {
        return (this.jobConstrainerSearchRecordTypes.contains(jobConstrainerSearchRecordType));
    }


    /**
     *  Adds support for a job constrainer search record type.
     *
     *  @param jobConstrainerSearchRecordType a job constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addJobConstrainerSearchRecordType(org.osid.type.Type jobConstrainerSearchRecordType) {
        this.jobConstrainerSearchRecordTypes.add(jobConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a job constrainer search record type.
     *
     *  @param jobConstrainerSearchRecordType a job constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeJobConstrainerSearchRecordType(org.osid.type.Type jobConstrainerSearchRecordType) {
        this.jobConstrainerSearchRecordTypes.remove(jobConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> JobConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobConstrainerEnabler </code> record type is 
     *  supported. 
     *
     *  @param  jobConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerRecordType(org.osid.type.Type jobConstrainerEnablerRecordType) {
        return (this.jobConstrainerEnablerRecordTypes.contains(jobConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for a job constrainer enabler record type.
     *
     *  @param jobConstrainerEnablerRecordType a job constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addJobConstrainerEnablerRecordType(org.osid.type.Type jobConstrainerEnablerRecordType) {
        this.jobConstrainerEnablerRecordTypes.add(jobConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a job constrainer enabler record type.
     *
     *  @param jobConstrainerEnablerRecordType a job constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeJobConstrainerEnablerRecordType(org.osid.type.Type jobConstrainerEnablerRecordType) {
        this.jobConstrainerEnablerRecordTypes.remove(jobConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> JobConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  jobConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsJobConstrainerEnablerSearchRecordType(org.osid.type.Type jobConstrainerEnablerSearchRecordType) {
        return (this.jobConstrainerEnablerSearchRecordTypes.contains(jobConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a job constrainer enabler search record type.
     *
     *  @param jobConstrainerEnablerSearchRecordType a job constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addJobConstrainerEnablerSearchRecordType(org.osid.type.Type jobConstrainerEnablerSearchRecordType) {
        this.jobConstrainerEnablerSearchRecordTypes.add(jobConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a job constrainer enabler search record type.
     *
     *  @param jobConstrainerEnablerSearchRecordType a job constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeJobConstrainerEnablerSearchRecordType(org.osid.type.Type jobConstrainerEnablerSearchRecordType) {
        this.jobConstrainerEnablerSearchRecordTypes.remove(jobConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> JobProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobProcessor </code> record type is 
     *  supported. 
     *
     *  @param  jobProcessorRecordType a <code> Type </code> indicating a 
     *          <code> JobProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> jobProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorRecordType(org.osid.type.Type jobProcessorRecordType) {
        return (this.jobProcessorRecordTypes.contains(jobProcessorRecordType));
    }


    /**
     *  Adds support for a job processor record type.
     *
     *  @param jobProcessorRecordType a job processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorRecordType</code> is <code>null</code>
     */

    protected void addJobProcessorRecordType(org.osid.type.Type jobProcessorRecordType) {
        this.jobProcessorRecordTypes.add(jobProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a job processor record type.
     *
     *  @param jobProcessorRecordType a job processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorRecordType</code> is <code>null</code>
     */

    protected void removeJobProcessorRecordType(org.osid.type.Type jobProcessorRecordType) {
        this.jobProcessorRecordTypes.remove(jobProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> JobProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  jobProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> JobProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorSearchRecordType(org.osid.type.Type jobProcessorSearchRecordType) {
        return (this.jobProcessorSearchRecordTypes.contains(jobProcessorSearchRecordType));
    }


    /**
     *  Adds support for a job processor search record type.
     *
     *  @param jobProcessorSearchRecordType a job processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addJobProcessorSearchRecordType(org.osid.type.Type jobProcessorSearchRecordType) {
        this.jobProcessorSearchRecordTypes.add(jobProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a job processor search record type.
     *
     *  @param jobProcessorSearchRecordType a job processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeJobProcessorSearchRecordType(org.osid.type.Type jobProcessorSearchRecordType) {
        this.jobProcessorSearchRecordTypes.remove(jobProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> JobProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  jobProcessorEnablerRecordType a <code> Type </code> indicating 
     *          a <code> JobProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerRecordType(org.osid.type.Type jobProcessorEnablerRecordType) {
        return (this.jobProcessorEnablerRecordTypes.contains(jobProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a job processor enabler record type.
     *
     *  @param jobProcessorEnablerRecordType a job processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addJobProcessorEnablerRecordType(org.osid.type.Type jobProcessorEnablerRecordType) {
        this.jobProcessorEnablerRecordTypes.add(jobProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a job processor enabler record type.
     *
     *  @param jobProcessorEnablerRecordType a job processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeJobProcessorEnablerRecordType(org.osid.type.Type jobProcessorEnablerRecordType) {
        this.jobProcessorEnablerRecordTypes.remove(jobProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> JobProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> JobProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJobProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.jobProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JobProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  jobProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> JobProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsJobProcessorEnablerSearchRecordType(org.osid.type.Type jobProcessorEnablerSearchRecordType) {
        return (this.jobProcessorEnablerSearchRecordTypes.contains(jobProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a job processor enabler search record type.
     *
     *  @param jobProcessorEnablerSearchRecordType a job processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addJobProcessorEnablerSearchRecordType(org.osid.type.Type jobProcessorEnablerSearchRecordType) {
        this.jobProcessorEnablerSearchRecordTypes.add(jobProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a job processor enabler search record type.
     *
     *  @param jobProcessorEnablerSearchRecordType a job processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>jobProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeJobProcessorEnablerSearchRecordType(org.osid.type.Type jobProcessorEnablerSearchRecordType) {
        this.jobProcessorEnablerSearchRecordTypes.remove(jobProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service. 
     *
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerLookupSession getAvailabilityEnablerLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service. 
     *
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuerySession getAvailabilityEnablerQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler search service. 
     *
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSearchSession getAvailabilityEnablerSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service. 
     *
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerAdminSession getAvailabilityEnablerAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSession(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSession(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service for the given foundry. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver, 
                                                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler notification service for the given foundry. 
     *
     *  @param  availabilityConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          availabilityConstrainerReceiver, foundryId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerNotificationSession getAvailabilityEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.AvailabilityEnablerReceiver availabilityConstrainerReceiver, 
                                                                                                                                org.osid.id.Id foundryId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability 
     *  enabler/foundry mappings for availability enablers. 
     *
     *  @return an <code> AvailabilityEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundrySession getAvailabilityEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup availability 
     *  enabler/foundry mappings for availability enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundrySession getAvailabilityEnablerFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availability enabler to foundries. 
     *
     *  @return an <code> AvailabilityEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundryAssignmentSession getAvailabilityEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  availability enabler to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerFoundryAssignmentSession getAvailabilityEnablerFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage availability enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSmartFoundrySession getAvailabilityEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage availability enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerSmartFoundrySession getAvailabilityEnablerSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for looking up the rules applied to an 
     *  availability. 
     *
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for looking up the rules applied to tan 
     *  availability. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to an availability. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to an availability. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleLookupSession getAvailabilityEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service to apply to availabilities. 
     *
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service to apply to availabilities. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service for the given foundry to apply to 
     *  availabilities. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getAvailabilityEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the availability 
     *  enabler assignment service for the given foundry to apply to 
     *  availabilities. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AvailabilityEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerRuleApplicationSession getAvailabilityEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getAvailabilityEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service. 
     *
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerLookupSession getCommissionEnablerLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service. 
     *
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerQuerySession getCommissionEnablerQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler search service. 
     *
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          CommissionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSearchSession getCommissionEnablerSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service. 
     *
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerAdminSession getCommissionEnablerAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSession(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSession(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service for the given foundry. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver, 
                                                                                                                            org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler notification service for the given foundry. 
     *
     *  @param  commissionConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commissionConstrainerReceiver, foundryId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerNotificationSession getCommissionEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.CommissionEnablerReceiver commissionConstrainerReceiver, 
                                                                                                                            org.osid.id.Id foundryId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission 
     *  enabler/foundry mappings for commission enablers. 
     *
     *  @return a <code> CommissionEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundrySession getCommissionEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commission 
     *  enabler/foundry mappings for commission enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundrySession getCommissionEnablerFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commission enabler to foundries. 
     *
     *  @return a <code> CommissionEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundryAssignmentSession getCommissionEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commission enabler to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerFoundryAssignmentSession getCommissionEnablerFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission enabler smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSmartFoundrySession getCommissionEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commission enabler smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerSmartFoundrySession getCommissionEnablerSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  commission. 
     *
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for looking up the rules applied to a 
     *  commission. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to a commission. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler mapping lookup service for the given foundry for looking up 
     *  rules applied to a commission. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleLookupSession getCommissionEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service to apply to commissions. 
     *
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service to apply to commissions. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service for the given foundry to apply to 
     *  commissions. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getCommissionEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commission 
     *  enabler assignment service for the given foundry to apply to 
     *  commissions. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommissionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommissionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerRuleApplicationSession getCommissionEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getCommissionEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service. 
     *
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerLookupSession getJobConstrainerLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service. 
     *
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuerySession getJobConstrainerQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer search service. 
     *
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSearchSession getJobConstrainerSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service. 
     *
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerAdminSession getJobConstrainerAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSession(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSession(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service for the given foundry. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver, 
                                                                                                                      org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer notification service for the given foundry. 
     *
     *  @param  jobConstrainerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerReceiver, 
     *          foundryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerNotificationSession getJobConstrainerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerReceiver jobConstrainerReceiver, 
                                                                                                                      org.osid.id.Id foundryId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer/foundry 
     *  mappings for job constrainers. 
     *
     *  @return a <code> JobConstrainerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundrySession getJobConstrainerFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer/foundry 
     *  mappings for job constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundrySession getJobConstrainerFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer to foundries. 
     *
     *  @return a <code> JobConstrainerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundryAssignmentSession getJobConstrainerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerFoundryAssignmentSession getJobConstrainerFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSmartFoundrySession getJobConstrainerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerSmartFoundrySession getJobConstrainerSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a job. 
     *
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a job. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for the given foundry for looking 
     *  up rules applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer mapping lookup service for the given foundry for looking 
     *  up rules applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleLookupSession getJobConstrainerRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service to apply to jobs. 
     *
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service to apply to jobs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service for the given foundry to apply to jobs. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer assignment service for the given foundry to apply to jobs. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerRuleApplicationSession getJobConstrainerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerLookupSession getJobConstrainerEnablerLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service. 
     *
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerQuerySession getJobConstrainerEnablerQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler search service. 
     *
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSearchSession getJobConstrainerEnablerSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerAdminSession getJobConstrainerEnablerAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSession(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSession(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service for the given foundry. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler notification service for the given foundry. 
     *
     *  @param  jobConstrainerEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobConstrainerEnablerReceiver, foundryId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerNotificationSession getJobConstrainerEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobConstrainerEnablerReceiver jobConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id foundryId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer 
     *  enabler/foundry mappings for job constrainer enablers. 
     *
     *  @return a <code> JobConstrainerEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundrySession getJobConstrainerEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job constrainer 
     *  enabler/foundry mappings for job constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundrySession getJobConstrainerEnablerFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer enablers to foundries. 
     *
     *  @return a <code> JobConstrainerEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundryAssignmentSession getJobConstrainerEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  constrainer enablers to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerFoundryAssignmentSession getJobConstrainerEnablerFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSmartFoundrySession getJobConstrainerEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job constrainer enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerSmartFoundrySession getJobConstrainerEnablerSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler mapping lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleLookupSession getJobConstrainerEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobConstrainerEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job 
     *  constrainer enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerRuleApplicationSession getJobConstrainerEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobConstrainerEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service. 
     *
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorLookupSession getJobProcessorLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service. 
     *
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuerySession getJobProcessorQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  search service. 
     *
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSearchSession getJobProcessorSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service. 
     *
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorAdminSession getJobProcessorAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSession(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSession(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service for the given foundry. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver 
     *          </code> or <code> foundryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver, 
                                                                                                                  org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  notification service for the given foundry. 
     *
     *  @param  jobProcessorReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> jobProcessorReceiver, 
     *          foundryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorNotificationSession getJobProcessorNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorReceiver jobProcessorReceiver, 
                                                                                                                  org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor/foundry 
     *  mappings for job processors. 
     *
     *  @return a <code> JobProcessorFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundrySession getJobProcessorFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor/foundry 
     *  mappings for job processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundry() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundrySession getJobProcessorFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor to foundries. 
     *
     *  @return a <code> JobProcessorFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundryAssignmentSession getJobProcessorFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorFoundryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorFoundryAssignmentSession getJobProcessorFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSmartFoundrySession getJobProcessorSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor smart 
     *  foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorSmartFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorSmartFoundrySession getJobProcessorSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for looking up the rules applied to the job. 
     *
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for looking up the rules applied to a job. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for the given foundry for looking up rules 
     *  applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  mapping lookup service for the given foundry for looking up rules 
     *  applied to a job. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleLookupSession getJobProcessorRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service. 
     *
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service to apply to jobs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  assignment service for the given foundry to apply to jobs. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorRuleApplicationSession getJobProcessorRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service. 
     *
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerLookupSession getJobProcessorEnablerLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service. 
     *
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler query service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerQuerySession getJobProcessorEnablerQuerySessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerQuerySessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler search service. 
     *
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enablers earch service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSearchSession getJobProcessorEnablerSearchSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerSearchSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service. 
     *
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler administration service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerAdminSession getJobProcessorEnablerAdminSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerAdminSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSession(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSession(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service for the given foundry. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver </code> or <code> foundryId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver, 
                                                                                                                                org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler notification service for the given foundry. 
     *
     *  @param  jobProcessorEnablerReceiver the notification callback 
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no foundry found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          jobProcessorEnablerReceiver, foundryId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerNotificationSession getJobProcessorEnablerNotificationSessionForFoundry(org.osid.resourcing.rules.JobProcessorEnablerReceiver jobProcessorEnablerReceiver, 
                                                                                                                                org.osid.id.Id foundryId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerNotificationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor 
     *  enabler/foundry mappings for job processor enablers. 
     *
     *  @return a <code> JobProcessorEnablerFoundrySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundrySession getJobProcessorEnablerFoundrySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup job processor 
     *  enabler/foundry mappings for job processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerFoundrySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundry() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundrySession getJobProcessorEnablerFoundrySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor enablers to foundries. 
     *
     *  @return a <code> JobProcessorEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundryAssignmentSession getJobProcessorEnablerFoundryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning job 
     *  processor enablers to foundries. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerFoundryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerFoundryAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerFoundryAssignmentSession getJobProcessorEnablerFoundryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerFoundryAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSmartFoundrySession getJobProcessorEnablerSmartFoundrySession(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage job processor enabler 
     *  smart foundries. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerSmartFoundrySession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerSmartFoundry() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerSmartFoundrySession getJobProcessorEnablerSmartFoundrySession(org.osid.id.Id foundryId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerSmartFoundrySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler mapping lookup service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleLookupSession getJobProcessorEnablerRuleLookupSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerRuleLookupSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service. 
     *
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesManager.getJobProcessorEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the job processor 
     *  enabler assignment service for the given foundry. 
     *
     *  @param  foundryId the <code> Id </code> of the <code> Foundry </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JobProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Foundry </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJobProcessorEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerRuleApplicationSession getJobProcessorEnablerRuleApplicationSessionForFoundry(org.osid.id.Id foundryId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resourcing.rules.ResourcingRulesProxyManager.getJobProcessorEnablerRuleApplicationSessionForFoundry not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.availabilityEnablerRecordTypes.clear();
        this.availabilityEnablerRecordTypes.clear();

        this.availabilityEnablerSearchRecordTypes.clear();
        this.availabilityEnablerSearchRecordTypes.clear();

        this.commissionEnablerRecordTypes.clear();
        this.commissionEnablerRecordTypes.clear();

        this.commissionEnablerSearchRecordTypes.clear();
        this.commissionEnablerSearchRecordTypes.clear();

        this.jobConstrainerRecordTypes.clear();
        this.jobConstrainerRecordTypes.clear();

        this.jobConstrainerSearchRecordTypes.clear();
        this.jobConstrainerSearchRecordTypes.clear();

        this.jobConstrainerEnablerRecordTypes.clear();
        this.jobConstrainerEnablerRecordTypes.clear();

        this.jobConstrainerEnablerSearchRecordTypes.clear();
        this.jobConstrainerEnablerSearchRecordTypes.clear();

        this.jobProcessorRecordTypes.clear();
        this.jobProcessorRecordTypes.clear();

        this.jobProcessorSearchRecordTypes.clear();
        this.jobProcessorSearchRecordTypes.clear();

        this.jobProcessorEnablerRecordTypes.clear();
        this.jobProcessorEnablerRecordTypes.clear();

        this.jobProcessorEnablerSearchRecordTypes.clear();
        this.jobProcessorEnablerSearchRecordTypes.clear();

        return;
    }
}
