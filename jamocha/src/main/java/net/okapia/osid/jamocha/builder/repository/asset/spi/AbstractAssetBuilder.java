//
// AbstractAsset.java
//
//     Defines an Asset builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.asset.spi;


/**
 *  Defines an <code>Asset</code> builder.
 */

public abstract class AbstractAssetBuilder<T extends AbstractAssetBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractSourceableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.repository.asset.AssetMiter asset;


    /**
     *  Constructs a new <code>AbstractAssetBuilder</code>.
     *
     *  @param asset the asset to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAssetBuilder(net.okapia.osid.jamocha.builder.repository.asset.AssetMiter asset) {
        super(asset);
        this.asset = asset;
        return;
    }


    /**
     *  Builds the asset.
     *
     *  @return the new asset
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.repository.Asset build() {
        (new net.okapia.osid.jamocha.builder.validator.repository.asset.AssetValidator(getValidations())).validate(this.asset);
        return (new net.okapia.osid.jamocha.builder.repository.asset.ImmutableAsset(this.asset));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the asset miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.repository.asset.AssetMiter getMiter() {
        return (this.asset);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the copyright status known flag.
     *
     *  @return the builder
     */

    public T copyrightStatusKnown() {
        getMiter().setCopyrightStatusKnown(true);
        return (self());
    }


    /**
     *  Sets the uncopyright status known flag.
     *
     *  @return the builder
     */

    public T copyrightStatusUnknown() {
        getMiter().setCopyrightStatusKnown(false);
        return (self());
    }


    /**
     *  Sets the public domain flag and enables all the distribution
     *  rights if <code>true</code>.
     *
     *  @return the builder
     */

    public T publicDomain() {
        getMiter().setPublicDomain(true);
        return (self());
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    public T copyright(org.osid.locale.DisplayText copyright) {
        getMiter().setCopyright(copyright);
        return (self());
    }


    /**
     *  Sets the copyright registration.
     *
     *  @param registration a copyright registration
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    public T copyrightRegistration(String registration) {
        getMiter().setCopyrightRegistration(registration);
        return (self());
    }


    /**
     *  Sets the can distribute verbatim flag.
     *
     *  @return the builder
     */

    public T canDistributeVerbatim() {
        getMiter().setCanDistributeVerbatim(true);
        return (self());
    }


    /**
     *  Unsets the can distribute verbatim flag.
     *
     *  @return the builder
     */

    public T cannotDistributeVerbatim() {
        getMiter().setCanDistributeVerbatim(false);
        return (self());
    }


    /**
     *  Sets the can distribute alterations flag.
     *
     *  @return the builder
     */

    public T canDistributeAlterations() {
        getMiter().setCanDistributeAlterations(true);
        return (self());
    }


    /**
     *  Unsets the can distribute alterations flag.
     *
     *  @return the builder
     */

    public T cannotDistributeAlterations() {
        getMiter().setCanDistributeAlterations(false);
        return (self());
    }


    /**
     *  Sets the can distribute compositions flag.
     *
     *  @return the builder
     */

    public T canDistributeCompositions() {
        getMiter().setCanDistributeCompositions(true);
        return (self());
    }


    /**
     *  Unsets the can distribute compositions flag.
     *
     *  @return the builder
     */

    public T cannotDistributeCompositions() {
        getMiter().setCanDistributeCompositions(false);
        return (self());
    }


    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>source</code> is
     *          <code>null</code>
     */

    public T source(org.osid.resource.Resource source) {
        getMiter().setSource(source);
        return (self());
    }


    /**
     *  Adds a provider link.
     *
     *  @param providerLink a provider link
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>providerLink</code> is <code>null</code>
     */

    public T providerLink(org.osid.resource.Resource providerLink) {
        getMiter().addProviderLink(providerLink);
        return (self());
    }


    /**
     *  Sets all the provider links.
     *
     *  @param providerLinks a collection of provider links
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>providerLinks</code> is <code>null</code>
     */

    public T providerLinks(java.util.Collection<org.osid.resource.Resource> providerLinks) {
        getMiter().setProviderLinks(providerLinks);
        return (self());
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T createdDate(org.osid.calendaring.DateTime date) {
        getMiter().setCreatedDate(date);
        return (self());
    }


    /**
     *  Sets the published date.
     *
     *  @param date a published date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T publishedDate(org.osid.calendaring.DateTime date) {
        getMiter().setPublishedDate(date);
        return (self());
    }


    /**
     *  Sets the principal credit string.
     *
     *  @param credit a principal credit string
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credit</code> is
     *          <code>null</code>
     */

    public T principalCreditString(org.osid.locale.DisplayText credit) {
        getMiter().setPrincipalCreditString(credit);
        return (self());
    }


    /**
     *  Adds an asset content.
     *
     *  @param assetContent an asset content
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assetContent</code> is <code>null</code>
     */

    public T assetContent(org.osid.repository.AssetContent assetContent) {
        getMiter().addAssetContent(assetContent);
        return (self());
    }


    /**
     *  Sets all the asset contents.
     *
     *  @param assetContents a collection of asset contents
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assetContents</code> is <code>null</code>
     */

    public T assetContents(java.util.Collection<org.osid.repository.AssetContent> assetContents) {
        getMiter().setAssetContents(assetContents);
        return (self());
    }


    /**
     *  Sets the composition.
     *
     *  @param composition a composition
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>composition</code> is <code>null</code>
     */

    public T composition(org.osid.repository.Composition composition) {
        getMiter().setComposition(composition);
        return (self());
    }


    /**
     *  Adds an Asset record.
     *
     *  @param record an asset record
     *  @param recordType the type of asset record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.repository.records.AssetRecord record, org.osid.type.Type recordType) {
        getMiter().addAssetRecord(record, recordType);
        return (self());
    }
}       


