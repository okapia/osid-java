//
// WorkMiter.java
//
//     Defines a Work miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.work;


/**
 *  Defines a <code>Work</code> miter for use with the builders.
 */

public interface WorkMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.resourcing.Work {


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @throws org.osid.NullArgumentException <code>job</code> is
     *          <code>null</code>
     */

    public void setJob(org.osid.resourcing.Job job);


    /**
     *  Adds a competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    public void addCompetency(org.osid.resourcing.Competency competency);


    /**
     *  Sets all the competencies.
     *
     *  @param competencies a collection of competencies
     *  @throws org.osid.NullArgumentException
     *          <code>competencies</code> is <code>null</code>
     */

    public void setCompetencies(java.util.Collection<org.osid.resourcing.Competency> competencies);


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCreatedDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the completion date.
     *
     *  @param date a completion date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCompletionDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a Work record.
     *
     *  @param record a work record
     *  @param recordType the type of work record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addWorkRecord(org.osid.resourcing.records.WorkRecord record, org.osid.type.Type recordType);
}       


