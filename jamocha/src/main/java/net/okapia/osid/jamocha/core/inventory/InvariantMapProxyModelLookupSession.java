//
// InvariantMapProxyModelLookupSession
//
//    Implements a Model lookup service backed by a fixed
//    collection of models. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Model lookup service backed by a fixed
 *  collection of models. The models are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyModelLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractMapModelLookupSession
    implements org.osid.inventory.ModelLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyModelLookupSession} with no
     *  models.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyModelLookupSession} with a single
     *  model.
     *
     *  @param warehouse the warehouse
     *  @param model a single model
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code model} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Model model, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putModel(model);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyModelLookupSession} using
     *  an array of models.
     *
     *  @param warehouse the warehouse
     *  @param models an array of models
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code models} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Model[] models, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putModels(models);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyModelLookupSession} using a
     *  collection of models.
     *
     *  @param warehouse the warehouse
     *  @param models a collection of models
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code models} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  java.util.Collection<? extends org.osid.inventory.Model> models,
                                                  org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putModels(models);
        return;
    }
}
