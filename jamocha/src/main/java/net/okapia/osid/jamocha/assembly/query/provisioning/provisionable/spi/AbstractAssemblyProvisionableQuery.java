//
// AbstractAssemblyProvisionableQuery.java
//
//     A ProvisionableQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.provisionable.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProvisionableQuery that stores terms.
 */

public abstract class AbstractAssemblyProvisionableQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.provisioning.ProvisionableQuery,
               org.osid.provisioning.ProvisionableQueryInspector,
               org.osid.provisioning.ProvisionableSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionableQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionableQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionableSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProvisionableQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProvisionableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        getAssembler().addIdTerm(getPoolIdColumn(), poolId, match);
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        getAssembler().clearTerms(getPoolIdColumn());
        return;
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (getAssembler().getIdTerms(getPoolIdColumn()));
    }


    /**
     *  Orders the results by pool. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPool(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPoolColumn(), style);
        return;
    }


    /**
     *  Gets the PoolId column name.
     *
     * @return the column name
     */

    protected String getPoolIdColumn() {
        return ("pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        getAssembler().clearTerms(getPoolColumn());
        return;
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Tests if a pool search order is available. 
     *
     *  @return <code> true </code> if a pool search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearchOrder() {
        return (false);
    }


    /**
     *  Gets the pool search order. 
     *
     *  @return the pool search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsPoolSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchOrder getPoolSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPoolSearchOrder() is false");
    }


    /**
     *  Gets the Pool column name.
     *
     * @return the column name
     */

    protected String getPoolColumn() {
        return ("pool");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Orders the results by resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches provisionables provisioned the number of times in the given 
     *  range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchUse(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getUseColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the position query terms. 
     */

    @OSID @Override
    public void clearUseTerms() {
        getAssembler().clearTerms(getUseColumn());
        return;
    }


    /**
     *  Gets the use query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getUseTerms() {
        return (getAssembler().getCardinalRangeTerms(getUseColumn()));
    }


    /**
     *  Orders the results by use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByUse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getUseColumn(), style);
        return;
    }


    /**
     *  Gets the Use column name.
     *
     * @return the column name
     */

    protected String getUseColumn() {
        return ("use");
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this provisionable supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return <code>true</code> if the provisionableRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionableRecordType) {
        for (org.osid.provisioning.records.ProvisionableQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  provisionableRecordType the provisionable record type 
     *  @return the provisionable query record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionableRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableQueryRecord getProvisionableQueryRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  provisionableRecordType the provisionable record type 
     *  @return the provisionable query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionableRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableQueryInspectorRecord getProvisionableQueryInspectorRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param provisionableRecordType the provisionable record type
     *  @return the provisionable search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionableRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionableSearchOrderRecord getProvisionableSearchOrderRecord(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionableSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(provisionableRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionableRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this provisionable. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionableQueryRecord the provisionable query record
     *  @param provisionableQueryInspectorRecord the provisionable query inspector
     *         record
     *  @param provisionableSearchOrderRecord the provisionable search order record
     *  @param provisionableRecordType provisionable record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableQueryRecord</code>,
     *          <code>provisionableQueryInspectorRecord</code>,
     *          <code>provisionableSearchOrderRecord</code> or
     *          <code>provisionableRecordTypeprovisionable</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionableRecords(org.osid.provisioning.records.ProvisionableQueryRecord provisionableQueryRecord, 
                                      org.osid.provisioning.records.ProvisionableQueryInspectorRecord provisionableQueryInspectorRecord, 
                                      org.osid.provisioning.records.ProvisionableSearchOrderRecord provisionableSearchOrderRecord, 
                                      org.osid.type.Type provisionableRecordType) {

        addRecordType(provisionableRecordType);

        nullarg(provisionableQueryRecord, "provisionable query record");
        nullarg(provisionableQueryInspectorRecord, "provisionable query inspector record");
        nullarg(provisionableSearchOrderRecord, "provisionable search odrer record");

        this.queryRecords.add(provisionableQueryRecord);
        this.queryInspectorRecords.add(provisionableQueryInspectorRecord);
        this.searchOrderRecords.add(provisionableSearchOrderRecord);
        
        return;
    }
}
