//
// SequenceRuleMiter.java
//
//     Defines a SequenceRule miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule;


/**
 *  Defines a <code>SequenceRule</code> miter for use with the builders.
 */

public interface SequenceRuleMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.assessment.authoring.SequenceRule {


    /**
     *  Sets the assessment part.
     *
     *  @param assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    public void setAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart);


    /**
     *  Sets the next assessment part.
     *
     *  @param nextAssessmentPart a next assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>nextAssessmentPart</code> is <code>null</code>
     */

    public void setNextAssessmentPart(org.osid.assessment.authoring.AssessmentPart nextAssessmentPart);


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     */

    public void setMinimumScore(long score);


    /**
     *  Sets the maximum score.
     *
     *  @param score a maximum score
     */

    public void setMaximumScore(long score);


    /**
     *  Sets the cumulative flag.
     *
     *  @param cumulative <code> true </code> if the score is applied
     *         to all previous assessment parts, <code> false </code>
     *         otherwise
     */

    public void setCumulative(boolean cumulative);


    /**
     *  Adds an applied assessment part.
     *
     *  @param appliedAssessmentPart an applied assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentPart</code> is <code>null</code>
     */

    public void addAppliedAssessmentPart(org.osid.assessment.authoring.AssessmentPart appliedAssessmentPart);


    /**
     *  Sets all the applied assessment parts.
     *
     *  @param appliedAssessmentParts a collection of applied assessment parts
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentParts</code> is <code>null</code>
     */

    public void setAppliedAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> appliedAssessmentParts);


    /**
     *  Adds a SequenceRule record.
     *
     *  @param record a sequenceRule record
     *  @param recordType the type of sequenceRule record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSequenceRuleRecord(org.osid.assessment.authoring.records.SequenceRuleRecord record, org.osid.type.Type recordType);
}       


