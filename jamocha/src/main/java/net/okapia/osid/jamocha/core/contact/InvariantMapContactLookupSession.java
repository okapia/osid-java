//
// InvariantMapContactLookupSession
//
//    Implements a Contact lookup service backed by a fixed collection of
//    contacts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact;


/**
 *  Implements a Contact lookup service backed by a fixed
 *  collection of contacts. The contacts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapContactLookupSession
    extends net.okapia.osid.jamocha.core.contact.spi.AbstractMapContactLookupSession
    implements org.osid.contact.ContactLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapContactLookupSession</code> with no
     *  contacts.
     *  
     *  @param addressBook the address book
     *  @throws org.osid.NullArgumnetException {@code addressBook} is
     *          {@code null}
     */

    public InvariantMapContactLookupSession(org.osid.contact.AddressBook addressBook) {
        setAddressBook(addressBook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactLookupSession</code> with a single
     *  contact.
     *  
     *  @param addressBook the address book
     *  @param contact a single contact
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contact} is <code>null</code>
     */

      public InvariantMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                               org.osid.contact.Contact contact) {
        this(addressBook);
        putContact(contact);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactLookupSession</code> using an array
     *  of contacts.
     *  
     *  @param addressBook the address book
     *  @param contacts an array of contacts
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contacts} is <code>null</code>
     */

      public InvariantMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                               org.osid.contact.Contact[] contacts) {
        this(addressBook);
        putContacts(contacts);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactLookupSession</code> using a
     *  collection of contacts.
     *
     *  @param addressBook the address book
     *  @param contacts a collection of contacts
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contacts} is <code>null</code>
     */

      public InvariantMapContactLookupSession(org.osid.contact.AddressBook addressBook,
                                               java.util.Collection<? extends org.osid.contact.Contact> contacts) {
        this(addressBook);
        putContacts(contacts);
        return;
    }
}
