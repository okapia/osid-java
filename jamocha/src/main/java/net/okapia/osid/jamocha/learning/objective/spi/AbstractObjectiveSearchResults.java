//
// AbstractObjectiveSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractObjectiveSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.learning.ObjectiveSearchResults {

    private org.osid.learning.ObjectiveList objectives;
    private final org.osid.learning.ObjectiveQueryInspector inspector;
    private final java.util.Collection<org.osid.learning.records.ObjectiveSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractObjectiveSearchResults.
     *
     *  @param objectives the result set
     *  @param objectiveQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          or <code>objectiveQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractObjectiveSearchResults(org.osid.learning.ObjectiveList objectives,
                                            org.osid.learning.ObjectiveQueryInspector objectiveQueryInspector) {
        nullarg(objectives, "objectives");
        nullarg(objectiveQueryInspector, "objective query inspectpr");

        this.objectives = objectives;
        this.inspector = objectiveQueryInspector;

        return;
    }


    /**
     *  Gets the objective list resulting from a search.
     *
     *  @return an objective list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectives() {
        if (this.objectives == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.learning.ObjectiveList objectives = this.objectives;
        this.objectives = null;
	return (objectives);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.learning.ObjectiveQueryInspector getObjectiveQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  objective search record <code> Type. </code> This method must
     *  be used to retrieve an objective implementing the requested
     *  record.
     *
     *  @param objectiveSearchRecordType an objective search 
     *         record type 
     *  @return the objective search
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(objectiveSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ObjectiveSearchResultsRecord getObjectiveSearchResultsRecord(org.osid.type.Type objectiveSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.learning.records.ObjectiveSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(objectiveSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(objectiveSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record objective search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addObjectiveRecord(org.osid.learning.records.ObjectiveSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "objective record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
