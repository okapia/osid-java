//
// AbstractQueryPeriodLookupSession.java
//
//    An inline adapter that maps a PeriodLookupSession to
//    a PeriodQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PeriodLookupSession to
 *  a PeriodQuerySession.
 */

public abstract class AbstractQueryPeriodLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractPeriodLookupSession
    implements org.osid.billing.PeriodLookupSession {

    private final org.osid.billing.PeriodQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPeriodLookupSession.
     *
     *  @param querySession the underlying period query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPeriodLookupSession(org.osid.billing.PeriodQuerySession querySession) {
        nullarg(querySession, "period query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Period</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPeriods() {
        return (this.session.canSearchPeriods());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>Period</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Period</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Period</code> and
     *  retained for compatibility.
     *
     *  @param  periodId <code>Id</code> of the
     *          <code>Period</code>
     *  @return the period
     *  @throws org.osid.NotFoundException <code>periodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>periodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod(org.osid.id.Id periodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchId(periodId, true);
        org.osid.billing.PeriodList periods = this.session.getPeriodsByQuery(query);
        if (periods.hasNext()) {
            return (periods.getNextPeriod());
        } 
        
        throw new org.osid.NotFoundException(periodId + " not found");
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  periods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Periods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  periodIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>periodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByIds(org.osid.id.IdList periodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();

        try (org.osid.id.IdList ids = periodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> which does not include
     *  periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchGenusType(periodGenusType, true);
        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> and include any additional
     *  periods with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByParentGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchParentGenusType(periodGenusType, true);
        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets a <code>PeriodList</code> containing the given
     *  period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  periodRecordType a period record type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByRecordType(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchRecordType(periodRecordType, true);
        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets a <code> PeriodList </code> where to the given <code>
     *  DateTime </code> range falls within the period
     *  inclusive. Periods containing the given date are matched. In
     *  plenary mode, the returned list contains all of the periods
     *  specified in the <code> Id </code> list, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or inaccessible.
     *  Otherwise, inaccessible <code> Periods </code> may be omitted
     *  from the list including returning a unique set.
     *
     *  @param from start of date range 
     *  @param to end of date range 
     *  @return the returned <code> Period </code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsOnDate(org.osid.calendaring.DateTime from,
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchOpenDate(from, to, true);
        query.matchCloseDate(from, to, true);
        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets all <code>Periods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Periods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.PeriodQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPeriodsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.PeriodQuery getQuery() {
        org.osid.billing.PeriodQuery query = this.session.getPeriodQuery();
        return (query);
    }
}
