//
// RequestTransactionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.requesttransaction.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RequestTransactionElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the RequestTransactionElement Id.
     *
     *  @return the request transaction element Id
     */

    public static org.osid.id.Id getRequestTransactionEntityId() {
        return (makeEntityId("osid.provisioning.RequestTransaction"));
    }


    /**
     *  Gets the BrokerId element Id.
     *
     *  @return the BrokerId element Id
     */

    public static org.osid.id.Id getBrokerId() {
        return (makeElementId("osid.provisioning.requesttransaction.BrokerId"));
    }


    /**
     *  Gets the Broker element Id.
     *
     *  @return the Broker element Id
     */

    public static org.osid.id.Id getBroker() {
        return (makeElementId("osid.provisioning.requesttransaction.Broker"));
    }


    /**
     *  Gets the SubmitDate element Id.
     *
     *  @return the SubmitDate element Id
     */

    public static org.osid.id.Id getSubmitDate() {
        return (makeElementId("osid.provisioning.requesttransaction.SubmitDate"));
    }


    /**
     *  Gets the SubmitterId element Id.
     *
     *  @return the SubmitterId element Id
     */

    public static org.osid.id.Id getSubmitterId() {
        return (makeElementId("osid.provisioning.requesttransaction.SubmitterId"));
    }


    /**
     *  Gets the Submitter element Id.
     *
     *  @return the Submitter element Id
     */

    public static org.osid.id.Id getSubmitter() {
        return (makeElementId("osid.provisioning.requesttransaction.Submitter"));
    }


    /**
     *  Gets the SubmittingAgentId element Id.
     *
     *  @return the SubmittingAgentId element Id
     */

    public static org.osid.id.Id getSubmittingAgentId() {
        return (makeElementId("osid.provisioning.requesttransaction.SubmittingAgentId"));
    }


    /**
     *  Gets the SubmittingAgent element Id.
     *
     *  @return the SubmittingAgent element Id
     */

    public static org.osid.id.Id getSubmittingAgent() {
        return (makeElementId("osid.provisioning.requesttransaction.SubmittingAgent"));
    }


    /**
     *  Gets the RequestIds element Id.
     *
     *  @return the RequestIds element Id
     */

    public static org.osid.id.Id getRequestIds() {
        return (makeElementId("osid.provisioning.requesttransaction.RequestIds"));
    }


    /**
     *  Gets the Requests element Id.
     *
     *  @return the Requests element Id
     */

    public static org.osid.id.Id getRequests() {
        return (makeElementId("osid.provisioning.requesttransaction.Requests"));
    }
}
