//
// AbstractVotingRulesManager.java
//
//     An adapter for a VotingRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a VotingRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterVotingRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.voting.rules.VotingRulesManager>
    implements org.osid.voting.rules.VotingRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterVotingRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterVotingRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterVotingRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterVotingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerLookup() {
        return (getAdapteeManager().supportsRaceConstrainerLookup());
    }


    /**
     *  Tests if querying race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerQuery() {
        return (getAdapteeManager().supportsRaceConstrainerQuery());
    }


    /**
     *  Tests if searching race constrainer is supported. 
     *
     *  @return <code> true </code> if race constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSearch() {
        return (getAdapteeManager().supportsRaceConstrainerSearch());
    }


    /**
     *  Tests if a race constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if race constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerAdmin() {
        return (getAdapteeManager().supportsRaceConstrainerAdmin());
    }


    /**
     *  Tests if a race constrainer notification service is supported. 
     *
     *  @return <code> true </code> if race constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerNotification() {
        return (getAdapteeManager().supportsRaceConstrainerNotification());
    }


    /**
     *  Tests if a race constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer polls lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerPolls() {
        return (getAdapteeManager().supportsRaceConstrainerPolls());
    }


    /**
     *  Tests if a race constrainer polls service is supported. 
     *
     *  @return <code> true </code> if race constrainer polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerPollsAssignment() {
        return (getAdapteeManager().supportsRaceConstrainerPollsAssignment());
    }


    /**
     *  Tests if a race constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSmartPolls() {
        return (getAdapteeManager().supportsRaceConstrainerSmartPolls());
    }


    /**
     *  Tests if a race constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRuleLookup() {
        return (getAdapteeManager().supportsRaceConstrainerRuleLookup());
    }


    /**
     *  Tests if a race constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a race constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRuleApplication() {
        return (getAdapteeManager().supportsRaceConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching race constrainer enablers is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerSearch());
    }


    /**
     *  Tests if a race constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if race constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a race constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerNotification());
    }


    /**
     *  Tests if a race constrainer enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerPolls() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerPolls());
    }


    /**
     *  Tests if a race constrainer enabler polls service is supported. 
     *
     *  @return <code> true </code> if race constrainer enabler polls 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerPollsAssignment() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerPollsAssignment());
    }


    /**
     *  Tests if a race constrainer enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler polls 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSmartPolls() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerSmartPolls());
    }


    /**
     *  Tests if a race constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a race constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if race constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsRaceConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up race processor is supported. 
     *
     *  @return <code> true </code> if race processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorLookup() {
        return (getAdapteeManager().supportsRaceProcessorLookup());
    }


    /**
     *  Tests if querying race processor is supported. 
     *
     *  @return <code> true </code> if race processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorQuery() {
        return (getAdapteeManager().supportsRaceProcessorQuery());
    }


    /**
     *  Tests if searching race processor is supported. 
     *
     *  @return <code> true </code> if race processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSearch() {
        return (getAdapteeManager().supportsRaceProcessorSearch());
    }


    /**
     *  Tests if a race processor administrative service is supported. 
     *
     *  @return <code> true </code> if race processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorAdmin() {
        return (getAdapteeManager().supportsRaceProcessorAdmin());
    }


    /**
     *  Tests if a race processor notification service is supported. 
     *
     *  @return <code> true </code> if race processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorNotification() {
        return (getAdapteeManager().supportsRaceProcessorNotification());
    }


    /**
     *  Tests if a race processor polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor polls lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorPolls() {
        return (getAdapteeManager().supportsRaceProcessorPolls());
    }


    /**
     *  Tests if a race processor polls service is supported. 
     *
     *  @return <code> true </code> if race processor polls assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorPollsAssignment() {
        return (getAdapteeManager().supportsRaceProcessorPollsAssignment());
    }


    /**
     *  Tests if a race processor polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSmartPolls() {
        return (getAdapteeManager().supportsRaceProcessorSmartPolls());
    }


    /**
     *  Tests if a race processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRuleLookup() {
        return (getAdapteeManager().supportsRaceProcessorRuleLookup());
    }


    /**
     *  Tests if a race processor rule application service is supported. 
     *
     *  @return <code> true </code> if race processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRuleApplication() {
        return (getAdapteeManager().supportsRaceProcessorRuleApplication());
    }


    /**
     *  Tests if looking up race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerLookup() {
        return (getAdapteeManager().supportsRaceProcessorEnablerLookup());
    }


    /**
     *  Tests if querying race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerQuery() {
        return (getAdapteeManager().supportsRaceProcessorEnablerQuery());
    }


    /**
     *  Tests if searching race processor enablers is supported. 
     *
     *  @return <code> true </code> if race processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSearch() {
        return (getAdapteeManager().supportsRaceProcessorEnablerSearch());
    }


    /**
     *  Tests if a race processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if race processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsRaceProcessorEnablerAdmin());
    }


    /**
     *  Tests if a race processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if race processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerNotification() {
        return (getAdapteeManager().supportsRaceProcessorEnablerNotification());
    }


    /**
     *  Tests if a race processor enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor enabler polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerPolls() {
        return (getAdapteeManager().supportsRaceProcessorEnablerPolls());
    }


    /**
     *  Tests if a race processor enabler polls service is supported. 
     *
     *  @return <code> true </code> if race processor enabler polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerPollsAssignment() {
        return (getAdapteeManager().supportsRaceProcessorEnablerPollsAssignment());
    }


    /**
     *  Tests if a race processor enabler polls lookup service is supported. 
     *
     *  @return <code> true </code> if a race processor enabler polls service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSmartPolls() {
        return (getAdapteeManager().supportsRaceProcessorEnablerSmartPolls());
    }


    /**
     *  Tests if a race processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsRaceProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a race processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if race processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsRaceProcessorEnablerRuleApplication());
    }


    /**
     *  Tests if looking up ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerLookup() {
        return (getAdapteeManager().supportsBallotConstrainerLookup());
    }


    /**
     *  Tests if querying ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerQuery() {
        return (getAdapteeManager().supportsBallotConstrainerQuery());
    }


    /**
     *  Tests if searching ballot constrainer is supported. 
     *
     *  @return <code> true </code> if ballot constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSearch() {
        return (getAdapteeManager().supportsBallotConstrainerSearch());
    }


    /**
     *  Tests if a ballot constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerAdmin() {
        return (getAdapteeManager().supportsBallotConstrainerAdmin());
    }


    /**
     *  Tests if a ballot constrainer notification service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerNotification() {
        return (getAdapteeManager().supportsBallotConstrainerNotification());
    }


    /**
     *  Tests if a ballot constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer polls lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerPolls() {
        return (getAdapteeManager().supportsBallotConstrainerPolls());
    }


    /**
     *  Tests if a ballot constrainer polls service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer polls assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerPollsAssignment() {
        return (getAdapteeManager().supportsBallotConstrainerPollsAssignment());
    }


    /**
     *  Tests if a ballot constrainer polls lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer polls service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSmartPolls() {
        return (getAdapteeManager().supportsBallotConstrainerSmartPolls());
    }


    /**
     *  Tests if a ballot constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRuleLookup() {
        return (getAdapteeManager().supportsBallotConstrainerRuleLookup());
    }


    /**
     *  Tests if a ballot constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a ballot constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRuleApplication() {
        return (getAdapteeManager().supportsBallotConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching ballot constrainer enablers is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerSearch());
    }


    /**
     *  Tests if a ballot constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a ballot constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerNotification());
    }


    /**
     *  Tests if a ballot constrainer enabler polls lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler polls 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerPolls() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerPolls());
    }


    /**
     *  Tests if a ballot constrainer enabler polls service is supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler polls 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerPollsAssignment() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerPollsAssignment());
    }


    /**
     *  Tests if a ballot constrainer enabler polls lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler polls 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSmartPolls() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerSmartPolls());
    }


    /**
     *  Tests if a ballot constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a ballot constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a ballot constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if ballot constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsBallotConstrainerEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> RaceConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerRecordTypes() {
        return (getAdapteeManager().getRaceConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  raceConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> RaceConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerRecordType(org.osid.type.Type raceConstrainerRecordType) {
        return (getAdapteeManager().supportsRaceConstrainerRecordType(raceConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> RaceConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getRaceConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  raceConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerSearchRecordType(org.osid.type.Type raceConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsRaceConstrainerSearchRecordType(raceConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> RaceConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RaceConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getRaceConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  raceConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerRecordType(org.osid.type.Type raceConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsRaceConstrainerEnablerRecordType(raceConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> RaceConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RaceConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getRaceConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  raceConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceConstrainerEnablerSearchRecordType(org.osid.type.Type raceConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsRaceConstrainerEnablerSearchRecordType(raceConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> RaceProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> RaceProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorRecordTypes() {
        return (getAdapteeManager().getRaceProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceProcessor </code> record type is 
     *  supported. 
     *
     *  @param  raceProcessorRecordType a <code> Type </code> indicating a 
     *          <code> RaceProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> raceProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorRecordType(org.osid.type.Type raceProcessorRecordType) {
        return (getAdapteeManager().supportsRaceProcessorRecordType(raceProcessorRecordType));
    }


    /**
     *  Gets the supported <code> RaceProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> RaceProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorSearchRecordTypes() {
        return (getAdapteeManager().getRaceProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  raceProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> RaceProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorSearchRecordType(org.osid.type.Type raceProcessorSearchRecordType) {
        return (getAdapteeManager().supportsRaceProcessorSearchRecordType(raceProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> RaceProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> RaceProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getRaceProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  raceProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RaceProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerRecordType(org.osid.type.Type raceProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsRaceProcessorEnablerRecordType(raceProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> RaceProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> RaceProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRaceProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getRaceProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> RaceProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  raceProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RaceProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRaceProcessorEnablerSearchRecordType(org.osid.type.Type raceProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsRaceProcessorEnablerSearchRecordType(raceProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> BallotConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> BallotConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerRecordTypes() {
        return (getAdapteeManager().getBallotConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> BallotConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  ballotConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> BallotConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerRecordType(org.osid.type.Type ballotConstrainerRecordType) {
        return (getAdapteeManager().supportsBallotConstrainerRecordType(ballotConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> BallotConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> BallotConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getBallotConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BallotConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  ballotConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerSearchRecordType(org.osid.type.Type ballotConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsBallotConstrainerSearchRecordType(ballotConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> BallotConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          BallotConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getBallotConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> BallotConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  ballotConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsBallotConstrainerEnablerRecordType(ballotConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> BallotConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          BallotConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBallotConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getBallotConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BallotConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  ballotConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BallotConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsBallotConstrainerEnablerSearchRecordType(org.osid.type.Type ballotConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsBallotConstrainerEnablerSearchRecordType(ballotConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service. 
     *
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerLookupSession getRaceConstrainerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service. 
     *
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuerySession getRaceConstrainerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer search service. 
     *
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSearchSession getRaceConstrainerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service. 
     *
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerAdminSession getRaceConstrainerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSession(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerNotificationSession(raceConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer notification service for the given polls. 
     *
     *  @param  raceConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerNotificationSession getRaceConstrainerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerReceiver raceConstrainerReceiver, 
                                                                                                                  org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerNotificationSessionForPolls(raceConstrainerReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer/polls 
     *  mappings for race constrainers. 
     *
     *  @return a <code> RaceConstrainerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsSession getRaceConstrainerPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer to polls. 
     *
     *  @return a <code> RaceConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerPollsAssignmentSession getRaceConstrainerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerSmartPollsSession getRaceConstrainerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a race. 
     *
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a race. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleLookupSession getRaceConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service to apply to races. 
     *
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer assignment service for the given polls to apply to races. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerRuleApplicationSession getRaceConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerRuleApplicationSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerLookupSession getRaceConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service. 
     *
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerQuerySession getRaceConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler search service. 
     *
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSearchSession getRaceConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerAdminSession getRaceConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSession(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerNotificationSession(raceConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  raceConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceConstrainerEnablerReceiver </code> or <code> pollsId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerNotificationSession getRaceConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceConstrainerEnablerReceiver raceConstrainerEnablerReceiver, 
                                                                                                                                org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerNotificationSessionForPolls(raceConstrainerEnablerReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race constrainer 
     *  enabler/polls mappings for race constrainer enablers. 
     *
     *  @return a <code> RaceConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsSession getRaceConstrainerEnablerPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  constrainer enablers to polls. 
     *
     *  @return a <code> RaceConstrainerEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerPollsAssignmentSession getRaceConstrainerEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race constrainer enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerSmartPollsSession getRaceConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleLookupSession getRaceConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerRuleApplicationSession getRaceConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceConstrainerEnablerRuleApplicationSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service. 
     *
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorLookupSession getRaceProcessorLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service. 
     *
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorQuerySession getRaceProcessorQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  search service. 
     *
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSearchSession getRaceProcessorSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service. 
     *
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorAdminSession getRaceProcessorAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSession(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorNotificationSession(raceProcessorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  notification service for the given polls. 
     *
     *  @param  raceProcessorReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> raceProcessorReceiver 
     *          </code> or <code> pollsId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorNotificationSession getRaceProcessorNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorReceiver raceProcessorReceiver, 
                                                                                                              org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorNotificationSessionForPolls(raceProcessorReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor/polls 
     *  mappings for race processors. 
     *
     *  @return a <code> RaceProcessorPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPolls() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsSession getRaceProcessorPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor to polls. 
     *
     *  @return a <code> RaceProcessorPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorPollsAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorPollsAssignmentSession getRaceProcessorPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorSmartPollsSession getRaceProcessorSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for looking up the rules applied to a race. 
     *
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  mapping lookup service for the given polls for looking up rules 
     *  applied to a race. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleLookupSession getRaceProcessorRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service. 
     *
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorRuleApplicationSession getRaceProcessorRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorRuleApplicationSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service. 
     *
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerLookupSession getRaceProcessorEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service. 
     *
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerQuerySession getRaceProcessorEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler search service. 
     *
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSearchSession getRaceProcessorEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service. 
     *
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerAdminSession getRaceProcessorEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSession(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerNotificationSession(raceProcessorEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler notification service for the given polls. 
     *
     *  @param  raceProcessorEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          raceProcessorEnablerReceiver </code> or <code> pollsId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerNotificationSession getRaceProcessorEnablerNotificationSessionForPolls(org.osid.voting.rules.RaceProcessorEnablerReceiver raceProcessorEnablerReceiver, 
                                                                                                                            org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerNotificationSessionForPolls(raceProcessorEnablerReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup race processor 
     *  enabler/polls mappings for race processor enablers. 
     *
     *  @return a <code> RaceProcessorEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsSession getRaceProcessorEnablerPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning race 
     *  processor enablers to polls. 
     *
     *  @return a <code> RaceProcessorEnablerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerPollsAssignmentSession getRaceProcessorEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage race processor enabler 
     *  smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerSmartPollsSession getRaceProcessorEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler mapping lookup service. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleLookupSession getRaceProcessorEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service. 
     *
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the race processor 
     *  enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerRuleApplicationSession getRaceProcessorEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceProcessorEnablerRuleApplicationSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service. 
     *
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerLookupSession getBallotConstrainerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service. 
     *
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuerySession getBallotConstrainerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer search service. 
     *
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSearchSession getBallotConstrainerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service. 
     *
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerAdminSession getBallotConstrainerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSession(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerNotificationSession(ballotConstrainerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer notification service for the given polls. 
     *
     *  @param  ballotConstrainerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerReceiver </code> or <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerNotificationSession getBallotConstrainerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerReceiver ballotConstrainerReceiver, 
                                                                                                                      org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerNotificationSessionForPolls(ballotConstrainerReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer/polls 
     *  mappings for ballot constrainers. 
     *
     *  @return a <code> BallotConstrainerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsSession getBallotConstrainerPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer to polls. 
     *
     *  @return a <code> BallotConstrainerPollsAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerPollsAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerPollsAssignmentSession getBallotConstrainerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer smart 
     *  polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerSmartPolls() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerSmartPollsSession getBallotConstrainerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  the ballot. 
     *
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer mapping lookup service for the given polls for looking up 
     *  rules applied to a ballot. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleLookupSession getBallotConstrainerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service to apply to ballots. 
     *
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer assignment service for the given polls to apply to 
     *  ballots. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerRuleApplicationSession getBallotConstrainerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerRuleApplicationSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerLookupSession getBallotConstrainerEnablerLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service. 
     *
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler query service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerQuerySession getBallotConstrainerEnablerQuerySessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerQuerySessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler search service. 
     *
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enablers earch service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSearchSession getBallotConstrainerEnablerSearchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerSearchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerAdminSession getBallotConstrainerEnablerAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSession(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerNotificationSession(ballotConstrainerEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler notification service for the given polls. 
     *
     *  @param  ballotConstrainerEnablerReceiver the notification callback 
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no polls found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          ballotConstrainerEnablerReceiver </code> or <code> pollsId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerNotificationSession getBallotConstrainerEnablerNotificationSessionForPolls(org.osid.voting.rules.BallotConstrainerEnablerReceiver ballotConstrainerEnablerReceiver, 
                                                                                                                                    org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerNotificationSessionForPolls(ballotConstrainerEnablerReceiver, pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup ballot constrainer 
     *  enabler/polls mappings for ballot constrainer enablers. 
     *
     *  @return a <code> BallotConstrainerEnablerPollsSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsSession getBallotConstrainerEnablerPollsSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerPollsSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning ballot 
     *  constrainer enablers to polls. 
     *
     *  @return a <code> BallotConstrainerEnablerPollsAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerPollsAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerPollsAssignmentSession getBallotConstrainerEnablerPollsAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerPollsAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage ballot constrainer 
     *  enabler smart polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerSmartPollsSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerSmartPolls() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerSmartPollsSession getBallotConstrainerEnablerSmartPollsSession(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerSmartPollsSession(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler mapping lookup service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleLookupSession getBallotConstrainerEnablerRuleLookupSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerRuleLookupSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the ballot 
     *  constrainer enabler assignment service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerRuleApplicationSession getBallotConstrainerEnablerRuleApplicationSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotConstrainerEnablerRuleApplicationSessionForPolls(pollsId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
