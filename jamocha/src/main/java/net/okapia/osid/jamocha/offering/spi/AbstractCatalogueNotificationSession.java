//
// AbstractCatalogueNotificationSession.java
//
//     A template for making CatalogueNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Catalogue} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Catalogue} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for catalogue entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCatalogueNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.CatalogueNotificationSession {


    /**
     *  Tests if this user can register for {@code Catalogue}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCatalogueNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new catalogues. {@code
     *  CatalogueReceiver.newCatalogue()} is invoked when a new {@code
     *  Catalogue} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  catalogue. {@code CatalogueReceiver.newAncestorCatalogue()} is
     *  invoked when the specified catalogue node gets a new ancestor.
     *
     *  @param catalogueId the {@code Id} of the
     *         {@code Catalogue} node to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCatalogueAncestors(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  catalogue. {@code CatalogueReceiver.newDescendantCatalogue()}
     *  is invoked when the specified catalogue node gets a new
     *  descendant.
     *
     *  @param catalogueId the {@code Id} of the
     *         {@code Catalogue} node to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCatalogueDescendants(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated catalogues. {@code
     *  CatalogueReceiver.changedCatalogue()} is invoked when a
     *  catalogue is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated catalogue. {@code
     *  CatalogueReceiver.changedCatalogue()} is invoked when the
     *  specified catalogue is changed.
     *
     *  @param catalogueId the {@code Id} of the {@code Catalogue} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted catalogues. {@code
     *  CatalogueReceiver.deletedCatalogue()} is invoked when a
     *  catalogue is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted catalogue. {@code
     *  CatalogueReceiver.deletedCatalogue()} is invoked when the
     *  specified catalogue is deleted.
     *
     *  @param catalogueId the {@code Id} of the
     *          {@code Catalogue} to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified catalogue. {@code
     *  CatalogueReceiver.deletedAncestor()} is invoked when the
     *  specified catalogue node loses an ancestor.
     *
     *  @param catalogueId the {@code Id} of the
     *         {@code Catalogue} node to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCatalogueAncestors(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified catalogue. {@code
     *  CatalogueReceiver.deletedDescendant()} is invoked when the
     *  specified catalogue node loses a descendant.
     *
     *  @param catalogueId the {@code Id} of the
     *          {@code Catalogue} node to monitor
     *  @throws org.osid.NullArgumentException {@code catalogueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCatalogueDescendants(org.osid.id.Id catalogueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
