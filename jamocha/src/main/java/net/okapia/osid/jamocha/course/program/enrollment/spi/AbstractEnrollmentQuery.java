//
// AbstractEnrollmentQuery.java
//
//     A template for making an Enrollment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.enrollment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for enrollments.
 */

public abstract class AbstractEnrollmentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.program.EnrollmentQuery {

    private final java.util.Collection<org.osid.course.program.records.EnrollmentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the program offering <code> Id </code> for this query. 
     *
     *  @param  programOfferingId a program offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programOfferingId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProgramOfferingId(org.osid.id.Id programOfferingId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the program offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramOffering </code> is available. 
     *
     *  @return <code> true </code> if a program offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the program offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuery getProgramOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsProgramOfferingQuery() is false");
    }


    /**
     *  Clears the program offering terms. 
     */

    @OSID @Override
    public void clearProgramOfferingTerms() {
        return;
    }


    /**
     *  Sets the student resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student resource terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given enrollment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an enrollment implementing the requested record.
     *
     *  @param enrollmentRecordType an enrollment record type
     *  @return the enrollment query record
     *  @throws org.osid.NullArgumentException
     *          <code>enrollmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(enrollmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.EnrollmentQueryRecord getEnrollmentQueryRecord(org.osid.type.Type enrollmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.EnrollmentQueryRecord record : this.records) {
            if (record.implementsRecordType(enrollmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(enrollmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this enrollment query. 
     *
     *  @param enrollmentQueryRecord enrollment query record
     *  @param enrollmentRecordType enrollment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEnrollmentQueryRecord(org.osid.course.program.records.EnrollmentQueryRecord enrollmentQueryRecord, 
                                          org.osid.type.Type enrollmentRecordType) {

        addRecordType(enrollmentRecordType);
        nullarg(enrollmentQueryRecord, "enrollment query record");
        this.records.add(enrollmentQueryRecord);        
        return;
    }
}
