//
// AbstractDeviceEnablerQueryInspector.java
//
//     A template for making a DeviceEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for device enablers.
 */

public abstract class AbstractDeviceEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.control.rules.DeviceEnablerQueryInspector {

    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledTriggerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getRuledTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given device enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a device enabler implementing the requested record.
     *
     *  @param deviceEnablerRecordType a device enabler record type
     *  @return the device enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord getDeviceEnablerQueryInspectorRecord(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(deviceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this device enabler query. 
     *
     *  @param deviceEnablerQueryInspectorRecord device enabler query inspector
     *         record
     *  @param deviceEnablerRecordType deviceEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeviceEnablerQueryInspectorRecord(org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord deviceEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type deviceEnablerRecordType) {

        addRecordType(deviceEnablerRecordType);
        nullarg(deviceEnablerRecordType, "device enabler record type");
        this.records.add(deviceEnablerQueryInspectorRecord);        
        return;
    }
}
