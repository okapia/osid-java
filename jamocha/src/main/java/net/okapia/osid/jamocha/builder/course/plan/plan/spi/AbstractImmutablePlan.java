//
// AbstractImmutablePlan.java
//
//     Wraps a mutable Plan to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Plan</code> to hide modifiers. This
 *  wrapper provides an immutized Plan from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying plan whose state changes are visible.
 */

public abstract class AbstractImmutablePlan
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.plan.Plan {

    private final org.osid.course.plan.Plan plan;


    /**
     *  Constructs a new <code>AbstractImmutablePlan</code>.
     *
     *  @param plan the plan to immutablize
     *  @throws org.osid.NullArgumentException <code>plan</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePlan(org.osid.course.plan.Plan plan) {
        super(plan);
        this.plan = plan;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the syllabus. 
     *
     *  @return the syllabus <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSyllabusId() {
        return (this.plan.getSyllabusId());
    }


    /**
     *  Gets the syllabus. 
     *
     *  @return the syllabus 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus()
        throws org.osid.OperationFailedException {

        return (this.plan.getSyllabus());
    }


    /**
     *  Gets the <code> Id </code> of the course offering. 
     *
     *  @return the course offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.plan.getCourseOfferingId());
    }


    /**
     *  Gets the course offering. 
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.plan.getCourseOffering());
    }


    /**
     *  Gets the modules to apply to this plan. 
     *
     *  @return the module <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getModuleIds() {
        return (this.plan.getModuleIds());
    }


    /**
     *  Gets the modules to apply to this plan. 
     *
     *  @return the module 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException {

        return (this.plan.getModules());
    }


    /**
     *  Gets the plan record corresponding to the given <code> Plan </code> 
     *  record <code> Type. </code> This method must be used to retrieve an 
     *  object implementing the requested record. The <code> planRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(planRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  planRecordType the type of plan record to retrieve 
     *  @return the plan record 
     *  @throws org.osid.NullArgumentException <code> planRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(planRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanRecord getPlanRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        return (this.plan.getPlanRecord(planRecordType));
    }
}

