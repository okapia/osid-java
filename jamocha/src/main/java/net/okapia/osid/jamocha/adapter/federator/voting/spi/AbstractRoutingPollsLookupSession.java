//
// AbstractRoutingPollsLookupSession
//
//     A routing federating adapter for a PollsLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.IdHashMap;
import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A federating adapter for a PollsLookupSession. Sessions are
 *  added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 *
 *  The routing for Ids and Types are manually set by subclasses of
 *  this session that add child sessions for federation.
 *
 *  Routing for Ids is based on the existince of an Id embedded in the
 *  authority of a polls Id. The embedded Id is specified when
 *  adding child sessions. Optionally, a list of types for genus and
 *  record may also be mapped to a child session. A service Id for
 *  routing must be unique across all federated sessions while a Type
 *  may map to more than one session.
 *
 *  Lookup methods consult the mapping tables for Ids and Types, and
 *  will fallback to searching all sessions if the polls is not
 *  found and fallback is true. In the case of record and genus Type
 *  lookups, a fallback will search all child sessions regardless if
 *  any are found.
 */

public abstract class AbstractRoutingPollsLookupSession
    extends AbstractFederatingPollsLookupSession
    implements org.osid.voting.PollsLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.PollsLookupSession> sessionsById = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.PollsLookupSession>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.PollsLookupSession> sessionsByType = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.PollsLookupSession>());
    private final boolean idFallback;
    private final boolean typeFallback;

    
    /**
     *  Constructs a new
     *  <code>AbstractRoutingPollsLookupSession</code> with
     *  fallbacks for both Types and Ids.
     */

    protected AbstractRoutingPollsLookupSession() {
        this.idFallback   = true;
        this.typeFallback = true;
        selectAll();
        return;
    }


    /**
     *  Constructs a new
     *  <code>AbstractRoutingPollsLookupSession</code>.
     *
     *  @param idFallback <code>true</code> to fall back to searching
     *         all sessions if an Id is not in routing table
     *  @param typeFallback <code>true</code> to fall back to
     *         searching all sessions if a Type is not in routing
     *         table
     */

    protected AbstractRoutingPollsLookupSession(boolean idFallback, boolean typeFallback) {
        this.idFallback   = idFallback;
        this.typeFallback = typeFallback;
        selectAll();
        return;
    }


    /**
     *  Maps a session to a service Id. Use <code>addSession()</code>
     *  to add a session to this federation.
     *
     *  @param session a session to map
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void mapSession(org.osid.voting.PollsLookupSession session, org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.put(serviceId, session);
        return;
    }


    /**
     *  Maps a session to a record or genus Type. Use
     *  <code>addSession()</code> to add a session to this federation.
     *
     *  @param session a session to add
     *  @param type
     *  @throws org.osid.NullArgumentException <code>session</code> or
     *          <code>type</code> is <code>null</code>
     */

    protected void mapSession(org.osid.voting.PollsLookupSession session, org.osid.type.Type type) {
        nullarg(type, "type");
        this.sessionsByType.put(type, session);
        return;
    }


    /**
     *  Unmaps a session from a serviceId.
     *
     *  @param serviceId 
     *  @throws org.osid.NullArgumentException 
     *          <code>serviceId</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.id.Id serviceId) {
        nullarg(serviceId, "service Id");
        this.sessionsById.remove(serviceId);
        return;
    }


    /**
     *  Unmaps a session from both the Id and Type indices.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void unmapSession(org.osid.voting.PollsLookupSession session) {
        nullarg(session, "session");

        for (org.osid.id.Id id : this.sessionsById.keySet()) {
            if (session.equals(this.sessionsById.get(id))) {
                this.sessionsById.remove(id);
            }
        }

        this.sessionsByType.removeValue(session);

        return;
    }


    /**
     *  Removes a session from this federation.
     *
     *  @param session
     *  @throws org.osid.NullArgumentException 
     *          <code>session</code> is <code>null</code>
     */

    protected void removeSession(org.osid.voting.PollsLookupSession session) {
        unmapSession(session);
        super.removeSession(session);
        return;
    }


    /**
     *  Gets the <code>Polls</code> specified by its <code>Id</code>. 
     *
     *  @param  pollsId <code>Id</code> of the
     *          <code>Polls</code>
     *  @return the polls
     *  @throws org.osid.NotFoundException <code>pollsId</code> not 
     *          found in any session
     *  @throws org.osid.NullArgumentException <code>pollsId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.PollsLookupSession session = this.sessionsById.get(pollsId.getAuthority());
        if (session != null) {
            return (session.getPolls(pollsId));
        }
        
        if (this.idFallback) {
            return (super.getPolls(pollsId));
        }

        throw new org.osid.NotFoundException(pollsId + " not found");
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given polls
     *  genus <code>Type</code> which does not include pollses of
     *  types derived from the specified <code>Type</code>.
     *  
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();
        java.util.Collection<org.osid.voting.PollsLookupSession> sessions = this.sessionsByType.get(pollsGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPollsByGenusType(pollsGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.voting.polls.EmptyPollsList());
            }
        }


        for (org.osid.voting.PollsLookupSession session : sessions) {
            ret.addPollsList(session.getPollsByGenusType(pollsGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> corresponding to the given
     *  polls genus <code>Type</code> and include any additional
     *  pollses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  @param  pollsGenusType a polls genus type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByParentGenusType(org.osid.type.Type pollsGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();
        java.util.Collection<org.osid.voting.PollsLookupSession> sessions = this.sessionsByType.get(pollsGenusType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPollsByParentGenusType(pollsGenusType));
            } else {
                return (new net.okapia.osid.jamocha.nil.voting.polls.EmptyPollsList());
            }
        }


        for (org.osid.voting.PollsLookupSession session : sessions) {
            ret.addPollsList(session.getPollsByParentGenusType(pollsGenusType));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PollsList</code> containing the given
     *  polls record <code>Type</code>.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return the returned <code>Polls</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.PollsList getPollsByRecordType(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.polls.FederatingPollsList ret = getPollsList();
        java.util.Collection<org.osid.voting.PollsLookupSession> sessions = this.sessionsByType.get(pollsRecordType);

        if (sessions.size() == 0) {
            if (this.typeFallback) {
                return (super.getPollsByRecordType(pollsRecordType));
            } else {
                return (new net.okapia.osid.jamocha.nil.voting.polls.EmptyPollsList());
            }
        }

        for (org.osid.voting.PollsLookupSession session : sessions) {
            ret.addPollsList(session.getPollsByRecordType(pollsRecordType));
        }
        
        ret.noMore();
        return (ret);
    }
}
