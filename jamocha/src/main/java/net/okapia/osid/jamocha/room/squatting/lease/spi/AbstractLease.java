//
// AbstractLease.java
//
//     Defines a Lease.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Lease</code>.
 */

public abstract class AbstractLease
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.room.squatting.Lease {

    private org.osid.room.Room room;
    private org.osid.resource.Resource tenant;

    private final java.util.Collection<org.osid.room.squatting.records.LeaseRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the room. 
     *
     *  @return the room <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRoomId() {
        return (this.room.getId());
    }


    /**
     *  Gets the room. 
     *
     *  @return the room 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Room getRoom()
        throws org.osid.OperationFailedException {

        return (this.room);
    }


    /**
     *  Sets the room.
     *
     *  @param room a room
     *  @throws org.osid.NullArgumentException
     *          <code>room</code> is <code>null</code>
     */

    protected void setRoom(org.osid.room.Room room) {
        nullarg(room, "room");
        this.room = room;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the tenant resource. 
     *
     *  @return the tenant <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTenantId() {
        return (this.tenant.getId());
    }


    /**
     *  Gets the tenant resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getTenant()
        throws org.osid.OperationFailedException {

        return (this.tenant);
    }


    /**
     *  Sets the tenant.
     *
     *  @param tenant a tenant
     *  @throws org.osid.NullArgumentException
     *          <code>tenant</code> is <code>null</code>
     */

    protected void setTenant(org.osid.resource.Resource tenant) {
        nullarg(tenant, "tenant");
        this.tenant = tenant;
        return;
    }


    /**
     *  Tests if this lease supports the given record
     *  <code>Type</code>.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return <code>true</code> if the leaseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type leaseRecordType) {
        for (org.osid.room.squatting.records.LeaseRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Lease</code>
     *  record <code>Type</code>.
     *
     *  @param  leaseRecordType the lease record type 
     *  @return the lease record 
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(leaseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseRecord getLeaseRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lease. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param leaseRecord the lease record
     *  @param leaseRecordType lease record type
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecord</code> or
     *          <code>leaseRecordTypelease</code> is
     *          <code>null</code>
     */
            
    protected void addLeaseRecord(org.osid.room.squatting.records.LeaseRecord leaseRecord, 
                                  org.osid.type.Type leaseRecordType) {
        
        nullarg(leaseRecord, "lease record");
        addRecordType(leaseRecordType);
        this.records.add(leaseRecord);
        
        return;
    }
}
