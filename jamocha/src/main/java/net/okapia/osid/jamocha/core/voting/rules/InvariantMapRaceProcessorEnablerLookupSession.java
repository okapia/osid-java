//
// InvariantMapRaceProcessorEnablerLookupSession
//
//    Implements a RaceProcessorEnabler lookup service backed by a fixed collection of
//    raceProcessorEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceProcessorEnabler lookup service backed by a fixed
 *  collection of race processor enablers. The race processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractMapRaceProcessorEnablerLookupSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceProcessorEnablerLookupSession</code> with no
     *  race processor enablers.
     *  
     *  @param polls the polls
     *  @throws org.osid.NullArgumnetException {@code polls} is
     *          {@code null}
     */

    public InvariantMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceProcessorEnablerLookupSession</code> with a single
     *  race processor enabler.
     *  
     *  @param polls the polls
     *  @param raceProcessorEnabler a single race processor enabler
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnabler} is <code>null</code>
     */

      public InvariantMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler) {
        this(polls);
        putRaceProcessorEnabler(raceProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceProcessorEnablerLookupSession</code> using an array
     *  of race processor enablers.
     *  
     *  @param polls the polls
     *  @param raceProcessorEnablers an array of race processor enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnablers} is <code>null</code>
     */

      public InvariantMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                               org.osid.voting.rules.RaceProcessorEnabler[] raceProcessorEnablers) {
        this(polls);
        putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRaceProcessorEnablerLookupSession</code> using a
     *  collection of race processor enablers.
     *
     *  @param polls the polls
     *  @param raceProcessorEnablers a collection of race processor enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnablers} is <code>null</code>
     */

      public InvariantMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                               java.util.Collection<? extends org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablers) {
        this(polls);
        putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }
}
