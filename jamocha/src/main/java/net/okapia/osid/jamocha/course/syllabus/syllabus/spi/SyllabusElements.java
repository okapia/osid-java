//
// SyllabusElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SyllabusElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the SyllabusElement Id.
     *
     *  @return the syllabus element Id
     */

    public static org.osid.id.Id getSyllabusEntityId() {
        return (makeEntityId("osid.course.syllabus.Syllabus"));
    }


    /**
     *  Gets the CourseId element Id.
     *
     *  @return the CourseId element Id
     */

    public static org.osid.id.Id getCourseId() {
        return (makeElementId("osid.course.syllabus.syllabus.CourseId"));
    }


    /**
     *  Gets the Course element Id.
     *
     *  @return the Course element Id
     */

    public static org.osid.id.Id getCourse() {
        return (makeElementId("osid.course.syllabus.syllabus.Course"));
    }


    /**
     *  Gets the ModuleId element Id.
     *
     *  @return the ModuleId element Id
     */

    public static org.osid.id.Id getModuleId() {
        return (makeQueryElementId("osid.course.syllabus.syllabus.ModuleId"));
    }


    /**
     *  Gets the Module element Id.
     *
     *  @return the Module element Id
     */

    public static org.osid.id.Id getModule() {
        return (makeQueryElementId("osid.course.syllabus.syllabus.Module"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.syllabus.syllabus.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.syllabus.syllabus.CourseCatalog"));
    }
}
