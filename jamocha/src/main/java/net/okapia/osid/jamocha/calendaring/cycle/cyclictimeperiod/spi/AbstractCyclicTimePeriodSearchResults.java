//
// AbstractCyclicTimePeriodSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCyclicTimePeriodSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.cycle.CyclicTimePeriodSearchResults {

    private org.osid.calendaring.cycle.CyclicTimePeriodList cyclicTimePeriods;
    private final org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCyclicTimePeriodSearchResults.
     *
     *  @param cyclicTimePeriods the result set
     *  @param cyclicTimePeriodQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriods</code>
     *          or <code>cyclicTimePeriodQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCyclicTimePeriodSearchResults(org.osid.calendaring.cycle.CyclicTimePeriodList cyclicTimePeriods,
                                            org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector cyclicTimePeriodQueryInspector) {
        nullarg(cyclicTimePeriods, "cyclic time periods");
        nullarg(cyclicTimePeriodQueryInspector, "cyclic time period query inspectpr");

        this.cyclicTimePeriods = cyclicTimePeriods;
        this.inspector = cyclicTimePeriodQueryInspector;

        return;
    }


    /**
     *  Gets the cyclic time period list resulting from a search.
     *
     *  @return a cyclic time period list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods() {
        if (this.cyclicTimePeriods == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.cycle.CyclicTimePeriodList cyclicTimePeriods = this.cyclicTimePeriods;
        this.cyclicTimePeriods = null;
	return (cyclicTimePeriods);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector getCyclicTimePeriodQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  cyclic time period search record <code> Type. </code> This method must
     *  be used to retrieve a cyclicTimePeriod implementing the requested
     *  record.
     *
     *  @param cyclicTimePeriodSearchRecordType a cyclicTimePeriod search 
     *         record type 
     *  @return the cyclic time period search
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(cyclicTimePeriodSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodSearchResultsRecord getCyclicTimePeriodSearchResultsRecord(org.osid.type.Type cyclicTimePeriodSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.cycle.records.CyclicTimePeriodSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(cyclicTimePeriodSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(cyclicTimePeriodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record cyclic time period search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCyclicTimePeriodRecord(org.osid.calendaring.cycle.records.CyclicTimePeriodSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "cyclic time period record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
