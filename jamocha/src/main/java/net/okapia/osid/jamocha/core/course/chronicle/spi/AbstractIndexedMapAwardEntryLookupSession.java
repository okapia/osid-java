//
// AbstractIndexedMapAwardEntryLookupSession.java
//
//    A simple framework for providing an AwardEntry lookup service
//    backed by a fixed collection of award entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AwardEntry lookup service backed by a
 *  fixed collection of award entries. The award entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some award entries may be compatible
 *  with more types than are indicated through these award entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AwardEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAwardEntryLookupSession
    extends AbstractMapAwardEntryLookupSession
    implements org.osid.course.chronicle.AwardEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.AwardEntry> awardEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.AwardEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.AwardEntry> awardEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.AwardEntry>());


    /**
     *  Makes an <code>AwardEntry</code> available in this session.
     *
     *  @param  awardEntry an award entry
     *  @throws org.osid.NullArgumentException <code>awardEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAwardEntry(org.osid.course.chronicle.AwardEntry awardEntry) {
        super.putAwardEntry(awardEntry);

        this.awardEntriesByGenus.put(awardEntry.getGenusType(), awardEntry);
        
        try (org.osid.type.TypeList types = awardEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.awardEntriesByRecord.put(types.getNextType(), awardEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an award entry from this session.
     *
     *  @param awardEntryId the <code>Id</code> of the award entry
     *  @throws org.osid.NullArgumentException <code>awardEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAwardEntry(org.osid.id.Id awardEntryId) {
        org.osid.course.chronicle.AwardEntry awardEntry;
        try {
            awardEntry = getAwardEntry(awardEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.awardEntriesByGenus.remove(awardEntry.getGenusType());

        try (org.osid.type.TypeList types = awardEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.awardEntriesByRecord.remove(types.getNextType(), awardEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAwardEntry(awardEntryId);
        return;
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  award entry genus <code>Type</code> which does not include
     *  award entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known award entries or an error results. Otherwise,
     *  the returned list may contain only those award entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  awardEntryGenusType an award entry genus type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByGenusType(org.osid.type.Type awardEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.awardentry.ArrayAwardEntryList(this.awardEntriesByGenus.get(awardEntryGenusType)));
    }


    /**
     *  Gets an <code>AwardEntryList</code> containing the given
     *  award entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known award entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  award entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  awardEntryRecordType an award entry record type 
     *  @return the returned <code>awardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByRecordType(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.awardentry.ArrayAwardEntryList(this.awardEntriesByRecord.get(awardEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.awardEntriesByGenus.clear();
        this.awardEntriesByRecord.clear();

        super.close();

        return;
    }
}
