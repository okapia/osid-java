//
// InvariantMapProxyInquiryEnablerLookupSession
//
//    Implements an InquiryEnabler lookup service backed by a fixed
//    collection of inquiryEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an InquiryEnabler lookup service backed by a fixed
 *  collection of inquiry enablers. The inquiry enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractMapInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyInquiryEnablerLookupSession} with no
     *  inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.proxy.Proxy proxy) {
        setInquest(inquest);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyInquiryEnablerLookupSession} with a single
     *  inquiry enabler.
     *
     *  @param inquest the inquest
     *  @param inquiryEnabler an single inquiry enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.rules.InquiryEnabler inquiryEnabler, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiryEnabler(inquiryEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyInquiryEnablerLookupSession} using
     *  an array of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param inquiryEnablers an array of inquiry enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyInquiryEnablerLookupSession} using a
     *  collection of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param inquiryEnablers a collection of inquiry enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiryEnablers(inquiryEnablers);
        return;
    }
}
