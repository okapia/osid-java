//
// AbstractAssessmentOfferedSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssessmentOfferedSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.AssessmentOfferedSearchResults {

    private org.osid.assessment.AssessmentOfferedList assessmentsOffered;
    private final org.osid.assessment.AssessmentOfferedQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.records.AssessmentOfferedSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssessmentOfferedSearchResults.
     *
     *  @param assessmentsOffered the result set
     *  @param assessmentOfferedQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assessmentsOffered</code>
     *          or <code>assessmentOfferedQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssessmentOfferedSearchResults(org.osid.assessment.AssessmentOfferedList assessmentsOffered,
                                            org.osid.assessment.AssessmentOfferedQueryInspector assessmentOfferedQueryInspector) {
        nullarg(assessmentsOffered, "assessments offered");
        nullarg(assessmentOfferedQueryInspector, "assessment offered query inspectpr");

        this.assessmentsOffered = assessmentsOffered;
        this.inspector = assessmentOfferedQueryInspector;

        return;
    }


    /**
     *  Gets the assessment offered list resulting from a search.
     *
     *  @return an assessment offered list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOffered() {
        if (this.assessmentsOffered == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.AssessmentOfferedList assessmentsOffered = this.assessmentsOffered;
        this.assessmentsOffered = null;
	return (assessmentsOffered);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.AssessmentOfferedQueryInspector getAssessmentOfferedQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  assessment offered search record <code> Type. </code> This method must
     *  be used to retrieve an assessmentOffered implementing the requested
     *  record.
     *
     *  @param assessmentOfferedSearchRecordType an assessmentOffered search 
     *         record type 
     *  @return the assessment offered search
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOfferedSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assessmentOfferedSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentOfferedSearchResultsRecord getAssessmentOfferedSearchResultsRecord(org.osid.type.Type assessmentOfferedSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.records.AssessmentOfferedSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assessmentOfferedSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assessmentOfferedSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record assessment offered search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssessmentOfferedRecord(org.osid.assessment.records.AssessmentOfferedSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "assessment offered record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
