//
// AbstractCommunicationManager.java
//
//     An adapter for a CommunicationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.communication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CommunicationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCommunicationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.communication.CommunicationManager>
    implements org.osid.communication.CommunicationManager {


    /**
     *  Constructs a new {@code AbstractAdapterCommunicationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCommunicationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCommunicationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCommunicationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if a communication service is available. 
     *
     *  @return <code> true </code> if communication is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunication() {
        return (getAdapteeManager().supportsCommunication());
    }


    /**
     *  Gets the supported <code> Communique </code> record types. 
     *
     *  @return a list containing the supported <code> Communique </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommuniqueRecordTypes() {
        return (getAdapteeManager().getCommuniqueRecordTypes());
    }


    /**
     *  Tests if the given <code> Communique </code> record type is supported. 
     *
     *  @param  communiqueRecordType a <code> Type </code> indicating a <code> 
     *          Communique </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> communiqueRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommuniqueRecordType(org.osid.type.Type communiqueRecordType) {
        return (getAdapteeManager().supportsCommuniqueRecordType(communiqueRecordType));
    }


    /**
     *  Gets the supported <code> ResponseOption </code> record types. 
     *
     *  @return a list containing the supported <code> ResponseOption </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseOptionRecordTypes() {
        return (getAdapteeManager().getResponseOptionRecordTypes());
    }


    /**
     *  Tests if the given <code> ResponseOption </code> record type is 
     *  supported. 
     *
     *  @param  responseOptionRecordType a <code> Type </code> indicating a 
     *          <code> ResponseOption </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseOptionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseOptionRecordType(org.osid.type.Type responseOptionRecordType) {
        return (getAdapteeManager().supportsResponseOptionRecordType(responseOptionRecordType));
    }


    /**
     *  Gets the supported <code> Response </code> record types. 
     *
     *  @return a list containing the supported <code> Response </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (getAdapteeManager().getResponseRecordTypes());
    }


    /**
     *  Tests if the given <code> Response </code> record type is supported. 
     *
     *  @param  responseRecordType a <code> Type </code> indicating a <code> 
     *          Response </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (getAdapteeManager().supportsResponseRecordType(responseRecordType));
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the communication 
     *  service. 
     *
     *  @param  receiver the communication receiver 
     *  @return a <code> CommunicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommunication() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationSession getCommunicationSession(org.osid.communication.CommunicationReceiver receiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommunicationSession(receiver));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
