//
// AbstractIndexedMapReceiptLookupSession.java
//
//    A simple framework for providing a Receipt lookup service
//    backed by a fixed collection of receipts with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Receipt lookup service backed by a
 *  fixed collection of receipts. The receipts are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some receipts may be compatible
 *  with more types than are indicated through these receipt
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Receipts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapReceiptLookupSession
    extends AbstractMapReceiptLookupSession
    implements org.osid.messaging.ReceiptLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.messaging.Receipt> receiptsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Receipt>());
    private final MultiMap<org.osid.type.Type, org.osid.messaging.Receipt> receiptsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.messaging.Receipt>());


    /**
     *  Makes a <code>Receipt</code> available in this session.
     *
     *  @param  receipt a receipt
     *  @throws org.osid.NullArgumentException <code>receipt<code> is
     *          <code>null</code>
     */

    @Override
    protected void putReceipt(org.osid.messaging.Receipt receipt) {
        super.putReceipt(receipt);

        this.receiptsByGenus.put(receipt.getGenusType(), receipt);
        
        try (org.osid.type.TypeList types = receipt.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.receiptsByRecord.put(types.getNextType(), receipt);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a receipt from this session.
     *
     *  @param receiptId the <code>Id</code> of the receipt
     *  @throws org.osid.NullArgumentException <code>receiptId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeReceipt(org.osid.id.Id receiptId) {
        org.osid.messaging.Receipt receipt;
        try {
            receipt = getReceipt(receiptId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.receiptsByGenus.remove(receipt.getGenusType());

        try (org.osid.type.TypeList types = receipt.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.receiptsByRecord.remove(types.getNextType(), receipt);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeReceipt(receiptId);
        return;
    }


    /**
     *  Gets a <code>ReceiptList</code> corresponding to the given
     *  receipt genus <code>Type</code> which does not include
     *  receipts of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known receipts or an error results. Otherwise,
     *  the returned list may contain only those receipts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned <code>Receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.receipt.ArrayReceiptList(this.receiptsByGenus.get(receiptGenusType)));
    }


    /**
     *  Gets a <code>ReceiptList</code> containing the given
     *  receipt record <code>Type</code>. In plenary mode, the
     *  returned list contains all known receipts or an error
     *  results. Otherwise, the returned list may contain only those
     *  receipts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return the returned <code>receipt</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByRecordType(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.receipt.ArrayReceiptList(this.receiptsByRecord.get(receiptRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.receiptsByGenus.clear();
        this.receiptsByRecord.clear();

        super.close();

        return;
    }
}
