//
// AbstractOfferingRulesProxyManager.java
//
//     An adapter for a OfferingRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OfferingRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOfferingRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.offering.rules.OfferingRulesProxyManager>
    implements org.osid.offering.rules.OfferingRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOfferingRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOfferingRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOfferingRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerLookup() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerLookup());
    }


    /**
     *  Tests if querying canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerQuery() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerQuery());
    }


    /**
     *  Tests if searching canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSearch() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerSearch());
    }


    /**
     *  Tests if a canonical unit enabler administrative service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerAdmin() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerAdmin());
    }


    /**
     *  Tests if a canonical unit enabler notification service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerNotification() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerNotification());
    }


    /**
     *  Tests if a canonical unit enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit enabler catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerCatalogue());
    }


    /**
     *  Tests if a canonical unit enabler catalogue service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerCatalogueAssignment() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerCatalogueAssignment());
    }


    /**
     *  Tests if a canonical unit enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit enabler catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSmartCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitEnablerSmartCatalogue());
    }


    /**
     *  Tests if looking up canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorLookup() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorLookup());
    }


    /**
     *  Tests if querying canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorQuery() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorQuery());
    }


    /**
     *  Tests if searching canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSearch() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorSearch());
    }


    /**
     *  Tests if a canonical unit processor administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorAdmin() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorAdmin());
    }


    /**
     *  Tests if a canonical unit processor notification service is supported. 
     *
     *  @return <code> true </code> if canonical unit processor notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorNotification() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorNotification());
    }


    /**
     *  Tests if a canonical unit processor catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit processor catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorCatalogue());
    }


    /**
     *  Tests if a canonical unit processor catalogue service is supported. 
     *
     *  @return <code> true </code> if canonical unit processor catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorCatalogueAssignment() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorCatalogueAssignment());
    }


    /**
     *  Tests if a canonical unit processor catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit processor catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSmartCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorSmartCatalogue());
    }


    /**
     *  Tests if a canonical unit processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRuleLookup() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorRuleLookup());
    }


    /**
     *  Tests if a canonical unit processor rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRuleApplication() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorRuleApplication());
    }


    /**
     *  Tests if looking up canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler lookup 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerLookup() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerLookup());
    }


    /**
     *  Tests if querying canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler query 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerQuery() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerQuery());
    }


    /**
     *  Tests if searching canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler search 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSearch() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerSearch());
    }


    /**
     *  Tests if a canonical unit processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerAdmin());
    }


    /**
     *  Tests if a canonical unit processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerNotification() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerNotification());
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor enabler 
     *          catalogue lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerCatalogue());
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          catalogue assignment service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerCatalogueAssignment() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerCatalogueAssignment());
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor enabler 
     *          catalogue service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSmartCatalogue() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerSmartCatalogue());
    }


    /**
     *  Tests if a canonical unit processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a canonical unit processor enabler rule application service 
     *  is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerRuleApplication());
    }


    /**
     *  Tests if looking up offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerLookup() {
        return (getAdapteeManager().supportsOfferingConstrainerLookup());
    }


    /**
     *  Tests if querying offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerQuery() {
        return (getAdapteeManager().supportsOfferingConstrainerQuery());
    }


    /**
     *  Tests if searching offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSearch() {
        return (getAdapteeManager().supportsOfferingConstrainerSearch());
    }


    /**
     *  Tests if an offering constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if offering constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerAdmin() {
        return (getAdapteeManager().supportsOfferingConstrainerAdmin());
    }


    /**
     *  Tests if an offering constrainer notification service is supported. 
     *
     *  @return <code> true </code> if offering constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerNotification() {
        return (getAdapteeManager().supportsOfferingConstrainerNotification());
    }


    /**
     *  Tests if an offering constrainer catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerCatalogue() {
        return (getAdapteeManager().supportsOfferingConstrainerCatalogue());
    }


    /**
     *  Tests if an offering constrainer catalogue service is supported. 
     *
     *  @return <code> true </code> if offering constrainer catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerCatalogueAssignment() {
        return (getAdapteeManager().supportsOfferingConstrainerCatalogueAssignment());
    }


    /**
     *  Tests if an offering constrainer catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSmartCatalogue() {
        return (getAdapteeManager().supportsOfferingConstrainerSmartCatalogue());
    }


    /**
     *  Tests if an offering constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if an offering constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRuleLookup() {
        return (getAdapteeManager().supportsOfferingConstrainerRuleLookup());
    }


    /**
     *  Tests if an offering constrainer rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRuleApplication() {
        return (getAdapteeManager().supportsOfferingConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerSearch());
    }


    /**
     *  Tests if an offering constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerAdmin());
    }


    /**
     *  Tests if an offering constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerNotification());
    }


    /**
     *  Tests if an offering constrainer enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler 
     *          catalogue lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerCatalogue() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerCatalogue());
    }


    /**
     *  Tests if an offering constrainer enabler catalogue service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerCatalogueAssignment() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerCatalogueAssignment());
    }


    /**
     *  Tests if an offering constrainer enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler 
     *          catalogue service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSmartCatalogue() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerSmartCatalogue());
    }


    /**
     *  Tests if an offering constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if an offering constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> CanonicalUnitEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitEnablerRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitEnabler </code> record type is 
     *  supported. 
     *
     *  @param  canonicalUnitEnablerRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerRecordType(org.osid.type.Type canonicalUnitEnablerRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitEnablerRecordType(canonicalUnitEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CanonicalUnitEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  canonicalUnitEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSearchRecordType(org.osid.type.Type canonicalUnitEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitEnablerSearchRecordType(canonicalUnitEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessor </code> record type 
     *  is supported. 
     *
     *  @param  canonicalUnitProcessorRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitProcessorRecordType(canonicalUnitProcessorRecordType));
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorSearchRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessor </code> search record 
     *  type is supported. 
     *
     *  @param  canonicalUnitProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessor </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSearchRecordType(org.osid.type.Type canonicalUnitProcessorSearchRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitProcessorSearchRecordType(canonicalUnitProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          CanonicalUnitProcessorEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessorEnabler </code> record 
     *  type is supported. 
     *
     *  @param  canonicalUnitProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessorEnabler </code> 
     *          record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerRecordType(canonicalUnitProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          CanonicalUnitProcessorEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getCanonicalUnitProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  canonicalUnitProcessorEnablerSearchRecordType a <code> Type 
     *          </code> indicating a <code> CanonicalUnitProcessorEnabler 
     *          </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerSearchRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSearchRecordType(org.osid.type.Type canonicalUnitProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsCanonicalUnitProcessorEnablerSearchRecordType(canonicalUnitProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> OfferingConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> OfferingConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerRecordTypes() {
        return (getAdapteeManager().getOfferingConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> OfferingConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  offeringConstrainerRecordType a <code> Type </code> indicating 
     *          an <code> OfferingConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        return (getAdapteeManager().supportsOfferingConstrainerRecordType(offeringConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> OfferingConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> OfferingConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getOfferingConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> OfferingConstrainer </code> search record 
     *  type is supported. 
     *
     *  @param  offeringConstrainerSearchRecordType a <code> Type </code> 
     *          indicating an <code> OfferingConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSearchRecordType(org.osid.type.Type offeringConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsOfferingConstrainerSearchRecordType(offeringConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> OfferingConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          OfferingConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getOfferingConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> OfferingConstrainerEnabler </code> record 
     *  type is supported. 
     *
     *  @param  offeringConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating an <code> OfferingConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerRecordType(offeringConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> OfferingConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          OfferingConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getOfferingConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> OfferingConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  offeringConstrainerEnablerSearchRecordType a <code> Type 
     *          </code> indicating an <code> OfferingConstrainerEnabler 
     *          </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSearchRecordType(org.osid.type.Type offeringConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsOfferingConstrainerEnablerSearchRecordType(offeringConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerQuerySessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerSearchSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerAdminSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerNotificationSession(canonicalUnitEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver, 
                                                                                                                                  org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerNotificationSessionForCatalogue(canonicalUnitEnablerReceiver, catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  enabler/catalogue mappings for canonical unit enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueSession getCanonicalUnitEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerCatalogueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueAssignmentSession getCanonicalUnitEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerCatalogueAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit enabler 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSmartCatalogueSession getCanonicalUnitEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerSmartCatalogueSession(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerRuleLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitEnablerRuleApplicationSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorQuerySessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorSearchSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorAdminSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service. 
     *
     *  @param  canonicalUnitProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessorReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorNotificationSession(canonicalUnitProcessorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessoReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessoReceiver, 
                                                                                                                                      org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorNotificationSessionForCatalogue(canonicalUnitProcessoReceiver, catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  processor/catalogue mappings for canonical unit processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueSession getCanonicalUnitProcessorCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorCatalogueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueAssignmentSession getCanonicalUnitProcessorCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorCatalogueAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSmartCatalogueSession getCanonicalUnitProcessorSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorSmartCatalogueSession(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the catalogue. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorRuleLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor assignment service to apply to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor assignment service for the given catalogue to apply to 
     *  catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorRuleApplicationSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerQuerySessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerSearchSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerAdminSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service. 
     *
     *  @param  canonicalUnitProcessoEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessoEnablerReceiver, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerNotificationSession(canonicalUnitProcessoEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessoEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoEnablerReceiver, catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessoEnablerReceiver, 
                                                                                                                                                    org.osid.id.Id catalogueId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue(canonicalUnitProcessoEnablerReceiver, catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit processor 
     *  enabler/catalogue mappings for canonical unit processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueSession getCanonicalUnitProcessorEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerCatalogueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> 
     *          CanonicalUnitProcessorEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueAssignmentSession getCanonicalUnitProcessorEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerCatalogueAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSmartCatalogue() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSmartCatalogueSession getCanonicalUnitProcessorEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerSmartCatalogueSession(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerQuerySessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerSearchSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerAdminSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSession(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerNotificationSession(offeringConstrainerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver, 
                                                                                                                                org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerNotificationSessionForCatalogue(offeringConstrainerReceiver, catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering 
     *  constrainer/catalogue mappings for offering constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueSession getOfferingConstrainerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerCatalogueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueAssignmentSession getOfferingConstrainerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerCatalogueAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSmartCatalogueSession getOfferingConstrainerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerSmartCatalogueSession(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  the catalogue. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerRuleLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service to apply to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service for the given catalogue to apply to 
     *  catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerRuleApplicationSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerQuerySessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerSearchSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerAdminSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSession(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerNotificationSession(offeringConstrainerEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver, catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id catalogueId, 
                                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerNotificationSessionForCatalogue(offeringConstrainerEnablerReceiver, catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering constrainer 
     *  enabler/catalogue mappings for offering constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueSession getOfferingConstrainerEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerCatalogueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueAssignmentSession getOfferingConstrainerEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerCatalogueAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSmartCatalogueSession getOfferingConstrainerEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerSmartCatalogueSession(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerRuleLookupSessionForCatalogue(catalogueId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue(catalogueId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
