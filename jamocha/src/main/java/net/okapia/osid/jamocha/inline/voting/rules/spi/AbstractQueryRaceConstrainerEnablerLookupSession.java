//
// AbstractQueryRaceConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps a RaceConstrainerEnablerLookupSession to
//    a RaceConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RaceConstrainerEnablerLookupSession to
 *  a RaceConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryRaceConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractRaceConstrainerEnablerLookupSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.voting.rules.RaceConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new
     *  AbstractQueryRaceConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying race constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRaceConstrainerEnablerLookupSession(org.osid.voting.rules.RaceConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "race constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform
     *  <code>RaceConstrainerEnabler</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceConstrainerEnablers() {
        return (this.session.canSearchRaceConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race constrainer enablers in pollses which
     *  are children of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race constrainer enablers are returned by methods
     *  in this session.
     */
     
    @OSID @Override
    public void useActiveRaceConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive race constrainer enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RaceConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceConstrainerEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RaceConstrainerEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param  raceConstrainerEnablerId <code>Id</code> of the
     *          <code>RaceConstrainerEnabler</code>
     *  @return the race constrainer enabler
     *  @throws org.osid.NotFoundException <code>raceConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnabler getRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchId(raceConstrainerEnablerId, true);
        org.osid.voting.rules.RaceConstrainerEnablerList raceConstrainerEnablers = this.session.getRaceConstrainerEnablersByQuery(query);
        if (raceConstrainerEnablers.hasNext()) {
            return (raceConstrainerEnablers.getNextRaceConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(raceConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  raceConstrainerEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RaceConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param raceConstrainerEnablerIds the list of <code>Ids</code>
     *         to retrieve
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByIds(org.osid.id.IdList raceConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = raceConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding
     *  to the given race constrainer enabler genus <code>Type</code>
     *  which does not include race constrainer enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those race constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param raceConstrainerEnablerGenusType a
     *         raceConstrainerEnabler genus type
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByGenusType(org.osid.type.Type raceConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(raceConstrainerEnablerGenusType, true);
        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding
     *  to the given race constrainer enabler genus <code>Type</code>
     *  and include any additional race constrainer enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those race constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param  raceConstrainerEnablerGenusType a raceConstrainerEnabler genus type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByParentGenusType(org.osid.type.Type raceConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(raceConstrainerEnablerGenusType, true);
        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> containing the
     *  given race constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those race constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param  raceConstrainerEnablerRecordType a raceConstrainerEnabler record type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByRecordType(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(raceConstrainerEnablerRecordType, true);
        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known race
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those race constrainer enablers
     *  that are accessible through this session.
     *  
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RaceConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>RaceConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known race
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those race constrainer enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  race constrainer enablers are returned.
     *
     *  @return a list of <code>RaceConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.RaceConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRaceConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.rules.RaceConstrainerEnablerQuery getQuery() {
        org.osid.voting.rules.RaceConstrainerEnablerQuery query = this.session.getRaceConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
