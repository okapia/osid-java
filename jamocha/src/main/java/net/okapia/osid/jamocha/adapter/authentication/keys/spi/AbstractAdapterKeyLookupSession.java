//
// AbstractAdapterKeyLookupSession.java
//
//    A Key lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.keys.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Key lookup session adapter.
 */

public abstract class AbstractAdapterKeyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authentication.keys.KeyLookupSession {

    private final org.osid.authentication.keys.KeyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterKeyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterKeyLookupSession(org.osid.authentication.keys.KeyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Agency/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Agency Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the {@code Agency} associated with this session.
     *
     *  @return the {@code Agency} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform {@code Key} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupKeys() {
        return (this.session.canLookupKeys());
    }


    /**
     *  A complete view of the {@code Key} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeKeyView() {
        this.session.useComparativeKeyView();
        return;
    }


    /**
     *  A complete view of the {@code Key} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryKeyView() {
        this.session.usePlenaryKeyView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include keys in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
     
    /**
     *  Gets the {@code Key} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Key} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Key} and
     *  retained for compatibility.
     *
     *  @param keyId {@code Id} of the {@code Key}
     *  @return the key
     *  @throws org.osid.NotFoundException {@code keyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code keyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKey(org.osid.id.Id keyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKey(keyId));
    }


    /**
     *  Gets a {@code KeyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  keys specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Keys} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Key} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code keyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByIds(org.osid.id.IdList keyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeysByIds(keyIds));
    }


    /**
     *  Gets a {@code KeyList} corresponding to the given
     *  key genus {@code Type} which does not include
     *  keys of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned {@code Key} list
     *  @throws org.osid.NullArgumentException
     *          {@code keyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeysByGenusType(keyGenusType));
    }


    /**
     *  Gets a {@code KeyList} corresponding to the given
     *  key genus {@code Type} and include any additional
     *  keys with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyGenusType a key genus type 
     *  @return the returned {@code Key} list
     *  @throws org.osid.NullArgumentException
     *          {@code keyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByParentGenusType(org.osid.type.Type keyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeysByParentGenusType(keyGenusType));
    }


    /**
     *  Gets a {@code KeyList} containing the given
     *  key record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  keyRecordType a key record type 
     *  @return the returned {@code Key} list
     *  @throws org.osid.NullArgumentException
     *          {@code keyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeysByRecordType(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeysByRecordType(keyRecordType));
    }


    /**
     *  Gets the agent key. 
     *
     *  @param  agentId the {@code Id} of the {@code Agent} 
     *  @return the key 
     *  @throws org.osid.NotFoundException {@code agentId} is not
     *          found or no key exists
     *  @throws org.osid.NullArgumentException {@code agentId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.keys.Key getKeyForAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeyForAgent(agentId));
    }


    /**
     *  Gets all {@code Keys}. 
     *
     *  In plenary mode, the returned list contains all known
     *  keys or an error results. Otherwise, the returned list
     *  may contain only those keys that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Keys} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeys()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getKeys());
    }
}
