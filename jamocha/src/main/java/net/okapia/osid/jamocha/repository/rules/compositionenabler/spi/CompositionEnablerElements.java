//
// CompositionEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.rules.compositionenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CompositionEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the CompositionEnablerElement Id.
     *
     *  @return the composition enabler element Id
     */

    public static org.osid.id.Id getCompositionEnablerEntityId() {
        return (makeEntityId("osid.repository.rules.CompositionEnabler"));
    }


    /**
     *  Gets the RuledCompositionId element Id.
     *
     *  @return the RuledCompositionId element Id
     */

    public static org.osid.id.Id getRuledCompositionId() {
        return (makeQueryElementId("osid.repository.rules.compositionenabler.RuledCompositionId"));
    }


    /**
     *  Gets the RuledComposition element Id.
     *
     *  @return the RuledComposition element Id
     */

    public static org.osid.id.Id getRuledComposition() {
        return (makeQueryElementId("osid.repository.rules.compositionenabler.RuledComposition"));
    }


    /**
     *  Gets the RepositoryId element Id.
     *
     *  @return the RepositoryId element Id
     */

    public static org.osid.id.Id getRepositoryId() {
        return (makeQueryElementId("osid.repository.rules.compositionenabler.RepositoryId"));
    }


    /**
     *  Gets the Repository element Id.
     *
     *  @return the Repository element Id
     */

    public static org.osid.id.Id getRepository() {
        return (makeQueryElementId("osid.repository.rules.compositionenabler.Repository"));
    }
}
