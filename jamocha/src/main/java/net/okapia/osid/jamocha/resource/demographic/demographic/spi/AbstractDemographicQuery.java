//
// AbstractDemographicQuery.java
//
//     A template for making a Demographic Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for demographics.
 */

public abstract class AbstractDemographicQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.resource.demographic.DemographicQuery {

    private final java.util.Collection<org.osid.resource.demographic.records.DemographicQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches demographics including the given demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedDemographicId(org.osid.id.Id demographicId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a demographic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          demographic, <code> false </code> to match demographics 
     *          including no demographics 
     */

    @OSID @Override
    public void matchAnyIncludedDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedDemographicTerms() {
        return;
    }


    /**
     *  Matches demographics including the given intersecting demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedIntersectingDemographicId(org.osid.id.Id demographicId, 
                                                       boolean match) {
        return;
    }


    /**
     *  Clears the intersecting demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedIntersectingDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedIntersectingDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedIntersectingDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedIntersectingDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any intersecting demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          intersecting demographic, <code> false </code> to match 
     *          demographics including no intersecting demographics 
     */

    @OSID @Override
    public void matchAnyIncludedIntersectingDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the intersecting demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedIntersectingDemographicTerms() {
        return;
    }


    /**
     *  Matches demographics including the given exclusive demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedExclusiveDemographicId(org.osid.id.Id demographicId, 
                                                    boolean match) {
        return;
    }


    /**
     *  Clears the exclusive demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedExclusiveDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedExclusiveDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exclusive demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getIncludedExclusiveDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedExclusiveDemographicQuery() is false");
    }


    /**
     *  Matches demographics including any exclusive demographic. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          exclusive demographic, <code> false </code> to match 
     *          demographics including no exclusive demographics 
     */

    @OSID @Override
    public void matchAnyIncludedExclusiveDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the demographic query terms. 
     */

    @OSID @Override
    public void clearIncludedExclusiveDemographicTerms() {
        return;
    }


    /**
     *  Matches demographics excluding the given a demographic. 
     *
     *  @param  demographicId the demographic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> demographicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExcludedDemographicId(org.osid.id.Id demographicId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the excluded demographic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExcludedDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DemographicQuery </code> is available to match 
     *  demographics with an excluded demographic. 
     *
     *  @return <code> true </code> if a demographic query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExcludedDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for an excluded demographic. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the demographic query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExcludedDemographicQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuery getExcludedDemographicQuery() {
        throw new org.osid.UnimplementedException("supportsExcludedDemographicQuery() is false");
    }


    /**
     *  Matches demographics excluding any demographic. 
     *
     *  @param  match <code> true </code> for demographics with any excluded 
     *          demographic, <code> false </code> to match demographics with 
     *          no excluded demographics 
     */

    @OSID @Override
    public void matchAnyExcludedDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the excluded demographic query terms. 
     */

    @OSID @Override
    public void clearExcludedDemographicTerms() {
        return;
    }


    /**
     *  Matches demographics including the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIncludedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the included resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIncludedResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIncludedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIncludedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getIncludedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsIncludedResourceQuery() is false");
    }


    /**
     *  Matches demographics including any resource. 
     *
     *  @param  match <code> true </code> for demographics that include any 
     *          resource, <code> false </code> to match demographics including 
     *          no resources 
     */

    @OSID @Override
    public void matchAnyIncludedResource(boolean match) {
        return;
    }


    /**
     *  Clears the included resource query terms. 
     */

    @OSID @Override
    public void clearIncludedResourceTerms() {
        return;
    }


    /**
     *  Matches demographics excluding the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExcludedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the excluded resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExcludedResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available to match 
     *  demographics with an excluded resource. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExcludedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for an excluded resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExcludedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getExcludedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsExcludedResourceQuery() is false");
    }


    /**
     *  Matches demographics excluding any resource. 
     *
     *  @param  match <code> true </code> for demographics that exclude any 
     *          resource, <code> false </code> to match demographics excluding 
     *          no resources 
     */

    @OSID @Override
    public void matchAnyExcludedResource(boolean match) {
        return;
    }


    /**
     *  Clears the excluded resource query terms. 
     */

    @OSID @Override
    public void clearExcludedResourceTerms() {
        return;
    }


    /**
     *  Matches demographics resulting in the given resource. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultingResourceId(org.osid.id.Id resourceId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the resulting resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResultingResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available to match 
     *  demographics resulting in a resource. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultingResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resulting resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultingResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResultingResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResultingResourceQuery() is false");
    }


    /**
     *  Matches demographics that have any resulting resource. 
     *
     *  @param  match <code> true </code> for demographics that have any 
     *          resulting resource, <code> false </code> to match demographics 
     *          with no resulting resources 
     */

    @OSID @Override
    public void matchAnyResultingResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResultingResourceTerms() {
        return;
    }


    /**
     *  Matches mapped to the bin. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        return;
    }


    /**
     *  Clears the bin <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin query terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given demographic query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a demographic implementing the requested record.
     *
     *  @param demographicRecordType a demographic record type
     *  @return the demographic query record
     *  @throws org.osid.NullArgumentException
     *          <code>demographicRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(demographicRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicQueryRecord getDemographicQueryRecord(org.osid.type.Type demographicRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.demographic.records.DemographicQueryRecord record : this.records) {
            if (record.implementsRecordType(demographicRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(demographicRecordType + " is not supported");
    }


    /**
     *  Adds a record to this demographic query. 
     *
     *  @param demographicQueryRecord demographic query record
     *  @param demographicRecordType demographic record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDemographicQueryRecord(org.osid.resource.demographic.records.DemographicQueryRecord demographicQueryRecord, 
                                          org.osid.type.Type demographicRecordType) {

        addRecordType(demographicRecordType);
        nullarg(demographicQueryRecord, "demographic query record");
        this.records.add(demographicQueryRecord);        
        return;
    }
}
