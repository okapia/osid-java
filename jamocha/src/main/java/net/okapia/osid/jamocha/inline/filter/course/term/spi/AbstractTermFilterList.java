//
// AbstractTermList
//
//     Implements a filter for a TermList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a TermList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedTermList
 *  to improve performance.
 */

public abstract class AbstractTermFilterList
    extends net.okapia.osid.jamocha.course.term.spi.AbstractTermList
    implements org.osid.course.TermList,
               net.okapia.osid.jamocha.inline.filter.course.term.TermFilter {

    private org.osid.course.Term term;
    private final org.osid.course.TermList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractTermFilterList</code>.
     *
     *  @param termList a <code>TermList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>termList</code> is <code>null</code>
     */

    protected AbstractTermFilterList(org.osid.course.TermList termList) {
        nullarg(termList, "term list");
        this.list = termList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.term == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Term </code> in this list. 
     *
     *  @return the next <code> Term </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Term </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getNextTerm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.course.Term term = this.term;
            this.term = null;
            return (term);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in term list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.term = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Terms.
     *
     *  @param term the term to filter
     *  @return <code>true</code> if the term passes the filter,
     *          <code>false</code> if the term should be filtered
     */

    public abstract boolean pass(org.osid.course.Term term);


    protected void prime() {
        if (this.term != null) {
            return;
        }

        org.osid.course.Term term = null;

        while (this.list.hasNext()) {
            try {
                term = this.list.getNextTerm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(term)) {
                this.term = term;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
