//
// RecipeElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RecipeElements
    extends net.okapia.osid.jamocha.spi.SourceableOsidObjectElements {


    /**
     *  Gets the RecipeElement Id.
     *
     *  @return the recipe element Id
     */

    public static org.osid.id.Id getRecipeEntityId() {
        return (makeEntityId("osid.recipe.Recipe"));
    }


    /**
     *  Gets the TotalEstimatedDuration element Id.
     *
     *  @return the TotalEstimatedDuration element Id
     */

    public static org.osid.id.Id getTotalEstimatedDuration() {
        return (makeElementId("osid.recipe.recipe.TotalEstimatedDuration"));
    }


    /**
     *  Gets the AssetIds element Id.
     *
     *  @return the AssetIds element Id
     */

    public static org.osid.id.Id getAssetIds() {
        return (makeElementId("osid.recipe.recipe.AssetIds"));
    }


    /**
     *  Gets the Assets element Id.
     *
     *  @return the Assets element Id
     */

    public static org.osid.id.Id getAssets() {
        return (makeElementId("osid.recipe.recipe.Assets"));
    }


    /**
     *  Gets the DirectionId element Id.
     *
     *  @return the DirectionId element Id
     */

    public static org.osid.id.Id getDirectionId() {
        return (makeQueryElementId("osid.recipe.recipe.DirectionId"));
    }


    /**
     *  Gets the Direction element Id.
     *
     *  @return the Direction element Id
     */

    public static org.osid.id.Id getDirection() {
        return (makeQueryElementId("osid.recipe.recipe.Direction"));
    }


    /**
     *  Gets the TotalDuration element Id.
     *
     *  @return the TotalDuration element Id
     */

    public static org.osid.id.Id getTotalDuration() {
        return (makeQueryElementId("osid.recipe.recipe.TotalDuration"));
    }


    /**
     *  Gets the CookbookId element Id.
     *
     *  @return the CookbookId element Id
     */

    public static org.osid.id.Id getCookbookId() {
        return (makeQueryElementId("osid.recipe.recipe.CookbookId"));
    }


    /**
     *  Gets the Cookbook element Id.
     *
     *  @return the Cookbook element Id
     */

    public static org.osid.id.Id getCookbook() {
        return (makeQueryElementId("osid.recipe.recipe.Cookbook"));
    }
}
