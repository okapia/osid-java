//
// AbstractAssemblyOsidCompendiumQuery.java
//
//     An OsidCompendiumQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OsidCompendiumQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidCompendiumQuery
    extends AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.OsidCompendiumQuery,
               org.osid.OsidCompendiumQueryInspector,
               org.osid.OsidCompendiumSearchOrder {

    
    /** 
     *  Constructs a new
     *  <code>AbstractAssemblyOsidCompendiumQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidCompendiumQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }


    /**
     *  Matches compendiums whose start date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getStartDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches compendiums with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        getAssembler().addIdWildcardTerm(getStartDateColumn(), match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        getAssembler().clearTerms(getStartDateColumn());        
        return;
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getStartDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the start date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStartDateColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the start date field.
     *
     *  @return the column name
     */

    protected String getStartDateColumn() {
        return ("start_date");        
    }    


    /**
     *  Matches compendiums whose end date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param match <code> true </code> if a positive match, <code>
     *          false </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is less than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, boolean match) {

        getAssembler().addDateTimeRangeTerm(getEndDateColumn(), start, end, match);
        return;
    }


    /**
     *  Matches compendiums with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> false 
     *          </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        getAssembler().addIdWildcardTerm(getEndDateColumn(), match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        getAssembler().clearTerms(getEndDateColumn()); 
        return;
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getEndDateColumn()));
    }


    
    /**
     *  Specifies a preference for ordering the result set by the end date. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEndDateColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the end date field.
     *
     *  @return the column name
     */

    protected String getEndDateColumn() {
        return ("end_date");
    }    


    /**
     *  Matches interpolated compendiums.
     *
     *  @param match <code> true </code> to match interpolated objects,
     *          <code> false </code> to match ininterpolated objects
     */

    @OSID @Override
    public void matchInterpolated(boolean match) {
        getAssembler().addBooleanTerm(getInterpolatedColumn(), match);
        return;
    }


    /**
     *  Clears all match interpolated terms. 
     */

    @OSID @Override
    public void clearInterpolatedTerms() {
        getAssembler().clearTerms(getInterpolatedColumn());
        return;
    }


    /**
     *  Gets the interpolated query terms. 
     *
     *  @return the interpolated terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getInterpolatedTerms() {
        return (getAssembler().getBooleanTerms(getInterpolatedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  interpolated flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInterpolated(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInterpolatedColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the interpolated field.
     *
     *  @return the column name
     */

    protected String getInterpolatedColumn() {
        return ("interpolated");
    }


    /**
     *  Matches extrapolated compendiums.
     *
     *  @param match <code> true </code> to match extrapolated objects,
     *          <code> false </code> to match inextrapolated objects
     */

    @OSID @Override
    public void matchExtrapolated(boolean match) {
        getAssembler().addBooleanTerm(getExtrapolatedColumn(), match);
        return;
    }


    /**
     *  Clears all match extrapolated terms. 
     */

    @OSID @Override
    public void clearExtrapolatedTerms() {
        getAssembler().clearTerms(getExtrapolatedColumn());
        return;
    }


    /**
     *  Gets the extrapolated query terms. 
     *
     *  @return the extrapolated terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getExtrapolatedTerms() {
        return (getAssembler().getBooleanTerms(getExtrapolatedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  extrapolated flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExtrapolated(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getExtrapolatedColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the extrapolated field.
     *
     *  @return the column name
     */

    protected String getExtrapolatedColumn() {
        return ("extrapolated");
    }
}
