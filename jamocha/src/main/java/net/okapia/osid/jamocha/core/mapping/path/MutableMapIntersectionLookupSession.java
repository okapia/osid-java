//
// MutableMapIntersectionLookupSession
//
//    Implements an Intersection lookup service backed by a collection of
//    intersections that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements an Intersection lookup service backed by a collection of
 *  intersections. The intersections are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of intersections can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapIntersectionLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapIntersectionLookupSession
    implements org.osid.mapping.path.IntersectionLookupSession {


    /**
     *  Constructs a new {@code MutableMapIntersectionLookupSession}
     *  with no intersections.
     *
     *  @param map the map
     *  @throws org.osid.NullArgumentException {@code map} is
     *          {@code null}
     */

      public MutableMapIntersectionLookupSession(org.osid.mapping.Map map) {
        setMap(map);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapIntersectionLookupSession} with a
     *  single intersection.
     *
     *  @param map the map  
     *  @param intersection an intersection
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code intersection} is {@code null}
     */

    public MutableMapIntersectionLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.path.Intersection intersection) {
        this(map);
        putIntersection(intersection);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapIntersectionLookupSession}
     *  using an array of intersections.
     *
     *  @param map the map
     *  @param intersections an array of intersections
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code intersections} is {@code null}
     */

    public MutableMapIntersectionLookupSession(org.osid.mapping.Map map,
                                           org.osid.mapping.path.Intersection[] intersections) {
        this(map);
        putIntersections(intersections);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapIntersectionLookupSession}
     *  using a collection of intersections.
     *
     *  @param map the map
     *  @param intersections a collection of intersections
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code intersections} is {@code null}
     */

    public MutableMapIntersectionLookupSession(org.osid.mapping.Map map,
                                           java.util.Collection<? extends org.osid.mapping.path.Intersection> intersections) {

        this(map);
        putIntersections(intersections);
        return;
    }

    
    /**
     *  Makes an {@code Intersection} available in this session.
     *
     *  @param intersection an intersection
     *  @throws org.osid.NullArgumentException {@code intersection{@code  is
     *          {@code null}
     */

    @Override
    public void putIntersection(org.osid.mapping.path.Intersection intersection) {
        super.putIntersection(intersection);
        return;
    }


    /**
     *  Makes an array of intersections available in this session.
     *
     *  @param intersections an array of intersections
     *  @throws org.osid.NullArgumentException {@code intersections{@code 
     *          is {@code null}
     */

    @Override
    public void putIntersections(org.osid.mapping.path.Intersection[] intersections) {
        super.putIntersections(intersections);
        return;
    }


    /**
     *  Makes collection of intersections available in this session.
     *
     *  @param intersections a collection of intersections
     *  @throws org.osid.NullArgumentException {@code intersections{@code  is
     *          {@code null}
     */

    @Override
    public void putIntersections(java.util.Collection<? extends org.osid.mapping.path.Intersection> intersections) {
        super.putIntersections(intersections);
        return;
    }


    /**
     *  Removes an Intersection from this session.
     *
     *  @param intersectionId the {@code Id} of the intersection
     *  @throws org.osid.NullArgumentException {@code intersectionId{@code 
     *          is {@code null}
     */

    @Override
    public void removeIntersection(org.osid.id.Id intersectionId) {
        super.removeIntersection(intersectionId);
        return;
    }    
}
