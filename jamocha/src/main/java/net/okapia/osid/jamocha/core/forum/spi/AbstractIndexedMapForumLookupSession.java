//
// AbstractIndexedMapForumLookupSession.java
//
//    A simple framework for providing a Forum lookup service
//    backed by a fixed collection of forums with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Forum lookup service backed by a
 *  fixed collection of forums. The forums are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some forums may be compatible
 *  with more types than are indicated through these forum
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Forums</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapForumLookupSession
    extends AbstractMapForumLookupSession
    implements org.osid.forum.ForumLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.forum.Forum> forumsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.forum.Forum>());
    private final MultiMap<org.osid.type.Type, org.osid.forum.Forum> forumsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.forum.Forum>());


    /**
     *  Makes a <code>Forum</code> available in this session.
     *
     *  @param  forum a forum
     *  @throws org.osid.NullArgumentException <code>forum<code> is
     *          <code>null</code>
     */

    @Override
    protected void putForum(org.osid.forum.Forum forum) {
        super.putForum(forum);

        this.forumsByGenus.put(forum.getGenusType(), forum);
        
        try (org.osid.type.TypeList types = forum.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.forumsByRecord.put(types.getNextType(), forum);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a forum from this session.
     *
     *  @param forumId the <code>Id</code> of the forum
     *  @throws org.osid.NullArgumentException <code>forumId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeForum(org.osid.id.Id forumId) {
        org.osid.forum.Forum forum;
        try {
            forum = getForum(forumId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.forumsByGenus.remove(forum.getGenusType());

        try (org.osid.type.TypeList types = forum.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.forumsByRecord.remove(types.getNextType(), forum);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeForum(forumId);
        return;
    }


    /**
     *  Gets a <code>ForumList</code> corresponding to the given
     *  forum genus <code>Type</code> which does not include
     *  forums of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known forums or an error results. Otherwise,
     *  the returned list may contain only those forums that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  forumGenusType a forum genus type 
     *  @return the returned <code>Forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByGenusType(org.osid.type.Type forumGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.forum.forum.ArrayForumList(this.forumsByGenus.get(forumGenusType)));
    }


    /**
     *  Gets a <code>ForumList</code> containing the given
     *  forum record <code>Type</code>. In plenary mode, the
     *  returned list contains all known forums or an error
     *  results. Otherwise, the returned list may contain only those
     *  forums that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  forumRecordType a forum record type 
     *  @return the returned <code>forum</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForumsByRecordType(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.forum.forum.ArrayForumList(this.forumsByRecord.get(forumRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.forumsByGenus.clear();
        this.forumsByRecord.clear();

        super.close();

        return;
    }
}
