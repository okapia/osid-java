//
// AbstractQueryConferralLookupSession.java
//
//    An inline adapter that maps a ConferralLookupSession to
//    a ConferralQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ConferralLookupSession to
 *  a ConferralQuerySession.
 */

public abstract class AbstractQueryConferralLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractConferralLookupSession
    implements org.osid.recognition.ConferralLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.recognition.ConferralQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryConferralLookupSession.
     *
     *  @param querySession the underlying conferral query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryConferralLookupSession(org.osid.recognition.ConferralQuerySession querySession) {
        nullarg(querySession, "conferral query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Academy</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Academy Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the <code>Academy</code> associated with this 
     *  session.
     *
     *  @return the <code>Academy</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform <code>Conferral</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupConferrals() {
        return (this.session.canSearchConferrals());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include conferrals in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    

    /**
     *  Only conferrals whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveConferralView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All conferrals of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveConferralView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Conferral</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Conferral</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Conferral</code> and
     *  retained for compatibility.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralId <code>Id</code> of the
     *          <code>Conferral</code>
     *  @return the conferral
     *  @throws org.osid.NotFoundException <code>conferralId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>conferralId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Conferral getConferral(org.osid.id.Id conferralId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchId(conferralId, true);
        org.osid.recognition.ConferralList conferrals = this.session.getConferralsByQuery(query);
        if (conferrals.hasNext()) {
            return (conferrals.getNextConferral());
        } 
        
        throw new org.osid.NotFoundException(conferralId + " not found");
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  conferrals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Conferrals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, conferrals are returned that are currently effective.
     *  In any effective mode, effective conferrals and those currently expired
     *  are returned.
     *
     *  @param  conferralIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>conferralIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByIds(org.osid.id.IdList conferralIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();

        try (org.osid.id.IdList ids = conferralIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> which does not include
     *  conferrals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently effective.
     *  In any effective mode, effective conferrals and those currently expired
     *  are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchGenusType(conferralGenusType, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a <code>ConferralList</code> corresponding to the given
     *  conferral genus <code>Type</code> and include any additional
     *  conferrals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByParentGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchParentGenusType(conferralGenusType, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a <code>ConferralList</code> containing the given
     *  conferral record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return the returned <code>Conferral</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>conferralRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByRecordType(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchRecordType(conferralRecordType, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a <code>ConferralList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Conferral</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsOnDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }
        

    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible
     *  through this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.recognition.ConferralList getConferralsForAward(org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchAwardId(awardId, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to an award
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are
     *  currently effective.  In any effective mode, effective
     *  conferrals and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForAwardOnDate(org.osid.id.Id awardId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchAwardId(awardId, true);
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAward(org.osid.id.Id resourceId,
                                                                                org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchAwardId(awardId, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ConferralList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchRecipientId(resourceId, true);
        query.matchAwardId(awardId, true);
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of conferrals corresponding to a reference <code>
     *  Id.  </code>
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @return the returned <code> ConferralList </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of all conferrals corresponding to a reference
     *  <code> Id </code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> ConferralList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId, from 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReferenceOnDate(org.osid.id.Id referenceId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of all conferrals corresponding to a convocation <code> 
     *  Id. </code> 
     *  
     *  In plenary mode, the returned list contains all known conferrals or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  conferrals that are accessible through this session. 
     *  
     *  In effective mode, conferrals are returned that are currently 
     *  effective. In any effective mode, effective conferrals and those 
     *  currently expired are returned. 
     *
     *  @param  convocationId the <code> Id </code> of the convocation 
     *  @return the returned <code> ConferralList </code> 
     *  @throws org.osid.NullArgumentException <code> convocationId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocation(org.osid.id.Id convocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchConvocationId(convocationId, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets a list of all conferrals corresponding to a convocation
     *  <code> Id </code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  convocationId a convocation <code> Id </code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> ConferralList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> convocationId,
     *          from, </code> or <code> to </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocationOnDate(org.osid.id.Id convocationId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchConvocationId(convocationId, true);
        query.matchDate(from, to, true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets all <code>Conferrals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Conferrals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferrals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.ConferralQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getConferralsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recognition.ConferralQuery getQuery() {
        org.osid.recognition.ConferralQuery query = this.session.getConferralQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
