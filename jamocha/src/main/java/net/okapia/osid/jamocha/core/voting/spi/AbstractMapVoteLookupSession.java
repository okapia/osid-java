//
// AbstractMapVoteLookupSession
//
//    A simple framework for providing a Vote lookup service
//    backed by a fixed collection of votes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Vote lookup service backed by a
 *  fixed collection of votes. The votes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Votes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapVoteLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractVoteLookupSession
    implements org.osid.voting.VoteLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.Vote> votes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.Vote>());


    /**
     *  Makes a <code>Vote</code> available in this session.
     *
     *  @param  vote a vote
     *  @throws org.osid.NullArgumentException <code>vote<code>
     *          is <code>null</code>
     */

    protected void putVote(org.osid.voting.Vote vote) {
        this.votes.put(vote.getId(), vote);
        return;
    }


    /**
     *  Makes an array of votes available in this session.
     *
     *  @param  votes an array of votes
     *  @throws org.osid.NullArgumentException <code>votes<code>
     *          is <code>null</code>
     */

    protected void putVotes(org.osid.voting.Vote[] votes) {
        putVotes(java.util.Arrays.asList(votes));
        return;
    }


    /**
     *  Makes a collection of votes available in this session.
     *
     *  @param  votes a collection of votes
     *  @throws org.osid.NullArgumentException <code>votes<code>
     *          is <code>null</code>
     */

    protected void putVotes(java.util.Collection<? extends org.osid.voting.Vote> votes) {
        for (org.osid.voting.Vote vote : votes) {
            this.votes.put(vote.getId(), vote);
        }

        return;
    }


    /**
     *  Removes a Vote from this session.
     *
     *  @param  voteId the <code>Id</code> of the vote
     *  @throws org.osid.NullArgumentException <code>voteId<code> is
     *          <code>null</code>
     */

    protected void removeVote(org.osid.id.Id voteId) {
        this.votes.remove(voteId);
        return;
    }


    /**
     *  Gets the <code>Vote</code> specified by its <code>Id</code>.
     *
     *  @param  voteId <code>Id</code> of the <code>Vote</code>
     *  @return the vote
     *  @throws org.osid.NotFoundException <code>voteId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>voteId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Vote getVote(org.osid.id.Id voteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.Vote vote = this.votes.get(voteId);
        if (vote == null) {
            throw new org.osid.NotFoundException("vote not found: " + voteId);
        }

        return (vote);
    }


    /**
     *  Gets all <code>Votes</code>. In plenary mode, the returned
     *  list contains all known votes or an error
     *  results. Otherwise, the returned list may contain only those
     *  votes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Votes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.vote.ArrayVoteList(this.votes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.votes.clear();
        super.close();
        return;
    }
}
