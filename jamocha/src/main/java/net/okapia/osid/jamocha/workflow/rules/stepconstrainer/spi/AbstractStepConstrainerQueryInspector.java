//
// AbstractStepConstrainerQueryInspector.java
//
//     A template for making a StepConstrainerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for step constrainers.
 */

public abstract class AbstractStepConstrainerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQueryInspector
    implements org.osid.workflow.rules.StepConstrainerQueryInspector {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledStepIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getRuledStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given step constrainer query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a step constrainer implementing the requested record.
     *
     *  @param stepConstrainerRecordType a step constrainer record type
     *  @return the step constrainer query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord getStepConstrainerQueryInspectorRecord(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step constrainer query. 
     *
     *  @param stepConstrainerQueryInspectorRecord step constrainer query inspector
     *         record
     *  @param stepConstrainerRecordType stepConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepConstrainerQueryInspectorRecord(org.osid.workflow.rules.records.StepConstrainerQueryInspectorRecord stepConstrainerQueryInspectorRecord, 
                                                   org.osid.type.Type stepConstrainerRecordType) {

        addRecordType(stepConstrainerRecordType);
        nullarg(stepConstrainerRecordType, "step constrainer record type");
        this.records.add(stepConstrainerQueryInspectorRecord);        
        return;
    }
}
