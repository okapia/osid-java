//
// AbstractImmutableForum.java
//
//     Wraps a mutable Forum to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Forum</code> to hide modifiers. This
 *  wrapper provides an immutized Forum from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying forum whose state changes are visible.
 */

public abstract class AbstractImmutableForum
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.forum.Forum {

    private final org.osid.forum.Forum forum;


    /**
     *  Constructs a new <code>AbstractImmutableForum</code>.
     *
     *  @param forum the forum to immutablize
     *  @throws org.osid.NullArgumentException <code>forum</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableForum(org.osid.forum.Forum forum) {
        super(forum);
        this.forum = forum;
        return;
    }


    /**
     *  Gets the forum record corresponding to the given <code> Forum </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> forumRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(forumRecordType) </code> is <code> true </code> . 
     *
     *  @param  forumRecordType the type of forum record to retrieve 
     *  @return the forum record 
     *  @throws org.osid.NullArgumentException <code> forumRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(forumRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.records.ForumRecord getForumRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        return (this.forum.getForumRecord(forumRecordType));
    }
}

