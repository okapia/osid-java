//
// AbstractBusinessSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBusinessSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.BusinessSearchResults {

    private org.osid.billing.BusinessList businesses;
    private final org.osid.billing.BusinessQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.records.BusinessSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBusinessSearchResults.
     *
     *  @param businesses the result set
     *  @param businessQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>businesses</code>
     *          or <code>businessQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBusinessSearchResults(org.osid.billing.BusinessList businesses,
                                            org.osid.billing.BusinessQueryInspector businessQueryInspector) {
        nullarg(businesses, "businesses");
        nullarg(businessQueryInspector, "business query inspectpr");

        this.businesses = businesses;
        this.inspector = businessQueryInspector;

        return;
    }


    /**
     *  Gets the business list resulting from a search.
     *
     *  @return a business list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinesses() {
        if (this.businesses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.BusinessList businesses = this.businesses;
        this.businesses = null;
	return (businesses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.BusinessQueryInspector getBusinessQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  business search record <code> Type. </code> This method must
     *  be used to retrieve a business implementing the requested
     *  record.
     *
     *  @param businessSearchRecordType a business search 
     *         record type 
     *  @return the business search
     *  @throws org.osid.NullArgumentException
     *          <code>businessSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(businessSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessSearchResultsRecord getBusinessSearchResultsRecord(org.osid.type.Type businessSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.records.BusinessSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(businessSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(businessSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record business search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBusinessRecord(org.osid.billing.records.BusinessSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "business record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
