//
// ActivityElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.activity.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ActivityElement Id.
     *
     *  @return the activity element Id
     */

    public static org.osid.id.Id getActivityEntityId() {
        return (makeEntityId("osid.learning.Activity"));
    }


    /**
     *  Gets the ObjectiveId element Id.
     *
     *  @return the ObjectiveId element Id
     */

    public static org.osid.id.Id getObjectiveId() {
        return (makeElementId("osid.learning.activity.ObjectiveId"));
    }


    /**
     *  Gets the Objective element Id.
     *
     *  @return the Objective element Id
     */

    public static org.osid.id.Id getObjective() {
        return (makeElementId("osid.learning.activity.Objective"));
    }


    /**
     *  Gets the AssetIds element Id.
     *
     *  @return the AssetIds element Id
     */

    public static org.osid.id.Id getAssetIds() {
        return (makeElementId("osid.learning.activity.AssetIds"));
    }


    /**
     *  Gets the Assets element Id.
     *
     *  @return the Assets element Id
     */

    public static org.osid.id.Id getAssets() {
        return (makeElementId("osid.learning.activity.Assets"));
    }


    /**
     *  Gets the CourseIds element Id.
     *
     *  @return the CourseIds element Id
     */

    public static org.osid.id.Id getCourseIds() {
        return (makeElementId("osid.learning.activity.CourseIds"));
    }


    /**
     *  Gets the Courses element Id.
     *
     *  @return the Courses element Id
     */

    public static org.osid.id.Id getCourses() {
        return (makeElementId("osid.learning.activity.Courses"));
    }


    /**
     *  Gets the AssessmentIds element Id.
     *
     *  @return the AssessmentIds element Id
     */

    public static org.osid.id.Id getAssessmentIds() {
        return (makeElementId("osid.learning.activity.AssessmentIds"));
    }


    /**
     *  Gets the Assessments element Id.
     *
     *  @return the Assessments element Id
     */

    public static org.osid.id.Id getAssessments() {
        return (makeElementId("osid.learning.activity.Assessments"));
    }


    /**
     *  Gets the ObjectiveBankId element Id.
     *
     *  @return the ObjectiveBankId element Id
     */

    public static org.osid.id.Id getObjectiveBankId() {
        return (makeQueryElementId("osid.learning.activity.ObjectiveBankId"));
    }


    /**
     *  Gets the ObjectiveBank element Id.
     *
     *  @return the ObjectiveBank element Id
     */

    public static org.osid.id.Id getObjectiveBank() {
        return (makeQueryElementId("osid.learning.activity.ObjectiveBank"));
    }
}
