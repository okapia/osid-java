//
// AbstractQueryEntryLookupSession.java
//
//    An inline adapter that maps an EntryLookupSession to
//    an EntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EntryLookupSession to
 *  an EntryQuerySession.
 */

public abstract class AbstractQueryEntryLookupSession
    extends net.okapia.osid.jamocha.blogging.spi.AbstractEntryLookupSession
    implements org.osid.blogging.EntryLookupSession {

    private final org.osid.blogging.EntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEntryLookupSession.
     *
     *  @param querySession the underlying entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEntryLookupSession(org.osid.blogging.EntryQuerySession querySession) {
        nullarg(querySession, "entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Blog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Blog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBlogId() {
        return (this.session.getBlogId());
    }


    /**
     *  Gets the <code>Blog</code> associated with this 
     *  session.
     *
     *  @return the <code>Blog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBlog());
    }


    /**
     *  Tests if this user can perform <code>Entry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (this.session.canSearchEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in blogs which are children
     *  of this blog in the blog hierarchy.
     */

    @OSID @Override
    public void useFederatedBlogView() {
        this.session.useFederatedBlogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this blog only.
     */

    @OSID @Override
    public void useIsolatedBlogView() {
        this.session.useIsolatedBlogView();
        return;
    }
    
     
    /**
     *  Gets the <code>Entry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Entry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Entry</code> and
     *  retained for compatibility.
     *
     *  @param  entryId <code>Id</code> of the
     *          <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchId(entryId, true);
        org.osid.blogging.EntryList entries = this.session.getEntriesByQuery(query);
        if (entries.hasNext()) {
            return (entries.getNextEntry());
        } 
        
        throw new org.osid.NotFoundException(entryId + " not found");
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Entries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  entryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>entryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();

        try (org.osid.id.IdList ids = entryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEntriesByQuery(query));
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchGenusType(entryGenusType, true);
        return (this.session.getEntriesByQuery(query));
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> and include any additional
     *  entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchParentGenusType(entryGenusType, true);
        return (this.session.getEntriesByQuery(query));
    }


    /**
     *  Gets an <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchRecordType(entryRecordType, true);
        return (this.session.getEntriesByQuery(query));
    }


    /**
     *  Gets an <code> EntryList </code> posted within the specified
     *  range inclusive. In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Entry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchTimestamp(from, to, true);
        return (this.session.getEntriesByQuery(query));
    }        


    /**
     *  Gets an <code> EntryList </code> for the given poster resource
     *  <code> Id. </code> In plenary mode, the returned list contains
     *  all known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  resourceId a poster <code> Id </code> 
     *  @return the returned <code> Entry </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchPosterId(resourceId, true);
        return (this.session.getEntriesByQuery(query));
    }        


    /**
     *  Gets an <code> EntryList </code> for a poster resource <code>
     *  Id </code> posted within the specified range inclusive. In
     *  plenary mode, the returned list contains all known entries or
     *  an error results.  Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  @param  resourceId a poster <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Entry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code> 
     *          or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByDateForPoster(org.osid.id.Id resourceId, 
                                                                 org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchPosterId(resourceId, true);
        query.matchTimestamp(from, to, true);
        return (this.session.getEntriesByQuery(query));
    }        


    /**
     *  Gets an <code>EntryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Entry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getEntriesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Entries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.blogging.EntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.blogging.EntryQuery getQuery() {
        org.osid.blogging.EntryQuery query = this.session.getEntryQuery();
        return (query);
    }
}
