//
// AbstractImmutableCredentialRequirement.java
//
//     Wraps a mutable CredentialRequirement to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.credentialrequirement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CredentialRequirement</code> to hide modifiers. This
 *  wrapper provides an immutized CredentialRequirement from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying credentialRequirement whose state changes are visible.
 */

public abstract class AbstractImmutableCredentialRequirement
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.course.requisite.CredentialRequirement {

    private final org.osid.course.requisite.CredentialRequirement credentialRequirement;


    /**
     *  Constructs a new <code>AbstractImmutableCredentialRequirement</code>.
     *
     *  @param credentialRequirement the credential requirement to immutablize
     *  @throws org.osid.NullArgumentException <code>credentialRequirement</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCredentialRequirement(org.osid.course.requisite.CredentialRequirement credentialRequirement) {
        super(credentialRequirement);
        this.credentialRequirement = credentialRequirement;
        return;
    }


    /**
     *  Gets any <code> Requisites </code> that may be substituted in place of 
     *  this <code> CredentialRequirement. </code> All <code> Requisites 
     *  </code> must be satisifed to be a substitute for this credential 
     *  requirement. Inactive <code> Requisites </code> are not evaluated but 
     *  if no applicable requisite exists, then the alternate requisite is not 
     *  satisifed. 
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.credentialRequirement.getAltRequisites());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Credential. </code> 
     *
     *  @return the credential <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCredentialId() {
        return (this.credentialRequirement.getCredentialId());
    }


    /**
     *  Gets the <code> Credential. </code> 
     *
     *  @return the credential 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential()
        throws org.osid.OperationFailedException {

        return (this.credentialRequirement.getCredential());
    }


    /**
     *  Tests if the credential has to be earned within the required duration. 
     *
     *  @return <code> true </code> if the credential must be earned within a 
     *          required time, <code> false </code> if it could have been 
     *          earned at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.credentialRequirement.hasTimeframe());
    }


    /**
     *  Gets the timeframe in which the credential has to be earned. A 
     *  negative duration indicates the credential had to be earned within the 
     *  specified amount of time in the past. A posiitive duration indicates 
     *  the credential must be earned within the specified amount of time in 
     *  the future. A zero duration indicates the credential must be earned in 
     *  the current term. 
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        return (this.credentialRequirement.getTimeframe());
    }


    /**
     *  Gets the credential requirement record corresponding to the given 
     *  <code> CredentialRequirement </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> credentialRequirementRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(credentialRequirementRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  credentialRequirementRecordType the type of credential 
     *          requirement record to retrieve 
     *  @return the credential requirement record 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialRequirementRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(credentialRequirementRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.CredentialRequirementRecord getCredentialRequirementRecord(org.osid.type.Type credentialRequirementRecordType)
        throws org.osid.OperationFailedException {

        return (this.credentialRequirement.getCredentialRequirementRecord(credentialRequirementRecordType));
    }
}

