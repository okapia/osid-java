//
// AbstractQueryRoomLookupSession.java
//
//    A RoomQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RoomQuerySession adapter.
 */

public abstract class AbstractAdapterRoomQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.RoomQuerySession {

    private final org.osid.room.RoomQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterRoomQuerySession.
     *
     *  @param session the underlying room query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRoomQuerySession(org.osid.room.RoomQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCampus</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCampus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@codeCampus</code> associated with this 
     *  session.
     *
     *  @return the {@codeCampus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@codeRoom</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchRooms() {
        return (this.session.canSearchRooms());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rooms in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this campus only.
     */
    
    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    
      
    /**
     *  Gets a room query. The returned query will not have an
     *  extension query.
     *
     *  @return the room query 
     */
      
    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        return (this.session.getRoomQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  roomQuery the room query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code roomQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code roomQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByQuery(org.osid.room.RoomQuery roomQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getRoomsByQuery(roomQuery));
    }
}
