//
// InvariantMapBlogLookupSession
//
//    Implements a Blog lookup service backed by a fixed collection of
//    blogs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging;


/**
 *  Implements a Blog lookup service backed by a fixed
 *  collection of blogs. The blogs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBlogLookupSession
    extends net.okapia.osid.jamocha.core.blogging.spi.AbstractMapBlogLookupSession
    implements org.osid.blogging.BlogLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBlogLookupSession</code> with no
     *  blogs.
     */

    public InvariantMapBlogLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBlogLookupSession</code> with a single
     *  blog.
     *  
     *  @throws org.osid.NullArgumentException {@code blog}
     *          is <code>null</code>
     */

    public InvariantMapBlogLookupSession(org.osid.blogging.Blog blog) {
        putBlog(blog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBlogLookupSession</code> using an array
     *  of blogs.
     *  
     *  @throws org.osid.NullArgumentException {@code blogs}
     *          is <code>null</code>
     */

    public InvariantMapBlogLookupSession(org.osid.blogging.Blog[] blogs) {
        putBlogs(blogs);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBlogLookupSession</code> using a
     *  collection of blogs.
     *
     *  @throws org.osid.NullArgumentException {@code blogs}
     *          is <code>null</code>
     */

    public InvariantMapBlogLookupSession(java.util.Collection<? extends org.osid.blogging.Blog> blogs) {
        putBlogs(blogs);
        return;
    }
}
