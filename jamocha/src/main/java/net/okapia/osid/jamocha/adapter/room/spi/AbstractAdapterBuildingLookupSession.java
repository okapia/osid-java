//
// AbstractAdapterBuildingLookupSession.java
//
//    A Building lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Building lookup session adapter.
 */

public abstract class AbstractAdapterBuildingLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.BuildingLookupSession {

    private final org.osid.room.BuildingLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBuildingLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBuildingLookupSession(org.osid.room.BuildingLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Building} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBuildings() {
        return (this.session.canLookupBuildings());
    }


    /**
     *  A complete view of the {@code Building} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBuildingView() {
        this.session.useComparativeBuildingView();
        return;
    }


    /**
     *  A complete view of the {@code Building} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBuildingView() {
        this.session.usePlenaryBuildingView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include buildings in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only buildings whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveBuildingView() {
        this.session.useEffectiveBuildingView();
        return;
    }
    

    /**
     *  All buildings of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveBuildingView() {
        this.session.useAnyEffectiveBuildingView();
        return;
    }

     
    /**
     *  Gets the {@code Building} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Building} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Building} and
     *  retained for compatibility.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param buildingId {@code Id} of the {@code Building}
     *  @return the building
     *  @throws org.osid.NotFoundException {@code buildingId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding(org.osid.id.Id buildingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuilding(buildingId));
    }


    /**
     *  Gets a {@code BuildingList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  buildings specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Buildings} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  buildingIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Building} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code buildingIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByIds(org.osid.id.IdList buildingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildingsByIds(buildingIds));
    }


    /**
     *  Gets a {@code BuildingList} corresponding to the given
     *  building genus {@code Type} which does not include
     *  buildings of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  buildingGenusType a building genus type 
     *  @return the returned {@code Building} list
     *  @throws org.osid.NullArgumentException
     *          {@code buildingGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByGenusType(org.osid.type.Type buildingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildingsByGenusType(buildingGenusType));
    }


    /**
     *  Gets a {@code BuildingList} corresponding to the given
     *  building genus {@code Type} and include any additional
     *  buildings with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  buildingGenusType a building genus type 
     *  @return the returned {@code Building} list
     *  @throws org.osid.NullArgumentException
     *          {@code buildingGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByParentGenusType(org.osid.type.Type buildingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildingsByParentGenusType(buildingGenusType));
    }


    /**
     *  Gets a {@code BuildingList} containing the given
     *  building record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  buildingRecordType a building record type 
     *  @return the returned {@code Building} list
     *  @throws org.osid.NullArgumentException
     *          {@code buildingRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByRecordType(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildingsByRecordType(buildingRecordType));
    }


    /**
     *  Gets a {@code BuildingList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible
     *  through this session.
     *  
     *  In active mode, buildings are returned that are currently
     *  active. In any status mode, active and inactive buildings
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Building} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.BuildingList getBuildingsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildingsOnDate(from, to));
    }
        
    
    /**
     *  Gets a {@code BuildingList} containing of the given building
     *  number.
     *  
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session.
     *  
     *  In effective mode, buildings are returned that are currently
     *  effective. In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  number a building number 
     *  @return the returned {@code Building} list 
     *  @throws org.osid.NullArgumentException {@code number} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBuildingsByNumber(number));
    }


    /**
     *  Gets all {@code Buildings}. 
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Buildings} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBuildings());
    }
}
