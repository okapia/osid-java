//
// AbstractOrganizationQueryInspector.java
//
//     A template for making an OrganizationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for organizations.
 */

public abstract class AbstractOrganizationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.personnel.OrganizationQueryInspector {

    private final java.util.Collection<org.osid.personnel.records.OrganizationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the display label query terms. 
     *
     *  @return the display label terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the ancestor organization <code> Id </code> query terms. 
     *
     *  @return the ancestor organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOrganizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor organization query terms. 
     *
     *  @return the ancestor organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getAncestorOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the descendant organization <code> Id </code> query terms. 
     *
     *  @return the descendant organization <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOrganizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant organization query terms. 
     *
     *  @return the descendant organization terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getDescendantOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given organization query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an organization implementing the requested record.
     *
     *  @param organizationRecordType an organization record type
     *  @return the organization query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>organizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationQueryInspectorRecord getOrganizationQueryInspectorRecord(org.osid.type.Type organizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.OrganizationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(organizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this organization query. 
     *
     *  @param organizationQueryInspectorRecord organization query inspector
     *         record
     *  @param organizationRecordType organization record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrganizationQueryInspectorRecord(org.osid.personnel.records.OrganizationQueryInspectorRecord organizationQueryInspectorRecord, 
                                                   org.osid.type.Type organizationRecordType) {

        addRecordType(organizationRecordType);
        nullarg(organizationRecordType, "organization record type");
        this.records.add(organizationQueryInspectorRecord);        
        return;
    }
}
