//
// AbstractFunction.java
//
//     Defines a Function builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.function.spi;


/**
 *  Defines a <code>Function</code> builder.
 */

public abstract class AbstractFunctionBuilder<T extends AbstractFunctionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authorization.function.FunctionMiter function;


    /**
     *  Constructs a new <code>AbstractFunctionBuilder</code>.
     *
     *  @param function the function to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractFunctionBuilder(net.okapia.osid.jamocha.builder.authorization.function.FunctionMiter function) {
        super(function);
        this.function = function;
        return;
    }


    /**
     *  Builds the function.
     *
     *  @return the new function
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authorization.Function build() {
        (new net.okapia.osid.jamocha.builder.validator.authorization.function.FunctionValidator(getValidations())).validate(this.function);
        return (new net.okapia.osid.jamocha.builder.authorization.function.ImmutableFunction(this.function));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the function miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authorization.function.FunctionMiter getMiter() {
        return (this.function);
    }


    /**
     *  Sets the qualifier hierarchy.
     *
     *  @param qualifierHierarchy a qualifier hierarchy
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierHierarchy</code> is <code>null</code>
     */

    public T qualifierHierarchy(org.osid.hierarchy.Hierarchy qualifierHierarchy) {
        getMiter().setQualifierHierarchy(qualifierHierarchy);
        return (self());
    }


    /**
     *  Adds a Function record.
     *
     *  @param record a function record
     *  @param recordType the type of function record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authorization.records.FunctionRecord record, org.osid.type.Type recordType) {
        getMiter().addFunctionRecord(record, recordType);
        return (self());
    }
}       


