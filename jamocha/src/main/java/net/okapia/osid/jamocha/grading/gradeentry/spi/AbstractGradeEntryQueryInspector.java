//
// AbstractGradeEntryQueryInspector.java
//
//     A template for making a GradeEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for grade entries.
 */

public abstract class AbstractGradeEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.grading.GradeEntryQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradeEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the gradebook column <code> Id </code> terms. 
     *
     *  @return the gradebook column <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookColumnIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook column terms. 
     *
     *  @return the gradebook column terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQueryInspector[] getGradebookColumnTerms() {
        return (new org.osid.grading.GradebookColumnQueryInspector[0]);
    }


    /**
     *  Gets the key resource <code> Id </code> terms. 
     *
     *  @return the key resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getKeyResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the key resource terms. 
     *
     *  @return the key resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getKeyResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the derived terms. 
     *
     *  @return the derived terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDerivedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the overridden calculated grade entry <code> Id </code> terms. 
     *
     *  @return the overridden grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOverriddenGradeEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the overriden derived grade terms. 
     *
     *  @return the overridden grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getOverriddenGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the ignored for caluclation entries terms. 
     *
     *  @return the ignored for calculation terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getIgnoredForCalculationsTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the grade <code> Id </code> terms. 
     *
     *  @return the grade <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the score terms. 
     *
     *  @return the score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the time graded terms. 
     *
     *  @return the time graded terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimeGradedTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the grader <code> Id </code> terms. 
     *
     *  @return the grader <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraderIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grader terms. 
     *
     *  @return the grader terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getGraderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the grading agent <code> Id </code> terms. 
     *
     *  @return the grading agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grading agent terms. 
     *
     *  @return the grading agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getGradingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given grade entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a grade entry implementing the requested record.
     *
     *  @param gradeEntryRecordType a grade entry record type
     *  @return the grade entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryQueryInspectorRecord getGradeEntryQueryInspectorRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade entry query. 
     *
     *  @param gradeEntryQueryInspectorRecord grade entry query inspector
     *         record
     *  @param gradeEntryRecordType gradeEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeEntryQueryInspectorRecord(org.osid.grading.records.GradeEntryQueryInspectorRecord gradeEntryQueryInspectorRecord, 
                                                   org.osid.type.Type gradeEntryRecordType) {

        addRecordType(gradeEntryRecordType);
        nullarg(gradeEntryRecordType, "grade entry record type");
        this.records.add(gradeEntryQueryInspectorRecord);        
        return;
    }
}
