//
// AbstractRenovationQueryInspector.java
//
//     A template for making a RenovationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.renovation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for renovations.
 */

public abstract class AbstractRenovationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.room.construction.RenovationQueryInspector {

    private final java.util.Collection<org.osid.room.construction.records.RenovationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given renovation query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a renovation implementing the requested record.
     *
     *  @param renovationRecordType a renovation record type
     *  @return the renovation query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(renovationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.RenovationQueryInspectorRecord getRenovationQueryInspectorRecord(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.construction.records.RenovationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(renovationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(renovationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this renovation query. 
     *
     *  @param renovationQueryInspectorRecord renovation query inspector
     *         record
     *  @param renovationRecordType renovation record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRenovationQueryInspectorRecord(org.osid.room.construction.records.RenovationQueryInspectorRecord renovationQueryInspectorRecord, 
                                                   org.osid.type.Type renovationRecordType) {

        addRecordType(renovationRecordType);
        nullarg(renovationRecordType, "renovation record type");
        this.records.add(renovationQueryInspectorRecord);        
        return;
    }
}
