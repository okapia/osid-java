//
// AbstractImmutableBrowsable
//
//     Defines an immutable wrapper for a Browsable Object.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for a Browsable Object.
 */

public abstract class AbstractImmutableBrowsable
    extends AbstractImmutableExtensible
    implements org.osid.Browsable {

    private final org.osid.Browsable object;


    /**
     *  Constructs a new <code>AbstractImmutableBrowsable</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException <code>object</code> is
     *          <code>null</code>
     */

    protected AbstractImmutableBrowsable(org.osid.Extensible object) {
        super(object);

        if (!(object instanceof org.osid.Browsable)) {
            throw new org.osid.UnsupportedException("object not a Browsable");
        }

        this.object = (org.osid.Browsable) object;;
        return;
    }


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code> Type. </code>
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.object.getProperties());
    }


    /**
     *  Gets the properties by record type.
     *
     *  @param  recordType the record type corresponding to the
     *          properties set to retrieve
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> hasRecordType(recordType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.object.getPropertiesByRecordType(recordType));
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.object.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>OsidObject</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this object
     */

    @Override
    public int hashCode() {
        return (this.object.hashCode());
    }


    /**
     *  Returns a string representation of this OsidObject.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.object.toString());
    }
}
