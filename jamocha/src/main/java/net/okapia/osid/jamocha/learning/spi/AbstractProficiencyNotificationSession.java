//
// AbstractProficiencyNotificationSession.java
//
//     A template for making ProficiencyNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Proficiency} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Proficiency} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for proficiency entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractProficiencyNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ProficiencyNotificationSession {

    private boolean federated = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();


    /**
     *  Gets the {@code ObjectiveBank/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code ObjectiveBank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }

    
    /**
     *  Gets the {@code ObjectiveBank} associated with this session.
     *
     *  @return the {@code ObjectiveBank} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the {@code ObjectiveBank}.
     *
     *  @param objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException {@code objectiveBank}
     *          is {@code null}
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can register for {@code Proficiency}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForProficiencyNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeProficiencyNotification() </code>.
     */

    @OSID @Override
    public void reliableProficiencyNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableProficiencyNotifications() {
        return;
    }


    /**
     *  Acknowledge a proficiency notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeProficiencyNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include proficiencies in objective banks which are
     *  children of this objective bank in the objective bank
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new proficiencies. {@code
     *  ProficiencyReceiver.newProficiency()} is invoked when a new
     *  {@code Proficiency} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new proficiencies with the given
     *  genus type. {@code ProficiencyReceiver.newProficiency()} is
     *  invoked when a new {@code Proficiency} appears for the given
     *  resource in this objective bank.
     *
     *  @param proficiencyGenusType the genus type of a proficiency to
     *         monitor
     *  @throws org.osid.NullArgumentException {@code
     *          proficiencyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new proficiencies for the given
     *  objective {@code Id}. {@code
     *  ProficiencyReceiver.newProficiency()} is invoked when a new
     *  {@code Proficiency} is created.
     *
     *  @param  objectiveId the {@code Id} of the objective to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProficienciesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new proficiencies for the given
     *  resource {@code Id}. {@code
     *  ProficiencyReceiver.newProficiency()} is invoked when a new
     *  {@code Proficiency} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProficienciesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated proficiencies. {@code
     *  ProficiencyReceiver.changedProficiency()} is invoked when a
     *  proficiency is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated proficiencies of the
     *  given genus type. {@code
     *  ProficiencyReceiver.changedProficiency()} is invoked when a
     *  proficiency in this objective bank is changed.
     *
     *  @param proficiencyGenusType the genus type of the {@code
     *         Proficiency} to monitor
     *  @throws org.osid.NullArgumentException {@code
     *         proficiencyGenusTYpe} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated proficiencies for the
     *  given objective {@code Id}. {@code
     *  ProficiencyReceiver.changedProficiency()} is invoked when a
     *  {@code Proficiency} in this objectiveBank is changed.
     *
     *  @param  objectiveId the {@code Id} of the objective to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProficienciesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated proficiencies for the
     *  given resource {@code Id}. {@code
     *  ProficiencyReceiver.changedProficiency()} is invoked when a
     *  {@code Proficiency} in this objectiveBank is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProficienciesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated proficiency. {@code
     *  ProficiencyReceiver.changedProficiency()} is invoked when the
     *  specified proficiency is changed.
     *
     *  @param proficiencyId the {@code Id} of the {@code Proficiency} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code proficiencyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProficiency(org.osid.id.Id proficiencyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted proficiencies. {@code
     *  ProficiencyReceiver.deletedProficiency()} is invoked when a
     *  proficiency is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of deleted proficiencies of the
     *  given genus type. {@code
     *  ProficiencyReceiver.deletedProficiency()} is invoked when a
     *  proficiency is deleted or removed from this objective bank.
     *
     *  @param proficiencyGenusType the genus type of the {@code
     *         Proficiency} to monitor
     *  @throws org.osid.NullArgumentException {@code
     *         proficiencyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted proficiencies for the
     *  given objective {@code Id}. {@code
     *  ProficiencyReceiver.deletedProficiency()} is invoked when a
     *  {@code Proficiency} is deleted or removed from this
     *  objectiveBank.
     *
     *  @param  objectiveId the {@code Id} of the objective to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedProficienciesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted proficiencies for the
     *  given resource {@code Id}. {@code
     *  ProficiencyReceiver.deletedProficiency()} is invoked when a
     *  {@code Proficiency} is deleted or removed from this
     *  objectiveBank.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedProficienciesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted proficiency. {@code
     *  ProficiencyReceiver.deletedProficiency()} is invoked when the
     *  specified proficiency is deleted.
     *
     *  @param proficiencyId the {@code Id} of the
     *          {@code Proficiency} to monitor
     *  @throws org.osid.NullArgumentException {@code proficiencyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProficiency(org.osid.id.Id proficiencyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
