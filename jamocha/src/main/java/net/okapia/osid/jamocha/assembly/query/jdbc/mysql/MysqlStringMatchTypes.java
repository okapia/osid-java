//
// MysqlStringMatchTypes .java
//
//     An enumeration of supported string match types.
//
//
// Tom Coppeto
// OnTapSolutions
// 5 October 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc.mysql;

import net.okapia.osid.primordium.types.search.StringMatchTypes;


/**
 *  An enumeration of supported string match types.
 */

public enum MysqlStringMatchTypes
    implements net.okapia.osid.torrefacto.collect.TypeEnum {

    EXACT          (StringMatchTypes.EXACT.getType()),
    IGNORECASE     (StringMatchTypes.IGNORECASE.getType()),
    WORD           (StringMatchTypes.WORD.getType()),
    WORDIGNORECASE (StringMatchTypes.WORDIGNORECASE.getType()),
    WILDCARD       (StringMatchTypes.WILDCARD.getType()),
    REGEX          (StringMatchTypes.REGEX.getType()),
    SOUND          (StringMatchTypes.SOUND.getType());

    private final org.osid.type.Type type;

    private MysqlStringMatchTypes(org.osid.type.Type type) {
        this.type = type;
        return;
    }
    

    /**
     *  Gets the Type.
     *
     *  @return the Type
     */

    public org.osid.type.Type getType() {
        return (this.type);
    }
}
