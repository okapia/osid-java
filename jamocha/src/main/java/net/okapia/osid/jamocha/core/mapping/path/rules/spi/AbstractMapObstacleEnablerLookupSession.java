//
// AbstractMapObstacleEnablerLookupSession
//
//    A simple framework for providing an ObstacleEnabler lookup service
//    backed by a fixed collection of obstacle enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ObstacleEnabler lookup service backed by a
 *  fixed collection of obstacle enablers. The obstacle enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ObstacleEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.mapping.path.rules.spi.AbstractObstacleEnablerLookupSession
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.path.rules.ObstacleEnabler>());


    /**
     *  Makes an <code>ObstacleEnabler</code> available in this session.
     *
     *  @param  obstacleEnabler an obstacle enabler
     *  @throws org.osid.NullArgumentException <code>obstacleEnabler<code>
     *          is <code>null</code>
     */

    protected void putObstacleEnabler(org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler) {
        this.obstacleEnablers.put(obstacleEnabler.getId(), obstacleEnabler);
        return;
    }


    /**
     *  Makes an array of obstacle enablers available in this session.
     *
     *  @param  obstacleEnablers an array of obstacle enablers
     *  @throws org.osid.NullArgumentException <code>obstacleEnablers<code>
     *          is <code>null</code>
     */

    protected void putObstacleEnablers(org.osid.mapping.path.rules.ObstacleEnabler[] obstacleEnablers) {
        putObstacleEnablers(java.util.Arrays.asList(obstacleEnablers));
        return;
    }


    /**
     *  Makes a collection of obstacle enablers available in this session.
     *
     *  @param  obstacleEnablers a collection of obstacle enablers
     *  @throws org.osid.NullArgumentException <code>obstacleEnablers<code>
     *          is <code>null</code>
     */

    protected void putObstacleEnablers(java.util.Collection<? extends org.osid.mapping.path.rules.ObstacleEnabler> obstacleEnablers) {
        for (org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler : obstacleEnablers) {
            this.obstacleEnablers.put(obstacleEnabler.getId(), obstacleEnabler);
        }

        return;
    }


    /**
     *  Removes an ObstacleEnabler from this session.
     *
     *  @param  obstacleEnablerId the <code>Id</code> of the obstacle enabler
     *  @throws org.osid.NullArgumentException <code>obstacleEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeObstacleEnabler(org.osid.id.Id obstacleEnablerId) {
        this.obstacleEnablers.remove(obstacleEnablerId);
        return;
    }


    /**
     *  Gets the <code>ObstacleEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  obstacleEnablerId <code>Id</code> of the <code>ObstacleEnabler</code>
     *  @return the obstacleEnabler
     *  @throws org.osid.NotFoundException <code>obstacleEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>obstacleEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnabler getObstacleEnabler(org.osid.id.Id obstacleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.path.rules.ObstacleEnabler obstacleEnabler = this.obstacleEnablers.get(obstacleEnablerId);
        if (obstacleEnabler == null) {
            throw new org.osid.NotFoundException("obstacleEnabler not found: " + obstacleEnablerId);
        }

        return (obstacleEnabler);
    }


    /**
     *  Gets all <code>ObstacleEnablers</code>. In plenary mode, the returned
     *  list contains all known obstacleEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  obstacleEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ObstacleEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.ArrayObstacleEnablerList(this.obstacleEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.obstacleEnablers.clear();
        super.close();
        return;
    }
}
