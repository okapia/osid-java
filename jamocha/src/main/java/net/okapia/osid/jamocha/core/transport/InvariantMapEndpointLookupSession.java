//
// InvariantMapEndpointLookupSession
//
//    Implements an Endpoint lookup service backed by a fixed collection of
//    endpoints.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.transport;


/**
 *  Implements an Endpoint lookup service backed by a fixed
 *  collection of endpoints. The endpoints are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapEndpointLookupSession
    extends net.okapia.osid.jamocha.core.transport.spi.AbstractMapEndpointLookupSession
    implements org.osid.transport.EndpointLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapEndpointLookupSession</code> with no
     *  endpoints.
     */

    public InvariantMapEndpointLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEndpointLookupSession</code> with a single
     *  endpoint.
     *  
     *  @throws org.osid.NullArgumentException {@code endpoint}
     *          is <code>null</code>
     */

    public InvariantMapEndpointLookupSession(org.osid.transport.Endpoint endpoint) {
        putEndpoint(endpoint);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEndpointLookupSession</code> using an array
     *  of endpoints.
     *  
     *  @throws org.osid.NullArgumentException {@code endpoints}
     *          is <code>null</code>
     */

    public InvariantMapEndpointLookupSession(org.osid.transport.Endpoint[] endpoints) {
        putEndpoints(endpoints);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapEndpointLookupSession</code> using a
     *  collection of endpoints.
     *
     *  @throws org.osid.NullArgumentException {@code endpoints}
     *          is <code>null</code>
     */

    public InvariantMapEndpointLookupSession(java.util.Collection<? extends org.osid.transport.Endpoint> endpoints) {
        putEndpoints(endpoints);
        return;
    }
}
