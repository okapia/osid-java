//
// AbstractIndexedMapResourceLookupSession.java
//
//    A simple framework for providing a Resource lookup service
//    backed by a fixed collection of resources with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Resource lookup service backed by a
 *  fixed collection of resources. The resources are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some resources may be compatible
 *  with more types than are indicated through these resource
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Resources</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapResourceLookupSession
    extends AbstractMapResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resource.Resource> resourcesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.Resource>());
    private final MultiMap<org.osid.type.Type, org.osid.resource.Resource> resourcesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.Resource>());


    /**
     *  Makes a <code>Resource</code> available in this session.
     *
     *  @param  resource a resource
     *  @throws org.osid.NullArgumentException <code>resource<code> is
     *          <code>null</code>
     */

    @Override
    protected void putResource(org.osid.resource.Resource resource) {
        super.putResource(resource);

        this.resourcesByGenus.put(resource.getGenusType(), resource);
        
        try (org.osid.type.TypeList types = resource.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resourcesByRecord.put(types.getNextType(), resource);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a resource from this session.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeResource(org.osid.id.Id resourceId) {
        org.osid.resource.Resource resource;
        try {
            resource = getResource(resourceId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.resourcesByGenus.remove(resource.getGenusType());

        try (org.osid.type.TypeList types = resource.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.resourcesByRecord.remove(types.getNextType(), resource);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeResource(resourceId);
        return;
    }


    /**
     *  Gets a <code>ResourceList</code> corresponding to the given
     *  resource genus <code>Type</code> which does not include
     *  resources of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known resources or an error results. Otherwise,
     *  the returned list may contain only those resources that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  resourceGenusType a resource genus type 
     *  @return the returned <code>Resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByGenusType(org.osid.type.Type resourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.resourcesByGenus.get(resourceGenusType)));
    }


    /**
     *  Gets a <code>ResourceList</code> containing the given
     *  resource record <code>Type</code>. In plenary mode, the
     *  returned list contains all known resources or an error
     *  results. Otherwise, the returned list may contain only those
     *  resources that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  resourceRecordType a resource record type 
     *  @return the returned <code>resource</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResourcesByRecordType(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.resourcesByRecord.get(resourceRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.resourcesByGenus.clear();
        this.resourcesByRecord.clear();

        super.close();

        return;
    }
}
