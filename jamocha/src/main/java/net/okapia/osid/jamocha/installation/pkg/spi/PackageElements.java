//
// PackageElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PackageElements
    extends net.okapia.osid.jamocha.spi.SourceableOsidObjectElements {


    /**
     *  Gets the PackageElement Id.
     *
     *  @return the package element Id
     */

    public static org.osid.id.Id getPackageEntityId() {
        return (makeEntityId("osid.installation.Package"));
    }


    /**
     *  Gets the Version element Id.
     *
     *  @return the Version element Id
     */

    public static org.osid.id.Id getVersion() {
        return (makeElementId("osid.installation.package.Version"));
    }


    /**
     *  Gets the Copyright element Id.
     *
     *  @return the Copyright element Id
     */

    public static org.osid.id.Id getCopyright() {
        return (makeElementId("osid.installation.package.Copyright"));
    }


    /**
     *  Gets the CreatorId element Id.
     *
     *  @return the CreatorId element Id
     */

    public static org.osid.id.Id getCreatorId() {
        return (makeElementId("osid.installation.package.CreatorId"));
    }


    /**
     *  Gets the Creator element Id.
     *
     *  @return the Creator element Id
     */

    public static org.osid.id.Id getCreator() {
        return (makeElementId("osid.installation.package.Creator"));
    }


    /**
     *  Gets the ReleaseDate element Id.
     *
     *  @return the ReleaseDate element Id
     */

    public static org.osid.id.Id getReleaseDate() {
        return (makeElementId("osid.installation.package.ReleaseDate"));
    }


    /**
     *  Gets the DependencyIds element Id.
     *
     *  @return the DependencyIds element Id
     */

    public static org.osid.id.Id getDependencyIds() {
        return (makeElementId("osid.installation.package.DependencyIds"));
    }


    /**
     *  Gets the Dependencies element Id.
     *
     *  @return the Dependencies element Id
     */

    public static org.osid.id.Id getDependencies() {
        return (makeElementId("osid.installation.package.Dependencies"));
    }


    /**
     *  Gets the URL element Id.
     *
     *  @return the URL element Id
     */

    public static org.osid.id.Id getURL() {
        return (makeElementId("osid.installation.package.URL"));
    }


    /**
     *  Gets the VersionSince element Id.
     *
     *  @return the VersionSince element Id
     */

    public static org.osid.id.Id getVersionSince() {
        return (makeQueryElementId("osid.installation.package.VersionSince"));
    }


    /**
     *  Gets the RequiresLicenseAcknowledgement element Id.
     *
     *  @return the RequiresLicenseAcknowledgement element Id
     */

    public static org.osid.id.Id getRequiresLicenseAcknowledgement() {
        return (makeElementId("osid.installation.package.RequiresLicenseAcknowledgement"));
    }


    /**
     *  Gets the InstallationId element Id.
     *
     *  @return the InstallationId element Id
     */

    public static org.osid.id.Id getInstallationId() {
        return (makeQueryElementId("osid.installation.package.InstallationId"));
    }


    /**
     *  Gets the Installation element Id.
     *
     *  @return the Installation element Id
     */

    public static org.osid.id.Id getInstallation() {
        return (makeQueryElementId("osid.installation.package.Installation"));
    }


    /**
     *  Gets the DependentId element Id.
     *
     *  @return the DependentId element Id
     */

    public static org.osid.id.Id getDependentId() {
        return (makeQueryElementId("osid.installation.package.DependentId"));
    }


    /**
     *  Gets the Dependent element Id.
     *
     *  @return the Dependent element Id
     */

    public static org.osid.id.Id getDependent() {
        return (makeQueryElementId("osid.installation.package.Dependent"));
    }


    /**
     *  Gets the VersionedPackageId element Id.
     *
     *  @return the VersionedPackageId element Id
     */

    public static org.osid.id.Id getVersionedPackageId() {
        return (makeQueryElementId("osid.installation.package.VersionedPackageId"));
    }


    /**
     *  Gets the VersionedPackage element Id.
     *
     *  @return the VersionedPackage element Id
     */

    public static org.osid.id.Id getVersionedPackage() {
        return (makeQueryElementId("osid.installation.package.VersionedPackage"));
    }


    /**
     *  Gets the InstallFormatType element Id.
     *
     *  @return the InstallFormatType element Id
     */

    public static org.osid.id.Id getInstallFormatType() {
        return (makeQueryElementId("osid.installation.package.InstallFormatType"));
    }


    /**
     *  Gets the DepotId element Id.
     *
     *  @return the DepotId element Id
     */

    public static org.osid.id.Id getDepotId() {
        return (makeQueryElementId("osid.installation.package.DepotId"));
    }


    /**
     *  Gets the Depot element Id.
     *
     *  @return the Depot element Id
     */

    public static org.osid.id.Id getDepot() {
        return (makeQueryElementId("osid.installation.package.Depot"));
    }
}
