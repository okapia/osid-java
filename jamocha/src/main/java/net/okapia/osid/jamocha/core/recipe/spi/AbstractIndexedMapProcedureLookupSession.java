//
// AbstractIndexedMapProcedureLookupSession.java
//
//    A simple framework for providing a Procedure lookup service
//    backed by a fixed collection of procedures with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Procedure lookup service backed by a
 *  fixed collection of procedures. The procedures are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some procedures may be compatible
 *  with more types than are indicated through these procedure
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Procedures</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProcedureLookupSession
    extends AbstractMapProcedureLookupSession
    implements org.osid.recipe.ProcedureLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recipe.Procedure> proceduresByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Procedure>());
    private final MultiMap<org.osid.type.Type, org.osid.recipe.Procedure> proceduresByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Procedure>());


    /**
     *  Makes a <code>Procedure</code> available in this session.
     *
     *  @param  procedure a procedure
     *  @throws org.osid.NullArgumentException <code>procedure<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProcedure(org.osid.recipe.Procedure procedure) {
        super.putProcedure(procedure);

        this.proceduresByGenus.put(procedure.getGenusType(), procedure);
        
        try (org.osid.type.TypeList types = procedure.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.proceduresByRecord.put(types.getNextType(), procedure);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a procedure from this session.
     *
     *  @param procedureId the <code>Id</code> of the procedure
     *  @throws org.osid.NullArgumentException <code>procedureId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProcedure(org.osid.id.Id procedureId) {
        org.osid.recipe.Procedure procedure;
        try {
            procedure = getProcedure(procedureId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.proceduresByGenus.remove(procedure.getGenusType());

        try (org.osid.type.TypeList types = procedure.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.proceduresByRecord.remove(types.getNextType(), procedure);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProcedure(procedureId);
        return;
    }


    /**
     *  Gets a <code>ProcedureList</code> corresponding to the given
     *  procedure genus <code>Type</code> which does not include
     *  procedures of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known procedures or an error results. Otherwise,
     *  the returned list may contain only those procedures that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  procedureGenusType a procedure genus type 
     *  @return the returned <code>Procedure</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>procedureGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByGenusType(org.osid.type.Type procedureGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.procedure.ArrayProcedureList(this.proceduresByGenus.get(procedureGenusType)));
    }


    /**
     *  Gets a <code>ProcedureList</code> containing the given
     *  procedure record <code>Type</code>. In plenary mode, the
     *  returned list contains all known procedures or an error
     *  results. Otherwise, the returned list may contain only those
     *  procedures that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  procedureRecordType a procedure record type 
     *  @return the returned <code>procedure</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByRecordType(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.procedure.ArrayProcedureList(this.proceduresByRecord.get(procedureRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.proceduresByGenus.clear();
        this.proceduresByRecord.clear();

        super.close();

        return;
    }
}
