//
// AbstractMapMapLookupSession
//
//    A simple framework for providing a Map lookup service
//    backed by a fixed collection of maps.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Map lookup service backed by a
 *  fixed collection of maps. The maps are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Maps</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapMapLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMapLookupSession
    implements org.osid.mapping.MapLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.Map> maps = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.Map>());


    /**
     *  Makes a <code>Map</code> available in this session.
     *
     *  @param  map a map
     *  @throws org.osid.NullArgumentException <code>map<code>
     *          is <code>null</code>
     */

    protected void putMap(org.osid.mapping.Map map) {
        this.maps.put(map.getId(), map);
        return;
    }


    /**
     *  Makes an array of maps available in this session.
     *
     *  @param  maps an array of maps
     *  @throws org.osid.NullArgumentException <code>maps<code>
     *          is <code>null</code>
     */

    protected void putMaps(org.osid.mapping.Map[] maps) {
        putMaps(java.util.Arrays.asList(maps));
        return;
    }


    /**
     *  Makes a collection of maps available in this session.
     *
     *  @param  maps a collection of maps
     *  @throws org.osid.NullArgumentException <code>maps<code>
     *          is <code>null</code>
     */

    protected void putMaps(java.util.Collection<? extends org.osid.mapping.Map> maps) {
        for (org.osid.mapping.Map map : maps) {
            this.maps.put(map.getId(), map);
        }

        return;
    }


    /**
     *  Removes a Map from this session.
     *
     *  @param  mapId the <code>Id</code> of the map
     *  @throws org.osid.NullArgumentException <code>mapId<code> is
     *          <code>null</code>
     */

    protected void removeMap(org.osid.id.Id mapId) {
        this.maps.remove(mapId);
        return;
    }


    /**
     *  Gets the <code>Map</code> specified by its <code>Id</code>.
     *
     *  @param  mapId <code>Id</code> of the <code>Map</code>
     *  @return the map
     *  @throws org.osid.NotFoundException <code>mapId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>mapId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.Map map = this.maps.get(mapId);
        if (map == null) {
            throw new org.osid.NotFoundException("map not found: " + mapId);
        }

        return (map);
    }


    /**
     *  Gets all <code>Maps</code>. In plenary mode, the returned
     *  list contains all known maps or an error
     *  results. Otherwise, the returned list may contain only those
     *  maps that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Maps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.MapList getMaps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.map.ArrayMapList(this.maps.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.maps.clear();
        super.close();
        return;
    }
}
