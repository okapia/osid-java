//
// GradebookColumnCalculationValidator.java
//
//     Validates a GradebookColumnCalculation.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.grading.calculation.gradebookcolumncalculation;


/**
 *  Validates a GradebookColumnCalculation.
 */

public final class GradebookColumnCalculationValidator
    extends net.okapia.osid.jamocha.builder.validator.grading.calculation.gradebookcolumncalculation.spi.AbstractGradebookColumnCalculationValidator {


    /**
     *  Constructs a new <code>GradebookColumnCalculationValidator</code>.
     */

    public GradebookColumnCalculationValidator() {
        return;
    }


    /**
     *  Constructs a new <code>GradebookColumnCalculationValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public GradebookColumnCalculationValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a GradebookColumnCalculation with a default validation.
     *
     *  @param gradebookColumnCalculation a gradebook column calculation to validate
     *  @return the gradebook column calculation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculation</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.grading.calculation.GradebookColumnCalculation validateGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        GradebookColumnCalculationValidator validator = new GradebookColumnCalculationValidator();
        validator.validate(gradebookColumnCalculation);
        return (gradebookColumnCalculation);
    }


    /**
     *  Validates a GradebookColumnCalculation for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param gradebookColumnCalculation a gradebook column calculation to validate
     *  @return the gradebook column calculation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>gradebookColumnCalculation</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.grading.calculation.GradebookColumnCalculation validateGradebookColumnCalculation(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {

        GradebookColumnCalculationValidator validator = new GradebookColumnCalculationValidator(validation);
        validator.validate(gradebookColumnCalculation);
        return (gradebookColumnCalculation);
    }
}
