//
// AbstractStep.java
//
//     Defines a Step builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.step.spi;


/**
 *  Defines a <code>Step</code> builder.
 */

public abstract class AbstractStepBuilder<T extends AbstractStepBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.workflow.step.StepMiter step;


    /**
     *  Constructs a new <code>AbstractStepBuilder</code>.
     *
     *  @param step the step to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractStepBuilder(net.okapia.osid.jamocha.builder.workflow.step.StepMiter step) {
        super(step);
        this.step = step;
        return;
    }


    /**
     *  Builds the step.
     *
     *  @return the new step
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.workflow.Step build() {
        (new net.okapia.osid.jamocha.builder.validator.workflow.step.StepValidator(getValidations())).validate(this.step);
        return (new net.okapia.osid.jamocha.builder.workflow.step.ImmutableStep(this.step));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the step miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.workflow.step.StepMiter getMiter() {
        return (this.step);
    }


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>process</code> is <code>null</code>
     */

    public T process(org.osid.workflow.Process process) {
        getMiter().setProcess(process);
        return (self());
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().addResource(resource);
        return (self());
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resources</code>
     *          is <code>null</code>
     */

    public T resources(java.util.Collection<org.osid.resource.Resource> resources) {
        getMiter().setResources(resources);
        return (self());
    }


    /**
     *  Adds an input state.
     *
     *  @param state an input state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T inputState(org.osid.process.State state) {
        getMiter().addInputState(state);
        return (self());
    }


    /**
     *  Sets all the input states.
     *
     *  @param states a collection of input states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public T inputStates(java.util.Collection<org.osid.process.State> states) {
        getMiter().setInputStates(states);
        return (self());
    }


    /**
     *  Sets the next state.
     *
     *  @param state a next state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T nextState(org.osid.process.State state) {
        getMiter().setNextState(state);
        return (self());
    }


    /**
     *  Adds a Step record.
     *
     *  @param record a step record
     *  @param recordType the type of step record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.workflow.records.StepRecord record, org.osid.type.Type recordType) {
        getMiter().addStepRecord(record, recordType);
        return (self());
    }
}       


