//
// AbstractBudgetQueryInspector.java
//
//     A template for making a BudgetQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for budgets.
 */

public abstract class AbstractBudgetQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.financials.budgeting.BudgetQueryInspector {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Gets the budget entry <code> Id </code> query terms. 
     *
     *  @return the budget entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBudgetEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the budget entry query terms. 
     *
     *  @return the budget entry query terms 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQueryInspector[] getBudgetEntryTerms() {
        return (new org.osid.financials.budgeting.BudgetEntryQueryInspector[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given budget query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a budget implementing the requested record.
     *
     *  @param budgetRecordType a budget record type
     *  @return the budget query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetQueryInspectorRecord getBudgetQueryInspectorRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget query. 
     *
     *  @param budgetQueryInspectorRecord budget query inspector
     *         record
     *  @param budgetRecordType budget record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBudgetQueryInspectorRecord(org.osid.financials.budgeting.records.BudgetQueryInspectorRecord budgetQueryInspectorRecord, 
                                                   org.osid.type.Type budgetRecordType) {

        addRecordType(budgetRecordType);
        nullarg(budgetRecordType, "budget record type");
        this.records.add(budgetQueryInspectorRecord);        
        return;
    }
}
