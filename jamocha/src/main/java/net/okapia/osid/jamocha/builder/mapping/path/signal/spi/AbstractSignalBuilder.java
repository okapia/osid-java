//
// AbstractSignal.java
//
//     Defines a Signal builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.signal.spi;


/**
 *  Defines a <code>Signal</code> builder.
 */

public abstract class AbstractSignalBuilder<T extends AbstractSignalBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.path.signal.SignalMiter signal;


    /**
     *  Constructs a new <code>AbstractSignalBuilder</code>.
     *
     *  @param signal the signal to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSignalBuilder(net.okapia.osid.jamocha.builder.mapping.path.signal.SignalMiter signal) {
        super(signal);
        this.signal = signal;
        return;
    }


    /**
     *  Builds the signal.
     *
     *  @return the new signal
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.path.Signal build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.path.signal.SignalValidator(getValidations())).validate(this.signal);
        return (new net.okapia.osid.jamocha.builder.mapping.path.signal.ImmutableSignal(this.signal));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the signal miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.path.signal.SignalMiter getMiter() {
        return (this.signal);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(org.osid.mapping.path.Path path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public T coordinate(org.osid.mapping.Coordinate coordinate) {
        getMiter().setCoordinate(coordinate);
        return (self());
    }


    /**
     *  Adds a state.
     *
     *  @param state a state
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public T state(org.osid.process.State state) {
        getMiter().addState(state);
        return (self());
    }


    /**
     *  Sets all the states.
     *
     *  @param states a collection of states
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    public T states(java.util.Collection<org.osid.process.State> states) {
        getMiter().setStates(states);
        return (self());
    }


    /**
     *  Adds a Signal record.
     *
     *  @param record a signal record
     *  @param recordType the type of signal record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.path.records.SignalRecord record, org.osid.type.Type recordType) {
        getMiter().addSignalRecord(record, recordType);
        return (self());
    }
}       


