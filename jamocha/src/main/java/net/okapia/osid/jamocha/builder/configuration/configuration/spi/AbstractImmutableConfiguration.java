//
// AbstractImmutableConfiguration.java
//
//     Wraps a mutable Configuration to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Configuration</code> to hide modifiers. This
 *  wrapper provides an immutized Configuration from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying configuration whose state changes are visible.
 */

public abstract class AbstractImmutableConfiguration
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.configuration.Configuration {

    private final org.osid.configuration.Configuration configuration;


    /**
     *  Constructs a new <code>AbstractImmutableConfiguration</code>.
     *
     *  @param configuration the configuration to immutablize
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableConfiguration(org.osid.configuration.Configuration configuration) {
        super(configuration);
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this configuration is a parameter registry. A parameter 
     *  registry contains parameter definitions with no values. 
     *
     *  @return <code> true </code> if this is a registry, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isRegistry() {
        return (this.configuration.isRegistry());
    }


    /**
     *  Gets the configuration record corresponding to the given <code> 
     *  Configuration </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  configurationRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(configurationRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  configurationRecordType the type of configuration record to 
     *          retrieve 
     *  @return the configuration record 
     *  @throws org.osid.NullArgumentException <code> configurationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(configurationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.records.ConfigurationRecord getConfigurationRecord(org.osid.type.Type configurationRecordType)
        throws org.osid.OperationFailedException {

        return (this.configuration.getConfigurationRecord(configurationRecordType));
    }
}

