//
// AbstractFederatingCatalogueLookupSession.java
//
//     An abstract federating adapter for a CatalogueLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CatalogueLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCatalogueLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.offering.CatalogueLookupSession>
    implements org.osid.offering.CatalogueLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingCatalogueLookupSession</code>.
     */

    protected AbstractFederatingCatalogueLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.offering.CatalogueLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Catalogue</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCatalogues() {
        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            if (session.canLookupCatalogues()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Catalogue</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogueView() {
        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            session.useComparativeCatalogueView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Catalogue</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogueView() {
        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            session.usePlenaryCatalogueView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Catalogue</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Catalogue</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Catalogue</code> and
     *  retained for compatibility.
     *
     *  @param  catalogueId <code>Id</code> of the
     *          <code>Catalogue</code>
     *  @return the catalogue
     *  @throws org.osid.NotFoundException <code>catalogueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>catalogueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            try {
                return (session.getCatalogue(catalogueId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(catalogueId + " not found");
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogues specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Catalogues</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCatalogues()</code>.
     *
     *  @param  catalogueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByIds(org.osid.id.IdList catalogueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.offering.catalogue.MutableCatalogueList ret = new net.okapia.osid.jamocha.offering.catalogue.MutableCatalogueList();

        try (org.osid.id.IdList ids = catalogueIds) {
            while (ids.hasNext()) {
                ret.addCatalogue(getCatalogue(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  catalogue genus <code>Type</code> which does not include
     *  catalogues of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCatalogues()</code>.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList ret = getCatalogueList();

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            ret.addCatalogueList(session.getCataloguesByGenusType(catalogueGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogueList</code> corresponding to the given
     *  catalogue genus <code>Type</code> and include any additional
     *  catalogues with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCatalogues()</code>.
     *
     *  @param  catalogueGenusType a catalogue genus type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByParentGenusType(org.osid.type.Type catalogueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList ret = getCatalogueList();

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            ret.addCatalogueList(session.getCataloguesByParentGenusType(catalogueGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogueList</code> containing the given
     *  catalogue record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCatalogues()</code>.
     *
     *  @param  catalogueRecordType a catalogue record type 
     *  @return the returned <code>Catalogue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByRecordType(org.osid.type.Type catalogueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList ret = getCatalogueList();

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            ret.addCatalogueList(session.getCataloguesByRecordType(catalogueRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CatalogueList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known catalogues or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  catalogues that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Catalogue</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCataloguesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList ret = getCatalogueList();

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            ret.addCatalogueList(session.getCataloguesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Catalogues</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalogues or an error results. Otherwise, the returned list
     *  may contain only those catalogues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Catalogues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList ret = getCatalogueList();

        for (org.osid.offering.CatalogueLookupSession session : getSessions()) {
            ret.addCatalogueList(session.getCatalogues());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.offering.catalogue.FederatingCatalogueList getCatalogueList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.catalogue.ParallelCatalogueList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.offering.catalogue.CompositeCatalogueList());
        }
    }
}
