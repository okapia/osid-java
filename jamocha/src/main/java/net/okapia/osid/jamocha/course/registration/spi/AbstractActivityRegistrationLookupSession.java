//
// AbstractActivityRegistrationLookupSession.java
//
//    A starter implementation framework for providing an ActivityRegistration
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an ActivityRegistration
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivityRegistrations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityRegistrationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActivityRegistration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityRegistrations() {
        return (true);
    }


    /**
     *  A complete view of the <code>ActivityRegistration</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityRegistrationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ActivityRegistration</code>
     *  returns is desired.  Methods will return what is requested or
     *  result in an error. This view is used when greater precision
     *  is desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityRegistrationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity registrations in course catalogs
     *  which are children of this course catalog in the course
     *  catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only activity registrations whose effective dates are current
     *  are returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveActivityRegistrationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activity registrations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveActivityRegistrationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ActivityRegistration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityRegistration</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ActivityRegistration</code> and retained for
     *  compatibility.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityRegistrationId <code>Id</code> of the
     *          <code>ActivityRegistration</code>
     *  @return the activity registration
     *  @throws org.osid.NotFoundException <code>activityRegistrationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityRegistrationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistration getActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.registration.ActivityRegistrationList activityRegistrations = getActivityRegistrations()) {
            while (activityRegistrations.hasNext()) {
                org.osid.course.registration.ActivityRegistration activityRegistration = activityRegistrations.getNextActivityRegistration();
                if (activityRegistration.getId().equals(activityRegistrationId)) {
                    return (activityRegistration);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityRegistrationId + " not found");
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  activityRegistrations specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>ActivityRegistrations</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivityRegistrations()</code>.
     *
     *  @param  activityRegistrationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByIds(org.osid.id.IdList activityRegistrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.ActivityRegistration> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityRegistrationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivityRegistration(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity registration " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.activityregistration.LinkedActivityRegistrationList(ret));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given activity registration genus <code>Type</code> which
     *  does not include activity registrations of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivityRegistrations()</code>.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationGenusFilterList(getActivityRegistrations(), activityRegistrationGenusType));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given activity registration genus <code>Type</code> and
     *  include any additional activity registrations with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityRegistrations()</code>.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByParentGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivityRegistrationsByGenusType(activityRegistrationGenusType));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> containing the
     *  given activity registration record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityRegistrations()</code>.
     *
     *  @param  activityRegistrationRecordType an activityRegistration record type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByRecordType(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationRecordFilterList(getActivityRegistrations(), activityRegistrationRecordType));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *  
     *  In active mode, activity registrations are returned that are
     *  currently active. In any status mode, active and inactive
     *  activity registrations are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ActivityRegistration</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrations(), from, to));
    }
        

    /**
     *  Gets all <code>ActivityRegistrations</code> for a
     *  registration.
     *  
     *  In plenary mode, the returned list contains all known activity 
     *  registrations or an error results. Otherwise, the returned list may 
     *  contain only those registrations that are accessible through this 
     *  session. 
     *  
     *  In effective mode, activity registrations are returned that are 
     *  currently effective. In any effective mode, effective activity 
     *  registrations and those currently expired are returned. 
     *
     *  @param  registrationId a registration <code>Id</code> 
     *  @return a list of <code> ActivityRegistrations </code> 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilterList(new RegistrationFilter(registrationId), getActivityRegistrations()));
    }


    /**
     *  Gets a list of activity registrations corresponding to a
     *  registration <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  registrationId the <code>Id</code> of the registration
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>registrationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistrationOnDate(org.osid.id.Id registrationId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForRegistration(registrationId), from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  activity <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivity(org.osid.id.Id activityId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilterList(new ActivityFilter(activityId), getActivityRegistrations()));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  activity <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityOnDate(org.osid.id.Id activityId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForActivity(activityId), from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilterList(new StudentFilter(resourceId), getActivityRegistrations()));
    }


    /**
     *  Gets a list of activity registrations corresponding to a
     *  student <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  ativity registrations and those currently expired are
     *  returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                                          org.osid.calendaring.DateTime from,
                                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  activity and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudent(org.osid.id.Id activityId,
                                                                                                               org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilterList(new StudentFilter(resourceId), getActivityRegistrationsForActivity(activityId)));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  activity and student <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudentOnDate(org.osid.id.Id activityId,
                                                                                                                     org.osid.id.Id resourceId,
                                                                                                                     org.osid.calendaring.DateTime from,
                                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForActivityAndStudent(activityId, resourceId), from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to a
     *  course offering <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.ActivityRegistration> ret = new java.util.ArrayList<>();

        try (org.osid.course.registration.ActivityRegistrationList registrations = getActivityRegistrations()) {
            while (registrations.hasNext()) {
                org.osid.course.registration.ActivityRegistration registration = registrations.getNextActivityRegistration();
                org.osid.course.Activity activity = registration.getActivity();
                if (activity.getCourseOfferingId().equals(courseOfferingId)) {
                    ret.add(registration);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.activityregistration.LinkedActivityRegistrationList(ret));
    }


    /**
     *  Gets a list of activity registrations corresponding to an
     *  course offering <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForCourseOffering(courseOfferingId), from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  course offering and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code>, <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                                     org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilterList(new StudentFilter(resourceId), getActivityRegistrationsForCourseOffering(courseOfferingId)));
    }


    /**
     *  Gets a list of activity registrations corresponding to course
     *  offering and student <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                                     org.osid.id.Id resourceId,
                                                                                                                     org.osid.calendaring.DateTime from,
                                                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.TemporalActivityRegistrationFilterList(getActivityRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId), from, to));
    }

    
    /**
     *  Gets all <code>ActivityRegistrations</code>. 
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registratins and those currently expired are
     *  returned.
     *
     *  @return a list of <code>ActivityRegistrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.registration.ActivityRegistrationList getActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity registration list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activity registrations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.registration.ActivityRegistrationList filterActivityRegistrationsOnViews(org.osid.course.registration.ActivityRegistrationList list)
        throws org.osid.OperationFailedException {

        org.osid.course.registration.ActivityRegistrationList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.EffectiveActivityRegistrationFilterList(ret);
        }

        return (ret);
    }


    public static class RegistrationFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilter {
         
        private final org.osid.id.Id registrationId;
         
         
        /**
         *  Constructs a new <code>RegistrationFilter</code>.
         *
         *  @param registrationId the registration to filter
         *  @throws org.osid.NullArgumentException
         *          <code>registrationId</code> is <code>null</code>
         */
        
        public RegistrationFilter(org.osid.id.Id registrationId) {
            nullarg(registrationId, "registration Id");
            this.registrationId = registrationId;
            return;
        }

         
        /**
         *  Used by the ActivityRegistrationFilterList to filter the 
         *  activity registration list based on registration.
         *
         *  @param activityRegistration the activity registration
         *  @return <code>true</code> to pass the activity registration,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.ActivityRegistration activityRegistration) {
            return (activityRegistration.getRegistrationId().equals(this.registrationId));
        }
    }


    public static class ActivityFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilter {
         
        private final org.osid.id.Id activityId;
         
         
        /**
         *  Constructs a new <code>ActivityFilter</code>.
         *
         *  @param activityId the activity to filter
         *  @throws org.osid.NullArgumentException
         *          <code>activityId</code> is <code>null</code>
         */
        
        public ActivityFilter(org.osid.id.Id activityId) {
            nullarg(activityId, "activity Id");
            this.activityId = activityId;
            return;
        }

         
        /**
         *  Used by the ActivityRegistrationFilterList to filter the 
         *  activity registration list based on activity.
         *
         *  @param activityRegistration the activity registration
         *  @return <code>true</code> to pass the activity registration,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.ActivityRegistration activityRegistration) {
            return (activityRegistration.getActivityId().equals(this.activityId));
        }
    }


    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.ActivityRegistrationFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ActivityRegistrationFilterList to filter the 
         *  activity registration list based on student.
         *
         *  @param activityRegistration the activity registration
         *  @return <code>true</code> to pass the activity registration,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.ActivityRegistration activityRegistration) {
            return (activityRegistration.getStudentId().equals(this.resourceId));
        }
    }
}
