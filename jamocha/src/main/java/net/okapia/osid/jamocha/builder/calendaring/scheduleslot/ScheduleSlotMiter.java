//
// ScheduleSlotMiter.java
//
//     Defines a ScheduleSlot miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.scheduleslot;


/**
 *  Defines a <code>ScheduleSlot</code> miter for use with the builders.
 */

public interface ScheduleSlotMiter
    extends net.okapia.osid.jamocha.builder.spi.ContainableOsidObjectMiter,
            org.osid.calendaring.ScheduleSlot {


    /**
     *  Adds a schedule slot.
     *
     *  @param scheduleSlot a schedule slot
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlot</code> is <code>null</code>
     */

    public void addScheduleSlot(org.osid.calendaring.ScheduleSlot scheduleSlot);


    /**
     *  Sets all the schedule slots.
     *
     *  @param scheduleSlots a collection of schedule slots
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlots</code> is <code>null</code>
     */

    public void setScheduleSlots(java.util.Collection<org.osid.calendaring.ScheduleSlot> scheduleSlots);


    /**
     *  Adds a weekday.
     *
     *  @param weekday a weekday
     */

    public void addWeekday(long weekday);


    /**
     *  Sets all the weekdays.
     *
     *  @param weekdays a collection of weekdays
     *  @throws org.osid.NullArgumentException <code>weekdays</code>
     *          is <code>null</code>
     */

    public void setWeekdays(java.util.Collection<Long> weekdays);


    /**
     *  Sets the weekly interval.
     *
     *  @param interval a weekly interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    public void setWeeklyInterval(long interval);


    /**
     *  Sets the week of month.
     *
     *  @param weekOfMonth a week of month
     *  @throws org.osid.NullArgumentException
     *          <code>weekOfMonth</code> is <code>null</code>
     */

    public void setWeekOfMonth(long weekOfMonth);


    /**
     *  Sets the weekday time.
     *
     *  @param time a weekday time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setWeekdayTime(org.osid.calendaring.Time time);


    /**
     *  Sets the fixed interval.
     *
     *  @param interval a fixed interval
     *  @throws org.osid.NullArgumentException <code>interval</code>
     *          is <code>null</code>
     */

    public void setFixedInterval(org.osid.calendaring.Duration interval);


    /**
     *  Sets the duration.
     *
     *  @param duration the duration
     *  @throws org.osid.NullArgumentException <code>duration</code> is
     *          <code>null</code>
     */

    public void setDuration(org.osid.calendaring.Duration duration);


    /**
     *  Adds a ScheduleSlot record.
     *
     *  @param record a scheduleSlot record
     *  @param recordType the type of scheduleSlot record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addScheduleSlotRecord(org.osid.calendaring.records.ScheduleSlotRecord record, org.osid.type.Type recordType);
}       


