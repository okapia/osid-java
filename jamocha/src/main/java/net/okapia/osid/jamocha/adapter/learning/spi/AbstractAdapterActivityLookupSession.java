//
// AbstractAdapterActivityLookupSession.java
//
//    An Activity lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Activity lookup session adapter.
 */

public abstract class AbstractAdapterActivityLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.learning.ActivityLookupSession {

    private final org.osid.learning.ActivityLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActivityLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActivityLookupSession(org.osid.learning.ActivityLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code ObjectiveBank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code ObjectiveBank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the {@code ObjectiveBank} associated with this session.
     *
     *  @return the {@code ObjectiveBank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform {@code Activity} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (this.session.canLookupActivities());
    }


    /**
     *  A complete view of the {@code Activity} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        this.session.useComparativeActivityView();
        return;
    }


    /**
     *  A complete view of the {@code Activity} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        this.session.usePlenaryActivityView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    
     
    /**
     *  Gets the {@code Activity} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Activity} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Activity} and
     *  retained for compatibility.
     *
     *  @param activityId {@code Id} of the {@code Activity}
     *  @return the activity
     *  @throws org.osid.NotFoundException {@code activityId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code activityId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivity(activityId));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Activities} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  activityIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code activityIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByIds(activityIds));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  activity genus {@code Type} which does not include
     *  activities of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByGenusType(activityGenusType));
    }


    /**
     *  Gets an {@code ActivityList} corresponding to the given
     *  activity genus {@code Type} and include any additional
     *  activities with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByParentGenusType(activityGenusType));
    }


    /**
     *  Gets an {@code ActivityList} containing the given
     *  activity record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned {@code Activity} list
     *  @throws org.osid.NullArgumentException
     *          {@code activityRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivitiesByRecordType(activityRecordType));
    }


    /**
     *  Gets the activities for the given objective. In plenary mode,
     *  the returned list contains all of the activities mapped to the
     *  objective {@code Id} or an error results if an Id in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Activities} may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  @param  objectiveId {@code Id} of the {@code Objective} 
     *  @return list of enrollments 
     *  @throws org.osid.NotFoundException {@code objectiveId} not 
     *          found 
     *  @throws org.osid.NullArgumentException {@code objectiveId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivitiesForObjective(objectiveId));
    }


    /**
     *  Gets the activities for the given objectives. In plenary mode,
     *  the returned list contains all of the activities specified in
     *  the objective {@code Id} list, in the order of the list,
     *  including duplicates, or an error results if a course offering
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Activities} may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  objectiveIds list of objective {@code Ids} 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an {@code objectiveId} not 
     *          found 
     *  @throws org.osid.NullArgumentException {@code objectiveIdList} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivitiesForObjectives(objectiveIds));
    }


    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset {@code Id} or an error results if an {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Activities} may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  @param  assetId {@code Id} of an {@code Asset} 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException {@code assetId} not found 
     *  @throws org.osid.NullArgumentException {@code assetId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivitiesByAsset(assetId));
    }

    
    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset {@code Id} or an error results if an {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Activities} may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  @param  assetIds {@code Ids} of {@code Assets} 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an {@code assetId} not found 
     *  @throws org.osid.NullArgumentException {@code assetIdList} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAssets(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getActivitiesByAssets(assetIds));
    }

    
    /**
     *  Gets all {@code Activities}. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Activities} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActivities());
    }
}
