//
// AbstractAdapterPoolProcessorEnablerLookupSession.java
//
//    A PoolProcessorEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PoolProcessorEnabler lookup session adapter.
 */

public abstract class AbstractAdapterPoolProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {

    private final org.osid.provisioning.rules.PoolProcessorEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPoolProcessorEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPoolProcessorEnablerLookupSession(org.osid.provisioning.rules.PoolProcessorEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code PoolProcessorEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPoolProcessorEnablers() {
        return (this.session.canLookupPoolProcessorEnablers());
    }


    /**
     *  A complete view of the {@code PoolProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolProcessorEnablerView() {
        this.session.useComparativePoolProcessorEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code PoolProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolProcessorEnablerView() {
        this.session.usePlenaryPoolProcessorEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool processor enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolProcessorEnablerView() {
        this.session.useActivePoolProcessorEnablerView();
        return;
    }


    /**
     *  Active and inactive pool processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolProcessorEnablerView() {
        this.session.useAnyStatusPoolProcessorEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code PoolProcessorEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PoolProcessorEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PoolProcessorEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param poolProcessorEnablerId {@code Id} of the {@code PoolProcessorEnabler}
     *  @return the pool processor enabler
     *  @throws org.osid.NotFoundException {@code poolProcessorEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnabler getPoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnabler(poolProcessorEnablerId));
    }


    /**
     *  Gets a {@code PoolProcessorEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  poolProcessorEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PoolProcessorEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  poolProcessorEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PoolProcessorEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByIds(org.osid.id.IdList poolProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablersByIds(poolProcessorEnablerIds));
    }


    /**
     *  Gets a {@code PoolProcessorEnablerList} corresponding to the given
     *  pool processor enabler genus {@code Type} which does not include
     *  pool processor enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  poolProcessorEnablerGenusType a poolProcessorEnabler genus type 
     *  @return the returned {@code PoolProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByGenusType(org.osid.type.Type poolProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablersByGenusType(poolProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code PoolProcessorEnablerList} corresponding to the given
     *  pool processor enabler genus {@code Type} and include any additional
     *  pool processor enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  poolProcessorEnablerGenusType a poolProcessorEnabler genus type 
     *  @return the returned {@code PoolProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByParentGenusType(org.osid.type.Type poolProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablersByParentGenusType(poolProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code PoolProcessorEnablerList} containing the given
     *  pool processor enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  poolProcessorEnablerRecordType a poolProcessorEnabler record type 
     *  @return the returned {@code PoolProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersByRecordType(org.osid.type.Type poolProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablersByRecordType(poolProcessorEnablerRecordType));
    }


    /**
     *  Gets a {@code PoolProcessorEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code PoolProcessorEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PoolProcessorEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code PoolProcessorEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getPoolProcessorEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code PoolProcessorEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool processor enablers or an error results. Otherwise, the returned list
     *  may contain only those pool processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool processor enablers are returned that are currently
     *  active. In any status mode, active and inactive pool processor enablers
     *  are returned.
     *
     *  @return a list of {@code PoolProcessorEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolProcessorEnablers());
    }
}
