//
// AbstractIndexedMapShipmentLookupSession.java
//
//    A simple framework for providing a Shipment lookup service
//    backed by a fixed collection of shipments with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Shipment lookup service backed by a
 *  fixed collection of shipments. The shipments are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some shipments may be compatible
 *  with more types than are indicated through these shipment
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Shipments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapShipmentLookupSession
    extends AbstractMapShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inventory.shipment.Shipment> shipmentsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.shipment.Shipment>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.shipment.Shipment> shipmentsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.shipment.Shipment>());


    /**
     *  Makes a <code>Shipment</code> available in this session.
     *
     *  @param  shipment a shipment
     *  @throws org.osid.NullArgumentException <code>shipment<code> is
     *          <code>null</code>
     */

    @Override
    protected void putShipment(org.osid.inventory.shipment.Shipment shipment) {
        super.putShipment(shipment);

        this.shipmentsByGenus.put(shipment.getGenusType(), shipment);
        
        try (org.osid.type.TypeList types = shipment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.shipmentsByRecord.put(types.getNextType(), shipment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a shipment from this session.
     *
     *  @param shipmentId the <code>Id</code> of the shipment
     *  @throws org.osid.NullArgumentException <code>shipmentId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeShipment(org.osid.id.Id shipmentId) {
        org.osid.inventory.shipment.Shipment shipment;
        try {
            shipment = getShipment(shipmentId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.shipmentsByGenus.remove(shipment.getGenusType());

        try (org.osid.type.TypeList types = shipment.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.shipmentsByRecord.remove(types.getNextType(), shipment);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeShipment(shipmentId);
        return;
    }


    /**
     *  Gets a <code>ShipmentList</code> corresponding to the given
     *  shipment genus <code>Type</code> which does not include
     *  shipments of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known shipments or an error results. Otherwise,
     *  the returned list may contain only those shipments that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  shipmentGenusType a shipment genus type 
     *  @return the returned <code>Shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByGenusType(org.osid.type.Type shipmentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.shipment.shipment.ArrayShipmentList(this.shipmentsByGenus.get(shipmentGenusType)));
    }


    /**
     *  Gets a <code>ShipmentList</code> containing the given
     *  shipment record <code>Type</code>. In plenary mode, the
     *  returned list contains all known shipments or an error
     *  results. Otherwise, the returned list may contain only those
     *  shipments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return the returned <code>shipment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.shipment.ShipmentList getShipmentsByRecordType(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.shipment.shipment.ArrayShipmentList(this.shipmentsByRecord.get(shipmentRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.shipmentsByGenus.clear();
        this.shipmentsByRecord.clear();

        super.close();

        return;
    }
}
