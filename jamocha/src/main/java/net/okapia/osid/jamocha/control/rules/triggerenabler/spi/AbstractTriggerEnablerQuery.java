//
// AbstractTriggerEnablerQuery.java
//
//     A template for making a TriggerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.triggerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for trigger enablers.
 */

public abstract class AbstractTriggerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.control.rules.TriggerEnablerQuery {

    private final java.util.Collection<org.osid.control.rules.records.TriggerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the trigger. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledTriggerId(org.osid.id.Id triggerId, boolean match) {
        return;
    }


    /**
     *  Clears the trigger <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TriggerQuery </code> is available. 
     *
     *  @return <code> true </code> if a trigger query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledTriggerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a trigger. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the trigger query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledTriggerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuery getRuledTriggerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledTriggerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any trigger. 
     *
     *  @param  match <code> true </code> for enablers mapped to any trigger, 
     *          <code> false </code> to match enablers mapped to no triggers 
     */

    @OSID @Override
    public void matchAnyRuledTrigger(boolean match) {
        return;
    }


    /**
     *  Clears the trigger query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given trigger enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a trigger enabler implementing the requested record.
     *
     *  @param triggerEnablerRecordType a trigger enabler record type
     *  @return the trigger enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.TriggerEnablerQueryRecord getTriggerEnablerQueryRecord(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.TriggerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(triggerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger enabler query. 
     *
     *  @param triggerEnablerQueryRecord trigger enabler query record
     *  @param triggerEnablerRecordType triggerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerEnablerQueryRecord(org.osid.control.rules.records.TriggerEnablerQueryRecord triggerEnablerQueryRecord, 
                                          org.osid.type.Type triggerEnablerRecordType) {

        addRecordType(triggerEnablerRecordType);
        nullarg(triggerEnablerQueryRecord, "trigger enabler query record");
        this.records.add(triggerEnablerQueryRecord);        
        return;
    }
}
