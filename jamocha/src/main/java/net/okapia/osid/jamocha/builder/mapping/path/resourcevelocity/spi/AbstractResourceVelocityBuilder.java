//
// AbstractResourceVelocity.java
//
//     Defines a ResourceVelocity builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.spi;


/**
 *  Defines a <code>ResourceVelocity</code> builder.
 */

public abstract class AbstractResourceVelocityBuilder<T extends AbstractResourceVelocityBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCompendiumBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.ResourceVelocityMiter resourceVelocity;


    /**
     *  Constructs a new <code>AbstractResourceVelocityBuilder</code>.
     *
     *  @param resourceVelocity the resource velocity to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResourceVelocityBuilder(net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.ResourceVelocityMiter resourceVelocity) {
        super(resourceVelocity);
        this.resourceVelocity = resourceVelocity;
        return;
    }


    /**
     *  Builds the resource velocity.
     *
     *  @return the new resource velocity
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.path.ResourceVelocity build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.path.resourcevelocity.ResourceVelocityValidator(getValidations())).validate(this.resourceVelocity);
        return (new net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.ImmutableResourceVelocity(this.resourceVelocity));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the resource velocity miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.ResourceVelocityMiter getMiter() {
        return (this.resourceVelocity);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the speed.
     *
     *  @param speed the speed
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    public T speed(org.osid.mapping.Speed speed) {
        getMiter().setSpeed(speed);
        return (self());
    }


    /**
     *  Sets the coordinate position.
     *
     *  @param coordinate the position
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public T position(org.osid.mapping.Coordinate coordinate) {
        getMiter().setPosition(coordinate);
        return (self());
    }


    /**
     *  Sets the heading.
     *
     *  @param heading the heading
     *  @throws org.osid.NullArgumentException <code>heading</code>
     *          is <code>null</code>
     */

    public T heading(org.osid.mapping.Heading heading) {
        getMiter().setHeading(heading);
        return (self());
    }


    /**
     *  Sets the path.
     *
     *  @param path the path
     *  @throws org.osid.NullArgumentException <code>path</code>
     *          is <code>null</code>
     */

    public T path(org.osid.mapping.path.Path path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Adds a ResourceVelocity record.
     *
     *  @param record a resource velocity record
     *  @param recordType the type of resourceVelocity record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.path.records.ResourceVelocityRecord record, org.osid.type.Type recordType) {
        getMiter().addResourceVelocityRecord(record, recordType);
        return (self());
    }
}       


