//
// AbstractMapItemLookupSession
//
//    A simple framework for providing an Item lookup service
//    backed by a fixed collection of items.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Item lookup service backed by a
 *  fixed collection of items. The items are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Items</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapItemLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractItemLookupSession
    implements org.osid.assessment.ItemLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.Item> items = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.Item>());


    /**
     *  Makes an <code>Item</code> available in this session.
     *
     *  @param  item an item
     *  @throws org.osid.NullArgumentException <code>item<code>
     *          is <code>null</code>
     */

    protected void putItem(org.osid.assessment.Item item) {
        this.items.put(item.getId(), item);
        return;
    }


    /**
     *  Makes an array of items available in this session.
     *
     *  @param  items an array of items
     *  @throws org.osid.NullArgumentException <code>items<code>
     *          is <code>null</code>
     */

    protected void putItems(org.osid.assessment.Item[] items) {
        putItems(java.util.Arrays.asList(items));
        return;
    }


    /**
     *  Makes a collection of items available in this session.
     *
     *  @param  items a collection of items
     *  @throws org.osid.NullArgumentException <code>items<code>
     *          is <code>null</code>
     */

    protected void putItems(java.util.Collection<? extends org.osid.assessment.Item> items) {
        for (org.osid.assessment.Item item : items) {
            this.items.put(item.getId(), item);
        }

        return;
    }


    /**
     *  Removes an Item from this session.
     *
     *  @param  itemId the <code>Id</code> of the item
     *  @throws org.osid.NullArgumentException <code>itemId<code> is
     *          <code>null</code>
     */

    protected void removeItem(org.osid.id.Id itemId) {
        this.items.remove(itemId);
        return;
    }


    /**
     *  Gets the <code>Item</code> specified by its <code>Id</code>.
     *
     *  @param  itemId <code>Id</code> of the <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>itemId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.Item item = this.items.get(itemId);
        if (item == null) {
            throw new org.osid.NotFoundException("item not found: " + itemId);
        }

        return (item);
    }


    /**
     *  Gets all <code>Items</code>. In plenary mode, the returned
     *  list contains all known items or an error
     *  results. Otherwise, the returned list may contain only those
     *  items that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.item.ArrayItemList(this.items.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.items.clear();
        super.close();
        return;
    }
}
