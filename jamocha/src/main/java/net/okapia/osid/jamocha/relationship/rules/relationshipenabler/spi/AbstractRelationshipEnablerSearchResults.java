//
// AbstractRelationshipEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRelationshipEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.relationship.rules.RelationshipEnablerSearchResults {

    private org.osid.relationship.rules.RelationshipEnablerList relationshipEnablers;
    private final org.osid.relationship.rules.RelationshipEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRelationshipEnablerSearchResults.
     *
     *  @param relationshipEnablers the result set
     *  @param relationshipEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>relationshipEnablers</code>
     *          or <code>relationshipEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRelationshipEnablerSearchResults(org.osid.relationship.rules.RelationshipEnablerList relationshipEnablers,
                                            org.osid.relationship.rules.RelationshipEnablerQueryInspector relationshipEnablerQueryInspector) {
        nullarg(relationshipEnablers, "relationship enablers");
        nullarg(relationshipEnablerQueryInspector, "relationship enabler query inspectpr");

        this.relationshipEnablers = relationshipEnablers;
        this.inspector = relationshipEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the relationship enabler list resulting from a search.
     *
     *  @return a relationship enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablers() {
        if (this.relationshipEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.relationship.rules.RelationshipEnablerList relationshipEnablers = this.relationshipEnablers;
        this.relationshipEnablers = null;
	return (relationshipEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.relationship.rules.RelationshipEnablerQueryInspector getRelationshipEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  relationship enabler search record <code> Type. </code> This method must
     *  be used to retrieve a relationshipEnabler implementing the requested
     *  record.
     *
     *  @param relationshipEnablerSearchRecordType a relationshipEnabler search 
     *         record type 
     *  @return the relationship enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(relationshipEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerSearchResultsRecord getRelationshipEnablerSearchResultsRecord(org.osid.type.Type relationshipEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.relationship.rules.records.RelationshipEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(relationshipEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(relationshipEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record relationship enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRelationshipEnablerRecord(org.osid.relationship.rules.records.RelationshipEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "relationship enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
