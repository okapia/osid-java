//
// ScheduleSlotElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.scheduleslot.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ScheduleSlotElements
    extends net.okapia.osid.jamocha.spi.ContainableOsidObjectElements {


    /**
     *  Gets the ScheduleSlotElement Id.
     *
     *  @return the schedule slot element Id
     */

    public static org.osid.id.Id getScheduleSlotEntityId() {
        return (makeEntityId("osid.calendaring.ScheduleSlot"));
    }


    /**
     *  Gets the ScheduleSlotIds element Id.
     *
     *  @return the ScheduleSlotIds element Id
     */

    public static org.osid.id.Id getScheduleSlotIds() {
        return (makeElementId("osid.calendaring.scheduleslot.ScheduleSlotIds"));
    }


    /**
     *  Gets the ScheduleSlots element Id.
     *
     *  @return the ScheduleSlots element Id
     */

    public static org.osid.id.Id getScheduleSlots() {
        return (makeElementId("osid.calendaring.scheduleslot.ScheduleSlots"));
    }


    /**
     *  Gets the Weekdays element Id.
     *
     *  @return the Weekdays element Id
     */

    public static org.osid.id.Id getWeekdays() {
        return (makeElementId("osid.calendaring.scheduleslot.Weekdays"));
    }


    /**
     *  Gets the WeeklyInterval element Id.
     *
     *  @return the WeeklyInterval element Id
     */

    public static org.osid.id.Id getWeeklyInterval() {
        return (makeElementId("osid.calendaring.scheduleslot.WeeklyInterval"));
    }


    /**
     *  Gets the WeekOfMonth element Id.
     *
     *  @return the WeekOfMonth element Id
     */

    public static org.osid.id.Id getWeekOfMonth() {
        return (makeElementId("osid.calendaring.scheduleslot.WeekOfMonth"));
    }


    /**
     *  Gets the WeekdayTime element Id.
     *
     *  @return the WeekdayTime element Id
     */

    public static org.osid.id.Id getWeekdayTime() {
        return (makeElementId("osid.calendaring.scheduleslot.WeekdayTime"));
    }


    /**
     *  Gets the FixedInterval element Id.
     *
     *  @return the FixedInterval element Id
     */

    public static org.osid.id.Id getFixedInterval() {
        return (makeElementId("osid.calendaring.scheduleslot.FixedInterval"));
    }


    /**
     *  Gets the Duration element Id.
     *
     *  @return the Duration element Id
     */

    public static org.osid.id.Id getDuration() {
        return (makeElementId("osid.calendaring.scheduleslot.Duration"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.scheduleslot.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.scheduleslot.Calendar"));
    }


    /**
     *  Gets the WeekdayStart element Id.
     *
     *  @return the WeekdayStart element Id
     */

    public static org.osid.id.Id getWeekdayStart() {
        return (makeSearchOrderElementId("osid.calendaring.scheduleslot.WeekdayStart"));
    }
}
