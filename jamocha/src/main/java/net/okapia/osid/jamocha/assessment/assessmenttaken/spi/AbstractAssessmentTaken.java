//
// AbstractAssessmentTaken.java
//
//     Defines an AssessmentTaken.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentTaken</code>.
 */

public abstract class AbstractAssessmentTaken
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.assessment.AssessmentTaken {

    private org.osid.assessment.AssessmentOffered assessmentOffered;
    private org.osid.resource.Resource taker;
    private org.osid.authentication.Agent takingAgent;
    private org.osid.calendaring.DateTime startTime;
    private org.osid.calendaring.DateTime endTime;
    private org.osid.calendaring.Duration timeSpent;
    private long completion;
    private org.osid.grading.GradeSystem scoreSystem;
    private java.math.BigDecimal score;
    private org.osid.grading.Grade grade;
    private org.osid.locale.DisplayText feedback;
    private org.osid.assessment.AssessmentTaken rubric;

    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code>
     *  AssessmentOffered. </code>
     *
     *  @return the assessment offered <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentOfferedId() {
        return (this.assessmentOffered.getId());
    }


    /**
     *  Gets the <code> AssessmentOffered. </code> 
     *
     *  @return the assessment offered 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered()
        throws org.osid.OperationFailedException {

        return (this.assessmentOffered);
    }


    /**
     *  Sets the assessment offered.
     *
     *  @param assessmentOffered an assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    protected void setAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        nullarg(assessmentOffered, "assessment offered");
        this.assessmentOffered = assessmentOffered;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource who took or is taking this 
     *  assessment. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTakerId() {
        return (this.taker.getId());
    }


    /**
     *  Gets the <code> Resource </code> taking this assessment. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getTaker()
        throws org.osid.OperationFailedException {

        return (this.taker);
    }


    /**
     *  Sets the taker.
     *
     *  @param taker a taker
     *  @throws org.osid.NullArgumentException <code>taker</code> is
     *          <code>null</code>
     */

    protected void setTaker(org.osid.resource.Resource taker) {
        nullarg(taker, "taker");
        this.taker = taker;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> who
     *  took or is taking the assessment.
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTakingAgentId() {
        return (this.takingAgent.getId());
    }


    /**
     *  Gets the <code> Agent. </code> 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getTakingAgent()
        throws org.osid.OperationFailedException {

        return (this.takingAgent);
    }


    /**
     *  Sets the taking agent.
     *
     *  @param agent the taking agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setTakingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "taking agent");
        this.takingAgent = agent;
        return;
    }


    /**
     *  Tests if this assessment has begun. 
     *
     *  @return <code> true </code> if the assessment has begun,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasStarted() {
        return (this.startTime != null);
    }


    /**
     *  Gets the time this assessment was started. 
     *
     *  @return the start time 
     *  @throws org.osid.IllegalStateException <code> hasStarted()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getActualStartTime() {
        if (!hasStarted()) {
            throw new org.osid.IllegalStateException("hasStarted() is false");
        }

        return (this.startTime);
    }


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setActualStartTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "start time");
        this.startTime = time;
        return;
    }


    /**
     *  Tests if this assessment has ended. 
     *
     *  @return <code> true </code> if the assessment has ended, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasEnded() {
        return (this.endTime != null);
    }


    /**
     *  Gets the time of this assessment was completed.
     *
     *  @return the end time 
     *  @throws org.osid.IllegalStateException <code> hasEnded()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCompletionTime() {
        if (!hasEnded()) {
            throw new org.osid.IllegalStateException("hasEnded() is false");
        }

        return (this.endTime);
    }


    /**
     *  Sets the completion time.
     *
     *  @param time a completion time
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    protected void setCompletionTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "end time");
        this.endTime = time;
        return;
    }


    /**
     *  Gets the total time spent taking this assessment. 
     *
     *  @return the total time spent 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeSpent() {
        return (this.timeSpent);
    }


    /**
     *  Sets the time spent.
     *
     *  @param timeSpent a time spent
     *  @throws org.osid.NullArgumentException
     *          <code>timeSpent</code> is <code>null</code>
     */

    protected void setTimeSpent(org.osid.calendaring.Duration timeSpent) {
        nullarg(timeSpent, "time spent");
        this.timeSpent = timeSpent;
        return;
    }


    /**
     *  Gets a completion percentage of the assessment. 
     *
     *  @return the percent complete (0-100) 
     */

    @OSID @Override
    public long getCompletion() {
        return (this.completion);
    }


    /**
     *  Sets the completion.
     *
     *  @param completion a completion
     *  @throws org.osid.InavlidArgumentException
     *          <code>completion</code> is out of range
     */

    protected void setCompletion(long completion) {
        cardinalarg(completion, "completion");
        if (completion > 100) {
            throw new org.osid.InvalidArgumentException("completion > 100");
        }

        this.completion = completion;
        return;
    }


    /**
     *  Tests if a score is available for this assessment. 
     *
     *  @return <code> true </code> if a score is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isScored() {
        return ((this.scoreSystem != null) && (this.score != null));
    }


    /**
     *  Gets a score system <code> Id </code> for the assessment. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreSystemId() {
        if (!isScored()) {
            throw new org.osid.IllegalStateException("isScored() is false");
        }

        return (this.scoreSystem.getId());
    }


    /**
     *  Gets a grade system for the score. 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> isScored() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreSystem()
        throws org.osid.OperationFailedException {

        if (!isScored()) {
            throw new org.osid.IllegalStateException("isScored() is false");
        }

        return (this.scoreSystem);
    }


    /**
     *  Sets the score system.
     *
     *  @param system a score system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    protected void setScoreSystem(org.osid.grading.GradeSystem system) {
        nullarg(system, "score system");
        this.scoreSystem = system;
        return;
    }


    /**
     *  Gets a score for the assessment. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> isScored()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        if (!isScored()) {
            throw new org.osid.IllegalStateException("isScored() is false");
        }

        return (this.score);
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    protected void setScore(java.math.BigDecimal score) {
        nullarg(score, "score");
        this.score = score;
        return;
    }


    /**
     *  Tests if a grade is available for this assessment. 
     *
     *  @return <code> true </code> if a grade is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.grade != null);
    }


    /**
     *  Gets a grade <code> Id </code> for the assessment. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade.getId());
    }


    /**
     *  Gets a grade for the assessment. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade);
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    protected void setGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");
        this.grade = grade;
        return;
    }


    /**
     *  Gets any overall feedback available for this assessment by the
     *  grader.
     *
     *  @return feedback 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getFeedback() {
        return (this.feedback);
    }


    /**
     *  Sets the feedback.
     *
     *  @param feedback feedback
     *  @throws org.osid.NullArgumentException <code>feedback</code>
     *          is <code>null</code>
     */

    protected void setFeedback(org.osid.locale.DisplayText feedback) {
        nullarg(feedback, "feedback");
        this.feedback = feedback;
        return;
    }


    /**
     *  Tests if a rubric assessment taken is associated with this
     *  assessment taken.
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        if (this.rubric == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment taken <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {
        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric.getId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment  taken
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getRubric() {
        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric);
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessmentTaken the rubric assessmentTaken
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    protected void setRubric(org.osid.assessment.AssessmentTaken assessmentTaken) {
        nullarg(assessmentTaken, "rubric assessment taken");
        this.rubric = assessmentTaken;
        return;
    }


    /**
     *  Tests if this assessment taken supports the given record
     *  <code>Type</code>.
     *
     *  @param assessmentTakenRecordType an assessment taken record
     *         type
     *  @return <code>true</code> if the assessmentTakenRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentTakenRecordType) {
        for (org.osid.assessment.records.AssessmentTakenRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentTaken</code> record <code>Type</code>.
     *
     *  @param assessmentTakenRecordType the assessment taken record
     *         type
     *  @return the assessment taken record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable
     *          to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenRecord getAssessmentTakenRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment taken. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentTakenRecord the assessment taken record
     *  @param assessmentTakenRecordType assessment taken record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecord</code> or
     *          <code>assessmentTakenRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentTakenRecord(org.osid.assessment.records.AssessmentTakenRecord assessmentTakenRecord, 
                                            org.osid.type.Type assessmentTakenRecordType) {

        nullarg(assessmentTakenRecord, "assessment taken record");
        addRecordType(assessmentTakenRecordType);
        this.records.add(assessmentTakenRecord);
        
        return;
    }
}
