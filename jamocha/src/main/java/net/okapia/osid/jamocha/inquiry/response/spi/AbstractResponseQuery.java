//
// AbstractResponseQuery.java
//
//     A template for making a Response Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.response.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for responses.
 */

public abstract class AbstractResponseQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.inquiry.ResponseQuery {

    private final java.util.Collection<org.osid.inquiry.records.ResponseQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the inquiry <code> Id </code> for this query. 
     *
     *  @param  inquiryId the inquiry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquiryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquiryId(org.osid.id.Id inquiryId, boolean match) {
        return;
    }


    /**
     *  Clears the inquiry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquiryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquiryQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquiry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquiry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquiry query 
     *  @throws org.osid.UnimplementedException <code> supportsInquiryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQuery getInquiryQuery() {
        throw new org.osid.UnimplementedException("supportsInquiryQuery() is false");
    }


    /**
     *  Clears the inquiry query terms. 
     */

    @OSID @Override
    public void clearInquiryTerms() {
        return;
    }


    /**
     *  Sets the responder resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResponderId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the responder resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResponderIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResponderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a responder resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResponderQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResponderQuery() {
        throw new org.osid.UnimplementedException("supportsResponderQuery() is false");
    }


    /**
     *  Clears the responder resource query terms. 
     */

    @OSID @Override
    public void clearResponderTerms() {
        return;
    }


    /**
     *  Sets the responding agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRespondingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the responding agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRespondingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRespondingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a responding agent. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRespondingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getRespondingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsRespondingAgentQuery() is false");
    }


    /**
     *  Clears the responding agent query terms. 
     */

    @OSID @Override
    public void clearRespondingAgentTerms() {
        return;
    }


    /**
     *  Matches affirmative responses. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAffirmative(boolean match) {
        return;
    }


    /**
     *  Clears the affirmative query terms. 
     */

    @OSID @Override
    public void clearAffirmativeTerms() {
        return;
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match audits 
     *  assigned to inquests. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given response query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a response implementing the requested record.
     *
     *  @param responseRecordType a response record type
     *  @return the response query record
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.ResponseQueryRecord getResponseQueryRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.ResponseQueryRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this response query. 
     *
     *  @param responseQueryRecord response query record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResponseQueryRecord(org.osid.inquiry.records.ResponseQueryRecord responseQueryRecord, 
                                          org.osid.type.Type responseRecordType) {

        addRecordType(responseRecordType);
        nullarg(responseQueryRecord, "response query record");
        this.records.add(responseQueryRecord);        
        return;
    }
}
