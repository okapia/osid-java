//
// AbstractKeySearchOdrer.java
//
//     Defines a KeySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code KeySearchOrder}.
 */

public abstract class AbstractKeySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.authentication.keys.KeySearchOrder {

    private final java.util.Collection<org.osid.authentication.keys.records.KeySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  keyRecordType a key record type 
     *  @return {@code true} if the keyRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code keyRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type keyRecordType) {
        for (org.osid.authentication.keys.records.KeySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  keyRecordType the key record type 
     *  @return the key search order record
     *  @throws org.osid.NullArgumentException
     *          {@code keyRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(keyRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeySearchOrderRecord getKeySearchOrderRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this key. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param keyRecord the key search odrer record
     *  @param keyRecordType key record type
     *  @throws org.osid.NullArgumentException
     *          {@code keyRecord} or
     *          {@code keyRecordTypekey} is
     *          {@code null}
     */
            
    protected void addKeyRecord(org.osid.authentication.keys.records.KeySearchOrderRecord keySearchOrderRecord, 
                                     org.osid.type.Type keyRecordType) {

        addRecordType(keyRecordType);
        this.records.add(keySearchOrderRecord);
        
        return;
    }
}
