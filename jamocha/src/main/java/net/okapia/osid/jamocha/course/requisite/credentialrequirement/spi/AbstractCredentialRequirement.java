//
// AbstractCredentialRequirement.java
//
//     Defines a CredentialRequirement.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.credentialrequirement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CredentialRequirement</code>.
 */

public abstract class AbstractCredentialRequirement
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.course.requisite.CredentialRequirement {

    private org.osid.course.program.Credential credential;
    private org.osid.calendaring.Duration timeframe;

    private final java.util.Collection<org.osid.course.requisite.Requisite> altRequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.CredentialRequirementRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets any <code> Requisites </code> that may be substituted in
     *  place of this <code> CredentialRequirement. </code> All <code>
     *  Requisites </code> must be satisifed to be a substitute for
     *  this credential requirement. Inactive <code> Requisites
     *  </code> are not evaluated but if no applicable requisite
     *  exists, then the alternate requisite is not satisifed.
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.altRequisites.toArray(new org.osid.course.requisite.Requisite[this.altRequisites.size()]));
    }


    /**
     *  Adds an alt requisite.
     *
     *  @param altRequisite an alternate requisite
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    protected void addAltRequisite(org.osid.course.requisite.Requisite altRequisite) {
        nullarg(altRequisite, "alt requisite");
        this.altRequisites.add(altRequisite);
        return;
    }


    /**
     *  Sets all the alt requisites.
     *
     *  @param altRequisites a collection of alternate requisites
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisites</code> is <code>null</code>
     */

    protected void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> altRequisites) {
        nullarg(altRequisites, "alt requisites");

        this.altRequisites.clear();
        this.altRequisites.addAll(altRequisites);

        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Credential. </code> 
     *
     *  @return the credential <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCredentialId() {
        return (this.credential.getId());
    }


    /**
     *  Gets the <code> Credential. </code> 
     *
     *  @return the credential 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential()
        throws org.osid.OperationFailedException {

        return (this.credential);
    }


    /**
     *  Sets the credential.
     *
     *  @param credential a credential
     *  @throws org.osid.NullArgumentException
     *          <code>credential</code> is <code>null</code>
     */

    protected void setCredential(org.osid.course.program.Credential credential) {
        nullarg(credential, "credential");
        this.credential = credential;
        return;
    }


    /**
     *  Tests if the credential has to be earned within the required duration. 
     *
     *  @return <code> true </code> if the credential must be earned within a 
     *          required time, <code> false </code> if it could have been 
     *          earned at any time in the past 
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.timeframe != null);
    }


    /**
     *  Gets the timeframe in which the credential has to be earned. A
     *  negative duration indicates the credential had to be earned
     *  within the specified amount of time in the past. A posiitive
     *  duration indicates the credential must be earned within the
     *  specified amount of time in the future. A zero duration
     *  indicates the credential must be earned in the current term.
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        if (!hasTimeframe()) {
            throw new org.osid.IllegalStateException("hasTimeframe() is false");
        }

        return (this.timeframe);
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException
     *          <code>timeframe</code> is <code>null</code>
     */

    protected void setTimeframe(org.osid.calendaring.Duration timeframe) {
        nullarg(timeframe, "timeframe");
        this.timeframe = timeframe;
        return;
    }


    /**
     *  Tests if this credentialRequirement supports the given record
     *  <code>Type</code>.
     *
     *  @param  credentialRequirementRecordType a credential requirement record type 
     *  @return <code>true</code> if the credentialRequirementRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialRequirementRecordType) {
        for (org.osid.course.requisite.records.CredentialRequirementRecord record : this.records) {
            if (record.implementsRecordType(credentialRequirementRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>CredentialRequirement</code> record <code>Type</code>.
     *
     *  @param  credentialRequirementRecordType the credential requirement record type 
     *  @return the credential requirement record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialRequirementRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.CredentialRequirementRecord getCredentialRequirementRecord(org.osid.type.Type credentialRequirementRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.CredentialRequirementRecord record : this.records) {
            if (record.implementsRecordType(credentialRequirementRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRequirementRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential requirement. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialRequirementRecord the credential requirement record
     *  @param credentialRequirementRecordType credential requirement record type
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementRecord</code> or
     *          <code>credentialRequirementRecordTypecredentialRequirement</code>
     *          is <code>null</code>
     */
            
    protected void addCredentialRequirementRecord(org.osid.course.requisite.records.CredentialRequirementRecord credentialRequirementRecord, 
                                                  org.osid.type.Type credentialRequirementRecordType) {
        
        nullarg(credentialRequirementRecord, "credential requirement record");
        addRecordType(credentialRequirementRecordType);
        this.records.add(credentialRequirementRecord);
        
        return;
    }
}
