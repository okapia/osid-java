//
// AbstractFederatingControllerLookupSession.java
//
//     An abstract federating adapter for a ControllerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ControllerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingControllerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.ControllerLookupSession>
    implements org.osid.control.ControllerLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingControllerLookupSession</code>.
     */

    protected AbstractFederatingControllerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.ControllerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Controller</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupControllers() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            if (session.canLookupControllers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Controller</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeControllerView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.useComparativeControllerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Controller</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryControllerView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.usePlenaryControllerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include controllers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }


    /**
     *  Only active controllers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveControllerView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.useActiveControllerView();
        }

        return;
    }


    /**
     *  Active and inactive controllers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusControllerView() {
        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            session.useAnyStatusControllerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Controller</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Controller</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Controller</code> and
     *  retained for compatibility.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  controllerId <code>Id</code> of the
     *          <code>Controller</code>
     *  @return the controller
     *  @throws org.osid.NotFoundException <code>controllerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>controllerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Controller getController(org.osid.id.Id controllerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            try {
                return (session.getController(controllerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(controllerId + " not found");
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  controllers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Controllers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>controllerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByIds(org.osid.id.IdList controllerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.controller.MutableControllerList ret = new net.okapia.osid.jamocha.control.controller.MutableControllerList();

        try (org.osid.id.IdList ids = controllerIds) {
            while (ids.hasNext()) {
                ret.addController(getController(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  controller genus <code>Type</code> which does not include
     *  controllers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList ret = getControllerList();

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            ret.addControllerList(session.getControllersByGenusType(controllerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ControllerList</code> corresponding to the given
     *  controller genus <code>Type</code> and include any additional
     *  controllers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerGenusType a controller genus type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByParentGenusType(org.osid.type.Type controllerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList ret = getControllerList();

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            ret.addControllerList(session.getControllersByParentGenusType(controllerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ControllerList</code> containing the given
     *  controller record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getControllers()</code>.
     *
     *  @param  controllerRecordType a controller record type 
     *  @return the returned <code>Controller</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByRecordType(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList ret = getControllerList();

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            ret.addControllerList(session.getControllersByRecordType(controllerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ControllerList </code> with the given address. <code>
     *  </code>
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @param  address a controller address
     *  @return the returned <code> Controller </code> list
     *  @throws org.osid.NullArgumentException <code> address </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllersByAddress(String address)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList ret = getControllerList();

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            ret.addControllerList(session.getControllersByAddress(address));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets all <code>Controllers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  controllers or an error results. Otherwise, the returned list
     *  may contain only those controllers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, controllers are returned that are currently
     *  active. In any status mode, active and inactive controllers
     *  are returned.
     *
     *  @return a list of <code>Controllers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ControllerList getControllers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList ret = getControllerList();

        for (org.osid.control.ControllerLookupSession session : getSessions()) {
            ret.addControllerList(session.getControllers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.controller.FederatingControllerList getControllerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.controller.ParallelControllerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.controller.CompositeControllerList());
        }
    }
}
