//
// InvariantMapProxyRuleLookupSession
//
//    Implements a Rule lookup service backed by a fixed
//    collection of rules. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules;


/**
 *  Implements a Rule lookup service backed by a fixed
 *  collection of rules. The rules are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyRuleLookupSession
    extends net.okapia.osid.jamocha.core.rules.spi.AbstractMapRuleLookupSession
    implements org.osid.rules.RuleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRuleLookupSession} with no
     *  rules.
     *
     *  @param engine the engine
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.proxy.Proxy proxy) {
        setEngine(engine);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyRuleLookupSession} with a single
     *  rule.
     *
     *  @param engine the engine
     *  @param rule a single rule
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rule} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.Rule rule, org.osid.proxy.Proxy proxy) {

        this(engine, proxy);
        putRule(rule);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyRuleLookupSession} using
     *  an array of rules.
     *
     *  @param engine the engine
     *  @param rules an array of rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.Rule[] rules, org.osid.proxy.Proxy proxy) {

        this(engine, proxy);
        putRules(rules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyRuleLookupSession} using a
     *  collection of rules.
     *
     *  @param engine the engine
     *  @param rules a collection of rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code engine},
     *          {@code rules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyRuleLookupSession(org.osid.rules.Engine engine,
                                                  java.util.Collection<? extends org.osid.rules.Rule> rules,
                                                  org.osid.proxy.Proxy proxy) {

        this(engine, proxy);
        putRules(rules);
        return;
    }
}
