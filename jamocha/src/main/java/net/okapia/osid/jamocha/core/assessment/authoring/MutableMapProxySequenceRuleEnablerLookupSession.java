//
// MutableMapProxySequenceRuleEnablerLookupSession
//
//    Implements a SequenceRuleEnabler lookup service backed by a collection of
//    sequenceRuleEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRuleEnabler lookup service backed by a collection of
 *  sequenceRuleEnablers. The sequenceRuleEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of sequence rule enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxySequenceRuleEnablerLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractMapSequenceRuleEnablerLookupSession
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxySequenceRuleEnablerLookupSession}
     *  with no sequence rule enablers.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxySequenceRuleEnablerLookupSession} with a
     *  single sequence rule enabler.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnabler a sequence rule enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putSequenceRuleEnabler(sequenceRuleEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySequenceRuleEnablerLookupSession} using an
     *  array of sequence rule enablers.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnablers an array of sequence rule enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.authoring.SequenceRuleEnabler[] sequenceRuleEnablers, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxySequenceRuleEnablerLookupSession} using a
     *  collection of sequence rule enablers.
     *
     *  @param bank the bank
     *  @param sequenceRuleEnablers a collection of sequence rule enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRuleEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxySequenceRuleEnablerLookupSession(org.osid.assessment.Bank bank,
                                                java.util.Collection<? extends org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(bank, proxy);
        setSessionProxy(proxy);
        putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }

    
    /**
     *  Makes a {@code SequenceRuleEnabler} available in this session.
     *
     *  @param sequenceRuleEnabler an sequence rule enabler
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRuleEnabler(org.osid.assessment.authoring.SequenceRuleEnabler sequenceRuleEnabler) {
        super.putSequenceRuleEnabler(sequenceRuleEnabler);
        return;
    }


    /**
     *  Makes an array of sequenceRuleEnablers available in this session.
     *
     *  @param sequenceRuleEnablers an array of sequence rule enablers
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRuleEnablers(org.osid.assessment.authoring.SequenceRuleEnabler[] sequenceRuleEnablers) {
        super.putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }


    /**
     *  Makes collection of sequence rule enablers available in this session.
     *
     *  @param sequenceRuleEnablers
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putSequenceRuleEnablers(java.util.Collection<? extends org.osid.assessment.authoring.SequenceRuleEnabler> sequenceRuleEnablers) {
        super.putSequenceRuleEnablers(sequenceRuleEnablers);
        return;
    }


    /**
     *  Removes a SequenceRuleEnabler from this session.
     *
     *  @param sequenceRuleEnablerId the {@code Id} of the sequence rule enabler
     *  @throws org.osid.NullArgumentException {@code sequenceRuleEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId) {
        super.removeSequenceRuleEnabler(sequenceRuleEnablerId);
        return;
    }    
}
