//
// AbstractRequestLookupSession.java
//
//    A starter implementation framework for providing a Request
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Request
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRequests(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRequestLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.RequestLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Request</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequests() {
        return (true);
    }


    /**
     *  A complete view of the <code>Request</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Request</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requests in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only requests whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRequestView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All requests of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRequestView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Request</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Request</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Request</code> and retained for
     *  compatibility.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestId <code>Id</code> of the
     *          <code>Request</code>
     *  @return the request
     *  @throws org.osid.NotFoundException <code>requestId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requestId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest(org.osid.id.Id requestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.RequestList requests = getRequests()) {
            while (requests.hasNext()) {
                org.osid.provisioning.Request request = requests.getNextRequest();
                if (request.getId().equals(requestId)) {
                    return (request);
                }
            }
        } 

        throw new org.osid.NotFoundException(requestId + " not found");
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  requests specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Requests</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRequests()</code>.
     *
     *  @param  requestIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>requestIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByIds(org.osid.id.IdList requestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.Request> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = requestIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRequest(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("request " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.request.LinkedRequestList(ret));
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  request genus <code>Type</code> which does not include
     *  requests of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRequests()</code>.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestGenusFilterList(getRequests(), requestGenusType));
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  request genus <code>Type</code> and include any additional
     *  requests with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequests()</code>.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByParentGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRequestsByGenusType(requestGenusType));
    }


    /**
     *  Gets a <code>RequestList</code> containing the given request
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequests()</code>.
     *
     *  @param  requestRecordType a request record type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByRecordType(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestRecordFilterList(getRequests(), requestRecordType));
    }


    /**
     *  Gets a <code>RequestList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *  
     *  In active mode, requests are returned that are currently
     *  active. In any status mode, active and inactive requests are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Request</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.TemporalRequestFilterList(getRequests(), from, to));
    }
        

    /**
     *  Gets a list of requests corresponding to a requester
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestList getRequestsForRequester(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestFilterList(new RequesterFilter(resourceId), getRequests()));
    }


    /**
     *  Gets a list of requests corresponding to a requester
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterOnDate(org.osid.id.Id resourceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.TemporalRequestFilterList(getRequestsForRequester(resourceId), from, to));
    }


    /**
     *  Gets a list of requests corresponding to a queue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestList getRequestsForQueue(org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestFilterList(new QueueFilter(queueId), getRequests()));
    }


    /**
     *  Gets a list of requests corresponding to a queue
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForQueueOnDate(org.osid.id.Id queueId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.TemporalRequestFilterList(getRequestsForQueue(queueId), from, to));
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>queueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueue(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestFilterList(new QueueFilter(queueId), getRequestsForRequester(resourceId)));
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known requests
     *  or an error results. Otherwise, the returned list may contain
     *  only those requests that are accessible through this session.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>queueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueueOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id queueId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.request.TemporalRequestFilterList(getRequestsForRequesterAndQueue(resourceId, queueId), from, to));
    }


    /**
     *  Gets all <code>Requests</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Requests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the request list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of requests
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.RequestList filterRequestsOnViews(org.osid.provisioning.RequestList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.RequestList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.request.EffectiveRequestFilterList(ret);
        }

        return (ret);
    }


    public static class RequesterFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>RequesterFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public RequesterFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the RequestFilterList to filter the 
         *  request list based on resource.
         *
         *  @param request the request
         *  @return <code>true</code> to pass the request,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Request request) {
            return (request.getRequesterId().equals(this.resourceId));
        }
    }


    public static class QueueFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.request.RequestFilter {
         
        private final org.osid.id.Id queueId;
         
         
        /**
         *  Constructs a new <code>QueueFilter</code>.
         *
         *  @param queueId the queue to filter
         *  @throws org.osid.NullArgumentException
         *          <code>queueId</code> is <code>null</code>
         */
        
        public QueueFilter(org.osid.id.Id queueId) {
            nullarg(queueId, "queue Id");
            this.queueId = queueId;
            return;
        }

         
        /**
         *  Used by the RequestFilterList to filter the 
         *  request list based on queue.
         *
         *  @param request the request
         *  @return <code>true</code> to pass the request,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Request request) {
            return (request.getQueueId().equals(this.queueId));
        }
    }
}
