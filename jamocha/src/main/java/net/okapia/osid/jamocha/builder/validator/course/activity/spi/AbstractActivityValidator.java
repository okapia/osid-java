//
// AbstractActivityValidator.java
//
//     Validates an Activity.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.activity.spi;


/**
 *  Validates an Activity.
 */

public abstract class AbstractActivityValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractActivityValidator</code>.
     */

    protected AbstractActivityValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractActivityValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractActivityValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Activity.
     *
     *  @param activity an activity to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.Activity activity) {
        super.validate(activity);
        
        testNestedObject(activity, "getActivityUnit");
        testNestedObject(activity, "getCourseOffering");
        testNestedObject(activity, "getTerm");
        testNestedObjects(activity, "getScheduleIds", "getSchedules");
        testNestedObjects(activity, "getSupersedingActivityIds", "getSupersedingActivities");

        test(activity.getSpecificMeetingTimes(), "getSpecificMeetingTimes()");

        test(activity.getBlackouts(), "getBlackouts()");
        testNestedObjects(activity, "getInstructorIds", "getInstructors");

        testConditionalMethod(activity, "getMaximumSeats", activity.isSeatingLimited(), "isSeatingLimited()");
        if (activity.isSeatingLimited()) {
            testCardinalRange(activity.getMinimumSeats(), activity.getMaximumSeats(), "minimum and maximum seats");
        }

        test(activity.getTotalTargetEffort(), "getTotalTargetEffort()");
        test(activity.getTotalTargetContactTime(), "getTotalTargetContactTime()");
        test(activity.getTotalTargetIndividualEffort(), "getTotalTargetIndividualEffort()");
        
        testConditionalMethod(activity, "getWeeklyEffort", activity.isRecurringWeekly(), "isRecurringWeekly()");
        testConditionalMethod(activity, "getWeeklyContactTime", activity.isRecurringWeekly(), "isRecurringWeekly()");
        testConditionalMethod(activity, "getWeeklyIndividualEffort", activity.isRecurringWeekly(), "isRecurringWeekly()");

        return;
    }
}
