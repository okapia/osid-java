//
// AbstractIndexedMapSupersedingEventLookupSession.java
//
//    A simple framework for providing a SupersedingEvent lookup service
//    backed by a fixed collection of superseding events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SupersedingEvent lookup service backed by a
 *  fixed collection of superseding events. The superseding events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some superseding events may be compatible
 *  with more types than are indicated through these superseding event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SupersedingEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSupersedingEventLookupSession
    extends AbstractMapSupersedingEventLookupSession
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.SupersedingEvent> supersedingEventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.SupersedingEvent>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.SupersedingEvent> supersedingEventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.SupersedingEvent>());


    /**
     *  Makes a <code>SupersedingEvent</code> available in this session.
     *
     *  @param  supersedingEvent a superseding event
     *  @throws org.osid.NullArgumentException <code>supersedingEvent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        super.putSupersedingEvent(supersedingEvent);

        this.supersedingEventsByGenus.put(supersedingEvent.getGenusType(), supersedingEvent);
        
        try (org.osid.type.TypeList types = supersedingEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.supersedingEventsByRecord.put(types.getNextType(), supersedingEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a superseding event from this session.
     *
     *  @param supersedingEventId the <code>Id</code> of the superseding event
     *  @throws org.osid.NullArgumentException <code>supersedingEventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSupersedingEvent(org.osid.id.Id supersedingEventId) {
        org.osid.calendaring.SupersedingEvent supersedingEvent;
        try {
            supersedingEvent = getSupersedingEvent(supersedingEventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.supersedingEventsByGenus.remove(supersedingEvent.getGenusType());

        try (org.osid.type.TypeList types = supersedingEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.supersedingEventsByRecord.remove(types.getNextType(), supersedingEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSupersedingEvent(supersedingEventId);
        return;
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> which does not include
     *  superseding events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known superseding events or an error results. Otherwise,
     *  the returned list may contain only those superseding events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  supersedingEventGenusType a superseding event genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.supersedingevent.ArraySupersedingEventList(this.supersedingEventsByGenus.get(supersedingEventGenusType)));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> containing the given
     *  superseding event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known superseding events or an error
     *  results. Otherwise, the returned list may contain only those
     *  superseding events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  supersedingEventRecordType a superseding event record type 
     *  @return the returned <code>supersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByRecordType(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.supersedingevent.ArraySupersedingEventList(this.supersedingEventsByRecord.get(supersedingEventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.supersedingEventsByGenus.clear();
        this.supersedingEventsByRecord.clear();

        super.close();

        return;
    }
}
