//
// AbstractFederatingExtensible.java
//
//     This adapter federates one or more Extensibles.
//
//
// Tom Coppeto
// Okapia
// 30 November 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This adapter federates one or more Extensibles.
 */

public abstract class AbstractFederatingExtensible<T extends org.osid.Extensible>
    implements org.osid.Extensible {
    
    private final java.util.Collection<T> objects = new java.util.ArrayList<>();


    /**
     *  Adds an extensible.
     *
     *  @param object the underlying object
     *  @throws org.osid.NullArgumentException <code>condition</code>
     *          id <code>null</code>
     */

    protected void add(T object) {
        nullarg(object, "object");
        this.objects.add(object);
        return;
    }


    /**
     *  Gets the record types available in this object. A record
     *  <code> Type </code> explicitly indicates the specification of
     *  an interface to the record. A record may or may not inherit
     *  other record interfaces through interface inheritance in which
     *  case support of a record type may not be explicit in the
     *  returned list. Interoperability with the typed interface to
     *  this object should be performed through <code>
     *  hasRecordType(). </code>
     *
     *  @return the record types available 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        java.util.Collection<org.osid.type.Type> ret = new java.util.HashSet<>();

        for (org.osid.Extensible e : this.objects) {

            try (org.osid.type.TypeList types = e.getRecordTypes()) {
                while (types.hasNext()) {
                    ret.add(types.getNextType());
                }
            } catch (org.osid.OperationFailedException ofe) {
            }
        }
        
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(ret));
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code> The given record type may be supported by the
     *  object through interface/type inheritence. This method should
     *  be checked before retrieving the record interface.
     *
     *  @param  recordType a record type 
     *  @return <code> true </code> if a record of the given record
     *          <code> Type </code> is available, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        for (org.osid.Extensible e : this.objects) {
            if (e.hasRecordType(recordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the underlying objects.
     *
     *  @return a list of the underlying objects
     */

    protected java.util.Collection<T> getList() {
        return (java.util.Collections.unmodifiableCollection(this.objects));
    }
}
    
