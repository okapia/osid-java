//
// MutableMapProxyTodoLookupSession
//
//    Implements a Todo lookup service backed by a collection of
//    todos that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist;


/**
 *  Implements a Todo lookup service backed by a collection of
 *  todos. The todos are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of todos can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyTodoLookupSession
    extends net.okapia.osid.jamocha.core.checklist.spi.AbstractMapTodoLookupSession
    implements org.osid.checklist.TodoLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyTodoLookupSession}
     *  with no todos.
     *
     *  @param checklist the checklist
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                  org.osid.proxy.Proxy proxy) {
        setChecklist(checklist);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyTodoLookupSession} with a
     *  single todo.
     *
     *  @param checklist the checklist
     *  @param todo a todo
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist},
     *          {@code todo}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                org.osid.checklist.Todo todo, org.osid.proxy.Proxy proxy) {
        this(checklist, proxy);
        putTodo(todo);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyTodoLookupSession} using an
     *  array of todos.
     *
     *  @param checklist the checklist
     *  @param todos an array of todos
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist},
     *          {@code todos}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                org.osid.checklist.Todo[] todos, org.osid.proxy.Proxy proxy) {
        this(checklist, proxy);
        putTodos(todos);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyTodoLookupSession} using a
     *  collection of todos.
     *
     *  @param checklist the checklist
     *  @param todos a collection of todos
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code checklist},
     *          {@code todos}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyTodoLookupSession(org.osid.checklist.Checklist checklist,
                                                java.util.Collection<? extends org.osid.checklist.Todo> todos,
                                                org.osid.proxy.Proxy proxy) {
   
        this(checklist, proxy);
        setSessionProxy(proxy);
        putTodos(todos);
        return;
    }

    
    /**
     *  Makes a {@code Todo} available in this session.
     *
     *  @param todo an todo
     *  @throws org.osid.NullArgumentException {@code todo{@code 
     *          is {@code null}
     */

    @Override
    public void putTodo(org.osid.checklist.Todo todo) {
        super.putTodo(todo);
        return;
    }


    /**
     *  Makes an array of todos available in this session.
     *
     *  @param todos an array of todos
     *  @throws org.osid.NullArgumentException {@code todos{@code 
     *          is {@code null}
     */

    @Override
    public void putTodos(org.osid.checklist.Todo[] todos) {
        super.putTodos(todos);
        return;
    }


    /**
     *  Makes collection of todos available in this session.
     *
     *  @param todos
     *  @throws org.osid.NullArgumentException {@code todo{@code 
     *          is {@code null}
     */

    @Override
    public void putTodos(java.util.Collection<? extends org.osid.checklist.Todo> todos) {
        super.putTodos(todos);
        return;
    }


    /**
     *  Removes a Todo from this session.
     *
     *  @param todoId the {@code Id} of the todo
     *  @throws org.osid.NullArgumentException {@code todoId{@code  is
     *          {@code null}
     */

    @Override
    public void removeTodo(org.osid.id.Id todoId) {
        super.removeTodo(todoId);
        return;
    }    
}
