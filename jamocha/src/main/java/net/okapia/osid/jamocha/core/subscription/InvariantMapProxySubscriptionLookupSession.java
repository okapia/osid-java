//
// InvariantMapProxySubscriptionLookupSession
//
//    Implements a Subscription lookup service backed by a fixed
//    collection of subscriptions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Subscription lookup service backed by a fixed
 *  collection of subscriptions. The subscriptions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySubscriptionLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractMapSubscriptionLookupSession
    implements org.osid.subscription.SubscriptionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySubscriptionLookupSession} with no
     *  subscriptions.
     *
     *  @param publisher the publisher
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySubscriptionLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.proxy.Proxy proxy) {
        setPublisher(publisher);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySubscriptionLookupSession} with a single
     *  subscription.
     *
     *  @param publisher the publisher
     *  @param subscription a single subscription
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code subscription} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubscriptionLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.subscription.Subscription subscription, org.osid.proxy.Proxy proxy) {

        this(publisher, proxy);
        putSubscription(subscription);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySubscriptionLookupSession} using
     *  an array of subscriptions.
     *
     *  @param publisher the publisher
     *  @param subscriptions an array of subscriptions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code subscriptions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubscriptionLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.subscription.Subscription[] subscriptions, org.osid.proxy.Proxy proxy) {

        this(publisher, proxy);
        putSubscriptions(subscriptions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySubscriptionLookupSession} using a
     *  collection of subscriptions.
     *
     *  @param publisher the publisher
     *  @param subscriptions a collection of subscriptions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code subscriptions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySubscriptionLookupSession(org.osid.subscription.Publisher publisher,
                                                  java.util.Collection<? extends org.osid.subscription.Subscription> subscriptions,
                                                  org.osid.proxy.Proxy proxy) {

        this(publisher, proxy);
        putSubscriptions(subscriptions);
        return;
    }
}
