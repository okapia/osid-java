//
// AbstractQueryIssueLookupSession.java
//
//    An inline adapter that maps an IssueLookupSession to
//    an IssueQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an IssueLookupSession to
 *  an IssueQuerySession.
 */

public abstract class AbstractQueryIssueLookupSession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractIssueLookupSession
    implements org.osid.tracking.IssueLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.tracking.IssueQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryIssueLookupSession.
     *
     *  @param querySession the underlying issue query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryIssueLookupSession(org.osid.tracking.IssueQuerySession querySession) {
        nullarg(querySession, "issue query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>FrontOffice</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>FrontOffice Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the <code>FrontOffice</code> associated with this 
     *  session.
     *
     *  @return the <code>FrontOffice</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform <code>Issue</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupIssues() {
        return (this.session.canSearchIssues());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include issues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only issues whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveIssueView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All issues of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveIssueView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Issue</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Issue</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Issue</code> and
     *  retained for compatibility.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueId <code>Id</code> of the
     *          <code>Issue</code>
     *  @return the issue
     *  @throws org.osid.NotFoundException <code>issueId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>issueId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue(org.osid.id.Id issueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchId(issueId, true);
        org.osid.tracking.IssueList issues = this.session.getIssuesByQuery(query);
        if (issues.hasNext()) {
            return (issues.getNextIssue());
        } 
        
        throw new org.osid.NotFoundException(issueId + " not found");
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  issues specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Issues</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, issues are returned that are currently effective.
     *  In any effective mode, effective issues and those currently expired
     *  are returned.
     *
     *  @param  issueIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>issueIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByIds(org.osid.id.IdList issueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();

        try (org.osid.id.IdList ids = issueIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  issue genus <code>Type</code> which does not include
     *  issues of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently effective.
     *  In any effective mode, effective issues and those currently expired
     *  are returned.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchGenusType(issueGenusType, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  issue genus <code>Type</code> and include any additional
     *  issues with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByParentGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchParentGenusType(issueGenusType, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets an <code>IssueList</code> containing the given
     *  issue record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueRecordType an issue record type 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByRecordType(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchRecordType(issueRecordType, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets an <code>IssueList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *  
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Issue</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.tracking.IssueList getIssuesOnDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getIssuesByQuery(query));
    }
        

    /**
     *  Gets a list of issues corresponding to a queue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.tracking.IssueList getIssuesForQueue(org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchQueueId(queueId, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets a list of issues corresponding to a queue
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueOnDate(org.osid.id.Id queueId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchQueueId(queueId, true);
        query.matchDate(from, to, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets a list of issues corresponding to a customer
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the customer
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.tracking.IssueList getIssuesForCustomer(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchCustomerId(resourceId, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets a list of issues corresponding to a customer
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForCustomerOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchCustomerId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  resourceId the <code>Id</code> of the customer
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomer(org.osid.id.Id queueId,
                                                                    org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchQueueId(queueId, true);
        query.matchCustomerId(resourceId, true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible
     *  through this session.
     *
     *  In effective mode, issues are returned that are
     *  currently effective.  In any effective mode, effective
     *  issues and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  resourceId the <code>Id</code> of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>IssueList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomerOnDate(org.osid.id.Id queueId,
                                                                          org.osid.id.Id resourceId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.tracking.IssueQuery query = getQuery();
        query.matchQueueId(queueId, true);
        query.matchCustomerId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getIssuesByQuery(query));
    }

    
    /**
     *  Gets all <code>Issues</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Issues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.tracking.IssueQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getIssuesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.tracking.IssueQuery getQuery() {
        org.osid.tracking.IssueQuery query = this.session.getIssueQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
