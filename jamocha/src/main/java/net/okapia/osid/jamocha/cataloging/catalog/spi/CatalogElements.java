//
// CatalogElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.catalog.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CatalogElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the CatalogElement Id.
     *
     *  @return the catalog element Id
     */

    public static org.osid.id.Id getCatalogEntityId() {
        return (makeEntityId("osid.cataloging.Catalog"));
    }


    /**
     *  Gets the Id element Id.
     *
     *  @return the Id element Id
     */

    public static org.osid.id.Id getId() {
        return (makeQueryElementId("osid.cataloging.catalog.Id"));
    }


    /**
     *  Gets the AncestorCatalogId element Id.
     *
     *  @return the AncestorCatalogId element Id
     */

    public static org.osid.id.Id getAncestorCatalogId() {
        return (makeQueryElementId("osid.cataloging.catalog.AncestorCatalogId"));
    }


    /**
     *  Gets the AncestorCatalog element Id.
     *
     *  @return the AncestorCatalog element Id
     */

    public static org.osid.id.Id getAncestorCatalog() {
        return (makeQueryElementId("osid.cataloging.catalog.AncestorCatalog"));
    }


    /**
     *  Gets the DescendantCatalogId element Id.
     *
     *  @return the DescendantCatalogId element Id
     */

    public static org.osid.id.Id getDescendantCatalogId() {
        return (makeQueryElementId("osid.cataloging.catalog.DescendantCatalogId"));
    }


    /**
     *  Gets the DescendantCatalog element Id.
     *
     *  @return the DescendantCatalog element Id
     */

    public static org.osid.id.Id getDescendantCatalog() {
        return (makeQueryElementId("osid.cataloging.catalog.DescendantCatalog"));
    }
}
