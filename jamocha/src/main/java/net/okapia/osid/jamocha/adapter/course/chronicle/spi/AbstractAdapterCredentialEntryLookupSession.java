//
// AbstractAdapterCredentialEntryLookupSession.java
//
//    A CredentialEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CredentialEntry lookup session adapter.
 */

public abstract class AbstractAdapterCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {

    private final org.osid.course.chronicle.CredentialEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCredentialEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCredentialEntryLookupSession(org.osid.course.chronicle.CredentialEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code CredentialEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCredentialEntries() {
        return (this.session.canLookupCredentialEntries());
    }


    /**
     *  A complete view of the {@code CredentialEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCredentialEntryView() {
        this.session.useComparativeCredentialEntryView();
        return;
    }


    /**
     *  A complete view of the {@code CredentialEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCredentialEntryView() {
        this.session.usePlenaryCredentialEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credential entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only credential entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCredentialEntryView() {
        this.session.useEffectiveCredentialEntryView();
        return;
    }
    

    /**
     *  All credential entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCredentialEntryView() {
        this.session.useAnyEffectiveCredentialEntryView();
        return;
    }

     
    /**
     *  Gets the {@code CredentialEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CredentialEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CredentialEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param credentialEntryId {@code Id} of the {@code CredentialEntry}
     *  @return the credential entry
     *  @throws org.osid.NotFoundException {@code credentialEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code credentialEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntry getCredentialEntry(org.osid.id.Id credentialEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntry(credentialEntryId));
    }


    /**
     *  Gets a {@code CredentialEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credentialEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CredentialEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CredentialEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByIds(org.osid.id.IdList credentialEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesByIds(credentialEntryIds));
    }


    /**
     *  Gets a {@code CredentialEntryList} corresponding to the given
     *  credential entry genus {@code Type} which does not include
     *  credential entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialEntryGenusType a credentialEntry genus type 
     *  @return the returned {@code CredentialEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByGenusType(org.osid.type.Type credentialEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesByGenusType(credentialEntryGenusType));
    }


    /**
     *  Gets a {@code CredentialEntryList} corresponding to the given
     *  credential entry genus {@code Type} and include any additional
     *  credential entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialEntryGenusType a credentialEntry genus type 
     *  @return the returned {@code CredentialEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByParentGenusType(org.osid.type.Type credentialEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesByParentGenusType(credentialEntryGenusType));
    }


    /**
     *  Gets a {@code CredentialEntryList} containing the given
     *  credential entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialEntryRecordType a credentialEntry record type 
     *  @return the returned {@code CredentialEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code credentialEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesByRecordType(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesByRecordType(credentialEntryRecordType));
    }


    /**
     *  Gets a {@code CredentialEntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *  
     *  In active mode, credential entries are returned that are currently
     *  active. In any status mode, active and inactive credential entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CredentialEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of credential entries corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForStudent(resourceId));
    }


    /**
     *  Gets a list of credential entries corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of credential entries corresponding to a credential
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  credentialId the {@code Id} of the credential
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code credentialId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForCredential(credentialId));
    }


    /**
     *  Gets a list of credential entries corresponding to a credential
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  credentialId the {@code Id} of the credential
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code credentialId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForCredentialOnDate(org.osid.id.Id credentialId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForCredentialOnDate(credentialId, from, to));
    }


    /**
     *  Gets a list of credential entries corresponding to student and credential
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  credential entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  credentialId the {@code Id} of the credential
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code credentialId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentAndCredential(org.osid.id.Id resourceId,
                                                                                                     org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForStudentAndCredential(resourceId, credentialId));
    }


    /**
     *  Gets a list of credential entries corresponding to student and credential
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible
     *  through this session.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective. In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @param  credentialId the {@code Id} of the credential
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CredentialEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code credentialId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntriesForStudentAndCredentialOnDate(org.osid.id.Id resourceId,
                                                                                                           org.osid.id.Id credentialId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntriesForStudentAndCredentialOnDate(resourceId, credentialId, from, to));
    }


    /**
     *  Gets all {@code CredentialEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  credential entries or an error results. Otherwise, the returned list
     *  may contain only those credential entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credential entries are returned that are currently
     *  effective.  In any effective mode, effective credential entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code CredentialEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCredentialEntries());
    }
}
