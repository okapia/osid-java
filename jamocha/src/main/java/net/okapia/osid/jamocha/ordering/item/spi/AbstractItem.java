//
// AbstractItem.java
//
//     Defines an Item.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.item.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Item</code>.
 */

public abstract class AbstractItem
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.ordering.Item {

    private boolean derived = false;
    private org.osid.ordering.Order order;
    private org.osid.ordering.Product product;
    private final java.util.Collection<org.osid.ordering.Price> unitPrices = new java.util.LinkedHashSet<>();
    private long quantity;
    private final java.util.Collection<org.osid.ordering.Cost> costs = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.ordering.records.ItemRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the order <code> Id </code> for this item. 
     *
     *  @return the order <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrderId() {
        return (this.order.getId());
    }


    /**
     *  Gets the order for this item. 
     *
     *  @return the order 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder()
        throws org.osid.OperationFailedException {

        return (this.order);
    }


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @throws org.osid.NullArgumentException
     *          <code>order</code> is <code>null</code>
     */

    protected void setOrder(org.osid.ordering.Order order) {
        nullarg(order, "order");
        this.order = order;
        return;
    }


    /**
     *  Tests if the item is a derived item as opposed to one that has been 
     *  explicitly selected. 
     *
     *  @return <code> true </code> if this item is derived, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isDerived() {
        return (this.derived);
    }


    /**
     *  Sets the derived flag.
     *
     *  @param derived <code> true </code> if this item is derived,
     *         <code> false </code> otherwise
     */

    protected void setDerived(boolean derived) {
        this.derived = derived;
        return;
    }


    /**
     *  Gets the product <code> Id </code> for this item. 
     *
     *  @return the product <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProductId() {
        return (this.product.getId());
    }


    /**
     *  Gets the product for this item. 
     *
     *  @return the product 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct()
        throws org.osid.OperationFailedException {

        return (this.product);
    }


    /**
     *  Sets the product.
     *
     *  @param product a product
     *  @throws org.osid.NullArgumentException
     *          <code>product</code> is <code>null</code>
     */

    protected void setProduct(org.osid.ordering.Product product) {
        nullarg(product, "product");
        this.product = product;
        return;
    }


    /**
     *  Gets the price <code> Ids </code> for this item. 
     *
     *  @return the price <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getUnitPriceIds() {
        try {
            org.osid.ordering.PriceList unitPrices = getUnitPrices();
            return (new net.okapia.osid.jamocha.adapter.converter.ordering.price.PriceToIdList(unitPrices));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the prices for this item. There may be different price types for 
     *  a single item. 
     *
     *  @return the prices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceList getUnitPrices()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.ordering.price.ArrayPriceList(this.unitPrices));
    }


    /**
     *  Adds a unit price.
     *
     *  @param price a unit price
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    protected void addUnitPrice(org.osid.ordering.Price price) {
        nullarg(price, "price");
        this.unitPrices.add(price);
        return;
    }


    /**
     *  Sets all the unit prices.
     *
     *  @param prices a collection of unit prices
     *  @throws org.osid.NullArgumentException
     *          <code>prices</code> is <code>null</code>
     */

    protected void setUnitPrices(java.util.Collection<org.osid.ordering.Price> prices) {
        nullarg(prices, "unit prices");
        this.unitPrices.clear();
        this.unitPrices.addAll(prices);
        return;
    }


    /**
     *  Gets the quantity of the product. 
     *
     *  @return the quantity of the product 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity the quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    protected void setQuantity(long quantity) {
        cardinalarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Gets the line item costs. 
     *
     *  @return the costs. 
     */

    @OSID @Override
    public org.osid.ordering.CostList getCosts() {
        return (new net.okapia.osid.jamocha.ordering.cost.ArrayCostList(this.costs));
    }


    /**
     *  Adds a cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void addCost(org.osid.ordering.Cost cost) {
        nullarg(cost, "cost");
        this.costs.add(cost);
        return;
    }


    /**
     *  Sets all the costs.
     *
     *  @param costs a collection of costs
     *  @throws org.osid.NullArgumentException
     *          <code>costs</code> is <code>null</code>
     */

    protected void setCosts(java.util.Collection<org.osid.ordering.Cost> costs) {
        nullarg(costs, "costs");
        this.costs.clear();
        this.costs.addAll(costs);
        return;
    }


    /**
     *  Tests if this item supports the given record
     *  <code>Type</code>.
     *
     *  @param  itemRecordType an item record type 
     *  @return <code>true</code> if the itemRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type itemRecordType) {
        for (org.osid.ordering.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Item</code> record <code>Type</code>.
     *
     *  @param  itemRecordType the item record type 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param itemRecord the item record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecord</code> or
     *          <code>itemRecordTypeitem</code> is
     *          <code>null</code>
     */
            
    protected void addItemRecord(org.osid.ordering.records.ItemRecord itemRecord, 
                                 org.osid.type.Type itemRecordType) {
        
        nullarg(itemRecord, "item record");
        addRecordType(itemRecordType);
        this.records.add(itemRecord);
        
        return;
    }
}
