//
// AbstractCost.java
//
//     Defines a Cost.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.cost.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Cost</code>.
 */

public abstract class AbstractCost
    implements org.osid.ordering.Cost {

    private org.osid.ordering.PriceSchedule schedule;
    private org.osid.financials.Currency amount;


    /**
     *  Gets the price schedule <code> Id </code> to which this price
     *  belongs.
     *
     *  @return the price schedule <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPriceScheduleId() {
        return (this.schedule.getId());
    }


    /**
     *  Gets the price schedule to which this price belongs. 
     *
     *  @return the price schedule 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule()
        throws org.osid.OperationFailedException {

        return (this.schedule);
    }


    /** 
     *  Sets the schedule.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected void setPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        nullarg(schedule, "schedule");
        this.schedule = schedule;
        return;
    }


    /**
     *  Gets the cost. 
     *
     *  @return the cost 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }

    
    /** 
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }
}