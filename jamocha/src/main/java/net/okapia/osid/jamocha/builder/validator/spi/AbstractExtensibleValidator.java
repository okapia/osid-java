//
// AbstractExtensibleValidator.java
//
//     Validates Extensibles.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.spi;

import net.okapia.osid.jamocha.builder.validator.Validation;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Validates an Extensible.
 */

public abstract class AbstractExtensibleValidator
    extends AbstractValidator {


    /**
     *  Constructs a new <code>AbstractExtensibleValidator</code>.
     */

    protected AbstractExtensibleValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractExtensibleValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractExtensibleValidator(java.util.EnumSet<Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Extensible.
     *
     *  @param extensible the extensible to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */
    
    public void validate(org.osid.Extensible extensible) {
        super.validate(extensible);

        java.lang.reflect.Method method = null;
        if (perform(Validation.RECORDS)) {
            for (java.lang.reflect.Method m : extensible.getClass().getMethods()) {
                Class<?>[] parameters = m.getParameterTypes();

                /* find the getXYZRecord method */
                if ((parameters.length == 1) && (org.osid.type.Type.class.isAssignableFrom(parameters[0]))) {
                    if (org.osid.OsidRecord.class.isAssignableFrom(m.getReturnType())) {
                        if (m.getName().startsWith("get") && m.getName().endsWith("Record")) {
                            method = m;
                            break;
                        }
                    }
                }
            }
        }

        try (org.osid.type.TypeList types = extensible.getRecordTypes()) {
            test(types, "getRecordTypes()");

            while (types.hasNext()) {
                org.osid.type.Type type = types.getNextType();
                if (perform(Validation.TYPE_SUPPORT)) {
                    if (!(extensible.hasRecordType(type))) {
                        throw new org.osid.BadLogicException(type + " not supported");
                    }               
                }
                
                if (perform(Validation.RECORDS)) {
                    if (method != null) {
                        org.osid.OsidRecord record = (org.osid.OsidRecord) method.invoke(extensible, type);
                        if (!record.implementsRecordType(type)) {
                            throw new org.osid.BadLogicException(record.getClass().getName() + " does not implement " + type);
                        }
                    } else {
                        throw new org.osid.OsidRuntimeException("no record method in " + extensible.getClass().getName());
                    }
                }
            }
        } catch (org.osid.OperationFailedException | 
                 IllegalAccessException | java.lang.reflect.InvocationTargetException oe) {
            throw new org.osid.OsidRuntimeException(oe);
        }

        return;
    }
}
