//
// AbstractFederatingCreditLookupSession.java
//
//     An abstract federating adapter for a CreditLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CreditLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCreditLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.acknowledgement.CreditLookupSession>
    implements org.osid.acknowledgement.CreditLookupSession {

    private boolean parallel = false;
    private org.osid.acknowledgement.Billing billing = new net.okapia.osid.jamocha.nil.acknowledgement.billing.UnknownBilling();


    /**
     *  Constructs a new <code>AbstractFederatingCreditLookupSession</code>.
     */

    protected AbstractFederatingCreditLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.acknowledgement.CreditLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Gets the <code>Billing/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Billing Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBillingId() {
        return (this.billing.getId());
    }


    /**
     *  Gets the <code>Billing</code> associated with this 
     *  session.
     *
     *  @return the <code>Billing</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.billing);
    }


    /**
     *  Sets the <code>Billing</code>.
     *
     *  @param  billing the billing for this session
     *  @throws org.osid.NullArgumentException <code>billing</code>
     *          is <code>null</code>
     */

    protected void setBilling(org.osid.acknowledgement.Billing billing) {
        nullarg(billing, "billing");
        this.billing = billing;
        return;
    }

    
    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Credit</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCredits() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            if (session.canLookupCredits()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Credit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCreditView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.useComparativeCreditView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Credit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCreditView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.usePlenaryCreditView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include credits in billings which are children
     *  of this billing in the billing hierarchy.
     */

    @OSID @Override
    public void useFederatedBillingView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.useFederatedBillingView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this billing only.
     */

    @OSID @Override
    public void useIsolatedBillingView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.useIsolatedBillingView();
        }

        return;
    }


    /**
     *  Only credits whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveCreditView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.useEffectiveCreditView();
        }

        return;
    }


    /**
     *  All credits of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCreditView() {
        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            session.useAnyEffectiveCreditView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Credit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Credit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Credit</code> and
     *  retained for compatibility.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @param  creditId <code>Id</code> of the
     *          <code>Credit</code>
     *  @return the credit
     *  @throws org.osid.NotFoundException <code>creditId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>creditId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Credit getCredit(org.osid.id.Id creditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            try {
                return (session.getCredit(creditId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(creditId + " not found");
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  credits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Credits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  creditIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>creditIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByIds(org.osid.id.IdList creditIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.acknowledgement.credit.MutableCreditList ret = new net.okapia.osid.jamocha.acknowledgement.credit.MutableCreditList();

        try (org.osid.id.IdList ids = creditIds) {
            while (ids.hasNext()) {
                ret.addCredit(getCredit(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> which does not include
     *  credits of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusType(creditGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> corresponding to the given
     *  credit genus <code>Type</code> and include any additional
     *  credits with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @param  creditGenusType a credit genus type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByParentGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByParentGenusType(creditGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> containing the given
     *  credit record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @param  creditRecordType a credit record type 
     *  @return the returned <code>Credit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByRecordType(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByRecordType(creditRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *  
     *  In active mode, credits are returned that are currently
     *  active. In any status mode, active and inactive credits
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Credit</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of credits of the given genus type and effective entire 
     *  given date range inclusive but not confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known credits or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  credits that are accessible through this session. 
     *  
     *  In effective mode, credits are returned that are currently effective 
     *  in addition to being effective in the given date range. In any 
     *  effective mode, effective credits and those currently expired are 
     *  returned. 
     *
     *  @param  creditGenusType a credit genus <code> Type </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> CreditList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> creditGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeOnDate(org.osid.type.Type creditGenusType, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeOnDate(creditGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.acknowledgement.CreditList getCreditsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceOnDate(org.osid.id.Id resourceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by a genus type for a resource.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException <code> creditGenusType </code>
     *          or <code> resourceId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResource(org.osid.id.Id resourceId,
                                                                                org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForResource(resourceId, creditGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by genus type for a resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.type.Type creditGenusType,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceOnDate(resourceId, creditGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.acknowledgement.CreditList getCreditsForReference(org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForReference(referenceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForReferenceOnDate(referenceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits by a genus type for a reference.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> or
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReference(org.osid.id.Id referenceId,
                                                                                 org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForReference(referenceId, creditGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits of the given genus type for a reference
     *  and effective entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known credits
     *  or an error results. Otherwise, the returned list may contain
     *  only those credits that are accessible through this session.
     *
     *  In effective mode, credits are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code>
     *  @param  creditGenusType a credit genus <code> Type </code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> referenceId,
     *          creditGenusType, from, </code> or <code> to </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

        @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId,
                                                                                       org.osid.type.Type creditGenusType,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForReferenceOnDate(referenceId, creditGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReference(org.osid.id.Id resourceId,
                                                                                 org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForResourceAndReference(resourceId, referenceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of credits corresponding to resource and reference
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible
     *  through this session.
     *
     *  In effective mode, credits are returned that are
     *  currently effective.  In any effective mode, effective
     *  credits and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>referenceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id referenceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsForResourceAndReferenceOnDate(resourceId, referenceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> of the given genus type for
     *  the given resource and reference.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given resource and reference, including
     *  duplicates, or an error results if a credit is inaccessible.
     *  Otherwise, inaccessible <code> Credits </code> may be omitted
     *  from the list.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @return the returned <code>CreditList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code> or
     *          <code>creditGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReference(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id referenceId,
                                                                                            org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceAndReference(resourceId, referenceId, creditGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CreditList</code> of the given genus type corresponding
     *  to the given resource and reference and effective entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all of the credits
     *  corresponding to the given peer, including duplicates, or an error
     *  results if a credit is inaccessible. Otherwise, inaccessible <code>
     *  Credits </code> may be omitted from the list.
     *
     *  In effective mode, credits are returned that are currently effective
     *  in addition to being effective in the given date range. In any
     *  effective mode, effective credits and those currently expired are
     *  returned.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  referenceId a reference <code>Id</code>
     *  @param  creditGenusType a credit genus <code>Type</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code> CreditList </code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>referenceId</code>,
     *          <code>creditGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCreditsByGenusTypeForResourceAndReferenceOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id referenceId,
                                                                                                  org.osid.type.Type creditGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCreditsByGenusTypeForResourceAndReferenceOnDate(resourceId, referenceId, creditGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Credits</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  credits or an error results. Otherwise, the returned list
     *  may contain only those credits that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, credits are returned that are currently
     *  effective.  In any effective mode, effective credits and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Credits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList ret = getCreditList();

        for (org.osid.acknowledgement.CreditLookupSession session : getSessions()) {
            ret.addCreditList(session.getCredits());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.FederatingCreditList getCreditList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.ParallelCreditList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.acknowledgement.credit.CompositeCreditList());
        }
    }
}
