//
// InvariantMapContactEnablerLookupSession
//
//    Implements a ContactEnabler lookup service backed by a fixed collection of
//    contactEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.rules;


/**
 *  Implements a ContactEnabler lookup service backed by a fixed
 *  collection of contact enablers. The contact enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapContactEnablerLookupSession
    extends net.okapia.osid.jamocha.core.contact.rules.spi.AbstractMapContactEnablerLookupSession
    implements org.osid.contact.rules.ContactEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapContactEnablerLookupSession</code> with no
     *  contact enablers.
     *  
     *  @param addressBook the address book
     *  @throws org.osid.NullArgumnetException {@code addressBook} is
     *          {@code null}
     */

    public InvariantMapContactEnablerLookupSession(org.osid.contact.AddressBook addressBook) {
        setAddressBook(addressBook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactEnablerLookupSession</code> with a single
     *  contact enabler.
     *  
     *  @param addressBook the address book
     *  @param contactEnabler a single contact enabler
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contactEnabler} is <code>null</code>
     */

      public InvariantMapContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                               org.osid.contact.rules.ContactEnabler contactEnabler) {
        this(addressBook);
        putContactEnabler(contactEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactEnablerLookupSession</code> using an array
     *  of contact enablers.
     *  
     *  @param addressBook the address book
     *  @param contactEnablers an array of contact enablers
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contactEnablers} is <code>null</code>
     */

      public InvariantMapContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                               org.osid.contact.rules.ContactEnabler[] contactEnablers) {
        this(addressBook);
        putContactEnablers(contactEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapContactEnablerLookupSession</code> using a
     *  collection of contact enablers.
     *
     *  @param addressBook the address book
     *  @param contactEnablers a collection of contact enablers
     *  @throws org.osid.NullArgumentException {@code addressBook} or
     *          {@code contactEnablers} is <code>null</code>
     */

      public InvariantMapContactEnablerLookupSession(org.osid.contact.AddressBook addressBook,
                                               java.util.Collection<? extends org.osid.contact.rules.ContactEnabler> contactEnablers) {
        this(addressBook);
        putContactEnablers(contactEnablers);
        return;
    }
}
