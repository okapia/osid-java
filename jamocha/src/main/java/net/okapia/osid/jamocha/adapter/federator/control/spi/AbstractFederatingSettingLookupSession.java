//
// AbstractFederatingSettingLookupSession.java
//
//     An abstract federating adapter for a SettingLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SettingLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSettingLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.SettingLookupSession>
    implements org.osid.control.SettingLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingSettingLookupSession</code>.
     */

    protected AbstractFederatingSettingLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.SettingLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Setting</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSettings() {
        for (org.osid.control.SettingLookupSession session : getSessions()) {
            if (session.canLookupSettings()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Setting</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSettingView() {
        for (org.osid.control.SettingLookupSession session : getSessions()) {
            session.useComparativeSettingView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Setting</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySettingView() {
        for (org.osid.control.SettingLookupSession session : getSessions()) {
            session.usePlenarySettingView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include settings in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.SettingLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.SettingLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Setting</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Setting</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Setting</code> and
     *  retained for compatibility.
     *
     *  @param  settingId <code>Id</code> of the
     *          <code>Setting</code>
     *  @return the setting
     *  @throws org.osid.NotFoundException <code>settingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>settingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting(org.osid.id.Id settingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            try {
                return (session.getSetting(settingId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(settingId + " not found");
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  settings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Settings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSettings()</code>.
     *
     *  @param  settingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>settingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByIds(org.osid.id.IdList settingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.setting.MutableSettingList ret = new net.okapia.osid.jamocha.control.setting.MutableSettingList();

        try (org.osid.id.IdList ids = settingIds) {
            while (ids.hasNext()) {
                ret.addSetting(getSetting(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  setting genus <code>Type</code> which does not include
     *  settings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList ret = getSettingList();

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            ret.addSettingList(session.getSettingsByGenusType(settingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SettingList</code> corresponding to the given
     *  setting genus <code>Type</code> and include any additional
     *  settings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingGenusType a setting genus type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByParentGenusType(org.osid.type.Type settingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList ret = getSettingList();

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            ret.addSettingList(session.getSettingsByParentGenusType(settingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SettingList</code> containing the given
     *  setting record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSettings()</code>.
     *
     *  @param  settingRecordType a setting record type 
     *  @return the returned <code>Setting</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsByRecordType(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList ret = getSettingList();

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            ret.addSettingList(session.getSettingsByRecordType(settingRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of settings for a controller. <code> </code>
     *
     *  In plenary mode, the returned list contains all known settings
     *  or an error results. Otherwise, the returned list may contain
     *  only those settings that are accessible through this session.
     *
     *  @param  controllerId a controller <code> Id </code>
     *  @return the returned <code> Setting </code> list
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.SettingList getSettingsForController(org.osid.id.Id controllerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList ret = getSettingList();

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            ret.addSettingList(session.getSettingsForController(controllerId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Settings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  settings or an error results. Otherwise, the returned list
     *  may contain only those settings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Settings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList ret = getSettingList();

        for (org.osid.control.SettingLookupSession session : getSessions()) {
            ret.addSettingList(session.getSettings());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.setting.FederatingSettingList getSettingList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.setting.ParallelSettingList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.setting.CompositeSettingList());
        }
    }
}
