//
// AbstractAssessmentPartQuery.java
//
//     A template for making an AssessmentPart Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for assessment parts.
 */

public abstract class AbstractAssessmentPartQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.assessment.authoring.AssessmentPartQuery {

    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartQueryRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchParentAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearParentAssessmentPartIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParentAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParentAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getParentAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsParentAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any parent assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          parent, <code> false </code> to match assessment parts with no 
     *          parents 
     */

    @OSID @Override
    public void matchAnyParentAssessmentPart(boolean match) {
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearParentAssessmentPartTerms() {
        return;
    }


    /**
     *  Matches assessment parts that are also used as sections. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSection(boolean match) {
        return;
    }


    /**
     *  Clears all section terms. 
     */

    @OSID @Override
    public void clearSectionTerms() {
        return;
    }


    /**
     *  Matches assessment parts that fall in between the given weights 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchWeight(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches assessment parts with any weight assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          wieght, <code> false </code> to match assessment parts with no 
     *          weight 
     */

    @OSID @Override
    public void matchAnyWeight(boolean match) {
        return;
    }


    /**
     *  Clears all weight terms. 
     */

    @OSID @Override
    public void clearWeightTerms() {
        return;
    }


    /**
     *  Matches assessment parts hose allocated time falls in between the 
     *  given times inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchAllocatedTime(org.osid.calendaring.Duration low, 
                                   org.osid.calendaring.Duration high, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches assessment parts with any time assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          alloocated time, <code> false </code> to match assessment 
     *          parts with no allocated time 
     */

    @OSID @Override
    public void matchAnyAllocatedTime(boolean match) {
        return;
    }


    /**
     *  Clears all allocated time terms. 
     */

    @OSID @Override
    public void clearAllocatedTimeTerms() {
        return;
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchChildAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearChildAssessmentPartIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChildAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChildAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getChildAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsChildAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any child assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          children, <code> false </code> to match assessment parts with 
     *          no children 
     */

    @OSID @Override
    public void matchAnyChildAssessmentPart(boolean match) {
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearChildAssessmentPartTerms() {
        return;
    }


    /**
     *  Matches constrainers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given assessment part query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment part implementing the requested record.
     *
     *  @param assessmentPartRecordType an assessment part record type
     *  @return the assessment part query record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentPartRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartQueryRecord getAssessmentPartQueryRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartQueryRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment part query. 
     *
     *  @param assessmentPartQueryRecord assessment part query record
     *  @param assessmentPartRecordType assessmentPart record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentPartQueryRecord(org.osid.assessment.authoring.records.AssessmentPartQueryRecord assessmentPartQueryRecord, 
                                                org.osid.type.Type assessmentPartRecordType) {

        addRecordType(assessmentPartRecordType);
        nullarg(assessmentPartQueryRecord, "assessment part query record");
        this.records.add(assessmentPartQueryRecord);        
        return;
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {

    }    
}
