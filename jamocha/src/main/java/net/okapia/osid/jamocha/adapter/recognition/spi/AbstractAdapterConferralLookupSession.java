//
// AbstractAdapterConferralLookupSession.java
//
//    A Conferral lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Conferral lookup session adapter.
 */

public abstract class AbstractAdapterConferralLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recognition.ConferralLookupSession {

    private final org.osid.recognition.ConferralLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterConferralLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterConferralLookupSession(org.osid.recognition.ConferralLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Academy/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Academy Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the {@code Academy} associated with this session.
     *
     *  @return the {@code Academy} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform {@code Conferral} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupConferrals() {
        return (this.session.canLookupConferrals());
    }


    /**
     *  A complete view of the {@code Conferral} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeConferralView() {
        this.session.useComparativeConferralView();
        return;
    }


    /**
     *  A complete view of the {@code Conferral} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryConferralView() {
        this.session.usePlenaryConferralView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include conferrals in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    

    /**
     *  Only conferrals whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveConferralView() {
        this.session.useEffectiveConferralView();
        return;
    }
    

    /**
     *  All conferrals of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveConferralView() {
        this.session.useAnyEffectiveConferralView();
        return;
    }

     
    /**
     *  Gets the {@code Conferral} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Conferral} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Conferral} and
     *  retained for compatibility.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param conferralId {@code Id} of the {@code Conferral}
     *  @return the conferral
     *  @throws org.osid.NotFoundException {@code conferralId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code conferralId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Conferral getConferral(org.osid.id.Id conferralId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferral(conferralId));
    }


    /**
     *  Gets a {@code ConferralList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  conferrals specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Conferrals} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Conferral} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code conferralIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByIds(org.osid.id.IdList conferralIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByIds(conferralIds));
    }


    /**
     *  Gets a {@code ConferralList} corresponding to the given
     *  conferral genus {@code Type} which does not include
     *  conferrals of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned {@code Conferral} list
     *  @throws org.osid.NullArgumentException
     *          {@code conferralGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByGenusType(conferralGenusType));
    }


    /**
     *  Gets a {@code ConferralList} corresponding to the given
     *  conferral genus {@code Type} and include any additional
     *  conferrals with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralGenusType a conferral genus type 
     *  @return the returned {@code Conferral} list
     *  @throws org.osid.NullArgumentException
     *          {@code conferralGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByParentGenusType(org.osid.type.Type conferralGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByParentGenusType(conferralGenusType));
    }


    /**
     *  Gets a {@code ConferralList} containing the given
     *  conferral record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return the returned {@code Conferral} list
     *  @throws org.osid.NullArgumentException
     *          {@code conferralRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByRecordType(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByRecordType(conferralRecordType));
    }


    /**
     *  Gets a {@code ConferralList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In active mode, conferrals are returned that are currently
     *  active. In any status mode, active and inactive conferrals are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Conferral} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsOnDate(org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsOnDate(from, to));
    }
        

    /**
     *  Gets a list of conferrals corresponding to a recipient
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForRecipient(resourceId));
    }


    /**
     *  Gets a list of conferrals corresponding to a recipient {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForRecipientOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to an award {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  awardId the {@code Id} of the award
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code awardId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForAward(awardId));
    }


    /**
     *  Gets a list of conferrals corresponding to an award {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  awardId the {@code Id} of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code awardId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForAwardOnDate(org.osid.id.Id awardId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForAwardOnDate(awardId, from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the recipient
     *  @param  awardId the {@code Id} of the award
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code awardId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAward(org.osid.id.Id resourceId,
                                                                                org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForRecipientAndAward(resourceId, awardId));
    }


    /**
     *  Gets a list of conferrals corresponding to recipient and award
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  awardId the {@code Id} of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ConferralList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code awardId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsForRecipientAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsForRecipientAndAwardOnDate(resourceId, awardId, from, to));
    }


    /**
     *  Gets a list of conferrals corresponding to a reference {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  referenceId the {@code Id} of the reference 
     *  @return the returned {@code ConferralList} 
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByReference(referenceId));
    }


    /**
     *  Gets a list of all conferrals corresponding to a reference
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code ConferralList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less
     *          than {@code from}
     *  @throws org.osid.NullArgumentException {@code referenceId,
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByReferenceOnDate(org.osid.id.Id referenceId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByReferenceOnDate(referenceId, from, to));
    }


    /**
     *  Gets a list of all conferrals corresponding to a convocation
     *  {@code Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  convocationId the {@code Id} of the convocation 
     *  @return the returned {@code ConferralList} 
     *  @throws org.osid.NullArgumentException {@code convocationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocation(org.osid.id.Id convocationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferralsByConvocation(convocationId));
    }


    /**
     *  Gets a list of all conferrals corresponding to a convocation
     * {@code Id } and effective during the entire given date range
     * inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session.
     *  
     *  In effective mode, conferrals are returned that are currently
     *  effective. In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @param  convocationId a convocation {@code Id} 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned {@code ConferralList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code convocationId,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferralsByConvocationOnDate(org.osid.id.Id convocationId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.getConferralsByConvocationOnDate(convocationId, from, to));
    }
    
    
    /**
     *  Gets all {@code Conferrals}. 
     *
     *  In plenary mode, the returned list contains all known
     *  conferrals or an error results. Otherwise, the returned list
     *  may contain only those conferrals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, conferrals are returned that are currently
     *  effective.  In any effective mode, effective conferrals and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Conferrals} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConferralList getConferrals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getConferrals());
    }
}
