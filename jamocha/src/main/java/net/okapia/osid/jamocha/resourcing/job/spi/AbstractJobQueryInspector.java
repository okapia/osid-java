//
// AbstractJobQueryInspector.java
//
//     A template for making a JobQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for jobs.
 */

public abstract class AbstractJobQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.resourcing.JobQueryInspector {

    private final java.util.Collection<org.osid.resourcing.records.JobQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the competency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompetencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the competency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQueryInspector[] getCompetencyTerms() {
        return (new org.osid.resourcing.CompetencyQueryInspector[0]);
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.resourcing.WorkQueryInspector[0]);
    }


    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAvailabilityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given job query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a job implementing the requested record.
     *
     *  @param jobRecordType a job record type
     *  @return the job query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobQueryInspectorRecord getJobQueryInspectorRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job query. 
     *
     *  @param jobQueryInspectorRecord job query inspector
     *         record
     *  @param jobRecordType job record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobQueryInspectorRecord(org.osid.resourcing.records.JobQueryInspectorRecord jobQueryInspectorRecord, 
                                                   org.osid.type.Type jobRecordType) {

        addRecordType(jobRecordType);
        nullarg(jobRecordType, "job record type");
        this.records.add(jobQueryInspectorRecord);        
        return;
    }
}
