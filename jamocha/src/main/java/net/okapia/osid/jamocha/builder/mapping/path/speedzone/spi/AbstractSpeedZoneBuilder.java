//
// AbstractSpeedZone.java
//
//     Defines a SpeedZone builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.speedzone.spi;


/**
 *  Defines a <code>SpeedZone</code> builder.
 */

public abstract class AbstractSpeedZoneBuilder<T extends AbstractSpeedZoneBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.path.speedzone.SpeedZoneMiter speedZone;


    /**
     *  Constructs a new <code>AbstractSpeedZoneBuilder</code>.
     *
     *  @param speedZone the speed zone to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSpeedZoneBuilder(net.okapia.osid.jamocha.builder.mapping.path.speedzone.SpeedZoneMiter speedZone) {
        super(speedZone);
        this.speedZone = speedZone;
        return;
    }


    /**
     *  Builds the speed zone.
     *
     *  @return the new speed zone
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.path.SpeedZone build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.path.speedzone.SpeedZoneValidator(getValidations())).validate(this.speedZone);
        return (new net.okapia.osid.jamocha.builder.mapping.path.speedzone.ImmutableSpeedZone(this.speedZone));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the speed zone miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.path.speedzone.SpeedZoneMiter getMiter() {
        return (this.speedZone);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(org.osid.mapping.path.Path path) {
        getMiter().setPath(path);
        return (self());
    }


    /**
     *  Sets the starting coordinate.
     *
     *  @param coordinate a starting coordinate
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    public T startingCoordinate(org.osid.mapping.Coordinate coordinate) {
        getMiter().setStartingCoordinate(coordinate);
        return (self());
    }


    /**
     *  Sets the ending coordinate.
     *
     *  @param coordinate an ending coordinate
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public T endingCoordinate(org.osid.mapping.Coordinate coordinate) {
        getMiter().setEndingCoordinate(coordinate);
        return (self());
    }


    /**
     *  Sets the implicit flag.
     *
     *  @return the builder
     */

    public T implicit() {
        getMiter().setImplicit(true);
        return (self());
    }


    /**
     *  Unsets the implicit flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setImplicit(false);
        return (self());
    }


    /**
     *  Sets the speed limit.
     *
     *  @param limit a speed limit
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>limit</code> is
     *          <code>null</code>
     */

    public T speedLimit(org.osid.mapping.Speed limit) {
        getMiter().setSpeedLimit(limit);
        return (self());
    }


    /**
     *  Adds a SpeedZone record.
     *
     *  @param record a speed zone record
     *  @param recordType the type of speed zone record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.path.records.SpeedZoneRecord record, org.osid.type.Type recordType) {
        getMiter().addSpeedZoneRecord(record, recordType);
        return (self());
    }
}       


