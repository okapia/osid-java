//
// AbstractQueryDepotLookupSession.java
//
//    An inline adapter that maps a DepotLookupSession to
//    a DepotQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DepotLookupSession to
 *  a DepotQuerySession.
 */

public abstract class AbstractQueryDepotLookupSession
    extends net.okapia.osid.jamocha.installation.spi.AbstractDepotLookupSession
    implements org.osid.installation.DepotLookupSession {

    private final org.osid.installation.DepotQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDepotLookupSession.
     *
     *  @param querySession the underlying depot query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDepotLookupSession(org.osid.installation.DepotQuerySession querySession) {
        nullarg(querySession, "depot query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Depot</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDepots() {
        return (this.session.canSearchDepots());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Depot</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Depot</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Depot</code> and
     *  retained for compatibility.
     *
     *  @param  depotId <code>Id</code> of the
     *          <code>Depot</code>
     *  @return the depot
     *  @throws org.osid.NotFoundException <code>depotId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>depotId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchId(depotId, true);
        org.osid.installation.DepotList depots = this.session.getDepotsByQuery(query);
        if (depots.hasNext()) {
            return (depots.getNextDepot());
        } 
        
        throw new org.osid.NotFoundException(depotId + " not found");
    }


    /**
     *  Gets a <code>DepotList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  depots specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Depots</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  depotIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Depot</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>depotIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByIds(org.osid.id.IdList depotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();

        try (org.osid.id.IdList ids = depotIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDepotsByQuery(query));
    }


    /**
     *  Gets a <code>DepotList</code> corresponding to the given
     *  depot genus <code>Type</code> which does not include
     *  depots of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotGenusType a depot genus type 
     *  @return the returned <code>Depot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>depotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByGenusType(org.osid.type.Type depotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchGenusType(depotGenusType, true);
        return (this.session.getDepotsByQuery(query));
    }


    /**
     *  Gets a <code>DepotList</code> corresponding to the given
     *  depot genus <code>Type</code> and include any additional
     *  depots with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotGenusType a depot genus type 
     *  @return the returned <code>Depot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>depotGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByParentGenusType(org.osid.type.Type depotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchParentGenusType(depotGenusType, true);
        return (this.session.getDepotsByQuery(query));
    }


    /**
     *  Gets a <code>DepotList</code> containing the given
     *  depot record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  depotRecordType a depot record type 
     *  @return the returned <code>Depot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByRecordType(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchRecordType(depotRecordType, true);
        return (this.session.getDepotsByQuery(query));
    }


    /**
     *  Gets a <code>DepotList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known depots or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  depots that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Depot</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepotsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getDepotsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Depots</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  depots or an error results. Otherwise, the returned list
     *  may contain only those depots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Depots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.DepotList getDepots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.installation.DepotQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDepotsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.installation.DepotQuery getQuery() {
        org.osid.installation.DepotQuery query = this.session.getDepotQuery();
        return (query);
    }
}
