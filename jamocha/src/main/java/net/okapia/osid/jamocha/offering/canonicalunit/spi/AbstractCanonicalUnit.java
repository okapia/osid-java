//
// AbstractCanonicalUnit.java
//
//     Defines a CanonicalUnit.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CanonicalUnit</code>.
 */

public abstract class AbstractCanonicalUnit
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObject
    implements org.osid.offering.CanonicalUnit {

    private org.osid.locale.DisplayText title;
    private String code;
    private final java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> offeredCyclicTimePeriods = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.GradeSystem> resultOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.offering.records.CanonicalUnitRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the title for this canonical unit. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the code for this canonical unit. 
     *
     *  @return the code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "code");
        this.code = code;
        return;
    }


    /**
     *  Gets the cyclic period <code> Ids </code> in which this CU can be 
     *  offered. If there are no time cycles, then it can be offered any time. 
     *
     *  @return a list of cyclic period <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getOfferedCyclicTimePeriodIds() {

        try {
            org.osid.calendaring.cycle.CyclicTimePeriodList periods = getOfferedCyclicTimePeriods();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.cycle.cyclictimeperiod.CyclicTimePeriodToIdList(periods));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the cyclic periods in which this CU can be offered. If there are 
     *  no time cycles, then it can be offered any time. 
     *
     *  @return a list of cyclic periods 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getOfferedCyclicTimePeriods()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.ArrayCyclicTimePeriodList(this.offeredCyclicTimePeriods));
    }


    /**
     *  Adds an offered cyclic time period.
     *
     *  @param offeredCyclicTimePeriod an offered cyclic time period
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriod</code> is <code>null</code>
     */

    protected void addOfferedCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod offeredCyclicTimePeriod) {
        nullarg(offeredCyclicTimePeriod, "cyclic time period");
        this.offeredCyclicTimePeriods.add(offeredCyclicTimePeriod);
        return;
    }


    /**
     *  Sets all the offered cyclic time periods.
     *
     *  @param offeredCyclicTimePeriods a collection of offered cyclic time periods
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriods</code> is <code>null</code>
     */

    protected void setOfferedCyclicTimePeriods(java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> offeredCyclicTimePeriods) {
        nullarg(offeredCyclicTimePeriods, "cyclic time periods");
        this.offeredCyclicTimePeriods.clear();
        this.offeredCyclicTimePeriods.addAll(offeredCyclicTimePeriods);
        return;
    }


    /**
     *  Tests if this canonical has results when offered. 
     *
     *  @return <code> true </code> if this canonical has results, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.resultOptions.size() > 0);
    }


    /**
     *  Gets the various result option <code> Ids </code> allowed for results. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasResults() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        try {
            org.osid.grading.GradeSystemList resultOptions = getResultOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(resultOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various result options allowed for this canonical unit. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> hasResults() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        if (!hasResults()) {
            throw new org.osid.IllegalStateException("hasResults() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.resultOptions));
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    protected void addResultOption(org.osid.grading.GradeSystem resultOption) {
        nullarg(resultOption, "result option");
        this.resultOptions.add(resultOption);
        return;
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    protected void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        nullarg(resultOptions, "result options");
        this.resultOptions.clear();
        this.resultOptions.addAll(resultOptions);
        return;
    }


    /**
     *  Tests if this canonical has sponsors. 
     *
     *  @return <code> true </code> if this canonical has sponsors, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.sponsors.size() > 0);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() ia false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");
        this.sponsors.add(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");
        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        return;
    }


    /**
     *  Tests if this canonicalUnit supports the given record
     *  <code>Type</code>.
     *
     *  @param  canonicalUnitRecordType a canonical unit record type 
     *  @return <code>true</code> if the canonicalUnitRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitRecordType) {
        for (org.osid.offering.records.CanonicalUnitRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  canonicalUnitRecordType the canonical unit record type 
     *  @return the canonical unit record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitRecord getCanonicalUnitRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitRecord the canonical unit record
     *  @param canonicalUnitRecordType canonical unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecord</code> or
     *          <code>canonicalUnitRecordTypecanonicalUnit</code> is
     *          <code>null</code>
     */
            
    protected void addCanonicalUnitRecord(org.osid.offering.records.CanonicalUnitRecord canonicalUnitRecord, 
                                          org.osid.type.Type canonicalUnitRecordType) {

        nullarg(canonicalUnitRecord, "canonical unit record");
        addRecordType(canonicalUnitRecordType);
        this.records.add(canonicalUnitRecord);
        
        return;
    }
}
