//
// AbstractIndexedMapRelevancyLookupSession.java
//
//    A simple framework for providing a Relevancy lookup service
//    backed by a fixed collection of relevancies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Relevancy lookup service backed by a
 *  fixed collection of relevancies. The relevancies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some relevancies may be compatible
 *  with more types than are indicated through these relevancy
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Relevancies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRelevancyLookupSession
    extends AbstractMapRelevancyLookupSession
    implements org.osid.ontology.RelevancyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ontology.Relevancy> relevanciesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Relevancy>());
    private final MultiMap<org.osid.type.Type, org.osid.ontology.Relevancy> relevanciesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Relevancy>());


    /**
     *  Makes a <code>Relevancy</code> available in this session.
     *
     *  @param  relevancy a relevancy
     *  @throws org.osid.NullArgumentException <code>relevancy<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRelevancy(org.osid.ontology.Relevancy relevancy) {
        super.putRelevancy(relevancy);

        this.relevanciesByGenus.put(relevancy.getGenusType(), relevancy);
        
        try (org.osid.type.TypeList types = relevancy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relevanciesByRecord.put(types.getNextType(), relevancy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a relevancy from this session.
     *
     *  @param relevancyId the <code>Id</code> of the relevancy
     *  @throws org.osid.NullArgumentException <code>relevancyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRelevancy(org.osid.id.Id relevancyId) {
        org.osid.ontology.Relevancy relevancy;
        try {
            relevancy = getRelevancy(relevancyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.relevanciesByGenus.remove(relevancy.getGenusType());

        try (org.osid.type.TypeList types = relevancy.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relevanciesByRecord.remove(types.getNextType(), relevancy);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRelevancy(relevancyId);
        return;
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  relevancy genus <code>Type</code> which does not include
     *  relevancies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known relevancies or an error results. Otherwise,
     *  the returned list may contain only those relevancies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.relevancy.ArrayRelevancyList(this.relevanciesByGenus.get(relevancyGenusType)));
    }


    /**
     *  Gets a <code>RelevancyList</code> containing the given
     *  relevancy record <code>Type</code>. In plenary mode, the
     *  returned list contains all known relevancies or an error
     *  results. Otherwise, the returned list may contain only those
     *  relevancies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return the returned <code>relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByRecordType(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.relevancy.ArrayRelevancyList(this.relevanciesByRecord.get(relevancyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relevanciesByGenus.clear();
        this.relevanciesByRecord.clear();

        super.close();

        return;
    }
}
