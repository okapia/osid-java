//
// AbstractIndexedMapQueueConstrainerLookupSession.java
//
//    A simple framework for providing a QueueConstrainer lookup service
//    backed by a fixed collection of queue constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a QueueConstrainer lookup service backed by a
 *  fixed collection of queue constrainers. The queue constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some queue constrainers may be compatible
 *  with more types than are indicated through these queue constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQueueConstrainerLookupSession
    extends AbstractMapQueueConstrainerLookupSession
    implements org.osid.provisioning.rules.QueueConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueConstrainer> queueConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueConstrainer> queueConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueConstrainer>());


    /**
     *  Makes a <code>QueueConstrainer</code> available in this session.
     *
     *  @param  queueConstrainer a queue constrainer
     *  @throws org.osid.NullArgumentException <code>queueConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQueueConstrainer(org.osid.provisioning.rules.QueueConstrainer queueConstrainer) {
        super.putQueueConstrainer(queueConstrainer);

        this.queueConstrainersByGenus.put(queueConstrainer.getGenusType(), queueConstrainer);
        
        try (org.osid.type.TypeList types = queueConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueConstrainersByRecord.put(types.getNextType(), queueConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a queue constrainer from this session.
     *
     *  @param queueConstrainerId the <code>Id</code> of the queue constrainer
     *  @throws org.osid.NullArgumentException <code>queueConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQueueConstrainer(org.osid.id.Id queueConstrainerId) {
        org.osid.provisioning.rules.QueueConstrainer queueConstrainer;
        try {
            queueConstrainer = getQueueConstrainer(queueConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.queueConstrainersByGenus.remove(queueConstrainer.getGenusType());

        try (org.osid.type.TypeList types = queueConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueConstrainersByRecord.remove(types.getNextType(), queueConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQueueConstrainer(queueConstrainerId);
        return;
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> corresponding to the given
     *  queue constrainer genus <code>Type</code> which does not include
     *  queue constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known queue constrainers or an error results. Otherwise,
     *  the returned list may contain only those queue constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  queueConstrainerGenusType a queue constrainer genus type 
     *  @return the returned <code>QueueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerList getQueueConstrainersByGenusType(org.osid.type.Type queueConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.ArrayQueueConstrainerList(this.queueConstrainersByGenus.get(queueConstrainerGenusType)));
    }


    /**
     *  Gets a <code>QueueConstrainerList</code> containing the given
     *  queue constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known queue constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  queue constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  queueConstrainerRecordType a queue constrainer record type 
     *  @return the returned <code>queueConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerList getQueueConstrainersByRecordType(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.ArrayQueueConstrainerList(this.queueConstrainersByRecord.get(queueConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueConstrainersByGenus.clear();
        this.queueConstrainersByRecord.clear();

        super.close();

        return;
    }
}
