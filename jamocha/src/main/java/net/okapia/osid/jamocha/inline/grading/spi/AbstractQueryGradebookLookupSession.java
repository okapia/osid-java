//
// AbstractQueryGradebookLookupSession.java
//
//    An inline adapter that maps a GradebookLookupSession to
//    a GradebookQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a GradebookLookupSession to
 *  a GradebookQuerySession.
 */

public abstract class AbstractQueryGradebookLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradebookLookupSession
    implements org.osid.grading.GradebookLookupSession {

    private final org.osid.grading.GradebookQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryGradebookLookupSession.
     *
     *  @param querySession the underlying gradebook query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryGradebookLookupSession(org.osid.grading.GradebookQuerySession querySession) {
        nullarg(querySession, "gradebook query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Gradebook</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradebooks() {
        return (this.session.canSearchGradebooks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Gradebook</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Gradebook</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Gradebook</code> and
     *  retained for compatibility.
     *
     *  @param  gradebookId <code>Id</code> of the
     *          <code>Gradebook</code>
     *  @return the gradebook
     *  @throws org.osid.NotFoundException <code>gradebookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradebookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchId(gradebookId, true);
        org.osid.grading.GradebookList gradebooks = this.session.getGradebooksByQuery(query);
        if (gradebooks.hasNext()) {
            return (gradebooks.getNextGradebook());
        } 
        
        throw new org.osid.NotFoundException(gradebookId + " not found");
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebooks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Gradebooks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  gradebookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByIds(org.osid.id.IdList gradebookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();

        try (org.osid.id.IdList ids = gradebookIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getGradebooksByQuery(query));
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  gradebook genus <code>Type</code> which does not include
     *  gradebooks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchGenusType(gradebookGenusType, true);
        return (this.session.getGradebooksByQuery(query));
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  gradebook genus <code>Type</code> and include any additional
     *  gradebooks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByParentGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchParentGenusType(gradebookGenusType, true);
        return (this.session.getGradebooksByQuery(query));
    }


    /**
     *  Gets a <code>GradebookList</code> containing the given
     *  gradebook record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByRecordType(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchRecordType(gradebookRecordType, true);
        return (this.session.getGradebooksByQuery(query));
    }


    /**
     *  Gets a <code>GradebookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known gradebooks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  gradebooks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Gradebook</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getGradebooksByQuery(query));        
    }

    
    /**
     *  Gets all <code>Gradebooks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebooks or an error results. Otherwise, the returned list
     *  may contain only those gradebooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Gradebooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradebookQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getGradebooksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.grading.GradebookQuery getQuery() {
        org.osid.grading.GradebookQuery query = this.session.getGradebookQuery();
        return (query);
    }
}
