//
// AbstractOrderingRulesProxyManager.java
//
//     An adapter for a OrderingRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OrderingRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOrderingRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.ordering.rules.OrderingRulesProxyManager>
    implements org.osid.ordering.rules.OrderingRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterOrderingRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOrderingRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOrderingRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOrderingRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerLookup() {
        return (getAdapteeManager().supportsPriceEnablerLookup());
    }


    /**
     *  Tests if querying price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerQuery() {
        return (getAdapteeManager().supportsPriceEnablerQuery());
    }


    /**
     *  Tests if searching price enablers is supported. 
     *
     *  @return <code> true </code> if price enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSearch() {
        return (getAdapteeManager().supportsPriceEnablerSearch());
    }


    /**
     *  Tests if a price enabler administrative service is supported. 
     *
     *  @return <code> true </code> if price enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerAdmin() {
        return (getAdapteeManager().supportsPriceEnablerAdmin());
    }


    /**
     *  Tests if a price enabler notification service is supported. 
     *
     *  @return <code> true </code> if price enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerNotification() {
        return (getAdapteeManager().supportsPriceEnablerNotification());
    }


    /**
     *  Tests if a price enabler store lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler store lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerStore() {
        return (getAdapteeManager().supportsPriceEnablerStore());
    }


    /**
     *  Tests if a price enabler store service is supported. 
     *
     *  @return <code> true </code> if price enabler store assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerStoreAssignment() {
        return (getAdapteeManager().supportsPriceEnablerStoreAssignment());
    }


    /**
     *  Tests if a price enabler store lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler store service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSmartStore() {
        return (getAdapteeManager().supportsPriceEnablerSmartStore());
    }


    /**
     *  Tests if a price enabler price rule lookup service is supported. 
     *
     *  @return <code> true </code> if a price enabler price rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRuleLookup() {
        return (getAdapteeManager().supportsPriceEnablerRuleLookup());
    }


    /**
     *  Tests if a price enabler price rule application service is supported. 
     *
     *  @return <code> true </code> if price enabler price rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRuleApplication() {
        return (getAdapteeManager().supportsPriceEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> PriceEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> PriceEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceEnablerRecordTypes() {
        return (getAdapteeManager().getPriceEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> PriceEnabler </code> record type is 
     *  supported. 
     *
     *  @param  priceEnablerRecordType a <code> Type </code> indicating a 
     *          <code> PriceEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priceEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceEnablerRecordType(org.osid.type.Type priceEnablerRecordType) {
        return (getAdapteeManager().supportsPriceEnablerRecordType(priceEnablerRecordType));
    }


    /**
     *  Gets the supported <code> PriceEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> PriceEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriceEnablerSearchRecordTypes() {
        return (getAdapteeManager().getPriceEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PriceEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  priceEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> PriceEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          priceEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriceEnablerSearchRecordType(org.osid.type.Type priceEnablerSearchRecordType) {
        return (getAdapteeManager().supportsPriceEnablerSearchRecordType(priceEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  lookup service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerLookupSession getPriceEnablerLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerLookupSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  query service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuerySession getPriceEnablerQuerySessionForStore(org.osid.id.Id storeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerQuerySessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enablers 
     *  earch service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSearchSession getPriceEnablerSearchSessionForStore(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerSearchSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerAdminSession getPriceEnablerAdminSessionForStore(org.osid.id.Id storeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerAdminSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSession(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerNotificationSession(priceEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  notification service for the given store. 
     *
     *  @param  priceEnablerReceiver the notification callback 
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no store found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> priceEnablerReceiver, 
     *          storeId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerNotificationSession getPriceEnablerNotificationSessionForStore(org.osid.ordering.rules.PriceEnablerReceiver priceEnablerReceiver, 
                                                                                                              org.osid.id.Id storeId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerNotificationSessionForStore(priceEnablerReceiver, storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup price enabler/store 
     *  mappings for price enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerStoreSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStore() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreSession getPriceEnablerStoreSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerStoreSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning price 
     *  enablers to stores for price. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerStoreAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerStoreAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerStoreAssignmentSession getPriceEnablerStoreAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerStoreAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage price enabler smart 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerSmartStoreSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerSmartStore() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerSmartStoreSession getPriceEnablerSmartStoreSession(org.osid.id.Id storeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerSmartStoreSession(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for looking up the rules applied to the 
     *  store. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerPriceRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price mapping lookup service for the given store for looking up rules 
     *  applied to an store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleLookupSession getPriceEnablerRuleLookupSessionForStore(org.osid.id.Id storeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerRuleLookupSessionForStore(storeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service to apply enablers to stores. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerPricApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the price enabler 
     *  price assignment service for the given store to apply enablers to 
     *  stores. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PriceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerRuleApplicationSession getPriceEnablerRuleApplicationSessionForStore(org.osid.id.Id storeId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceEnablerRuleApplicationSessionForStore(storeId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
