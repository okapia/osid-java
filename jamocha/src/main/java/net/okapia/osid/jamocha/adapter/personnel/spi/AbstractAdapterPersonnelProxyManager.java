//
// AbstractPersonnelProxyManager.java
//
//     An adapter for a PersonnelProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a PersonnelProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterPersonnelProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.personnel.PersonnelProxyManager>
    implements org.osid.personnel.PersonnelProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterPersonnelProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterPersonnelProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterPersonnelProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterPersonnelProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any realm federation is exposed. Federation is exposed when a 
     *  specific realm may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of realms 
     *  appears as a single realm. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a person lookup service. 
     *
     *  @return <code> true </code> if person lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonLookup() {
        return (getAdapteeManager().supportsPersonLookup());
    }


    /**
     *  Tests for the availability of a person query service. 
     *
     *  @return <code> true </code> if person query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (getAdapteeManager().supportsPersonQuery());
    }


    /**
     *  Tests if searching for persons is available. 
     *
     *  @return <code> true </code> if person search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonSearch() {
        return (getAdapteeManager().supportsPersonSearch());
    }


    /**
     *  Tests if managing for persons is available. 
     *
     *  @return <code> true </code> if a person adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonAdmin() {
        return (getAdapteeManager().supportsPersonAdmin());
    }


    /**
     *  Tests if person notification is available. 
     *
     *  @return <code> true </code> if person notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonNotification() {
        return (getAdapteeManager().supportsPersonNotification());
    }


    /**
     *  Tests if a person to realm lookup session is available. 
     *
     *  @return <code> true </code> if person realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonRealm() {
        return (getAdapteeManager().supportsPersonRealm());
    }


    /**
     *  Tests if a person to realm assignment session is available. 
     *
     *  @return <code> true </code> if person realm assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonRealmAssignment() {
        return (getAdapteeManager().supportsPersonRealmAssignment());
    }


    /**
     *  Tests if a person smart realm session is available. 
     *
     *  @return <code> true </code> if person smart realm is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonSmartRealm() {
        return (getAdapteeManager().supportsPersonSmartRealm());
    }


    /**
     *  Tests for the availability of an organization lookup service. 
     *
     *  @return <code> true </code> if organization lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationLookup() {
        return (getAdapteeManager().supportsOrganizationLookup());
    }


    /**
     *  Tests for the availability of an organization query service. 
     *
     *  @return <code> true </code> if organization query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (getAdapteeManager().supportsOrganizationQuery());
    }


    /**
     *  Tests if searching for organizations is available. 
     *
     *  @return <code> true </code> if organization search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSearch() {
        return (getAdapteeManager().supportsOrganizationSearch());
    }


    /**
     *  Tests if managing for organizations is available. 
     *
     *  @return <code> true </code> if an organization adminstrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationAdmin() {
        return (getAdapteeManager().supportsOrganizationAdmin());
    }


    /**
     *  Tests if organization notification is available. 
     *
     *  @return <code> true </code> if organization notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationNotification() {
        return (getAdapteeManager().supportsOrganizationNotification());
    }


    /**
     *  Tests if an organization hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an organization hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationHierarchy() {
        return (getAdapteeManager().supportsOrganizationHierarchy());
    }


    /**
     *  Tests if organization hierarchy design is supported. 
     *
     *  @return <code> true </code> if an organization hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationHierarchyDesign() {
        return (getAdapteeManager().supportsOrganizationHierarchyDesign());
    }


    /**
     *  Tests if an organization to realm lookup session is available. 
     *
     *  @return <code> true </code> if organization realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationRealm() {
        return (getAdapteeManager().supportsOrganizationRealm());
    }


    /**
     *  Tests if an organization to realm assignment session is available. 
     *
     *  @return <code> true </code> if organization realm assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationRealmAssignment() {
        return (getAdapteeManager().supportsOrganizationRealmAssignment());
    }


    /**
     *  Tests if an organization smart realm session is available. 
     *
     *  @return <code> true </code> if organization smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationSmartRealm() {
        return (getAdapteeManager().supportsOrganizationSmartRealm());
    }


    /**
     *  Tests for the availability of a position lookup service. 
     *
     *  @return <code> true </code> if position lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionLookup() {
        return (getAdapteeManager().supportsPositionLookup());
    }


    /**
     *  Tests for the availability of a position query service. 
     *
     *  @return <code> true </code> if position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (getAdapteeManager().supportsPositionQuery());
    }


    /**
     *  Tests if searching for positions is available. 
     *
     *  @return <code> true </code> if position search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionSearch() {
        return (getAdapteeManager().supportsPositionSearch());
    }


    /**
     *  Tests if managing for positions is available. 
     *
     *  @return <code> true </code> if a position adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionAdmin() {
        return (getAdapteeManager().supportsPositionAdmin());
    }


    /**
     *  Tests if position notification is available. 
     *
     *  @return <code> true </code> if position notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionNotification() {
        return (getAdapteeManager().supportsPositionNotification());
    }


    /**
     *  Tests if a position to realm lookup session is available. 
     *
     *  @return <code> true </code> if position realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionRealm() {
        return (getAdapteeManager().supportsPositionRealm());
    }


    /**
     *  Tests if a position to realm assignment session is available. 
     *
     *  @return <code> true </code> if position realm assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionRealmAssignment() {
        return (getAdapteeManager().supportsPositionRealmAssignment());
    }


    /**
     *  Tests if a position smart realm session is available. 
     *
     *  @return <code> true </code> if position smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionSmartRealm() {
        return (getAdapteeManager().supportsPositionSmartRealm());
    }


    /**
     *  Tests for the availability of an appointment lookup service. 
     *
     *  @return <code> true </code> if appointment lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentLookup() {
        return (getAdapteeManager().supportsAppointmentLookup());
    }


    /**
     *  Tests for the availability of an appointment query service. 
     *
     *  @return <code> true </code> if appointment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentQuery() {
        return (getAdapteeManager().supportsAppointmentQuery());
    }


    /**
     *  Tests if searching for appointments is available. 
     *
     *  @return <code> true </code> if appointment search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentSearch() {
        return (getAdapteeManager().supportsAppointmentSearch());
    }


    /**
     *  Tests if managing for appointments is available. 
     *
     *  @return <code> true </code> if an appointment adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentAdmin() {
        return (getAdapteeManager().supportsAppointmentAdmin());
    }


    /**
     *  Tests if appointment notification is available. 
     *
     *  @return <code> true </code> if appointment notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentNotification() {
        return (getAdapteeManager().supportsAppointmentNotification());
    }


    /**
     *  Tests if an appointment to realm lookup session is available. 
     *
     *  @return <code> true </code> if appointment realm lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentRealm() {
        return (getAdapteeManager().supportsAppointmentRealm());
    }


    /**
     *  Tests if an appointment to realm assignment session is available. 
     *
     *  @return <code> true </code> if appointment realm assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentRealmAssignment() {
        return (getAdapteeManager().supportsAppointmentRealmAssignment());
    }


    /**
     *  Tests if an appointment smart realm session is available. 
     *
     *  @return <code> true </code> if appointment smart realm is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentSmartRealm() {
        return (getAdapteeManager().supportsAppointmentSmartRealm());
    }


    /**
     *  Tests for the availability of an realm lookup service. 
     *
     *  @return <code> true </code> if realm lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmLookup() {
        return (getAdapteeManager().supportsRealmLookup());
    }


    /**
     *  Tests if querying realms is available. 
     *
     *  @return <code> true </code> if realm query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (getAdapteeManager().supportsRealmQuery());
    }


    /**
     *  Tests if searching for realms is available. 
     *
     *  @return <code> true </code> if realm search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmSearch() {
        return (getAdapteeManager().supportsRealmSearch());
    }


    /**
     *  Tests for the availability of a realm administrative service for 
     *  creating and deleting realms. 
     *
     *  @return <code> true </code> if realm administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmAdmin() {
        return (getAdapteeManager().supportsRealmAdmin());
    }


    /**
     *  Tests for the availability of a realm notification service. 
     *
     *  @return <code> true </code> if realm notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmNotification() {
        return (getAdapteeManager().supportsRealmNotification());
    }


    /**
     *  Tests for the availability of a realm hierarchy traversal service. 
     *
     *  @return <code> true </code> if realm hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmHierarchy() {
        return (getAdapteeManager().supportsRealmHierarchy());
    }


    /**
     *  Tests for the availability of a realm hierarchy design service. 
     *
     *  @return <code> true </code> if realm hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmHierarchyDesign() {
        return (getAdapteeManager().supportsRealmHierarchyDesign());
    }


    /**
     *  Tests for the availability of a personnel batch service. 
     *
     *  @return <code> true </code> if a personnel batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelBatch() {
        return (getAdapteeManager().supportsPersonnelBatch());
    }


    /**
     *  Gets the supported <code> Person </code> record types. 
     *
     *  @return a list containing the supported person record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPersonRecordTypes() {
        return (getAdapteeManager().getPersonRecordTypes());
    }


    /**
     *  Tests if the given <code> Person </code> record type is supported. 
     *
     *  @param  personRecordType a <code> Type </code> indicating a <code> 
     *          Person </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> personRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPersonRecordType(org.osid.type.Type personRecordType) {
        return (getAdapteeManager().supportsPersonRecordType(personRecordType));
    }


    /**
     *  Gets the supported person search record types. 
     *
     *  @return a list containing the supported person search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPersonSearchRecordTypes() {
        return (getAdapteeManager().getPersonSearchRecordTypes());
    }


    /**
     *  Tests if the given person search record type is supported. 
     *
     *  @param  personSearchRecordType a <code> Type </code> indicating a 
     *          person record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> personSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPersonSearchRecordType(org.osid.type.Type personSearchRecordType) {
        return (getAdapteeManager().supportsPersonSearchRecordType(personSearchRecordType));
    }


    /**
     *  Gets the supported <code> Organization </code> record types. 
     *
     *  @return a list containing the supported organization record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrganizationRecordTypes() {
        return (getAdapteeManager().getOrganizationRecordTypes());
    }


    /**
     *  Tests if the given <code> Organization </code> record type is 
     *  supported. 
     *
     *  @param  organizationRecordType a <code> Type </code> indicating an 
     *          <code> Organization </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> organizationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrganizationRecordType(org.osid.type.Type organizationRecordType) {
        return (getAdapteeManager().supportsOrganizationRecordType(organizationRecordType));
    }


    /**
     *  Gets the supported organization search record types. 
     *
     *  @return a list containing the supported organization search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOrganizationSearchRecordTypes() {
        return (getAdapteeManager().getOrganizationSearchRecordTypes());
    }


    /**
     *  Tests if the given organization search record type is supported. 
     *
     *  @param  organizationSearchRecordType a <code> Type </code> indicating 
     *          an organization record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          organizationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOrganizationSearchRecordType(org.osid.type.Type organizationSearchRecordType) {
        return (getAdapteeManager().supportsOrganizationSearchRecordType(organizationSearchRecordType));
    }


    /**
     *  Gets the supported <code> Position </code> record types. 
     *
     *  @return a list containing the supported position record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPositionRecordTypes() {
        return (getAdapteeManager().getPositionRecordTypes());
    }


    /**
     *  Tests if the given <code> Position </code> record type is supported. 
     *
     *  @param  positionRecordType a <code> Type </code> indicating a <code> 
     *          Position </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> positionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPositionRecordType(org.osid.type.Type positionRecordType) {
        return (getAdapteeManager().supportsPositionRecordType(positionRecordType));
    }


    /**
     *  Gets the supported position search record types. 
     *
     *  @return a list containing the supported position search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPositionSearchRecordTypes() {
        return (getAdapteeManager().getPositionSearchRecordTypes());
    }


    /**
     *  Tests if the given position search record type is supported. 
     *
     *  @param  positionSearchRecordType a <code> Type </code> indicating a 
     *          position record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> positionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPositionSearchRecordType(org.osid.type.Type positionSearchRecordType) {
        return (getAdapteeManager().supportsPositionSearchRecordType(positionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Appointment </code> record types. 
     *
     *  @return a list containing the supported appointment record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAppointmentRecordTypes() {
        return (getAdapteeManager().getAppointmentRecordTypes());
    }


    /**
     *  Tests if the given <code> Appointment </code> record type is 
     *  supported. 
     *
     *  @param  appointmentRecordType a <code> Type </code> indicating an 
     *          <code> Appointment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> appointmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAppointmentRecordType(org.osid.type.Type appointmentRecordType) {
        return (getAdapteeManager().supportsAppointmentRecordType(appointmentRecordType));
    }


    /**
     *  Gets the supported appointment search record types. 
     *
     *  @return a list containing the supported appointment search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAppointmentSearchRecordTypes() {
        return (getAdapteeManager().getAppointmentSearchRecordTypes());
    }


    /**
     *  Tests if the given appointment search record type is supported. 
     *
     *  @param  appointmentSearchRecordType a <code> Type </code> indicating 
     *          an appointment record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          appointmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAppointmentSearchRecordType(org.osid.type.Type appointmentSearchRecordType) {
        return (getAdapteeManager().supportsAppointmentSearchRecordType(appointmentSearchRecordType));
    }


    /**
     *  Gets the supported <code> Realm </code> record types. 
     *
     *  @return a list containing the supported realm record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRealmRecordTypes() {
        return (getAdapteeManager().getRealmRecordTypes());
    }


    /**
     *  Tests if the given <code> Realm </code> record type is supported. 
     *
     *  @param  realmRecordType a <code> Type </code> indicating a <code> 
     *          Realm </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> realmRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRealmRecordType(org.osid.type.Type realmRecordType) {
        return (getAdapteeManager().supportsRealmRecordType(realmRecordType));
    }


    /**
     *  Gets the supported realm search record types. 
     *
     *  @return a list containing the supported realm search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRealmSearchRecordTypes() {
        return (getAdapteeManager().getRealmSearchRecordTypes());
    }


    /**
     *  Tests if the given realm search record type is supported. 
     *
     *  @param  realmSearchRecordType a <code> Type </code> indicating a realm 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> realmSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRealmSearchRecordType(org.osid.type.Type realmSearchRecordType) {
        return (getAdapteeManager().supportsRealmSearchRecordType(realmSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person lookup 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonLookupSession getPersonLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonLookupSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuerySession getPersonQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonQuerySessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person search 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSearchSession getPersonSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonSearchSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonAdminSession getPersonAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service. 
     *
     *  @param  personReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSession(org.osid.personnel.PersonReceiver personReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonNotificationSession(personReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the person 
     *  notification service for the given realm. 
     *
     *  @param  personReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PersonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> personReceiver, realmId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonNotificationSession getPersonNotificationSessionForRealm(org.osid.personnel.PersonReceiver personReceiver, 
                                                                                             org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonNotificationSessionForRealm(personReceiver, realmId, proxy));
    }


    /**
     *  Gets the session for retrieving person to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPersonRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmSession getPersonRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonRealmSession(proxy));
    }


    /**
     *  Gets the session for assigning person to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PersonRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonRealmAssignmentSession getPersonRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonRealmAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic person realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return a <code> PersonSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonSmartRealmSession getPersonSmartRealmSession(org.osid.id.Id realmId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonSmartRealmSession(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationLookupSession getOrganizationLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationLookupSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuerySession getOrganizationQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationQuerySessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSearchSession getOrganizationSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationSearchSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationAdminSession getOrganizationAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service. 
     *
     *  @param  organizationReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSession(org.osid.personnel.OrganizationReceiver organizationReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationNotificationSession(organizationReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  notification service for the given realm. 
     *
     *  @param  organizationReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> organizationReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationNotificationSession getOrganizationNotificationSessionForRealm(org.osid.personnel.OrganizationReceiver organizationReceiver, 
                                                                                                         org.osid.id.Id realmId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationNotificationSessionForRealm(organizationReceiver, realmId, proxy));
    }


    /**
     *  Gets the session traversing organization hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy traversal service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchySession getOrganizationHierarchySessionForRealm(org.osid.id.Id realmId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationHierarchySessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the session designing organization hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the organization 
     *  heirarchy design service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationHierarchyDesignSession getOrganizationHierarchyDesignSessionForRealm(org.osid.id.Id realmId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationHierarchyDesignSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the session for retrieving organization to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmSession getOrganizationRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationRealmSession(proxy));
    }


    /**
     *  Gets the session for assigning organization to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationRealmAssignmentSession getOrganizationRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationRealmAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic organization realms for the 
     *  given realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return an <code> OrganizationSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationSmartRealmSession getOrganizationSmartRealmSession(org.osid.id.Id realmId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrganizationSmartRealmSession(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionLookupSession getPositionLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionLookupSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position query 
     *  service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuerySession getPositionQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionQuerySessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSearchSession getPositionSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionSearchSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionAdminSession getPositionAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service. 
     *
     *  @param  positionReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSession(org.osid.personnel.PositionReceiver positionReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionNotificationSession(positionReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the position 
     *  notification service for the given realm. 
     *
     *  @param  positionReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> positionReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionNotificationSession getPositionNotificationSessionForRealm(org.osid.personnel.PositionReceiver positionReceiver, 
                                                                                                 org.osid.id.Id realmId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionNotificationSessionForRealm(positionReceiver, realmId, proxy));
    }


    /**
     *  Gets the session for retrieving position to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPositionRealm() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmSession getPositionRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionRealmSession(proxy));
    }


    /**
     *  Gets the session for assigning position to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PositionRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionRealmAssignmentSession getPositionRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionRealmAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic position realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return a <code> PositionSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPositionSmartRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionSmartRealmSession getPositionSmartRealmSession(org.osid.id.Id realmId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPositionSmartRealmSession(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  lookup service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentLookupSession getAppointmentLookupSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentLookupSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  query service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuerySession getAppointmentQuerySessionForRealm(org.osid.id.Id realmId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentQuerySessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  search service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSearchSession getAppointmentSearchSessionForRealm(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentSearchSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  administration service for the given realm. 
     *
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentAdminSession getAppointmentAdminSessionForRealm(org.osid.id.Id realmId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentAdminSessionForRealm(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSession(org.osid.personnel.AppointmentReceiver appointmentReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentNotificationSession(appointmentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the appointment 
     *  notification service for the given realm. 
     *
     *  @param  appointmentReceiver the receiver 
     *  @param  realmId the <code> Id </code> of the <code> Realm </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Realm </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> appointmentReceiver, 
     *          realmId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentNotificationSession getAppointmentNotificationSessionForRealm(org.osid.personnel.AppointmentReceiver appointmentReceiver, 
                                                                                                       org.osid.id.Id realmId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentNotificationSessionForRealm(appointmentReceiver, realmId, proxy));
    }


    /**
     *  Gets the session for retrieving appointment to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentRealmSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealm() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmSession getAppointmentRealmSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentRealmSession(proxy));
    }


    /**
     *  Gets the session for assigning appointment to realm mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentRealmAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentRealmAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentRealmAssignmentSession getAppointmentRealmAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentRealmAssignmentSession(proxy));
    }


    /**
     *  Gets the session for managing dynamic appointment realms for the given 
     *  realm. 
     *
     *  @param  realmId the <code> Id </code> of a realm 
     *  @param  proxy a proxy 
     *  @return an <code> AppointmentSmartRealmSession </code> 
     *  @throws org.osid.NotFoundException <code> realmId </code> not found 
     *  @throws org.osid.NullArgumentException <code> realmId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentSmartRealm() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentSmartRealmSession getAppointmentSmartRealmSession(org.osid.id.Id realmId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAppointmentSmartRealmSession(realmId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmLookupSession getRealmLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuerySession getRealmQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmSearchSession getRealmSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRealmAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmAdminSession getRealmAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  notification service. 
     *
     *  @param  realmReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RealmNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> realmReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmNotificationSession getRealmNotificationSession(org.osid.personnel.RealmReceiver realmReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmNotificationSession(realmReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchySession getRealmHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the realm 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RealmHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRealmHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmHierarchyDesignSession getRealmHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRealmHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> PersonnelBatchProxyManager. </code> 
     *
     *  @return a <code> PersonnelBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.batch.PersonnelBatchProxyManager getPersonnelBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonnelBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
