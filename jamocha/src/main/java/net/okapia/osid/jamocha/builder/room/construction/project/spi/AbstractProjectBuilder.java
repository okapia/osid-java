//
// AbstractProject.java
//
//     Defines a Project builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.construction.project.spi;


/**
 *  Defines a <code>Project</code> builder.
 */

public abstract class AbstractProjectBuilder<T extends AbstractProjectBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.construction.project.ProjectMiter project;


    /**
     *  Constructs a new <code>AbstractProjectBuilder</code>.
     *
     *  @param project the project to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProjectBuilder(net.okapia.osid.jamocha.builder.room.construction.project.ProjectMiter project) {
        super(project);
        this.project = project;
        return;
    }


    /**
     *  Builds the project.
     *
     *  @return the new project
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.construction.Project build() {
        (new net.okapia.osid.jamocha.builder.validator.room.construction.project.ProjectValidator(getValidations())).validate(this.project);
        return (new net.okapia.osid.jamocha.builder.room.construction.project.ImmutableProject(this.project));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the project miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.construction.project.ProjectMiter getMiter() {
        return (this.project);
    }


    /**
     *  Sets the building.
     *
     *  @param building a building
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public T building(org.osid.room.Building building) {
        getMiter().setBuilding(building);
        return (self());
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T cost(org.osid.financials.Currency cost) {
        getMiter().setCost(cost);
        return (self());
    }


    /**
     *  Adds a Project record.
     *
     *  @param record a project record
     *  @param recordType the type of project record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.construction.records.ProjectRecord record, org.osid.type.Type recordType) {
        getMiter().addProjectRecord(record, recordType);
        return (self());
    }
}       


