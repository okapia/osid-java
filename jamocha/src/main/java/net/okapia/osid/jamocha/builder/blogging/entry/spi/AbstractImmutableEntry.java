//
// AbstractImmutableEntry.java
//
//     Wraps a mutable Entry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.blogging.entry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Entry</code> to hide modifiers. This
 *  wrapper provides an immutized Entry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying entry whose state changes are visible.
 */

public abstract class AbstractImmutableEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableSourceableOsidObject
    implements org.osid.blogging.Entry {

    private final org.osid.blogging.Entry entry;


    /**
     *  Constructs a new <code>AbstractImmutableEntry</code>.
     *
     *  @param entry the entry to immutablize
     *  @throws org.osid.NullArgumentException <code>entry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEntry(org.osid.blogging.Entry entry) {
        super(entry);
        this.entry = entry;
        return;
    }


    /**
     *  Gets the time of this entry. 
     *
     *  @return the time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.entry.getTimestamp());
    }


    /**
     *  Gets the poster <code> Id </code> of this entry. 
     *
     *  @return the poster resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPosterId() {
        return (this.entry.getPosterId());
    }


    /**
     *  Gets the poster of this entry. 
     *
     *  @return the poster resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getPoster()
        throws org.osid.OperationFailedException {

        return (this.entry.getPoster());
    }


    /**
     *  Gets the posting <code> Id </code> of this entry. 
     *
     *  @return the posting agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostingAgentId() {
        return (this.entry.getPostingAgentId());
    }


    /**
     *  Gets the posting of this entry. 
     *
     *  @return the posting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getPostingAgent()
        throws org.osid.OperationFailedException {

        return (this.entry.getPostingAgent());
    }


    /**
     *  Gets the subject line of this entry. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.entry.getSubjectLine());
    }


    /**
     *  Gets the summary or excerpt of this entry. 
     *
     *  @return the summary 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSummary() {
        return (this.entry.getSummary());
    }


    /**
     *  Gets the text of the entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.entry.getText());
    }


    /**
     *  Gets the copyright statement for this entry. 
     *
     *  @return the copyright statement 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getCopyright() {
        return (this.entry.getCopyright());
    }


    /**
     *  Gets the record corresponding to the given <code> Entry </code> record 
     *  <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> entryRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(entryRecordType) </code> is <code> true </code> . 
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(entryRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        return (this.entry.getEntryRecord(entryRecordType));
    }
}

