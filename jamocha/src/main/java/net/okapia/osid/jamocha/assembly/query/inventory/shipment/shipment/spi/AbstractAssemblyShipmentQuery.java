//
// AbstractAssemblyShipmentQuery.java
//
//     A ShipmentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ShipmentQuery that stores terms.
 */

public abstract class AbstractAssemblyShipmentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.shipment.ShipmentQuery,
               org.osid.inventory.shipment.ShipmentQueryInspector,
               org.osid.inventory.shipment.ShipmentSearchOrder {

    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyShipmentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyShipmentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        getAssembler().clearTerms(getSourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (getAssembler().getIdTerms(getSourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the source. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceColumn(), style);
        return;
    }


    /**
     *  Gets the SourceId column name.
     *
     * @return the column name
     */

    protected String getSourceIdColumn() {
        return ("source_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a source. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceQuery() is false");
    }


    /**
     *  Matches shipments with any source. 
     *
     *  @param  match <code> true </code> to match shipments with any source, 
     *          <code> false </code> to match shipments with no source 
     */

    @OSID @Override
    public void matchAnySource(boolean match) {
        getAssembler().addIdWildcardTerm(getSourceColumn(), match);
        return;
    }


    /**
     *  Clears the source terms. 
     */

    @OSID @Override
    public void clearSourceTerms() {
        getAssembler().clearTerms(getSourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the source. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceSearchOrder() is false");
    }


    /**
     *  Gets the Source column name.
     *
     * @return the column name
     */

    protected String getSourceColumn() {
        return ("source");
    }


    /**
     *  Sets the order <code> Id </code> for this query. 
     *
     *  @param  orderId an order <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        getAssembler().addIdTerm(getOrderIdColumn(), orderId, match);
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        getAssembler().clearTerms(getOrderIdColumn());
        return;
    }


    /**
     *  Gets the order <code> Id </code> query terms. 
     *
     *  @return the order <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (getAssembler().getIdTerms(getOrderIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the order. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOrder(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOrderColumn(), style);
        return;
    }


    /**
     *  Gets the OrderId column name.
     *
     * @return the column name
     */

    protected String getOrderIdColumn() {
        return ("order_id");
    }


    /**
     *  Tests if an <code> OrderQuery </code> is available. 
     *
     *  @return <code> true </code> if an order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an order. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return an odrer query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Matches shipments with any related order. 
     *
     *  @param  match <code> true </code> to match shipments with any related 
     *          order, <code> false </code> to match shipments with no order 
     */

    @OSID @Override
    public void matchAnyOrder(boolean match) {
        getAssembler().addIdWildcardTerm(getOrderColumn(), match);
        return;
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        getAssembler().clearTerms(getOrderColumn());
        return;
    }


    /**
     *  Gets the order query terms. 
     *
     *  @return the order query terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Tests if an order search order is available. 
     *
     *  @return <code> true </code> if an order search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the order. 
     *
     *  @return the order search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderSearchOrder getOrderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOrderSearchOrder() is false");
    }


    /**
     *  Gets the Order column name.
     *
     * @return the column name
     */

    protected String getOrderColumn() {
        return ("order");
    }


    /**
     *  Matches shipment dates within the given date range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches shipments with any date. 
     *
     *  @param  match <code> true </code> to match shipments with any date, 
     *          <code> false </code> to match shipments with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateColumn(), match);
        return;
    }


    /**
     *  Clears the shipment date. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the shipment 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateColumn(), style);
        return;
    }


    /**
     *  Gets the Date column name.
     *
     * @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }


    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), entryId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return an entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches shipments that have any entry. 
     *
     *  @param  match <code> true </code> to match shipments with any entry, 
     *          <code> false </code> to match shipments with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.inventory.shipment.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match shipments 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this shipment supports the given record
     *  <code>Type</code>.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return <code>true</code> if the shipmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type shipmentRecordType) {
        for (org.osid.inventory.shipment.records.ShipmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  shipmentRecordType the shipment record type 
     *  @return the shipment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentQueryRecord getShipmentQueryRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  shipmentRecordType the shipment record type 
     *  @return the shipment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord getShipmentQueryInspectorRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param shipmentRecordType the shipment record type
     *  @return the shipment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentSearchOrderRecord getShipmentSearchOrderRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this shipment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param shipmentQueryRecord the shipment query record
     *  @param shipmentQueryInspectorRecord the shipment query inspector
     *         record
     *  @param shipmentSearchOrderRecord the shipment search order record
     *  @param shipmentRecordType shipment record type
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentQueryRecord</code>,
     *          <code>shipmentQueryInspectorRecord</code>,
     *          <code>shipmentSearchOrderRecord</code> or
     *          <code>shipmentRecordTypeshipment</code> is
     *          <code>null</code>
     */
            
    protected void addShipmentRecords(org.osid.inventory.shipment.records.ShipmentQueryRecord shipmentQueryRecord, 
                                      org.osid.inventory.shipment.records.ShipmentQueryInspectorRecord shipmentQueryInspectorRecord, 
                                      org.osid.inventory.shipment.records.ShipmentSearchOrderRecord shipmentSearchOrderRecord, 
                                      org.osid.type.Type shipmentRecordType) {

        addRecordType(shipmentRecordType);

        nullarg(shipmentQueryRecord, "shipment query record");
        nullarg(shipmentQueryInspectorRecord, "shipment query inspector record");
        nullarg(shipmentSearchOrderRecord, "shipment search odrer record");

        this.queryRecords.add(shipmentQueryRecord);
        this.queryInspectorRecords.add(shipmentQueryInspectorRecord);
        this.searchOrderRecords.add(shipmentSearchOrderRecord);
        
        return;
    }
}
