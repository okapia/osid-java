//
// AbstractInventoryBatchManager.java
//
//     An adapter for a InventoryBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InventoryBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInventoryBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.inventory.batch.InventoryBatchManager>
    implements org.osid.inventory.batch.InventoryBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterInventoryBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInventoryBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInventoryBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInventoryBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of items is available. 
     *
     *  @return <code> true </code> if an item bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBatchAdmin() {
        return (getAdapteeManager().supportsItemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of stocks is available. 
     *
     *  @return <code> true </code> if a stock bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockBatchAdmin() {
        return (getAdapteeManager().supportsStockBatchAdmin());
    }


    /**
     *  Tests if bulk administration of models is available. 
     *
     *  @return <code> true </code> if a model bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelBatchAdmin() {
        return (getAdapteeManager().supportsModelBatchAdmin());
    }


    /**
     *  Tests if bulk administration of inventories is available. 
     *
     *  @return <code> true </code> if an inventory bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryBatchAdmin() {
        return (getAdapteeManager().supportsInventoryBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ItemBatchAdminSession getItemBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSessionForInventory(inventoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service. 
     *
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStockBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk stock 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return a <code> StockBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.StockBatchAdminSession getStockBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStockBatchAdminSessionForInventory(inventoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service. 
     *
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getModelBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk model 
     *  administration service for the given inventory. 
     *
     *  @param  inventoryId the <code> Id </code> of the <code> Inventory 
     *          </code> 
     *  @return an <code> ModelBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inventory </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inventoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.ModelBatchAdminSession getModelBatchAdminSessionForInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getModelBatchAdminSessionForInventory(inventoryId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk inventory 
     *  administration service. 
     *
     *  @return an <code> InventoryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.batch.InventoryBatchAdminSession getInventoryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
