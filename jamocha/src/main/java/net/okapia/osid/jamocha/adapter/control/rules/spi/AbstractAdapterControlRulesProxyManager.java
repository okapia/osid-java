//
// AbstractControlRulesProxyManager.java
//
//     An adapter for a ControlRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ControlRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterControlRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.control.rules.ControlRulesProxyManager>
    implements org.osid.control.rules.ControlRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterControlRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterControlRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterControlRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterControlRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerLookup() {
        return (getAdapteeManager().supportsDeviceEnablerLookup());
    }


    /**
     *  Tests if querying device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerQuery() {
        return (getAdapteeManager().supportsDeviceEnablerQuery());
    }


    /**
     *  Tests if searching device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearch() {
        return (getAdapteeManager().supportsDeviceEnablerSearch());
    }


    /**
     *  Tests if a device enabler administrative service is supported. 
     *
     *  @return <code> true </code> if device enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerAdmin() {
        return (getAdapteeManager().supportsDeviceEnablerAdmin());
    }


    /**
     *  Tests if a device enabler notification service is supported. 
     *
     *  @return <code> true </code> if device enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerNotification() {
        return (getAdapteeManager().supportsDeviceEnablerNotification());
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystem() {
        return (getAdapteeManager().supportsDeviceEnablerSystem());
    }


    /**
     *  Tests if a device enabler system service is supported. 
     *
     *  @return <code> true </code> if device enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystemAssignment() {
        return (getAdapteeManager().supportsDeviceEnablerSystemAssignment());
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSmartSystem() {
        return (getAdapteeManager().supportsDeviceEnablerSmartSystem());
    }


    /**
     *  Tests if looking up input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerLookup() {
        return (getAdapteeManager().supportsInputEnablerLookup());
    }


    /**
     *  Tests if querying input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerQuery() {
        return (getAdapteeManager().supportsInputEnablerQuery());
    }


    /**
     *  Tests if searching input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearch() {
        return (getAdapteeManager().supportsInputEnablerSearch());
    }


    /**
     *  Tests if an input enabler administrative service is supported. 
     *
     *  @return <code> true </code> if input enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerAdmin() {
        return (getAdapteeManager().supportsInputEnablerAdmin());
    }


    /**
     *  Tests if an input enabler notification service is supported. 
     *
     *  @return <code> true </code> if input enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerNotification() {
        return (getAdapteeManager().supportsInputEnablerNotification());
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystem() {
        return (getAdapteeManager().supportsInputEnablerSystem());
    }


    /**
     *  Tests if an input enabler system service is supported. 
     *
     *  @return <code> true </code> if input enabler system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystemAssignment() {
        return (getAdapteeManager().supportsInputEnablerSystemAssignment());
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSmartSystem() {
        return (getAdapteeManager().supportsInputEnablerSmartSystem());
    }


    /**
     *  Tests if an input enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleLookup() {
        return (getAdapteeManager().supportsInputEnablerRuleLookup());
    }


    /**
     *  Tests if an input enabler rule application service is supported. 
     *
     *  @return <code> true </code> if input enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleApplication() {
        return (getAdapteeManager().supportsInputEnablerRuleApplication());
    }


    /**
     *  Tests if looking up trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerLookup() {
        return (getAdapteeManager().supportsTriggerEnablerLookup());
    }


    /**
     *  Tests if querying trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerQuery() {
        return (getAdapteeManager().supportsTriggerEnablerQuery());
    }


    /**
     *  Tests if searching trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearch() {
        return (getAdapteeManager().supportsTriggerEnablerSearch());
    }


    /**
     *  Tests if a trigger enabler administrative service is supported. 
     *
     *  @return <code> true </code> if trigger enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerAdmin() {
        return (getAdapteeManager().supportsTriggerEnablerAdmin());
    }


    /**
     *  Tests if a trigger enabler notification service is supported. 
     *
     *  @return <code> true </code> if trigger enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerNotification() {
        return (getAdapteeManager().supportsTriggerEnablerNotification());
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystem() {
        return (getAdapteeManager().supportsTriggerEnablerSystem());
    }


    /**
     *  Tests if a trigger enabler system service is supported. 
     *
     *  @return <code> true </code> if trigger enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystemAssignment() {
        return (getAdapteeManager().supportsTriggerEnablerSystemAssignment());
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSmartSystem() {
        return (getAdapteeManager().supportsTriggerEnablerSmartSystem());
    }


    /**
     *  Tests if a trigger enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleLookup() {
        return (getAdapteeManager().supportsTriggerEnablerRuleLookup());
    }


    /**
     *  Tests if a trigger enabler rule application service is supported. 
     *
     *  @return <code> true </code> if trigger enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleApplication() {
        return (getAdapteeManager().supportsTriggerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerLookup() {
        return (getAdapteeManager().supportsActionEnablerLookup());
    }


    /**
     *  Tests if querying action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerQuery() {
        return (getAdapteeManager().supportsActionEnablerQuery());
    }


    /**
     *  Tests if searching action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearch() {
        return (getAdapteeManager().supportsActionEnablerSearch());
    }


    /**
     *  Tests if an action enabler administrative service is supported. 
     *
     *  @return <code> true </code> if action enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerAdmin() {
        return (getAdapteeManager().supportsActionEnablerAdmin());
    }


    /**
     *  Tests if an action enabler notification service is supported. 
     *
     *  @return <code> true </code> if action enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerNotification() {
        return (getAdapteeManager().supportsActionEnablerNotification());
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystem() {
        return (getAdapteeManager().supportsActionEnablerSystem());
    }


    /**
     *  Tests if an action enabler system service is supported. 
     *
     *  @return <code> true </code> if action enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystemAssignment() {
        return (getAdapteeManager().supportsActionEnablerSystemAssignment());
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSmartSystem() {
        return (getAdapteeManager().supportsActionEnablerSmartSystem());
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerRecordTypes() {
        return (getAdapteeManager().getDeviceEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> record type is 
     *  supported. 
     *
     *  @param  deviceEnablerRecordType a <code> Type </code> indicating a 
     *          <code> DeviceEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerRecordType(org.osid.type.Type deviceEnablerRecordType) {
        return (getAdapteeManager().supportsDeviceEnablerRecordType(deviceEnablerRecordType));
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerSearchRecordTypes() {
        return (getAdapteeManager().getDeviceEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  deviceEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> DeviceEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearchRecordType(org.osid.type.Type deviceEnablerSearchRecordType) {
        return (getAdapteeManager().supportsDeviceEnablerSearchRecordType(deviceEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> InputEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerRecordTypes() {
        return (getAdapteeManager().getInputEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> InputEnabler </code> record type is 
     *  supported. 
     *
     *  @param  inputEnablerRecordType a <code> Type </code> indicating an 
     *          <code> InputEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerRecordType(org.osid.type.Type inputEnablerRecordType) {
        return (getAdapteeManager().supportsInputEnablerRecordType(inputEnablerRecordType));
    }


    /**
     *  Gets the supported <code> InputEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerSearchRecordTypes() {
        return (getAdapteeManager().getInputEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> InputEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  inputEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> InputEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inputEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearchRecordType(org.osid.type.Type inputEnablerSearchRecordType) {
        return (getAdapteeManager().supportsInputEnablerSearchRecordType(inputEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerRecordTypes() {
        return (getAdapteeManager().getTriggerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> record type is 
     *  supported. 
     *
     *  @param  triggerEnablerRecordType a <code> Type </code> indicating a 
     *          <code> TriggerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRecordType(org.osid.type.Type triggerEnablerRecordType) {
        return (getAdapteeManager().supportsTriggerEnablerRecordType(triggerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getTriggerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  triggerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> TriggerEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          triggerEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearchRecordType(org.osid.type.Type triggerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsTriggerEnablerSearchRecordType(triggerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerRecordTypes() {
        return (getAdapteeManager().getActionEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  actionEnablerRecordType a <code> Type </code> indicating an 
     *          <code> ActionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerRecordType(org.osid.type.Type actionEnablerRecordType) {
        return (getAdapteeManager().supportsActionEnablerRecordType(actionEnablerRecordType));
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerSearchRecordTypes() {
        return (getAdapteeManager().getActionEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  actionEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActionEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          actionEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearchRecordType(org.osid.type.Type actionEnablerSearchRecordType) {
        return (getAdapteeManager().supportsActionEnablerSearchRecordType(actionEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSession(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerNotificationSession(deviceEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver, 
     *          systemId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSessionForSystem(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerNotificationSessionForSystem(deviceEnablerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device enabler/system 
     *  mappings for device enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemSession getDeviceEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning device 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemAssignmentSession getDeviceEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSmartSystemSession getDeviceEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceEnablerRuleApplicationSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enablers 
     *  earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service. 
     *
     *  @param  deviceProcessoEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceProcessoEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSession(org.osid.control.rules.InputEnablerReceiver deviceProcessoEnablerReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerNotificationSession(deviceProcessoEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceProcessoEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceProcessoEnablerReceiver, systemId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSessionForSystem(org.osid.control.rules.InputEnablerReceiver deviceProcessoEnablerReceiver, 
                                                                                                              org.osid.id.Id systemId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerNotificationSessionForSystem(deviceProcessoEnablerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input enabler/system 
     *  mappings for input enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemSession getInputEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning input 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemAssignmentSession getInputEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSmartSystemSession getInputEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputEnablerRuleApplicationSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSession(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerNotificationSession(triggerEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service for the given system. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver, 
     *          systemId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSessionForSystem(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                                  org.osid.id.Id systemId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerNotificationSessionForSystem(triggerEnablerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger enabler/system 
     *  mappings for trigger enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemSession getTriggerEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning trigger 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemAssignmentSession getTriggerEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSmartSystemSession getTriggerEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerEnablerRuleApplicationSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException an <code> 
     *          ActionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSession(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerNotificationSession(actionEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service for the given system. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSessionForSystem(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerNotificationSessionForSystem(actionEnablerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action enabler/system 
     *  mappings for action enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemSession getActionEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemAssignmentSession getActionEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSmartSystemSession getActionEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionEnablerRuleApplicationSessionForSystem(systemId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
        super.close();

        return;
    }
}
