//
// AbstractDepotQuery.java
//
//     A template for making a Depot Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.depot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for depots.
 */

public abstract class AbstractDepotQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.installation.DepotQuery {

    private final java.util.Collection<org.osid.installation.records.DepotQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the package <code> Id </code> for this query. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPackageId(org.osid.id.Id packageId, boolean match) {
        return;
    }


    /**
     *  Clears the package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPackageIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a package. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getPackageQuery() {
        throw new org.osid.UnimplementedException("supportsPackageQuery() is false");
    }


    /**
     *  Matches depots that have any package. 
     *
     *  @param  match <code> true </code> to match depots with any packages, 
     *          <code> false </code> to match depots with no packages 
     */

    @OSID @Override
    public void matchAnyPackage(boolean match) {
        return;
    }


    /**
     *  Clears the package query terms. 
     */

    @OSID @Override
    public void clearPackageTerms() {
        return;
    }


    /**
     *  Sets the depot <code> Id </code> for this query to match depots that 
     *  have the specified depot as an ancestor. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorDepotId(org.osid.id.Id depotId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorDepotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a Depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorDepotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getAncestorDepotQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorDepotQuery() is false");
    }


    /**
     *  Matches depots with any ancestor. 
     *
     *  @param  match <code> true </code> to match depots with any ancestor, 
     *          <code> false </code> to match root depots 
     */

    @OSID @Override
    public void matchAnyAncestorDepot(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor depot query terms. 
     */

    @OSID @Override
    public void clearAncestorDepotTerms() {
        return;
    }


    /**
     *  Sets the depot <code> Id </code> for this query to match depots that 
     *  have the specified depot as a descendant. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantDepotId(org.osid.id.Id depotId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantDepotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a Depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantDepotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getDescendantDepotQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantDepotQuery() is false");
    }


    /**
     *  Matches depots with any descendant. 
     *
     *  @param  match <code> true </code> to match depots with any descendant, 
     *          <code> false </code> to match leaf depots 
     */

    @OSID @Override
    public void matchAnyDescendantDepot(boolean match) {
        return;
    }


    /**
     *  Clears the descendant depot query terms. 
     */

    @OSID @Override
    public void clearDescendantDepotTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given depot query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a depot implementing the requested record.
     *
     *  @param depotRecordType a depot record type
     *  @return the depot query record
     *  @throws org.osid.NullArgumentException
     *          <code>depotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(depotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.DepotQueryRecord getDepotQueryRecord(org.osid.type.Type depotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.DepotQueryRecord record : this.records) {
            if (record.implementsRecordType(depotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(depotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this depot query. 
     *
     *  @param depotQueryRecord depot query record
     *  @param depotRecordType depot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDepotQueryRecord(org.osid.installation.records.DepotQueryRecord depotQueryRecord, 
                                          org.osid.type.Type depotRecordType) {

        addRecordType(depotRecordType);
        nullarg(depotQueryRecord, "depot query record");
        this.records.add(depotQueryRecord);        
        return;
    }
}
