//
// AbstractActivitySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractActivitySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.ActivitySearchResults {

    private org.osid.course.ActivityList activities;
    private final org.osid.course.ActivityQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.ActivitySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractActivitySearchResults.
     *
     *  @param activities the result set
     *  @param activityQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          or <code>activityQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractActivitySearchResults(org.osid.course.ActivityList activities,
                                            org.osid.course.ActivityQueryInspector activityQueryInspector) {
        nullarg(activities, "activities");
        nullarg(activityQueryInspector, "activity query inspectpr");

        this.activities = activities;
        this.inspector = activityQueryInspector;

        return;
    }


    /**
     *  Gets the activity list resulting from a search.
     *
     *  @return an activity list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities() {
        if (this.activities == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.ActivityList activities = this.activities;
        this.activities = null;
	return (activities);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.ActivityQueryInspector getActivityQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  activity search record <code> Type. </code> This method must
     *  be used to retrieve an activity implementing the requested
     *  record.
     *
     *  @param activitySearchRecordType an activity search 
     *         record type 
     *  @return the activity search
     *  @throws org.osid.NullArgumentException
     *          <code>activitySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(activitySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivitySearchResultsRecord getActivitySearchResultsRecord(org.osid.type.Type activitySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.ActivitySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(activitySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(activitySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record activity search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addActivityRecord(org.osid.course.records.ActivitySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "activity record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
