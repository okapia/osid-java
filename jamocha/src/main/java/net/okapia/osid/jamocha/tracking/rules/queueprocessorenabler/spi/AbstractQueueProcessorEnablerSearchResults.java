//
// AbstractQueueProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractQueueProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.tracking.rules.QueueProcessorEnablerSearchResults {

    private org.osid.tracking.rules.QueueProcessorEnablerList queueProcessorEnablers;
    private final org.osid.tracking.rules.QueueProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.tracking.rules.records.QueueProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractQueueProcessorEnablerSearchResults.
     *
     *  @param queueProcessorEnablers the result set
     *  @param queueProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablers</code>
     *          or <code>queueProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractQueueProcessorEnablerSearchResults(org.osid.tracking.rules.QueueProcessorEnablerList queueProcessorEnablers,
                                            org.osid.tracking.rules.QueueProcessorEnablerQueryInspector queueProcessorEnablerQueryInspector) {
        nullarg(queueProcessorEnablers, "queue processor enablers");
        nullarg(queueProcessorEnablerQueryInspector, "queue processor enabler query inspectpr");

        this.queueProcessorEnablers = queueProcessorEnablers;
        this.inspector = queueProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the queue processor enabler list resulting from a search.
     *
     *  @return a queue processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablers() {
        if (this.queueProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.tracking.rules.QueueProcessorEnablerList queueProcessorEnablers = this.queueProcessorEnablers;
        this.queueProcessorEnablers = null;
	return (queueProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.tracking.rules.QueueProcessorEnablerQueryInspector getQueueProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  queue processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a queueProcessorEnabler implementing the requested
     *  record.
     *
     *  @param queueProcessorEnablerSearchRecordType a queueProcessorEnabler search 
     *         record type 
     *  @return the queue processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(queueProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueProcessorEnablerSearchResultsRecord getQueueProcessorEnablerSearchResultsRecord(org.osid.type.Type queueProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.tracking.rules.records.QueueProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(queueProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record queue processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addQueueProcessorEnablerRecord(org.osid.tracking.rules.records.QueueProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "queue processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
