//
// AbstractContactBatchProxyManager.java
//
//     An adapter for a ContactBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.contact.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ContactBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterContactBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.contact.batch.ContactBatchProxyManager>
    implements org.osid.contact.batch.ContactBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterContactBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterContactBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterContactBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterContactBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of contacts is available. 
     *
     *  @return <code> true </code> if a contact bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactBatchAdmin() {
        return (getAdapteeManager().supportsContactBatchAdmin());
    }


    /**
     *  Tests if bulk administration of addresses is available. 
     *
     *  @return <code> true </code> if an address bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBatchAdmin() {
        return (getAdapteeManager().supportsAddressBatchAdmin());
    }


    /**
     *  Tests if bulk administration of address books is available. 
     *
     *  @return <code> true </code> if a address book bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookBatchAdmin() {
        return (getAdapteeManager().supportsAddressBookBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk contact 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ContactBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.ContactBatchAdminSession getContactBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk contact 
     *  administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ContactBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.ContactBatchAdminSession getContactBatchAdminSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getContactBatchAdminSessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk address 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AddressBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.AddressBatchAdminSession getAddressBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk address 
     *  administration service for the given address book. 
     *
     *  @param  addressBookId the <code> Id </code> of the <code> AddressBook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AddressBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AddressBook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.AddressBatchAdminSession getAddressBatchAdminSessionForAddressBook(org.osid.id.Id addressBookId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBatchAdminSessionForAddressBook(addressBookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk address 
     *  book administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AddressBookBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.contact.batch.AddressBookBatchAdminSession getAddressBookBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAddressBookBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
