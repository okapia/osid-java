//
// AbstractSubscription.java
//
//     Defines a Subscription.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Subscription</code>.
 */

public abstract class AbstractSubscription
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.subscription.Subscription {

    private org.osid.subscription.Dispatch dispatch;
    private org.osid.resource.Resource subscriber;
    private org.osid.contact.Address address;

    private final java.util.Collection<org.osid.subscription.records.SubscriptionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the dispatch. 
     *
     *  @return the dispatch <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDispatchId() {
        return (this.dispatch.getId());
    }


    /**
     *  Gets the dispatch. 
     *
     *  @return the dispatch <code> Id </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch()
        throws org.osid.OperationFailedException {

        return (this.dispatch);
    }


    /**
     *  Sets the dispatch.
     *
     *  @param dispatch a dispatch
     *  @throws org.osid.NullArgumentException
     *          <code>dispatch</code> is <code>null</code>
     */

    protected void setDispatch(org.osid.subscription.Dispatch dispatch) {
        nullarg(dispatch, "dispatch");
        this.dispatch = dispatch;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the subscriber. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubscriberId() {
        return (this.subscriber.getId());
    }


    /**
     *  Gets the subscriber. 
     *
     *  @return the subscriber 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubscriber()
        throws org.osid.OperationFailedException {

        return (this.subscriber);
    }


    /**
     *  Sets the subscriber.
     *
     *  @param subscriber a subscriber
     *  @throws org.osid.NullArgumentException
     *          <code>subscriber</code> is <code>null</code>
     */

    protected void setSubscriber(org.osid.resource.Resource subscriber) {
        nullarg(subscriber, "subscriber");
        this.subscriber = subscriber;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the subscriber's address. 
     *
     *  @return the address <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.address.getId());
    }


    /**
     *  Gets the subscriber's address. 
     *
     *  @return the subscriber's address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.address);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    protected void setAddress(org.osid.contact.Address address) {
        nullarg(address, "address");
        this.address = address;
        return;
    }


    /**
     *  Tests if this subscription supports the given record
     *  <code>Type</code>.
     *
     *  @param  subscriptionRecordType a subscription record type 
     *  @return <code>true</code> if the subscriptionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type subscriptionRecordType) {
        for (org.osid.subscription.records.SubscriptionRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Subscription</code> record <code>Type</code>.
     *
     *  @param  subscriptionRecordType the subscription record type 
     *  @return the subscription record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.SubscriptionRecord getSubscriptionRecord(org.osid.type.Type subscriptionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.SubscriptionRecord record : this.records) {
            if (record.implementsRecordType(subscriptionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subscription. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param subscriptionRecord the subscription record
     *  @param subscriptionRecordType subscription record type
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionRecord</code> or
     *          <code>subscriptionRecordTypesubscription</code> is
     *          <code>null</code>
     */
            
    protected void addSubscriptionRecord(org.osid.subscription.records.SubscriptionRecord subscriptionRecord, 
                                         org.osid.type.Type subscriptionRecordType) {

        nullarg(subscriptionRecord, "subscriptiuon record");
        addRecordType(subscriptionRecordType);
        this.records.add(subscriptionRecord);
        
        return;
    }
}
