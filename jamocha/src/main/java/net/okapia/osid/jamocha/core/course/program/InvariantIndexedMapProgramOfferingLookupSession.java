//
// InvariantIndexedMapProgramOfferingLookupSession
//
//    Implements a ProgramOffering lookup service backed by a fixed
//    collection of programOfferings indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a ProgramOffering lookup service backed by a fixed
 *  collection of program offerings. The program offerings are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some program offerings may be compatible
 *  with more types than are indicated through these program offering
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProgramOfferingLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractIndexedMapProgramOfferingLookupSession
    implements org.osid.course.program.ProgramOfferingLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProgramOfferingLookupSession} using an
     *  array of programOfferings.
     *
     *  @param courseCatalog the course catalog
     *  @param programOfferings an array of program offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programOfferings} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    org.osid.course.program.ProgramOffering[] programOfferings) {

        setCourseCatalog(courseCatalog);
        putProgramOfferings(programOfferings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProgramOfferingLookupSession} using a
     *  collection of program offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param programOfferings a collection of program offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code programOfferings} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProgramOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    java.util.Collection<? extends org.osid.course.program.ProgramOffering> programOfferings) {

        setCourseCatalog(courseCatalog);
        putProgramOfferings(programOfferings);
        return;
    }
}
