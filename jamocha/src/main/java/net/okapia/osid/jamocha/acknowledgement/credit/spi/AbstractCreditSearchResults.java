//
// AbstractCreditSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCreditSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.acknowledgement.CreditSearchResults {

    private org.osid.acknowledgement.CreditList credits;
    private final org.osid.acknowledgement.CreditQueryInspector inspector;
    private final java.util.Collection<org.osid.acknowledgement.records.CreditSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCreditSearchResults.
     *
     *  @param credits the result set
     *  @param creditQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>credits</code>
     *          or <code>creditQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCreditSearchResults(org.osid.acknowledgement.CreditList credits,
                                            org.osid.acknowledgement.CreditQueryInspector creditQueryInspector) {
        nullarg(credits, "credits");
        nullarg(creditQueryInspector, "credit query inspectpr");

        this.credits = credits;
        this.inspector = creditQueryInspector;

        return;
    }


    /**
     *  Gets the credit list resulting from a search.
     *
     *  @return a credit list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCredits() {
        if (this.credits == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.acknowledgement.CreditList credits = this.credits;
        this.credits = null;
	return (credits);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.acknowledgement.CreditQueryInspector getCreditQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  credit search record <code> Type. </code> This method must
     *  be used to retrieve a credit implementing the requested
     *  record.
     *
     *  @param creditSearchRecordType a credit search 
     *         record type 
     *  @return the credit search
     *  @throws org.osid.NullArgumentException
     *          <code>creditSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(creditSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditSearchResultsRecord getCreditSearchResultsRecord(org.osid.type.Type creditSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.acknowledgement.records.CreditSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(creditSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(creditSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record credit search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCreditRecord(org.osid.acknowledgement.records.CreditSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "credit record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
