//
// AbstractAssemblyResourceRelationshipQuery.java
//
//     A ResourceRelationshipQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ResourceRelationshipQuery that stores terms.
 */

public abstract class AbstractAssemblyResourceRelationshipQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.resource.ResourceRelationshipQuery,
               org.osid.resource.ResourceRelationshipQueryInspector,
               org.osid.resource.ResourceRelationshipSearchOrder {

    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyResourceRelationshipQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyResourceRelationshipQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSourceResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceResourceIdTerms() {
        getAssembler().clearTerms(getSourceResourceIdColumn());
        return;
    }


    /**
     *  Gets the source resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceResourceIdTerms() {
        return (getAssembler().getIdTerms(getSourceResourceIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySourceResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceResourceColumn(), style);
        return;
    }


    /**
     *  Gets the SourceResourceId column name.
     *
     * @return the column name
     */

    protected String getSourceResourceIdColumn() {
        return ("source_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceResourceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceResourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearSourceResourceTerms() {
        getAssembler().clearTerms(getSourceResourceColumn());
        return;
    }


    /**
     *  Gets the source resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSourceResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSourceResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceResourceSearchOrder() is false");
    }


    /**
     *  Gets the SourceResource column name.
     *
     * @return the column name
     */

    protected String getSourceResourceColumn() {
        return ("source_resource");
    }


    /**
     *  Sets the peer resource <code> Id </code> for this query. 
     *
     *  @param  peerResourceId a peer resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peerResourceId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDestinationResourceId(org.osid.id.Id peerResourceId, 
                                           boolean match) {
        getAssembler().addIdTerm(getDestinationResourceIdColumn(), peerResourceId, match);
        return;
    }


    /**
     *  Clears the peer resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationResourceIdTerms() {
        getAssembler().clearTerms(getDestinationResourceIdColumn());
        return;
    }


    /**
     *  Gets the Destination resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationResourceIdTerms() {
        return (getAssembler().getIdTerms(getDestinationResourceIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDestinationResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDestinationResourceColumn(), style);
        return;
    }


    /**
     *  Gets the DestinationResourceId column name.
     *
     * @return the column name
     */

    protected String getDestinationResourceIdColumn() {
        return ("destination_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDestinationResourceQuery() {
        throw new org.osid.UnimplementedException("supportsDestinationResourceQuery() is false");
    }


    /**
     *  Clears the peer resource terms. 
     */

    @OSID @Override
    public void clearDestinationResourceTerms() {
        getAssembler().clearTerms(getDestinationResourceColumn());
        return;
    }


    /**
     *  Gets the Destination resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDestinationResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a peer resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationResourceSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getDestinationResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDestinationResourceSearchOrder() is false");
    }


    /**
     *  Gets the DestinationResource column name.
     *
     * @return the column name
     */

    protected String getDestinationResourceColumn() {
        return ("destination_resource");
    }


    /**
     *  Matches relationships where the peer resources are the same. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSameResource(boolean match) {
        getAssembler().addBooleanTerm(getSameResourceColumn(), match);
        return;
    }


    /**
     *  Clears the same resource terms. 
     */

    @OSID @Override
    public void clearSameResourceTerms() {
        getAssembler().clearTerms(getSameResourceColumn());
        return;
    }


    /**
     *  Gets the same resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSameResourceTerms() {
        return (getAssembler().getBooleanTerms(getSameResourceColumn()));
    }


    /**
     *  Gets the SameResource column name.
     *
     * @return the column name
     */

    protected String getSameResourceColumn() {
        return ("same_resource");
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match terms assigned 
     *  to bins. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        getAssembler().addIdTerm(getBinIdColumn(), binId, match);
        return;
    }


    /**
     *  Clears the bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        getAssembler().clearTerms(getBinIdColumn());
        return;
    }


    /**
     *  Gets the bin <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBinIdTerms() {
        return (getAssembler().getIdTerms(getBinIdColumn()));
    }


    /**
     *  Gets the BinId column name.
     *
     * @return the column name
     */

    protected String getBinIdColumn() {
        return ("bin_id");
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        getAssembler().clearTerms(getBinColumn());
        return;
    }


    /**
     *  Gets the bin query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.BinQueryInspector[] getBinTerms() {
        return (new org.osid.resource.BinQueryInspector[0]);
    }


    /**
     *  Gets the Bin column name.
     *
     * @return the column name
     */

    protected String getBinColumn() {
        return ("bin");
    }


    /**
     *  Tests if this resourceRelationship supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceRelationshipRecordType a resource relationship record type 
     *  @return <code>true</code> if the resourceRelationshipRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        for (org.osid.resource.records.ResourceRelationshipQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  resourceRelationshipRecordType the resource relationship record type 
     *  @return the resource relationship query record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipQueryRecord getResourceRelationshipQueryRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  resourceRelationshipRecordType the resource relationship record type 
     *  @return the resource relationship query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipQueryInspectorRecord getResourceRelationshipQueryInspectorRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param resourceRelationshipRecordType the resource relationship record type
     *  @return the resource relationship search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipSearchOrderRecord getResourceRelationshipSearchOrderRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this resource relationship. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceRelationshipQueryRecord the resource relationship query record
     *  @param resourceRelationshipQueryInspectorRecord the resource relationship query inspector
     *         record
     *  @param resourceRelationshipSearchOrderRecord the resource relationship search order record
     *  @param resourceRelationshipRecordType resource relationship record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipQueryRecord</code>,
     *          <code>resourceRelationshipQueryInspectorRecord</code>,
     *          <code>resourceRelationshipSearchOrderRecord</code> or
     *          <code>resourceRelationshipRecordTyperesourceRelationship</code> is
     *          <code>null</code>
     */
            
    protected void addResourceRelationshipRecords(org.osid.resource.records.ResourceRelationshipQueryRecord resourceRelationshipQueryRecord, 
                                      org.osid.resource.records.ResourceRelationshipQueryInspectorRecord resourceRelationshipQueryInspectorRecord, 
                                      org.osid.resource.records.ResourceRelationshipSearchOrderRecord resourceRelationshipSearchOrderRecord, 
                                      org.osid.type.Type resourceRelationshipRecordType) {

        addRecordType(resourceRelationshipRecordType);

        nullarg(resourceRelationshipQueryRecord, "resource relationship query record");
        nullarg(resourceRelationshipQueryInspectorRecord, "resource relationship query inspector record");
        nullarg(resourceRelationshipSearchOrderRecord, "resource relationship search odrer record");

        this.queryRecords.add(resourceRelationshipQueryRecord);
        this.queryInspectorRecords.add(resourceRelationshipQueryInspectorRecord);
        this.searchOrderRecords.add(resourceRelationshipSearchOrderRecord);
        
        return;
    }
}
