//
// AbstractObstacleSearch.java
//
//     A template for making an Obstacle Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.obstacle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing obstacle searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractObstacleSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.mapping.path.ObstacleSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.mapping.path.records.ObstacleSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.mapping.path.ObstacleSearchOrder obstacleSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of obstacles. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  obstacleIds list of obstacles
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongObstacles(org.osid.id.IdList obstacleIds) {
        while (obstacleIds.hasNext()) {
            try {
                this.ids.add(obstacleIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongObstacles</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of obstacle Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getObstacleIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  obstacleSearchOrder obstacle search order 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>obstacleSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderObstacleResults(org.osid.mapping.path.ObstacleSearchOrder obstacleSearchOrder) {
	this.obstacleSearchOrder = obstacleSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.mapping.path.ObstacleSearchOrder getObstacleSearchOrder() {
	return (this.obstacleSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given obstacle search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an obstacle implementing the requested record.
     *
     *  @param obstacleSearchRecordType an obstacle search record
     *         type
     *  @return the obstacle search record
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.ObstacleSearchRecord getObstacleSearchRecord(org.osid.type.Type obstacleSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.mapping.path.records.ObstacleSearchRecord record : this.records) {
            if (record.implementsRecordType(obstacleSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle search. 
     *
     *  @param obstacleSearchRecord obstacle search record
     *  @param obstacleSearchRecordType obstacle search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObstacleSearchRecord(org.osid.mapping.path.records.ObstacleSearchRecord obstacleSearchRecord, 
                                           org.osid.type.Type obstacleSearchRecordType) {

        addRecordType(obstacleSearchRecordType);
        this.records.add(obstacleSearchRecord);        
        return;
    }
}
