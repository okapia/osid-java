//
// AbstractBudgetSearchOdrer.java
//
//     Defines a BudgetSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code BudgetSearchOrder}.
 */

public abstract class AbstractBudgetSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.financials.budgeting.BudgetSearchOrder {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by activity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivity(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an activity search order is available. 
     *
     *  @return <code> true </code> if an activity search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearchOrder() {
        return (false);
    }


    /**
     *  Gets the activity search order. 
     *
     *  @return the activity search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivitySearchOrder getActivitySearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivitySearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalPeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a fiscal period search order is available. 
     *
     *  @return <code> true </code> if a fiscal period search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the fiscal period search order. 
     *
     *  @return the fiscal period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchOrder getFiscalPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return {@code true} if the budgetRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code budgetRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type budgetRecordType) {
        for (org.osid.financials.budgeting.records.BudgetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  budgetRecordType the budget record type 
     *  @return the budget search order record
     *  @throws org.osid.NullArgumentException
     *          {@code budgetRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(budgetRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetSearchOrderRecord getBudgetSearchOrderRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this budget. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param budgetRecord the budget search odrer record
     *  @param budgetRecordType budget record type
     *  @throws org.osid.NullArgumentException
     *          {@code budgetRecord} or
     *          {@code budgetRecordTypebudget} is
     *          {@code null}
     */
            
    protected void addBudgetRecord(org.osid.financials.budgeting.records.BudgetSearchOrderRecord budgetSearchOrderRecord, 
                                     org.osid.type.Type budgetRecordType) {

        addRecordType(budgetRecordType);
        this.records.add(budgetSearchOrderRecord);
        
        return;
    }
}
