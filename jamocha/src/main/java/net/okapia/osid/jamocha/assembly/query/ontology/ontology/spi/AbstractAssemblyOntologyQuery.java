//
// AbstractAssemblyOntologyQuery.java
//
//     An OntologyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ontology.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OntologyQuery that stores terms.
 */

public abstract class AbstractAssemblyOntologyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.ontology.OntologyQuery,
               org.osid.ontology.OntologyQueryInspector,
               org.osid.ontology.OntologySearchOrder {

    private final java.util.Collection<org.osid.ontology.records.OntologyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.OntologyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.OntologySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOntologyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOntologyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the subject <code> Id </code> for this query. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        getAssembler().addIdTerm(getSubjectIdColumn(), subjectId, match);
        return;
    }


    /**
     *  Clears the subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        getAssembler().clearTerms(getSubjectIdColumn());
        return;
    }


    /**
     *  Gets the subject <code> Id </code> terms. 
     *
     *  @return the subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (getAssembler().getIdTerms(getSubjectIdColumn()));
    }


    /**
     *  Gets the SubjectId column name.
     *
     * @return the column name
     */

    protected String getSubjectIdColumn() {
        return ("subject_id");
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Matches ontologies that have any subject. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          subject, <code> false </code> to match ontologies with no 
     *          subject 
     */

    @OSID @Override
    public void matchAnySubject(boolean match) {
        getAssembler().addIdWildcardTerm(getSubjectColumn(), match);
        return;
    }


    /**
     *  Clears the subject terms for this query. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        getAssembler().clearTerms(getSubjectColumn());
        return;
    }


    /**
     *  Gets the subject terms. 
     *
     *  @return the subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the Subject column name.
     *
     * @return the column name
     */

    protected String getSubjectColumn() {
        return ("subject");
    }


    /**
     *  Sets the relevancy <code> Id </code> for this query. 
     *
     *  @param  relevancyId a relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRelevancyId(org.osid.id.Id relevancyId, boolean match) {
        getAssembler().addIdTerm(getRelevancyIdColumn(), relevancyId, match);
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyIdTerms() {
        getAssembler().clearTerms(getRelevancyIdColumn());
        return;
    }


    /**
     *  Gets the relevancy <code> Id </code> terms. 
     *
     *  @return the relevancy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelevancyIdTerms() {
        return (getAssembler().getIdTerms(getRelevancyIdColumn()));
    }


    /**
     *  Gets the RelevancyId column name.
     *
     * @return the column name
     */

    protected String getRelevancyIdColumn() {
        return ("relevancy_id");
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Matches ontologies that have any relevancy. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          relevancy, <code> false </code> to match ontologies with no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRelevancy(boolean match) {
        getAssembler().addIdWildcardTerm(getRelevancyColumn(), match);
        return;
    }


    /**
     *  Clears the relevancy terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyTerms() {
        getAssembler().clearTerms(getRelevancyColumn());
        return;
    }


    /**
     *  Gets the relevancy terms. 
     *
     *  @return the relevancy terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the Relevancy column name.
     *
     * @return the column name
     */

    protected String getRelevancyColumn() {
        return ("relevancy");
    }


    /**
     *  Sets the ontology <code> Id </code> for this query to match ontologies 
     *  that have the specified ontology as an ancestor. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorOntologyId(org.osid.id.Id ontologyId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAncestorOntologyIdColumn(), ontologyId, match);
        return;
    }


    /**
     *  Clears the ancestor ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearAncestorOntologyIdTerms() {
        getAssembler().clearTerms(getAncestorOntologyIdColumn());
        return;
    }


    /**
     *  Gets the ancestor ontology <code> Id </code> terms. 
     *
     *  @return the ancestor ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOntologyIdTerms() {
        return (getAssembler().getIdTerms(getAncestorOntologyIdColumn()));
    }


    /**
     *  Gets the AncestorOntologyId column name.
     *
     * @return the column name
     */

    protected String getAncestorOntologyIdColumn() {
        return ("ancestor_ontology_id");
    }


    /**
     *  Tests if an <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if an ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorOntologyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getAncestorOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorOntologyQuery() is false");
    }


    /**
     *  Matches ontologies with any ancestor. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          ancestor, <code> false </code> to match root ontologies 
     */

    @OSID @Override
    public void matchAnyAncestorOntology(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorOntologyColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor ontology terms for this query. 
     */

    @OSID @Override
    public void clearAncestorOntologyTerms() {
        getAssembler().clearTerms(getAncestorOntologyColumn());
        return;
    }


    /**
     *  Gets the ancestor ontology terms. 
     *
     *  @return the ancestor ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getAncestorOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the AncestorOntology column name.
     *
     * @return the column name
     */

    protected String getAncestorOntologyColumn() {
        return ("ancestor_ontology");
    }


    /**
     *  Sets the ontology <code> Id </code> for this query to match ontologies 
     *  that have the specified ontology as a descendant. 
     *
     *  @param  ontologyId an ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantOntologyId(org.osid.id.Id ontologyId, 
                                          boolean match) {
        getAssembler().addIdTerm(getDescendantOntologyIdColumn(), ontologyId, match);
        return;
    }


    /**
     *  Clears the descendant ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearDescendantOntologyIdTerms() {
        getAssembler().clearTerms(getDescendantOntologyIdColumn());
        return;
    }


    /**
     *  Gets the descendant ontology <code> Id </code> terms. 
     *
     *  @return the descendant ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOntologyIdTerms() {
        return (getAssembler().getIdTerms(getDescendantOntologyIdColumn()));
    }


    /**
     *  Gets the DescendantOntologyId column name.
     *
     * @return the column name
     */

    protected String getDescendantOntologyIdColumn() {
        return ("descendant_ontology_id");
    }


    /**
     *  Tests if an <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if an ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantOntologyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getDescendantOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantOntologyQuery() is false");
    }


    /**
     *  Matches ontologies with any descendant. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          descendant, <code> false </code> to match leaf ontologies 
     */

    @OSID @Override
    public void matchAnyDescendantOntology(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantOntologyColumn(), match);
        return;
    }


    /**
     *  Clears the descendant ontology terms for this query. 
     */

    @OSID @Override
    public void clearDescendantOntologyTerms() {
        getAssembler().clearTerms(getDescendantOntologyColumn());
        return;
    }


    /**
     *  Gets the descendant ontology terms. 
     *
     *  @return the descendant ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getDescendantOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the DescendantOntology column name.
     *
     * @return the column name
     */

    protected String getDescendantOntologyColumn() {
        return ("descendant_ontology");
    }


    /**
     *  Tests if this ontology supports the given record
     *  <code>Type</code>.
     *
     *  @param  ontologyRecordType an ontology record type 
     *  @return <code>true</code> if the ontologyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ontologyRecordType) {
        for (org.osid.ontology.records.OntologyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  ontologyRecordType the ontology record type 
     *  @return the ontology query record 
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologyQueryRecord getOntologyQueryRecord(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.OntologyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  ontologyRecordType the ontology record type 
     *  @return the ontology query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologyQueryInspectorRecord getOntologyQueryInspectorRecord(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.OntologyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param ontologyRecordType the ontology record type
     *  @return the ontology search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologySearchOrderRecord getOntologySearchOrderRecord(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.OntologySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this ontology. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ontologyQueryRecord the ontology query record
     *  @param ontologyQueryInspectorRecord the ontology query inspector
     *         record
     *  @param ontologySearchOrderRecord the ontology search order record
     *  @param ontologyRecordType ontology record type
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyQueryRecord</code>,
     *          <code>ontologyQueryInspectorRecord</code>,
     *          <code>ontologySearchOrderRecord</code> or
     *          <code>ontologyRecordTypeontology</code> is
     *          <code>null</code>
     */
            
    protected void addOntologyRecords(org.osid.ontology.records.OntologyQueryRecord ontologyQueryRecord, 
                                      org.osid.ontology.records.OntologyQueryInspectorRecord ontologyQueryInspectorRecord, 
                                      org.osid.ontology.records.OntologySearchOrderRecord ontologySearchOrderRecord, 
                                      org.osid.type.Type ontologyRecordType) {

        addRecordType(ontologyRecordType);

        nullarg(ontologyQueryRecord, "ontology query record");
        nullarg(ontologyQueryInspectorRecord, "ontology query inspector record");
        nullarg(ontologySearchOrderRecord, "ontology search odrer record");

        this.queryRecords.add(ontologyQueryRecord);
        this.queryInspectorRecords.add(ontologyQueryInspectorRecord);
        this.searchOrderRecords.add(ontologySearchOrderRecord);
        
        return;
    }
}
