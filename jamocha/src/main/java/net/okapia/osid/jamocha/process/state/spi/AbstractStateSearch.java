//
// AbstractStateSearch.java
//
//     A template for making a State Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.state.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing state searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractStateSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.process.StateSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.process.records.StateSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.process.StateSearchOrder stateSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of states. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  stateIds list of states
     *  @throws org.osid.NullArgumentException
     *          <code>stateIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongStates(org.osid.id.IdList stateIds) {
        while (stateIds.hasNext()) {
            try {
                this.ids.add(stateIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongStates</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of state Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getStateIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  stateSearchOrder state search order 
     *  @throws org.osid.NullArgumentException
     *          <code>stateSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>stateSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderStateResults(org.osid.process.StateSearchOrder stateSearchOrder) {
	this.stateSearchOrder = stateSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.process.StateSearchOrder getStateSearchOrder() {
	return (this.stateSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given state search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a state implementing the requested record.
     *
     *  @param stateSearchRecordType a state search record
     *         type
     *  @return the state search record
     *  @throws org.osid.NullArgumentException
     *          <code>stateSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stateSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.StateSearchRecord getStateSearchRecord(org.osid.type.Type stateSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.process.records.StateSearchRecord record : this.records) {
            if (record.implementsRecordType(stateSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stateSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this state search. 
     *
     *  @param stateSearchRecord state search record
     *  @param stateSearchRecordType state search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStateSearchRecord(org.osid.process.records.StateSearchRecord stateSearchRecord, 
                                           org.osid.type.Type stateSearchRecordType) {

        addRecordType(stateSearchRecordType);
        this.records.add(stateSearchRecord);        
        return;
    }
}
