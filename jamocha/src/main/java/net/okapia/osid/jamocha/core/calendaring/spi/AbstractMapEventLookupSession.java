//
// AbstractMapEventLookupSession
//
//    A simple framework for providing an Event lookup service
//    backed by a fixed collection of events.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Event lookup service backed by a
 *  fixed collection of events. The events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Events</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEventLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractEventLookupSession
    implements org.osid.calendaring.EventLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.Event> events = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.Event>());


    /**
     *  Makes an <code>Event</code> available in this session.
     *
     *  @param  event an event
     *  @throws org.osid.NullArgumentException <code>event<code>
     *          is <code>null</code>
     */

    protected void putEvent(org.osid.calendaring.Event event) {
        this.events.put(event.getId(), event);
        return;
    }


    /**
     *  Makes an array of events available in this session.
     *
     *  @param  events an array of events
     *  @throws org.osid.NullArgumentException <code>events<code>
     *          is <code>null</code>
     */

    protected void putEvents(org.osid.calendaring.Event[] events) {
        putEvents(java.util.Arrays.asList(events));
        return;
    }


    /**
     *  Makes a collection of events available in this session.
     *
     *  @param  events a collection of events
     *  @throws org.osid.NullArgumentException <code>events<code>
     *          is <code>null</code>
     */

    protected void putEvents(java.util.Collection<? extends org.osid.calendaring.Event> events) {
        for (org.osid.calendaring.Event event : events) {
            this.events.put(event.getId(), event);
        }

        return;
    }


    /**
     *  Removes an Event from this session.
     *
     *  @param  eventId the <code>Id</code> of the event
     *  @throws org.osid.NullArgumentException <code>eventId<code> is
     *          <code>null</code>
     */

    protected void removeEvent(org.osid.id.Id eventId) {
        this.events.remove(eventId);
        return;
    }


    /**
     *  Gets the <code>Event</code> specified by its <code>Id</code>.
     *
     *  @param  eventId <code>Id</code> of the <code>Event</code>
     *  @return the event
     *  @throws org.osid.NotFoundException <code>eventId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>eventId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent(org.osid.id.Id eventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.Event event = this.events.get(eventId);
        if (event == null) {
            throw new org.osid.NotFoundException("event not found: " + eventId);
        }

        return (event);
    }


    /**
     *  Gets all <code>Events</code>. In plenary mode, the returned
     *  list contains all known events or an error
     *  results. Otherwise, the returned list may contain only those
     *  events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Events</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.event.ArrayEventList(this.events.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.events.clear();
        super.close();
        return;
    }
}
