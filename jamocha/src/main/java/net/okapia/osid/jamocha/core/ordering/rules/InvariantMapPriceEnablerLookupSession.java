//
// InvariantMapPriceEnablerLookupSession
//
//    Implements a PriceEnabler lookup service backed by a fixed collection of
//    priceEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.rules;


/**
 *  Implements a PriceEnabler lookup service backed by a fixed
 *  collection of price enablers. The price enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.core.ordering.rules.spi.AbstractMapPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPriceEnablerLookupSession</code> with no
     *  price enablers.
     *  
     *  @param store the store
     *  @throws org.osid.NullArgumnetException {@code store} is
     *          {@code null}
     */

    public InvariantMapPriceEnablerLookupSession(org.osid.ordering.Store store) {
        setStore(store);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPriceEnablerLookupSession</code> with a single
     *  price enabler.
     *  
     *  @param store the store
     *  @param priceEnabler a single price enabler
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnabler} is <code>null</code>
     */

      public InvariantMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                               org.osid.ordering.rules.PriceEnabler priceEnabler) {
        this(store);
        putPriceEnabler(priceEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPriceEnablerLookupSession</code> using an array
     *  of price enablers.
     *  
     *  @param store the store
     *  @param priceEnablers an array of price enablers
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnablers} is <code>null</code>
     */

      public InvariantMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                               org.osid.ordering.rules.PriceEnabler[] priceEnablers) {
        this(store);
        putPriceEnablers(priceEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPriceEnablerLookupSession</code> using a
     *  collection of price enablers.
     *
     *  @param store the store
     *  @param priceEnablers a collection of price enablers
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code priceEnablers} is <code>null</code>
     */

      public InvariantMapPriceEnablerLookupSession(org.osid.ordering.Store store,
                                               java.util.Collection<? extends org.osid.ordering.rules.PriceEnabler> priceEnablers) {
        this(store);
        putPriceEnablers(priceEnablers);
        return;
    }
}
