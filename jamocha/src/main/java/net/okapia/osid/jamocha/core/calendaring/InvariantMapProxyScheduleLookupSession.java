//
// InvariantMapProxyScheduleLookupSession
//
//    Implements a Schedule lookup service backed by a fixed
//    collection of schedules. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a Schedule lookup service backed by a fixed
 *  collection of schedules. The schedules are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyScheduleLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapScheduleLookupSession
    implements org.osid.calendaring.ScheduleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyScheduleLookupSession} with no
     *  schedules.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyScheduleLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyScheduleLookupSession} with a single
     *  schedule.
     *
     *  @param calendar the calendar
     *  @param schedule a single schedule
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code schedule} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyScheduleLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Schedule schedule, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSchedule(schedule);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyScheduleLookupSession} using
     *  an array of schedules.
     *
     *  @param calendar the calendar
     *  @param schedules an array of schedules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code schedules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyScheduleLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Schedule[] schedules, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSchedules(schedules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyScheduleLookupSession} using a
     *  collection of schedules.
     *
     *  @param calendar the calendar
     *  @param schedules a collection of schedules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code schedules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyScheduleLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.Schedule> schedules,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSchedules(schedules);
        return;
    }
}
