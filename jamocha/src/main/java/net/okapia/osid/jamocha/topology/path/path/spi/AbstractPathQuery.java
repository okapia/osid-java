//
// AbstractPathQuery.java
//
//     A template for making a Path Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for paths.
 */

public abstract class AbstractPathQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.topology.path.PathQuery {

    private final java.util.Collection<org.osid.topology.path.records.PathQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches paths that are complete. 
     *
     *  @param  match <code> true </code> to match complete paths, <code> 
     *          false </code> to match inactive paths 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        return;
    }


    /**
     *  Clears the path complete query terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        return;
    }


    /**
     *  Matches paths that are closed. 
     *
     *  @param  match <code> true </code> to match closed paths, <code> false 
     *          </code> to match inactive paths 
     */

    @OSID @Override
    public void matchClosed(boolean match) {
        return;
    }


    /**
     *  Clears the path closed query terms. 
     */

    @OSID @Override
    public void clearClosedTerms() {
        return;
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths with a 
     *  starting node. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchStartingNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the starting node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStartingNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available for a starting node. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getStartingNodeQuery() {
        throw new org.osid.UnimplementedException("supportsStartingNodeQuery() is false");
    }


    /**
     *  Clears the starting node query terms. 
     */

    @OSID @Override
    public void clearStartingNodeTerms() {
        return;
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths with an 
     *  ending node. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEndingNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the ending node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEndingNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available for an ending node. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getEndingNodeQuery() {
        throw new org.osid.UnimplementedException("supportsEndingNodeQuery() is false");
    }


    /**
     *  Clears the ending node query terms. 
     */

    @OSID @Override
    public void clearEndingNodeTerms() {
        return;
    }


    /**
     *  Sets the node <code> Ids </code> for this query to match paths along 
     *  the given node. 
     *
     *  @param  nodeIds the node <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeIds </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAlongNodeIds(org.osid.id.Id[] nodeIds, boolean match) {
        return;
    }


    /**
     *  Clears the along node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAlongNodeIdsTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match paths 
     *  intersecting with another path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchIntersectingPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the intersecting path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available for intersecting 
     *  paths, 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectingPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for an intersecting path, Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectingPathQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuery getIntersectingPathQuery() {
        throw new org.osid.UnimplementedException("supportsIntersectingPathQuery() is false");
    }


    /**
     *  Matches paths with any intersecting path, 
     *
     *  @param  match <code> true </code> to match paths with any intersecting 
     *          path, <code> false </code> to match paths with no intersecting 
     *          path 
     */

    @OSID @Override
    public void matchAnyIntersectingPath(boolean match) {
        return;
    }


    /**
     *  Clears the intersecting path query terms. 
     */

    @OSID @Override
    public void clearIntersectingPathTerms() {
        return;
    }


    /**
     *  Matches paths that have a number of hops within the given range 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchHops(long from, long to, boolean match) {
        return;
    }


    /**
     *  Clears the number of hops query terms. 
     */

    @OSID @Override
    public void clearHopsTerms() {
        return;
    }


    /**
     *  Matches paths that have a distance with the given range inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(java.math.BigDecimal from, 
                              java.math.BigDecimal to, boolean match) {
        return;
    }


    /**
     *  Matches paths that has any distance assigned. 
     *
     *  @param  match <code> true </code> to match paths with any distance, 
     *          <code> false </code> to match paths with no distance assigned 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        return;
    }


    /**
     *  Matches paths that have a cost with the given range inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchCost(java.math.BigDecimal from, java.math.BigDecimal to, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the cost query terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        return;
    }


    /**
     *  Sets the node <code> Id </code> for this query to match paths that 
     *  pass through nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the node <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getNodeQuery() {
        throw new org.osid.UnimplementedException("supportsNodeQuery() is false");
    }


    /**
     *  Clears the node query terms. 
     */

    @OSID @Override
    public void clearNodeTerms() {
        return;
    }


    /**
     *  Sets the edge <code> Id </code> for this query to match paths contain 
     *  the edge. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEdgeId(org.osid.id.Id edgeId, boolean match) {
        return;
    }


    /**
     *  Clears the edge <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEdgeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsEdgeQuery() is false");
    }


    /**
     *  Clears the edge query terms. 
     */

    @OSID @Override
    public void clearEdgeTerms() {
        return;
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match edges 
     *  assigned to graphs. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given path query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a path implementing the requested record.
     *
     *  @param pathRecordType a path record type
     *  @return the path query record
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathQueryRecord getPathQueryRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathQueryRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record to this path query. 
     *
     *  @param pathQueryRecord path query record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPathQueryRecord(org.osid.topology.path.records.PathQueryRecord pathQueryRecord, 
                                          org.osid.type.Type pathRecordType) {

        addRecordType(pathRecordType);
        nullarg(pathQueryRecord, "path query record");
        this.records.add(pathQueryRecord);        
        return;
    }
}
