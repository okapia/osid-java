//
// AbstractGraphNodeToGraphList.java
//
//     Implements an AbstractGraphNodeToGraphList adapter.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 September 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.topology.graphnode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an AbstractGraphList adapter to convert graph
 *  nodes into graphs.
 */

public abstract class AbstractGraphNodeToGraphList
    extends net.okapia.osid.jamocha.adapter.topology.graph.spi.AbstractAdapterGraphList
    implements org.osid.topology.GraphList {

    private final org.osid.topology.GraphNodeList list;


    /**
     *  Constructs a new {@code AbstractGraphNodeToGraphList}.
     *
     *  @param list the graph node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is
     *          {@code null}
     */

    protected AbstractGraphNodeToGraphList(org.osid.topology.GraphNodeList list) {
        super(list);
        this.list = list;
        return;
    }


    /**
     *  Gets the next {@code Graph} in this list. 
     *
     *  @return the next {@code Graph} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Graph} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Graph getNextGraph()
        throws org.osid.OperationFailedException {

        return (this.list.getNextGraphNode().getGraph());
    }
}
