//
// AbstractImmutableActivity.java
//
//     Wraps a mutable Activity to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Activity</code> to hide modifiers. This
 *  wrapper provides an immutized Activity from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activity whose state changes are visible.
 */

public abstract class AbstractImmutableActivity
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.Activity {

    private final org.osid.course.Activity activity;


    /**
     *  Constructs a new <code>AbstractImmutableActivity</code>.
     *
     *  @param activity the activity to immutablize
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivity(org.osid.course.Activity activity) {
        super(activity);
        this.activity = activity;
        return;
    }


    /**
     *  Gets the activity unit <code> Id </code> associated with this 
     *  activity. 
     *
     *  @return the activity unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityUnitId() {
        return (this.activity.getActivityUnitId());
    }


    /**
     *  Gets the activity unit associated with this activity. 
     *
     *  @return the activity unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit()
        throws org.osid.OperationFailedException {

        return (this.activity.getActivityUnit());
    }


    /**
     *  Gets the <code> Id </code> of the <code>CourseOffering</code>
     *  of this activity.
     *
     *  @return the <code>CourseOffering</code> <code>Id</code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.activity.getCourseOfferingId());
    }


    /**
     *  Gets the <code>CourseOffering</code> of this activity.
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.activity.getCourseOffering());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.activity.getTermId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.activity.getTerm());
    }


    /**
     *  Tests if this is a an implicit or explicit activity. 
     *
     *  @return <code> true </code> if this is an implicit activity,
     *          <code> false </code> if an explicit activity
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.activity.isImplicit());
    }


    /**
     *  Gets the schedule <code> Ids </code> associated with this activity. 
     *
     *  @return the schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        return (this.activity.getScheduleIds());
    }


    /**
     *  Gets the schedules associated with this activity. 
     *
     *  @return the schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (this.activity.getSchedules());
    }


    /**
     *  Gets the superseding activity <code> Ids </code> whose sessions 
     *  override sessions of this activity. 
     *
     *  @return the superseding activity <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSupersedingActivityIds() {
        return (this.activity.getSupersedingActivityIds());
    }


    /**
     *  Gets the superseding activities whose sessions that override sessions 
     *  of this activity. 
     *
     *  @return the superseding activities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getSupersedingActivities()
        throws org.osid.OperationFailedException {

        return (this.activity.getSupersedingActivities());
    }


    /**
     *  Gets the specific meeting times added to this activity. 
     *
     *  @return a list of specific meeting times 
     */

    @OSID @Override
    public org.osid.calendaring.MeetingTimeList getSpecificMeetingTimes() {
        return (this.activity.getSpecificMeetingTimes());
    }


    /**
     *  Gets the blackout dates for this activity. Activitiy sessions 
     *  overlapping with the time intervals do not appear in the series. 
     *
     *  @return the exceptions to the scheduled activity 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeIntervalList getBlackouts() {
        return (this.activity.getBlackouts());
    }


    /**
     *  Gets the instructor <code> Ids. </code> If each activity has its own 
     *  instructor, the headlining instructors may be returned. 
     *
     *  @return the instructor <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInstructorIds() {
        return (this.activity.getInstructorIds());
    }


    /**
     *  Gets the instructors. If each activity has its own instructor, the 
     *  headlining instructors may be returned. 
     *
     *  @return the sponsors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getInstructors()
        throws org.osid.OperationFailedException {

        return (this.activity.getInstructors());
    }


    /**
     *  Tests if this course offering requires advanced registration. 
     *
     *  @return <code> true </code> if this course requires advance 
     *          registration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.activity.requiresRegistration());
    }

    
    /**
     *  Gets the minimum number of students this activity can have. 
     *
     *  @return the minimum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMinimumSeats() {
        return (this.activity.getMinimumSeats());
    }


    /**
     *  Tests if this activity offering has limited seating. 
     *
     *  @return <code> true </code> if this has limietd seating
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isSeatingLimited() {
        return (this.activity.isSeatingLimited());
    }


    /**
     *  Gets the maximum number of students this activity can accommodate. 
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumSeats() {
        return (this.activity.getMaximumSeats());
    }


    /**
     *  Gets the total time required for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetEffort() {
        return (this.activity.getTotalTargetEffort());
    }


    /**
     *  Tests if this is a contact activity. 
     *
     *  @return <code> true </code> if this is a contact activity, <code> 
     *          false </code> if an independent activity 
     */

    @OSID @Override
    public boolean isContact() {
        return (this.activity.isContact());
    }


    /**
     *  Gets the total contact time for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetContactTime() {
        return (this.activity.getTotalTargetContactTime());
    }


    /**
     *  Gets the total individual time required for this activity. 
     *
     *  @return the total individual effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetIndividualEffort() {
        return (this.activity.getTotalTargetIndividualEffort());
    }


    /**
     *  Tests if this activity is recurring. 
     *
     *  @return <code> true </code> if this activity is recurring, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isRecurringWeekly() {
        return (this.activity.isRecurringWeekly());
    }


    /**
     *  Gets the time required for this recurring effort on a weekly basis. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyEffort() {
        return (this.activity.getWeeklyEffort());
    }


    /**
     *  Gets the weekly contact time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyContactTime() {
        return (this.activity.getWeeklyContactTime());
    }


    /**
     *  Gets the weekly individual time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyIndividualEffort() {
        return (this.activity.getWeeklyIndividualEffort());
    }


    /**
     *  Gets the activity record corresponding to the given <code> Activity 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  activityRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(activityRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  activityRecordType the type of activity record to retrieve 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        return (this.activity.getActivityRecord(activityRecordType));
    }
}

