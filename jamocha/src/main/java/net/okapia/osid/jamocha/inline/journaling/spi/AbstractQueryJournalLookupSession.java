//
// AbstractQueryJournalLookupSession.java
//
//    An inline adapter that maps a JournalLookupSession to
//    a JournalQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a JournalLookupSession to
 *  a JournalQuerySession.
 */

public abstract class AbstractQueryJournalLookupSession
    extends net.okapia.osid.jamocha.journaling.spi.AbstractJournalLookupSession
    implements org.osid.journaling.JournalLookupSession {

    private final org.osid.journaling.JournalQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryJournalLookupSession.
     *
     *  @param querySession the underlying journal query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryJournalLookupSession(org.osid.journaling.JournalQuerySession querySession) {
        nullarg(querySession, "journal query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Journal</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJournals() {
        return (this.session.canSearchJournals());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Journal</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Journal</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Journal</code> and
     *  retained for compatibility.
     *
     *  @param  journalId <code>Id</code> of the
     *          <code>Journal</code>
     *  @return the journal
     *  @throws org.osid.NotFoundException <code>journalId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchId(journalId, true);
        org.osid.journaling.JournalList journals = this.session.getJournalsByQuery(query);
        if (journals.hasNext()) {
            return (journals.getNextJournal());
        } 
        
        throw new org.osid.NotFoundException(journalId + " not found");
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Journals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  journalIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>journalIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByIds(org.osid.id.IdList journalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();

        try (org.osid.id.IdList ids = journalIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getJournalsByQuery(query));
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> which does not include
     *  journals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchGenusType(journalGenusType, true);
        return (this.session.getJournalsByQuery(query));
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> and include any additional
     *  journals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByParentGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchParentGenusType(journalGenusType, true);
        return (this.session.getJournalsByQuery(query));
    }


    /**
     *  Gets a <code>JournalList</code> containing the given
     *  journal record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalRecordType a journal record type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByRecordType(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchRecordType(journalRecordType, true);
        return (this.session.getJournalsByQuery(query));
    }


    /**
     *  Gets a <code>JournalList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known journals or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  journals that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Journal</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getJournalsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Journals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Journals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.journaling.JournalQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getJournalsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.journaling.JournalQuery getQuery() {
        org.osid.journaling.JournalQuery query = this.session.getJournalQuery();
        return (query);
    }
}
