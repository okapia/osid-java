//
// AbstractIndexedMapJobProcessorLookupSession.java
//
//    A simple framework for providing a JobProcessor lookup service
//    backed by a fixed collection of job processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a JobProcessor lookup service backed by a
 *  fixed collection of job processors. The job processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some job processors may be compatible
 *  with more types than are indicated through these job processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJobProcessorLookupSession
    extends AbstractMapJobProcessorLookupSession
    implements org.osid.resourcing.rules.JobProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobProcessor> jobProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobProcessor> jobProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobProcessor>());


    /**
     *  Makes a <code>JobProcessor</code> available in this session.
     *
     *  @param  jobProcessor a job processor
     *  @throws org.osid.NullArgumentException <code>jobProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJobProcessor(org.osid.resourcing.rules.JobProcessor jobProcessor) {
        super.putJobProcessor(jobProcessor);

        this.jobProcessorsByGenus.put(jobProcessor.getGenusType(), jobProcessor);
        
        try (org.osid.type.TypeList types = jobProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobProcessorsByRecord.put(types.getNextType(), jobProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a job processor from this session.
     *
     *  @param jobProcessorId the <code>Id</code> of the job processor
     *  @throws org.osid.NullArgumentException <code>jobProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJobProcessor(org.osid.id.Id jobProcessorId) {
        org.osid.resourcing.rules.JobProcessor jobProcessor;
        try {
            jobProcessor = getJobProcessor(jobProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.jobProcessorsByGenus.remove(jobProcessor.getGenusType());

        try (org.osid.type.TypeList types = jobProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobProcessorsByRecord.remove(types.getNextType(), jobProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJobProcessor(jobProcessorId);
        return;
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  job processor genus <code>Type</code> which does not include
     *  job processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known job processors or an error results. Otherwise,
     *  the returned list may contain only those job processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  jobProcessorGenusType a job processor genus type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessor.ArrayJobProcessorList(this.jobProcessorsByGenus.get(jobProcessorGenusType)));
    }


    /**
     *  Gets a <code>JobProcessorList</code> containing the given
     *  job processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known job processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  job processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  jobProcessorRecordType a job processor record type 
     *  @return the returned <code>jobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByRecordType(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessor.ArrayJobProcessorList(this.jobProcessorsByRecord.get(jobProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobProcessorsByGenus.clear();
        this.jobProcessorsByRecord.clear();

        super.close();

        return;
    }
}
