//
// AbstractAssemblyIssueQuery.java
//
//     An IssueQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.issue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An IssueQuery that stores terms.
 */

public abstract class AbstractAssemblyIssueQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.tracking.IssueQuery,
               org.osid.tracking.IssueQueryInspector,
               org.osid.tracking.IssueSearchOrder {

    private final java.util.Collection<org.osid.tracking.records.IssueQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.IssueQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.IssueSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyIssueQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyIssueQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        getAssembler().clearTerms(getQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (getAssembler().getIdTerms(getQueueIdColumn()));
    }


    /**
     *  Orders the results by queue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQueue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQueueColumn(), style);
        return;
    }


    /**
     *  Gets the QueueId column name.
     *
     * @return the column name
     */

    protected String getQueueIdColumn() {
        return ("queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        getAssembler().clearTerms(getQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.tracking.QueueQueryInspector[0]);
    }


    /**
     *  Tests if a queue search order is available. 
     *
     *  @return <code> true </code> if a queue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the queue search order. 
     *
     *  @return the queue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsQueueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchOrder getQueueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQueueSearchOrder() is false");
    }


    /**
     *  Gets the Queue column name.
     *
     * @return the column name
     */

    protected String getQueueColumn() {
        return ("queue");
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the customer resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Orders the results by the customer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer query terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCustomerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Sets the topic <code> Id </code> for this query. 
     *
     *  @param  topicId the topic <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> topicId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTopicId(org.osid.id.Id topicId, boolean match) {
        getAssembler().addIdTerm(getTopicIdColumn(), topicId, match);
        return;
    }


    /**
     *  Clears the topic <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTopicIdTerms() {
        getAssembler().clearTerms(getTopicIdColumn());
        return;
    }


    /**
     *  Gets the issue topic <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTopicIdTerms() {
        return (getAssembler().getIdTerms(getTopicIdColumn()));
    }


    /**
     *  Orders the results by the topic. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTopic(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTopicColumn(), style);
        return;
    }


    /**
     *  Gets the TopicId column name.
     *
     * @return the column name
     */

    protected String getTopicIdColumn() {
        return ("topic_id");
    }


    /**
     *  Tests if a <code> TopicQuery </code> is available. 
     *
     *  @return <code> true </code> if a topic query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopicQuery() {
        return (false);
    }


    /**
     *  Gets the query for a topic. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the topic query 
     *  @throws org.osid.UnimplementedException <code> supportsTopicQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getTopicQuery() {
        throw new org.osid.UnimplementedException("supportsTopicQuery() is false");
    }


    /**
     *  Matches issues that have any topic. 
     *
     *  @param  match <code> true </code> to match issues with any topic, 
     *          <code> false </code> to match issues with no topic 
     */

    @OSID @Override
    public void matchAnyTopic(boolean match) {
        getAssembler().addIdWildcardTerm(getTopicColumn(), match);
        return;
    }


    /**
     *  Clears the topic query terms. 
     */

    @OSID @Override
    public void clearTopicTerms() {
        getAssembler().clearTerms(getTopicColumn());
        return;
    }


    /**
     *  Gets the topic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getTopicTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Tests if a subject search order is available. 
     *
     *  @return <code> true </code> if a subject search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopicSearchOrder() {
        return (false);
    }


    /**
     *  Gets the topic search order. 
     *
     *  @return the subject search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsTopicSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectSearchOrder getTopicSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTopicSearchOrder() is false");
    }


    /**
     *  Gets the Topic column name.
     *
     * @return the column name
     */

    protected String getTopicColumn() {
        return ("topic");
    }


    /**
     *  Sets the subtask <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSubtaskId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getSubtaskIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the subtask issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSubtaskIdTerms() {
        getAssembler().clearTerms(getSubtaskIdColumn());
        return;
    }


    /**
     *  Gets the subtask issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubtaskIdTerms() {
        return (getAssembler().getIdTerms(getSubtaskIdColumn()));
    }


    /**
     *  Gets the SubtaskId column name.
     *
     * @return the column name
     */

    protected String getSubtaskIdColumn() {
        return ("subtask_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a subtask issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subtask issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the subtask issue query 
     *  @throws org.osid.UnimplementedException <code> supportsSubtaskQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getSubtaskQuery() {
        throw new org.osid.UnimplementedException("supportsSubtaskQuery() is false");
    }


    /**
     *  Matches issues that have any subtask. 
     *
     *  @param  match <code> true </code> to match issues with any subtasks, 
     *          <code> false </code> to match issues on no subtasks 
     */

    @OSID @Override
    public void matchAnySubtask(boolean match) {
        getAssembler().addIdWildcardTerm(getSubtaskColumn(), match);
        return;
    }


    /**
     *  Clears the subtask query terms. 
     */

    @OSID @Override
    public void clearSubtaskTerms() {
        getAssembler().clearTerms(getSubtaskColumn());
        return;
    }


    /**
     *  Gets the subtask issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getSubtaskTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the Subtask column name.
     *
     * @return the column name
     */

    protected String getSubtaskColumn() {
        return ("subtask");
    }


    /**
     *  Sets the master issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMasterIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getMasterIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the master issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMasterIssueIdTerms() {
        getAssembler().clearTerms(getMasterIssueIdColumn());
        return;
    }


    /**
     *  Gets the master issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMasterIssueIdTerms() {
        return (getAssembler().getIdTerms(getMasterIssueIdColumn()));
    }


    /**
     *  Orders the results by the master issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMasterIssue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMasterIssueColumn(), style);
        return;
    }


    /**
     *  Gets the MasterIssueId column name.
     *
     * @return the column name
     */

    protected String getMasterIssueIdColumn() {
        return ("master_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a master issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMasterIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a master issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the master issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMasterIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getMasterIssueQuery() {
        throw new org.osid.UnimplementedException("supportsMasterIssueQuery() is false");
    }


    /**
     *  Matches issues that are subtasks. 
     *
     *  @param  match <code> true </code> to match issues with any master 
     *          issue, <code> false </code> to match issues on no master 
     *          issues 
     */

    @OSID @Override
    public void matchAnyMasterIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getMasterIssueColumn(), match);
        return;
    }


    /**
     *  Clears the linked issue query terms. 
     */

    @OSID @Override
    public void clearMasterIssueTerms() {
        getAssembler().clearTerms(getMasterIssueColumn());
        return;
    }


    /**
     *  Gets the master issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getMasterIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMasterIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsMasterIssueSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchOrder getMasterIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsMasterIssueSearchOrder() is false");
    }


    /**
     *  Gets the MasterIssue column name.
     *
     * @return the column name
     */

    protected String getMasterIssueColumn() {
        return ("master_issue");
    }


    /**
     *  Sets the linked issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDuplicateIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getDuplicateIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the duplicate issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDuplicateIssueIdTerms() {
        getAssembler().clearTerms(getDuplicateIssueIdColumn());
        return;
    }


    /**
     *  Gets the duplicate issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDuplicateIssueIdTerms() {
        return (getAssembler().getIdTerms(getDuplicateIssueIdColumn()));
    }


    /**
     *  Gets the DuplicateIssueId column name.
     *
     * @return the column name
     */

    protected String getDuplicateIssueIdColumn() {
        return ("duplicate_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a duplicate issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a duplicate issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the duplicate issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getDuplicateIssueQuery() {
        throw new org.osid.UnimplementedException("supportsDuplicateIssueQuery() is false");
    }


    /**
     *  Matches issues that are duplicated by any other issue. 
     *
     *  @param  match <code> true </code> to match duplicate issues, <code> 
     *          false </code> to match unlinked issues 
     */

    @OSID @Override
    public void matchAnyDuplicateIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getDuplicateIssueColumn(), match);
        return;
    }


    /**
     *  Clears the duplicate issue query terms. 
     */

    @OSID @Override
    public void clearDuplicateIssueTerms() {
        getAssembler().clearTerms(getDuplicateIssueColumn());
        return;
    }


    /**
     *  Gets the duplicate issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getDuplicateIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the DuplicateIssue column name.
     *
     * @return the column name
     */

    protected String getDuplicateIssueColumn() {
        return ("duplicate_issue");
    }


    /**
     *  Sets the branched issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchedIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getBranchedIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the branched issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBranchedIssueIdTerms() {
        getAssembler().clearTerms(getBranchedIssueIdColumn());
        return;
    }


    /**
     *  Gets the branched issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBranchedIssueIdTerms() {
        return (getAssembler().getIdTerms(getBranchedIssueIdColumn()));
    }


    /**
     *  Gets the BranchedIssueId column name.
     *
     * @return the column name
     */

    protected String getBranchedIssueIdColumn() {
        return ("branched_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a branched issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branched issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the branched issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBranchedIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBranchedIssueQuery() is false");
    }


    /**
     *  Matches issues that branched to any other issue. 
     *
     *  @param  match <code> true </code> to match issues with branches, 
     *          <code> false </code> to match issues with no branches 
     */

    @OSID @Override
    public void matchAnyBranchedIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getBranchedIssueColumn(), match);
        return;
    }


    /**
     *  Clears the branched issue query terms. 
     */

    @OSID @Override
    public void clearBranchedIssueTerms() {
        getAssembler().clearTerms(getBranchedIssueColumn());
        return;
    }


    /**
     *  Gets the branched issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getBranchedIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the BranchedIssue column name.
     *
     * @return the column name
     */

    protected String getBranchedIssueColumn() {
        return ("branched_issue");
    }


    /**
     *  Sets the root of the branch issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRootIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getRootIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the root issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRootIssueIdTerms() {
        getAssembler().clearTerms(getRootIssueIdColumn());
        return;
    }


    /**
     *  Gets the root issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRootIssueIdTerms() {
        return (getAssembler().getIdTerms(getRootIssueIdColumn()));
    }


    /**
     *  Orders the results by the root issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRootIssue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRootIssueColumn(), style);
        return;
    }


    /**
     *  Gets the RootIssueId column name.
     *
     * @return the column name
     */

    protected String getRootIssueIdColumn() {
        return ("root_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a root issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRootIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a root of the branch issue. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the root issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRootIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getRootIssueQuery() {
        throw new org.osid.UnimplementedException("supportsRootIssueQuery() is false");
    }


    /**
     *  Matches issue branches with a root. 
     *
     *  @param  match <code> true </code> to match branches with a root, 
     *          <code> false </code> to match issues that are not a branch of 
     *          another issue 
     */

    @OSID @Override
    public void matchAnyRootIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getRootIssueColumn(), match);
        return;
    }


    /**
     *  Clears the root issue query terms. 
     */

    @OSID @Override
    public void clearRootIssueTerms() {
        getAssembler().clearTerms(getRootIssueColumn());
        return;
    }


    /**
     *  Gets the root issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getRootIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRootIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRootIssueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchOrder getRootIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRootIssueSearchOrder() is false");
    }


    /**
     *  Gets the RootIssue column name.
     *
     * @return the column name
     */

    protected String getRootIssueColumn() {
        return ("root_issue");
    }


    /**
     *  Matches issues with the given priority type. 
     *
     *  @param  priorityType the priority <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriorityType(org.osid.type.Type priorityType, 
                                  boolean match) {
        getAssembler().addTypeTerm(getPriorityTypeColumn(), priorityType, match);
        return;
    }


    /**
     *  Clears the priority type query terms. 
     */

    @OSID @Override
    public void clearPriorityTypeTerms() {
        getAssembler().clearTerms(getPriorityTypeColumn());
        return;
    }


    /**
     *  Gets the priority <code> Type </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getPriorityTypeTerms() {
        return (getAssembler().getTypeTerms(getPriorityTypeColumn()));
    }


    /**
     *  Gets the PriorityType column name.
     *
     * @return the column name
     */

    protected String getPriorityTypeColumn() {
        return ("priority_type");
    }


    /**
     *  Sets the creator <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCreatorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatorIdTerms() {
        getAssembler().clearTerms(getCreatorIdColumn());
        return;
    }


    /**
     *  Gets the creator agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreatorIdTerms() {
        return (getAssembler().getIdTerms(getCreatorIdColumn()));
    }


    /**
     *  Orders the results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreator(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatorColumn(), style);
        return;
    }


    /**
     *  Gets the CreatorId column name.
     *
     * @return the column name
     */

    protected String getCreatorIdColumn() {
        return ("creator_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCreatorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCreatorQuery() {
        throw new org.osid.UnimplementedException("supportsCreatorQuery() is false");
    }


    /**
     *  Clears the creator query terms. 
     */

    @OSID @Override
    public void clearCreatorTerms() {
        getAssembler().clearTerms(getCreatorColumn());
        return;
    }


    /**
     *  Gets the creator query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCreatorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCreatorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCreatorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatorSearchOrder() is false");
    }


    /**
     *  Gets the Creator column name.
     *
     * @return the column name
     */

    protected String getCreatorColumn() {
        return ("creator");
    }


    /**
     *  Sets the creating agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getCreatingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatingAgentIdTerms() {
        getAssembler().clearTerms(getCreatingAgentIdColumn());
        return;
    }


    /**
     *  Gets the creating agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreatingAgentIdTerms() {
        return (getAssembler().getIdTerms(getCreatingAgentIdColumn()));
    }


    /**
     *  Orders the results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the CreatingAgentId column name.
     *
     * @return the column name
     */

    protected String getCreatingAgentIdColumn() {
        return ("creating_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreatingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getCreatingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsCreatingAgentQuery() is false");
    }


    /**
     *  Clears the creating agent query terms. 
     */

    @OSID @Override
    public void clearCreatingAgentTerms() {
        getAssembler().clearTerms(getCreatingAgentColumn());
        return;
    }


    /**
     *  Gets the creating agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getCreatingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCreatingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCreatingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatingAgentSearchOrder() is false");
    }


    /**
     *  Gets the CreatingAgent column name.
     *
     * @return the column name
     */

    protected String getCreatingAgentColumn() {
        return ("creating_agent");
    }


    /**
     *  Matches issues created within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getCreatedDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the created date query terms. 
     */

    @OSID @Override
    public void clearCreatedDateTerms() {
        getAssembler().clearTerms(getCreatedDateColumn());
        return;
    }


    /**
     *  Gets the created date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCreatedDateColumn()));
    }


    /**
     *  Orders the results by the created date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatedDateColumn(), style);
        return;
    }


    /**
     *  Gets the CreatedDate column name.
     *
     * @return the column name
     */

    protected String getCreatedDateColumn() {
        return ("created_date");
    }


    /**
     *  Sets the reopener <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReopenerId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getReopenerIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the reopener <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReopenerIdTerms() {
        getAssembler().clearTerms(getReopenerIdColumn());
        return;
    }


    /**
     *  Gets the reopener resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReopenerIdTerms() {
        return (getAssembler().getIdTerms(getReopenerIdColumn()));
    }


    /**
     *  Orders the results by the reopener. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopener(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReopenerColumn(), style);
        return;
    }


    /**
     *  Gets the ReopenerId column name.
     *
     * @return the column name
     */

    protected String getReopenerIdColumn() {
        return ("reopener_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopenerQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource who reopened this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsReopenerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getReopenerQuery() {
        throw new org.osid.UnimplementedException("supportsReopenerQuery() is false");
    }


    /**
     *  Clears the reopener query terms. 
     */

    @OSID @Override
    public void clearReopenerTerms() {
        getAssembler().clearTerms(getReopenerColumn());
        return;
    }


    /**
     *  Gets the reopener resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getReopenerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopenerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopenerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getReopenerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReopenerSearchOrder() is false");
    }


    /**
     *  Gets the Reopener column name.
     *
     * @return the column name
     */

    protected String getReopenerColumn() {
        return ("reopener");
    }


    /**
     *  Sets the reopening agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReopeningAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getReopeningAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the reopening agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearReopeningAgentIdTerms() {
        getAssembler().clearTerms(getReopeningAgentIdColumn());
        return;
    }


    /**
     *  Gets the reopening agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReopeningAgentIdTerms() {
        return (getAssembler().getIdTerms(getReopeningAgentIdColumn()));
    }


    /**
     *  Orders the results by the reopener. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopeningAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReopeningAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ReopeningAgentId column name.
     *
     * @return the column name
     */

    protected String getReopeningAgentIdColumn() {
        return ("reopening_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopeningAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who reopened this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReopeningAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getReopeningAgentQuery() {
        throw new org.osid.UnimplementedException("supportsReopeningAgentQuery() is false");
    }


    /**
     *  Clears the reopner query terms. 
     */

    @OSID @Override
    public void clearReopeningAgentTerms() {
        getAssembler().clearTerms(getReopeningAgentColumn());
        return;
    }


    /**
     *  Gets the reopener query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getReopeningAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReopeningAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopeningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getReopeningAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReopeningAgentSearchOrder() is false");
    }


    /**
     *  Gets the ReopeningAgent column name.
     *
     * @return the column name
     */

    protected String getReopeningAgentColumn() {
        return ("reopening_agent");
    }


    /**
     *  Matches issues reopened within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchReopenedDate(org.osid.calendaring.DateTime from, 
                                  org.osid.calendaring.DateTime to, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getReopenedDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches issues that have any reopened date. 
     *
     *  @param  match <code> true </code> to match issues with any reopened 
     *          date, <code> false </code> to match issues with no reopened 
     *          date 
     */

    @OSID @Override
    public void matchAnyReopenedDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getReopenedDateColumn(), match);
        return;
    }


    /**
     *  Clears the reopened date query terms. 
     */

    @OSID @Override
    public void clearReopenedDateTerms() {
        getAssembler().clearTerms(getReopenedDateColumn());
        return;
    }


    /**
     *  Gets the reopened date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReopenedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getReopenedDateColumn()));
    }


    /**
     *  Orders the results by the reopened date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReopenedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReopenedDateColumn(), style);
        return;
    }


    /**
     *  Gets the ReopenedDate column name.
     *
     * @return the column name
     */

    protected String getReopenedDateColumn() {
        return ("reopened_date");
    }


    /**
     *  Matches issue due dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDueDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches issues that have any due date. 
     *
     *  @param  match <code> true </code> to match issues with any due date, 
     *          <code> false </code> to match issues with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDueDateColumn(), match);
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        getAssembler().clearTerms(getDueDateColumn());
        return;
    }


    /**
     *  Gets the due date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDueDateColumn()));
    }


    /**
     *  Orders the results by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDueDateColumn(), style);
        return;
    }


    /**
     *  Gets the DueDate column name.
     *
     * @return the column name
     */

    protected String getDueDateColumn() {
        return ("due_date");
    }


    /**
     *  Matches issues that are pending a customer response. 
     *
     *  @param  match <code> true </code> to match pending issues, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchPending(boolean match) {
        getAssembler().addBooleanTerm(getPendingColumn(), match);
        return;
    }


    /**
     *  Clears the pending query terms. 
     */

    @OSID @Override
    public void clearPendingTerms() {
        getAssembler().clearTerms(getPendingColumn());
        return;
    }


    /**
     *  Gets the pending query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPendingTerms() {
        return (getAssembler().getBooleanTerms(getPendingColumn()));
    }


    /**
     *  Orders the results by the pending state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPending(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPendingColumn(), style);
        return;
    }


    /**
     *  Gets the Pending column name.
     *
     * @return the column name
     */

    protected String getPendingColumn() {
        return ("pending");
    }


    /**
     *  Sets the blocking issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockingIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getBlockingIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the blocking issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockingIssueIdTerms() {
        getAssembler().clearTerms(getBlockingIssueIdColumn());
        return;
    }


    /**
     *  Gets the blocking issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlockingIssueIdTerms() {
        return (getAssembler().getIdTerms(getBlockingIssueIdColumn()));
    }


    /**
     *  Gets the BlockingIssueId column name.
     *
     * @return the column name
     */

    protected String getBlockingIssueIdColumn() {
        return ("blocking_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a blocking issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blocking issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the blocking issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBlockingIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBlockingIssueQuery() is false");
    }


    /**
     *  Matches any blocking issue. 
     *
     *  @param  match <code> true </code> to match blocking issues, <code> 
     *          false </code> to match issues not blocking 
     */

    @OSID @Override
    public void matchAnyBlockingIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getBlockingIssueColumn(), match);
        return;
    }


    /**
     *  Clears the blocking issue query terms. 
     */

    @OSID @Override
    public void clearBlockingIssueTerms() {
        getAssembler().clearTerms(getBlockingIssueColumn());
        return;
    }


    /**
     *  Gets the blocking issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getBlockingIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the BlockingIssue column name.
     *
     * @return the column name
     */

    protected String getBlockingIssueColumn() {
        return ("blocking_issue");
    }


    /**
     *  Sets the blocked issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlockedIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getBlockedIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the blocked issue issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBlockedIssueIdTerms() {
        getAssembler().clearTerms(getBlockedIssueIdColumn());
        return;
    }


    /**
     *  Gets the blocked issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBlockedIssueIdTerms() {
        return (getAssembler().getIdTerms(getBlockedIssueIdColumn()));
    }


    /**
     *  Gets the BlockedIssueId column name.
     *
     * @return the column name
     */

    protected String getBlockedIssueIdColumn() {
        return ("blocked_issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available. 
     *
     *  @return <code> true </code> if a blocked issue query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockedIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blocked issue. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the blocking issue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockedIssueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getBlockedIssueQuery() {
        throw new org.osid.UnimplementedException("supportsBlockedIssueQuery() is false");
    }


    /**
     *  Matches any issue blocked. 
     *
     *  @param  match <code> true </code> to match issues blocked, <code> 
     *          false </code> to match issues that are not blocked 
     */

    @OSID @Override
    public void matchAnyBlockedIssue(boolean match) {
        getAssembler().addIdWildcardTerm(getBlockedIssueColumn(), match);
        return;
    }


    /**
     *  Clears the blocked issue query terms. 
     */

    @OSID @Override
    public void clearBlockedIssueTerms() {
        getAssembler().clearTerms(getBlockedIssueColumn());
        return;
    }


    /**
     *  Gets the blocked issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getBlockedIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the BlockedIssue column name.
     *
     * @return the column name
     */

    protected String getBlockedIssueColumn() {
        return ("blocked_issue");
    }


    /**
     *  Sets the resolver <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResolverId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResolverIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resolver <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResolverIdTerms() {
        getAssembler().clearTerms(getResolverIdColumn());
        return;
    }


    /**
     *  Gets the resolver resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResolverIdTerms() {
        return (getAssembler().getIdTerms(getResolverIdColumn()));
    }


    /**
     *  Orders the results by the resolver. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolver(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResolverColumn(), style);
        return;
    }


    /**
     *  Gets the ResolverId column name.
     *
     * @return the column name
     */

    protected String getResolverIdColumn() {
        return ("resolver_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolverQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource who resolved this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResolverQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResolverQuery() {
        throw new org.osid.UnimplementedException("supportsResolverQuery() is false");
    }


    /**
     *  Clears the resolver query terms. 
     */

    @OSID @Override
    public void clearResolverTerms() {
        getAssembler().clearTerms(getResolverColumn());
        return;
    }


    /**
     *  Gets the resolver resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResolverTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolverSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopenerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResolverSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResolverSearchOrder() is false");
    }


    /**
     *  Gets the Resolver column name.
     *
     * @return the column name
     */

    protected String getResolverColumn() {
        return ("resolver");
    }


    /**
     *  Sets the resolving agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResolvingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getResolvingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the resolving agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResolvingAgentIdTerms() {
        getAssembler().clearTerms(getResolvingAgentIdColumn());
        return;
    }


    /**
     *  Gets the resolving agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResolvingAgentIdTerms() {
        return (getAssembler().getIdTerms(getResolvingAgentIdColumn()));
    }


    /**
     *  Orders the results by the resolving agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolvingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResolvingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ResolvingAgentId column name.
     *
     * @return the column name
     */

    protected String getResolvingAgentIdColumn() {
        return ("resolving_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolvingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who resolved this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResolvingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getResolvingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsResolvingAgentQuery() is false");
    }


    /**
     *  Clears the resolving query terms. 
     */

    @OSID @Override
    public void clearResolvingAgentTerms() {
        getAssembler().clearTerms(getResolvingAgentColumn());
        return;
    }


    /**
     *  Gets the resolving agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getResolvingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResolvingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReopeningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getResolvingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResolvingAgentSearchOrder() is false");
    }


    /**
     *  Gets the ResolvingAgent column name.
     *
     * @return the column name
     */

    protected String getResolvingAgentColumn() {
        return ("resolving_agent");
    }


    /**
     *  Matches issue resolved dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchResolvedDate(org.osid.calendaring.DateTime from, 
                                  org.osid.calendaring.DateTime to, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getResolvedDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches any issue that has been resolved. 
     *
     *  @param  match <code> true </code> to match resolved issues, <code> 
     *          false </code> to match unresolved issues 
     */

    @OSID @Override
    public void matchAnyResolvedDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getResolvedDateColumn(), match);
        return;
    }


    /**
     *  Clears the resolved date query terms. 
     */

    @OSID @Override
    public void clearResolvedDateTerms() {
        getAssembler().clearTerms(getResolvedDateColumn());
        return;
    }


    /**
     *  Gets the date resolved query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getResolvedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getResolvedDateColumn()));
    }


    /**
     *  Orders the results by the resolved date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolvedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResolvedDateColumn(), style);
        return;
    }


    /**
     *  Gets the ResolvedDate column name.
     *
     * @return the column name
     */

    protected String getResolvedDateColumn() {
        return ("resolved_date");
    }


    /**
     *  Matches issues with the given resolution type. 
     *
     *  @param  resolutionType the resolution <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resolutionType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchResolutionType(org.osid.type.Type resolutionType, 
                                    boolean match) {
        getAssembler().addTypeTerm(getResolutionTypeColumn(), resolutionType, match);
        return;
    }


    /**
     *  Clears the resolution type query terms. 
     */

    @OSID @Override
    public void clearResolutionTypeTerms() {
        getAssembler().clearTerms(getResolutionTypeColumn());
        return;
    }


    /**
     *  Gets the resolution <code> Type </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getResolutionTypeTerms() {
        return (getAssembler().getTypeTerms(getResolutionTypeColumn()));
    }


    /**
     *  Orders the results by the resolution type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResolutionType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResolutionTypeColumn(), style);
        return;
    }


    /**
     *  Gets the ResolutionType column name.
     *
     * @return the column name
     */

    protected String getResolutionTypeColumn() {
        return ("resolution_type");
    }


    /**
     *  Sets the closer resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCloserId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCloserIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the closer resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCloserIdTerms() {
        getAssembler().clearTerms(getCloserIdColumn());
        return;
    }


    /**
     *  Gets the closer resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCloserIdTerms() {
        return (getAssembler().getIdTerms(getCloserIdColumn()));
    }


    /**
     *  Orders the results by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloser(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCloserColumn(), style);
        return;
    }


    /**
     *  Gets the CloserId column name.
     *
     * @return the column name
     */

    protected String getCloserIdColumn() {
        return ("closer_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who closed this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCloserQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCloserQuery() {
        throw new org.osid.UnimplementedException("supportsCloserQuery() is false");
    }


    /**
     *  Clears the closer query terms. 
     */

    @OSID @Override
    public void clearCloserTerms() {
        getAssembler().clearTerms(getCloserColumn());
        return;
    }


    /**
     *  Gets the closer resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCloserTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCloserSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCloserSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCloserSearchOrder() is false");
    }


    /**
     *  Gets the Closer column name.
     *
     * @return the column name
     */

    protected String getCloserColumn() {
        return ("closer");
    }


    /**
     *  Sets the closing agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchClosingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getClosingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the closing agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearClosingAgentIdTerms() {
        getAssembler().clearTerms(getClosingAgentIdColumn());
        return;
    }


    /**
     *  Gets the closing agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getClosingAgentIdTerms() {
        return (getAssembler().getIdTerms(getClosingAgentIdColumn()));
    }


    /**
     *  Orders the results by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the ClosingAgentId column name.
     *
     * @return the column name
     */

    protected String getClosingAgentIdColumn() {
        return ("closing_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if a closing agent query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the agent who closed this issue. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getClosingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsClosingAgentQuery() is false");
    }


    /**
     *  Clears the closing agent query terms. 
     */

    @OSID @Override
    public void clearClosingAgentTerms() {
        getAssembler().clearTerms(getClosingAgentColumn());
        return;
    }


    /**
     *  Gets the closing agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getClosingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsClosingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getClosingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsClosingAgentSearchOrder() is false");
    }


    /**
     *  Gets the ClosingAgent column name.
     *
     * @return the column name
     */

    protected String getClosingAgentColumn() {
        return ("closing_agent");
    }


    /**
     *  Matches issue closed dates within the given date range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchClosedDate(org.osid.calendaring.DateTime from, 
                                org.osid.calendaring.DateTime to, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getClosedDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches any issue that has been closed. 
     *
     *  @param  match <code> true </code> to match closed issues, <code> false 
     *          </code> to match unclosed issues 
     */

    @OSID @Override
    public void matchAnyClosedDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getClosedDateColumn(), match);
        return;
    }


    /**
     *  Clears the closed date query terms. 
     */

    @OSID @Override
    public void clearClosedDateTerms() {
        getAssembler().clearTerms(getClosedDateColumn());
        return;
    }


    /**
     *  Gets the date closed query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClosedDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClosedDateColumn()));
    }


    /**
     *  Orders the results by the closed date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosedDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClosedDateColumn(), style);
        return;
    }


    /**
     *  Gets the ClosedDate column name.
     *
     * @return the column name
     */

    protected String getClosedDateColumn() {
        return ("closed_date");
    }


    /**
     *  Sets the currently assigned resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssignedResourceId(org.osid.id.Id resourceId, 
                                        boolean match) {
        getAssembler().addIdTerm(getAssignedResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the currently assigned resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssignedResourceIdTerms() {
        getAssembler().clearTerms(getAssignedResourceIdColumn());
        return;
    }


    /**
     *  Gets the currently assigned resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssignedResourceIdTerms() {
        return (getAssembler().getIdTerms(getAssignedResourceIdColumn()));
    }


    /**
     *  Orders the results by the assigned resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssignedResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssignedResourceColumn(), style);
        return;
    }


    /**
     *  Gets the AssignedResourceId column name.
     *
     * @return the column name
     */

    protected String getAssignedResourceIdColumn() {
        return ("assigned_resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an assigned resource query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssignedResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the current assigned resource. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssignedResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getAssignedResourceQuery() {
        throw new org.osid.UnimplementedException("supportsAssignedResourceQuery() is false");
    }


    /**
     *  Matches any issue that has any current assigned resource. 
     *
     *  @param  match <code> true </code> to match issues with any currently 
     *          assigned resource, <code> false </code> to match issues with 
     *          no currently assigned resource 
     */

    @OSID @Override
    public void matchAnyAssignedResource(boolean match) {
        getAssembler().addIdWildcardTerm(getAssignedResourceColumn(), match);
        return;
    }


    /**
     *  Clears the current assigned resource query terms. 
     */

    @OSID @Override
    public void clearAssignedResourceTerms() {
        getAssembler().clearTerms(getAssignedResourceColumn());
        return;
    }


    /**
     *  Gets the currently assigned resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getAssignedResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssignedResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAssignedResourceSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getAssignedResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssignedResourceSearchOrder() is false");
    }


    /**
     *  Gets the AssignedResource column name.
     *
     * @return the column name
     */

    protected String getAssignedResourceColumn() {
        return ("assigned_resource");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match any 
     *  resource ever assigned to an issue. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the assigned resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if an assigned resource query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for the assigned resource. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches any issue that has any assigned resource. 
     *
     *  @param  match <code> true </code> to match issues with any resource 
     *          ever assigned, <code> false </code> to match issues that never 
     *          had an assigned resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        getAssembler().addIdWildcardTerm(getResourceColumn(), match);
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets the log entry <code> Id </code> for this query to match issues 
     *  along the given log entry. 
     *
     *  @param  logEntryId the log entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> logEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLogEntryId(org.osid.id.Id logEntryId, boolean match) {
        getAssembler().addIdTerm(getLogEntryIdColumn(), logEntryId, match);
        return;
    }


    /**
     *  Clears the log entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLogEntryIdTerms() {
        getAssembler().clearTerms(getLogEntryIdColumn());
        return;
    }


    /**
     *  Gets the log entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLogEntryIdTerms() {
        return (getAssembler().getIdTerms(getLogEntryIdColumn()));
    }


    /**
     *  Gets the LogEntryId column name.
     *
     * @return the column name
     */

    protected String getLogEntryIdColumn() {
        return ("log_entry_id");
    }


    /**
     *  Tests if a <code> LogEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a log entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a log entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the log entry query 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryQuery getLogEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLogEntryQuery() is false");
    }


    /**
     *  Matches issues that are used on any log entry. 
     *
     *  @param  match <code> true </code> to match issues with any log entry, 
     *          <code> false </code> to match issues with no log entries 
     */

    @OSID @Override
    public void matchAnyLogEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getLogEntryColumn(), match);
        return;
    }


    /**
     *  Clears the log entry query terms. 
     */

    @OSID @Override
    public void clearLogEntryTerms() {
        getAssembler().clearTerms(getLogEntryColumn());
        return;
    }


    /**
     *  Gets the log entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryQueryInspector[] getLogEntryTerms() {
        return (new org.osid.tracking.LogEntryQueryInspector[0]);
    }


    /**
     *  Gets the LogEntry column name.
     *
     * @return the column name
     */

    protected String getLogEntryColumn() {
        return ("log_entry");
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match issues 
     *  assigned to front offices. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        getAssembler().addIdTerm(getFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        getAssembler().clearTerms(getFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getFrontOfficeIdColumn()));
    }


    /**
     *  Gets the FrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeIdColumn() {
        return ("front_office_id");
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        getAssembler().clearTerms(getFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the FrontOffice column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeColumn() {
        return ("front_office");
    }


    /**
     *  Orders the results by the issue status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStatus(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStatusColumn(), style);
        return;
    }


    /**
     *  Gets the status column name.
     *
     * @return the column name
     */

    protected String getStatusColumn() {
        return ("status");
    }


    /**
     *  Tests if this issue supports the given record
     *  <code>Type</code>.
     *
     *  @param  issueRecordType an issue record type 
     *  @return <code>true</code> if the issueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type issueRecordType) {
        for (org.osid.tracking.records.IssueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue query record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.IssueQueryRecord getIssueQueryRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  issueRecordType the issue record type 
     *  @return the issue query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.IssueQueryInspectorRecord getIssueQueryInspectorRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param issueRecordType the issue record type
     *  @return the issue search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(issueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.IssueSearchOrderRecord getIssueSearchOrderRecord(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.IssueSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(issueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(issueRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this issue. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param issueQueryRecord the issue query record
     *  @param issueQueryInspectorRecord the issue query inspector
     *         record
     *  @param issueSearchOrderRecord the issue search order record
     *  @param issueRecordType issue record type
     *  @throws org.osid.NullArgumentException
     *          <code>issueQueryRecord</code>,
     *          <code>issueQueryInspectorRecord</code>,
     *          <code>issueSearchOrderRecord</code> or
     *          <code>issueRecordTypeissue</code> is
     *          <code>null</code>
     */
            
    protected void addIssueRecords(org.osid.tracking.records.IssueQueryRecord issueQueryRecord, 
                                      org.osid.tracking.records.IssueQueryInspectorRecord issueQueryInspectorRecord, 
                                      org.osid.tracking.records.IssueSearchOrderRecord issueSearchOrderRecord, 
                                      org.osid.type.Type issueRecordType) {

        addRecordType(issueRecordType);

        nullarg(issueQueryRecord, "issue query record");
        nullarg(issueQueryInspectorRecord, "issue query inspector record");
        nullarg(issueSearchOrderRecord, "issue search odrer record");

        this.queryRecords.add(issueQueryRecord);
        this.queryInspectorRecords.add(issueQueryInspectorRecord);
        this.searchOrderRecords.add(issueSearchOrderRecord);
        
        return;
    }
}
