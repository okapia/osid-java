//
// InstallationMiter.java
//
//     Defines an Installation miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installation;


/**
 *  Defines an <code>Installation</code> miter for use with the builders.
 */

public interface InstallationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.installation.Installation {


    /**
     *  Sets the site.
     *
     *  @param site a site
     *  @throws org.osid.NullArgumentException <code>site</code> is
     *          <code>null</code>
     */

    public void setSite(org.osid.installation.Site site);


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    public void setPackage(org.osid.installation.Package pkg);


    /**
     *  Sets the depot.
     *
     *  @param depot a depot
     *  @throws org.osid.NullArgumentException <code>depot</code> is
     *          <code>null</code>
     */

    public void setDepot(org.osid.installation.Depot depot);


    /**
     *  Sets the install date.
     *
     *  @param date an install date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setInstallDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the last check date.
     *
     *  @param date a last check date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setLastCheckDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds an Installation record.
     *
     *  @param record an installation record
     *  @param recordType the type of installation record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addInstallationRecord(org.osid.installation.records.InstallationRecord record, org.osid.type.Type recordType);
}       


