//
// AbstractIndexedMapObstacleLookupSession.java
//
//    A simple framework for providing an Obstacle lookup service
//    backed by a fixed collection of obstacles with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Obstacle lookup service backed by a
 *  fixed collection of obstacles. The obstacles are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some obstacles may be compatible
 *  with more types than are indicated through these obstacle
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Obstacles</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapObstacleLookupSession
    extends AbstractMapObstacleLookupSession
    implements org.osid.mapping.path.ObstacleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Obstacle> obstaclesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Obstacle>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Obstacle> obstaclesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Obstacle>());


    /**
     *  Makes an <code>Obstacle</code> available in this session.
     *
     *  @param  obstacle an obstacle
     *  @throws org.osid.NullArgumentException <code>obstacle<code> is
     *          <code>null</code>
     */

    @Override
    protected void putObstacle(org.osid.mapping.path.Obstacle obstacle) {
        super.putObstacle(obstacle);

        this.obstaclesByGenus.put(obstacle.getGenusType(), obstacle);
        
        try (org.osid.type.TypeList types = obstacle.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.obstaclesByRecord.put(types.getNextType(), obstacle);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an obstacle from this session.
     *
     *  @param obstacleId the <code>Id</code> of the obstacle
     *  @throws org.osid.NullArgumentException <code>obstacleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeObstacle(org.osid.id.Id obstacleId) {
        org.osid.mapping.path.Obstacle obstacle;
        try {
            obstacle = getObstacle(obstacleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.obstaclesByGenus.remove(obstacle.getGenusType());

        try (org.osid.type.TypeList types = obstacle.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.obstaclesByRecord.remove(types.getNextType(), obstacle);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeObstacle(obstacleId);
        return;
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> which does not include
     *  obstacles of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known obstacles or an error results. Otherwise,
     *  the returned list may contain only those obstacles that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.obstacle.ArrayObstacleList(this.obstaclesByGenus.get(obstacleGenusType)));
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given
     *  obstacle record <code>Type</code>. In plenary mode, the
     *  returned list contains all known obstacles or an error
     *  results. Otherwise, the returned list may contain only those
     *  obstacles that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return the returned <code>obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByRecordType(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.obstacle.ArrayObstacleList(this.obstaclesByRecord.get(obstacleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.obstaclesByGenus.clear();
        this.obstaclesByRecord.clear();

        super.close();

        return;
    }
}
