//
// MutableIndexedMapProxyBankLookupSession
//
//    Implements a Bank lookup service backed by a collection of
//    banks indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements a Bank lookup service backed by a collection of
 *  banks. The banks are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some banks may be compatible
 *  with more types than are indicated through these bank
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of banks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyBankLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractIndexedMapBankLookupSession
    implements org.osid.assessment.BankLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBankLookupSession} with
     *  no bank.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyBankLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBankLookupSession} with
     *  a single bank.
     *
     *  @param  bank an bank
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyBankLookupSession(org.osid.assessment.Bank bank, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyBankLookupSession} using
     *  an array of banks.
     *
     *  @param  banks an array of banks
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code banks} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyBankLookupSession(org.osid.assessment.Bank[] banks, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBanks(banks);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyBankLookupSession} using
     *  a collection of banks.
     *
     *  @param  banks a collection of banks
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code banks} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyBankLookupSession(java.util.Collection<? extends org.osid.assessment.Bank> banks,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBanks(banks);
        return;
    }

    
    /**
     *  Makes a {@code Bank} available in this session.
     *
     *  @param  bank a bank
     *  @throws org.osid.NullArgumentException {@code bank{@code 
     *          is {@code null}
     */

    @Override
    public void putBank(org.osid.assessment.Bank bank) {
        super.putBank(bank);
        return;
    }


    /**
     *  Makes an array of banks available in this session.
     *
     *  @param  banks an array of banks
     *  @throws org.osid.NullArgumentException {@code banks{@code 
     *          is {@code null}
     */

    @Override
    public void putBanks(org.osid.assessment.Bank[] banks) {
        super.putBanks(banks);
        return;
    }


    /**
     *  Makes collection of banks available in this session.
     *
     *  @param  banks a collection of banks
     *  @throws org.osid.NullArgumentException {@code bank{@code 
     *          is {@code null}
     */

    @Override
    public void putBanks(java.util.Collection<? extends org.osid.assessment.Bank> banks) {
        super.putBanks(banks);
        return;
    }


    /**
     *  Removes a Bank from this session.
     *
     *  @param bankId the {@code Id} of the bank
     *  @throws org.osid.NullArgumentException {@code bankId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBank(org.osid.id.Id bankId) {
        super.removeBank(bankId);
        return;
    }    
}
