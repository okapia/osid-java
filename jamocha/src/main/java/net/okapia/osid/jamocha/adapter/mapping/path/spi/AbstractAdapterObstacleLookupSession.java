//
// AbstractAdapterObstacleLookupSession.java
//
//    An Obstacle lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Obstacle lookup session adapter.
 */

public abstract class AbstractAdapterObstacleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.ObstacleLookupSession {

    private final org.osid.mapping.path.ObstacleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterObstacleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterObstacleLookupSession(org.osid.mapping.path.ObstacleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code Obstacle} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupObstacles() {
        return (this.session.canLookupObstacles());
    }


    /**
     *  A complete view of the {@code Obstacle} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObstacleView() {
        this.session.useComparativeObstacleView();
        return;
    }


    /**
     *  A complete view of the {@code Obstacle} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObstacleView() {
        this.session.usePlenaryObstacleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacles in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active obstacles are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleView() {
        this.session.useActiveObstacleView();
        return;
    }


    /**
     *  Active and inactive obstacles are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleView() {
        this.session.useAnyStatusObstacleView();
        return;
    }
    
     
    /**
     *  Gets the {@code Obstacle} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Obstacle} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Obstacle} and
     *  retained for compatibility.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param obstacleId {@code Id} of the {@code Obstacle}
     *  @return the obstacle
     *  @throws org.osid.NotFoundException {@code obstacleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code obstacleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Obstacle getObstacle(org.osid.id.Id obstacleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacle(obstacleId));
    }


    /**
     *  Gets an {@code ObstacleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacles specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Obstacles} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Obstacle} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByIds(org.osid.id.IdList obstacleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstaclesByIds(obstacleIds));
    }


    /**
     *  Gets an {@code ObstacleList} corresponding to the given
     *  obstacle genus {@code Type} which does not include
     *  obstacles of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned {@code Obstacle} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstaclesByGenusType(obstacleGenusType));
    }


    /**
     *  Gets an {@code ObstacleList} corresponding to the given
     *  obstacle genus {@code Type} and include any additional
     *  obstacles with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned {@code Obstacle} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByParentGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstaclesByParentGenusType(obstacleGenusType));
    }


    /**
     *  Gets an {@code ObstacleList} containing the given
     *  obstacle record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return the returned {@code Obstacle} list
     *  @throws org.osid.NullArgumentException
     *          {@code obstacleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByRecordType(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstaclesByRecordType(obstacleRecordType));
    }

    
    /**
     *  Gets an {@code ObstacleList} containing the given path.
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *  
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path {@code Id} 
     *  @return the returned {@code Obstacle} list 
     *  @throws org.osid.NullArgumentException {@code pathId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstaclesForPath(pathId));
    }

    
    /**
     *  Gets an {@code ObstacleList} containing the given path between 
     *  the given coordinates inclusive. 
     *  
     *  In plenary mode, the returned list contains all known obstacles or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  obstacles that are accessible through this session. 
     *  
     *  In active mode, obstacles are returned that are currently active. In 
     *  any status mode, active and inactive obstacles are returned. 
     *
     *  @param  pathId a path {@code Id} 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned {@code Obstacle} list 
     *  @throws org.osid.NullArgumentException {@code pathId,
     *         coordinate}, or {@code distance} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                              org.osid.mapping.Coordinate coordinate, 
                                                                              org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getObstaclesForPathAtCoordinate(pathId, coordinate, distance));
    }


    /**
     *  Gets all {@code Obstacles}. 
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @return a list of {@code Obstacles} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstacles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObstacles());
    }
}
