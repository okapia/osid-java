//
// MutableMapProcessEnablerLookupSession
//
//    Implements a ProcessEnabler lookup service backed by a collection of
//    processEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a ProcessEnabler lookup service backed by a collection of
 *  process enablers. The process enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of process enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProcessEnablerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractMapProcessEnablerLookupSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProcessEnablerLookupSession}
     *  with no process enablers.
     *
     *  @param office the office
     *  @throws org.osid.NullArgumentException {@code office} is
     *          {@code null}
     */

      public MutableMapProcessEnablerLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessEnablerLookupSession} with a
     *  single processEnabler.
     *
     *  @param office the office  
     *  @param processEnabler a process enabler
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code processEnabler} is {@code null}
     */

    public MutableMapProcessEnablerLookupSession(org.osid.workflow.Office office,
                                           org.osid.workflow.rules.ProcessEnabler processEnabler) {
        this(office);
        putProcessEnabler(processEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessEnablerLookupSession}
     *  using an array of process enablers.
     *
     *  @param office the office
     *  @param processEnablers an array of process enablers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code processEnablers} is {@code null}
     */

    public MutableMapProcessEnablerLookupSession(org.osid.workflow.Office office,
                                           org.osid.workflow.rules.ProcessEnabler[] processEnablers) {
        this(office);
        putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProcessEnablerLookupSession}
     *  using a collection of process enablers.
     *
     *  @param office the office
     *  @param processEnablers a collection of process enablers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code processEnablers} is {@code null}
     */

    public MutableMapProcessEnablerLookupSession(org.osid.workflow.Office office,
                                           java.util.Collection<? extends org.osid.workflow.rules.ProcessEnabler> processEnablers) {

        this(office);
        putProcessEnablers(processEnablers);
        return;
    }

    
    /**
     *  Makes a {@code ProcessEnabler} available in this session.
     *
     *  @param processEnabler a process enabler
     *  @throws org.osid.NullArgumentException {@code processEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putProcessEnabler(org.osid.workflow.rules.ProcessEnabler processEnabler) {
        super.putProcessEnabler(processEnabler);
        return;
    }


    /**
     *  Makes an array of process enablers available in this session.
     *
     *  @param processEnablers an array of process enablers
     *  @throws org.osid.NullArgumentException {@code processEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putProcessEnablers(org.osid.workflow.rules.ProcessEnabler[] processEnablers) {
        super.putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Makes collection of process enablers available in this session.
     *
     *  @param processEnablers a collection of process enablers
     *  @throws org.osid.NullArgumentException {@code processEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putProcessEnablers(java.util.Collection<? extends org.osid.workflow.rules.ProcessEnabler> processEnablers) {
        super.putProcessEnablers(processEnablers);
        return;
    }


    /**
     *  Removes a ProcessEnabler from this session.
     *
     *  @param processEnablerId the {@code Id} of the process enabler
     *  @throws org.osid.NullArgumentException {@code processEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeProcessEnabler(org.osid.id.Id processEnablerId) {
        super.removeProcessEnabler(processEnablerId);
        return;
    }    
}
