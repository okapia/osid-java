//
// AbstractAssessmentAuthoringManager.java
//
//     An adapter for a AssessmentAuthoringManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AssessmentAuthoringManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAssessmentAuthoringManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.assessment.authoring.AssessmentAuthoringManager>
    implements org.osid.assessment.authoring.AssessmentAuthoringManager {


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentAuthoringManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAssessmentAuthoringManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentAuthoringManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentAuthoringManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartLookup() {
        return (getAdapteeManager().supportsAssessmentPartLookup());
    }


    /**
     *  Tests if querying assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartQuery() {
        return (getAdapteeManager().supportsAssessmentPartQuery());
    }


    /**
     *  Tests if searching assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearch() {
        return (getAdapteeManager().supportsAssessmentPartSearch());
    }


    /**
     *  Tests if an assessment part administrative service is supported. 
     *
     *  @return <code> true </code> if assessment part administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartAdmin() {
        return (getAdapteeManager().supportsAssessmentPartAdmin());
    }


    /**
     *  Tests if an assessment part notification service is supported. 
     *
     *  @return <code> true </code> if assessment part notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartNotification() {
        return (getAdapteeManager().supportsAssessmentPartNotification());
    }


    /**
     *  Tests if an assessment part bank lookup service is supported. 
     *
     *  @return <code> true </code> if an assessment part bank lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartBank() {
        return (getAdapteeManager().supportsAssessmentPartBank());
    }


    /**
     *  Tests if an assessment part bank service is supported. 
     *
     *  @return <code> true </code> if assessment part bank assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartBankAssignment() {
        return (getAdapteeManager().supportsAssessmentPartBankAssignment());
    }


    /**
     *  Tests if an assessment part bank lookup service is supported. 
     *
     *  @return <code> true </code> if an assessment part bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSmartBank() {
        return (getAdapteeManager().supportsAssessmentPartSmartBank());
    }


    /**
     *  Tests if an assessment part item service is supported for looking up 
     *  assessment part and item mappings. 
     *
     *  @return <code> true </code> if assessment part item service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartItem() {
        return (getAdapteeManager().supportsAssessmentPartItem());
    }


    /**
     *  Tests if an assessment part item design session is supported. 
     *
     *  @return <code> true </code> if an assessment part item design service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartItemDesign() {
        return (getAdapteeManager().supportsAssessmentPartItemDesign());
    }


    /**
     *  Tests if looking up sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleLookup() {
        return (getAdapteeManager().supportsSequenceRuleLookup());
    }


    /**
     *  Tests if querying sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleQuery() {
        return (getAdapteeManager().supportsSequenceRuleQuery());
    }


    /**
     *  Tests if searching sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSearch() {
        return (getAdapteeManager().supportsSequenceRuleSearch());
    }


    /**
     *  Tests if a sequence rule administrative service is supported. 
     *
     *  @return <code> true </code> if sequence rule administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleAdmin() {
        return (getAdapteeManager().supportsSequenceRuleAdmin());
    }


    /**
     *  Tests if a sequence rule notification service is supported. 
     *
     *  @return <code> true </code> if sequence rule notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleNotification() {
        return (getAdapteeManager().supportsSequenceRuleNotification());
    }


    /**
     *  Tests if a sequence rule bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule bank lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleBank() {
        return (getAdapteeManager().supportsSequenceRuleBank());
    }


    /**
     *  Tests if a sequence rule bank service is supported. 
     *
     *  @return <code> true </code> if sequence rule bank assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleBankAssignment() {
        return (getAdapteeManager().supportsSequenceRuleBankAssignment());
    }


    /**
     *  Tests if a sequence rule bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSmartBank() {
        return (getAdapteeManager().supportsSequenceRuleSmartBank());
    }


    /**
     *  Tests if looking up sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerLookup() {
        return (getAdapteeManager().supportsSequenceRuleEnablerLookup());
    }


    /**
     *  Tests if querying sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerQuery() {
        return (getAdapteeManager().supportsSequenceRuleEnablerQuery());
    }


    /**
     *  Tests if searching sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSearch() {
        return (getAdapteeManager().supportsSequenceRuleEnablerSearch());
    }


    /**
     *  Tests if a sequence rule enabler administrative service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerAdmin() {
        return (getAdapteeManager().supportsSequenceRuleEnablerAdmin());
    }


    /**
     *  Tests if a sequence rule enabler notification service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerNotification() {
        return (getAdapteeManager().supportsSequenceRuleEnablerNotification());
    }


    /**
     *  Tests if a sequence rule enabler bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler bank lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerBank() {
        return (getAdapteeManager().supportsSequenceRuleEnablerBank());
    }


    /**
     *  Tests if a sequence rule enabler bank service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler bank assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerBankAssignment() {
        return (getAdapteeManager().supportsSequenceRuleEnablerBankAssignment());
    }


    /**
     *  Tests if a sequence rule enabler bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSmartBank() {
        return (getAdapteeManager().supportsSequenceRuleEnablerSmartBank());
    }


    /**
     *  Tests if a sequence rule enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRuleLookup() {
        return (getAdapteeManager().supportsSequenceRuleEnablerRuleLookup());
    }


    /**
     *  Tests if a sequence rule enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if sequence rule enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRuleApplication() {
        return (getAdapteeManager().supportsSequenceRuleEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> AssessmentPart </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentPart </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentPartRecordTypes() {
        return (getAdapteeManager().getAssessmentPartRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentPart </code> record type is 
     *  supported. 
     *
     *  @param  assessmentPartRecordType a <code> Type </code> indicating an 
     *          <code> AssessmentPart </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assessmentPartRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentPartRecordType(org.osid.type.Type assessmentPartRecordType) {
        return (getAdapteeManager().supportsAssessmentPartRecordType(assessmentPartRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentPart </code> search record types. 
     *
     *  @return a list containing the supported <code> AssessmentPart </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentPartSearchRecordTypes() {
        return (getAdapteeManager().getAssessmentPartSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentPart </code> search record type is 
     *  supported. 
     *
     *  @param  assessmentPartSearchRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentPart </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentPartSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearchRecordType(org.osid.type.Type assessmentPartSearchRecordType) {
        return (getAdapteeManager().supportsAssessmentPartSearchRecordType(assessmentPartSearchRecordType));
    }


    /**
     *  Gets the supported <code> SequenceRule </code> record types. 
     *
     *  @return a list containing the supported <code> SequenceRule </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleRecordTypes() {
        return (getAdapteeManager().getSequenceRuleRecordTypes());
    }


    /**
     *  Tests if the given <code> SequenceRule </code> record type is 
     *  supported. 
     *
     *  @param  sequenceRuleRecordType a <code> Type </code> indicating a 
     *          <code> SequenceRule </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleRecordType(org.osid.type.Type sequenceRuleRecordType) {
        return (getAdapteeManager().supportsSequenceRuleRecordType(sequenceRuleRecordType));
    }


    /**
     *  Gets the supported <code> SequenceRule </code> search record types. 
     *
     *  @return a list containing the supported <code> SequenceRule </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleSearchRecordTypes() {
        return (getAdapteeManager().getSequenceRuleSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SequenceRule </code> search record type is 
     *  supported. 
     *
     *  @param  sequenceRuleSearchRecordType a <code> Type </code> indicating 
     *          a <code> SequenceRule </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSearchRecordType(org.osid.type.Type sequenceRuleSearchRecordType) {
        return (getAdapteeManager().supportsSequenceRuleSearchRecordType(sequenceRuleSearchRecordType));
    }


    /**
     *  Gets the supported <code> SequenceRuleEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SequenceRuleEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleEnablerRecordTypes() {
        return (getAdapteeManager().getSequenceRuleEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> SequenceRuleEnabler </code> record type is 
     *  supported. 
     *
     *  @param  sequenceRuleEnablerRecordType a <code> Type </code> indicating 
     *          a <code> SequenceRuleEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRecordType(org.osid.type.Type sequenceRuleEnablerRecordType) {
        return (getAdapteeManager().supportsSequenceRuleEnablerRecordType(sequenceRuleEnablerRecordType));
    }


    /**
     *  Gets the supported <code> SequenceRuleEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SequenceRuleEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleEnablerSearchRecordTypes() {
        return (getAdapteeManager().getSequenceRuleEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> SequenceRuleEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  sequenceRuleEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> SequenceRuleEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSearchRecordType(org.osid.type.Type sequenceRuleEnablerSearchRecordType) {
        return (getAdapteeManager().supportsSequenceRuleEnablerSearchRecordType(sequenceRuleEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service. 
     *
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartLookupSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service. 
     *
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartQuerySessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part search service. 
     *
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartSearchSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service. 
     *
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartAdminSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSession(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartNotificationSession(assessmentPartReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service for the given bank. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver 
     *          </code> or <code> bankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSessionForBank(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver, 
                                                                                                                       org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartNotificationSessionForBank(assessmentPartReceiver, bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment part/bank 
     *  mappings for assessment parts. 
     *
     *  @return an <code> AssessmentPartBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankSession getAssessmentPartBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartBankSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment part to bank. 
     *
     *  @return an <code> AssessmentPartBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankAssignmentSession getAssessmentPartBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage assessment part smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSmartBankSession getAssessmentPartSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentPartSmartBankSession(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service. 
     *
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleLookupSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service. 
     *
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleQuerySessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  search service. 
     *
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleSearchSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service. 
     *
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleAdminSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSession(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleNotificationSession(sequenceRuleReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service for the given bank. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver 
     *          </code> or <code> bankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver, 
                                                                                                                   org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleNotificationSessionForBank(sequenceRuleReceiver, bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule/bank 
     *  mappings for sequence rules. 
     *
     *  @return a <code> SequenceRuleBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankSession getSequenceRuleBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleBankSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule to bank. 
     *
     *  @return a <code> SequenceRuleBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankAssignmentSession getSequenceRuleBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSmartBankSession getSequenceRuleSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleSmartBankSession(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service. 
     *
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerLookupSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service. 
     *
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerQuerySessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler search service. 
     *
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enablers earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerSearchSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service. 
     *
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerAdminSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSession(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerNotificationSession(sequenceRuleEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service for the given bank. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver </code> or <code> bankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver, 
                                                                                                                                 org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerNotificationSessionForBank(sequenceRuleEnablerReceiver, bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule 
     *  enabler/bank mappings for sequence rule enablers. 
     *
     *  @return a <code> SequenceRuleEnablerBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankSession getSequenceRuleEnablerBankSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerBankSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule enablers to bank. 
     *
     *  @return a <code> SequenceRuleEnablerBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankAssignmentSession getSequenceRuleEnablerBankAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerBankAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule enabler 
     *  smart bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSmartBankSession getSequenceRuleEnablerSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerSmartBankSession(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerRuleLookupSessionForBank(bankId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service. 
     *
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequenceRuleEnablerRuleApplicationSessionForBank(bankId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
