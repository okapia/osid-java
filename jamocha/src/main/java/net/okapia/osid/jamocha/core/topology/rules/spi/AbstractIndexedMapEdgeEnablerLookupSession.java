//
// AbstractIndexedMapEdgeEnablerLookupSession.java
//
//    A simple framework for providing an EdgeEnabler lookup service
//    backed by a fixed collection of edge enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an EdgeEnabler lookup service backed by a
 *  fixed collection of edge enablers. The edge enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some edge enablers may be compatible
 *  with more types than are indicated through these edge enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>EdgeEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEdgeEnablerLookupSession
    extends AbstractMapEdgeEnablerLookupSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.topology.rules.EdgeEnabler> edgeEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.rules.EdgeEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.topology.rules.EdgeEnabler> edgeEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.rules.EdgeEnabler>());


    /**
     *  Makes an <code>EdgeEnabler</code> available in this session.
     *
     *  @param  edgeEnabler an edge enabler
     *  @throws org.osid.NullArgumentException <code>edgeEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEdgeEnabler(org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        super.putEdgeEnabler(edgeEnabler);

        this.edgeEnablersByGenus.put(edgeEnabler.getGenusType(), edgeEnabler);
        
        try (org.osid.type.TypeList types = edgeEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.edgeEnablersByRecord.put(types.getNextType(), edgeEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an edge enabler from this session.
     *
     *  @param edgeEnablerId the <code>Id</code> of the edge enabler
     *  @throws org.osid.NullArgumentException <code>edgeEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEdgeEnabler(org.osid.id.Id edgeEnablerId) {
        org.osid.topology.rules.EdgeEnabler edgeEnabler;
        try {
            edgeEnabler = getEdgeEnabler(edgeEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.edgeEnablersByGenus.remove(edgeEnabler.getGenusType());

        try (org.osid.type.TypeList types = edgeEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.edgeEnablersByRecord.remove(types.getNextType(), edgeEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEdgeEnabler(edgeEnablerId);
        return;
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the given
     *  edge enabler genus <code>Type</code> which does not include
     *  edge enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known edge enablers or an error results. Otherwise,
     *  the returned list may contain only those edge enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  edgeEnablerGenusType an edge enabler genus type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.rules.edgeenabler.ArrayEdgeEnablerList(this.edgeEnablersByGenus.get(edgeEnablerGenusType)));
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> containing the given
     *  edge enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known edge enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  edge enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  edgeEnablerRecordType an edge enabler record type 
     *  @return the returned <code>edgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByRecordType(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.rules.edgeenabler.ArrayEdgeEnablerList(this.edgeEnablersByRecord.get(edgeEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.edgeEnablersByGenus.clear();
        this.edgeEnablersByRecord.clear();

        super.close();

        return;
    }
}
