//
// AbstractQueryCompositionLookupSession.java
//
//    A CompositionQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CompositionQuerySession adapter.
 */

public abstract class AbstractAdapterCompositionQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.repository.CompositionQuerySession {

    private final org.osid.repository.CompositionQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCompositionQuerySession.
     *
     *  @param session the underlying composition query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCompositionQuerySession(org.osid.repository.CompositionQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeRepository</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeRepository Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the {@codeRepository</code> associated with this 
     *  session.
     *
     *  @return the {@codeRepository</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform {@codeComposition</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCompositions() {
        return (this.session.canSearchCompositions());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include compositions in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this repository only.
     */
    
    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods omit sequestered
     *  compositions.
     */

    @OSID @Override
    public void useSequesteredCompositionView() {
        this.session.useSequesteredCompositionView();
        return;
    }


    /**
     *  All compositions are returned including sequestered
     *  compositions.
     */

    @OSID @Override
    public void useUnsequesteredCompositionView() {
        this.session.useUnsequesteredCompositionView();
        return;
    }


    /**
     *  Gets a composition query. The returned query will not have an
     *  extension query.
     *
     *  @return the composition query 
     */
      
    @OSID @Override
    public org.osid.repository.CompositionQuery getCompositionQuery() {
        return (this.session.getCompositionQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  compositionQuery the composition query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code compositionQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code compositionQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByQuery(org.osid.repository.CompositionQuery compositionQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCompositionsByQuery(compositionQuery));
    }
}
