//
// MutableIndexedMapProxyAssessmentLookupSession
//
//    Implements an Assessment lookup service backed by a collection of
//    assessments indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an Assessment lookup service backed by a collection of
 *  assessments. The assessments are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some assessments may be compatible
 *  with more types than are indicated through these assessment
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of assessments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyAssessmentLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractIndexedMapAssessmentLookupSession
    implements org.osid.assessment.AssessmentLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentLookupSession} with
     *  no assessment.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.proxy.Proxy proxy) {
        setBank(bank);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentLookupSession} with
     *  a single assessment.
     *
     *  @param bank the bank
     *  @param  assessment an assessment
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessment}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.assessment.Assessment assessment, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessment(assessment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentLookupSession} using
     *  an array of assessments.
     *
     *  @param bank the bank
     *  @param  assessments an array of assessments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.assessment.Assessment[] assessments, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessments(assessments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentLookupSession} using
     *  a collection of assessments.
     *
     *  @param bank the bank
     *  @param  assessments a collection of assessments
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessments}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentLookupSession(org.osid.assessment.Bank bank,
                                                       java.util.Collection<? extends org.osid.assessment.Assessment> assessments,
                                                       org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessments(assessments);
        return;
    }

    
    /**
     *  Makes an {@code Assessment} available in this session.
     *
     *  @param  assessment an assessment
     *  @throws org.osid.NullArgumentException {@code assessment{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessment(org.osid.assessment.Assessment assessment) {
        super.putAssessment(assessment);
        return;
    }


    /**
     *  Makes an array of assessments available in this session.
     *
     *  @param  assessments an array of assessments
     *  @throws org.osid.NullArgumentException {@code assessments{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessments(org.osid.assessment.Assessment[] assessments) {
        super.putAssessments(assessments);
        return;
    }


    /**
     *  Makes collection of assessments available in this session.
     *
     *  @param  assessments a collection of assessments
     *  @throws org.osid.NullArgumentException {@code assessment{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessments(java.util.Collection<? extends org.osid.assessment.Assessment> assessments) {
        super.putAssessments(assessments);
        return;
    }


    /**
     *  Removes an Assessment from this session.
     *
     *  @param assessmentId the {@code Id} of the assessment
     *  @throws org.osid.NullArgumentException {@code assessmentId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessment(org.osid.id.Id assessmentId) {
        super.removeAssessment(assessmentId);
        return;
    }    
}
