//
// MutableMapProxyAuthorizationLookupSession
//
//    Implements an Authorization lookup service backed by a collection of
//    authorizations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements an Authorization lookup service backed by a collection of
 *  authorizations. The authorizations are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of authorizations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAuthorizationLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAuthorizationLookupSession}
     *  with no authorizations.
     *
     *  @param vault the vault
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.proxy.Proxy proxy) {
        setVault(vault);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAuthorizationLookupSession} with a
     *  single authorization.
     *
     *  @param vault the vault
     *  @param authorization an authorization
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorization}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                org.osid.authorization.Authorization authorization, org.osid.proxy.Proxy proxy) {
        this(vault, proxy);
        putAuthorization(authorization);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAuthorizationLookupSession} using an
     *  array of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations an array of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                org.osid.authorization.Authorization[] authorizations, org.osid.proxy.Proxy proxy) {
        this(vault, proxy);
        putAuthorizations(authorizations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAuthorizationLookupSession} using a
     *  collection of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations a collection of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                java.util.Collection<? extends org.osid.authorization.Authorization> authorizations,
                                                org.osid.proxy.Proxy proxy) {
   
        this(vault, proxy);
        setSessionProxy(proxy);
        putAuthorizations(authorizations);
        return;
    }

    
    /**
     *  Makes a {@code Authorization} available in this session.
     *
     *  @param authorization an authorization
     *  @throws org.osid.NullArgumentException {@code authorization{@code 
     *          is {@code null}
     */

    @Override
    public void putAuthorization(org.osid.authorization.Authorization authorization) {
        super.putAuthorization(authorization);
        return;
    }


    /**
     *  Makes an array of authorizations available in this session.
     *
     *  @param authorizations an array of authorizations
     *  @throws org.osid.NullArgumentException {@code authorizations{@code 
     *          is {@code null}
     */

    @Override
    public void putAuthorizations(org.osid.authorization.Authorization[] authorizations) {
        super.putAuthorizations(authorizations);
        return;
    }


    /**
     *  Makes collection of authorizations available in this session.
     *
     *  @param authorizations
     *  @throws org.osid.NullArgumentException {@code authorization{@code 
     *          is {@code null}
     */

    @Override
    public void putAuthorizations(java.util.Collection<? extends org.osid.authorization.Authorization> authorizations) {
        super.putAuthorizations(authorizations);
        return;
    }


    /**
     *  Removes a Authorization from this session.
     *
     *  @param authorizationId the {@code Id} of the authorization
     *  @throws org.osid.NullArgumentException {@code authorizationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAuthorization(org.osid.id.Id authorizationId) {
        super.removeAuthorization(authorizationId);
        return;
    }    
}
