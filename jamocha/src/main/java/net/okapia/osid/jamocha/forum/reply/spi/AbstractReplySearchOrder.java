//
// AbstractReplySearchOdrer.java
//
//     Defines a ReplySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ReplySearchOrder}.
 */

public abstract class AbstractReplySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.forum.ReplySearchOrder {

    private final java.util.Collection<org.osid.forum.records.ReplySearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidContainableSearchOrder order = new OsidContainableSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  sequestered flag.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */
    
    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.order.orderBySequestered(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the timestamp. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the poster. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPoster(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a poster resource search order interface is available. 
     *
     *  @return <code> true </code> if a resource search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order interface. 
     *
     *  @return the resource search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPosterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getPosterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPosterSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a posting agent search order interface is available. 
     *
     *  @return <code> true </code> if a agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the posting agent search order interface. 
     *
     *  @return the agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getPostingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the subject. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  replyRecordType a reply record type 
     *  @return {@code true} if the replyRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code replyRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type replyRecordType) {
        for (org.osid.forum.records.ReplySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  replyRecordType the reply record type 
     *  @return the reply search order record
     *  @throws org.osid.NullArgumentException
     *          {@code replyRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(replyRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.forum.records.ReplySearchOrderRecord getReplySearchOrderRecord(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ReplySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(replyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replyRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this reply. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param replyRecord the reply search odrer record
     *  @param replyRecordType reply record type
     *  @throws org.osid.NullArgumentException
     *          {@code replyRecord} or
     *          {@code replyRecordTypereply} is
     *          {@code null}
     */
            
    protected void addReplyRecord(org.osid.forum.records.ReplySearchOrderRecord replySearchOrderRecord, 
                                     org.osid.type.Type replyRecordType) {

        addRecordType(replyRecordType);
        this.records.add(replySearchOrderRecord);
        
        return;
    }


    protected class OsidContainableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableSearchOrder
        implements org.osid.OsidContainableSearchOrder {
    }
}
