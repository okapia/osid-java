//
// AbstractAssemblyRelationshipQuery.java
//
//     A RelationshipQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelationshipQuery that stores terms.
 */

public abstract class AbstractAssemblyRelationshipQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.relationship.RelationshipQuery,
               org.osid.relationship.RelationshipQueryInspector,
               org.osid.relationship.RelationshipSearchOrder {

    private final java.util.Collection<org.osid.relationship.records.RelationshipQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.records.RelationshipQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.relationship.records.RelationshipSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRelationshipQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRelationshipQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a relationship peer. 
     *
     *  @param  peer peer <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peer </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id peer, boolean match) {
        getAssembler().addIdTerm(getSourceIdColumn(), peer, match);
        return;
    }


    /**
     *  Clears the peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        getAssembler().clearTerms(getSourceIdColumn());
        return;
    }


    /**
     *  Gets the peer <code> Id </code> terms. 
     *
     *  @return the peer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (getAssembler().getIdTerms(getSourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the source peer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceIdColumn(), style);
        return;
    }


    /**
     *  Gets the SourceId column name.
     *
     * @return the column name
     */

    protected String getSourceIdColumn() {
        return ("source_id");
    }


    /**
     *  Matches the other relationship peer. 
     *
     *  @param  peer peer <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peer </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDestinationId(org.osid.id.Id peer, boolean match) {
        getAssembler().addIdTerm(getDestinationIdColumn(), peer, match);
        return;
    }


    /**
     *  Clears the other peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationIdTerms() {
        getAssembler().clearTerms(getDestinationIdColumn());
        return;
    }


    /**
     *  Gets the other peer <code> Id </code> terms. 
     *
     *  @return the other peer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDestinationIdTerms() {
        return (getAssembler().getIdTerms(getDestinationIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the destination 
     *  peer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDestination(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDestinationIdColumn(), style);
        return;
    }


    /**
     *  Gets the DestinationId column name.
     *
     * @return the column name
     */

    protected String getDestinationIdColumn() {
        return ("destination_id");
    }


    /**
     *  Matches circular relationships to the same peer. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSamePeerId(boolean match) {
        getAssembler().addBooleanTerm(getSamePeerIdColumn(), match);
        return;
    }


    /**
     *  Clears the same peer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSamePeerIdTerms() {
        getAssembler().clearTerms(getSamePeerIdColumn());
        return;
    }


    /**
     *  Gets the same peer terms. 
     *
     *  @return the same peer terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSamePeerIdTerms() {
        return (getAssembler().getBooleanTerms(getSamePeerIdColumn()));
    }


    /**
     *  Gets the SamePeerId column name.
     *
     * @return the column name
     */

    protected String getSamePeerIdColumn() {
        return ("same_peer_id");
    }


    /**
     *  Sets the family <code> Id </code> for this query. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> familyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFamilyId(org.osid.id.Id familyId, boolean match) {
        getAssembler().addIdTerm(getFamilyIdColumn(), familyId, match);
        return;
    }


    /**
     *  Clears the family <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFamilyIdTerms() {
        getAssembler().clearTerms(getFamilyIdColumn());
        return;
    }


    /**
     *  Gets the family <code> Id </code> terms. 
     *
     *  @return the family <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFamilyIdTerms() {
        return (getAssembler().getIdTerms(getFamilyIdColumn()));
    }


    /**
     *  Gets the FamilyId column name.
     *
     * @return the column name
     */

    protected String getFamilyIdColumn() {
        return ("family_id");
    }


    /**
     *  Tests if a <code> FamilyQuery </code> is available. 
     *
     *  @return <code> true </code> if a family query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a family. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the family query 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuery getFamilyQuery() {
        throw new org.osid.UnimplementedException("supportsFamilyQuery() is false");
    }


    /**
     *  Clears the family terms. 
     */

    @OSID @Override
    public void clearFamilyTerms() {
        getAssembler().clearTerms(getFamilyColumn());
        return;
    }


    /**
     *  Gets the family terms. 
     *
     *  @return the family terms 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQueryInspector[] getFamilyTerms() {
        return (new org.osid.relationship.FamilyQueryInspector[0]);
    }


    /**
     *  Gets the Family column name.
     *
     * @return the column name
     */

    protected String getFamilyColumn() {
        return ("family");
    }


    /**
     *  Tests if this relationship supports the given record
     *  <code>Type</code>.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return <code>true</code> if the relationshipRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type relationshipRecordType) {
        for (org.osid.relationship.records.RelationshipQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  relationshipRecordType the relationship record type 
     *  @return the relationship query record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipQueryRecord getRelationshipQueryRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  relationshipRecordType the relationship record type 
     *  @return the relationship query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipQueryInspectorRecord getRelationshipQueryInspectorRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param relationshipRecordType the relationship record type
     *  @return the relationship search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipSearchOrderRecord getRelationshipSearchOrderRecord(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.relationship.records.RelationshipSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(relationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this relationship. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param relationshipQueryRecord the relationship query record
     *  @param relationshipQueryInspectorRecord the relationship query inspector
     *         record
     *  @param relationshipSearchOrderRecord the relationship search order record
     *  @param relationshipRecordType relationship record type
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipQueryRecord</code>,
     *          <code>relationshipQueryInspectorRecord</code>,
     *          <code>relationshipSearchOrderRecord</code> or
     *          <code>relationshipRecordTyperelationship</code> is
     *          <code>null</code>
     */
            
    protected void addRelationshipRecords(org.osid.relationship.records.RelationshipQueryRecord relationshipQueryRecord, 
                                      org.osid.relationship.records.RelationshipQueryInspectorRecord relationshipQueryInspectorRecord, 
                                      org.osid.relationship.records.RelationshipSearchOrderRecord relationshipSearchOrderRecord, 
                                      org.osid.type.Type relationshipRecordType) {

        addRecordType(relationshipRecordType);

        nullarg(relationshipQueryRecord, "relationship query record");
        nullarg(relationshipQueryInspectorRecord, "relationship query inspector record");
        nullarg(relationshipSearchOrderRecord, "relationship search odrer record");

        this.queryRecords.add(relationshipQueryRecord);
        this.queryInspectorRecords.add(relationshipQueryInspectorRecord);
        this.searchOrderRecords.add(relationshipSearchOrderRecord);
        
        return;
    }
}
