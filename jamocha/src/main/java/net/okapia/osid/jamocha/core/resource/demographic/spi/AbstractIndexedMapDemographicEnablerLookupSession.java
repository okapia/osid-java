//
// AbstractIndexedMapDemographicEnablerLookupSession.java
//
//    A simple framework for providing a DemographicEnabler lookup service
//    backed by a fixed collection of demographic enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a DemographicEnabler lookup service backed by a
 *  fixed collection of demographic enablers. The demographic enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some demographic enablers may be compatible
 *  with more types than are indicated through these demographic enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>DemographicEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDemographicEnablerLookupSession
    extends AbstractMapDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resource.demographic.DemographicEnabler> demographicEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.demographic.DemographicEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.resource.demographic.DemographicEnabler> demographicEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.demographic.DemographicEnabler>());


    /**
     *  Makes a <code>DemographicEnabler</code> available in this session.
     *
     *  @param  demographicEnabler a demographic enabler
     *  @throws org.osid.NullArgumentException <code>demographicEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDemographicEnabler(org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        super.putDemographicEnabler(demographicEnabler);

        this.demographicEnablersByGenus.put(demographicEnabler.getGenusType(), demographicEnabler);
        
        try (org.osid.type.TypeList types = demographicEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.demographicEnablersByRecord.put(types.getNextType(), demographicEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a demographic enabler from this session.
     *
     *  @param demographicEnablerId the <code>Id</code> of the demographic enabler
     *  @throws org.osid.NullArgumentException <code>demographicEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDemographicEnabler(org.osid.id.Id demographicEnablerId) {
        org.osid.resource.demographic.DemographicEnabler demographicEnabler;
        try {
            demographicEnabler = getDemographicEnabler(demographicEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.demographicEnablersByGenus.remove(demographicEnabler.getGenusType());

        try (org.osid.type.TypeList types = demographicEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.demographicEnablersByRecord.remove(types.getNextType(), demographicEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDemographicEnabler(demographicEnablerId);
        return;
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to the given
     *  demographic enabler genus <code>Type</code> which does not include
     *  demographic enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known demographic enablers or an error results. Otherwise,
     *  the returned list may contain only those demographic enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  demographicEnablerGenusType a demographic enabler genus type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographicenabler.ArrayDemographicEnablerList(this.demographicEnablersByGenus.get(demographicEnablerGenusType)));
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> containing the given
     *  demographic enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known demographic enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  demographic enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  demographicEnablerRecordType a demographic enabler record type 
     *  @return the returned <code>demographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByRecordType(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.demographic.demographicenabler.ArrayDemographicEnablerList(this.demographicEnablersByRecord.get(demographicEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.demographicEnablersByGenus.clear();
        this.demographicEnablersByRecord.clear();

        super.close();

        return;
    }
}
