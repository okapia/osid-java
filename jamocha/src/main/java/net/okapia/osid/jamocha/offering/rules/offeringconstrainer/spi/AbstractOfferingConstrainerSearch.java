//
// AbstractOfferingConstrainerSearch.java
//
//     A template for making an OfferingConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing offering constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOfferingConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.rules.OfferingConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.rules.OfferingConstrainerSearchOrder offeringConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offering constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  offeringConstrainerIds list of offering constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOfferingConstrainers(org.osid.id.IdList offeringConstrainerIds) {
        while (offeringConstrainerIds.hasNext()) {
            try {
                this.ids.add(offeringConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOfferingConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of offering constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOfferingConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  offeringConstrainerSearchOrder offering constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>offeringConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOfferingConstrainerResults(org.osid.offering.rules.OfferingConstrainerSearchOrder offeringConstrainerSearchOrder) {
	this.offeringConstrainerSearchOrder = offeringConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.rules.OfferingConstrainerSearchOrder getOfferingConstrainerSearchOrder() {
	return (this.offeringConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given offering constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offering constrainer implementing the requested record.
     *
     *  @param offeringConstrainerSearchRecordType an offering constrainer search record
     *         type
     *  @return the offering constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerSearchRecord getOfferingConstrainerSearchRecord(org.osid.type.Type offeringConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.rules.records.OfferingConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering constrainer search. 
     *
     *  @param offeringConstrainerSearchRecord offering constrainer search record
     *  @param offeringConstrainerSearchRecordType offeringConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingConstrainerSearchRecord(org.osid.offering.rules.records.OfferingConstrainerSearchRecord offeringConstrainerSearchRecord, 
                                           org.osid.type.Type offeringConstrainerSearchRecordType) {

        addRecordType(offeringConstrainerSearchRecordType);
        this.records.add(offeringConstrainerSearchRecord);        
        return;
    }
}
