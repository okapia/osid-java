//
// AbstractQueryInputLookupSession.java
//
//    An InputQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InputQuerySession adapter.
 */

public abstract class AbstractAdapterInputQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.InputQuerySession {

    private final org.osid.control.InputQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterInputQuerySession.
     *
     *  @param session the underlying input query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterInputQuerySession(org.osid.control.InputQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeSystem</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeSystem Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@codeSystem</code> associated with this 
     *  session.
     *
     *  @return the {@codeSystem</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@codeInput</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchInputs() {
        return (this.session.canSearchInputs());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inputs in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this system only.
     */
    
    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    
      
    /**
     *  Gets an input query. The returned query will not have an
     *  extension query.
     *
     *  @return the input query 
     */
      
    @OSID @Override
    public org.osid.control.InputQuery getInputQuery() {
        return (this.session.getInputQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  inputQuery the input query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code inputQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code inputQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.control.InputList getInputsByQuery(org.osid.control.InputQuery inputQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getInputsByQuery(inputQuery));
    }
}
