//
// PostEntryMiter.java
//
//     Defines a PostEntry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.postentry;


/**
 *  Defines a <code>PostEntry</code> miter for use with the builders.
 */

public interface PostEntryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.financials.posting.PostEntry {


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    public void setPost(org.osid.financials.posting.Post post);


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    public void setAccount(org.osid.financials.Account account);


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void setActivity(org.osid.financials.Activity activity);


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void setAmount(org.osid.financials.Currency amount);


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this entry amount is a
     *         debit, <code> false </code> if it is a credit
     */

    public void setDebit(boolean debit);


    /**
     *  Adds a PostEntry record.
     *
     *  @param record a postEntry record
     *  @param recordType the type of postEntry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPostEntryRecord(org.osid.financials.posting.records.PostEntryRecord record, org.osid.type.Type recordType);
}       


