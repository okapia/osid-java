//
// InvariantMapProxyCompetencyLookupSession
//
//    Implements a Competency lookup service backed by a fixed
//    collection of competencies. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Competency lookup service backed by a fixed
 *  collection of competencies. The competencies are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCompetencyLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapCompetencyLookupSession
    implements org.osid.resourcing.CompetencyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCompetencyLookupSession} with no
     *  competencies.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCompetencyLookupSession} with a single
     *  competency.
     *
     *  @param foundry the foundry
     *  @param competency a single competency
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code competency} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Competency competency, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putCompetency(competency);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCompetencyLookupSession} using
     *  an array of competencies.
     *
     *  @param foundry the foundry
     *  @param competencies an array of competencies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code competencies} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.Competency[] competencies, org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putCompetencies(competencies);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCompetencyLookupSession} using a
     *  collection of competencies.
     *
     *  @param foundry the foundry
     *  @param competencies a collection of competencies
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code competencies} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCompetencyLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.Competency> competencies,
                                                  org.osid.proxy.Proxy proxy) {

        this(foundry, proxy);
        putCompetencies(competencies);
        return;
    }
}
