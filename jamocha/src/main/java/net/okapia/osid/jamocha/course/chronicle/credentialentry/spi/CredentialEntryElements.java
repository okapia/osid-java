//
// CredentialEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CredentialEntryElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CredentialEntryElement Id.
     *
     *  @return the credential entry element Id
     */

    public static org.osid.id.Id getCredentialEntryEntityId() {
        return (makeEntityId("osid.course.chronicle.CredentialEntry"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.chronicle.credentialentry.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.chronicle.credentialentry.Student"));
    }


    /**
     *  Gets the CredentialId element Id.
     *
     *  @return the CredentialId element Id
     */

    public static org.osid.id.Id getCredentialId() {
        return (makeElementId("osid.course.chronicle.credentialentry.CredentialId"));
    }


    /**
     *  Gets the Credential element Id.
     *
     *  @return the Credential element Id
     */

    public static org.osid.id.Id getCredential() {
        return (makeElementId("osid.course.chronicle.credentialentry.Credential"));
    }


    /**
     *  Gets the DateAwarded element Id.
     *
     *  @return the DateAwarded element Id
     */

    public static org.osid.id.Id getDateAwarded() {
        return (makeElementId("osid.course.chronicle.credentialentry.DateAwarded"));
    }


    /**
     *  Gets the ProgramId element Id.
     *
     *  @return the ProgramId element Id
     */

    public static org.osid.id.Id getProgramId() {
        return (makeElementId("osid.course.chronicle.credentialentry.ProgramId"));
    }


    /**
     *  Gets the Program element Id.
     *
     *  @return the Program element Id
     */

    public static org.osid.id.Id getProgram() {
        return (makeElementId("osid.course.chronicle.credentialentry.Program"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.chronicle.credentialentry.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.chronicle.credentialentry.CourseCatalog"));
    }
}
