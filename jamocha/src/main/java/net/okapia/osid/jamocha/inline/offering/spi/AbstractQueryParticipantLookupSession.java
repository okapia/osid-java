//
// AbstractQueryParticipantLookupSession.java
//
//    An inline adapter that maps a ParticipantLookupSession to
//    a ParticipantQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ParticipantLookupSession to
 *  a ParticipantQuerySession.
 */

public abstract class AbstractQueryParticipantLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {

      private boolean effectiveonly = false;

    private final org.osid.offering.ParticipantQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryParticipantLookupSession.
     *
     *  @param querySession the underlying participant query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryParticipantLookupSession(org.osid.offering.ParticipantQuerySession querySession) {
        nullarg(querySession, "participant query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>Participant</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParticipants() {
        return (this.session.canSearchParticipants());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include participants in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only participants whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveParticipantView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All participants of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveParticipantView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Participant</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Participant</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Participant</code> and
     *  retained for compatibility.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantId <code>Id</code> of the
     *          <code>Participant</code>
     *  @return the participant
     *  @throws org.osid.NotFoundException <code>participantId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>participantId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant(org.osid.id.Id participantId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchId(participantId, true);
        org.osid.offering.ParticipantList participants = this.session.getParticipantsByQuery(query);
        if (participants.hasNext()) {
            return (participants.getNextParticipant());
        } 
        
        throw new org.osid.NotFoundException(participantId + " not found");
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  participants specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Participants</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, participants are returned that are currently effective.
     *  In any effective mode, effective participants and those currently expired
     *  are returned.
     *
     *  @param  participantIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>participantIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByIds(org.osid.id.IdList participantIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();

        try (org.osid.id.IdList ids = participantIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> which does not include
     *  participants of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently effective.
     *  In any effective mode, effective participants and those currently expired
     *  are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchGenusType(participantGenusType, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a <code>ParticipantList</code> corresponding to the given
     *  participant genus <code>Type</code> and include any additional
     *  participants with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantGenusType a participant genus type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByParentGenusType(org.osid.type.Type participantGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchParentGenusType(participantGenusType, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a <code>ParticipantList</code> containing the given
     *  participant record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  participantRecordType a participant record type 
     *  @return the returned <code>Participant</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>participantRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsByRecordType(org.osid.type.Type participantRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchRecordType(participantRecordType, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a <code>ParticipantList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *  
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Participant</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getParticipantsByQuery(query));
    }
        

    /**
     *  Gets a list of participants corresponding to a offering
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForOffering(org.osid.id.Id offeringId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchOfferingId(offeringId, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a list of participants corresponding to a offering
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingOnDate(org.osid.id.Id offeringId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchOfferingId(offeringId, true);
        query.matchDate(from, to, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.offering.ParticipantList getParticipantsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a list of participants corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForResourceOnDate(org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a list of participants corresponding to offering and resource
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResource(org.osid.id.Id offeringId,
                                                                                   org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchOfferingId(offeringId, true);
        query.matchResourceId(resourceId, true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets a list of participants corresponding to offering and resource
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible
     *  through this session.
     *
     *  In effective mode, participants are returned that are
     *  currently effective.  In any effective mode, effective
     *  participants and those currently expired are returned.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ParticipantList</code>
     *  @throws org.osid.NullArgumentException <code>offeringId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipantsForOfferingAndResourceOnDate(org.osid.id.Id offeringId,
                                                                                         org.osid.id.Id resourceId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchOfferingId(offeringId, true);
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getParticipantsByQuery(query));
    }

    
    /**
     *  Gets all <code>Participants</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  participants or an error results. Otherwise, the returned list
     *  may contain only those participants that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, participants are returned that are currently
     *  effective.  In any effective mode, effective participants and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Participants</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.ParticipantList getParticipants()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.ParticipantQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getParticipantsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.ParticipantQuery getQuery() {
        org.osid.offering.ParticipantQuery query = this.session.getParticipantQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
