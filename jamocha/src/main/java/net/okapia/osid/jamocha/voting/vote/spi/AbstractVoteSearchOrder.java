//
// AbstractVoteSearchOdrer.java
//
//     Defines a VoteSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code VoteSearchOrder}.
 */

public abstract class AbstractVoteSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.voting.VoteSearchOrder {

    private final java.util.Collection<org.osid.voting.records.VoteSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the candidate. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCandidate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> CandidateSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a candidate search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order interface for a candidate. 
     *
     *  @return the candidate search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateSearchOrder getCandidateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCandidateSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the voter. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVoter(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVoterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getVoterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsVoterSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the voteing agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVotingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AgentSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getVotingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsVotingAgentSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the number of votes. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVotes(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  voteRecordType a vote record type 
     *  @return {@code true} if the voteRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code voteRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type voteRecordType) {
        for (org.osid.voting.records.VoteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(voteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  voteRecordType the vote record type 
     *  @return the vote search order record
     *  @throws org.osid.NullArgumentException
     *          {@code voteRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(voteRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.voting.records.VoteSearchOrderRecord getVoteSearchOrderRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this vote. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param voteRecord the vote search odrer record
     *  @param voteRecordType vote record type
     *  @throws org.osid.NullArgumentException
     *          {@code voteRecord} or
     *          {@code voteRecordTypevote} is
     *          {@code null}
     */
            
    protected void addVoteRecord(org.osid.voting.records.VoteSearchOrderRecord voteSearchOrderRecord, 
                                     org.osid.type.Type voteRecordType) {

        addRecordType(voteRecordType);
        this.records.add(voteSearchOrderRecord);
        
        return;
    }
}
