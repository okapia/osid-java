//
// AbstractProficiency.java
//
//     Defines a Proficiency.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines a <code>Proficiency</code>.
 */

public abstract class AbstractProficiency
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.learning.Proficiency {

    private org.osid.resource.Resource resource;
    private org.osid.learning.Objective objective;
    private java.math.BigDecimal completion;
    private org.osid.grading.Grade level;

    private final java.util.Collection<org.osid.learning.records.ProficiencyRecord> records = new java.util.LinkedHashSet<>();

    
    /**
     *  Gets the resource <code> Id </code> to whom this proficiency applies. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource to whom this proficiency applies. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the objective <code> Id </code> to whom this proficiency applies. 
     *
     *  @return the objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveId() {
        return (this.objective.getId());
    }


    /**
     *  Gets the objective to whom this proficiency applies. 
     *
     *  @return the objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective()
        throws org.osid.OperationFailedException {

        return (this.objective);
    }


    /**
     *  Sets the objective.
     *
     *  @param objective an objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void setObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "objective");
        this.objective = objective;
        return;
    }


    /**
     *  Gets the completion of this objective as a percentage 0-100. 
     *
     *  @return the completion 
     */

    @OSID @Override
    public java.math.BigDecimal getCompletion() {
        return (this.completion);
    }


    /**
     *  Sets the completion.
     *
     *  @param completion a completion
     *  @throws org.osid.InvalidArgumentException
     *          <code>completion</code> is out of range
     *  @throws org.osid.NullArgumentException
     *          <code>completion</code> is <code>null</code>
     */

    protected void setCompletion(java.math.BigDecimal completion) {
        percentarg(completion, "completion");
        this.completion = completion;
        return;
    }


    /**
     *  Tests if a proficiency level is available. 
     *
     *  @return <code> true </code> if a level is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLevel() {
        return (this.level != null);
    }


    /**
     *  Gets the proficiency level expressed as a grade. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLevel() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        if (!hasLevel()) {
            throw new org.osid.IllegalStateException("hasLevel() is false");
        }

        return (this.level.getId());
    }


    /**
     *  Gets the proficiency level expressed as a grade. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasLevel() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        if (!hasLevel()) {
            throw new org.osid.IllegalStateException("hasLevel() is false");
        }

        return (this.level);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    protected void setLevel(org.osid.grading.Grade level) {
        nullarg(level, "level");
        this.level = level;
        return;
    }


    /**
     *  Tests if this proficiency supports the given record
     *  <code>Type</code>.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return <code>true</code> if the proficiencyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type proficiencyRecordType) {
        for (org.osid.learning.records.ProficiencyRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Proficiency</code> record <code>Type</code>.
     *
     *  @param  proficiencyRecordType the proficiency record type 
     *  @return the proficiency record 
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proficiencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyRecord getProficiencyRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ProficiencyRecord record : this.records) {
            if (record.implementsRecordType(proficiencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proficiencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proficiency. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param proficiencyRecord the proficiency record
     *  @param proficiencyRecordType proficiency record type
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecord</code> or
     *          <code>proficiencyRecordTypeproficiency</code> is
     *          <code>null</code>
     */
            
    protected void addProficiencyRecord(org.osid.learning.records.ProficiencyRecord proficiencyRecord, 
                                        org.osid.type.Type proficiencyRecordType) {

        nullarg(proficiencyRecord, "proficiency record");
        addRecordType(proficiencyRecordType);
        this.records.add(proficiencyRecord);
        
        return;
    }
}
