//
// AbstractAuthorizationBatchManager.java
//
//     An adapter for a AuthorizationBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthorizationBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthorizationBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.authorization.batch.AuthorizationBatchManager>
    implements org.osid.authorization.batch.AuthorizationBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthorizationBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthorizationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of authorizations is available. 
     *
     *  @return <code> true </code> if an authorization bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationBatchAdmin() {
        return (getAdapteeManager().supportsAuthorizationBatchAdmin());
    }


    /**
     *  Tests if bulk administration of functions is available. 
     *
     *  @return <code> true </code> if a function bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionBatchAdmin() {
        return (getAdapteeManager().supportsFunctionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of qualifiers is available. 
     *
     *  @return <code> true </code> if a qualifier bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierBatchAdmin() {
        return (getAdapteeManager().supportsQualifierBatchAdmin());
    }


    /**
     *  Tests if bulk administration of vaults is available. 
     *
     *  @return <code> true </code> if a vault bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultBatchAdmin() {
        return (getAdapteeManager().supportsVaultBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service. 
     *
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationBatchAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service. 
     *
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionBatchAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service. 
     *
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierBatchAdminSessionForVault(vaultId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk vault 
     *  administration service. 
     *
     *  @return a <code> VaultBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.VaultBatchAdminSession getVaultBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
