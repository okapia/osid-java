//
// AbstractMapCommitmentLookupSession
//
//    A simple framework for providing a Commitment lookup service
//    backed by a fixed collection of commitments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Commitment lookup service backed by a
 *  fixed collection of commitments. The commitments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Commitments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCommitmentLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCommitmentLookupSession
    implements org.osid.calendaring.CommitmentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.calendaring.Commitment> commitments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.calendaring.Commitment>());


    /**
     *  Makes a <code>Commitment</code> available in this session.
     *
     *  @param  commitment a commitment
     *  @throws org.osid.NullArgumentException <code>commitment<code>
     *          is <code>null</code>
     */

    protected void putCommitment(org.osid.calendaring.Commitment commitment) {
        this.commitments.put(commitment.getId(), commitment);
        return;
    }


    /**
     *  Makes an array of commitments available in this session.
     *
     *  @param  commitments an array of commitments
     *  @throws org.osid.NullArgumentException <code>commitments<code>
     *          is <code>null</code>
     */

    protected void putCommitments(org.osid.calendaring.Commitment[] commitments) {
        putCommitments(java.util.Arrays.asList(commitments));
        return;
    }


    /**
     *  Makes a collection of commitments available in this session.
     *
     *  @param  commitments a collection of commitments
     *  @throws org.osid.NullArgumentException <code>commitments<code>
     *          is <code>null</code>
     */

    protected void putCommitments(java.util.Collection<? extends org.osid.calendaring.Commitment> commitments) {
        for (org.osid.calendaring.Commitment commitment : commitments) {
            this.commitments.put(commitment.getId(), commitment);
        }

        return;
    }


    /**
     *  Removes a Commitment from this session.
     *
     *  @param  commitmentId the <code>Id</code> of the commitment
     *  @throws org.osid.NullArgumentException <code>commitmentId<code> is
     *          <code>null</code>
     */

    protected void removeCommitment(org.osid.id.Id commitmentId) {
        this.commitments.remove(commitmentId);
        return;
    }


    /**
     *  Gets the <code>Commitment</code> specified by its <code>Id</code>.
     *
     *  @param  commitmentId <code>Id</code> of the <code>Commitment</code>
     *  @return the commitment
     *  @throws org.osid.NotFoundException <code>commitmentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>commitmentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Commitment getCommitment(org.osid.id.Id commitmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.calendaring.Commitment commitment = this.commitments.get(commitmentId);
        if (commitment == null) {
            throw new org.osid.NotFoundException("commitment not found: " + commitmentId);
        }

        return (commitment);
    }


    /**
     *  Gets all <code>Commitments</code>. In plenary mode, the returned
     *  list contains all known commitments or an error
     *  results. Otherwise, the returned list may contain only those
     *  commitments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Commitments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentList getCommitments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.commitment.ArrayCommitmentList(this.commitments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commitments.clear();
        super.close();
        return;
    }
}
