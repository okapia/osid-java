//
// AbstractFederatingEdgeLookupSession.java
//
//     An abstract federating adapter for an EdgeLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.topology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EdgeLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEdgeLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.topology.EdgeLookupSession>
    implements org.osid.topology.EdgeLookupSession {

    private boolean parallel = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Constructs a new <code>AbstractFederatingEdgeLookupSession</code>.
     */

    protected AbstractFederatingEdgeLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.topology.EdgeLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Graph/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the <code>Graph</code>.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException <code>graph</code>
     *          is <code>null</code>
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can perform <code>Edge</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEdges() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            if (session.canLookupEdges()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Edge</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEdgeView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.useComparativeEdgeView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Edge</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEdgeView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.usePlenaryEdgeView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edges in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.useFederatedGraphView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.useIsolatedGraphView();
        }

        return;
    }


    /**
     *  Only edges whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveEdgeView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.useEffectiveEdgeView();
        }

        return;
    }


    /**
     *  All edges of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveEdgeView() {
        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            session.useAnyEffectiveEdgeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Edge</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Edge</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Edge</code> and
     *  retained for compatibility.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @param  edgeId <code>Id</code> of the
     *          <code>Edge</code>
     *  @return the edge
     *  @throws org.osid.NotFoundException <code>edgeId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>edgeId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Edge getEdge(org.osid.id.Id edgeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            try {
                return (session.getEdge(edgeId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(edgeId + " not found");
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  edges specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Edges</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>edgeIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByIds(org.osid.id.IdList edgeIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.topology.edge.MutableEdgeList ret = new net.okapia.osid.jamocha.topology.edge.MutableEdgeList();

        try (org.osid.id.IdList ids = edgeIds) {
            while (ids.hasNext()) {
                ret.addEdge(getEdge(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  edge genus <code>Type</code> which does not include
     *  edges of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusType(edgeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeList</code> corresponding to the given
     *  edge genus <code>Type</code> and include any additional
     *  edges with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeGenusType an edge genus type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByParentGenusType(org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByParentGenusType(edgeGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeList</code> containing the given
     *  edge record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByRecordType(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByRecordType(edgeRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EdgeList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *  
     *  In active mode, edges are returned that are currently
     *  active. In any status mode, active and inactive edges
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Edge</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.EdgeList getEdgesOnDate(org.osid.calendaring.DateTime from, 
                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>EdgeList</code> by genus type effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In active mode, edges are returned that are currently
     *  active. In any status mode, active and inactive edges are
     *  returned.
     *
     *  @param edgeGenusType edge genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>Edge</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>edgeGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeOnDate(org.osid.type.Type edgeGenusType,
                                                                org.osid.calendaring.DateTime from,
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeOnDate(edgeGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.EdgeList getEdgesForSourceNode(org.osid.id.Id sourceNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForSourceNode(sourceNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges corresponding to a source node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForSourceNodeOnDate(sourceNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a source
     *  node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNode(org.osid.id.Id sourceNodeId,
                                                                       org.osid.type.Type edgeGenusType)

        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForSourceNode(sourceNodeId, edgeGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a source
     *  node <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the source node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>, <code>edgeGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForSourceNodeOnDate(org.osid.id.Id sourceNodeId,
                                                                             org.osid.type.Type edgeGenusType,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForSourceNodeOnDate(sourceNodeId, edgeGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.topology.EdgeList getEdgesForDestinationNode(org.osid.id.Id destinationNodeId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForDestinationNode(destinationNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges corresponding to a destination node
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>destinationNodeId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForDestinationNodeOnDate(destinationNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a
     *  destination node <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNode(org.osid.id.Id destinationNodeId,
                                                                            org.osid.type.Type edgeGenusType)

        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForDestinationNode(destinationNodeId, edgeGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to a
     *  destination node <code>Id</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param destinationNodeId the <code>Id</code> of the
     *  destination node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>destinationNodeId</code>, <code>edgeGenusType</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForDestinationNodeOnDate(org.osid.id.Id destinationNodeId,
                                                                                  org.osid.type.Type edgeGenusType,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForDestinationNodeOnDate(destinationNodeId, edgeGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges corresponding to source node and destination node
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodes(org.osid.id.Id sourceNodeId,
                                                       org.osid.id.Id destinationNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForNodes(sourceNodeId, destinationNodeId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges corresponding to source node and destination node
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible
     *  through this session.
     *
     *  In effective mode, edges are returned that are
     *  currently effective.  In any effective mode, effective
     *  edges and those currently expired are returned.
     *
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                             org.osid.id.Id destinationNodeId,
                                                             org.osid.calendaring.DateTime from,
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesForNodesOnDate(sourceNodeId, destinationNodeId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to source
     *  node and destination node <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param  sourceNodeId the <code>Id</code> of the source node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code> or
     *          <code>edgeGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodes(org.osid.id.Id sourceNodeId,
                                                                  org.osid.id.Id destinationNodeId,
                                                                  org.osid.type.Type edgeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForNodes(sourceNodeId, destinationNodeId, edgeGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of edges of a genus type corresponding to source
     *  node and destination node <code>Ids</code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known edges or
     *  an error results. Otherwise, the returned list may contain
     *  only those edges that are accessible through this session.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and those
     *  currently expired are returned.
     *
     *  @param sourceNodeId the <code>Id</code> of the destination
     *  node
     *  @param  destinationNodeId the <code>Id</code> of the destination node
     *  @param edgeGenusType an edge genus type
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EdgeList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>sourceNodeId</code>,
     *          <code>destinationNodeId</code>,
     *          <code>edgeGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdgesByGenusTypeForNodesOnDate(org.osid.id.Id sourceNodeId,
                                                                        org.osid.id.Id destinationNodeId,
                                                                        org.osid.type.Type edgeGenusType,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdgesByGenusTypeForNodesOnDate(sourceNodeId, destinationNodeId, edgeGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Edges</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  edges or an error results. Otherwise, the returned list
     *  may contain only those edges that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, edges are returned that are currently
     *  effective.  In any effective mode, effective edges and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Edges</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList ret = getEdgeList();

        for (org.osid.topology.EdgeLookupSession session : getSessions()) {
            ret.addEdgeList(session.getEdges());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.topology.edge.FederatingEdgeList getEdgeList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.edge.ParallelEdgeList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.topology.edge.CompositeEdgeList());
        }
    }
}
