//
// AbstractMailboxQuery.java
//
//     A template for making a Mailbox Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.mailbox.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for mailboxes.
 */

public abstract class AbstractMailboxQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.messaging.MailboxQuery {

    private final java.util.Collection<org.osid.messaging.records.MailboxQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the message <code> Id </code> for this query. 
     *
     *  @param  messageId a message <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> messageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMessageId(org.osid.id.Id messageId, boolean match) {
        return;
    }


    /**
     *  Clears the message <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMessageIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MessageQuery </code> is available. 
     *
     *  @return <code> true </code> if a message query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a message. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the message query 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuery getMessageQuery() {
        throw new org.osid.UnimplementedException("supportsMessageQuery() is false");
    }


    /**
     *  Matches mailboxes with any messages. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          messages, <code> false </code> to match mailboxes with no 
     *          messages 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        return;
    }


    /**
     *  Clears the message terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        return;
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query to match mailboxes 
     *  that have the specified mailbox as an ancestor. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorMailboxId(org.osid.id.Id mailboxId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorMailboxIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorMailboxQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getAncestorMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorMailboxQuery() is false");
    }


    /**
     *  Matches mailboxes with any ancestor. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          ancestor, <code> false </code> to match root mailboxes 
     */

    @OSID @Override
    public void matchAnyAncestorMailbox(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor mailbox terms. 
     */

    @OSID @Override
    public void clearAncestorMailboxTerms() {
        return;
    }


    /**
     *  Sets the mailbox <code> Id </code> for this query to match mailboxes 
     *  that have the specified mailbox as a descendant. 
     *
     *  @param  mailboxId a mailbox <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantMailboxId(org.osid.id.Id mailboxId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant mailbox <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantMailboxIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MailboxQuery </code> is available. 
     *
     *  @return <code> true </code> if a mailbox query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantMailboxQuery() {
        return (false);
    }


    /**
     *  Gets the query for a mailbox. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the mailbox query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantMailboxQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQuery getDescendantMailboxQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantMailboxQuery() is false");
    }


    /**
     *  Matches mailboxes with any descendant. 
     *
     *  @param  match <code> true </code> to match mailboxes with any 
     *          descendant, <code> false </code> to match leaf mailboxes 
     */

    @OSID @Override
    public void matchAnyDescendantMailbox(boolean match) {
        return;
    }


    /**
     *  Clears the descendant mailbox terms. 
     */

    @OSID @Override
    public void clearDescendantMailboxTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given mailbox query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a mailbox implementing the requested record.
     *
     *  @param mailboxRecordType a mailbox record type
     *  @return the mailbox query record
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxQueryRecord getMailboxQueryRecord(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MailboxQueryRecord record : this.records) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxRecordType + " is not supported");
    }


    /**
     *  Adds a record to this mailbox query. 
     *
     *  @param mailboxQueryRecord mailbox query record
     *  @param mailboxRecordType mailbox record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMailboxQueryRecord(org.osid.messaging.records.MailboxQueryRecord mailboxQueryRecord, 
                                          org.osid.type.Type mailboxRecordType) {

        addRecordType(mailboxRecordType);
        nullarg(mailboxQueryRecord, "mailbox query record");
        this.records.add(mailboxQueryRecord);        
        return;
    }
}
