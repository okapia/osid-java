//
// AbstractFederatingScheduleSlotLookupSession.java
//
//     An abstract federating adapter for a ScheduleSlotLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ScheduleSlotLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.ScheduleSlotLookupSession>
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingScheduleSlotLookupSession</code>.
     */

    protected AbstractFederatingScheduleSlotLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.ScheduleSlotLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>ScheduleSlot</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupScheduleSlots() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            if (session.canLookupScheduleSlots()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ScheduleSlot</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleSlotView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.useComparativeScheduleSlotView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ScheduleSlot</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleSlotView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.usePlenaryScheduleSlotView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedule slots in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  The returns from the lookup methods omit sequestered
     *  schedule slots.
     */

    @OSID @Override
    public void useSequesteredScheduleSlotView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.useSequesteredScheduleSlotView();
        }

        return;
    }


    /**
     *  All schedule slots are returned including sequestered schedule slots.
     */

    @OSID @Override
    public void useUnsequesteredScheduleSlotView() {
        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            session.useUnsequesteredScheduleSlotView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ScheduleSlot</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ScheduleSlot</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ScheduleSlot</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleSlotId <code>Id</code> of the
     *          <code>ScheduleSlot</code>
     *  @return the schedule slot
     *  @throws org.osid.NotFoundException <code>scheduleSlotId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleSlotId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            try {
                return (session.getScheduleSlot(scheduleSlotId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(scheduleSlotId + " not found");
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scheduleSlots specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ScheduleSlots</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  scheduleSlotIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByIds(org.osid.id.IdList scheduleSlotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.scheduleslot.MutableScheduleSlotList ret = new net.okapia.osid.jamocha.calendaring.scheduleslot.MutableScheduleSlotList();

        try (org.osid.id.IdList ids = scheduleSlotIds) {
            while (ids.hasNext()) {
                ret.addScheduleSlot(getScheduleSlot(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> which does not include
     *  schedule slots of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlotsByGenusType(scheduleSlotGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> corresponding to the given
     *  schedule slot genus <code>Type</code> and include any additional
     *  schedule slots with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByParentGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlotsByParentGenusType(scheduleSlotGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  schedule slot record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotRecordType a scheduleSlot record type 
     *  @return the returned <code>ScheduleSlot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByRecordType(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlotsByRecordType(scheduleSlotRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ScheduleSlotList</code> containing the given
     *  set of weekdays.
     *
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  weekdays a set of weekdays
     *  @return the returned <code> ScheduleSlot </code> list
     *  @throws org.osid.InvalidArgumentException a <code> weekday </code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code> weekdays </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByWeekdays(long[] weekdays)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlotsByWeekdays(weekdays));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ScheduleSlotList </code> matching the given
     *  time.
     *
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  time a time
     *  @return the returned <code> ScheduleSlot </code> list
     *  @throws org.osid.NullArgumentException <code> time </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByTime(org.osid.calendaring.Time time)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlotsByTime(time));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ScheduleSlots</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ScheduleSlots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList ret = getScheduleSlotList();

        for (org.osid.calendaring.ScheduleSlotLookupSession session : getSessions()) {
            ret.addScheduleSlotList(session.getScheduleSlots());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.FederatingScheduleSlotList getScheduleSlotList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.ParallelScheduleSlotList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.scheduleslot.CompositeScheduleSlotList());
        }
    }
}
