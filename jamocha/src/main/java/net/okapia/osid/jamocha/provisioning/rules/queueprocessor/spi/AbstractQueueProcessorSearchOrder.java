//
// AbstractQueueProcessorSearchOdrer.java
//
//     Defines a QueueProcessorSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code QueueProcessorSearchOrder}.
 */

public abstract class AbstractQueueProcessorSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorSearchOrder
    implements org.osid.provisioning.rules.QueueProcessorSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueProcessorSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by automatic processing. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAutomatic(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the fifo processing. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFifo(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the removal of procesed queue entries. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRemovesProcessedQueueEntries(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  queueProcessorRecordType a queue processor record type 
     *  @return {@code true} if the queueProcessorRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueProcessorRecordType) {
        for (org.osid.provisioning.rules.records.QueueProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  queueProcessorRecordType the queue processor record type 
     *  @return the queue processor search order record
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(queueProcessorRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorSearchOrderRecord getQueueProcessorSearchOrderRecord(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this queue processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueProcessorRecord the queue processor search odrer record
     *  @param queueProcessorRecordType queue processor record type
     *  @throws org.osid.NullArgumentException
     *          {@code queueProcessorRecord} or
     *          {@code queueProcessorRecordTypequeueProcessor} is
     *          {@code null}
     */
            
    protected void addQueueProcessorRecord(org.osid.provisioning.rules.records.QueueProcessorSearchOrderRecord queueProcessorSearchOrderRecord, 
                                     org.osid.type.Type queueProcessorRecordType) {

        addRecordType(queueProcessorRecordType);
        this.records.add(queueProcessorSearchOrderRecord);
        
        return;
    }
}
