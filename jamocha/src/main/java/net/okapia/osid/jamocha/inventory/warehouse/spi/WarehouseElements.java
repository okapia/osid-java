//
// WarehouseElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.warehouse.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class WarehouseElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the WarehouseElement Id.
     *
     *  @return the warehouse element Id
     */

    public static org.osid.id.Id getWarehouseEntityId() {
        return (makeEntityId("osid.inventory.Warehouse"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.inventory.warehouse.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.inventory.warehouse.Item"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeQueryElementId("osid.inventory.warehouse.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeQueryElementId("osid.inventory.warehouse.Stock"));
    }


    /**
     *  Gets the ModelId element Id.
     *
     *  @return the ModelId element Id
     */

    public static org.osid.id.Id getModelId() {
        return (makeQueryElementId("osid.inventory.warehouse.ModelId"));
    }


    /**
     *  Gets the Model element Id.
     *
     *  @return the Model element Id
     */

    public static org.osid.id.Id getModel() {
        return (makeQueryElementId("osid.inventory.warehouse.Model"));
    }


    /**
     *  Gets the InventoryId element Id.
     *
     *  @return the InventoryId element Id
     */

    public static org.osid.id.Id getInventoryId() {
        return (makeQueryElementId("osid.inventory.warehouse.InventoryId"));
    }


    /**
     *  Gets the Inventory element Id.
     *
     *  @return the Inventory element Id
     */

    public static org.osid.id.Id getInventory() {
        return (makeQueryElementId("osid.inventory.warehouse.Inventory"));
    }


    /**
     *  Gets the AncestorWarehouseId element Id.
     *
     *  @return the AncestorWarehouseId element Id
     */

    public static org.osid.id.Id getAncestorWarehouseId() {
        return (makeQueryElementId("osid.inventory.warehouse.AncestorWarehouseId"));
    }


    /**
     *  Gets the AncestorWarehouse element Id.
     *
     *  @return the AncestorWarehouse element Id
     */

    public static org.osid.id.Id getAncestorWarehouse() {
        return (makeQueryElementId("osid.inventory.warehouse.AncestorWarehouse"));
    }


    /**
     *  Gets the DescendantWarehouseId element Id.
     *
     *  @return the DescendantWarehouseId element Id
     */

    public static org.osid.id.Id getDescendantWarehouseId() {
        return (makeQueryElementId("osid.inventory.warehouse.DescendantWarehouseId"));
    }


    /**
     *  Gets the DescendantWarehouse element Id.
     *
     *  @return the DescendantWarehouse element Id
     */

    public static org.osid.id.Id getDescendantWarehouse() {
        return (makeQueryElementId("osid.inventory.warehouse.DescendantWarehouse"));
    }
}
