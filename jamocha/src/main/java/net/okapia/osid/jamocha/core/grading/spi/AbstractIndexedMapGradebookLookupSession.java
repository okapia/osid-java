//
// AbstractIndexedMapGradebookLookupSession.java
//
//    A simple framework for providing a Gradebook lookup service
//    backed by a fixed collection of gradebooks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Gradebook lookup service backed by a
 *  fixed collection of gradebooks. The gradebooks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some gradebooks may be compatible
 *  with more types than are indicated through these gradebook
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Gradebooks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradebookLookupSession
    extends AbstractMapGradebookLookupSession
    implements org.osid.grading.GradebookLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.Gradebook> gradebooksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.Gradebook>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.Gradebook> gradebooksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.Gradebook>());


    /**
     *  Makes a <code>Gradebook</code> available in this session.
     *
     *  @param  gradebook a gradebook
     *  @throws org.osid.NullArgumentException <code>gradebook<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradebook(org.osid.grading.Gradebook gradebook) {
        super.putGradebook(gradebook);

        this.gradebooksByGenus.put(gradebook.getGenusType(), gradebook);
        
        try (org.osid.type.TypeList types = gradebook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebooksByRecord.put(types.getNextType(), gradebook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a gradebook from this session.
     *
     *  @param gradebookId the <code>Id</code> of the gradebook
     *  @throws org.osid.NullArgumentException <code>gradebookId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradebook(org.osid.id.Id gradebookId) {
        org.osid.grading.Gradebook gradebook;
        try {
            gradebook = getGradebook(gradebookId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradebooksByGenus.remove(gradebook.getGenusType());

        try (org.osid.type.TypeList types = gradebook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebooksByRecord.remove(types.getNextType(), gradebook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradebook(gradebookId);
        return;
    }


    /**
     *  Gets a <code>GradebookList</code> corresponding to the given
     *  gradebook genus <code>Type</code> which does not include
     *  gradebooks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known gradebooks or an error results. Otherwise,
     *  the returned list may contain only those gradebooks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradebookGenusType a gradebook genus type 
     *  @return the returned <code>Gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByGenusType(org.osid.type.Type gradebookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebook.ArrayGradebookList(this.gradebooksByGenus.get(gradebookGenusType)));
    }


    /**
     *  Gets a <code>GradebookList</code> containing the given
     *  gradebook record <code>Type</code>. In plenary mode, the
     *  returned list contains all known gradebooks or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebooks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return the returned <code>gradebook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooksByRecordType(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.gradebook.ArrayGradebookList(this.gradebooksByRecord.get(gradebookRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebooksByGenus.clear();
        this.gradebooksByRecord.clear();

        super.close();

        return;
    }
}
