//
// AbstractIndexedMapFoundryLookupSession.java
//
//    A simple framework for providing a Foundry lookup service
//    backed by a fixed collection of foundries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Foundry lookup service backed by a
 *  fixed collection of foundries. The foundries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some foundries may be compatible
 *  with more types than are indicated through these foundry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Foundries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapFoundryLookupSession
    extends AbstractMapFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Foundry> foundriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Foundry>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.Foundry> foundriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.Foundry>());


    /**
     *  Makes a <code>Foundry</code> available in this session.
     *
     *  @param  foundry a foundry
     *  @throws org.osid.NullArgumentException <code>foundry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putFoundry(org.osid.resourcing.Foundry foundry) {
        super.putFoundry(foundry);

        this.foundriesByGenus.put(foundry.getGenusType(), foundry);
        
        try (org.osid.type.TypeList types = foundry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.foundriesByRecord.put(types.getNextType(), foundry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a foundry from this session.
     *
     *  @param foundryId the <code>Id</code> of the foundry
     *  @throws org.osid.NullArgumentException <code>foundryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeFoundry(org.osid.id.Id foundryId) {
        org.osid.resourcing.Foundry foundry;
        try {
            foundry = getFoundry(foundryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.foundriesByGenus.remove(foundry.getGenusType());

        try (org.osid.type.TypeList types = foundry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.foundriesByRecord.remove(types.getNextType(), foundry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeFoundry(foundryId);
        return;
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> which does not include
     *  foundries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known foundries or an error results. Otherwise,
     *  the returned list may contain only those foundries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.foundry.ArrayFoundryList(this.foundriesByGenus.get(foundryGenusType)));
    }


    /**
     *  Gets a <code>FoundryList</code> containing the given
     *  foundry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known foundries or an error
     *  results. Otherwise, the returned list may contain only those
     *  foundries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return the returned <code>foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByRecordType(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.foundry.ArrayFoundryList(this.foundriesByRecord.get(foundryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.foundriesByGenus.clear();
        this.foundriesByRecord.clear();

        super.close();

        return;
    }
}
