//
// AbstractPriceScheduleQueryInspector.java
//
//     A template for making a PriceScheduleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for price schedules.
 */

public abstract class AbstractPriceScheduleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.ordering.PriceScheduleQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.PriceScheduleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the price <code> Id </code> terms. 
     *
     *  @return the price <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the price terms. 
     *
     *  @return the price terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceQueryInspector[] getPriceTerms() {
        return (new org.osid.ordering.PriceQueryInspector[0]);
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given price schedule query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a price schedule implementing the requested record.
     *
     *  @param priceScheduleRecordType a price schedule record type
     *  @return the price schedule query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleQueryInspectorRecord getPriceScheduleQueryInspectorRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price schedule query. 
     *
     *  @param priceScheduleQueryInspectorRecord price schedule query inspector
     *         record
     *  @param priceScheduleRecordType priceSchedule record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceScheduleQueryInspectorRecord(org.osid.ordering.records.PriceScheduleQueryInspectorRecord priceScheduleQueryInspectorRecord, 
                                                   org.osid.type.Type priceScheduleRecordType) {

        addRecordType(priceScheduleRecordType);
        nullarg(priceScheduleRecordType, "price schedule record type");
        this.records.add(priceScheduleQueryInspectorRecord);        
        return;
    }
}
