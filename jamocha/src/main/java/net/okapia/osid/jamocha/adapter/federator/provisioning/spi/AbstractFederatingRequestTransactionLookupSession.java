//
// AbstractFederatingRequestTransactionLookupSession.java
//
//     An abstract federating adapter for a RequestTransactionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RequestTransactionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.RequestTransactionLookupSession>
    implements org.osid.provisioning.RequestTransactionLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingRequestTransactionLookupSession</code>.
     */

    protected AbstractFederatingRequestTransactionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.RequestTransactionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>RequestTransaction</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequestTransactions() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            if (session.canLookupRequestTransactions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RequestTransaction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestTransactionView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.useComparativeRequestTransactionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RequestTransaction</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestTransactionView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.usePlenaryRequestTransactionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include request transactions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only request transactions whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRequestTransactionView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.useEffectiveRequestTransactionView();
        }

        return;
    }


    /**
     *  All request transactions of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRequestTransactionView() {
        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            session.useAnyEffectiveRequestTransactionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>RequestTransaction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RequestTransaction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RequestTransaction</code> and
     *  retained for compatibility.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionId <code>Id</code> of the
     *          <code>RequestTransaction</code>
     *  @return the request transaction
     *  @throws org.osid.NotFoundException <code>requestTransactionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requestTransactionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction(org.osid.id.Id requestTransactionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            try {
                return (session.getRequestTransaction(requestTransactionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(requestTransactionId + " not found");
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requestTransactions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RequestTransactions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByIds(org.osid.id.IdList requestTransactionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.requesttransaction.MutableRequestTransactionList ret = new net.okapia.osid.jamocha.provisioning.requesttransaction.MutableRequestTransactionList();

        try (org.osid.id.IdList ids = requestTransactionIds) {
            while (ids.hasNext()) {
                ret.addRequestTransaction(getRequestTransaction(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to the given
     *  request transaction genus <code>Type</code> which does not include
     *  request transactions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsByGenusType(requestTransactionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestTransactionList</code> corresponding to the given
     *  request transaction genus <code>Type</code> and include any additional
     *  request transactions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionGenusType a requestTransaction genus type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByParentGenusType(org.osid.type.Type requestTransactionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsByParentGenusType(requestTransactionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestTransactionList</code> containing the given
     *  request transaction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestTransactionRecordType a requestTransaction record type 
     *  @return the returned <code>RequestTransaction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRecordType(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsByRecordType(requestTransactionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestTransactionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *  
     *  In active mode, request transactions are returned that are currently
     *  active. In any status mode, active and inactive request transactions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RequestTransaction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of request transactions corresponding to a request
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective request
     *  transactions and those currently expired are returned.
     *
     *  @param  requestId the <code>Id</code> of the request
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>requestId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsByRequest(org.osid.id.Id requestId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsByRequest(requestId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to a
     *  submitter <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known request
     *  transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are
     *  accessible through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the submitter
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitter(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForSubmitter(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to a submitter
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the submitter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterOnDate(org.osid.id.Id resourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForSubmitterOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to a broker
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBroker(org.osid.id.Id brokerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForBroker(brokerId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to a broker
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForBrokerOnDate(org.osid.id.Id brokerId,
                                                                                              org.osid.calendaring.DateTime from,
                                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForBrokerOnDate(brokerId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to submitter and broker
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the submitter
     *  @param  brokerId the <code>Id</code> of the broker
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>brokerId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBroker(org.osid.id.Id resourceId,
                                                                                                    org.osid.id.Id brokerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForSubmitterAndBroker(resourceId, brokerId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of request transactions corresponding to submitter and broker
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible
     *  through this session.
     *
     *  In effective mode, request transactions are returned that are
     *  currently effective.  In any effective mode, effective
     *  request transactions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestTransactionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>brokerId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactionsForSubmitterAndBrokerOnDate(org.osid.id.Id resourceId,
                                                                                                          org.osid.id.Id brokerId,
                                                                                                          org.osid.calendaring.DateTime from,
                                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactionsForSubmitterAndBrokerOnDate(resourceId, brokerId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RequestTransactions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  request transactions or an error results. Otherwise, the returned list
     *  may contain only those request transactions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, request transactions are returned that are currently
     *  effective.  In any effective mode, effective request transactions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>RequestTransactions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionList getRequestTransactions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList ret = getRequestTransactionList();

        for (org.osid.provisioning.RequestTransactionLookupSession session : getSessions()) {
            ret.addRequestTransactionList(session.getRequestTransactions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.FederatingRequestTransactionList getRequestTransactionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.ParallelRequestTransactionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.requesttransaction.CompositeRequestTransactionList());
        }
    }
}
