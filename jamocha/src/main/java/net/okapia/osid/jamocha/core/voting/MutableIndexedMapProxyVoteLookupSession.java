//
// MutableIndexedMapProxyVoteLookupSession
//
//    Implements a Vote lookup service backed by a collection of
//    votes indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Vote lookup service backed by a collection of
 *  votes. The votes are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some votes may be compatible
 *  with more types than are indicated through these vote
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of votes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyVoteLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractIndexedMapVoteLookupSession
    implements org.osid.voting.VoteLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyVoteLookupSession} with
     *  no vote.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyVoteLookupSession(org.osid.voting.Polls polls,
                                                       org.osid.proxy.Proxy proxy) {
        setPolls(polls);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyVoteLookupSession} with
     *  a single vote.
     *
     *  @param polls the polls
     *  @param  vote an vote
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code vote}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyVoteLookupSession(org.osid.voting.Polls polls,
                                                       org.osid.voting.Vote vote, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putVote(vote);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyVoteLookupSession} using
     *  an array of votes.
     *
     *  @param polls the polls
     *  @param  votes an array of votes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code votes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyVoteLookupSession(org.osid.voting.Polls polls,
                                                       org.osid.voting.Vote[] votes, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putVotes(votes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyVoteLookupSession} using
     *  a collection of votes.
     *
     *  @param polls the polls
     *  @param  votes a collection of votes
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code votes}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyVoteLookupSession(org.osid.voting.Polls polls,
                                                       java.util.Collection<? extends org.osid.voting.Vote> votes,
                                                       org.osid.proxy.Proxy proxy) {
        this(polls, proxy);
        putVotes(votes);
        return;
    }

    
    /**
     *  Makes a {@code Vote} available in this session.
     *
     *  @param  vote a vote
     *  @throws org.osid.NullArgumentException {@code vote{@code 
     *          is {@code null}
     */

    @Override
    public void putVote(org.osid.voting.Vote vote) {
        super.putVote(vote);
        return;
    }


    /**
     *  Makes an array of votes available in this session.
     *
     *  @param  votes an array of votes
     *  @throws org.osid.NullArgumentException {@code votes{@code 
     *          is {@code null}
     */

    @Override
    public void putVotes(org.osid.voting.Vote[] votes) {
        super.putVotes(votes);
        return;
    }


    /**
     *  Makes collection of votes available in this session.
     *
     *  @param  votes a collection of votes
     *  @throws org.osid.NullArgumentException {@code vote{@code 
     *          is {@code null}
     */

    @Override
    public void putVotes(java.util.Collection<? extends org.osid.voting.Vote> votes) {
        super.putVotes(votes);
        return;
    }


    /**
     *  Removes a Vote from this session.
     *
     *  @param voteId the {@code Id} of the vote
     *  @throws org.osid.NullArgumentException {@code voteId{@code  is
     *          {@code null}
     */

    @Override
    public void removeVote(org.osid.id.Id voteId) {
        super.removeVote(voteId);
        return;
    }    
}
