//
// AbstractMapMessageLookupSession
//
//    A simple framework for providing a Message lookup service
//    backed by a fixed collection of messages.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Message lookup service backed by a
 *  fixed collection of messages. The messages are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Messages</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapMessageLookupSession
    extends net.okapia.osid.jamocha.messaging.spi.AbstractMessageLookupSession
    implements org.osid.messaging.MessageLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.messaging.Message> messages = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.messaging.Message>());


    /**
     *  Makes a <code>Message</code> available in this session.
     *
     *  @param  message a message
     *  @throws org.osid.NullArgumentException <code>message<code>
     *          is <code>null</code>
     */

    protected void putMessage(org.osid.messaging.Message message) {
        this.messages.put(message.getId(), message);
        return;
    }


    /**
     *  Makes an array of messages available in this session.
     *
     *  @param  messages an array of messages
     *  @throws org.osid.NullArgumentException <code>messages<code>
     *          is <code>null</code>
     */

    protected void putMessages(org.osid.messaging.Message[] messages) {
        putMessages(java.util.Arrays.asList(messages));
        return;
    }


    /**
     *  Makes a collection of messages available in this session.
     *
     *  @param  messages a collection of messages
     *  @throws org.osid.NullArgumentException <code>messages<code>
     *          is <code>null</code>
     */

    protected void putMessages(java.util.Collection<? extends org.osid.messaging.Message> messages) {
        for (org.osid.messaging.Message message : messages) {
            this.messages.put(message.getId(), message);
        }

        return;
    }


    /**
     *  Removes a Message from this session.
     *
     *  @param  messageId the <code>Id</code> of the message
     *  @throws org.osid.NullArgumentException <code>messageId<code> is
     *          <code>null</code>
     */

    protected void removeMessage(org.osid.id.Id messageId) {
        this.messages.remove(messageId);
        return;
    }


    /**
     *  Gets the <code>Message</code> specified by its <code>Id</code>.
     *
     *  @param  messageId <code>Id</code> of the <code>Message</code>
     *  @return the message
     *  @throws org.osid.NotFoundException <code>messageId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>messageId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Message getMessage(org.osid.id.Id messageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.messaging.Message message = this.messages.get(messageId);
        if (message == null) {
            throw new org.osid.NotFoundException("message not found: " + messageId);
        }

        return (message);
    }


    /**
     *  Gets all <code>Messages</code>. In plenary mode, the returned
     *  list contains all known messages or an error
     *  results. Otherwise, the returned list may contain only those
     *  messages that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Messages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.message.ArrayMessageList(this.messages.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.messages.clear();
        super.close();
        return;
    }
}
