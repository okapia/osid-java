//
// AbstractPool.java
//
//     Defines a Pool.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Pool</code>.
 */

public abstract class AbstractPool
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.provisioning.Pool {

    private org.osid.provisioning.Broker broker;
    private long size = -1;

    private final java.util.Collection<org.osid.provisioning.records.PoolRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the broker <code> Id. </code> 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.broker.getId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.broker);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException
     *          <code>broker</code> is <code>null</code>
     */

    protected void setBroker(org.osid.provisioning.Broker broker) {
        nullarg(broker, "broker");
        this.broker = broker;
        return;
    }


    /**
     *  Tests if a pool size is available. 
     *
     *  @return <code> true </code> if a queue size is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSize() {
        if (this.size < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the size of the pool. 
     *
     *  @return the size 
     *  @throws org.osid.IllegalStateException <code> hasSize() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getSize() {
        return (this.size);
    }


    /**
     *  Sets the size.
     *
     *  @param size the pool size
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    protected void setSize(long size) {
        cardinalarg(size, "size");
        this.size = size;
        return;
    }


    /**
     *  Tests if this pool supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolRecordType a pool record type 
     *  @return <code>true</code> if the poolRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolRecordType) {
        for (org.osid.provisioning.records.PoolRecord record : this.records) {
            if (record.implementsRecordType(poolRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Pool</code>
     *  record <code>Type</code>.
     *
     *  @param  poolRecordType the pool record type 
     *  @return the pool record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolRecord getPoolRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolRecord record : this.records) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolRecord the pool record
     *  @param poolRecordType pool record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecord</code> or
     *          <code>poolRecordTypepool</code> is
     *          <code>null</code>
     */
            
    protected void addPoolRecord(org.osid.provisioning.records.PoolRecord poolRecord, 
                                 org.osid.type.Type poolRecordType) {

        nullarg(poolRecord, "pool record");
        addRecordType(poolRecordType);
        this.records.add(poolRecord);
        
        return;
    }
}
