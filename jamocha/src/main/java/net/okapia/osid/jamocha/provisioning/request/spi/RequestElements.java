//
// RequestElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RequestElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the RequestElement Id.
     *
     *  @return the request element Id
     */

    public static org.osid.id.Id getRequestEntityId() {
        return (makeEntityId("osid.provisioning.Request"));
    }


    /**
     *  Gets the RequestTransactionId element Id.
     *
     *  @return the RequestTransactionId element Id
     */

    public static org.osid.id.Id getRequestTransactionId() {
        return (makeElementId("osid.provisioning.request.RequestTransactionId"));
    }


    /**
     *  Gets the RequestTransaction element Id.
     *
     *  @return the RequestTransaction element Id
     */

    public static org.osid.id.Id getRequestTransaction() {
        return (makeElementId("osid.provisioning.request.RequestTransaction"));
    }


    /**
     *  Gets the QueueId element Id.
     *
     *  @return the QueueId element Id
     */

    public static org.osid.id.Id getQueueId() {
        return (makeElementId("osid.provisioning.request.QueueId"));
    }


    /**
     *  Gets the Queue element Id.
     *
     *  @return the Queue element Id
     */

    public static org.osid.id.Id getQueue() {
        return (makeElementId("osid.provisioning.request.Queue"));
    }


    /**
     *  Gets the RequestDate element Id.
     *
     *  @return the RequestDate element Id
     */

    public static org.osid.id.Id getRequestDate() {
        return (makeElementId("osid.provisioning.request.RequestDate"));
    }


    /**
     *  Gets the RequesterId element Id.
     *
     *  @return the RequesterId element Id
     */

    public static org.osid.id.Id getRequesterId() {
        return (makeElementId("osid.provisioning.request.RequesterId"));
    }


    /**
     *  Gets the Requester element Id.
     *
     *  @return the Requester element Id
     */

    public static org.osid.id.Id getRequester() {
        return (makeElementId("osid.provisioning.request.Requester"));
    }


    /**
     *  Gets the RequestingAgentId element Id.
     *
     *  @return the RequestingAgentId element Id
     */

    public static org.osid.id.Id getRequestingAgentId() {
        return (makeElementId("osid.provisioning.request.RequestingAgentId"));
    }


    /**
     *  Gets the RequestingAgent element Id.
     *
     *  @return the RequestingAgent element Id
     */

    public static org.osid.id.Id getRequestingAgent() {
        return (makeElementId("osid.provisioning.request.RequestingAgent"));
    }


    /**
     *  Gets the PoolId element Id.
     *
     *  @return the PoolId element Id
     */

    public static org.osid.id.Id getPoolId() {
        return (makeElementId("osid.provisioning.request.PoolId"));
    }


    /**
     *  Gets the Pool element Id.
     *
     *  @return the Pool element Id
     */

    public static org.osid.id.Id getPool() {
        return (makeElementId("osid.provisioning.request.Pool"));
    }


    /**
     *  Gets the RequestedProvisionableIds element Id.
     *
     *  @return the RequestedProvisionableIds element Id
     */

    public static org.osid.id.Id getRequestedProvisionableIds() {
        return (makeElementId("osid.provisioning.request.RequestedProvisionableIds"));
    }


    /**
     *  Gets the RequestedProvisionables element Id.
     *
     *  @return the RequestedProvisionables element Id
     */

    public static org.osid.id.Id getRequestedProvisionables() {
        return (makeElementId("osid.provisioning.request.RequestedProvisionables"));
    }


    /**
     *  Gets the ExchangeProvisionId element Id.
     *
     *  @return the ExchangeProvisionId element Id
     */

    public static org.osid.id.Id getExchangeProvisionId() {
        return (makeElementId("osid.provisioning.request.ExchangeProvisionId"));
    }


    /**
     *  Gets the ExchangeProvision element Id.
     *
     *  @return the ExchangeProvision element Id
     */

    public static org.osid.id.Id getExchangeProvision() {
        return (makeElementId("osid.provisioning.request.ExchangeProvision"));
    }


    /**
     *  Gets the OriginProvisionId element Id.
     *
     *  @return the OriginProvisionId element Id
     */

    public static org.osid.id.Id getOriginProvisionId() {
        return (makeElementId("osid.provisioning.request.OriginProvisionId"));
    }


    /**
     *  Gets the OriginProvision element Id.
     *
     *  @return the OriginProvision element Id
     */

    public static org.osid.id.Id getOriginProvision() {
        return (makeElementId("osid.provisioning.request.OriginProvision"));
    }


    /**
     *  Gets the Position element Id.
     *
     *  @return the Position element Id
     */

    public static org.osid.id.Id getPosition() {
        return (makeElementId("osid.provisioning.request.Position"));
    }


    /**
     *  Gets the EWA element Id.
     *
     *  @return the EWA element Id
     */

    public static org.osid.id.Id getEWA() {
        return (makeElementId("osid.provisioning.request.EWA"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.request.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.request.Distributor"));
    }
}
