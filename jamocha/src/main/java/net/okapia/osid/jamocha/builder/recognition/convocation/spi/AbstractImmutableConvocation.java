//
// AbstractImmutableConvocation.java
//
//     Wraps a mutable Convocation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Convocation</code> to hide modifiers. This
 *  wrapper provides an immutized Convocation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying convocation whose state changes are visible.
 */

public abstract class AbstractImmutableConvocation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.recognition.Convocation {

    private final org.osid.recognition.Convocation convocation;


    /**
     *  Constructs a new <code>AbstractImmutableConvocation</code>.
     *
     *  @param convocation the convocation to immutablize
     *  @throws org.osid.NullArgumentException <code>convocation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableConvocation(org.osid.recognition.Convocation convocation) {
        super(convocation);
        this.convocation = convocation;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the awards. 
     *
     *  @return the award <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAwardIds() {
        return (this.convocation.getAwardIds());
    }


    /**
     *  Gets the awards. 
     *
     *  @return the awards 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException {

        return (this.convocation.getAwards());
    }


    /**
     *  Tests if the convocation confers awards for a period of time. 
     *
     *  @return <code> true </code> if a time period exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasTimePeriod() {
        return (this.convocation.hasTimePeriod());
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the time period <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.convocation.getTimePeriodId());
    }


    /**
     *  Gets the time period. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.convocation.getTimePeriod());
    }


    /**
     *  Gets the date the awards in this convocation are to be conferred. 
     *
     *  @return the awards date 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code>true</code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.convocation.getDate());
    }


    /**
     *  Gets the convocation record corresponding to the given <code> 
     *  Convocation </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  convocationRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(convocationRecordType) </code> is <code> true </code> . 
     *
     *  @param  convocationRecordType the type of convocation record to 
     *          retrieve 
     *  @return the convocation record 
     *  @throws org.osid.NullArgumentException <code> convocationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(convocationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationRecord getConvocationRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        return (this.convocation.getConvocationRecord(convocationRecordType));
    }
}

