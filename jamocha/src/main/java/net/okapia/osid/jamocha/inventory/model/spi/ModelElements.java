//
// ModelElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.model.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ModelElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ModelElement Id.
     *
     *  @return the model element Id
     */

    public static org.osid.id.Id getModelEntityId() {
        return (makeEntityId("osid.inventory.Model"));
    }


    /**
     *  Gets the ManufacturerId element Id.
     *
     *  @return the ManufacturerId element Id
     */

    public static org.osid.id.Id getManufacturerId() {
        return (makeElementId("osid.inventory.model.ManufacturerId"));
    }


    /**
     *  Gets the Manufacturer element Id.
     *
     *  @return the Manufacturer element Id
     */

    public static org.osid.id.Id getManufacturer() {
        return (makeElementId("osid.inventory.model.Manufacturer"));
    }


    /**
     *  Gets the Archetype element Id.
     *
     *  @return the Archetype element Id
     */

    public static org.osid.id.Id getArchetype() {
        return (makeElementId("osid.inventory.model.Archetype"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.inventory.model.Number"));
    }


    /**
     *  Gets the WarehouseId element Id.
     *
     *  @return the WarehouseId element Id
     */

    public static org.osid.id.Id getWarehouseId() {
        return (makeQueryElementId("osid.inventory.model.WarehouseId"));
    }


    /**
     *  Gets the Warehouse element Id.
     *
     *  @return the Warehouse element Id
     */

    public static org.osid.id.Id getWarehouse() {
        return (makeQueryElementId("osid.inventory.model.Warehouse"));
    }
}
