//
// AbstractRelationshipBatchProxyManager.java
//
//     An adapter for a RelationshipBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RelationshipBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRelationshipBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.relationship.batch.RelationshipBatchProxyManager>
    implements org.osid.relationship.batch.RelationshipBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRelationshipBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRelationshipBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of relationships is available. 
     *
     *  @return <code> true </code> if a relationship bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipBatchAdmin() {
        return (getAdapteeManager().supportsRelationshipBatchAdmin());
    }


    /**
     *  Tests if bulk administration of family is available. 
     *
     *  @return <code> true </code> if a family bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyBatchAdmin() {
        return (getAdapteeManager().supportsFamilyBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  relationship administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchAdminSession getRelationshipBatchAdminSessionForFamily(org.osid.id.Id familyId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipBatchAdminSessionForFamily(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk family 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.FamilyBatchAdminSession getFamilyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
