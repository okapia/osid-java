//
// AbstractMapConvocationLookupSession
//
//    A simple framework for providing a Convocation lookup service
//    backed by a fixed collection of convocations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Convocation lookup service backed by a
 *  fixed collection of convocations. The convocations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Convocations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapConvocationLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.recognition.Convocation> convocations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.recognition.Convocation>());


    /**
     *  Makes a <code>Convocation</code> available in this session.
     *
     *  @param  convocation a convocation
     *  @throws org.osid.NullArgumentException <code>convocation<code>
     *          is <code>null</code>
     */

    protected void putConvocation(org.osid.recognition.Convocation convocation) {
        this.convocations.put(convocation.getId(), convocation);
        return;
    }


    /**
     *  Makes an array of convocations available in this session.
     *
     *  @param  convocations an array of convocations
     *  @throws org.osid.NullArgumentException <code>convocations<code>
     *          is <code>null</code>
     */

    protected void putConvocations(org.osid.recognition.Convocation[] convocations) {
        putConvocations(java.util.Arrays.asList(convocations));
        return;
    }


    /**
     *  Makes a collection of convocations available in this session.
     *
     *  @param  convocations a collection of convocations
     *  @throws org.osid.NullArgumentException <code>convocations<code>
     *          is <code>null</code>
     */

    protected void putConvocations(java.util.Collection<? extends org.osid.recognition.Convocation> convocations) {
        for (org.osid.recognition.Convocation convocation : convocations) {
            this.convocations.put(convocation.getId(), convocation);
        }

        return;
    }


    /**
     *  Removes a Convocation from this session.
     *
     *  @param  convocationId the <code>Id</code> of the convocation
     *  @throws org.osid.NullArgumentException <code>convocationId<code> is
     *          <code>null</code>
     */

    protected void removeConvocation(org.osid.id.Id convocationId) {
        this.convocations.remove(convocationId);
        return;
    }


    /**
     *  Gets the <code>Convocation</code> specified by its <code>Id</code>.
     *
     *  @param  convocationId <code>Id</code> of the <code>Convocation</code>
     *  @return the convocation
     *  @throws org.osid.NotFoundException <code>convocationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>convocationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation(org.osid.id.Id convocationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.recognition.Convocation convocation = this.convocations.get(convocationId);
        if (convocation == null) {
            throw new org.osid.NotFoundException("convocation not found: " + convocationId);
        }

        return (convocation);
    }


    /**
     *  Gets all <code>Convocations</code>. In plenary mode, the returned
     *  list contains all known convocations or an error
     *  results. Otherwise, the returned list may contain only those
     *  convocations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Convocations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationList getConvocations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recognition.convocation.ArrayConvocationList(this.convocations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.convocations.clear();
        super.close();
        return;
    }
}
