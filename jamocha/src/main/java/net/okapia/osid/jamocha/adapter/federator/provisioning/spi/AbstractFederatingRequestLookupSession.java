//
// AbstractFederatingRequestLookupSession.java
//
//     An abstract federating adapter for a RequestLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RequestLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRequestLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.RequestLookupSession>
    implements org.osid.provisioning.RequestLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingRequestLookupSession</code>.
     */

    protected AbstractFederatingRequestLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.RequestLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Request</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequests() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            if (session.canLookupRequests()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Request</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequestView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.useComparativeRequestView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Request</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequestView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.usePlenaryRequestView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requests in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only requests whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRequestView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.useEffectiveRequestView();
        }

        return;
    }


    /**
     *  All requests of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRequestView() {
        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            session.useAnyEffectiveRequestView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Request</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Request</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Request</code> and
     *  retained for compatibility.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestId <code>Id</code> of the
     *          <code>Request</code>
     *  @return the request
     *  @throws org.osid.NotFoundException <code>requestId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requestId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest(org.osid.id.Id requestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            try {
                return (session.getRequest(requestId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(requestId + " not found");
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requests specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Requests</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, requests are returned that are currently effective.
     *  In any effective mode, effective requests and those currently expired
     *  are returned.
     *
     *  @param  requestIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>requestIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByIds(org.osid.id.IdList requestIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.request.MutableRequestList ret = new net.okapia.osid.jamocha.provisioning.request.MutableRequestList();

        try (org.osid.id.IdList ids = requestIds) {
            while (ids.hasNext()) {
                ret.addRequest(getRequest(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  request genus <code>Type</code> which does not include
     *  requests of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently effective.
     *  In any effective mode, effective requests and those currently expired
     *  are returned.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsByGenusType(requestGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestList</code> corresponding to the given
     *  request genus <code>Type</code> and include any additional
     *  requests with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestGenusType a request genus type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByParentGenusType(org.osid.type.Type requestGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsByParentGenusType(requestGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestList</code> containing the given
     *  request record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @param  requestRecordType a request record type 
     *  @return the returned <code>Request</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsByRecordType(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsByRecordType(requestRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RequestList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *  
     *  In active mode, requests are returned that are currently
     *  active. In any status mode, active and inactive requests
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Request</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsOnDate(org.osid.calendaring.DateTime from, 
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of requests corresponding to a requester
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the requester
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestList getRequestsForRequester(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForRequester(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of requests corresponding to a requester
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the requester
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterOnDate(org.osid.id.Id resourceId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForRequesterOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of requests corresponding to a queue
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.RequestList getRequestsForQueue(org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForQueue(queueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of requests corresponding to a queue
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>queueId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForQueueOnDate(org.osid.id.Id queueId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForQueueOnDate(queueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the requester
     *  @param  queueId the <code>Id</code> of the queue
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>queueId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueue(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id queueId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForRequesterAndQueue(resourceId, queueId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of requests corresponding to requester and queue
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible
     *  through this session.
     *
     *  In effective mode, requests are returned that are
     *  currently effective.  In any effective mode, effective
     *  requests and those currently expired are returned.
     *
     *  @param  queueId the <code>Id</code> of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RequestList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>queueId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequestsForRequesterAndQueueOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id queueId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequestsForRequesterAndQueueOnDate(resourceId, queueId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Requests</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  requests or an error results. Otherwise, the returned list
     *  may contain only those requests that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, requests are returned that are currently
     *  effective.  In any effective mode, effective requests and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Requests</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList ret = getRequestList();

        for (org.osid.provisioning.RequestLookupSession session : getSessions()) {
            ret.addRequestList(session.getRequests());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.request.FederatingRequestList getRequestList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.request.ParallelRequestList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.request.CompositeRequestList());
        }
    }
}
