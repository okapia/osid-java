//
// InvariantMapProxyStockLookupSession
//
//    Implements a Stock lookup service backed by a fixed
//    collection of stocks. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Stock lookup service backed by a fixed
 *  collection of stocks. The stocks are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyStockLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractMapStockLookupSession
    implements org.osid.inventory.StockLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyStockLookupSession} with no
     *  stocks.
     *
     *  @param warehouse the warehouse
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyStockLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.proxy.Proxy proxy) {
        setWarehouse(warehouse);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyStockLookupSession} with a single
     *  stock.
     *
     *  @param warehouse the warehouse
     *  @param stock a single stock
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code stock} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStockLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Stock stock, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putStock(stock);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyStockLookupSession} using
     *  an array of stocks.
     *
     *  @param warehouse the warehouse
     *  @param stocks an array of stocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code stocks} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStockLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Stock[] stocks, org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putStocks(stocks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyStockLookupSession} using a
     *  collection of stocks.
     *
     *  @param warehouse the warehouse
     *  @param stocks a collection of stocks
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code warehouse},
     *          {@code stocks} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyStockLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  java.util.Collection<? extends org.osid.inventory.Stock> stocks,
                                                  org.osid.proxy.Proxy proxy) {

        this(warehouse, proxy);
        putStocks(stocks);
        return;
    }
}
