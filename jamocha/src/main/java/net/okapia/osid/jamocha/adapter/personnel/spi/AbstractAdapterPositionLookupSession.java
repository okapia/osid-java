//
// AbstractAdapterPositionLookupSession.java
//
//    A Position lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Position lookup session adapter.
 */

public abstract class AbstractAdapterPositionLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.PositionLookupSession {

    private final org.osid.personnel.PositionLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPositionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPositionLookupSession(org.osid.personnel.PositionLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Realm/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Realm Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.session.getRealmId());
    }


    /**
     *  Gets the {@code Realm} associated with this session.
     *
     *  @return the {@code Realm} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRealm());
    }


    /**
     *  Tests if this user can perform {@code Position} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPositions() {
        return (this.session.canLookupPositions());
    }


    /**
     *  A complete view of the {@code Position} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePositionView() {
        this.session.useComparativePositionView();
        return;
    }


    /**
     *  A complete view of the {@code Position} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPositionView() {
        this.session.usePlenaryPositionView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include positions in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.session.useFederatedRealmView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.session.useIsolatedRealmView();
        return;
    }
    

    /**
     *  Only positions whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePositionView() {
        this.session.useEffectivePositionView();
        return;
    }
    

    /**
     *  All positions of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePositionView() {
        this.session.useAnyEffectivePositionView();
        return;
    }

     
    /**
     *  Gets the {@code Position} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Position} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Position} and
     *  retained for compatibility.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param positionId {@code Id} of the {@code Position}
     *  @return the position
     *  @throws org.osid.NotFoundException {@code positionId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code positionId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Position getPosition(org.osid.id.Id positionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPosition(positionId));
    }


    /**
     *  Gets a {@code PositionList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  positions specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Positions} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Position} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code positionIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByIds(org.osid.id.IdList positionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositionsByIds(positionIds));
    }


    /**
     *  Gets a {@code PositionList} corresponding to the given
     *  position genus {@code Type} which does not include
     *  positions of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned {@code Position} list
     *  @throws org.osid.NullArgumentException
     *          {@code positionGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositionsByGenusType(positionGenusType));
    }


    /**
     *  Gets a {@code PositionList} corresponding to the given
     *  position genus {@code Type} and include any additional
     *  positions with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionGenusType a position genus type 
     *  @return the returned {@code Position} list
     *  @throws org.osid.NullArgumentException
     *          {@code positionGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByParentGenusType(org.osid.type.Type positionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositionsByParentGenusType(positionGenusType));
    }


    /**
     *  Gets a {@code PositionList} containing the given
     *  position record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  positionRecordType a position record type 
     *  @return the returned {@code Position} list
     *  @throws org.osid.NullArgumentException
     *          {@code positionRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsByRecordType(org.osid.type.Type positionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositionsByRecordType(positionRecordType));
    }


    /**
     *  Gets a {@code PositionList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible
     *  through this session.
     *  
     *  In active mode, positions are returned that are currently
     *  active. In any status mode, active and inactive positions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Position} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.personnel.PositionList getPositionsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositionsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PositionList} for the given organization.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective. In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization {@code Id} 
     *  @return the returned {@code PositionList} 
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *         is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganization(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getPositionsForOrganization(organizationId));
    }


    /**
     *  Gets a {@code PositionList} for an organizatoon and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session.
     *  
     *  In effective mode, positions are returned that are currently
     *  effective. In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @param  organizationId an organization {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of positions 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code organization,
     *         from}, or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositionsForOrganizationOnDate(org.osid.id.Id organizationId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getPositionsForOrganizationOnDate(organizationId, from, to));
    }


    /**
     *  Gets all {@code Positions}. 
     *
     *  In plenary mode, the returned list contains all known
     *  positions or an error results. Otherwise, the returned list
     *  may contain only those positions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, positions are returned that are currently
     *  effective.  In any effective mode, effective positions and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Positions} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.PositionList getPositions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPositions());
    }
}
