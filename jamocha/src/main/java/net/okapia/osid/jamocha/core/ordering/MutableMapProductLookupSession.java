//
// MutableMapProductLookupSession
//
//    Implements a Product lookup service backed by a collection of
//    products that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a Product lookup service backed by a collection of
 *  products. The products are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of products can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProductLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractMapProductLookupSession
    implements org.osid.ordering.ProductLookupSession {


    /**
     *  Constructs a new {@code MutableMapProductLookupSession}
     *  with no products.
     *
     *  @param store the store
     *  @throws org.osid.NullArgumentException {@code store} is
     *          {@code null}
     */

      public MutableMapProductLookupSession(org.osid.ordering.Store store) {
        setStore(store);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProductLookupSession} with a
     *  single product.
     *
     *  @param store the store  
     *  @param product a product
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code product} is {@code null}
     */

    public MutableMapProductLookupSession(org.osid.ordering.Store store,
                                           org.osid.ordering.Product product) {
        this(store);
        putProduct(product);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProductLookupSession}
     *  using an array of products.
     *
     *  @param store the store
     *  @param products an array of products
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code products} is {@code null}
     */

    public MutableMapProductLookupSession(org.osid.ordering.Store store,
                                           org.osid.ordering.Product[] products) {
        this(store);
        putProducts(products);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProductLookupSession}
     *  using a collection of products.
     *
     *  @param store the store
     *  @param products a collection of products
     *  @throws org.osid.NullArgumentException {@code store} or
     *          {@code products} is {@code null}
     */

    public MutableMapProductLookupSession(org.osid.ordering.Store store,
                                           java.util.Collection<? extends org.osid.ordering.Product> products) {

        this(store);
        putProducts(products);
        return;
    }

    
    /**
     *  Makes a {@code Product} available in this session.
     *
     *  @param product a product
     *  @throws org.osid.NullArgumentException {@code product{@code  is
     *          {@code null}
     */

    @Override
    public void putProduct(org.osid.ordering.Product product) {
        super.putProduct(product);
        return;
    }


    /**
     *  Makes an array of products available in this session.
     *
     *  @param products an array of products
     *  @throws org.osid.NullArgumentException {@code products{@code 
     *          is {@code null}
     */

    @Override
    public void putProducts(org.osid.ordering.Product[] products) {
        super.putProducts(products);
        return;
    }


    /**
     *  Makes collection of products available in this session.
     *
     *  @param products a collection of products
     *  @throws org.osid.NullArgumentException {@code products{@code  is
     *          {@code null}
     */

    @Override
    public void putProducts(java.util.Collection<? extends org.osid.ordering.Product> products) {
        super.putProducts(products);
        return;
    }


    /**
     *  Removes a Product from this session.
     *
     *  @param productId the {@code Id} of the product
     *  @throws org.osid.NullArgumentException {@code productId{@code 
     *          is {@code null}
     */

    @Override
    public void removeProduct(org.osid.id.Id productId) {
        super.removeProduct(productId);
        return;
    }    
}
