//
// AbstractMutablePostEntry.java
//
//     Defines a mutable PostEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>PostEntry</code>.
 */

public abstract class AbstractMutablePostEntry
    extends net.okapia.osid.jamocha.financials.posting.postentry.spi.AbstractPostEntry
    implements org.osid.financials.posting.PostEntry,
               net.okapia.osid.jamocha.builder.financials.posting.postentry.PostEntryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this post entry. 
     *
     *  @param record post entry record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPostEntryRecord(org.osid.financials.posting.records.PostEntryRecord record, org.osid.type.Type recordType) {
        super.addPostEntryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this post entry.
     *
     *  @param displayName the name for this post entry
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this post entry.
     *
     *  @param description the description of this post entry
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    @Override
    public void setPost(org.osid.financials.posting.Post post) {
        super.setPost(post);
        return;
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException <code>account</code> is
     *          <code>null</code>
     */

    @Override
    public void setAccount(org.osid.financials.Account account) {
        super.setAccount(account);
        return;
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    @Override
    public void setActivity(org.osid.financials.Activity activity) {
        super.setActivity(activity);
        return;
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setAmount(org.osid.financials.Currency amount) {
        super.setAmount(amount);
        return;
    }


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this entry amount is a
     *          debit, <code> false </code> if it is a credit
     */

    @Override
    public void setDebit(boolean debit) {
        super.setDebit(debit);
        return;
    }
}

