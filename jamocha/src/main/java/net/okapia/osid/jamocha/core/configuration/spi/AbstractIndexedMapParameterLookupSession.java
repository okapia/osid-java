//
// AbstractIndexedMapParameterLookupSession.java
//
//    A simple framework for providing a Parameter lookup service
//    backed by a fixed collection of parameters with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Parameter lookup service backed by a
 *  fixed collection of parameters. The parameters are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some parameters may be compatible
 *  with more types than are indicated through these parameter
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Parameters</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapParameterLookupSession
    extends AbstractMapParameterLookupSession
    implements org.osid.configuration.ParameterLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.configuration.Parameter> parametersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.Parameter>());
    private final MultiMap<org.osid.type.Type, org.osid.configuration.Parameter> parametersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.configuration.Parameter>());


    /**
     *  Makes a <code>Parameter</code> available in this session.
     *
     *  @param  parameter a parameter
     *  @throws org.osid.NullArgumentException <code>parameter<code> is
     *          <code>null</code>
     */

    @Override
    protected void putParameter(org.osid.configuration.Parameter parameter) {
        super.putParameter(parameter);

        this.parametersByGenus.put(parameter.getGenusType(), parameter);
        
        try (org.osid.type.TypeList types = parameter.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parametersByRecord.put(types.getNextType(), parameter);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a parameter from this session.
     *
     *  @param parameterId the <code>Id</code> of the parameter
     *  @throws org.osid.NullArgumentException <code>parameterId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeParameter(org.osid.id.Id parameterId) {
        org.osid.configuration.Parameter parameter;
        try {
            parameter = getParameter(parameterId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.parametersByGenus.remove(parameter.getGenusType());

        try (org.osid.type.TypeList types = parameter.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.parametersByRecord.remove(types.getNextType(), parameter);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeParameter(parameterId);
        return;
    }


    /**
     *  Gets a <code>ParameterList</code> corresponding to the given
     *  parameter genus <code>Type</code> which does not include
     *  parameters of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known parameters or an error results. Otherwise,
     *  the returned list may contain only those parameters that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  parameterGenusType a parameter genus type 
     *  @return the returned <code>Parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByGenusType(org.osid.type.Type parameterGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.parameter.ArrayParameterList(this.parametersByGenus.get(parameterGenusType)));
    }


    /**
     *  Gets a <code>ParameterList</code> containing the given
     *  parameter record <code>Type</code>. In plenary mode, the
     *  returned list contains all known parameters or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameters that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return the returned <code>parameter</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParametersByRecordType(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.parameter.ArrayParameterList(this.parametersByRecord.get(parameterRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parametersByGenus.clear();
        this.parametersByRecord.clear();

        super.close();

        return;
    }
}
