//
// AbstractIndexedMapSubjectLookupSession.java
//
//    A simple framework for providing a Subject lookup service
//    backed by a fixed collection of subjects with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Subject lookup service backed by a
 *  fixed collection of subjects. The subjects are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some subjects may be compatible
 *  with more types than are indicated through these subject
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Subjects</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSubjectLookupSession
    extends AbstractMapSubjectLookupSession
    implements org.osid.ontology.SubjectLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ontology.Subject> subjectsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Subject>());
    private final MultiMap<org.osid.type.Type, org.osid.ontology.Subject> subjectsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Subject>());


    /**
     *  Makes a <code>Subject</code> available in this session.
     *
     *  @param  subject a subject
     *  @throws org.osid.NullArgumentException <code>subject<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSubject(org.osid.ontology.Subject subject) {
        super.putSubject(subject);

        this.subjectsByGenus.put(subject.getGenusType(), subject);
        
        try (org.osid.type.TypeList types = subject.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subjectsByRecord.put(types.getNextType(), subject);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a subject from this session.
     *
     *  @param subjectId the <code>Id</code> of the subject
     *  @throws org.osid.NullArgumentException <code>subjectId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSubject(org.osid.id.Id subjectId) {
        org.osid.ontology.Subject subject;
        try {
            subject = getSubject(subjectId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.subjectsByGenus.remove(subject.getGenusType());

        try (org.osid.type.TypeList types = subject.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.subjectsByRecord.remove(types.getNextType(), subject);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSubject(subjectId);
        return;
    }


    /**
     *  Gets a <code>SubjectList</code> corresponding to the given
     *  subject genus <code>Type</code> which does not include
     *  subjects of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known subjects or an error results. Otherwise,
     *  the returned list may contain only those subjects that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  subjectGenusType a subject genus type 
     *  @return the returned <code>Subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByGenusType(org.osid.type.Type subjectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.subject.ArraySubjectList(this.subjectsByGenus.get(subjectGenusType)));
    }


    /**
     *  Gets a <code>SubjectList</code> containing the given
     *  subject record <code>Type</code>. In plenary mode, the
     *  returned list contains all known subjects or an error
     *  results. Otherwise, the returned list may contain only those
     *  subjects that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  subjectRecordType a subject record type 
     *  @return the returned <code>subject</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjectsByRecordType(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.subject.ArraySubjectList(this.subjectsByRecord.get(subjectRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.subjectsByGenus.clear();
        this.subjectsByRecord.clear();

        super.close();

        return;
    }
}
