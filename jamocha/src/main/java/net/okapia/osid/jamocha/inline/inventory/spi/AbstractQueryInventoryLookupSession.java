//
// AbstractQueryInventoryLookupSession.java
//
//    An inline adapter that maps an InventoryLookupSession to
//    an InventoryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an InventoryLookupSession to
 *  an InventoryQuerySession.
 */

public abstract class AbstractQueryInventoryLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractInventoryLookupSession
    implements org.osid.inventory.InventoryLookupSession {

    private final org.osid.inventory.InventoryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryInventoryLookupSession.
     *
     *  @param querySession the underlying inventory query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryInventoryLookupSession(org.osid.inventory.InventoryQuerySession querySession) {
        nullarg(querySession, "inventory query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform <code>Inventory</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupInventories() {
        return (this.session.canSearchInventories());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include inventories in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the <code>Inventory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Inventory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Inventory</code> and
     *  retained for compatibility.
     *
     *  @param  inventoryId <code>Id</code> of the
     *          <code>Inventory</code>
     *  @return the inventory
     *  @throws org.osid.NotFoundException <code>inventoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>inventoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Inventory getInventory(org.osid.id.Id inventoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchId(inventoryId, true);
        org.osid.inventory.InventoryList inventories = this.session.getInventoriesByQuery(query);
        if (inventories.hasNext()) {
            return (inventories.getNextInventory());
        } 
        
        throw new org.osid.NotFoundException(inventoryId + " not found");
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  inventories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Inventories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  inventoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByIds(org.osid.id.IdList inventoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();

        try (org.osid.id.IdList ids = inventoryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> which does not include
     *  inventories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchGenusType(inventoryGenusType, true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets an <code>InventoryList</code> corresponding to the given
     *  inventory genus <code>Type</code> and include any additional
     *  inventories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryGenusType an inventory genus type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByParentGenusType(org.osid.type.Type inventoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchParentGenusType(inventoryGenusType, true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets an <code>InventoryList</code> containing the given
     *  inventory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  inventoryRecordType an inventory record type 
     *  @return the returned <code>Inventory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>inventoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByRecordType(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchRecordType(inventoryRecordType, true);
        return (this.session.getInventoriesByQuery(query));
    }

    
    /**
     *  Gets <code> Inventories </code> between the given date range
     *  inclusive <code> . </code> In plenary mode, the returned list
     *  contains all known inventories or an error results. Otherwise,
     *  the returned list may contain only those inventories that are
     *  accessible through this session.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Inventory </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDate(org.osid.calendaring.DateTime from, 
                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchDateInclusive(from, to, true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets all <code> Inventories </code> for the given <code>
     *  Stock.  </code> In plenary mode, the returned list contains
     *  all known inventories or an error results. Otherwise, the
     *  returned list may contain only those inventories that are
     *  accessible through this session.
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @return the returned <code> Inventory </code> list 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesForStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchStockId(stockId, true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets <code> Inventories </code> between the given date range
     *  inclusive for the given stock <code> . </code> In plenary
     *  mode, the returned list contains all known inventories or an
     *  error results. Otherwise, the returned list may contain only
     *  those inventories that are accessible through this session.
     *
     *  @param  stockId an inventory <code> Id </code> 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code> Inventory </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> stockid, from </code> or 
     *          <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventoriesByDateForStock(org.osid.id.Id stockId, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchStockId(stockId, true);
        query.matchDateInclusive(from, to, true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets all <code>Inventories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  inventories or an error results. Otherwise, the returned list
     *  may contain only those inventories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Inventories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.InventoryList getInventories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.InventoryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getInventoriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.InventoryQuery getQuery() {
        org.osid.inventory.InventoryQuery query = this.session.getInventoryQuery();
        return (query);
    }
}
