//
// ImmutableResourceVelocity.java
//
//     Wraps a mutable ResourceVelocity to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity;


/**
 *  Wraps a mutable <code>ResourceVelocity</code> to hide
 *  modifiers. This wrapper provides an immutized ResourceVelocity
 *  from the point of view external to the builder. Methods are passed
 *  through to the underlying resource velocity whose state changes
 *  are visible.
 */

public final class ImmutableResourceVelocity
    extends net.okapia.osid.jamocha.builder.mapping.path.resourcevelocity.spi.AbstractImmutableResourceVelocity
    implements org.osid.mapping.path.ResourceVelocity {


    /**
     *  Constructs a new <code>ImmutableResourceVelocity</code>.
     *
     *  @param resourceVelocity the resource velocity
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocity</code> is <code>null</code>
     */

    public ImmutableResourceVelocity(org.osid.mapping.path.ResourceVelocity resourceVelocity) {
        super(resourceVelocity);
        return;
    }
}

