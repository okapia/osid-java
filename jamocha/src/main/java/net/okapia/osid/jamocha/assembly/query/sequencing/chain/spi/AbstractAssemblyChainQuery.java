//
// AbstractAssemblyChainQuery.java
//
//     A ChainQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ChainQuery that stores terms.
 */

public abstract class AbstractAssemblyChainQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.sequencing.ChainQuery,
               org.osid.sequencing.ChainQueryInspector,
               org.osid.sequencing.ChainSearchOrder {

    private final java.util.Collection<org.osid.sequencing.records.ChainQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.ChainQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.ChainSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyChainQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyChainQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches fifo chains. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFifo(boolean match) {
        getAssembler().addBooleanTerm(getFifoColumn(), match);
        return;
    }


    /**
     *  Clears the fifo query terms. 
     */

    @OSID @Override
    public void clearFifoTerms() {
        getAssembler().clearTerms(getFifoColumn());
        return;
    }


    /**
     *  Gets the fifo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getFifoTerms() {
        return (getAssembler().getBooleanTerms(getFifoColumn()));
    }


    /**
     *  Orders the results by the fifo flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFifo(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFifoColumn(), style);
        return;
    }


    /**
     *  Gets the Fifo column name.
     *
     * @return the column name
     */

    protected String getFifoColumn() {
        return ("fifo");
    }


    /**
     *  Sets the element <code> Id </code> for this query. 
     *
     *  @param  elementId the element <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> elementId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchElement(org.osid.id.Id elementId, boolean match) {
        getAssembler().addIdTerm(getElementColumn(), elementId, match);
        return;
    }


    /**
     *  Matches chains with any element. 
     *
     *  @param match <code> true </code>to match chains with any
     *          element <code>false</code> to match chains with no
     *          element
     */

    @OSID @Override
    public void matchAnyElement(boolean match) {
        getAssembler().addBooleanTerm(getElementColumn(), match);
        return;
    }


    /**
     *  Clears the element query terms. 
     */

    @OSID @Override
    public void clearElementTerms() {
        getAssembler().clearTerms(getElementColumn());
        return;
    }


    /**
     *  Gets the element <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getElementTerms() {
        return (getAssembler().getIdTerms(getElementColumn()));
    }


    /**
     *  Gets the Element column name.
     *
     * @return the column name
     */

    protected String getElementColumn() {
        return ("element");
    }


    /**
     *  Sets the action group <code> Id </code> for this query to match 
     *  sequencinglers assigned to action groups. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAntimatroidId(org.osid.id.Id actionGroupId, boolean match) {
        getAssembler().addIdTerm(getAntimatroidIdColumn(), actionGroupId, match);
        return;
    }


    /**
     *  Clears the antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAntimatroidIdTerms() {
        getAssembler().clearTerms(getAntimatroidIdColumn());
        return;
    }


    /**
     *  Gets the antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAntimatroidIdTerms() {
        return (getAssembler().getIdTerms(getAntimatroidIdColumn()));
    }


    /**
     *  Gets the AntimatroidId column name.
     *
     * @return the column name
     */

    protected String getAntimatroidIdColumn() {
        return ("antimatroid_id");
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsAntimatroidQuery() is false");
    }


    /**
     *  Clears the antimatroid query terms. 
     */

    @OSID @Override
    public void clearAntimatroidTerms() {
        getAssembler().clearTerms(getAntimatroidColumn());
        return;
    }


    /**
     *  Gets the antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }


    /**
     *  Gets the Antimatroid column name.
     *
     * @return the column name
     */

    protected String getAntimatroidColumn() {
        return ("antimatroid");
    }


    /**
     *  Tests if this chain supports the given record
     *  <code>Type</code>.
     *
     *  @param  chainRecordType a chain record type 
     *  @return <code>true</code> if the chainRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type chainRecordType) {
        for (org.osid.sequencing.records.ChainQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(chainRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  chainRecordType the chain record type 
     *  @return the chain query record 
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainQueryRecord getChainQueryRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  chainRecordType the chain record type 
     *  @return the chain query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainQueryInspectorRecord getChainQueryInspectorRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param chainRecordType the chain record type
     *  @return the chain search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainSearchOrderRecord getChainSearchOrderRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this chain. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param chainQueryRecord the chain query record
     *  @param chainQueryInspectorRecord the chain query inspector
     *         record
     *  @param chainSearchOrderRecord the chain search order record
     *  @param chainRecordType chain record type
     *  @throws org.osid.NullArgumentException
     *          <code>chainQueryRecord</code>,
     *          <code>chainQueryInspectorRecord</code>,
     *          <code>chainSearchOrderRecord</code> or
     *          <code>chainRecordTypechain</code> is
     *          <code>null</code>
     */
            
    protected void addChainRecords(org.osid.sequencing.records.ChainQueryRecord chainQueryRecord, 
                                      org.osid.sequencing.records.ChainQueryInspectorRecord chainQueryInspectorRecord, 
                                      org.osid.sequencing.records.ChainSearchOrderRecord chainSearchOrderRecord, 
                                      org.osid.type.Type chainRecordType) {

        addRecordType(chainRecordType);

        nullarg(chainQueryRecord, "chain query record");
        nullarg(chainQueryInspectorRecord, "chain query inspector record");
        nullarg(chainSearchOrderRecord, "chain search odrer record");

        this.queryRecords.add(chainQueryRecord);
        this.queryInspectorRecords.add(chainQueryInspectorRecord);
        this.searchOrderRecords.add(chainSearchOrderRecord);
        
        return;
    }
}
