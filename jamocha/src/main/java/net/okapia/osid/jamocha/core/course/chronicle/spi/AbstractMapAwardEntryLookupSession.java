//
// AbstractMapAwardEntryLookupSession
//
//    A simple framework for providing an AwardEntry lookup service
//    backed by a fixed collection of award entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AwardEntry lookup service backed by a
 *  fixed collection of award entries. The award entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AwardEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAwardEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractAwardEntryLookupSession
    implements org.osid.course.chronicle.AwardEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.chronicle.AwardEntry> awardEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.chronicle.AwardEntry>());


    /**
     *  Makes an <code>AwardEntry</code> available in this session.
     *
     *  @param  awardEntry an award entry
     *  @throws org.osid.NullArgumentException <code>awardEntry<code>
     *          is <code>null</code>
     */

    protected void putAwardEntry(org.osid.course.chronicle.AwardEntry awardEntry) {
        this.awardEntries.put(awardEntry.getId(), awardEntry);
        return;
    }


    /**
     *  Makes an array of award entries available in this session.
     *
     *  @param  awardEntries an array of award entries
     *  @throws org.osid.NullArgumentException <code>awardEntries<code>
     *          is <code>null</code>
     */

    protected void putAwardEntries(org.osid.course.chronicle.AwardEntry[] awardEntries) {
        putAwardEntries(java.util.Arrays.asList(awardEntries));
        return;
    }


    /**
     *  Makes a collection of award entries available in this session.
     *
     *  @param  awardEntries a collection of award entries
     *  @throws org.osid.NullArgumentException <code>awardEntries<code>
     *          is <code>null</code>
     */

    protected void putAwardEntries(java.util.Collection<? extends org.osid.course.chronicle.AwardEntry> awardEntries) {
        for (org.osid.course.chronicle.AwardEntry awardEntry : awardEntries) {
            this.awardEntries.put(awardEntry.getId(), awardEntry);
        }

        return;
    }


    /**
     *  Removes an AwardEntry from this session.
     *
     *  @param  awardEntryId the <code>Id</code> of the award entry
     *  @throws org.osid.NullArgumentException <code>awardEntryId<code> is
     *          <code>null</code>
     */

    protected void removeAwardEntry(org.osid.id.Id awardEntryId) {
        this.awardEntries.remove(awardEntryId);
        return;
    }


    /**
     *  Gets the <code>AwardEntry</code> specified by its <code>Id</code>.
     *
     *  @param  awardEntryId <code>Id</code> of the <code>AwardEntry</code>
     *  @return the awardEntry
     *  @throws org.osid.NotFoundException <code>awardEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>awardEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntry getAwardEntry(org.osid.id.Id awardEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.AwardEntry awardEntry = this.awardEntries.get(awardEntryId);
        if (awardEntry == null) {
            throw new org.osid.NotFoundException("awardEntry not found: " + awardEntryId);
        }

        return (awardEntry);
    }


    /**
     *  Gets all <code>AwardEntries</code>. In plenary mode, the returned
     *  list contains all known awardEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  awardEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AwardEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.awardentry.ArrayAwardEntryList(this.awardEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.awardEntries.clear();
        super.close();
        return;
    }
}
