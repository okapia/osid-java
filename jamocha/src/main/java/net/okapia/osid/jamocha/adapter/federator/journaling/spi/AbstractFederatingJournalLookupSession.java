//
// AbstractFederatingJournalLookupSession.java
//
//     An abstract federating adapter for a JournalLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  JournalLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingJournalLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.journaling.JournalLookupSession>
    implements org.osid.journaling.JournalLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingJournalLookupSession</code>.
     */

    protected AbstractFederatingJournalLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.journaling.JournalLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Journal</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJournals() {
        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            if (session.canLookupJournals()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Journal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJournalView() {
        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            session.useComparativeJournalView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Journal</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJournalView() {
        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            session.usePlenaryJournalView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Journal</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Journal</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Journal</code> and
     *  retained for compatibility.
     *
     *  @param  journalId <code>Id</code> of the
     *          <code>Journal</code>
     *  @return the journal
     *  @throws org.osid.NotFoundException <code>journalId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>journalId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            try {
                return (session.getJournal(journalId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(journalId + " not found");
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  journals specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Journals</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  journalIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>journalIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByIds(org.osid.id.IdList journalIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.journaling.journal.MutableJournalList ret = new net.okapia.osid.jamocha.journaling.journal.MutableJournalList();

        try (org.osid.id.IdList ids = journalIds) {
            while (ids.hasNext()) {
                ret.addJournal(getJournal(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> which does not include
     *  journals of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList ret = getJournalList();

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            ret.addJournalList(session.getJournalsByGenusType(journalGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> and include any additional
     *  journals with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByParentGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList ret = getJournalList();

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            ret.addJournalList(session.getJournalsByParentGenusType(journalGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JournalList</code> containing the given
     *  journal record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  journalRecordType a journal record type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByRecordType(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList ret = getJournalList();

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            ret.addJournalList(session.getJournalsByRecordType(journalRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JournalList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known journals or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  journals that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Journal</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList ret = getJournalList();

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            ret.addJournalList(session.getJournalsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Journals</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  journals or an error results. Otherwise, the returned list
     *  may contain only those journals that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Journals</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournals()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList ret = getJournalList();

        for (org.osid.journaling.JournalLookupSession session : getSessions()) {
            ret.addJournalList(session.getJournals());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.journaling.journal.FederatingJournalList getJournalList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.journal.ParallelJournalList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.journaling.journal.CompositeJournalList());
        }
    }
}
