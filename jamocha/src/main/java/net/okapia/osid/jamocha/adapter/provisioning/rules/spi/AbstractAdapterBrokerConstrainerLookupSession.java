//
// AbstractAdapterBrokerConstrainerLookupSession.java
//
//    A BrokerConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A BrokerConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterBrokerConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.BrokerConstrainerLookupSession {

    private final org.osid.provisioning.rules.BrokerConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBrokerConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBrokerConstrainerLookupSession(org.osid.provisioning.rules.BrokerConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code BrokerConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBrokerConstrainers() {
        return (this.session.canLookupBrokerConstrainers());
    }


    /**
     *  A complete view of the {@code BrokerConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerConstrainerView() {
        this.session.useComparativeBrokerConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code BrokerConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerConstrainerView() {
        this.session.usePlenaryBrokerConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker constrainers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active broker constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerConstrainerView() {
        this.session.useActiveBrokerConstrainerView();
        return;
    }


    /**
     *  Active and inactive broker constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerConstrainerView() {
        this.session.useAnyStatusBrokerConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code BrokerConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code BrokerConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code BrokerConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @param brokerConstrainerId {@code Id} of the {@code BrokerConstrainer}
     *  @return the broker constrainer
     *  @throws org.osid.NotFoundException {@code brokerConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainer getBrokerConstrainer(org.osid.id.Id brokerConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainer(brokerConstrainerId));
    }


    /**
     *  Gets a {@code BrokerConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code BrokerConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @param  brokerConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code BrokerConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByIds(org.osid.id.IdList brokerConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainersByIds(brokerConstrainerIds));
    }


    /**
     *  Gets a {@code BrokerConstrainerList} corresponding to the given
     *  broker constrainer genus {@code Type} which does not include
     *  broker constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @param  brokerConstrainerGenusType a brokerConstrainer genus type 
     *  @return the returned {@code BrokerConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByGenusType(org.osid.type.Type brokerConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainersByGenusType(brokerConstrainerGenusType));
    }


    /**
     *  Gets a {@code BrokerConstrainerList} corresponding to the given
     *  broker constrainer genus {@code Type} and include any additional
     *  broker constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @param  brokerConstrainerGenusType a brokerConstrainer genus type 
     *  @return the returned {@code BrokerConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByParentGenusType(org.osid.type.Type brokerConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainersByParentGenusType(brokerConstrainerGenusType));
    }


    /**
     *  Gets a {@code BrokerConstrainerList} containing the given
     *  broker constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  broker constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @param  brokerConstrainerRecordType a brokerConstrainer record type 
     *  @return the returned {@code BrokerConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code brokerConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainersByRecordType(org.osid.type.Type brokerConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainersByRecordType(brokerConstrainerRecordType));
    }


    /**
     *  Gets all {@code BrokerConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  broker constrainers or an error results. Otherwise, the returned list
     *  may contain only those broker constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, broker constrainers are returned that are currently
     *  active. In any status mode, active and inactive broker constrainers
     *  are returned.
     *
     *  @return a list of {@code BrokerConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBrokerConstrainers());
    }
}
