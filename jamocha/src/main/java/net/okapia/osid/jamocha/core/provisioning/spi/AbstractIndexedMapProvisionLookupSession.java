//
// AbstractIndexedMapProvisionLookupSession.java
//
//    A simple framework for providing a Provision lookup service
//    backed by a fixed collection of provisions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Provision lookup service backed by a
 *  fixed collection of provisions. The provisions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some provisions may be compatible
 *  with more types than are indicated through these provision
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Provisions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProvisionLookupSession
    extends AbstractMapProvisionLookupSession
    implements org.osid.provisioning.ProvisionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Provision> provisionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Provision>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.Provision> provisionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.Provision>());


    /**
     *  Makes a <code>Provision</code> available in this session.
     *
     *  @param  provision a provision
     *  @throws org.osid.NullArgumentException <code>provision<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProvision(org.osid.provisioning.Provision provision) {
        super.putProvision(provision);

        this.provisionsByGenus.put(provision.getGenusType(), provision);
        
        try (org.osid.type.TypeList types = provision.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.provisionsByRecord.put(types.getNextType(), provision);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a provision from this session.
     *
     *  @param provisionId the <code>Id</code> of the provision
     *  @throws org.osid.NullArgumentException <code>provisionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProvision(org.osid.id.Id provisionId) {
        org.osid.provisioning.Provision provision;
        try {
            provision = getProvision(provisionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.provisionsByGenus.remove(provision.getGenusType());

        try (org.osid.type.TypeList types = provision.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.provisionsByRecord.remove(types.getNextType(), provision);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProvision(provisionId);
        return;
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> which does not include
     *  provisions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known provisions or an error results. Otherwise,
     *  the returned list may contain only those provisions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provision.ArrayProvisionList(this.provisionsByGenus.get(provisionGenusType)));
    }


    /**
     *  Gets a <code>ProvisionList</code> containing the given
     *  provision record <code>Type</code>. In plenary mode, the
     *  returned list contains all known provisions or an error
     *  results. Otherwise, the returned list may contain only those
     *  provisions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return the returned <code>provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByRecordType(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.provision.ArrayProvisionList(this.provisionsByRecord.get(provisionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.provisionsByGenus.clear();
        this.provisionsByRecord.clear();

        super.close();

        return;
    }
}
