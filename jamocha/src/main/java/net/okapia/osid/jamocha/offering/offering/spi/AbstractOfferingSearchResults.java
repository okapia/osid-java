//
// AbstractOfferingSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOfferingSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.OfferingSearchResults {

    private org.osid.offering.OfferingList offerings;
    private final org.osid.offering.OfferingQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.records.OfferingSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOfferingSearchResults.
     *
     *  @param offerings the result set
     *  @param offeringQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offerings</code>
     *          or <code>offeringQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOfferingSearchResults(org.osid.offering.OfferingList offerings,
                                            org.osid.offering.OfferingQueryInspector offeringQueryInspector) {
        nullarg(offerings, "offerings");
        nullarg(offeringQueryInspector, "offering query inspectpr");

        this.offerings = offerings;
        this.inspector = offeringQueryInspector;

        return;
    }


    /**
     *  Gets the offering list resulting from a search.
     *
     *  @return an offering list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferings() {
        if (this.offerings == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.OfferingList offerings = this.offerings;
        this.offerings = null;
	return (offerings);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.OfferingQueryInspector getOfferingQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  offering search record <code> Type. </code> This method must
     *  be used to retrieve an offering implementing the requested
     *  record.
     *
     *  @param offeringSearchRecordType an offering search 
     *         record type 
     *  @return the offering search
     *  @throws org.osid.NullArgumentException
     *          <code>offeringSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(offeringSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.OfferingSearchResultsRecord getOfferingSearchResultsRecord(org.osid.type.Type offeringSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.records.OfferingSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(offeringSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(offeringSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record offering search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOfferingRecord(org.osid.offering.records.OfferingSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "offering record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
