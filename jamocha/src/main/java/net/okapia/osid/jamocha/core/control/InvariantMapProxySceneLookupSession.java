//
// InvariantMapProxySceneLookupSession
//
//    Implements a Scene lookup service backed by a fixed
//    collection of scenes. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Scene lookup service backed by a fixed
 *  collection of scenes. The scenes are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySceneLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapSceneLookupSession
    implements org.osid.control.SceneLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySceneLookupSession} with no
     *  scenes.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySceneLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySceneLookupSession} with a single
     *  scene.
     *
     *  @param system the system
     *  @param scene a single scene
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code scene} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySceneLookupSession(org.osid.control.System system,
                                                  org.osid.control.Scene scene, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putScene(scene);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySceneLookupSession} using
     *  an array of scenes.
     *
     *  @param system the system
     *  @param scenes an array of scenes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code scenes} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySceneLookupSession(org.osid.control.System system,
                                                  org.osid.control.Scene[] scenes, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putScenes(scenes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySceneLookupSession} using a
     *  collection of scenes.
     *
     *  @param system the system
     *  @param scenes a collection of scenes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code scenes} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySceneLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.Scene> scenes,
                                                  org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putScenes(scenes);
        return;
    }
}
