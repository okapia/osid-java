//
// AbstractAssemblyPackageQuery.java
//
//     A PackageQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PackageQuery that stores terms.
 */

public abstract class AbstractAssemblyPackageQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblySourceableOsidObjectQuery
    implements org.osid.installation.PackageQuery,
               org.osid.installation.PackageQueryInspector,
               org.osid.installation.PackageSearchOrder {

    private final java.util.Collection<org.osid.installation.records.PackageQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.PackageQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.PackageSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPackageQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPackageQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches the version string. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException version type not supported 
     */

    @OSID @Override
    public void matchVersion(org.osid.installation.Version version, 
                             boolean match) {
        getAssembler().addVersionTerm(getVersionColumn(), version, match);
        return;
    }


    /**
     *  Matches packages with any version. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyVersion(boolean match) {
        getAssembler().addVersionWildcardTerm(getVersionColumn(), match);
        return;
    }


    /**
     *  Clears the version query terms. 
     */

    @OSID @Override
    public void clearVersionTerms() {
        getAssembler().clearTerms(getVersionColumn());
        return;
    }


    /**
     *  Gets the version query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionTerms() {
        return (getAssembler().getVersionTerms(getVersionColumn()));
    }


    /**
     *  Specified a preference for ordering results by the version. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByVersion(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getVersionColumn(), style);
        return;
    }


    /**
     *  Gets the Version column name.
     *
     * @return the column name
     */

    protected String getVersionColumn() {
        return ("version");
    }


    /**
     *  Matches packags with versions including and more recent than the given 
     *  version. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException version type not supported 
     */

    @OSID @Override
    public void matchVersionSince(org.osid.installation.Version version, 
                                  boolean match) {
        getAssembler().addVersionTerm(getVersionSinceColumn(), version, match);
        return;
    }


    /**
     *  Clears the version since query terms. 
     */

    @OSID @Override
    public void clearVersionSinceTerms() {
        getAssembler().clearTerms(getVersionSinceColumn());
        return;
    }


    /**
     *  Gets the version since terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.VersionTerm[] getVersionSinceTerms() {
        return (getAssembler().getVersionTerms(getVersionSinceColumn()));
    }


    /**
     *  Gets the VersionSince column name.
     *
     * @return the column name
     */

    protected String getVersionSinceColumn() {
        return ("version_since");
    }


    /**
     *  Matches the copyright string. 
     *
     *  @param  copyright copyright string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> copyright </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> copyright </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyright(String copyright, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        getAssembler().addStringTerm(getCopyrightColumn(), copyright, stringMatchType, match);
        return;
    }


    /**
     *  Matches packages with any copyright. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCopyright(boolean match) {
        getAssembler().addStringWildcardTerm(getCopyrightColumn(), match);
        return;
    }


    /**
     *  Clears the copyright query terms. 
     */

    @OSID @Override
    public void clearCopyrightTerms() {
        getAssembler().clearTerms(getCopyrightColumn());
        return;
    }


    /**
     *  Gets the copyright terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCopyrightTerms() {
        return (getAssembler().getStringTerms(getCopyrightColumn()));
    }


    /**
     *  Specified a preference for ordering results by the copyright. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCopyright(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCopyrightColumn(), style);
        return;
    }


    /**
     *  Gets the Copyright column name.
     *
     * @return the column name
     */

    protected String getCopyrightColumn() {
        return ("copyright");
    }


    /**
     *  Matches packages that require license acknowledgement. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequiresLicenseAcknowledgement(boolean match) {
        getAssembler().addBooleanTerm(getRequiresLicenseAcknowledgementColumn(), match);
        return;
    }


    /**
     *  Matches packages that have any acknowledgement value. 
     *
     *  @param  match <code> true </code> to match packages that have any 
     *          acknowledgement value, <code> false </code> for to match 
     *          packages that have no value 
     */

    @OSID @Override
    public void matchAnyRequiresLicenseAcknowledgement(boolean match) {
        getAssembler().addBooleanWildcardTerm(getRequiresLicenseAcknowledgementColumn(), match);
        return;
    }


    /**
     *  Clears the license acknowledgement query terms. 
     */

    @OSID @Override
    public void clearRequiresLicenseAcknowledgementTerms() {
        getAssembler().clearTerms(getRequiresLicenseAcknowledgementColumn());
        return;
    }


    /**
     *  Gets the requires license acknowledgement terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiresLicenseAcknowledgementTerms() {
        return (getAssembler().getBooleanTerms(getRequiresLicenseAcknowledgementColumn()));
    }


    /**
     *  Specified a preference for ordering results by the license 
     *  acknowledgement flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresLicenseAcknowledgement(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiresLicenseAcknowledgementColumn(), style);
        return;
    }


    /**
     *  Gets the RequiresLicenseAcknowledgement column name.
     *
     * @return the column name
     */

    protected String getRequiresLicenseAcknowledgementColumn() {
        return ("requires_license_acknowledgement");
    }


    /**
     *  Sets the creator resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCreatorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatorIdTerms() {
        getAssembler().clearTerms(getCreatorIdColumn());
        return;
    }


    /**
     *  Gets the creator <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreatorIdTerms() {
        return (getAssembler().getIdTerms(getCreatorIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the creator. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreator(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreatorColumn(), style);
        return;
    }


    /**
     *  Gets the CreatorId column name.
     *
     * @return the column name
     */

    protected String getCreatorIdColumn() {
        return ("creator_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  creators. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the creator resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCreatorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCreatorQuery() {
        throw new org.osid.UnimplementedException("supportsCreatorQuery() is false");
    }


    /**
     *  Matches packages with any creator. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCreator(boolean match) {
        getAssembler().addIdWildcardTerm(getCreatorColumn(), match);
        return;
    }


    /**
     *  Clears the creator query terms. 
     */

    @OSID @Override
    public void clearCreatorTerms() {
        getAssembler().clearTerms(getCreatorColumn());
        return;
    }


    /**
     *  Gets the creator query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCreatorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ResourceSearchOrder </code> is available for creator 
     *  resources. 
     *
     *  @return <code> true </code> if a creator resource search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a creator resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreatorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCreatorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreatorSearchOrder() is false");
    }


    /**
     *  Gets the Creator column name.
     *
     * @return the column name
     */

    protected String getCreatorColumn() {
        return ("creator");
    }


    /**
     *  Matches the release date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchReleaseDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getReleaseDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches packages that have any release date. 
     *
     *  @param  match <code> true </code> to match packages with any release 
     *          date, <code> false </code> to match packages with no release 
     *          date 
     */

    @OSID @Override
    public void matchAnyReleaseDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getReleaseDateColumn(), match);
        return;
    }


    /**
     *  Clears the release date query terms. 
     */

    @OSID @Override
    public void clearReleaseDateTerms() {
        getAssembler().clearTerms(getReleaseDateColumn());
        return;
    }


    /**
     *  Gets the release date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReleaseDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getReleaseDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the release date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReleaseDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReleaseDateColumn(), style);
        return;
    }


    /**
     *  Gets the ReleaseDate column name.
     *
     * @return the column name
     */

    protected String getReleaseDateColumn() {
        return ("release_date");
    }


    /**
     *  Sets the package <code> Id </code> to match packages on which a 
     *  package depends. 
     *
     *  @param  packageId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDependencyId(org.osid.id.Id packageId, boolean match) {
        getAssembler().addIdTerm(getDependencyIdColumn(), packageId, match);
        return;
    }


    /**
     *  Clears the dependency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDependencyIdTerms() {
        getAssembler().clearTerms(getDependencyIdColumn());
        return;
    }


    /**
     *  Gets the package dependency <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependencyIdTerms() {
        return (getAssembler().getIdTerms(getDependencyIdColumn()));
    }


    /**
     *  Gets the DependencyId column name.
     *
     * @return the column name
     */

    protected String getDependencyIdColumn() {
        return ("dependency_id");
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getDependencyQuery() {
        throw new org.osid.UnimplementedException("supportsDependencyQuery() is false");
    }


    /**
     *  Matches packages that have any dependency. 
     *
     *  @param  match <code> true </code> to match packages with any 
     *          dependency, <code> false </code> to match packages with no 
     *          dependencies 
     */

    @OSID @Override
    public void matchAnyDependency(boolean match) {
        getAssembler().addIdWildcardTerm(getDependencyColumn(), match);
        return;
    }


    /**
     *  Clears the dependency query terms. 
     */

    @OSID @Override
    public void clearDependencyTerms() {
        getAssembler().clearTerms(getDependencyColumn());
        return;
    }


    /**
     *  Gets the package dependency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getDependencyTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the Dependency column name.
     *
     * @return the column name
     */

    protected String getDependencyColumn() {
        return ("dependency");
    }


    /**
     *  Sets the url for this query. Supplying multiple strings behaves like a 
     *  boolean <code> OR </code> among the elements each which must 
     *  correspond to the <code> stringMatchType. </code> 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(url) </code> is <code> false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        getAssembler().addStringTerm(getURLColumn(), url, stringMatchType, match);
        return;
    }


    /**
     *  Matches packages that have any url. 
     *
     *  @param  match <code> true </code> to match packages with any url, 
     *          <code> false </code> to match packages with no url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        getAssembler().addStringWildcardTerm(getURLColumn(), match);
        return;
    }


    /**
     *  Clears the url query terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        getAssembler().clearTerms(getURLColumn());
        return;
    }


    /**
     *  Gets the url terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (getAssembler().getStringTerms(getURLColumn()));
    }


    /**
     *  Specified a preference for ordering results by the url. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByURL(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getURLColumn(), style);
        return;
    }


    /**
     *  Gets the URL column name.
     *
     * @return the column name
     */

    protected String getURLColumn() {
        return ("u_rl");
    }


    /**
     *  Sets the installation <code> Id </code> for this query. 
     *
     *  @param  installationId an installation <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> installationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchInstallationId(org.osid.id.Id installationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getInstallationIdColumn(), installationId, match);
        return;
    }


    /**
     *  Clears the installation <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstallationIdTerms() {
        getAssembler().clearTerms(getInstallationIdColumn());
        return;
    }


    /**
     *  Gets the installation <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationIdTerms() {
        return (getAssembler().getIdTerms(getInstallationIdColumn()));
    }


    /**
     *  Gets the InstallationId column name.
     *
     * @return the column name
     */

    protected String getInstallationIdColumn() {
        return ("installation_id");
    }


    /**
     *  Tests if an <code> InstallationQuery </code> is available. 
     *
     *  @return <code> true </code> if an installation query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an installation. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the installation query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuery getInstallationQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationQuery() is false");
    }


    /**
     *  Matches any packages that are installed. 
     *
     *  @param  match <code> true </code> to match installed packages, <code> 
     *          false </code> for uninstalled packages 
     */

    @OSID @Override
    public void matchAnyInstallation(boolean match) {
        getAssembler().addIdWildcardTerm(getInstallationColumn(), match);
        return;
    }


    /**
     *  Clears the installation query terms. 
     */

    @OSID @Override
    public void clearInstallationTerms() {
        getAssembler().clearTerms(getInstallationColumn());
        return;
    }


    /**
     *  Gets the installation query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationQueryInspector[] getInstallationTerms() {
        return (new org.osid.installation.InstallationQueryInspector[0]);
    }


    /**
     *  Gets the Installation column name.
     *
     * @return the column name
     */

    protected String getInstallationColumn() {
        return ("installation");
    }


    /**
     *  Sets the package <code> Id </code> to match packages on which other 
     *  packages depend. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDependentId(org.osid.id.Id packageId, boolean match) {
        getAssembler().addIdTerm(getDependentIdColumn(), packageId, match);
        return;
    }


    /**
     *  Clears the dependent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDependentIdTerms() {
        getAssembler().clearTerms(getDependentIdColumn());
        return;
    }


    /**
     *  Gets the dependent package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependentIdTerms() {
        return (getAssembler().getIdTerms(getDependentIdColumn()));
    }


    /**
     *  Gets the DependentId column name.
     *
     * @return the column name
     */

    protected String getDependentIdColumn() {
        return ("dependent_id");
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getDependentQuery() {
        throw new org.osid.UnimplementedException("supportsDependentQuery() is false");
    }


    /**
     *  Matches packages that have any depenents. 
     *
     *  @param  match <code> true </code> to match packages with any 
     *          dependents, <code> false </code> to match packages with no 
     *          dependents 
     */

    @OSID @Override
    public void matchAnyDependent(boolean match) {
        getAssembler().addIdWildcardTerm(getDependentColumn(), match);
        return;
    }


    /**
     *  Clears the dependent query terms. 
     */

    @OSID @Override
    public void clearDependentTerms() {
        getAssembler().clearTerms(getDependentColumn());
        return;
    }


    /**
     *  Gets the dependent package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getDependentTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the Dependent column name.
     *
     * @return the column name
     */

    protected String getDependentColumn() {
        return ("dependent");
    }


    /**
     *  Sets the package <code> Id </code> to match packages in the version 
     *  chain. 
     *
     *  @param  packageId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionedPackageId(org.osid.id.Id packageId, 
                                        boolean match) {
        getAssembler().addIdTerm(getVersionedPackageIdColumn(), packageId, match);
        return;
    }


    /**
     *  Clears the versioned package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVersionedPackageIdTerms() {
        getAssembler().clearTerms(getVersionedPackageIdColumn());
        return;
    }


    /**
     *  Gets the versioned package <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVersionedPackageIdTerms() {
        return (getAssembler().getIdTerms(getVersionedPackageIdColumn()));
    }


    /**
     *  Gets the VersionedPackageId column name.
     *
     * @return the column name
     */

    protected String getVersionedPackageIdColumn() {
        return ("versioned_package_id");
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVersionedPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a version chain. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsVersionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getVersionedPackageQuery() {
        throw new org.osid.UnimplementedException("supportsVersionedPackageQuery() is false");
    }


    /**
     *  Matches packages that have any versions. 
     *
     *  @param  match <code> true </code> to match packages with any versions, 
     *          <code> false </code> to match packages with no versions 
     */

    @OSID @Override
    public void matchAnyVersionedPackage(boolean match) {
        getAssembler().addIdWildcardTerm(getVersionedPackageColumn(), match);
        return;
    }


    /**
     *  Clears the versioned package query terms. 
     */

    @OSID @Override
    public void clearVersionedPackageTerms() {
        getAssembler().clearTerms(getVersionedPackageColumn());
        return;
    }


    /**
     *  Gets the versioned package query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.PackageQueryInspector[] getVersionedPackageTerms() {
        return (new org.osid.installation.PackageQueryInspector[0]);
    }


    /**
     *  Gets the VersionedPackage column name.
     *
     * @return the column name
     */

    protected String getVersionedPackageColumn() {
        return ("versioned_package");
    }


    /**
     *  Sets the installation content <code> Id </code>.
     *
     *  @param  installationContentId a state <code> Id </code> 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> stateId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchInstallationContentId(org.osid.id.Id installationContentId, boolean match) {
        getAssembler().addIdTerm(getInstallationContentIdColumn(), installationContentId, match);
        return;
    }


    /**
     *  Clears the installation content <code> Id </code> query terms.
     */

    @OSID @Override
    public void clearInstallationContentIdTerms() {
        getAssembler().clearTerms(getInstallationContentIdColumn());
        return;
    }


    /**
     *  Gets the installation content <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationContentIdTerms() {
        return (getAssembler().getIdTerms(getInstallationContentIdColumn()));
    }


    /**
     *  Gets the InstallationContentId column name.
     *
     * @return the column name
     */

    protected String getInstallationContentIdColumn() {
        return ("installation_content_id");
    }


    /**
     *  Tests if a <code> InstallationQuery </code> is available. 
     *
     *  @return <code> true </code> if an installation content query
     *          is available, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsInstallationContentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an installation content. Multiple
     *  retrievals produce a nested <code> OR </code> term.
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsInstallationContentQuery() </code> is <code>
     *          false </code>
     */

    @OSID @Override
    public org.osid.installation.InstallationContentQuery getInstallationContentQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationContentQuery() is false");
    }


    /**
     *  Matches packages that have any installation content. 
     *
     *  @param match <code> true </code> to match packages with any
     *          content, <code> false </code> to match packages with
     *          no content
     */

    @OSID @Override
    public void matchAnyInstallationContent(boolean match) {
        getAssembler().addIdWildcardTerm(getInstallationContentColumn(), match);
        return;
    }


    /**
     *  Clears the dependency query terms. 
     */

    @OSID @Override
    public void clearInstallationContentTerms() {
        getAssembler().clearTerms(getInstallationContentColumn());
        return;
    }


    /**
     *  Gets the package dependency query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationContentQueryInspector[] getInstallationContentTerms() {
        return (new org.osid.installation.InstallationContentQueryInspector[0]);
    }


    /**
     *  Gets the InstallationContent column name.
     *
     * @return the column name
     */

    protected String getInstallationContentColumn() {
        return ("installation_content");
    }


    /**
     *  Sets the depot <code> Id </code> for this query. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDepotId(org.osid.id.Id depotId, boolean match) {
        getAssembler().addIdTerm(getDepotIdColumn(), depotId, match);
        return;
    }


    /**
     *  Clears the depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDepotIdTerms() {
        getAssembler().clearTerms(getDepotIdColumn());
        return;
    }


    /**
     *  Gets the depot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDepotIdTerms() {
        return (getAssembler().getIdTerms(getDepotIdColumn()));
    }


    /**
     *  Gets the DepotId column name.
     *
     * @return the column name
     */

    protected String getDepotIdColumn() {
        return ("depot_id");
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> supportsDepotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getDepotQuery() {
        throw new org.osid.UnimplementedException("supportsDepotQuery() is false");
    }


    /**
     *  Clears the depot query terms. 
     */

    @OSID @Override
    public void clearDepotTerms() {
        getAssembler().clearTerms(getDepotColumn());
        return;
    }


    /**
     *  Gets the depot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.DepotQueryInspector[] getDepotTerms() {
        return (new org.osid.installation.DepotQueryInspector[0]);
    }


    /**
     *  Gets the Depot column name.
     *
     * @return the column name
     */

    protected String getDepotColumn() {
        return ("depot");
    }


    /**
     *  Tests if this pkg supports the given record
     *  <code>Type</code>.
     *
     *  @param  pkgRecordType a package record type 
     *  @return <code>true</code> if the pkgRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pkgRecordType) {
        for (org.osid.installation.records.PackageQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  pkgRecordType the package record type 
     *  @return the package query record 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageQueryRecord getPackageQueryRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  pkgRecordType the package record type 
     *  @return the package query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageQueryInspectorRecord getPackageQueryInspectorRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param pkgRecordType the package record type
     *  @return the package search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageSearchOrderRecord getPackageSearchOrderRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this package. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pkgQueryRecord the package query record
     *  @param pkgQueryInspectorRecord the package query inspector
     *         record
     *  @param pkgSearchOrderRecord the package search order record
     *  @param pkgRecordType package record type
     *  @throws org.osid.NullArgumentException
     *          <code>pkgQueryRecord</code>,
     *          <code>pkgQueryInspectorRecord</code>,
     *          <code>pkgSearchOrderRecord</code> or
     *          <code>pkgRecordTypepkg</code> is
     *          <code>null</code>
     */
            
    protected void addPackageRecords(org.osid.installation.records.PackageQueryRecord pkgQueryRecord, 
                                      org.osid.installation.records.PackageQueryInspectorRecord pkgQueryInspectorRecord, 
                                      org.osid.installation.records.PackageSearchOrderRecord pkgSearchOrderRecord, 
                                      org.osid.type.Type pkgRecordType) {

        addRecordType(pkgRecordType);

        nullarg(pkgQueryRecord, "package query record");
        nullarg(pkgQueryInspectorRecord, "package query inspector record");
        nullarg(pkgSearchOrderRecord, "package search odrer record");

        this.queryRecords.add(pkgQueryRecord);
        this.queryInspectorRecords.add(pkgQueryInspectorRecord);
        this.searchOrderRecords.add(pkgSearchOrderRecord);
        
        return;
    }
}
