//
// AbstractPriceEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPriceEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ordering.rules.PriceEnablerSearchResults {

    private org.osid.ordering.rules.PriceEnablerList priceEnablers;
    private final org.osid.ordering.rules.PriceEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPriceEnablerSearchResults.
     *
     *  @param priceEnablers the result set
     *  @param priceEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>priceEnablers</code>
     *          or <code>priceEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPriceEnablerSearchResults(org.osid.ordering.rules.PriceEnablerList priceEnablers,
                                            org.osid.ordering.rules.PriceEnablerQueryInspector priceEnablerQueryInspector) {
        nullarg(priceEnablers, "price enablers");
        nullarg(priceEnablerQueryInspector, "price enabler query inspectpr");

        this.priceEnablers = priceEnablers;
        this.inspector = priceEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the price enabler list resulting from a search.
     *
     *  @return a price enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablers() {
        if (this.priceEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ordering.rules.PriceEnablerList priceEnablers = this.priceEnablers;
        this.priceEnablers = null;
	return (priceEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ordering.rules.PriceEnablerQueryInspector getPriceEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  price enabler search record <code> Type. </code> This method must
     *  be used to retrieve a priceEnabler implementing the requested
     *  record.
     *
     *  @param priceEnablerSearchRecordType a priceEnabler search 
     *         record type 
     *  @return the price enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(priceEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerSearchResultsRecord getPriceEnablerSearchResultsRecord(org.osid.type.Type priceEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ordering.rules.records.PriceEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(priceEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record price enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPriceEnablerRecord(org.osid.ordering.rules.records.PriceEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "price enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
