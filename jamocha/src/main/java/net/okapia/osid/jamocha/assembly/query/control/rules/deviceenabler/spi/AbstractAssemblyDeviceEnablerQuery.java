//
// AbstractAssemblyDeviceEnablerQuery.java
//
//     A DeviceEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.rules.deviceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DeviceEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyDeviceEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.control.rules.DeviceEnablerQuery,
               org.osid.control.rules.DeviceEnablerQueryInspector,
               org.osid.control.rules.DeviceEnablerSearchOrder {

    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDeviceEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDeviceEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the trigger. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledTriggerId(org.osid.id.Id triggerId, boolean match) {
        getAssembler().addIdTerm(getRuledTriggerIdColumn(), triggerId, match);
        return;
    }


    /**
     *  Clears the trigger <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerIdTerms() {
        getAssembler().clearTerms(getRuledTriggerIdColumn());
        return;
    }


    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledTriggerIdTerms() {
        return (getAssembler().getIdTerms(getRuledTriggerIdColumn()));
    }


    /**
     *  Gets the RuledTriggerId column name.
     *
     * @return the column name
     */

    protected String getRuledTriggerIdColumn() {
        return ("ruled_trigger_id");
    }


    /**
     *  Tests if an <code> TriggerQuery </code> is available. 
     *
     *  @return <code> true </code> if an trigger query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledTriggerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an trigger. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the trigger query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledTriggerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuery getRuledTriggerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledTriggerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any trigger. 
     *
     *  @param  match <code> true </code> for enablers mapped to any trigger, 
     *          <code> false </code> to match enablers mapped to no triggers 
     */

    @OSID @Override
    public void matchAnyRuledTrigger(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledTriggerColumn(), match);
        return;
    }


    /**
     *  Clears the trigger query terms. 
     */

    @OSID @Override
    public void clearRuledTriggerTerms() {
        getAssembler().clearTerms(getRuledTriggerColumn());
        return;
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getRuledTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the RuledTrigger column name.
     *
     * @return the column name
     */

    protected String getRuledTriggerColumn() {
        return ("ruled_trigger");
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this deviceEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  deviceEnablerRecordType a device enabler record type 
     *  @return <code>true</code> if the deviceEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type deviceEnablerRecordType) {
        for (org.osid.control.rules.records.DeviceEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deviceEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  deviceEnablerRecordType the device enabler record type 
     *  @return the device enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerQueryRecord getDeviceEnablerQueryRecord(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.DeviceEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(deviceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  deviceEnablerRecordType the device enabler record type 
     *  @return the device enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord getDeviceEnablerQueryInspectorRecord(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(deviceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param deviceEnablerRecordType the device enabler record type
     *  @return the device enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerSearchOrderRecord getDeviceEnablerSearchOrderRecord(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.DeviceEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(deviceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this device enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param deviceEnablerQueryRecord the device enabler query record
     *  @param deviceEnablerQueryInspectorRecord the device enabler query inspector
     *         record
     *  @param deviceEnablerSearchOrderRecord the device enabler search order record
     *  @param deviceEnablerRecordType device enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerQueryRecord</code>,
     *          <code>deviceEnablerQueryInspectorRecord</code>,
     *          <code>deviceEnablerSearchOrderRecord</code> or
     *          <code>deviceEnablerRecordTypedeviceEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addDeviceEnablerRecords(org.osid.control.rules.records.DeviceEnablerQueryRecord deviceEnablerQueryRecord, 
                                      org.osid.control.rules.records.DeviceEnablerQueryInspectorRecord deviceEnablerQueryInspectorRecord, 
                                      org.osid.control.rules.records.DeviceEnablerSearchOrderRecord deviceEnablerSearchOrderRecord, 
                                      org.osid.type.Type deviceEnablerRecordType) {

        addRecordType(deviceEnablerRecordType);

        nullarg(deviceEnablerQueryRecord, "device enabler query record");
        nullarg(deviceEnablerQueryInspectorRecord, "device enabler query inspector record");
        nullarg(deviceEnablerSearchOrderRecord, "device enabler search odrer record");

        this.queryRecords.add(deviceEnablerQueryRecord);
        this.queryInspectorRecords.add(deviceEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(deviceEnablerSearchOrderRecord);
        
        return;
    }
}
