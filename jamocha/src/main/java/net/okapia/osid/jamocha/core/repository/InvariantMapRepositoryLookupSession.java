//
// InvariantMapRepositoryLookupSession
//
//    Implements a Repository lookup service backed by a fixed collection of
//    repositories.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Repository lookup service backed by a fixed
 *  collection of repositories. The repositories are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRepositoryLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRepositoryLookupSession</code> with no
     *  repositories.
     */

    public InvariantMapRepositoryLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRepositoryLookupSession</code> with a single
     *  repository.
     *  
     *  @throws org.osid.NullArgumentException {@code repository}
     *          is <code>null</code>
     */

    public InvariantMapRepositoryLookupSession(org.osid.repository.Repository repository) {
        putRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRepositoryLookupSession</code> using an array
     *  of repositories.
     *  
     *  @throws org.osid.NullArgumentException {@code repositories}
     *          is <code>null</code>
     */

    public InvariantMapRepositoryLookupSession(org.osid.repository.Repository[] repositories) {
        putRepositories(repositories);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRepositoryLookupSession</code> using a
     *  collection of repositories.
     *
     *  @throws org.osid.NullArgumentException {@code repositories}
     *          is <code>null</code>
     */

    public InvariantMapRepositoryLookupSession(java.util.Collection<? extends org.osid.repository.Repository> repositories) {
        putRepositories(repositories);
        return;
    }
}
