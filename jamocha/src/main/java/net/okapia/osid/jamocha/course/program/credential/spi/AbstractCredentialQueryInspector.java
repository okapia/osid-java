//
// AbstractCredentialQueryInspector.java
//
//     A template for making a CredentialQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for credentials.
 */

public abstract class AbstractCredentialQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.course.program.CredentialQueryInspector {

    private final java.util.Collection<org.osid.course.program.records.CredentialQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the lifetime query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getLifetimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given credential query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a credential implementing the requested record.
     *
     *  @param credentialRecordType a credential record type
     *  @return the credential query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialQueryInspectorRecord getCredentialQueryInspectorRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.CredentialQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credential query. 
     *
     *  @param credentialQueryInspectorRecord credential query inspector
     *         record
     *  @param credentialRecordType credential record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCredentialQueryInspectorRecord(org.osid.course.program.records.CredentialQueryInspectorRecord credentialQueryInspectorRecord, 
                                                   org.osid.type.Type credentialRecordType) {

        addRecordType(credentialRecordType);
        nullarg(credentialRecordType, "credential record type");
        this.records.add(credentialQueryInspectorRecord);        
        return;
    }
}
