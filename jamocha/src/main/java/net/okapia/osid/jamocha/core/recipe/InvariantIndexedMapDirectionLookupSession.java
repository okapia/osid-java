//
// InvariantIndexedMapDirectionLookupSession
//
//    Implements a Direction lookup service backed by a fixed
//    collection of directions indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Direction lookup service backed by a fixed
 *  collection of directions. The directions are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some directions may be compatible
 *  with more types than are indicated through these direction
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapDirectionLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractIndexedMapDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapDirectionLookupSession} using an
     *  array of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions an array of directions
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code directions} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                    org.osid.recipe.Direction[] directions) {

        setCookbook(cookbook);
        putDirections(directions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapDirectionLookupSession} using a
     *  collection of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions a collection of directions
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code directions} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                    java.util.Collection<? extends org.osid.recipe.Direction> directions) {

        setCookbook(cookbook);
        putDirections(directions);
        return;
    }
}
