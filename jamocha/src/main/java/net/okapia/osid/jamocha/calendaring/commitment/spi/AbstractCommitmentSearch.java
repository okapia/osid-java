//
// AbstractCommitmentSearch.java
//
//     A template for making a Commitment Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.commitment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing commitment searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCommitmentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.CommitmentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.CommitmentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.CommitmentSearchOrder commitmentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of commitments. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  commitmentIds list of commitments
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCommitments(org.osid.id.IdList commitmentIds) {
        while (commitmentIds.hasNext()) {
            try {
                this.ids.add(commitmentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCommitments</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of commitment Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCommitmentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  commitmentSearchOrder commitment search order 
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>commitmentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCommitmentResults(org.osid.calendaring.CommitmentSearchOrder commitmentSearchOrder) {
	this.commitmentSearchOrder = commitmentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.CommitmentSearchOrder getCommitmentSearchOrder() {
	return (this.commitmentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given commitment search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a commitment implementing the requested record.
     *
     *  @param commitmentSearchRecordType a commitment search record
     *         type
     *  @return the commitment search record
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commitmentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentSearchRecord getCommitmentSearchRecord(org.osid.type.Type commitmentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.CommitmentSearchRecord record : this.records) {
            if (record.implementsRecordType(commitmentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commitmentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this commitment search. 
     *
     *  @param commitmentSearchRecord commitment search record
     *  @param commitmentSearchRecordType commitment search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommitmentSearchRecord(org.osid.calendaring.records.CommitmentSearchRecord commitmentSearchRecord, 
                                           org.osid.type.Type commitmentSearchRecordType) {

        addRecordType(commitmentSearchRecordType);
        this.records.add(commitmentSearchRecord);        
        return;
    }
}
