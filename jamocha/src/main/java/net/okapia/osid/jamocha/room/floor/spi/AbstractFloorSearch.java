//
// AbstractFloorSearch.java
//
//     A template for making a Floor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing floor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFloorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.FloorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.records.FloorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.FloorSearchOrder floorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of floors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  floorIds list of floors
     *  @throws org.osid.NullArgumentException
     *          <code>floorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFloors(org.osid.id.IdList floorIds) {
        while (floorIds.hasNext()) {
            try {
                this.ids.add(floorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFloors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of floor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFloorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  floorSearchOrder floor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>floorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>floorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFloorResults(org.osid.room.FloorSearchOrder floorSearchOrder) {
	this.floorSearchOrder = floorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.FloorSearchOrder getFloorSearchOrder() {
	return (this.floorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given floor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a floor implementing the requested record.
     *
     *  @param floorSearchRecordType a floor search record
     *         type
     *  @return the floor search record
     *  @throws org.osid.NullArgumentException
     *          <code>floorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorSearchRecord getFloorSearchRecord(org.osid.type.Type floorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.records.FloorSearchRecord record : this.records) {
            if (record.implementsRecordType(floorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this floor search. 
     *
     *  @param floorSearchRecord floor search record
     *  @param floorSearchRecordType floor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFloorSearchRecord(org.osid.room.records.FloorSearchRecord floorSearchRecord, 
                                           org.osid.type.Type floorSearchRecordType) {

        addRecordType(floorSearchRecordType);
        this.records.add(floorSearchRecord);        
        return;
    }
}
