//
// AbstractLogEntry.java
//
//     Defines a LogEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.logentry.spi;


/**
 *  Defines a <code>LogEntry</code> builder.
 */

public abstract class AbstractLogEntryBuilder<T extends AbstractLogEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.tracking.logentry.LogEntryMiter logEntry;


    /**
     *  Constructs a new <code>AbstractLogEntryBuilder</code>.
     *
     *  @param logEntry the log entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLogEntryBuilder(net.okapia.osid.jamocha.builder.tracking.logentry.LogEntryMiter logEntry) {
        super(logEntry);
        this.logEntry = logEntry;
        return;
    }


    /**
     *  Builds the log entry.
     *
     *  @return the new log entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.tracking.LogEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.tracking.logentry.LogEntryValidator(getValidations())).validate(this.logEntry);
        return (new net.okapia.osid.jamocha.builder.tracking.logentry.ImmutableLogEntry(this.logEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the log entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.tracking.logentry.LogEntryMiter getMiter() {
        return (this.logEntry);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the issue.
     *
     *  @param issue an issue
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>issue</code> is
     *          <code>null</code>
     */

    public T issue(org.osid.tracking.Issue issue) {
        getMiter().setIssue(issue);
        return (self());
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T date(org.osid.calendaring.DateTime date) {
        getMiter().setDate(date);
        return (self());
    }


    /**
     *  Sets the action.
     *
     *  @param action an action
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>action</code> is
     *          <code>null</code>
     */

    public T action(org.osid.tracking.IssueAction action) {
        getMiter().setAction(action);
        return (self());
    }


    /**
     *  Sets the summary.
     *
     *  @param summary a summary
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>summary</code> is
     *          <code>null</code>
     */

    public T summary(org.osid.locale.DisplayText summary) {
        getMiter().setSummary(summary);
        return (self());
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T message(org.osid.locale.DisplayText message) {
        getMiter().setMessage(message);
        return (self());
    }


    /**
     *  Adds a LogEntry record.
     *
     *  @param record a log entry record
     *  @param recordType the type of log entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.tracking.records.LogEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addLogEntryRecord(record, recordType);
        return (self());
    }
}       


