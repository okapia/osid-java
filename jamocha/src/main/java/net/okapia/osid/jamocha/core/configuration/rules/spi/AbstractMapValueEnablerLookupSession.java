//
// AbstractMapValueEnablerLookupSession
//
//    A simple framework for providing a ValueEnabler lookup service
//    backed by a fixed collection of value enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ValueEnabler lookup service backed by a
 *  fixed collection of value enablers. The value enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ValueEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapValueEnablerLookupSession
    extends net.okapia.osid.jamocha.configuration.rules.spi.AbstractValueEnablerLookupSession
    implements org.osid.configuration.rules.ValueEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.rules.ValueEnabler> valueEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.rules.ValueEnabler>());


    /**
     *  Makes a <code>ValueEnabler</code> available in this session.
     *
     *  @param  valueEnabler a value enabler
     *  @throws org.osid.NullArgumentException <code>valueEnabler<code>
     *          is <code>null</code>
     */

    protected void putValueEnabler(org.osid.configuration.rules.ValueEnabler valueEnabler) {
        this.valueEnablers.put(valueEnabler.getId(), valueEnabler);
        return;
    }


    /**
     *  Makes an array of value enablers available in this session.
     *
     *  @param  valueEnablers an array of value enablers
     *  @throws org.osid.NullArgumentException <code>valueEnablers<code>
     *          is <code>null</code>
     */

    protected void putValueEnablers(org.osid.configuration.rules.ValueEnabler[] valueEnablers) {
        putValueEnablers(java.util.Arrays.asList(valueEnablers));
        return;
    }


    /**
     *  Makes a collection of value enablers available in this session.
     *
     *  @param  valueEnablers a collection of value enablers
     *  @throws org.osid.NullArgumentException <code>valueEnablers<code>
     *          is <code>null</code>
     */

    protected void putValueEnablers(java.util.Collection<? extends org.osid.configuration.rules.ValueEnabler> valueEnablers) {
        for (org.osid.configuration.rules.ValueEnabler valueEnabler : valueEnablers) {
            this.valueEnablers.put(valueEnabler.getId(), valueEnabler);
        }

        return;
    }


    /**
     *  Removes a ValueEnabler from this session.
     *
     *  @param  valueEnablerId the <code>Id</code> of the value enabler
     *  @throws org.osid.NullArgumentException <code>valueEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeValueEnabler(org.osid.id.Id valueEnablerId) {
        this.valueEnablers.remove(valueEnablerId);
        return;
    }


    /**
     *  Gets the <code>ValueEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  valueEnablerId <code>Id</code> of the <code>ValueEnabler</code>
     *  @return the valueEnabler
     *  @throws org.osid.NotFoundException <code>valueEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>valueEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnabler getValueEnabler(org.osid.id.Id valueEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.rules.ValueEnabler valueEnabler = this.valueEnablers.get(valueEnablerId);
        if (valueEnabler == null) {
            throw new org.osid.NotFoundException("valueEnabler not found: " + valueEnablerId);
        }

        return (valueEnabler);
    }


    /**
     *  Gets all <code>ValueEnablers</code>. In plenary mode, the returned
     *  list contains all known valueEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  valueEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ValueEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ValueEnablerList getValueEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.valueenabler.ArrayValueEnablerList(this.valueEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.valueEnablers.clear();
        super.close();
        return;
    }
}
