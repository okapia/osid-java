//
// AbstractIndexedMapOfferingLookupSession.java
//
//    A simple framework for providing an Offering lookup service
//    backed by a fixed collection of offerings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Offering lookup service backed by a
 *  fixed collection of offerings. The offerings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some offerings may be compatible
 *  with more types than are indicated through these offering
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Offerings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOfferingLookupSession
    extends AbstractMapOfferingLookupSession
    implements org.osid.offering.OfferingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.offering.Offering> offeringsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Offering>());
    private final MultiMap<org.osid.type.Type, org.osid.offering.Offering> offeringsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.offering.Offering>());


    /**
     *  Makes an <code>Offering</code> available in this session.
     *
     *  @param  offering an offering
     *  @throws org.osid.NullArgumentException <code>offering<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOffering(org.osid.offering.Offering offering) {
        super.putOffering(offering);

        this.offeringsByGenus.put(offering.getGenusType(), offering);
        
        try (org.osid.type.TypeList types = offering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringsByRecord.put(types.getNextType(), offering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of offerings available in this session.
     *
     *  @param  offerings an array of offerings
     *  @throws org.osid.NullArgumentException <code>offerings<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferings(org.osid.offering.Offering[] offerings) {
        for (org.osid.offering.Offering offering : offerings) {
            putOffering(offering);
        }

        return;
    }


    /**
     *  Makes a collection of offerings available in this session.
     *
     *  @param  offerings a collection of offerings
     *  @throws org.osid.NullArgumentException <code>offerings<code>
     *          is <code>null</code>
     */

    @Override
    protected void putOfferings(java.util.Collection<? extends org.osid.offering.Offering> offerings) {
        for (org.osid.offering.Offering offering : offerings) {
            putOffering(offering);
        }

        return;
    }


    /**
     *  Removes an offering from this session.
     *
     *  @param offeringId the <code>Id</code> of the offering
     *  @throws org.osid.NullArgumentException <code>offeringId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOffering(org.osid.id.Id offeringId) {
        org.osid.offering.Offering offering;
        try {
            offering = getOffering(offeringId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.offeringsByGenus.remove(offering.getGenusType());

        try (org.osid.type.TypeList types = offering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.offeringsByRecord.remove(types.getNextType(), offering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOffering(offeringId);
        return;
    }


    /**
     *  Gets an <code>OfferingList</code> corresponding to the given
     *  offering genus <code>Type</code> which does not include
     *  offerings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known offerings or an error results. Otherwise,
     *  the returned list may contain only those offerings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  offeringGenusType an offering genus type 
     *  @return the returned <code>Offering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByGenusType(org.osid.type.Type offeringGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.offering.ArrayOfferingList(this.offeringsByGenus.get(offeringGenusType)));
    }


    /**
     *  Gets an <code>OfferingList</code> containing the given
     *  offering record <code>Type</code>. In plenary mode, the
     *  returned list contains all known offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  offerings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  offeringRecordType an offering record type 
     *  @return the returned <code>offering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferingsByRecordType(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.offering.ArrayOfferingList(this.offeringsByRecord.get(offeringRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offeringsByGenus.clear();
        this.offeringsByRecord.clear();

        super.close();

        return;
    }
}
