//
// AbstractInstallationSearchOdrer.java
//
//     Defines an InstallationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code InstallationSearchOrder}.
 */

public abstract class AbstractInstallationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.installation.InstallationSearchOrder {

    private final java.util.Collection<org.osid.installation.records.InstallationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specified a preference for ordering results by the site. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySite(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> SiteSearchOrder </code> is available for sites. 
     *
     *  @return <code> true </code> if a site search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSiteSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a site. 
     *
     *  @return the site search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSiteSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteSearchOrder getSiteSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSiteSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the package. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPackage(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> PackageSearchOrder </code> is available for 
     *  packages. 
     *
     *  @return <code> true </code> if a package search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a package. 
     *
     *  @return the package search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchOrder getPackageSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPackageSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the install date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInstallDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specified a preference for ordering results by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AgenteSearchOrder </code> is available for agents. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an agent. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Specified a preference for ordering results by the last checked date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLastCheckDate(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  installationRecordType an installation record type 
     *  @return {@code true} if the installationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code installationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type installationRecordType) {
        for (org.osid.installation.records.InstallationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  installationRecordType the installation record type 
     *  @return the installation search order record
     *  @throws org.osid.NullArgumentException
     *          {@code installationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(installationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.installation.records.InstallationSearchOrderRecord getInstallationSearchOrderRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this installation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param installationRecord the installation search odrer record
     *  @param installationRecordType installation record type
     *  @throws org.osid.NullArgumentException
     *          {@code installationRecord} or
     *          {@code installationRecordTypeinstallation} is
     *          {@code null}
     */
            
    protected void addInstallationRecord(org.osid.installation.records.InstallationSearchOrderRecord installationSearchOrderRecord, 
                                     org.osid.type.Type installationRecordType) {

        addRecordType(installationRecordType);
        this.records.add(installationSearchOrderRecord);
        
        return;
    }
}
