//
// AbstractFederatingCommissionEnablerLookupSession.java
//
//     An abstract federating adapter for a CommissionEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CommissionEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCommissionEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.rules.CommissionEnablerLookupSession>
    implements org.osid.resourcing.rules.CommissionEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingCommissionEnablerLookupSession</code>.
     */

    protected AbstractFederatingCommissionEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.rules.CommissionEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>CommissionEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommissionEnablers() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            if (session.canLookupCommissionEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CommissionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommissionEnablerView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.useComparativeCommissionEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CommissionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommissionEnablerView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.usePlenaryCommissionEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commission enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }


    /**
     *  Only active commission enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCommissionEnablerView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.useActiveCommissionEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive commission enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCommissionEnablerView() {
        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            session.useAnyStatusCommissionEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>CommissionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CommissionEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CommissionEnabler</code> and retained for compatibility.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  commissionEnablerId <code>Id</code> of the
     *          <code>CommissionEnabler</code>
     *  @return the commission enabler
     *  @throws org.osid.NotFoundException <code>commissionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commissionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnabler getCommissionEnabler(org.osid.id.Id commissionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            try {
                return (session.getCommissionEnabler(commissionEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(commissionEnablerId + " not found");
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commissionEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>CommissionEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  commissionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByIds(org.osid.id.IdList commissionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.rules.commissionenabler.MutableCommissionEnablerList ret = new net.okapia.osid.jamocha.resourcing.rules.commissionenabler.MutableCommissionEnablerList();

        try (org.osid.id.IdList ids = commissionEnablerIds) {
            while (ids.hasNext()) {
                ret.addCommissionEnabler(getCommissionEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the
     *  given commission enabler genus <code>Type</code> which does
     *  not include commission enablers of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  commissionEnablerGenusType a commissionEnabler genus type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByGenusType(org.osid.type.Type commissionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablersByGenusType(commissionEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the
     *  given commission enabler genus <code>Type</code> and include
     *  any additional commission enablers with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  commissionEnablerGenusType a commissionEnabler genus type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByParentGenusType(org.osid.type.Type commissionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablersByParentGenusType(commissionEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> containing the given
     *  commission enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  commissionEnablerRecordType a commissionEnabler record type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByRecordType(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablersByRecordType(commissionEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session.
     *  
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CommissionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>CommissionEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined to
     *  the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CommissionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the
     *  returned list may contain only those commission enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, commission enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  commission enablers are returned.
     *
     *  @return a list of <code>CommissionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList ret = getCommissionEnablerList();

        for (org.osid.resourcing.rules.CommissionEnablerLookupSession session : getSessions()) {
            ret.addCommissionEnablerList(session.getCommissionEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.FederatingCommissionEnablerList getCommissionEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.ParallelCommissionEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.commissionenabler.CompositeCommissionEnablerList());
        }
    }
}
