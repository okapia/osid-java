//
// AbstractAssemblyModuleQuery.java
//
//     A ModuleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ModuleQuery that stores terms.
 */

public abstract class AbstractAssemblyModuleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.course.syllabus.ModuleQuery,
               org.osid.course.syllabus.ModuleQueryInspector,
               org.osid.course.syllabus.ModuleSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.ModuleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.ModuleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.ModuleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyModuleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyModuleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the syllabus <code> Id </code> for this query. 
     *
     *  @param  syllabusId a syllabus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSyllabusId(org.osid.id.Id syllabusId, boolean match) {
        getAssembler().addIdTerm(getSyllabusIdColumn(), syllabusId, match);
        return;
    }


    /**
     *  Clears the syllabus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSyllabusIdTerms() {
        getAssembler().clearTerms(getSyllabusIdColumn());
        return;
    }


    /**
     *  Gets the syllabus <code> Id </code> terms. 
     *
     *  @return the syllabus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSyllabusIdTerms() {
        return (getAssembler().getIdTerms(getSyllabusIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the syllabus. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySyllabus(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSyllabusColumn(), style);
        return;
    }


    /**
     *  Gets the SyllabusId column name.
     *
     * @return the column name
     */

    protected String getSyllabusIdColumn() {
        return ("syllabus_id");
    }


    /**
     *  Tests if a syllabus query is available. 
     *
     *  @return <code> true </code> if a syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet. 
     *
     *  @return the syllabus query 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuery getSyllabusQuery() {
        throw new org.osid.UnimplementedException("supportsSyllabusQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearSyllabusTerms() {
        getAssembler().clearTerms(getSyllabusColumn());
        return;
    }


    /**
     *  Gets the syllabus terms. 
     *
     *  @return the syllabus terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQueryInspector[] getSyllabusTerms() {
        return (new org.osid.course.syllabus.SyllabusQueryInspector[0]);
    }


    /**
     *  Tests if a syllabus order is available. 
     *
     *  @return <code> true </code> if a syllabus order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchOrder() {
        return (false);
    }


    /**
     *  Gets the syllabus order. 
     *
     *  @return the syllabus search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchOrder getSyllabusSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSyllabusSearchOrder() is false");
    }


    /**
     *  Gets the Syllabus column name.
     *
     * @return the column name
     */

    protected String getSyllabusColumn() {
        return ("syllabus");
    }


    /**
     *  Sets a docet <code> Id. </code> 
     *
     *  @param  docetId a docet <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> docetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDocetId(org.osid.id.Id docetId, boolean match) {
        getAssembler().addIdTerm(getDocetIdColumn(), docetId, match);
        return;
    }


    /**
     *  Clears the docet <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDocetIdTerms() {
        getAssembler().clearTerms(getDocetIdColumn());
        return;
    }


    /**
     *  Gets the docet <code> Id </code> terms. 
     *
     *  @return the docet <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDocetIdTerms() {
        return (getAssembler().getIdTerms(getDocetIdColumn()));
    }


    /**
     *  Gets the DocetId column name.
     *
     * @return the column name
     */

    protected String getDocetIdColumn() {
        return ("docet_id");
    }


    /**
     *  Tests if an <code> DocetQuery </code> is available. 
     *
     *  @return <code> true </code> if a docet query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDocetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a docet query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the docet query 
     *  @throws org.osid.UnimplementedException <code> supportsDocetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQuery getDocetQuery() {
        throw new org.osid.UnimplementedException("supportsDocetQuery() is false");
    }


    /**
     *  Matches modules with any docet. 
     *
     *  @param  match <code> true </code> to match modules with any docet, 
     *          <code> false </code> to match modules with no docets 
     */

    @OSID @Override
    public void matchAnyDocet(boolean match) {
        getAssembler().addIdWildcardTerm(getDocetColumn(), match);
        return;
    }


    /**
     *  Clears the docet terms. 
     */

    @OSID @Override
    public void clearDocetTerms() {
        getAssembler().clearTerms(getDocetColumn());
        return;
    }


    /**
     *  Gets the docet terms. 
     *
     *  @return the docet terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQueryInspector[] getDocetTerms() {
        return (new org.osid.course.syllabus.DocetQueryInspector[0]);
    }


    /**
     *  Gets the Docet column name.
     *
     * @return the column name
     */

    protected String getDocetColumn() {
        return ("docet");
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match modules 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this module supports the given record
     *  <code>Type</code>.
     *
     *  @param  moduleRecordType a module record type 
     *  @return <code>true</code> if the moduleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type moduleRecordType) {
        for (org.osid.course.syllabus.records.ModuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  moduleRecordType the module record type 
     *  @return the module query record 
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleQueryRecord getModuleQueryRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  moduleRecordType the module record type 
     *  @return the module query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleQueryInspectorRecord getModuleQueryInspectorRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param moduleRecordType the module record type
     *  @return the module search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleSearchOrderRecord getModuleSearchOrderRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this module. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param moduleQueryRecord the module query record
     *  @param moduleQueryInspectorRecord the module query inspector
     *         record
     *  @param moduleSearchOrderRecord the module search order record
     *  @param moduleRecordType module record type
     *  @throws org.osid.NullArgumentException
     *          <code>moduleQueryRecord</code>,
     *          <code>moduleQueryInspectorRecord</code>,
     *          <code>moduleSearchOrderRecord</code> or
     *          <code>moduleRecordTypemodule</code> is
     *          <code>null</code>
     */
            
    protected void addModuleRecords(org.osid.course.syllabus.records.ModuleQueryRecord moduleQueryRecord, 
                                      org.osid.course.syllabus.records.ModuleQueryInspectorRecord moduleQueryInspectorRecord, 
                                      org.osid.course.syllabus.records.ModuleSearchOrderRecord moduleSearchOrderRecord, 
                                      org.osid.type.Type moduleRecordType) {

        addRecordType(moduleRecordType);

        nullarg(moduleQueryRecord, "module query record");
        nullarg(moduleQueryInspectorRecord, "module query inspector record");
        nullarg(moduleSearchOrderRecord, "module search odrer record");

        this.queryRecords.add(moduleQueryRecord);
        this.queryInspectorRecords.add(moduleQueryInspectorRecord);
        this.searchOrderRecords.add(moduleSearchOrderRecord);
        
        return;
    }
}
