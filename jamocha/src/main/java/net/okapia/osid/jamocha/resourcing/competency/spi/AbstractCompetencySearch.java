//
// AbstractCompetencySearch.java
//
//     A template for making a Competency Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.competency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing competency searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCompetencySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.CompetencySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.CompetencySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.CompetencySearchOrder competencySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of competencies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  competencyIds list of competencies
     *  @throws org.osid.NullArgumentException
     *          <code>competencyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCompetencies(org.osid.id.IdList competencyIds) {
        while (competencyIds.hasNext()) {
            try {
                this.ids.add(competencyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCompetencies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of competency Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCompetencyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  competencySearchOrder competency search order 
     *  @throws org.osid.NullArgumentException
     *          <code>competencySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>competencySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCompetencyResults(org.osid.resourcing.CompetencySearchOrder competencySearchOrder) {
	this.competencySearchOrder = competencySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.CompetencySearchOrder getCompetencySearchOrder() {
	return (this.competencySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given competency search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a competency implementing the requested record.
     *
     *  @param competencySearchRecordType a competency search record
     *         type
     *  @return the competency search record
     *  @throws org.osid.NullArgumentException
     *          <code>competencySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(competencySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencySearchRecord getCompetencySearchRecord(org.osid.type.Type competencySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.records.CompetencySearchRecord record : this.records) {
            if (record.implementsRecordType(competencySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(competencySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this competency search. 
     *
     *  @param competencySearchRecord competency search record
     *  @param competencySearchRecordType competency search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompetencySearchRecord(org.osid.resourcing.records.CompetencySearchRecord competencySearchRecord, 
                                           org.osid.type.Type competencySearchRecordType) {

        addRecordType(competencySearchRecordType);
        this.records.add(competencySearchRecord);        
        return;
    }
}
