//
// AbstractSetting.java
//
//     Defines a Setting.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Setting</code>.
 */

public abstract class AbstractSetting
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.control.Setting {

    private org.osid.control.Controller controller;
    private java.math.BigDecimal variableAmount;
    private org.osid.process.State discreetState;
    private org.osid.calendaring.Duration rampRate;

    private boolean on;
    private boolean off;

    private final java.util.Collection<org.osid.control.records.SettingRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the controller <code> Id. </code> 
     *
     *  @return the controller <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getControllerId() {
        return (this.controller.getId());
    }


    /**
     *  Gets the controller. 
     *
     *  @return the controller 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.Controller getController()
        throws org.osid.OperationFailedException {

        return (this.controller);
    }


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    protected void setController(org.osid.control.Controller controller) {
        nullarg(controller, "controller");
        this.controller = controller;
        return;
    }


    /**
     *  Tests if the toggleable controller is on. 
     *
     *  @return <code> true </code> if the controller is on, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.isToggleable() </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean isOn() {
        try {
            if (!getController().isToggleable()) {
                throw new org.osid.IllegalStateException("isToggleable() is false");
            }
        } catch (org.osid.OperationFailedException oe) {}

        return (this.on);
    }


    /**
     *  Sets the on flag.
     *
     *  @param on {@code true} is on, {@code false} if off or unknown
     */

    protected void setOn(boolean on) {
        this.on = on;
        return;
    }


    /**
     *  Tests if the toggleable controller is off. 
     *
     *  @return <code> true </code> if the controller is iff, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.isToggleable() </code> is <code> false </code> 
     */

    @OSID @Override
    public boolean isOff() {
        try {
            if (!getController().isToggleable()) {
                throw new org.osid.IllegalStateException("isToggleable() is false");
            }
        } catch (org.osid.OperationFailedException oe) {}

        return (this.off);
    }


    /**
     *  Sets the off flag.
     *
     *  @param off {@code true} is off, {@code false} if on or unknown
     */

    protected void setOff(boolean off) {
        this.off = off;
        return;
    }


    /**
     *  Gets the level amount on a fixed scale. 
     *
     *  @return the level amount 
     *  @throws org.osid.IllegalStateException <code> Controller.isVariable() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getVariableAmount() {
        try {
            if (!getController().isVariable()) {
                throw new org.osid.IllegalStateException("isVariable() is false");
            }
        } catch (org.osid.OperationFailedException oe) {}

        return (this.variableAmount);
    }


    /**
     *  Sets the variable amount.
     *
     *  @param amount a variable amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setVariableAmount(java.math.BigDecimal amount) {
        nullarg(amount, "variable amount");
        this.variableAmount = amount;
        return;
    }


    /**
     *  Gets the discreet <code> State </code> <code> Id. </code> 
     *
     *  @return the state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.hasDiscreetStates() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDiscreetStateId() {
        return (this.discreetState.getId());
    }


    /**
     *  Gets the discreet <code> State. </code> 
     *
     *  @return the state 
     *  @throws org.osid.IllegalStateException <code> 
     *          Controller.hasDiscreetStates() </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getDiscreetState()
        throws org.osid.OperationFailedException {

        try {
            if (!getController().hasDiscreetStates()) {
                throw new org.osid.IllegalStateException("hasDiscreetStates() is false");
            }
        } catch (org.osid.OperationFailedException oe) {}

        return (this.discreetState);
    }


    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void setDiscreetState(org.osid.process.State state) {
        nullarg(state, "state");
        this.discreetState = state;
        return;
    }


    /**
     *  Gets the ramp rate from off to on to use for the transition
     *  for this setting. If the ramp rate is 10 seconds and the
     *  variable percentage is 50%, then the actual transition
     *  duration from off would be 5 seconds for a linear controller.
     *
     *  @return the ramp rate 
     *  @throws org.osid.IllegalStateException <code> Controller.isRampable() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRampRate() {
        try {
            if (!getController().isRampable()) {
                throw new org.osid.IllegalStateException("isRampable() is false");
            }
        } catch (org.osid.OperationFailedException oe) {}

        return (this.rampRate);
    }


    /**
     *  Sets the ramp rate.
     *
     *  @param rate a ramp rate
     *  @throws org.osid.NullArgumentException <code>rate</code> is
     *          <code>null</code>
     */

    protected void setRampRate(org.osid.calendaring.Duration rate) {
        nullarg(rate, "ramp rate");
        this.rampRate = rate;
        return;
    }


    /**
     *  Tests if this setting supports the given record
     *  <code>Type</code>.
     *
     *  @param  settingRecordType a setting record type 
     *  @return <code>true</code> if the settingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type settingRecordType) {
        for (org.osid.control.records.SettingRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  settingRecordType the setting record type 
     *  @return the setting record 
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingRecord getSettingRecord(org.osid.type.Type settingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SettingRecord record : this.records) {
            if (record.implementsRecordType(settingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this setting. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param settingRecord the setting record
     *  @param settingRecordType setting record type
     *  @throws org.osid.NullArgumentException
     *          <code>settingRecord</code> or
     *          <code>settingRecordTypesetting</code> is
     *          <code>null</code>
     */
            
    protected void addSettingRecord(org.osid.control.records.SettingRecord settingRecord, 
                                     org.osid.type.Type settingRecordType) {

        addRecordType(settingRecordType);
        this.records.add(settingRecord);
        
        return;
    }
}
