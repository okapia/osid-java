//
// AbstractNodeOublietteHierarchySession.java
//
//     Defines an Oubliette hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an oubliette hierarchy session for delivering a hierarchy
 *  of oubliettes using the OublietteNode interface.
 */

public abstract class AbstractNodeOublietteHierarchySession
    extends net.okapia.osid.jamocha.hold.spi.AbstractOublietteHierarchySession
    implements org.osid.hold.OublietteHierarchySession {

    private java.util.Collection<org.osid.hold.OublietteNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root oubliette <code> Ids </code> in this hierarchy.
     *
     *  @return the root oubliette <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootOublietteIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliettenode.OublietteNodeToIdList(this.roots));
    }


    /**
     *  Gets the root oubliettes in the oubliette hierarchy. A node
     *  with no parents is an orphan. While all oubliette <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root oubliettes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getRootOubliettes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliettenode.OublietteNodeToOublietteList(new net.okapia.osid.jamocha.hold.oubliettenode.ArrayOublietteNodeList(this.roots)));
    }


    /**
     *  Adds a root oubliette node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootOubliette(org.osid.hold.OublietteNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root oubliette nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootOubliettes(java.util.Collection<org.osid.hold.OublietteNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root oubliette node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootOubliette(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.hold.OublietteNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Oubliette </code> has any parents. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @return <code> true </code> if the oubliette has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentOubliettes(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getOublietteNode(oublietteId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  oubliette.
     *
     *  @param  id an <code> Id </code> 
     *  @param  oublietteId the <code> Id </code> of an oubliette 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> oublietteId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> oublietteId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfOubliette(org.osid.id.Id id, org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.hold.OublietteNodeList parents = getOublietteNode(oublietteId).getParentOublietteNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextOublietteNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given oubliette. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @return the parent <code> Ids </code> of the oubliette 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentOublietteIds(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliette.OublietteToIdList(getParentOubliettes(oublietteId)));
    }


    /**
     *  Gets the parents of the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> to query 
     *  @return the parents of the oubliette 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getParentOubliettes(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliettenode.OublietteNodeToOublietteList(getOublietteNode(oublietteId).getParentOublietteNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  oubliette.
     *
     *  @param  id an <code> Id </code> 
     *  @param  oublietteId the Id of an oubliette 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> oublietteId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfOubliette(org.osid.id.Id id, org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOubliette(id, oublietteId)) {
            return (true);
        }

        try (org.osid.hold.OublietteList parents = getParentOubliettes(oublietteId)) {
            while (parents.hasNext()) {
                if (isAncestorOfOubliette(id, parents.getNextOubliette().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an oubliette has any children. 
     *
     *  @param  oublietteId an oubliette <code> Id </code> 
     *  @return <code> true </code> if the <code> oublietteId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildOubliettes(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOublietteNode(oublietteId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  oubliette.
     *
     *  @param  id an <code> Id </code> 
     *  @param oublietteId the <code> Id </code> of an 
     *         oubliette
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> oublietteId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> oublietteId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfOubliette(org.osid.id.Id id, org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfOubliette(oublietteId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  oubliette.
     *
     *  @param  oublietteId the <code> Id </code> to query 
     *  @return the children of the oubliette 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildOublietteIds(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliette.OublietteToIdList(getChildOubliettes(oublietteId)));
    }


    /**
     *  Gets the children of the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> to query 
     *  @return the children of the oubliette 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getChildOubliettes(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliettenode.OublietteNodeToOublietteList(getOublietteNode(oublietteId).getChildOublietteNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  oubliette.
     *
     *  @param  id an <code> Id </code> 
     *  @param oublietteId the <code> Id </code> of an 
     *         oubliette
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> oublietteId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfOubliette(org.osid.id.Id id, org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfOubliette(oublietteId, id)) {
            return (true);
        }

        try (org.osid.hold.OublietteList children = getChildOubliettes(oublietteId)) {
            while (children.hasNext()) {
                if (isDescendantOfOubliette(id, children.getNextOubliette().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  oubliette.
     *
     *  @param  oublietteId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified oubliette node 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getOublietteNodeIds(org.osid.id.Id oublietteId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.hold.oubliettenode.OublietteNodeToNode(getOublietteNode(oublietteId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given oubliette.
     *
     *  @param  oublietteId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified oubliette node 
     *  @throws org.osid.NotFoundException <code> oublietteId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> oublietteId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteNode getOublietteNodes(org.osid.id.Id oublietteId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOublietteNode(oublietteId));
    }


    /**
     *  Closes this <code>OublietteHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an oubliette node.
     *
     *  @param oublietteId the id of the oubliette node
     *  @throws org.osid.NotFoundException <code>oublietteId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>oublietteId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.hold.OublietteNode getOublietteNode(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(oublietteId, "oubliette Id");
        for (org.osid.hold.OublietteNode oubliette : this.roots) {
            if (oubliette.getId().equals(oublietteId)) {
                return (oubliette);
            }

            org.osid.hold.OublietteNode r = findOubliette(oubliette, oublietteId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(oublietteId + " is not found");
    }


    protected org.osid.hold.OublietteNode findOubliette(org.osid.hold.OublietteNode node, 
                                                        org.osid.id.Id oublietteId) 
	throws org.osid.OperationFailedException {

        try (org.osid.hold.OublietteNodeList children = node.getChildOublietteNodes()) {
            while (children.hasNext()) {
                org.osid.hold.OublietteNode oubliette = children.getNextOublietteNode();
                if (oubliette.getId().equals(oublietteId)) {
                    return (oubliette);
                }
                
                oubliette = findOubliette(oubliette, oublietteId);
                if (oubliette != null) {
                    return (oubliette);
                }
            }
        }

        return (null);
    }
}
