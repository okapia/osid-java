//
// InvariantMapShipmentLookupSession
//
//    Implements a Shipment lookup service backed by a fixed collection of
//    shipments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.shipment;


/**
 *  Implements a Shipment lookup service backed by a fixed
 *  collection of shipments. The shipments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapShipmentLookupSession
    extends net.okapia.osid.jamocha.core.inventory.shipment.spi.AbstractMapShipmentLookupSession
    implements org.osid.inventory.shipment.ShipmentLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapShipmentLookupSession</code> with no
     *  shipments.
     *  
     *  @param warehouse the warehouse
     *  @throws org.osid.NullArgumnetException {@code warehouse} is
     *          {@code null}
     */

    public InvariantMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse) {
        setWarehouse(warehouse);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapShipmentLookupSession</code> with a single
     *  shipment.
     *  
     *  @param warehouse the warehouse
     *  @param shipment a single shipment
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipment} is <code>null</code>
     */

      public InvariantMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                               org.osid.inventory.shipment.Shipment shipment) {
        this(warehouse);
        putShipment(shipment);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapShipmentLookupSession</code> using an array
     *  of shipments.
     *  
     *  @param warehouse the warehouse
     *  @param shipments an array of shipments
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipments} is <code>null</code>
     */

      public InvariantMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                               org.osid.inventory.shipment.Shipment[] shipments) {
        this(warehouse);
        putShipments(shipments);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapShipmentLookupSession</code> using a
     *  collection of shipments.
     *
     *  @param warehouse the warehouse
     *  @param shipments a collection of shipments
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code shipments} is <code>null</code>
     */

      public InvariantMapShipmentLookupSession(org.osid.inventory.Warehouse warehouse,
                                               java.util.Collection<? extends org.osid.inventory.shipment.Shipment> shipments) {
        this(warehouse);
        putShipments(shipments);
        return;
    }
}
