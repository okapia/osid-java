//
// MutableIndexedMapProxyPayerLookupSession
//
//    Implements a Payer lookup service backed by a collection of
//    payers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment;


/**
 *  Implements a Payer lookup service backed by a collection of
 *  payers. The payers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some payers may be compatible
 *  with more types than are indicated through these payer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of payers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyPayerLookupSession
    extends net.okapia.osid.jamocha.core.billing.payment.spi.AbstractIndexedMapPayerLookupSession
    implements org.osid.billing.payment.PayerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPayerLookupSession} with
     *  no payer.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPayerLookupSession(org.osid.billing.Business business,
                                                       org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPayerLookupSession} with
     *  a single payer.
     *
     *  @param business the business
     *  @param  payer an payer
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code payer}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPayerLookupSession(org.osid.billing.Business business,
                                                       org.osid.billing.payment.Payer payer, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putPayer(payer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPayerLookupSession} using
     *  an array of payers.
     *
     *  @param business the business
     *  @param  payers an array of payers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code payers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPayerLookupSession(org.osid.billing.Business business,
                                                       org.osid.billing.payment.Payer[] payers, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putPayers(payers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPayerLookupSession} using
     *  a collection of payers.
     *
     *  @param business the business
     *  @param  payers a collection of payers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code payers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPayerLookupSession(org.osid.billing.Business business,
                                                       java.util.Collection<? extends org.osid.billing.payment.Payer> payers,
                                                       org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putPayers(payers);
        return;
    }

    
    /**
     *  Makes a {@code Payer} available in this session.
     *
     *  @param  payer a payer
     *  @throws org.osid.NullArgumentException {@code payer{@code 
     *          is {@code null}
     */

    @Override
    public void putPayer(org.osid.billing.payment.Payer payer) {
        super.putPayer(payer);
        return;
    }


    /**
     *  Makes an array of payers available in this session.
     *
     *  @param  payers an array of payers
     *  @throws org.osid.NullArgumentException {@code payers{@code 
     *          is {@code null}
     */

    @Override
    public void putPayers(org.osid.billing.payment.Payer[] payers) {
        super.putPayers(payers);
        return;
    }


    /**
     *  Makes collection of payers available in this session.
     *
     *  @param  payers a collection of payers
     *  @throws org.osid.NullArgumentException {@code payer{@code 
     *          is {@code null}
     */

    @Override
    public void putPayers(java.util.Collection<? extends org.osid.billing.payment.Payer> payers) {
        super.putPayers(payers);
        return;
    }


    /**
     *  Removes a Payer from this session.
     *
     *  @param payerId the {@code Id} of the payer
     *  @throws org.osid.NullArgumentException {@code payerId{@code  is
     *          {@code null}
     */

    @Override
    public void removePayer(org.osid.id.Id payerId) {
        super.removePayer(payerId);
        return;
    }    
}
