//
// AbstractStep.java
//
//     Defines a Step.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Step</code>.
 */

public abstract class AbstractStep
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernator
    implements org.osid.workflow.Step {

    private org.osid.workflow.Process process;
    private final java.util.Collection<org.osid.resource.Resource> resources = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.process.State> inputStates = new java.util.LinkedHashSet<>();
    private org.osid.process.State nextState;

    private final java.util.Collection<org.osid.workflow.records.StepRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the process. 
     *
     *  @return the process <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.process.getId());
    }


    /**
     *  Gets the process. 
     *
     *  @return the process 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Process getProcess()
        throws org.osid.OperationFailedException {

        return (this.process);
    }


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @throws org.osid.NullArgumentException
     *          <code>process</code> is <code>null</code>
     */

    protected void setProcess(org.osid.workflow.Process process) {
        nullarg(process, "process");
        this.process = process;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the resources working in this step. 
     *
     *  @return the resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResourceIds() {
        try {
            org.osid.resource.ResourceList resources = getResources();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(resources));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }        
    }


    /**
     *  Gets the resources working in this step. 
     *
     *  @return the resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.resources));
    }


    /**
     *  Adds a resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void addResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resources.add(resource);
        return;
    }


    /**
     *  Sets all the resources.
     *
     *  @param resources a collection of resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    protected void setResources(java.util.Collection<org.osid.resource.Resource> resources) {
        nullarg(resources, "resources");
        this.resources.clear();
        this.resources.addAll(resources);
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the valid states entering this step.
     *
     *  @return the state <code> Ids </code>
     */

    @OSID @Override
    public org.osid.id.IdList getInputStateIds() {
        try {
            org.osid.process.StateList states = getInputStates();
            return (new net.okapia.osid.jamocha.adapter.converter.process.state.StateToIdList(states));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }        
    }


    /**
     *  Gets the valid states to enter this step.
     *
     *  @return the states
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.process.StateList getInputStates()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.process.state.ArrayStateList(this.inputStates));
    }


    /**
     *  Adds an input state.
     *
     *  @param state an input state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void addInputState(org.osid.process.State state) {
        nullarg(state, "input state");
        this.inputStates.add(state);
        return;
    }


    /**
     *  Sets all the input states.
     *
     *  @param states a collection of input states
     *  @throws org.osid.NullArgumentException <code>states</code> is
     *          <code>null</code>
     */

    protected void setInputStates(java.util.Collection<org.osid.process.State> states) {
        nullarg(states, "input states");
        this.inputStates.clear();
        this.inputStates.addAll(states);
        return;
    }


    /**
     *  Gets the <code> Id </code> of the state of the work upon completing 
     *  this step. 
     *
     *  @return the state <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getNextStateId() {
        return (this.nextState.getId());
    }


    /**
     *  Gets the state of the work upon completing this step. 
     *
     *  @return the state 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getNextState()
        throws org.osid.OperationFailedException {

        return (this.nextState);
    }


    /**
     *  Sets the next state.
     *
     *  @param state a next state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    protected void setNextState(org.osid.process.State state) {
        nullarg(state, "next state");
        this.nextState = state;
        return;
    }


    /**
     *  Tests if this step supports the given record
     *  <code>Type</code>.
     *
     *  @param  stepRecordType a step record type 
     *  @return <code>true</code> if the stepRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type stepRecordType) {
        for (org.osid.workflow.records.StepRecord record : this.records) {
            if (record.implementsRecordType(stepRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Step</code>
     *  record <code>Type</code>.
     *
     *  @param  stepRecordType the step record type 
     *  @return the step record 
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.StepRecord getStepRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.StepRecord record : this.records) {
            if (record.implementsRecordType(stepRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param stepRecord the step record
     *  @param stepRecordType step record type
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecord</code> or
     *          <code>stepRecordTypestep</code> is
     *          <code>null</code>
     */
            
    protected void addStepRecord(org.osid.workflow.records.StepRecord stepRecord, 
                                 org.osid.type.Type stepRecordType) {
        
        nullarg(stepRecord, "step record");
        addRecordType(stepRecordType);
        this.records.add(stepRecord);
        
        return;
    }
}
