//
// AbstractRelevancyEnablerSearch.java
//
//     A template for making a RelevancyEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing relevancy enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRelevancyEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.ontology.rules.RelevancyEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.ontology.rules.RelevancyEnablerSearchOrder relevancyEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of relevancy enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  relevancyEnablerIds list of relevancy enablers
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRelevancyEnablers(org.osid.id.IdList relevancyEnablerIds) {
        while (relevancyEnablerIds.hasNext()) {
            try {
                this.ids.add(relevancyEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRelevancyEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of relevancy enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRelevancyEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  relevancyEnablerSearchOrder relevancy enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>relevancyEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRelevancyEnablerResults(org.osid.ontology.rules.RelevancyEnablerSearchOrder relevancyEnablerSearchOrder) {
	this.relevancyEnablerSearchOrder = relevancyEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.ontology.rules.RelevancyEnablerSearchOrder getRelevancyEnablerSearchOrder() {
	return (this.relevancyEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given relevancy enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relevancy enabler implementing the requested record.
     *
     *  @param relevancyEnablerSearchRecordType a relevancy enabler search record
     *         type
     *  @return the relevancy enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerSearchRecord getRelevancyEnablerSearchRecord(org.osid.type.Type relevancyEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.ontology.rules.records.RelevancyEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy enabler search. 
     *
     *  @param relevancyEnablerSearchRecord relevancy enabler search record
     *  @param relevancyEnablerSearchRecordType relevancyEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelevancyEnablerSearchRecord(org.osid.ontology.rules.records.RelevancyEnablerSearchRecord relevancyEnablerSearchRecord, 
                                           org.osid.type.Type relevancyEnablerSearchRecordType) {

        addRecordType(relevancyEnablerSearchRecordType);
        this.records.add(relevancyEnablerSearchRecord);        
        return;
    }
}
