//
// AbstractFederatingOsidSession.java
//
//     A session adapter to federate OsidSessions.
//
//
// Tom Coppeto
// Okapia
// 3 April 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An adapter template for federating OsidSessions.
 *
 *  <p>If any of the underlying sessions are authenticated, the
 *  adapter is also authenticated unless a SessionProxy has been
 *  specified. If no <code>SessionProxy</code> has been specified, the
 *  agents from all underlying sessions are returned in the
 *  <code>Agent</code> list. The default locale types are used for the
 *  adapter unless either a <codeSessionProxy</code> or
 *  <code>setLocale()</code> is supplied.</p>
 */

public abstract class AbstractFederatingOsidSession<T extends org.osid.OsidSession> 
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.OsidSession,
               org.osid.transaction.Transaction {
    
    private final java.util.Collection<T> sessions = java.util.Collections.synchronizedSet(new java.util.LinkedHashSet<T>());
    private final java.util.Collection<org.osid.transaction.Transaction> transactions = new java.util.ArrayList<>();

    private org.osid.transaction.TransactionState transactionState;
    private boolean selectFirst = false;
    
    
    /**
     *  Gets the federated OsidSessions.
     *
     *  @return the OsidSessions
     */
    
    protected java.util.Collection<T> getSessions() {
        return (java.util.Collections.unmodifiableCollection(this.sessions));
    }
    

    /**
     *  Tests for the availability of transactions. Transactions are available
     *  if all underlying sessions support transactions.
     *
     *  @return <code> true </code> if transaction methods are available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactions() {
        for (org.osid.OsidSession session : getSessions()) {
            if (!session.supportsTransactions()) {
                return (false);
            }
        }       

        return (true);
    }


    /**
     *  Starts a new transaction for this sesson. Transactions are a
     *  means for an OSID to provide an all-or-nothing set of
     *  operations within a session and may be used to coordinate this
     *  service from an external transaction manager. A session
     *  supports one transaction at a time.  Starting a second
     *  transaction before the previous has been committed or aborted
     *  results in an <code>ILLEGAL_STATE</code> error.
     *
     *  @return a new transaction 
     *  @throws org.osid.IllegalStateException a transaction is already open 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException transactions not supported 
     */

    @OSID @Override
    public org.osid.transaction.Transaction startTransaction()
        throws org.osid.IllegalStateException,
          org.osid.OperationFailedException,
        org.osid.UnsupportedException {

        if (!supportsTransactions()) {
            throw new org.osid.UnimplementedException("a transaction service is not available");
        }
        
        this.transactions.clear();

        try {
            for (org.osid.OsidSession session : getSessions()) {
                if (session.supportsTransactions()) {
                    this.transactions.add(session.startTransaction());
                }
            }
        } catch (Exception e) {
            abort();
            throw new org.osid.OperationFailedException("transaction aborted", e);
        }
        
        this.transactionState = org.osid.transaction.TransactionState.START;
        return (this);
    }


    /**
     *  Prepares for a <code>commit</code>. No further operations are
     *  permitted in the associated manager until this transaction is
     *  committed or aborted.
     *
     *  @throws org.osid.IllegalStateException this transaction has
     *          been committed or aborted
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.TransactionFailureException this transaction
     *          cannot proceed due to a bad transaction element
     */

    @OSID @Override
    public void prepare()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException,
               org.osid.TransactionFailureException {

        for (org.osid.transaction.Transaction transaction : this.transactions) {
            if (transaction.getState() != org.osid.transaction.TransactionState.START) {
                abort();
                throw new org.osid.TransactionFailureException("transaction in inavlid state");
            }
        }
        
        try {
            for (org.osid.transaction.Transaction transaction : this.transactions) {
                transaction.prepare();
            }
        } catch (Exception e) {
            abort();
            throw new org.osid.TransactionFailureException("transaction aborted", e);
        }

        this.transactionState = org.osid.transaction.TransactionState.COMMIT_READY;
        return;
    }


    /**
     *  Commits the transaction and makes the state change(s) visible. This 
     *  transaction is effectively closed and the only valid method that may 
     *  be invoked is <code> getState(). </code> 
     *
     *  @throws org.osid.IllegalStateException this transaction has been 
     *          committed or aborted 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void commit()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        for (org.osid.transaction.Transaction transaction : this.transactions) {
            if (transaction.getState() != org.osid.transaction.TransactionState.COMMIT_READY) {
                abort();
                throw new org.osid.OperationFailedException("transaction in inavlid state");
            }
        }

        try {
            for (org.osid.transaction.Transaction transaction : this.transactions) {
                transaction.commit();
            }
        } finally {
            this.transactions.clear();
        }

        this.transactionState = org.osid.transaction.TransactionState.COMMITTED;
        return;
    }


    /**
     *  Cancels this transaction, rolling back the queue of operations
     *  since the start of this transaction. This transaction is
     *  effectively closed and the only valid method that may be
     *  invoked is <code>getState()</code>.
     *
     *  @throws org.osid.IllegalStateException this transaction has been 
     *          committed or aborted 
     */

    @OSID @Override     
    public void abort() {

        try {
            for (org.osid.transaction.Transaction transaction : this.transactions) {
                if ((transaction.getState() != org.osid.transaction.TransactionState.ABORTED) && 
                    (transaction.getState() != org.osid.transaction.TransactionState.COMMITTED)) {
                    transaction.abort();
                }
            }
        } finally {
            this.transactions.clear();
            this.transactionState = org.osid.transaction.TransactionState.ABORTED;
        }

        return;
    }


    /**
     *  Gets the current state of this transaction. 
     *
     *  @return the current state of this transaction 
     */

    @OSID @Override
    public org.osid.transaction.TransactionState getState() {
        return (this.transactionState);
    }


    /**
     *  Instructs the provider to select the results from the first
     *  session to have them.
     */

    protected void selectFirst() {
        this.selectFirst = true;
        return;
    }


    /**
     *  Instructs the provider to search for and merge results from
     *  all sessions.
     */

    protected void selectAll() {
        this.selectFirst = false;
        return;
    }

    
    /**
     *  Tests if results from all sessions should be merged.
     *
     *  @return <code>true</code> to merge rsults from all sessions,
     *          <code>false</code> to return results from the first
     *          hit
     */

    protected boolean useAllResults() {
        return (!this.selectFirst);
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSID @Override
    public void close() {
        for (org.osid.OsidSession session : getSessions()) {
            session.close();
        }

        abort();
        this.transactions.clear();

        this.sessions.clear();
        super.close();
        return;
    }

    
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(T session) {
        nullarg(session, "session");
        this.sessions.add(session);
        return;
    }



    /**
     *  Removes a session from this federation.
     *
     *  @param session a session to remove
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void removeSession(T session) {
        nullarg(session, "session");
        this.sessions.remove(session);
        return;
    }
}


    
	
    
