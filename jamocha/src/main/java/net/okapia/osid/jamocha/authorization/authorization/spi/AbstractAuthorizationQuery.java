//
// AbstractAuthorizationQuery.java
//
//     A template for making an Authorization Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for authorizations.
 */

public abstract class AbstractAuthorizationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.authorization.AuthorizationQuery {

    private final java.util.Collection<org.osid.authorization.records.AuthorizationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches explciit authorizations. 
     *
     *  @param  match <code> true </code> to match explicit authorizations, 
     *          <code> false </code> to match implciit authorizations 
     */

    @OSID @Override
    public void matchExplicitAuthorizations(boolean match) {
        return;
    }


    /**
     *  Clears the explicit authorization query terms. 
     */

    @OSID @Override
    public void clearExplicitAuthorizationsTerms() {
        return;
    }


    /**
     *  Adds an <code> Id </code> to match explicit or implicitly related 
     *  authorizations depending on <code> matchExplicitAuthorizations(). 
     *  </code> Multiple <code> Ids </code> can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelatedAuthorizationId(org.osid.id.Id id, boolean match) {
        return;
    }


    /**
     *  Clears the related authorization <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRelatedAuthorizationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuthorizationQuery </code> is available. 
     *
     *  @return <code> true </code> if an authorization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelatedAuthorizationQuery() {
        return (false);
    }


    /**
     *  Gets the authorization query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AuthorizationQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelatedAuthorizationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQuery getRelatedAuthorizationQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsRelatedAuthroizationQuery() is false");
    }


    /**
     *  Clears the related authorization query terms. 
     */

    @OSID @Override
    public void clearRelatedAuthorizationTerms() {
        return;
    }


    /**
     *  Matches the resource identified by the given <code> Id. </code> 
     *
     *  @param  resourceId the <code> Id </code> of the <code> Resource 
     *          </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the resource query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> ResourceQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches authorizations that have any resource. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          resource, <code> false </code> to match authorizations with no 
     *          resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches the trust identified by the given <code> Id. </code> 
     *
     *  @param  trustId the <code> Id </code> of the <code> Trust </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> trustId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTrustId(org.osid.id.Id trustId, boolean match) {
        return;
    }


    /**
     *  Matches authorizations that have any trust defined. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          trust, <code> false </code> to match authorizations with no 
     *          trusts 
     */

    @OSID @Override
    public void matchAnyTrustId(boolean match) {
        return;
    }


    /**
     *  Clears the trust <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTrustIdTerms() {
        return;
    }


    /**
     *  Matches the agent identified by the given <code> Id. </code> 
     *
     *  @param  agentId the Id of the <code> Agent </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the agent query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AgentQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches authorizations that have any agent. 
     *
     *  @param  match <code> true </code> to match authorizations with any 
     *          agent, <code> false </code> to match authorizations with no 
     *          agent 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        return;
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Matches the function identified by the given <code> Id. </code> 
     *
     *  @param  functionId the Id of the <code> Function </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> functionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFunctionId(org.osid.id.Id functionId, boolean match) {
        return;
    }


    /**
     *  Clears the function <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFunctionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FunctionQuery </code> is available. 
     *
     *  @return <code> true </code> if a function query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionQuery() {
        return (false);
    }


    /**
     *  Gets the function query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> FunctinQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsFunctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionQuery getFunctionQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsFunctionQuery() is false");
    }


    /**
     *  Clears the function query terms. 
     */

    @OSID @Override
    public void clearFunctionTerms() {
        return;
    }


    /**
     *  Matches the qualifier identified by the given <code> Id. </code> 
     *
     *  @param  qualifierId the Id of the <code> Qualifier </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQualifierId(org.osid.id.Id qualifierId, boolean match) {
        return;
    }


    /**
     *  Clears the qualifier <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQualifierIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QualifierQuery </code> is available. 
     *
     *  @return <code> true </code> if a qualifier query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierQuery() {
        return (false);
    }


    /**
     *  Gets the qualiier query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> QualifierQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierQuery getQualifierQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsQualifierQuery() is false");
    }


    /**
     *  Clears the qualifier query terms. 
     */

    @OSID @Override
    public void clearQualifierTerms() {
        return;
    }


    /**
     *  Sets the vault <code> Id </code> for this query. 
     *
     *  @param  vaultId a vault <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVaultId(org.osid.id.Id vaultId, boolean match) {
        return;
    }


    /**
     *  Clears the vault <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVaultIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> VaultQuery </code> is available. 
     *
     *  @return <code> true </code> if a vault query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultQuery() {
        return (false);
    }


    /**
     *  Gets the query for a vault. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the vault query 
     *  @throws org.osid.UnimplementedException <code> supportsVaultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.VaultQuery getVaultQuery() {
        throw new org.osid.UnimplementedException("supportsVaultQuery() is false");
    }


    /**
     *  Clears the vault query terms. 
     */

    @OSID @Override
    public void clearVaultTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given authorization query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an authorization implementing the requested record.
     *
     *  @param authorizationRecordType an authorization record type
     *  @return the authorization query record
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(authorizationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationQueryRecord getAuthorizationQueryRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationQueryRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this authorization query. 
     *
     *  @param authorizationQueryRecord authorization query record
     *  @param authorizationRecordType authorization record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuthorizationQueryRecord(org.osid.authorization.records.AuthorizationQueryRecord authorizationQueryRecord, 
                                          org.osid.type.Type authorizationRecordType) {

        addRecordType(authorizationRecordType);
        nullarg(authorizationQueryRecord, "authorization query record");
        this.records.add(authorizationQueryRecord);        
        return;
    }
}
