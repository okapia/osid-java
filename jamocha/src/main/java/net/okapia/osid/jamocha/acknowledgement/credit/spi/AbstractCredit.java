//
// AbstractCredit.java
//
//     Defines a Credit.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Credit</code>.
 */

public abstract class AbstractCredit
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.acknowledgement.Credit {

    private org.osid.id.Id referenceId;
    private org.osid.resource.Resource resource;

    private final java.util.Collection<org.osid.acknowledgement.records.CreditRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the referenced object. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.referenceId);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    protected void setReferenceId(org.osid.id.Id referenceId) {
        nullarg(referenceId, "reference Id");
        this.referenceId = referenceId;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource in this credit. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource in this credit. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this credit supports the given record
     *  <code>Type</code>.
     *
     *  @param  creditRecordType a credit record type 
     *  @return <code>true</code> if the creditRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type creditRecordType) {
        for (org.osid.acknowledgement.records.CreditRecord record : this.records) {
            if (record.implementsRecordType(creditRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Credit</code>
     *  record <code>Type</code>.
     *
     *  @param  creditRecordType the credit record type 
     *  @return the credit record 
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable
     *          to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditRecord getCreditRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.CreditRecord record : this.records) {
            if (record.implementsRecordType(creditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credit. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param creditRecord the credit record
     *  @param creditRecordType credit record type
     *  @throws org.osid.NullArgumentException
     *          <code>creditRecord</code> or
     *          <code>creditRecordType</code> is <code>null</code>
     */
            
    protected void addCreditRecord(org.osid.acknowledgement.records.CreditRecord creditRecord, 
                                   org.osid.type.Type creditRecordType) {

        nullarg(creditRecord, "credit record");
        addRecordType(creditRecordType);
        this.records.add(creditRecord);
        
        return;
    }
}
