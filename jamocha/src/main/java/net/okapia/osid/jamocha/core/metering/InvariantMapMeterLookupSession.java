//
// InvariantMapMeterLookupSession
//
//    Implements a Meter lookup service backed by a fixed collection of
//    meters.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering;


/**
 *  Implements a Meter lookup service backed by a fixed
 *  collection of meters. The meters are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapMeterLookupSession
    extends net.okapia.osid.jamocha.core.metering.spi.AbstractMapMeterLookupSession
    implements org.osid.metering.MeterLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapMeterLookupSession</code> with no
     *  meters.
     *  
     *  @param utility the utility
     *  @throws org.osid.NullArgumnetException {@code utility} is
     *          {@code null}
     */

    public InvariantMapMeterLookupSession(org.osid.metering.Utility utility) {
        setUtility(utility);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMeterLookupSession</code> with a single
     *  meter.
     *  
     *  @param utility the utility
     *  @param meter a single meter
     *  @throws org.osid.NullArgumentException {@code utility} or
     *          {@code meter} is <code>null</code>
     */

      public InvariantMapMeterLookupSession(org.osid.metering.Utility utility,
                                               org.osid.metering.Meter meter) {
        this(utility);
        putMeter(meter);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMeterLookupSession</code> using an array
     *  of meters.
     *  
     *  @param utility the utility
     *  @param meters an array of meters
     *  @throws org.osid.NullArgumentException {@code utility} or
     *          {@code meters} is <code>null</code>
     */

      public InvariantMapMeterLookupSession(org.osid.metering.Utility utility,
                                               org.osid.metering.Meter[] meters) {
        this(utility);
        putMeters(meters);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapMeterLookupSession</code> using a
     *  collection of meters.
     *
     *  @param utility the utility
     *  @param meters a collection of meters
     *  @throws org.osid.NullArgumentException {@code utility} or
     *          {@code meters} is <code>null</code>
     */

      public InvariantMapMeterLookupSession(org.osid.metering.Utility utility,
                                               java.util.Collection<? extends org.osid.metering.Meter> meters) {
        this(utility);
        putMeters(meters);
        return;
    }
}
