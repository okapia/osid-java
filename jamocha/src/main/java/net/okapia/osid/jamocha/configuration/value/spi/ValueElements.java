//
// ValueElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.value.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ValueElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the ValueElement Id.
     *
     *  @return the value element Id
     */

    public static org.osid.id.Id getValueEntityId() {
        return (makeEntityId("osid.configuration.Value"));
    }


    /**
     *  Gets the ParameterId element Id.
     *
     *  @return the ParameterId element Id
     */

    public static org.osid.id.Id getParameterId() {
        return (makeElementId("osid.configuration.value.ParameterId"));
    }


    /**
     *  Gets the Parameter element Id.
     *
     *  @return the Parameter element Id
     */

    public static org.osid.id.Id getParameter() {
        return (makeElementId("osid.configuration.value.Parameter"));
    }


    /**
     *  Gets the Priority element Id.
     *
     *  @return the Priority element Id
     */

    public static org.osid.id.Id getPriority() {
        return (makeElementId("osid.configuration.value.Priority"));
    }


    /**
     *  Gets the BooleanValue element Id.
     *
     *  @return the BooleanValue element Id
     */

    public static org.osid.id.Id getBooleanValue() {
        return (makeElementId("osid.configuration.value.BooleanValue"));
    }


    /**
     *  Gets the BytesValue element Id.
     *
     *  @return the BytesValue element Id
     */

    public static org.osid.id.Id getBytesValue() {
        return (makeElementId("osid.configuration.value.BytesValue"));
    }


    /**
     *  Gets the CardinalValue element Id.
     *
     *  @return the CardinalValue element Id
     */

    public static org.osid.id.Id getCardinalValue() {
        return (makeElementId("osid.configuration.value.CardinalValue"));
    }


    /**
     *  Gets the CoordinateValue element Id.
     *
     *  @return the CoordinateValue element Id
     */

    public static org.osid.id.Id getCoordinateValue() {
        return (makeElementId("osid.configuration.value.CoordinateValue"));
    }


    /**
     *  Gets the CurrencyValue element Id.
     *
     *  @return the CurrencyValue element Id
     */

    public static org.osid.id.Id getCurrencyValue() {
        return (makeElementId("osid.configuration.value.CurrencyValue"));
    }


    /**
     *  Gets the DateTimeValue element Id.
     *
     *  @return the DateTimeValue element Id
     */

    public static org.osid.id.Id getDateTimeValue() {
        return (makeElementId("osid.configuration.value.DateTimeValue"));
    }


    /**
     *  Gets the DecimalValue element Id.
     *
     *  @return the DecimalValue element Id
     */

    public static org.osid.id.Id getDecimalValue() {
        return (makeElementId("osid.configuration.value.DecimalValue"));
    }


    /**
     *  Gets the DistanceValue element Id.
     *
     *  @return the DistanceValue element Id
     */

    public static org.osid.id.Id getDistanceValue() {
        return (makeElementId("osid.configuration.value.DistanceValue"));
    }


    /**
     *  Gets the DurationValue element Id.
     *
     *  @return the DurationValue element Id
     */

    public static org.osid.id.Id getDurationValue() {
        return (makeElementId("osid.configuration.value.DurationValue"));
    }


    /**
     *  Gets the HeadingValue element Id.
     *
     *  @return the HeadingValue element Id
     */

    public static org.osid.id.Id getHeadingValue() {
        return (makeElementId("osid.configuration.value.HeadingValue"));
    }


    /**
     *  Gets the IdValue element Id.
     *
     *  @return the IdValue element Id
     */

    public static org.osid.id.Id getIdValue() {
        return (makeElementId("osid.configuration.value.IdValue"));
    }


    /**
     *  Gets the IntegerValue element Id.
     *
     *  @return the IntegerValue element Id
     */

    public static org.osid.id.Id getIntegerValue() {
        return (makeElementId("osid.configuration.value.IntegerValue"));
    }


    /**
     *  Gets the ObjectValue element Id.
     *
     *  @return the ObjectValue element Id
     */

    public static org.osid.id.Id getObjectValue() {
        return (makeElementId("osid.configuration.value.ObjectValue"));
    }


    /**
     *  Gets the SpatialUnitValue element Id.
     *
     *  @return the SpatialUnitValue element Id
     */

    public static org.osid.id.Id getSpatialUnitValue() {
        return (makeElementId("osid.configuration.value.SpatialUnitValue"));
    }


    /**
     *  Gets the SpeedValue element Id.
     *
     *  @return the SpeedValue element Id
     */

    public static org.osid.id.Id getSpeedValue() {
        return (makeElementId("osid.configuration.value.SpeedValue"));
    }


    /**
     *  Gets the StringValue element Id.
     *
     *  @return the StringValue element Id
     */

    public static org.osid.id.Id getStringValue() {
        return (makeElementId("osid.configuration.value.StringValue"));
    }


    /**
     *  Gets the TimeValue element Id.
     *
     *  @return the TimeValue element Id
     */

    public static org.osid.id.Id getTimeValue() {
        return (makeElementId("osid.configuration.value.TimeValue"));
    }


    /**
     *  Gets the TypeValue element Id.
     *
     *  @return the TypeValue element Id
     */

    public static org.osid.id.Id getTypeValue() {
        return (makeElementId("osid.configuration.value.TypeValue"));
    }


    /**
     *  Gets the VersionValue element Id.
     *
     *  @return the VersionValue element Id
     */

    public static org.osid.id.Id getVersionValue() {
        return (makeElementId("osid.configuration.value.VersionValue"));
    }


    /**
     *  Gets the ObjectValueType element Id.
     *
     *  @return the ObjectValueType element Id
     */

    public static org.osid.id.Id getObjectValueType() {
        return (makeQueryElementId("osid.configuration.value.ObjectValueType"));
    }


    /**
     *  Gets the ConfigurationId element Id.
     *
     *  @return the ConfigurationId element Id
     */

    public static org.osid.id.Id getConfigurationId() {
        return (makeQueryElementId("osid.configuration.value.ConfigurationId"));
    }


    /**
     *  Gets the Configuration element Id.
     *
     *  @return the Configuration element Id
     */

    public static org.osid.id.Id getConfiguration() {
        return (makeQueryElementId("osid.configuration.value.Configuration"));
    }


    /**
     *  Gets the Value element Id.
     *
     *  @return the Value element Id
     */

    public static org.osid.id.Id getValue() {
        return (makeSearchOrderElementId("osid.configuration.value.Value"));
    }
}
