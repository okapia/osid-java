//
// AbstractEdgeEnablerNotificationSession.java
//
//     A template for making EdgeEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code EdgeEnabler} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code EdgeEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for edge enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractEdgeEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.rules.EdgeEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();


    /**
     *  Gets the {@code Graph} {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.graph.getId());
    }

    
    /**
     *  Gets the {@code Graph} associated with this 
     *  session.
     *
     *  @return the {@code Graph} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.graph);
    }


    /**
     *  Sets the {@code Graph}.
     *
     *  @param  graph the graph for this session
     *  @throws org.osid.NullArgumentException {@code graph}
     *          is {@code null}
     */

    protected void setGraph(org.osid.topology.Graph graph) {
        nullarg(graph, "graph");
        this.graph = graph;
        return;
    }


    /**
     *  Tests if this user can register for {@code EdgeEnabler}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a
     *  {@code PERMISSION_DENIED}. This is intended as a hint to
     *  an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForEdgeEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgeEdgeEnablerNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliableEdgeEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableEdgeEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge an edge enabler notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeEdgeEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edge enablers in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new
     *  edge enablers. {@code EdgeEnablerReceiver.newEdgeEnabler()} is
     *  invoked when an new {@code EdgeEnabler} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated
     *  edge enablers. {@code EdgeEnablerReceiver.changedEdgeEnabler()} is
     *  invoked when an edge enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated
     *  edge enabler. {@code EdgeEnablerReceiver.changedEdgeEnabler()} is
     *  invoked when the specified edge enabler is changed.
     *
     *  @param edgeEnablerId the {@code Id} of the {@code EdgeEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code edgeEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted
     *  edge enablers. {@code EdgeEnablerReceiver.deletedEdgeEnabler()} is
     *  invoked when an edge enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted
     *  edge enabler. {@code EdgeEnablerReceiver.deletedEdgeEnabler()} is
     *  invoked when the specified edge enabler is deleted.
     *
     *  @param edgeEnablerId the {@code Id} of the
     *          {@code EdgeEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code edgeEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }

}
