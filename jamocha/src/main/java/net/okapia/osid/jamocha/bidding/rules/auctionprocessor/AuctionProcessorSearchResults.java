//
// AuctionProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessor;


/**
 *  A template for implementing a search results.
 */

public final class AuctionProcessorSearchResults
    extends net.okapia.osid.jamocha.bidding.rules.auctionprocessor.spi.AbstractAuctionProcessorSearchResults
    implements org.osid.bidding.rules.AuctionProcessorSearchResults {


    /**
     *  Constructs a new <code>AuctionProcessorSearchResults.
     *
     *  @param auctionProcessors the result set
     *  @param auctionProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auctionProcessors</code>
     *          or <code>auctionProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    public AuctionProcessorSearchResults(org.osid.bidding.rules.AuctionProcessorList auctionProcessors,
                                 org.osid.bidding.rules.AuctionProcessorQueryInspector auctionProcessorQueryInspector) {
        super(auctionProcessors, auctionProcessorQueryInspector);
        return;
    }
}
