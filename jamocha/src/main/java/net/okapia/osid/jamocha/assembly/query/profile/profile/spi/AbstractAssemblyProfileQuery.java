//
// AbstractAssemblyProfileQuery.java
//
//     A ProfileQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.profile.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileQuery that stores terms.
 */

public abstract class AbstractAssemblyProfileQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.profile.ProfileQuery,
               org.osid.profile.ProfileQueryInspector,
               org.osid.profile.ProfileSearchOrder {

    private final java.util.Collection<org.osid.profile.records.ProfileQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProfileQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProfileQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the profile item <code> Id </code> for this query. 
     *
     *  @param  profileItemId a profile item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileItemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileItemId(org.osid.id.Id profileItemId, boolean match) {
        getAssembler().addIdTerm(getProfileItemIdColumn(), profileItemId, match);
        return;
    }


    /**
     *  Clears the profile item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileItemIdTerms() {
        getAssembler().clearTerms(getProfileItemIdColumn());
        return;
    }


    /**
     *  Gets the profile item <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileItemIdTerms() {
        return (getAssembler().getIdTerms(getProfileItemIdColumn()));
    }


    /**
     *  Gets the ProfileItemId column name.
     *
     * @return the column name
     */

    protected String getProfileItemIdColumn() {
        return ("profile_item_id");
    }


    /**
     *  Tests if a <code> ProfileItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile item query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile item. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile item query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuery getProfileItemQuery() {
        throw new org.osid.UnimplementedException("supportsProfileItemQuery() is false");
    }


    /**
     *  Matches profiles that have any profile item. 
     *
     *  @param  match <code> true </code> to match profiles with any item 
     *          mapping, <code> false </code> to match profiles with no item 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileItem(boolean match) {
        getAssembler().addIdWildcardTerm(getProfileItemColumn(), match);
        return;
    }


    /**
     *  Clears the profile item query terms. 
     */

    @OSID @Override
    public void clearProfileItemTerms() {
        getAssembler().clearTerms(getProfileItemColumn());
        return;
    }


    /**
     *  Gets the profile item query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQueryInspector[] getProfileItemTerms() {
        return (new org.osid.profile.ProfileItemQueryInspector[0]);
    }


    /**
     *  Gets the ProfileItem column name.
     *
     * @return the column name
     */

    protected String getProfileItemColumn() {
        return ("profile_item");
    }


    /**
     *  Sets the profile entry <code> Id </code> for this query. 
     *
     *  @param  profileEntryId a profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProfileEntryId(org.osid.id.Id profileEntryId, 
                                    boolean match) {
        getAssembler().addIdTerm(getProfileEntryIdColumn(), profileEntryId, match);
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileEntryIdTerms() {
        getAssembler().clearTerms(getProfileEntryIdColumn());
        return;
    }


    /**
     *  Gets the profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileEntryIdTerms() {
        return (getAssembler().getIdTerms(getProfileEntryIdColumn()));
    }


    /**
     *  Gets the ProfileEntryId column name.
     *
     * @return the column name
     */

    protected String getProfileEntryIdColumn() {
        return ("profile_entry_id");
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsProfileEntryQuery() is false");
    }


    /**
     *  Matches profiles that have any profile entry. 
     *
     *  @param  match <code> true </code> to match profiles with any entry 
     *          mapping, <code> false </code> to match profiles with no entry 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getProfileEntryColumn(), match);
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileEntryTerms() {
        getAssembler().clearTerms(getProfileEntryColumn());
        return;
    }


    /**
     *  Gets the profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the ProfileEntry column name.
     *
     * @return the column name
     */

    protected String getProfileEntryColumn() {
        return ("profile_entry");
    }


    /**
     *  Sets the profile <code> Id </code> for this query to match profiles 
     *  that have the specified profile as an ancestor. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorProfileId(org.osid.id.Id profileId, boolean match) {
        getAssembler().addIdTerm(getAncestorProfileIdColumn(), profileId, match);
        return;
    }


    /**
     *  Clears the ancestor profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorProfileIdTerms() {
        getAssembler().clearTerms(getAncestorProfileIdColumn());
        return;
    }


    /**
     *  Gets the ancestor profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorProfileIdTerms() {
        return (getAssembler().getIdTerms(getAncestorProfileIdColumn()));
    }


    /**
     *  Gets the AncestorProfileId column name.
     *
     * @return the column name
     */

    protected String getAncestorProfileIdColumn() {
        return ("ancestor_profile_id");
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorProfileQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getAncestorProfileQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorProfileQuery() is false");
    }


    /**
     *  Matches profiles with any ancestor. 
     *
     *  @param  match <code> true </code> to match profile with any ancestor, 
     *          <code> false </code> to match root profiles 
     */

    @OSID @Override
    public void matchAnyAncestorProfile(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorProfileColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor profile query terms. 
     */

    @OSID @Override
    public void clearAncestorProfileTerms() {
        getAssembler().clearTerms(getAncestorProfileColumn());
        return;
    }


    /**
     *  Gets the ancestor profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getAncestorProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the AncestorProfile column name.
     *
     * @return the column name
     */

    protected String getAncestorProfileColumn() {
        return ("ancestor_profile");
    }


    /**
     *  Sets the profile <code> Id </code> for this query to match that have 
     *  the specified profile as a descendant. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantProfileId(org.osid.id.Id profileId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantProfileIdColumn(), profileId, match);
        return;
    }


    /**
     *  Clears the descendant profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantProfileIdTerms() {
        getAssembler().clearTerms(getDescendantProfileIdColumn());
        return;
    }


    /**
     *  Gets the descendant profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantProfileIdTerms() {
        return (getAssembler().getIdTerms(getDescendantProfileIdColumn()));
    }


    /**
     *  Gets the DescendantProfileId column name.
     *
     * @return the column name
     */

    protected String getDescendantProfileIdColumn() {
        return ("descendant_profile_id");
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantProfileQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getDescendantProfileQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantProfileQuery() is false");
    }


    /**
     *  Matches profiles with any descendant. 
     *
     *  @param  match <code> true </code> to match profile with any 
     *          descendant, <code> false </code> to match leaf profiles 
     */

    @OSID @Override
    public void matchAnyDescendantProfile(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantProfileColumn(), match);
        return;
    }


    /**
     *  Clears the descendant profile query terms. 
     */

    @OSID @Override
    public void clearDescendantProfileTerms() {
        getAssembler().clearTerms(getDescendantProfileColumn());
        return;
    }


    /**
     *  Gets the descendant profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getDescendantProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the DescendantProfile column name.
     *
     * @return the column name
     */

    protected String getDescendantProfileColumn() {
        return ("descendant_profile");
    }


    /**
     *  Tests if this profile supports the given record
     *  <code>Type</code>.
     *
     *  @param  profileRecordType a profile record type 
     *  @return <code>true</code> if the profileRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileRecordType) {
        for (org.osid.profile.records.ProfileQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  profileRecordType the profile record type 
     *  @return the profile query record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileQueryRecord getProfileQueryRecord(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  profileRecordType the profile record type 
     *  @return the profile query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileQueryInspectorRecord getProfileQueryInspectorRecord(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(profileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param profileRecordType the profile record type
     *  @return the profile search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileSearchOrderRecord getProfileSearchOrderRecord(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(profileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this profile. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileQueryRecord the profile query record
     *  @param profileQueryInspectorRecord the profile query inspector
     *         record
     *  @param profileSearchOrderRecord the profile search order record
     *  @param profileRecordType profile record type
     *  @throws org.osid.NullArgumentException
     *          <code>profileQueryRecord</code>,
     *          <code>profileQueryInspectorRecord</code>,
     *          <code>profileSearchOrderRecord</code> or
     *          <code>profileRecordTypeprofile</code> is
     *          <code>null</code>
     */
            
    protected void addProfileRecords(org.osid.profile.records.ProfileQueryRecord profileQueryRecord, 
                                      org.osid.profile.records.ProfileQueryInspectorRecord profileQueryInspectorRecord, 
                                      org.osid.profile.records.ProfileSearchOrderRecord profileSearchOrderRecord, 
                                      org.osid.type.Type profileRecordType) {

        addRecordType(profileRecordType);

        nullarg(profileQueryRecord, "profile query record");
        nullarg(profileQueryInspectorRecord, "profile query inspector record");
        nullarg(profileSearchOrderRecord, "profile search odrer record");

        this.queryRecords.add(profileQueryRecord);
        this.queryInspectorRecords.add(profileQueryInspectorRecord);
        this.searchOrderRecords.add(profileSearchOrderRecord);
        
        return;
    }
}
