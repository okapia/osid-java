//
// AbstractBrokerProcessor.java
//
//     Defines a BrokerProcessor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>BrokerProcessor</code>.
 */

public abstract class AbstractBrokerProcessor
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessor
    implements org.osid.provisioning.rules.BrokerProcessor {

    private boolean leasing                 = false;
    private boolean mustReturnProvisions    = false;
    private boolean allowsProvisionExchange = false;
    private boolean allowsCompoundRequests  = false;

    private org.osid.calendaring.Duration fixedLeaseDuration;

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this broker provides leases. A lease is a temporary provision 
     *  that expires or must be returned. 
     *
     *  @return <code>true</code> if this is a leasing broker,
     *          <code>false</code> if the brokered provisions are
     *          permanent
     */

    @OSID @Override
    public boolean isLeasing() {
        return (this.leasing);
    }


    /**
     *  Sets the leasing flag.
     *
     *  @param leasing <code>true</code> if this is a leasing broker,
     *          <code>false</code> if the brokered provisions are
     *          permanent
     */

    protected void setLeasing(boolean leasing) {
        this.leasing = leasing;
        return;
    }


    /**
     *  Tests if leases from this broker have fixed durations. 
     *
     *  @return true if leases have fixed durations, false otherwise 
     *  @throws org.osid.IllegalStateException <code> isLeasing() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean hasFixedLeaseDuration() {
        if (!isLeasing()) {
            throw new org.osid.IllegalStateException("isLeasing() is false");
        }

        return (this.fixedLeaseDuration != null);
    }


    /**
     *  Gets the fixed lease duration. 
     *
     *  @return the fixed lease duration 
     *  @throws org.osid.IllegalStateException <code> hasFixedLeaseDuration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getFixedLeaseDuration() {
        if (!hasFixedLeaseDuration()) {
            throw new org.osid.IllegalStateException("hasFixedLeaseDuration() is false");
        }

        return (this.fixedLeaseDuration);
    }


    /**
     *  Sets the fixed lease duration.
     *
     *  @param duration a fixed lease duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setFixedLeaseDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.fixedLeaseDuration = duration;
        return;
    }


    /**
     *  Tests if provisions from this broker must be returned. 
     *
     *  @return <code>true<code> if this broker requires provision
     *          return, <code>false</code> otherwise
     *  @throws org.osid.IllegalStateException <code> isLeasing()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public boolean mustReturnProvisions() {
        if (!isLeasing()) {
            throw new org.osid.IllegalStateException("isLeasing() is false");
        }

        return (this.mustReturnProvisions);
    }


    /**
     *  Sets the must return provisions.
     *
     *  @param must <code>true<code> if this broker requires provision
     *          return, <code>false</code> otherwise
     */

    protected void setMustReturnProvisions(boolean must) {
        this.mustReturnProvisions = must;
        return;
    }


    /**
     *  Tests if this broker allows exchanging of provisions. 
     *
     *  @return <code>true</code> if provision exchange is permitted,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean allowsProvisionExchange() {
        return (this.allowsProvisionExchange);
    }


    /**
     *  Sets the allows provision exchange.
     *
     *  @param allows <code>true</code> if provision exchange is
     *          permitted, <code>false</code> otherwise
     */

    protected void setAllowsProvisionExchange(boolean allows) {
        this.allowsProvisionExchange = allows;
        return;
    }


    /**
     *  Tests if this broker allows compound requests using <code> 
     *  RequestTransactions. </code> 
     *
     *  @return <code>true</code> if compound requests are permitted,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean allowsCompoundRequests() {
        return (this.allowsCompoundRequests);
    }


    /**
     *  Sets the allows compound requests.
     *
     *  @param allows <code>true</code> if compound requests are
     *          permitted, <code>false</code> otherwise
     */

    protected void setAllowsCompoundRequests(boolean allows) {
        this.allowsCompoundRequests = allows;
        return;
    }


    /**
     *  Tests if this brokerProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  brokerProcessorRecordType a broker processor record type 
     *  @return <code>true</code> if the brokerProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type brokerProcessorRecordType) {
        for (org.osid.provisioning.rules.records.BrokerProcessorRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>BrokerProcessor</code> record <code>Type</code>.
     *
     *  @param  brokerProcessorRecordType the broker processor record type 
     *  @return the broker processor record 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorRecord getBrokerProcessorRecord(org.osid.type.Type brokerProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param brokerProcessorRecord the broker processor record
     *  @param brokerProcessorRecordType broker processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorRecord</code> or
     *          <code>brokerProcessorRecordTypebrokerProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addBrokerProcessorRecord(org.osid.provisioning.rules.records.BrokerProcessorRecord brokerProcessorRecord, 
                                            org.osid.type.Type brokerProcessorRecordType) {

        nullarg(brokerProcessorRecord, "broker processor record");
        addRecordType(brokerProcessorRecordType);
        this.records.add(brokerProcessorRecord);
        
        return;
    }
}
