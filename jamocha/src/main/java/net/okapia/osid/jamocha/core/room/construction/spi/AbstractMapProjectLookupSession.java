//
// AbstractMapProjectLookupSession
//
//    A simple framework for providing a Project lookup service
//    backed by a fixed collection of projects.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Project lookup service backed by a
 *  fixed collection of projects. The projects are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Projects</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProjectLookupSession
    extends net.okapia.osid.jamocha.room.construction.spi.AbstractProjectLookupSession
    implements org.osid.room.construction.ProjectLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.construction.Project> projects = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.construction.Project>());


    /**
     *  Makes a <code>Project</code> available in this session.
     *
     *  @param  project a project
     *  @throws org.osid.NullArgumentException <code>project<code>
     *          is <code>null</code>
     */

    protected void putProject(org.osid.room.construction.Project project) {
        this.projects.put(project.getId(), project);
        return;
    }


    /**
     *  Makes an array of projects available in this session.
     *
     *  @param  projects an array of projects
     *  @throws org.osid.NullArgumentException <code>projects<code>
     *          is <code>null</code>
     */

    protected void putProjects(org.osid.room.construction.Project[] projects) {
        putProjects(java.util.Arrays.asList(projects));
        return;
    }


    /**
     *  Makes a collection of projects available in this session.
     *
     *  @param  projects a collection of projects
     *  @throws org.osid.NullArgumentException <code>projects<code>
     *          is <code>null</code>
     */

    protected void putProjects(java.util.Collection<? extends org.osid.room.construction.Project> projects) {
        for (org.osid.room.construction.Project project : projects) {
            this.projects.put(project.getId(), project);
        }

        return;
    }


    /**
     *  Removes a Project from this session.
     *
     *  @param  projectId the <code>Id</code> of the project
     *  @throws org.osid.NullArgumentException <code>projectId<code> is
     *          <code>null</code>
     */

    protected void removeProject(org.osid.id.Id projectId) {
        this.projects.remove(projectId);
        return;
    }


    /**
     *  Gets the <code>Project</code> specified by its <code>Id</code>.
     *
     *  @param  projectId <code>Id</code> of the <code>Project</code>
     *  @return the project
     *  @throws org.osid.NotFoundException <code>projectId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>projectId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Project getProject(org.osid.id.Id projectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.construction.Project project = this.projects.get(projectId);
        if (project == null) {
            throw new org.osid.NotFoundException("project not found: " + projectId);
        }

        return (project);
    }


    /**
     *  Gets all <code>Projects</code>. In plenary mode, the returned
     *  list contains all known projects or an error
     *  results. Otherwise, the returned list may contain only those
     *  projects that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Projects</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.project.ArrayProjectList(this.projects.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.projects.clear();
        super.close();
        return;
    }
}
