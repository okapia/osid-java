//
// InvariantMapBudgetLookupSession
//
//    Implements a Budget lookup service backed by a fixed collection of
//    budgets.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting;


/**
 *  Implements a Budget lookup service backed by a fixed
 *  collection of budgets. The budgets are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBudgetLookupSession
    extends net.okapia.osid.jamocha.core.financials.budgeting.spi.AbstractMapBudgetLookupSession
    implements org.osid.financials.budgeting.BudgetLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBudgetLookupSession</code> with no
     *  budgets.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapBudgetLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBudgetLookupSession</code> with a single
     *  budget.
     *  
     *  @param business the business
     *  @param budget a single budget
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code budget} is <code>null</code>
     */

      public InvariantMapBudgetLookupSession(org.osid.financials.Business business,
                                               org.osid.financials.budgeting.Budget budget) {
        this(business);
        putBudget(budget);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBudgetLookupSession</code> using an array
     *  of budgets.
     *  
     *  @param business the business
     *  @param budgets an array of budgets
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code budgets} is <code>null</code>
     */

      public InvariantMapBudgetLookupSession(org.osid.financials.Business business,
                                               org.osid.financials.budgeting.Budget[] budgets) {
        this(business);
        putBudgets(budgets);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBudgetLookupSession</code> using a
     *  collection of budgets.
     *
     *  @param business the business
     *  @param budgets a collection of budgets
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code budgets} is <code>null</code>
     */

      public InvariantMapBudgetLookupSession(org.osid.financials.Business business,
                                               java.util.Collection<? extends org.osid.financials.budgeting.Budget> budgets) {
        this(business);
        putBudgets(budgets);
        return;
    }
}
