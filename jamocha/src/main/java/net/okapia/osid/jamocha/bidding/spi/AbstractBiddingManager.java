//
// AbstractBiddingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBiddingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.bidding.BiddingManager,
               org.osid.bidding.BiddingProxyManager {

    private final Types auctionRecordTypes                 = new TypeRefSet();
    private final Types auctionSearchRecordTypes           = new TypeRefSet();

    private final Types bidRecordTypes                     = new TypeRefSet();
    private final Types bidSearchRecordTypes               = new TypeRefSet();

    private final Types auctionHouseRecordTypes            = new TypeRefSet();
    private final Types auctionHouseSearchRecordTypes      = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractBiddingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBiddingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any auction house federation is exposed. Federation is 
     *  exposed when a specific auction house may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of auction houses appears as a single auction house. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a my auction service is supported for the current agent. 
     *
     *  @return <code> true </code> if my auction is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyAuction() {
        return (false);
    }


    /**
     *  Tests if a my auction notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if my auction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyAuctionNotification() {
        return (false);
    }


    /**
     *  Tests if looking up auctions is supported. 
     *
     *  @return <code> true </code> if auction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionLookup() {
        return (false);
    }


    /**
     *  Tests if querying auctions is supported. 
     *
     *  @return <code> true </code> if auction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (false);
    }


    /**
     *  Tests if searching auctions is supported. 
     *
     *  @return <code> true </code> if auction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSearch() {
        return (false);
    }


    /**
     *  Tests if an auction administrative service is supported. 
     *
     *  @return <code> true </code> if auction administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAdmin() {
        return (false);
    }


    /**
     *  Tests if an auction <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if auction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionNotification() {
        return (false);
    }


    /**
     *  Tests if an auction auction house lookup service is supported. 
     *
     *  @return <code> true </code> if an auction auction house lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction auction house assignment service is supported. 
     *
     *  @return <code> true </code> if an auction to auction house assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an auction smart auction house service is supported. 
     *
     *  @return <code> true </code> if an v smart auction house service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if looking up bids is supported. 
     *
     *  @return <code> true </code> if bid lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidLookup() {
        return (false);
    }


    /**
     *  Tests if querying bids is supported. 
     *
     *  @return <code> true </code> if bid query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidQuery() {
        return (false);
    }


    /**
     *  Tests if searching bids is supported. 
     *
     *  @return <code> true </code> if bid search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidSearch() {
        return (false);
    }


    /**
     *  Tests if bid administrative service is supported. 
     *
     *  @return <code> true </code> if bid administration is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAdmin() {
        return (false);
    }


    /**
     *  Tests if a bid notification service is supported. 
     *
     *  @return <code> true </code> if bid notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidNotification() {
        return (false);
    }


    /**
     *  Tests if a bid auction house lookup service is supported. 
     *
     *  @return <code> true </code> if a bid auction house lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if a bid auction house service is supported. 
     *
     *  @return <code> true </code> if bid to auction house assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if a bid smart auction house lookup service is supported. 
     *
     *  @return <code> true </code> if a bid smart auction house service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if looking up auction houses is supported. 
     *
     *  @return <code> true </code> if auction house lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseLookup() {
        return (false);
    }


    /**
     *  Tests if querying auction houses is supported. 
     *
     *  @return <code> true </code> if a auction house query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Tests if searching auction houses is supported. 
     *
     *  @return <code> true </code> if auction house search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseSearch() {
        return (false);
    }


    /**
     *  Tests if auction house administrative service is supported. 
     *
     *  @return <code> true </code> if auction house administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseAdmin() {
        return (false);
    }


    /**
     *  Tests if a auction house <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction house notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a auction house hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if auction house hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a auction house hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if auction house hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a bidding batch service. 
     *
     *  @return <code> true </code> ifa bidding batch servicen is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a bidding rules service. 
     *
     *  @return <code> true </code> ifa bidding rules servicen is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Auction </code> record types. 
     *
     *  @return a list containing the supported <code> Auction </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Auction </code> record type is supported. 
     *
     *  @param  auctionRecordType a <code> Type </code> indicating an <code> 
     *          Auction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionRecordType(org.osid.type.Type auctionRecordType) {
        return (this.auctionRecordTypes.contains(auctionRecordType));
    }


    /**
     *  Adds support for an auction record type.
     *
     *  @param auctionRecordType an auction record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionRecordType</code> is <code>null</code>
     */

    protected void addAuctionRecordType(org.osid.type.Type auctionRecordType) {
        this.auctionRecordTypes.add(auctionRecordType);
        return;
    }


    /**
     *  Removes support for an auction record type.
     *
     *  @param auctionRecordType an auction record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionRecordType</code> is <code>null</code>
     */

    protected void removeAuctionRecordType(org.osid.type.Type auctionRecordType) {
        this.auctionRecordTypes.remove(auctionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Auction </code> search types. 
     *
     *  @return a list containing the supported <code> Auction </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Auction </code> search type is supported. 
     *
     *  @param  auctionSearchRecordType a <code> Type </code> indicating an 
     *          <code> Auction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionSearchRecordType(org.osid.type.Type auctionSearchRecordType) {
        return (this.auctionSearchRecordTypes.contains(auctionSearchRecordType));
    }


    /**
     *  Adds support for an auction search record type.
     *
     *  @param auctionSearchRecordType an auction search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionSearchRecordType(org.osid.type.Type auctionSearchRecordType) {
        this.auctionSearchRecordTypes.add(auctionSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction search record type.
     *
     *  @param auctionSearchRecordType an auction search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionSearchRecordType(org.osid.type.Type auctionSearchRecordType) {
        this.auctionSearchRecordTypes.remove(auctionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Bid </code> record types. 
     *
     *  @return a list containing the supported <code> Bid </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBidRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.bidRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Bid </code> record type is supported. 
     *
     *  @param  bidRecordType a <code> Type </code> indicating a <code> Bid 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bidRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBidRecordType(org.osid.type.Type bidRecordType) {
        return (this.bidRecordTypes.contains(bidRecordType));
    }


    /**
     *  Adds support for a bid record type.
     *
     *  @param bidRecordType a bid record type
     *  @throws org.osid.NullArgumentException
     *  <code>bidRecordType</code> is <code>null</code>
     */

    protected void addBidRecordType(org.osid.type.Type bidRecordType) {
        this.bidRecordTypes.add(bidRecordType);
        return;
    }


    /**
     *  Removes support for a bid record type.
     *
     *  @param bidRecordType a bid record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>bidRecordType</code> is <code>null</code>
     */

    protected void removeBidRecordType(org.osid.type.Type bidRecordType) {
        this.bidRecordTypes.remove(bidRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Bid </code> search record types. 
     *
     *  @return a list containing the supported <code> Bid </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBidSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.bidSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Bid </code> search record type is supported. 
     *
     *  @param  bidSearchRecordType a <code> Type </code> indicating a <code> 
     *          Bid </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bidSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBidSearchRecordType(org.osid.type.Type bidSearchRecordType) {
        return (this.bidSearchRecordTypes.contains(bidSearchRecordType));
    }


    /**
     *  Adds support for a bid search record type.
     *
     *  @param bidSearchRecordType a bid search record type
     *  @throws org.osid.NullArgumentException
     *  <code>bidSearchRecordType</code> is <code>null</code>
     */

    protected void addBidSearchRecordType(org.osid.type.Type bidSearchRecordType) {
        this.bidSearchRecordTypes.add(bidSearchRecordType);
        return;
    }


    /**
     *  Removes support for a bid search record type.
     *
     *  @param bidSearchRecordType a bid search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>bidSearchRecordType</code> is <code>null</code>
     */

    protected void removeBidSearchRecordType(org.osid.type.Type bidSearchRecordType) {
        this.bidSearchRecordTypes.remove(bidSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionHouse </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionHouse </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionHouseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionHouseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionHouse </code> record type is 
     *  supported. 
     *
     *  @param  auctionHouseRecordType a <code> Type </code> indicating a 
     *          <code> AuctionHouse </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auctionHouseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionHouseRecordType(org.osid.type.Type auctionHouseRecordType) {
        return (this.auctionHouseRecordTypes.contains(auctionHouseRecordType));
    }


    /**
     *  Adds support for an auction house record type.
     *
     *  @param auctionHouseRecordType an auction house record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionHouseRecordType</code> is <code>null</code>
     */

    protected void addAuctionHouseRecordType(org.osid.type.Type auctionHouseRecordType) {
        this.auctionHouseRecordTypes.add(auctionHouseRecordType);
        return;
    }


    /**
     *  Removes support for an auction house record type.
     *
     *  @param auctionHouseRecordType an auction house record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionHouseRecordType</code> is <code>null</code>
     */

    protected void removeAuctionHouseRecordType(org.osid.type.Type auctionHouseRecordType) {
        this.auctionHouseRecordTypes.remove(auctionHouseRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionHouse </code> search record types. 
     *
     *  @return a list containing the supported <code> AuctionHouse </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionHouseSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionHouseSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionHouse </code> search record type is 
     *  supported. 
     *
     *  @param  auctionHouseSearchRecordType a <code> Type </code> indicating 
     *          a <code> AuctionHouse </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionHouseSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionHouseSearchRecordType(org.osid.type.Type auctionHouseSearchRecordType) {
        return (this.auctionHouseSearchRecordTypes.contains(auctionHouseSearchRecordType));
    }


    /**
     *  Adds support for an auction house search record type.
     *
     *  @param auctionHouseSearchRecordType an auction house search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionHouseSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionHouseSearchRecordType(org.osid.type.Type auctionHouseSearchRecordType) {
        this.auctionHouseSearchRecordTypes.add(auctionHouseSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction house search record type.
     *
     *  @param auctionHouseSearchRecordType an auction house search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionHouseSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionHouseSearchRecordType(org.osid.type.Type auctionHouseSearchRecordType) {
        this.auctionHouseSearchRecordTypes.remove(auctionHouseSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service. 
     *
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getMyAuctionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getMyAuctionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getMyAuctionSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @param  proxy a proxy 
     *  @return a <code> MyAuctionSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyAuction() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionSession getMyAuctionSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getMyAuctionSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSession(org.osid.bidding.MyAuctionReceiver myAuctionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getMyAuctionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSession(org.osid.bidding.MyAuctionReceiver myAuctionReceiver, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getMyAuctionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service for the given auction house. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver 
     *          </code> or <code> auctionHouseId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSessionForAuctionHouse(org.osid.bidding.MyAuctionReceiver myAuctionReceiver, 
                                                                                                        org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getMyAuctionNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my auction 
     *  notification service for the given auction house. 
     *
     *  @param  myAuctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the auction house 
     *  @param  proxy a proxy 
     *  @return a <code> MyAuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> myAuctionReceiver, 
     *          auctionHouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAuctionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.MyAuctionNotificationSession getMyAuctionNotificationSessionForAuctionHouse(org.osid.bidding.MyAuctionReceiver myAuctionReceiver, 
                                                                                                        org.osid.id.Id auctionHouseId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getMyAuctionNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service. 
     *
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionLookupSession getAuctionLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service. 
     *
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionQuerySession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuerySession getAuctionQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service. 
     *
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionSearchSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchSession getAuctionSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service. 
     *
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionAdminSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAdminSession getAuctionAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSession(org.osid.bidding.AuctionReceiver auctionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSession(org.osid.bidding.AuctionReceiver auctionReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service for the given auction house. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver </code> 
     *          or <code> auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSessionForAuctionHouse(org.osid.bidding.AuctionReceiver auctionReceiver, 
                                                                                                    org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  notification service for the given auction house. 
     *
     *  @param  auctionReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionReceiver, 
     *          auctionHouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionNotificationSession getAuctionNotificationSessionForAuctionHouse(org.osid.bidding.AuctionReceiver auctionReceiver, 
                                                                                                    org.osid.id.Id auctionHouseId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction/auctionHouse 
     *  mappings. 
     *
     *  @return an <code> AuctionAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction/auctionHouse 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auctions 
     *  to auction houses. 
     *
     *  @return an <code> AuctionAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouseAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseAssignmentSession getAuctionAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auctions 
     *  to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionAuctionHouseAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseAssignmentSession getAuctionAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionSmartAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionSmartAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionAuctionHouseSession getAuctionSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service. 
     *
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid lookup 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidLookupSession getBidLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service. 
     *
     *  @return a <code> BidQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> CBidQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid query 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidQuerySession getBidQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service. 
     *
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid search 
     *  service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSearchSession getBidSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service. 
     *
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSessionForAuction(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidAdminSessionForAuction not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  administrative service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBidAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAdminSession getBidAdminSessionForAuction(org.osid.id.Id auctionHouseId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidAdminSessionForAuction not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service. 
     *
     *  @param  bidReceiver the notification callback 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSession(org.osid.bidding.BidReceiver bidReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service. 
     *
     *  @param  bidReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSession(org.osid.bidding.BidReceiver bidReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service for the given auction house. 
     *
     *  @param  bidReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver </code> or 
     *          <code> auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSessionForAuctionHouse(org.osid.bidding.BidReceiver bidReceiver, 
                                                                                            org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bid 
     *  notification service for the given auction house. 
     *
     *  @param  bidReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bidReceiver, 
     *          auctionHouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidNotificationSession getBidNotificationSessionForAuctionHouse(org.osid.bidding.BidReceiver bidReceiver, 
                                                                                            org.osid.id.Id auctionHouseId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup bid/auctionHouse 
     *  mappings. 
     *
     *  @return a <code> BidAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseSession getBidAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup bid/auctionHouse 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseSession getBidAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to auction houses. 
     *
     *  @return a <code> BidyAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseAssignmentSession getBidAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BidyAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidAuctionHouseAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidAuctionHouseAssignmentSession getBidAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage bid smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return a <code> BidSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidSmartAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSmartAuctionHouseSession getBidSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBidSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage bid smart auction 
     *  houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BidSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBidSmartAuctionHouse() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BidSmartAuctionHouseSession getBidSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBidSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  lookup service. 
     *
     *  @return a <code> AuctionHouseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseLookupSession getAuctionHouseLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseLookupSession getAuctionHouseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  query service. 
     *
     *  @return a <code> AuctionHouseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuerySession getAuctionHouseQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuerySession getAuctionHouseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  search service. 
     *
     *  @return a <code> AuctionHouseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseSearchSession getAuctionHouseSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseSearchSession getAuctionHouseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  administrative service. 
     *
     *  @return a <code> AuctionHouseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseAdminSession getAuctionHouseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseAdminSession getAuctionHouseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  notification service. 
     *
     *  @param  auctionHouseReceiver the notification callback 
     *  @return a <code> AuctionHouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNotificationSession getAuctionHouseNotificationSession(org.osid.bidding.AuctionHouseReceiver auctionHouseReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  notification service. 
     *
     *  @param  auctionHouseReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseNotificationSession getAuctionHouseNotificationSession(org.osid.bidding.AuctionHouseReceiver auctionHouseReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy service. 
     *
     *  @return a <code> AuctionHouseHierarchySession </code> for auction 
     *          houses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchySession getAuctionHouseHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> AuctionHouseHierarchySession </code> for auction 
     *          houses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchySession getAuctionHouseHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for auction houses 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchyDesignSession getAuctionHouseHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getAuctionHouseHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction house 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for auction houses 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseHierarchyDesignSession getAuctionHouseHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getAuctionHouseHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> BiddingBatchManager. </code> 
     *
     *  @return a <code> BiddingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BiddingBatchManager getBiddingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBiddingBatchManager not implemented");
    }


    /**
     *  Gets a <code> BiddingBatchProxyManager. </code> 
     *
     *  @return a <code> BiddingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.batch.BiddingBatchProxyManager getBiddingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBiddingBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> BiddingRulesManager. </code> 
     *
     *  @return a <code> BiddingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.BiddingRulesManager getBiddingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingManager.getBiddingRulesManager not implemented");
    }


    /**
     *  Gets a <code> BiddingRulesProxyManager. </code> 
     *
     *  @return a <code> BiddingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBiddingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.BiddingRulesProxyManager getBiddingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.BiddingProxyManager.getBiddingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.auctionRecordTypes.clear();
        this.auctionRecordTypes.clear();

        this.auctionSearchRecordTypes.clear();
        this.auctionSearchRecordTypes.clear();

        this.bidRecordTypes.clear();
        this.bidRecordTypes.clear();

        this.bidSearchRecordTypes.clear();
        this.bidSearchRecordTypes.clear();

        this.auctionHouseRecordTypes.clear();
        this.auctionHouseRecordTypes.clear();

        this.auctionHouseSearchRecordTypes.clear();
        this.auctionHouseSearchRecordTypes.clear();

        return;
    }
}
