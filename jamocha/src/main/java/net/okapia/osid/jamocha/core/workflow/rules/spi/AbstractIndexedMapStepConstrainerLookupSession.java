//
// AbstractIndexedMapStepConstrainerLookupSession.java
//
//    A simple framework for providing a StepConstrainer lookup service
//    backed by a fixed collection of step constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a StepConstrainer lookup service backed by a
 *  fixed collection of step constrainers. The step constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some step constrainers may be compatible
 *  with more types than are indicated through these step constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStepConstrainerLookupSession
    extends AbstractMapStepConstrainerLookupSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepConstrainer> stepConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.workflow.rules.StepConstrainer> stepConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.workflow.rules.StepConstrainer>());


    /**
     *  Makes a <code>StepConstrainer</code> available in this session.
     *
     *  @param  stepConstrainer a step constrainer
     *  @throws org.osid.NullArgumentException <code>stepConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStepConstrainer(org.osid.workflow.rules.StepConstrainer stepConstrainer) {
        super.putStepConstrainer(stepConstrainer);

        this.stepConstrainersByGenus.put(stepConstrainer.getGenusType(), stepConstrainer);
        
        try (org.osid.type.TypeList types = stepConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepConstrainersByRecord.put(types.getNextType(), stepConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a step constrainer from this session.
     *
     *  @param stepConstrainerId the <code>Id</code> of the step constrainer
     *  @throws org.osid.NullArgumentException <code>stepConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStepConstrainer(org.osid.id.Id stepConstrainerId) {
        org.osid.workflow.rules.StepConstrainer stepConstrainer;
        try {
            stepConstrainer = getStepConstrainer(stepConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stepConstrainersByGenus.remove(stepConstrainer.getGenusType());

        try (org.osid.type.TypeList types = stepConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stepConstrainersByRecord.remove(types.getNextType(), stepConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStepConstrainer(stepConstrainerId);
        return;
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the given
     *  step constrainer genus <code>Type</code> which does not include
     *  step constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known step constrainers or an error results. Otherwise,
     *  the returned list may contain only those step constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stepConstrainerGenusType a step constrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainer.ArrayStepConstrainerList(this.stepConstrainersByGenus.get(stepConstrainerGenusType)));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> containing the given
     *  step constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known step constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  step constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stepConstrainerRecordType a step constrainer record type 
     *  @return the returned <code>stepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByRecordType(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainer.ArrayStepConstrainerList(this.stepConstrainersByRecord.get(stepConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepConstrainersByGenus.clear();
        this.stepConstrainersByRecord.clear();

        super.close();

        return;
    }
}
