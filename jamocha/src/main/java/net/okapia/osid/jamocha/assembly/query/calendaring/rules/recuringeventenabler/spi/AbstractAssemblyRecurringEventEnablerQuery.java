//
// AbstractAssemblyRecurringEventEnablerQuery.java
//
//     A RecurringEventEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RecurringEventEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyRecurringEventEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.calendaring.rules.RecurringEventEnablerQuery,
               org.osid.calendaring.rules.RecurringEventEnablerQueryInspector,
               org.osid.calendaring.rules.RecurringEventEnablerSearchOrder {

    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRecurringEventEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRecurringEventEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the recurring event. 
     *
     *  @param  recurringEventId the recurring event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recurringEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRecurringEventId(org.osid.id.Id recurringEventId, 
                                             boolean match) {
        getAssembler().addIdTerm(getRuledRecurringEventIdColumn(), recurringEventId, match);
        return;
    }


    /**
     *  Clears the recurring event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRecurringEventIdTerms() {
        getAssembler().clearTerms(getRuledRecurringEventIdColumn());
        return;
    }


    /**
     *  Gets the recurring event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRecurringEventIdTerms() {
        return (getAssembler().getIdTerms(getRuledRecurringEventIdColumn()));
    }


    /**
     *  Gets the RuledRecurringEventId column name.
     *
     * @return the column name
     */

    protected String getRuledRecurringEventIdColumn() {
        return ("ruled_recurring_event_id");
    }


    /**
     *  Tests if an <code> RecurringEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a recurring event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRecurringEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a supsreding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the recurring event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRecurringEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuery getRuledRecurringEventQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRecurringEventQuery() is false");
    }


    /**
     *  Matches enablers mapped to any supsreding event. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          supsreding event, <code> false </code> to match enablers 
     *          mapped to no supsreding events 
     */

    @OSID @Override
    public void matchAnyRuledRecurringEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledRecurringEventColumn(), match);
        return;
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearRuledRecurringEventTerms() {
        getAssembler().clearTerms(getRuledRecurringEventColumn());
        return;
    }


    /**
     *  Gets the recurring event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQueryInspector[] getRuledRecurringEventTerms() {
        return (new org.osid.calendaring.RecurringEventQueryInspector[0]);
    }


    /**
     *  Gets the RuledRecurringEvent column name.
     *
     * @return the column name
     */

    protected String getRuledRecurringEventColumn() {
        return ("ruled_recurring_event");
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this recurringEventEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  recurringEventEnablerRecordType a recurring event enabler record type 
     *  @return <code>true</code> if the recurringEventEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recurringEventEnablerRecordType) {
        for (org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  recurringEventEnablerRecordType the recurring event enabler record type 
     *  @return the recurring event enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord getRecurringEventEnablerQueryRecord(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  recurringEventEnablerRecordType the recurring event enabler record type 
     *  @return the recurring event enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord getRecurringEventEnablerQueryInspectorRecord(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param recurringEventEnablerRecordType the recurring event enabler record type
     *  @return the recurring event enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerSearchOrderRecord getRecurringEventEnablerSearchOrderRecord(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.RecurringEventEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this recurring event enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param recurringEventEnablerQueryRecord the recurring event enabler query record
     *  @param recurringEventEnablerQueryInspectorRecord the recurring event enabler query inspector
     *         record
     *  @param recurringEventEnablerSearchOrderRecord the recurring event enabler search order record
     *  @param recurringEventEnablerRecordType recurring event enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerQueryRecord</code>,
     *          <code>recurringEventEnablerQueryInspectorRecord</code>,
     *          <code>recurringEventEnablerSearchOrderRecord</code> or
     *          <code>recurringEventEnablerRecordTyperecurringEventEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addRecurringEventEnablerRecords(org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord recurringEventEnablerQueryRecord, 
                                      org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord recurringEventEnablerQueryInspectorRecord, 
                                      org.osid.calendaring.rules.records.RecurringEventEnablerSearchOrderRecord recurringEventEnablerSearchOrderRecord, 
                                      org.osid.type.Type recurringEventEnablerRecordType) {

        addRecordType(recurringEventEnablerRecordType);

        nullarg(recurringEventEnablerQueryRecord, "recurring event enabler query record");
        nullarg(recurringEventEnablerQueryInspectorRecord, "recurring event enabler query inspector record");
        nullarg(recurringEventEnablerSearchOrderRecord, "recurring event enabler search odrer record");

        this.queryRecords.add(recurringEventEnablerQueryRecord);
        this.queryInspectorRecords.add(recurringEventEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(recurringEventEnablerSearchOrderRecord);
        
        return;
    }
}
