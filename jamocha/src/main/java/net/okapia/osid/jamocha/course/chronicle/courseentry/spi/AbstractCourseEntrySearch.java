//
// AbstractCourseEntrySearch.java
//
//     A template for making a CourseEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing course entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCourseEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.chronicle.CourseEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.chronicle.CourseEntrySearchOrder courseEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of course entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  courseEntryIds list of course entries
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCourseEntries(org.osid.id.IdList courseEntryIds) {
        while (courseEntryIds.hasNext()) {
            try {
                this.ids.add(courseEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCourseEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of course entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCourseEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  courseEntrySearchOrder course entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>courseEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCourseEntryResults(org.osid.course.chronicle.CourseEntrySearchOrder courseEntrySearchOrder) {
	this.courseEntrySearchOrder = courseEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.chronicle.CourseEntrySearchOrder getCourseEntrySearchOrder() {
	return (this.courseEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given course entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course entry implementing the requested record.
     *
     *  @param courseEntrySearchRecordType a course entry search record
     *         type
     *  @return the course entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntrySearchRecord getCourseEntrySearchRecord(org.osid.type.Type courseEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.chronicle.records.CourseEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(courseEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course entry search. 
     *
     *  @param courseEntrySearchRecord course entry search record
     *  @param courseEntrySearchRecordType courseEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseEntrySearchRecord(org.osid.course.chronicle.records.CourseEntrySearchRecord courseEntrySearchRecord, 
                                           org.osid.type.Type courseEntrySearchRecordType) {

        addRecordType(courseEntrySearchRecordType);
        this.records.add(courseEntrySearchRecord);        
        return;
    }
}
