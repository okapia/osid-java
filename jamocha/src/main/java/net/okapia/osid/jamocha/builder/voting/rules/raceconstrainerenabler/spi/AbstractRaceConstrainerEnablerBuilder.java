//
// AbstractRaceConstrainerEnabler.java
//
//     Defines a RaceConstrainerEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.rules.raceconstrainerenabler.spi;


/**
 *  Defines a <code>RaceConstrainerEnabler</code> builder.
 */

public abstract class AbstractRaceConstrainerEnablerBuilder<T extends AbstractRaceConstrainerEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerMiter raceConstrainerEnabler;


    /**
     *  Constructs a new <code>AbstractRaceConstrainerEnablerBuilder</code>.
     *
     *  @param raceConstrainerEnabler the race constrainer enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRaceConstrainerEnablerBuilder(net.okapia.osid.jamocha.builder.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerMiter raceConstrainerEnabler) {
        super(raceConstrainerEnabler);
        this.raceConstrainerEnabler = raceConstrainerEnabler;
        return;
    }


    /**
     *  Builds the race constrainer enabler.
     *
     *  @return the new race constrainer enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.rules.RaceConstrainerEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerValidator(getValidations())).validate(this.raceConstrainerEnabler);
        return (new net.okapia.osid.jamocha.builder.voting.rules.raceconstrainerenabler.ImmutableRaceConstrainerEnabler(this.raceConstrainerEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the race constrainer enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerMiter getMiter() {
        return (this.raceConstrainerEnabler);
    }


    /**
     *  Adds a RaceConstrainerEnabler record.
     *
     *  @param record a race constrainer enabler record
     *  @param recordType the type of race constrainer enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.rules.records.RaceConstrainerEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addRaceConstrainerEnablerRecord(record, recordType);
        return (self());
    }
}       


