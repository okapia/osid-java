//
// AbstractProcessSearch.java
//
//     A template for making a Process Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.process.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing process searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProcessSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.process.ProcessSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.process.records.ProcessSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.process.ProcessSearchOrder processSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of processes. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  processIds list of processes
     *  @throws org.osid.NullArgumentException
     *          <code>processIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProcesses(org.osid.id.IdList processIds) {
        while (processIds.hasNext()) {
            try {
                this.ids.add(processIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProcesses</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of process Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProcessIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  processSearchOrder process search order 
     *  @throws org.osid.NullArgumentException
     *          <code>processSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>processSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProcessResults(org.osid.process.ProcessSearchOrder processSearchOrder) {
	this.processSearchOrder = processSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.process.ProcessSearchOrder getProcessSearchOrder() {
	return (this.processSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given process search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a process implementing the requested record.
     *
     *  @param processSearchRecordType a process search record
     *         type
     *  @return the process search record
     *  @throws org.osid.NullArgumentException
     *          <code>processSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.process.records.ProcessSearchRecord getProcessSearchRecord(org.osid.type.Type processSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.process.records.ProcessSearchRecord record : this.records) {
            if (record.implementsRecordType(processSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this process search. 
     *
     *  @param processSearchRecord process search record
     *  @param processSearchRecordType process search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcessSearchRecord(org.osid.process.records.ProcessSearchRecord processSearchRecord, 
                                           org.osid.type.Type processSearchRecordType) {

        addRecordType(processSearchRecordType);
        this.records.add(processSearchRecord);        
        return;
    }
}
