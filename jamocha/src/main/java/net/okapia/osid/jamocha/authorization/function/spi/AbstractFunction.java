//
// AbstractFunction.java
//
//     Defines a Function.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Function</code>.
 */

public abstract class AbstractFunction
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.authorization.Function {

    private org.osid.hierarchy.Hierarchy qualifierHierarchy;

    private final java.util.Collection<org.osid.authorization.records.FunctionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the qualifier hierarchy <code> Id </code> for this function. 
     *
     *  @return the qualifier hierarchy <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQualifierHierarchyId() {
        return (this.qualifierHierarchy.getId());
    }


    /**
     *  Gets the qualifier hierarchy for this function. 
     *
     *  @return the qualifier hierarchy 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getQualifierHierarchy()
        throws org.osid.OperationFailedException {

        return (this.qualifierHierarchy);
    }


    /**
     *  Sets the qualifier hierarchy.
     *
     *  @param hierarchy a qualifier hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          is <code>null</code>
     */

    protected void setQualifierHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        nullarg(hierarchy, "qualifier hierarchy");
        this.qualifierHierarchy = hierarchy;
        return;
    }


    /**
     *  Tests if this function supports the given record
     *  <code>Type</code>.
     *
     *  @param  functionRecordType a function record type 
     *  @return <code>true</code> if the functionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type functionRecordType) {
        for (org.osid.authorization.records.FunctionRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Function</code> record <code>Type</code>.
     *
     *  @param  functionRecordType the function record type 
     *  @return the function record 
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionRecord getFunctionRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this function. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param functionRecord the function record
     *  @param functionRecordType function record type
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecord</code> or
     *          <code>functionRecordType</code> is <code>null</code>
     */
            
    protected void addFunctionRecord(org.osid.authorization.records.FunctionRecord functionRecord, 
                                     org.osid.type.Type functionRecordType) {

        nullarg(functionRecord, "function record");
        addRecordType(functionRecordType);
        this.records.add(functionRecord);
        
        return;
    }
}
