//
// MutableMapRelationshipLookupSession
//
//    Implements a Relationship lookup service backed by a collection of
//    relationships that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship;


/**
 *  Implements a Relationship lookup service backed by a collection of
 *  relationships. The relationships are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of relationships can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRelationshipLookupSession
    extends net.okapia.osid.jamocha.core.relationship.spi.AbstractMapRelationshipLookupSession
    implements org.osid.relationship.RelationshipLookupSession {


    /**
     *  Constructs a new {@code MutableMapRelationshipLookupSession}
     *  with no relationships.
     *
     *  @param family the family
     *  @throws org.osid.NullArgumentException {@code family} is
     *          {@code null}
     */

      public MutableMapRelationshipLookupSession(org.osid.relationship.Family family) {
        setFamily(family);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelationshipLookupSession} with a
     *  single relationship.
     *
     *  @param family the family  
     *  @param relationship a relationship
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationship} is {@code null}
     */

    public MutableMapRelationshipLookupSession(org.osid.relationship.Family family,
                                           org.osid.relationship.Relationship relationship) {
        this(family);
        putRelationship(relationship);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelationshipLookupSession}
     *  using an array of relationships.
     *
     *  @param family the family
     *  @param relationships an array of relationships
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationships} is {@code null}
     */

    public MutableMapRelationshipLookupSession(org.osid.relationship.Family family,
                                           org.osid.relationship.Relationship[] relationships) {
        this(family);
        putRelationships(relationships);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelationshipLookupSession}
     *  using a collection of relationships.
     *
     *  @param family the family
     *  @param relationships a collection of relationships
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code relationships} is {@code null}
     */

    public MutableMapRelationshipLookupSession(org.osid.relationship.Family family,
                                           java.util.Collection<? extends org.osid.relationship.Relationship> relationships) {

        this(family);
        putRelationships(relationships);
        return;
    }

    
    /**
     *  Makes a {@code Relationship} available in this session.
     *
     *  @param relationship a relationship
     *  @throws org.osid.NullArgumentException {@code relationship{@code  is
     *          {@code null}
     */

    @Override
    public void putRelationship(org.osid.relationship.Relationship relationship) {
        super.putRelationship(relationship);
        return;
    }


    /**
     *  Makes an array of relationships available in this session.
     *
     *  @param relationships an array of relationships
     *  @throws org.osid.NullArgumentException {@code relationships{@code 
     *          is {@code null}
     */

    @Override
    public void putRelationships(org.osid.relationship.Relationship[] relationships) {
        super.putRelationships(relationships);
        return;
    }


    /**
     *  Makes collection of relationships available in this session.
     *
     *  @param relationships a collection of relationships
     *  @throws org.osid.NullArgumentException {@code relationships{@code  is
     *          {@code null}
     */

    @Override
    public void putRelationships(java.util.Collection<? extends org.osid.relationship.Relationship> relationships) {
        super.putRelationships(relationships);
        return;
    }


    /**
     *  Removes a Relationship from this session.
     *
     *  @param relationshipId the {@code Id} of the relationship
     *  @throws org.osid.NullArgumentException {@code relationshipId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRelationship(org.osid.id.Id relationshipId) {
        super.removeRelationship(relationshipId);
        return;
    }    
}
