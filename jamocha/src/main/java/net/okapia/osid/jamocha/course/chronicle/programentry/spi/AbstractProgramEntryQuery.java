//
// AbstractProgramEntryQuery.java
//
//     A template for making a ProgramEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for program entries.
 */

public abstract class AbstractProgramEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.chronicle.ProgramEntryQuery {

    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the student <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStudentId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the student <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStudentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StudentQuery </code> is available. 
     *
     *  @return <code> true </code> if a student query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a student option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a student query 
     *  @throws org.osid.UnimplementedException <code> supportsStudentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getStudentQuery() {
        throw new org.osid.UnimplementedException("supportsStudentQuery() is false");
    }


    /**
     *  Clears the student option terms. 
     */

    @OSID @Override
    public void clearStudentTerms() {
        return;
    }


    /**
     *  Sets the program <code> Id </code> for this query to match entries 
     *  that have an entry for the given course. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        return;
    }


    /**
     *  Matches admission dates between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAdmissionDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        return;
    }


    /**
     *  Matches entries that have any admission date. 
     *
     *  @param  match <code> true </code> to match entries with any admission 
     *          date, <code> false </code> to match entries with no admission 
     *          date 
     */

    @OSID @Override
    public void matchAnyAdmissionDate(boolean match) {
        return;
    }


    /**
     *  Clears the admission date terms. 
     */

    @OSID @Override
    public void clearAdmissionDateTerms() {
        return;
    }


    /**
     *  Matches completed programs. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Matches entries that have any term. 
     *
     *  @param  match <code> true </code> to match entries specific to a term, 
     *          <code> false </code> to match entries for the entire 
     *          enrollment 
     */

    @OSID @Override
    public void matchAnyTerm(boolean match) {
        return;
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Matches a credit scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditScaleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getCreditScaleQuery() {
        throw new org.osid.UnimplementedException("supportsCreditScaleQuery() is false");
    }


    /**
     *  Matches entries that have any credit scale. 
     *
     *  @param  match <code> true </code> to match entries with any credit 
     *          scale, <code> false </code> to match entries with no credit 
     *          scale 
     */

    @OSID @Override
    public void matchAnyCreditScale(boolean match) {
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearCreditScaleTerms() {
        return;
    }


    /**
     *  Matches earned credits between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditsEarned(java.math.BigDecimal from, 
                                   java.math.BigDecimal to, boolean match) {
        return;
    }


    /**
     *  Matches entries that have any earned credits. 
     *
     *  @param  match <code> true </code> to match entries with any earned 
     *          credits, <code> false </code> to match entries with no earned 
     *          credits 
     */

    @OSID @Override
    public void matchAnyCreditsEarned(boolean match) {
        return;
    }


    /**
     *  Clears the earned credits terms. 
     */

    @OSID @Override
    public void clearCreditsEarnedTerms() {
        return;
    }


    /**
     *  Matches a GPA scale <code> Id. </code> 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGPAScaleId(org.osid.id.Id gradeSystemId, boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGPAScaleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGPAScaleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> supportsGPAScaleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGPAScaleQuery() {
        throw new org.osid.UnimplementedException("supportsGPAScaleQuery() is false");
    }


    /**
     *  Matches entries that have any GPA scale. 
     *
     *  @param  match <code> true </code> to match entries with any GPA scale, 
     *          <code> false </code> to match entries with no GPA scale 
     */

    @OSID @Override
    public void matchAnyGPAScale(boolean match) {
        return;
    }


    /**
     *  Clears the credit scale terms. 
     */

    @OSID @Override
    public void clearGPAScaleTerms() {
        return;
    }


    /**
     *  Matches GPA between the given range inclusive. 
     *
     *  @param  from starting value 
     *  @param  to ending value 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGPA(java.math.BigDecimal from, java.math.BigDecimal to, 
                         boolean match) {
        return;
    }


    /**
     *  Matches entries that have any GPA. 
     *
     *  @param  match <code> true </code> to match entries with any GPA, 
     *          <code> false </code> to match entries with no GPA 
     */

    @OSID @Override
    public void matchAnyGPA(boolean match) {
        return;
    }


    /**
     *  Clears the GPA terms. 
     */

    @OSID @Override
    public void clearGPATerms() {
        return;
    }


    /**
     *  Sets the enrollment <code> Id </code> for this query. 
     *
     *  @param  enrollmentId an enrollment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> enrollmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEnrollmentId(org.osid.id.Id enrollmentId, boolean match) {
        return;
    }


    /**
     *  Clears the enrollment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnrollmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EnrollmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an enrollment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an enrollment entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an enrollment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuery getEnrollmentQuery() {
        throw new org.osid.UnimplementedException("supportsEnrollmentQuery() is false");
    }


    /**
     *  Matches entries that have any enrollment. 
     *
     *  @param  match <code> true </code> to match enries with any enrollment, 
     *          <code> false </code> to match enries with no enrollments 
     */

    @OSID @Override
    public void matchAnyEnrollment(boolean match) {
        return;
    }


    /**
     *  Clears the enrollment terms. 
     */

    @OSID @Override
    public void clearEnrollmentTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  entries assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given program entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a program entry implementing the requested record.
     *
     *  @param programEntryRecordType a program entry record type
     *  @return the program entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntryQueryRecord getProgramEntryQueryRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program entry query. 
     *
     *  @param programEntryQueryRecord program entry query record
     *  @param programEntryRecordType programEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramEntryQueryRecord(org.osid.course.chronicle.records.ProgramEntryQueryRecord programEntryQueryRecord, 
                                          org.osid.type.Type programEntryRecordType) {

        addRecordType(programEntryRecordType);
        nullarg(programEntryQueryRecord, "program entry query record");
        this.records.add(programEntryQueryRecord);        
        return;
    }
}
