//
// InvariantMapProcedureLookupSession
//
//    Implements a Procedure lookup service backed by a fixed collection of
//    procedures.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Procedure lookup service backed by a fixed
 *  collection of procedures. The procedures are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProcedureLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapProcedureLookupSession
    implements org.osid.recipe.ProcedureLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapProcedureLookupSession</code> with no
     *  procedures.
     *  
     *  @param cookbook the cookbook
     *  @throws org.osid.NullArgumnetException {@code cookbook} is
     *          {@code null}
     */

    public InvariantMapProcedureLookupSession(org.osid.recipe.Cookbook cookbook) {
        setCookbook(cookbook);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcedureLookupSession</code> with a single
     *  procedure.
     *  
     *  @param cookbook the cookbook
     *  @param procedure a single procedure
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code procedure} is <code>null</code>
     */

      public InvariantMapProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                               org.osid.recipe.Procedure procedure) {
        this(cookbook);
        putProcedure(procedure);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcedureLookupSession</code> using an array
     *  of procedures.
     *  
     *  @param cookbook the cookbook
     *  @param procedures an array of procedures
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code procedures} is <code>null</code>
     */

      public InvariantMapProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                               org.osid.recipe.Procedure[] procedures) {
        this(cookbook);
        putProcedures(procedures);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapProcedureLookupSession</code> using a
     *  collection of procedures.
     *
     *  @param cookbook the cookbook
     *  @param procedures a collection of procedures
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code procedures} is <code>null</code>
     */

      public InvariantMapProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                               java.util.Collection<? extends org.osid.recipe.Procedure> procedures) {
        this(cookbook);
        putProcedures(procedures);
        return;
    }
}
