//
// AbstractPackageSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPackageSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.installation.PackageSearchResults {

    private org.osid.installation.PackageList packages;
    private final org.osid.installation.PackageQueryInspector inspector;
    private final java.util.Collection<org.osid.installation.records.PackageSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPackageSearchResults.
     *
     *  @param packages the result set
     *  @param pkgQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>packages</code>
     *          or <code>pkgQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPackageSearchResults(org.osid.installation.PackageList packages,
                                            org.osid.installation.PackageQueryInspector pkgQueryInspector) {
        nullarg(packages, "packages");
        nullarg(pkgQueryInspector, "package query inspectpr");

        this.packages = packages;
        this.inspector = pkgQueryInspector;

        return;
    }


    /**
     *  Gets the package list resulting from a search.
     *
     *  @return a package list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackages() {
        if (this.packages == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.installation.PackageList packages = this.packages;
        this.packages = null;
	return (packages);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.installation.PackageQueryInspector getPackageQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  package search record <code> Type. </code> This method must
     *  be used to retrieve a pkg implementing the requested
     *  record.
     *
     *  @param pkgSearchRecordType a pkg search 
     *         record type 
     *  @return the package search
     *  @throws org.osid.NullArgumentException
     *          <code>pkgSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(pkgSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageSearchResultsRecord getPackageSearchResultsRecord(org.osid.type.Type pkgSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.installation.records.PackageSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(pkgSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(pkgSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record package search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPackageRecord(org.osid.installation.records.PackageSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "package record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
