//
// MutableMapAuctionConstrainerLookupSession
//
//    Implements an AuctionConstrainer lookup service backed by a collection of
//    auctionConstrainers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionConstrainer lookup service backed by a collection of
 *  auction constrainers. The auction constrainers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of auction constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractMapAuctionConstrainerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerLookupSession}
     *  with no auction constrainers.
     *
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse} is
     *          {@code null}
     */

      public MutableMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerLookupSession} with a
     *  single auctionConstrainer.
     *
     *  @param auctionHouse the auction house  
     *  @param auctionConstrainer an auction constrainer
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainer} is {@code null}
     */

    public MutableMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        this(auctionHouse);
        putAuctionConstrainer(auctionConstrainer);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerLookupSession}
     *  using an array of auction constrainers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionConstrainers an array of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainers} is {@code null}
     */

    public MutableMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           org.osid.bidding.rules.AuctionConstrainer[] auctionConstrainers) {
        this(auctionHouse);
        putAuctionConstrainers(auctionConstrainers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerLookupSession}
     *  using a collection of auction constrainers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionConstrainers a collection of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainers} is {@code null}
     */

    public MutableMapAuctionConstrainerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainer> auctionConstrainers) {

        this(auctionHouse);
        putAuctionConstrainers(auctionConstrainers);
        return;
    }

    
    /**
     *  Makes an {@code AuctionConstrainer} available in this session.
     *
     *  @param auctionConstrainer an auction constrainer
     *  @throws org.osid.NullArgumentException {@code auctionConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionConstrainer(org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        super.putAuctionConstrainer(auctionConstrainer);
        return;
    }


    /**
     *  Makes an array of auction constrainers available in this session.
     *
     *  @param auctionConstrainers an array of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctionConstrainers(org.osid.bidding.rules.AuctionConstrainer[] auctionConstrainers) {
        super.putAuctionConstrainers(auctionConstrainers);
        return;
    }


    /**
     *  Makes collection of auction constrainers available in this session.
     *
     *  @param auctionConstrainers a collection of auction constrainers
     *  @throws org.osid.NullArgumentException {@code auctionConstrainers{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionConstrainers(java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainer> auctionConstrainers) {
        super.putAuctionConstrainers(auctionConstrainers);
        return;
    }


    /**
     *  Removes an AuctionConstrainer from this session.
     *
     *  @param auctionConstrainerId the {@code Id} of the auction constrainer
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAuctionConstrainer(org.osid.id.Id auctionConstrainerId) {
        super.removeAuctionConstrainer(auctionConstrainerId);
        return;
    }    
}
