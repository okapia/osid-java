//
// AbstractQueryEdgeEnablerLookupSession.java
//
//    An inline adapter that maps an EdgeEnablerLookupSession to
//    an EdgeEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EdgeEnablerLookupSession to
 *  an EdgeEnablerQuerySession.
 */

public abstract class AbstractQueryEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.topology.rules.spi.AbstractEdgeEnablerLookupSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;   
    private final org.osid.topology.rules.EdgeEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEdgeEnablerLookupSession.
     *
     *  @param querySession the underlying edge enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEdgeEnablerLookupSession(org.osid.topology.rules.EdgeEnablerQuerySession querySession) {
        nullarg(querySession, "edge enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Graph</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Graph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the <code>Graph</code> associated with this 
     *  session.
     *
     *  @return the <code>Graph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform <code>EdgeEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEdgeEnablers() {
        return (this.session.canSearchEdgeEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edge enablers in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only active edge enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveEdgeEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive edge enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusEdgeEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>EdgeEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>EdgeEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>EdgeEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerId <code>Id</code> of the
     *          <code>EdgeEnabler</code>
     *  @return the edge enabler
     *  @throws org.osid.NotFoundException <code>edgeEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>edgeEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler getEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchId(edgeEnablerId, true);
        org.osid.topology.rules.EdgeEnablerList edgeEnablers = this.session.getEdgeEnablersByQuery(query);
        if (edgeEnablers.hasNext()) {
            return (edgeEnablers.getNextEdgeEnabler());
        } 
        
        throw new org.osid.NotFoundException(edgeEnablerId + " not found");
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  edgeEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>EdgeEnablers</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByIds(org.osid.id.IdList edgeEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = edgeEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEdgeEnablersByQuery(query));
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the
     *  given edge enabler genus <code>Type</code> which does not
     *  include edge enablers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchGenusType(edgeEnablerGenusType, true);
        return (this.session.getEdgeEnablersByQuery(query));
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> corresponding to the
     *  given edge enabler genus <code>Type</code> and include any
     *  additional edge enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerGenusType an edgeEnabler genus type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByParentGenusType(org.osid.type.Type edgeEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchParentGenusType(edgeEnablerGenusType, true);
        return (this.session.getEdgeEnablersByQuery(query));
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> containing the given edge
     *  enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  edgeEnablerRecordType an edgeEnabler record type 
     *  @return the returned <code>EdgeEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByRecordType(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchRecordType(edgeEnablerRecordType, true);
        return (this.session.getEdgeEnablersByQuery(query));
    }


    /**
     *  Gets an <code>EdgeEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session.
     *  
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>EdgeEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getEdgeEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>EdgeEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known edge
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those edge enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, edge enablers are returned that are currently
     *  active. In any status mode, active and inactive edge enablers
     *  are returned.
     *
     *  @return a list of <code>EdgeEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.topology.rules.EdgeEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEdgeEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.topology.rules.EdgeEnablerQuery getQuery() {
        org.osid.topology.rules.EdgeEnablerQuery query = this.session.getEdgeEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
