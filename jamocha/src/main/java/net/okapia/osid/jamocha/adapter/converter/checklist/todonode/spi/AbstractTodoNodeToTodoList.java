//
// AbstractTodoNodeToTodoList.java
//
//     Implements an AbstractTodoNodeToTodoList adapter.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 September 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.checklist.todonode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an AbstractTodoList adapter to convert todo
 *  nodes into todos.
 */

public abstract class AbstractTodoNodeToTodoList
    extends net.okapia.osid.jamocha.adapter.checklist.todo.spi.AbstractAdapterTodoList
    implements org.osid.checklist.TodoList {

    private final org.osid.checklist.TodoNodeList list;


    /**
     *  Constructs a new {@code AbstractTodoNodeToTodoList}.
     *
     *  @param list the todo node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is
     *          {@code null}
     */

    protected AbstractTodoNodeToTodoList(org.osid.checklist.TodoNodeList list) {
        super(list);
        this.list = list;
        return;
    }


    /**
     *  Gets the next {@code Todo} in this list. 
     *
     *  @return the next {@code Todo} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Todo} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.checklist.Todo getNextTodo()
        throws org.osid.OperationFailedException {

        return (this.list.getNextTodoNode().getTodo());
    }
}
