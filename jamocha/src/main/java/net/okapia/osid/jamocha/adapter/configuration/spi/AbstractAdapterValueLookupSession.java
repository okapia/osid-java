//
// AbstractAdapterValueLookupSession.java
//
//    A Value lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Value lookup session adapter.
 */

public abstract class AbstractAdapterValueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.configuration.ValueLookupSession {

    private final org.osid.configuration.ValueLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterValueLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterValueLookupSession(org.osid.configuration.ValueLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Configuration/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Configuration Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.session.getConfigurationId());
    }


    /**
     *  Gets the {@code Configuration} associated with this session.
     *
     *  @return the {@code Configuration} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getConfiguration());
    }


    /**
     *  Tests if this user can perform {@code Value} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupValues() {
        return (this.session.canLookupValues());
    }


    /**
     *  A complete view of the {@code Value} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeValueView() {
        this.session.useComparativeValueView();
        return;
    }


    /**
     *  A complete view of the {@code Value} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryValueView() {
        this.session.usePlenaryValueView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include values in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.session.useFederatedConfigurationView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.session.useIsolatedConfigurationView();
        return;
    }
    

    /**
     *  Returns only values that pass the defined parameter
     *  condition. Some parameter conditions do not require explicit
     *  conditional data to be passed and the <code> Values </code>
     *  returned from any method in this session are filtered on an
     *  implicit condition.
     */

    @OSID @Override
    public void useConditionalView() {
        this.session.useUnconditionalView();
        return;
    }


    /**
     *  Values that are filtered based on an implicit condition are
     *  not filtered out from methods in this session. Methods that
     *  take an explicit condition as a parameter are filtered on only
     *  those conditions that are specified.
     */

    @OSID @Override
    public void useUnconditionalView() {
        this.session.useUnconditionalView();
        return;
    }


    /**
     *  Only active values are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveValueView() {
        this.session.useActiveValueView();
        return;
    }


    /**
     *  Active and inactive values are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusValueView() {
        this.session.useAnyStatusValueView();
        return;
    }

    
    /**
     *  Gets a {@code Value} for the given parameter {@code Id.} If
     *  more than one value exists for the given parameter, the most
     *  preferred value is returned. This method can be used as a
     *  convenience when only one value is expected. {@code
     *  getValuesByParameters()} should be used for getting all the
     *  active values.
     *
     *  @param  parameterId the {@code Id} of the {@code Parameter 
     *         } to retrieve 
     *  @return the value 
     *  @throws org.osid.NotFoundException the {@code parameterId} not 
     *          found or no value available 
     *  @throws org.osid.NullArgumentException the {@code parameterId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueByParameter(parameterId));
    }


    /**
     *  Gets all the {@code Values} for the given parameter
     *  {@code Id. }
     *  
     *  In plenary mode, all values are returned in the order
     *  requested or an error results. In comparative mode,
     *  inaccessible values may be omitted or the values reordered.
     *  
     *  In conditional mode, the values are filtered through
     *  evaluation. In unconditional mode, the values are returned
     *  unfiltered.
     *
     *  @param parameterId the {@code Id} of the {@code Parameter} to
     *         retrieve
     *  @return the value list 
     *  @throws org.osid.NotFoundException the {@code parameterId} not 
     *          found 
     *  @throws org.osid.NullArgumentException the {@code parameterId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByParameter(parameterId));
    }


    /**
     *  Gets the {@code Values} for the given parameter {@code Ids}.
     *  
     *  In plenary mode, the values for all parameters are returned in
     *  the order requested or an error results. In comparative mode,
     *  inaccessible values may be omitted or the values reordered.
     *  
     *  In conditional mode, the values are filtered through
     *  evaluation. In unconditional mode, the values are returned
     *  unfiltered.
     *
     *  @param parameterIds the {@code Id} of the {@code
     *          Parameter} to retrieve
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter {@code Id} is 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code parameterIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameters(org.osid.id.IdList parameterIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByParameters(parameterIds));
    }


    /**
     *  Gets a value condition for the given parameter. 
     *
     *  @param parameterId the {@code Id} of a {@code Parameter}
     *  @return a value condition
     *  @throws org.osid.NullArgumentException {@code parameterId} is
     *         {@code null}
     */

    @OSID @Override
    public org.osid.configuration.ValueCondition getValueCondition(org.osid.id.Id parameterId) {
        return (this.session.getValueCondition(parameterId));
    }


    /**
     *  Gets a value in this configuration based on a condition. If
     *  multiple values are available the most preferred one is
     *  returned. The condition specified is applied to any or all
     *  parameters in this configuration as applicable.
     *
     *  @param parameterId the {@code Id} of a {@code Parameter}
     *  @param  valueCondition the condition 
     *  @return the value 
     *  @throws org.osid.NotFoundException parameter {@code Id} is not 
     *          found 
     *  @throws org.osid.NullArgumentException {@code parameterId} or 
     *          {@code valueCondition} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code valueCondition} 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.configuration.Value getValueByParameterOnCondition(org.osid.id.Id parameterId, 
                                                                       org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValueByParameterOnCondition(parameterId, valueCondition));
    }


    /**
     *  Gets all the values for a parameter based on a condition. In
     *  plenary mode, all values are returned or an error results. In
     *  comparative mode, inaccessible values may be omitted.
     *
     *  @param parameterId the {@code Id} of a {@code Parameter}
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException parameter {@code Id} is not 
     *          found 
     *  @throws org.osid.NullArgumentException {@code parameterId} or 
     *          {@code valueCondition} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code valueCondition} is 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParameterOnCondition(org.osid.id.Id parameterId, 
                                                                            org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByParameterOnCondition(parameterId, valueCondition));
    }


    /**
     *  Gets the values for parameters based on a condition. The
     *  specified condition is applied to any or all of the parameters
     *  as applicable. In plenary mode, all values are returned or an
     *  error results. In comparative mode, inaccessible values may be
     *  omitted.
     *
     *  @param parameterIds the {@code Id} of a {@code Parameter}
     *  @param  valueCondition the condition 
     *  @return the value list 
     *  @throws org.osid.NotFoundException a parameter {@code Id} is 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code parameterIds} or 
     *          {@code valueCondition} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code valueCondition} 
     *          not of this service 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParametersOnCondition(org.osid.id.IdList parameterIds, 
                                                                             org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getValuesByParametersOnCondition(parameterIds, valueCondition));
    }
    
     
    /**
     *  Gets the {@code Value} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Value} may
     *  have a different {@code Id} than requested, such as the case
     *  where a duplicate {@code Id} was assigned to a {@code Value}
     *  and retained for compatibility.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values are
     *  returned.
     *
     *  @param valueId {@code Id} of the {@code Value}
     *  @return the value
     *  @throws org.osid.NotFoundException {@code valueId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code valueId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Value getValue(org.osid.id.Id valueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValue(valueId));
    }


    /**
     *  Gets a {@code ValueList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  values specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Values} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Value} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code valueIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByIds(org.osid.id.IdList valueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByIds(valueIds));
    }


    /**
     *  Gets a {@code ValueList} corresponding to the given
     *  value genus {@code Type} which does not include
     *  values of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned {@code Value} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByGenusType(valueGenusType));
    }


    /**
     *  Gets a {@code ValueList} corresponding to the given
     *  value genus {@code Type} and include any additional
     *  values with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueGenusType a value genus type 
     *  @return the returned {@code Value} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByParentGenusType(org.osid.type.Type valueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByParentGenusType(valueGenusType));
    }


    /**
     *  Gets a {@code ValueList} containing the given
     *  value record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @param  valueRecordType a value record type 
     *  @return the returned {@code Value} list
     *  @throws org.osid.NullArgumentException
     *          {@code valueRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesByRecordType(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesByRecordType(valueRecordType));
    }

    
    /**
     *  Gets the values in this configuration based on a
     *  condition. The condition specified is applied to any or all
     *  parameters in this configuration as applicable. In plenary
     *  mode, all values are returned or an error results. In
     *  comparative mode, inaccessible values may be omitted.
     *
     *  @param  valueCondition a value condition 
     *  @return the value list 
     *  @throws org.osid.NullArgumentException {@code valueCondition} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code valueCondition} 
     *          not of this service 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValuesOnCondition(org.osid.configuration.ValueCondition valueCondition)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValuesOnCondition(valueCondition));
    }


    /**
     *  Gets all {@code Values}. 
     *
     *  In plenary mode, the returned list contains all known
     *  values or an error results. Otherwise, the returned list
     *  may contain only those values that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, values are returned that are currently
     *  active. In any status mode, active and inactive values
     *  are returned.
     *
     *  @return a list of {@code Values} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ValueList getValues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getValues());
    }
}
