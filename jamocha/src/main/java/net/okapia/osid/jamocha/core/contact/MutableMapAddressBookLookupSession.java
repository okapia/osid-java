//
// MutableMapAddressBookLookupSession
//
//    Implements an AddressBook lookup service backed by a collection of
//    addressBooks that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact;


/**
 *  Implements an AddressBook lookup service backed by a collection of
 *  address books. The address books are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of address books can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAddressBookLookupSession
    extends net.okapia.osid.jamocha.core.contact.spi.AbstractMapAddressBookLookupSession
    implements org.osid.contact.AddressBookLookupSession {


    /**
     *  Constructs a new {@code MutableMapAddressBookLookupSession}
     *  with no address books.
     */

    public MutableMapAddressBookLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAddressBookLookupSession} with a
     *  single addressBook.
     *  
     *  @param addressBook an address book
     *  @throws org.osid.NullArgumentException {@code addressBook}
     *          is {@code null}
     */

    public MutableMapAddressBookLookupSession(org.osid.contact.AddressBook addressBook) {
        putAddressBook(addressBook);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAddressBookLookupSession}
     *  using an array of address books.
     *
     *  @param addressBooks an array of address books
     *  @throws org.osid.NullArgumentException {@code addressBooks}
     *          is {@code null}
     */

    public MutableMapAddressBookLookupSession(org.osid.contact.AddressBook[] addressBooks) {
        putAddressBooks(addressBooks);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAddressBookLookupSession}
     *  using a collection of address books.
     *
     *  @param addressBooks a collection of address books
     *  @throws org.osid.NullArgumentException {@code addressBooks}
     *          is {@code null}
     */

    public MutableMapAddressBookLookupSession(java.util.Collection<? extends org.osid.contact.AddressBook> addressBooks) {
        putAddressBooks(addressBooks);
        return;
    }

    
    /**
     *  Makes an {@code AddressBook} available in this session.
     *
     *  @param addressBook an address book
     *  @throws org.osid.NullArgumentException {@code addressBook{@code  is
     *          {@code null}
     */

    @Override
    public void putAddressBook(org.osid.contact.AddressBook addressBook) {
        super.putAddressBook(addressBook);
        return;
    }


    /**
     *  Makes an array of address books available in this session.
     *
     *  @param addressBooks an array of address books
     *  @throws org.osid.NullArgumentException {@code addressBooks{@code 
     *          is {@code null}
     */

    @Override
    public void putAddressBooks(org.osid.contact.AddressBook[] addressBooks) {
        super.putAddressBooks(addressBooks);
        return;
    }


    /**
     *  Makes collection of address books available in this session.
     *
     *  @param addressBooks a collection of address books
     *  @throws org.osid.NullArgumentException {@code addressBooks{@code  is
     *          {@code null}
     */

    @Override
    public void putAddressBooks(java.util.Collection<? extends org.osid.contact.AddressBook> addressBooks) {
        super.putAddressBooks(addressBooks);
        return;
    }


    /**
     *  Removes an AddressBook from this session.
     *
     *  @param addressBookId the {@code Id} of the address book
     *  @throws org.osid.NullArgumentException {@code addressBookId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAddressBook(org.osid.id.Id addressBookId) {
        super.removeAddressBook(addressBookId);
        return;
    }    
}
