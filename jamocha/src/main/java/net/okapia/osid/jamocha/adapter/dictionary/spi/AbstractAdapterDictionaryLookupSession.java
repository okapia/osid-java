//
// AbstractAdapterDictionaryLookupSession.java
//
//    A Dictionary lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.dictionary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Dictionary lookup session adapter.
 */

public abstract class AbstractAdapterDictionaryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.dictionary.DictionaryLookupSession {

    private final org.osid.dictionary.DictionaryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDictionaryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDictionaryLookupSession(org.osid.dictionary.DictionaryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Dictionary} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDictionaries() {
        return (this.session.canLookupDictionaries());
    }


    /**
     *  A complete view of the {@code Dictionary} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDictionaryView() {
        this.session.useComparativeDictionaryView();
        return;
    }


    /**
     *  A complete view of the {@code Dictionary} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDictionaryView() {
        this.session.usePlenaryDictionaryView();
        return;
    }

     
    /**
     *  Gets the {@code Dictionary} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Dictionary} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Dictionary} and
     *  retained for compatibility.
     *
     *  @param dictionaryId {@code Id} of the {@code Dictionary}
     *  @return the dictionary
     *  @throws org.osid.NotFoundException {@code dictionaryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code dictionaryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionary(dictionaryId));
    }


    /**
     *  Gets a {@code DictionaryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  dictionaries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Dictionaries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  dictionaryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Dictionary} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code dictionaryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByIds(org.osid.id.IdList dictionaryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionariesByIds(dictionaryIds));
    }


    /**
     *  Gets a {@code DictionaryList} corresponding to the given
     *  dictionary genus {@code Type} which does not include
     *  dictionaries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned {@code Dictionary} list
     *  @throws org.osid.NullArgumentException
     *          {@code dictionaryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionariesByGenusType(dictionaryGenusType));
    }


    /**
     *  Gets a {@code DictionaryList} corresponding to the given
     *  dictionary genus {@code Type} and include any additional
     *  dictionaries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned {@code Dictionary} list
     *  @throws org.osid.NullArgumentException
     *          {@code dictionaryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByParentGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionariesByParentGenusType(dictionaryGenusType));
    }


    /**
     *  Gets a {@code DictionaryList} containing the given
     *  dictionary record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryRecordType a dictionary record type 
     *  @return the returned {@code Dictionary} list
     *  @throws org.osid.NullArgumentException
     *          {@code dictionaryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByRecordType(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionariesByRecordType(dictionaryRecordType));
    }


    /**
     *  Gets a {@code DictionaryList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Dictionary} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionariesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Dictionaries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Dictionaries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionaries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDictionaries());
    }
}
