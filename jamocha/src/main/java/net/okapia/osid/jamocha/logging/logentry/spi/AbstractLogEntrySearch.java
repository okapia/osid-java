//
// AbstractLogEntrySearch.java
//
//     A template for making a LogEntry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing log entry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractLogEntrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.logging.LogEntrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.logging.records.LogEntrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.logging.LogEntrySearchOrder logEntrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of log entries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  logEntryIds list of log entries
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongLogEntries(org.osid.id.IdList logEntryIds) {
        while (logEntryIds.hasNext()) {
            try {
                this.ids.add(logEntryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongLogEntries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of log entry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getLogEntryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  logEntrySearchOrder log entry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>logEntrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderLogEntryResults(org.osid.logging.LogEntrySearchOrder logEntrySearchOrder) {
	this.logEntrySearchOrder = logEntrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.logging.LogEntrySearchOrder getLogEntrySearchOrder() {
	return (this.logEntrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given log entry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a log entry implementing the requested record.
     *
     *  @param logEntrySearchRecordType a log entry search record
     *         type
     *  @return the log entry search record
     *  @throws org.osid.NullArgumentException
     *          <code>logEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.logging.records.LogEntrySearchRecord getLogEntrySearchRecord(org.osid.type.Type logEntrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.logging.records.LogEntrySearchRecord record : this.records) {
            if (record.implementsRecordType(logEntrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry search. 
     *
     *  @param logEntrySearchRecord log entry search record
     *  @param logEntrySearchRecordType logEntry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLogEntrySearchRecord(org.osid.logging.records.LogEntrySearchRecord logEntrySearchRecord, 
                                           org.osid.type.Type logEntrySearchRecordType) {

        addRecordType(logEntrySearchRecordType);
        this.records.add(logEntrySearchRecord);        
        return;
    }
}
