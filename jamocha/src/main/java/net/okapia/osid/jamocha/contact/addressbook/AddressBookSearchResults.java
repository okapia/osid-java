//
// AddressBookSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook;


/**
 *  A template for implementing a search results.
 */

public final class AddressBookSearchResults
    extends net.okapia.osid.jamocha.contact.addressbook.spi.AbstractAddressBookSearchResults
    implements org.osid.contact.AddressBookSearchResults {


    /**
     *  Constructs a new <code>AddressBookSearchResults.
     *
     *  @param addressBooks the result set
     *  @param addressBookQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>addressBooks</code>
     *          or <code>addressBookQueryInspector</code> is
     *          <code>null</code>
     */

    public AddressBookSearchResults(org.osid.contact.AddressBookList addressBooks,
                                 org.osid.contact.AddressBookQueryInspector addressBookQueryInspector) {
        super(addressBooks, addressBookQueryInspector);
        return;
    }
}
