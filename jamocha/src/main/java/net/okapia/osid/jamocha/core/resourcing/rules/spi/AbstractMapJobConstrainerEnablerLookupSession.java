//
// AbstractMapJobConstrainerEnablerLookupSession
//
//    A simple framework for providing a JobConstrainerEnabler lookup service
//    backed by a fixed collection of job constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a JobConstrainerEnabler lookup service backed by a
 *  fixed collection of job constrainer enablers. The job constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJobConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractJobConstrainerEnablerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.JobConstrainerEnabler>());


    /**
     *  Makes a <code>JobConstrainerEnabler</code> available in this session.
     *
     *  @param  jobConstrainerEnabler a job constrainer enabler
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler) {
        this.jobConstrainerEnablers.put(jobConstrainerEnabler.getId(), jobConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of job constrainer enablers available in this session.
     *
     *  @param  jobConstrainerEnablers an array of job constrainer enablers
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putJobConstrainerEnablers(org.osid.resourcing.rules.JobConstrainerEnabler[] jobConstrainerEnablers) {
        putJobConstrainerEnablers(java.util.Arrays.asList(jobConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of job constrainer enablers available in this session.
     *
     *  @param  jobConstrainerEnablers a collection of job constrainer enablers
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putJobConstrainerEnablers(java.util.Collection<? extends org.osid.resourcing.rules.JobConstrainerEnabler> jobConstrainerEnablers) {
        for (org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler : jobConstrainerEnablers) {
            this.jobConstrainerEnablers.put(jobConstrainerEnabler.getId(), jobConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes a JobConstrainerEnabler from this session.
     *
     *  @param  jobConstrainerEnablerId the <code>Id</code> of the job constrainer enabler
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId) {
        this.jobConstrainerEnablers.remove(jobConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>JobConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  jobConstrainerEnablerId <code>Id</code> of the <code>JobConstrainerEnabler</code>
     *  @return the jobConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>jobConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnabler getJobConstrainerEnabler(org.osid.id.Id jobConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler = this.jobConstrainerEnablers.get(jobConstrainerEnablerId);
        if (jobConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("jobConstrainerEnabler not found: " + jobConstrainerEnablerId);
        }

        return (jobConstrainerEnabler);
    }


    /**
     *  Gets all <code>JobConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known jobConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  jobConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>JobConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerEnablerList getJobConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.ArrayJobConstrainerEnablerList(this.jobConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobConstrainerEnablers.clear();
        super.close();
        return;
    }
}
