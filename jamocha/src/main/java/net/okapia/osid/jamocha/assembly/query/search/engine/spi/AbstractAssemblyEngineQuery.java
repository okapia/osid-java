//
// AbstractAssemblyEngineQuery.java
//
//     An EngineQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.search.engine.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EngineQuery that stores terms.
 */

public abstract class AbstractAssemblyEngineQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.search.EngineQuery,
               org.osid.search.EngineQueryInspector,
               org.osid.search.EngineSearchOrder {

    private final java.util.Collection<org.osid.search.records.EngineQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.records.EngineQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.records.EngineSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEngineQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEngineQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the engine <code> Id </code> for this query to match engines that 
     *  have the specified engine as an ancestor. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getAncestorEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears all ancestor engine <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorEngineIdTerms() {
        getAssembler().clearTerms(getAncestorEngineIdColumn());
        return;
    }


    /**
     *  Gets the ancestor engine <code> Id </code> query terms. 
     *
     *  @return the ancestor engine <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorEngineIdTerms() {
        return (getAssembler().getIdTerms(getAncestorEngineIdColumn()));
    }


    /**
     *  Gets the AncestorEngineId column name.
     *
     * @return the column name
     */

    protected String getAncestorEngineIdColumn() {
        return ("ancestor_engine_id");
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorEngineQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuery getAncestorEngineQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorEngineQuery() is false");
    }


    /**
     *  Matches engines with any ancestor. 
     *
     *  @param  match <code> true </code> to match engine with any ancestor, 
     *          <code> false </code> to match root engines 
     */

    @OSID @Override
    public void matchAnyAncestorEngine(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorEngineColumn(), match);
        return;
    }


    /**
     *  Clears all ancestor engine terms. 
     */

    @OSID @Override
    public void clearAncestorEngineTerms() {
        getAssembler().clearTerms(getAncestorEngineColumn());
        return;
    }


    /**
     *  Gets the ancestor engine query terms. 
     *
     *  @return the ancestor engine terms 
     */

    @OSID @Override
    public org.osid.search.EngineQueryInspector[] getAncestorEngineTerms() {
        return (new org.osid.search.EngineQueryInspector[0]);
    }


    /**
     *  Gets the AncestorEngine column name.
     *
     * @return the column name
     */

    protected String getAncestorEngineColumn() {
        return ("ancestor_engine");
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match engines that 
     *  have the specified engine as a descendant. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getDescendantEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears all descendant engine <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantEngineIdTerms() {
        getAssembler().clearTerms(getDescendantEngineIdColumn());
        return;
    }


    /**
     *  Gets the descendant engine <code> Id </code> query terms. 
     *
     *  @return the descendant engine <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantEngineIdTerms() {
        return (getAssembler().getIdTerms(getDescendantEngineIdColumn()));
    }


    /**
     *  Gets the DescendantEngineId column name.
     *
     * @return the column name
     */

    protected String getDescendantEngineIdColumn() {
        return ("descendant_engine_id");
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantEngineQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuery getDescendantEngineQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantEngineQuery() is false");
    }


    /**
     *  Matches engines with any descendant. 
     *
     *  @param  match <code> true </code> to match engine with any descendant, 
     *          <code> false </code> to match leaf engines 
     */

    @OSID @Override
    public void matchAnyDescendantEngine(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantEngineColumn(), match);
        return;
    }


    /**
     *  Clears all descendant engine terms. 
     */

    @OSID @Override
    public void clearDescendantEngineTerms() {
        getAssembler().clearTerms(getDescendantEngineColumn());
        return;
    }


    /**
     *  Gets the descendant engine query terms. 
     *
     *  @return the descendant engine terms 
     */

    @OSID @Override
    public org.osid.search.EngineQueryInspector[] getDescendantEngineTerms() {
        return (new org.osid.search.EngineQueryInspector[0]);
    }


    /**
     *  Gets the DescendantEngine column name.
     *
     * @return the column name
     */

    protected String getDescendantEngineColumn() {
        return ("descendant_engine");
    }


    /**
     *  Tests if this engine supports the given record
     *  <code>Type</code>.
     *
     *  @param  engineRecordType an engine record type 
     *  @return <code>true</code> if the engineRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type engineRecordType) {
        for (org.osid.search.records.EngineQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(engineRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  engineRecordType the engine record type 
     *  @return the engine query record 
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineQueryRecord getEngineQueryRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.search.records.EngineQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(engineRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  engineRecordType the engine record type 
     *  @return the engine query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineQueryInspectorRecord getEngineQueryInspectorRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.search.records.EngineQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(engineRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param engineRecordType the engine record type
     *  @return the engine search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(engineRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.search.records.EngineSearchOrderRecord getEngineSearchOrderRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.search.records.EngineSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(engineRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(engineRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this engine. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param engineQueryRecord the engine query record
     *  @param engineQueryInspectorRecord the engine query inspector
     *         record
     *  @param engineSearchOrderRecord the engine search order record
     *  @param engineRecordType engine record type
     *  @throws org.osid.NullArgumentException
     *          <code>engineQueryRecord</code>,
     *          <code>engineQueryInspectorRecord</code>,
     *          <code>engineSearchOrderRecord</code> or
     *          <code>engineRecordTypeengine</code> is
     *          <code>null</code>
     */
            
    protected void addEngineRecords(org.osid.search.records.EngineQueryRecord engineQueryRecord, 
                                      org.osid.search.records.EngineQueryInspectorRecord engineQueryInspectorRecord, 
                                      org.osid.search.records.EngineSearchOrderRecord engineSearchOrderRecord, 
                                      org.osid.type.Type engineRecordType) {

        addRecordType(engineRecordType);

        nullarg(engineQueryRecord, "engine query record");
        nullarg(engineQueryInspectorRecord, "engine query inspector record");
        nullarg(engineSearchOrderRecord, "engine search odrer record");

        this.queryRecords.add(engineQueryRecord);
        this.queryInspectorRecords.add(engineQueryInspectorRecord);
        this.searchOrderRecords.add(engineSearchOrderRecord);
        
        return;
    }
}
