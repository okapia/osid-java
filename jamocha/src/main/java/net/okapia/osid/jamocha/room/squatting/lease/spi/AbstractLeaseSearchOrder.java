//
// AbstractLeaseSearchOdrer.java
//
//     Defines a LeaseSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.lease.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code LeaseSearchOrder}.
 */

public abstract class AbstractLeaseSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.room.squatting.LeaseSearchOrder {

    private final java.util.Collection<org.osid.room.squatting.records.LeaseSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the room. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRoom(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a room search order is available. 
     *
     *  @return <code> true </code> if a room search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSearchOrder() {
        return (false);
    }


    /**
     *  Gets the room search order. 
     *
     *  @return the room search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchOrder getRoomSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRoomSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the owner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTenant(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a tenant resource search order is available. 
     *
     *  @return <code> true </code> if an tenant search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTenantSearchOrder() {
        return (false);
    }


    /**
     *  Gets the tenant resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTenantSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getTenantSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTenantSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return {@code true} if the leaseRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code leaseRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type leaseRecordType) {
        for (org.osid.room.squatting.records.LeaseSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  leaseRecordType the lease record type 
     *  @return the lease search order record
     *  @throws org.osid.NullArgumentException
     *          {@code leaseRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(leaseRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.room.squatting.records.LeaseSearchOrderRecord getLeaseSearchOrderRecord(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.LeaseSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(leaseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(leaseRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this lease. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param leaseRecord the lease search odrer record
     *  @param leaseRecordType lease record type
     *  @throws org.osid.NullArgumentException
     *          {@code leaseRecord} or
     *          {@code leaseRecordTypelease} is
     *          {@code null}
     */
            
    protected void addLeaseRecord(org.osid.room.squatting.records.LeaseSearchOrderRecord leaseSearchOrderRecord, 
                                     org.osid.type.Type leaseRecordType) {

        addRecordType(leaseRecordType);
        this.records.add(leaseSearchOrderRecord);
        
        return;
    }
}
