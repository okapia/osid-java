//
// AbstractHoldEnabler.java
//
//     Defines a HoldEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.rules.holdenabler.spi;


/**
 *  Defines a <code>HoldEnabler</code> builder.
 */

public abstract class AbstractHoldEnablerBuilder<T extends AbstractHoldEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.hold.rules.holdenabler.HoldEnablerMiter holdEnabler;


    /**
     *  Constructs a new <code>AbstractHoldEnablerBuilder</code>.
     *
     *  @param holdEnabler the hold enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractHoldEnablerBuilder(net.okapia.osid.jamocha.builder.hold.rules.holdenabler.HoldEnablerMiter holdEnabler) {
        super(holdEnabler);
        this.holdEnabler = holdEnabler;
        return;
    }


    /**
     *  Builds the hold enabler.
     *
     *  @return the new hold enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.hold.rules.HoldEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.hold.rules.holdenabler.HoldEnablerValidator(getValidations())).validate(this.holdEnabler);
        return (new net.okapia.osid.jamocha.builder.hold.rules.holdenabler.ImmutableHoldEnabler(this.holdEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the hold enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.hold.rules.holdenabler.HoldEnablerMiter getMiter() {
        return (this.holdEnabler);
    }


    /**
     *  Adds a HoldEnabler record.
     *
     *  @param record a hold enabler record
     *  @param recordType the type of hold enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.hold.rules.records.HoldEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addHoldEnablerRecord(record, recordType);
        return (self());
    }
}       


