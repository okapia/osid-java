//
// AbstractAssemblyJobConstrainerQuery.java
//
//     A JobConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.jobconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JobConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyJobConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.resourcing.rules.JobConstrainerQuery,
               org.osid.resourcing.rules.JobConstrainerQueryInspector,
               org.osid.resourcing.rules.JobConstrainerSearchOrder {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJobConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJobConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a job. 
     *
     *  @param  foundryId the job <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getRuledJobIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the job <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobIdTerms() {
        getAssembler().clearTerms(getRuledJobIdColumn());
        return;
    }


    /**
     *  Gets the job <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledJobIdTerms() {
        return (getAssembler().getIdTerms(getRuledJobIdColumn()));
    }


    /**
     *  Gets the RuledJobId column name.
     *
     * @return the column name
     */

    protected String getRuledJobIdColumn() {
        return ("ruled_job_id");
    }


    /**
     *  Tests if an <code> JobQuery </code> is available. 
     *
     *  @return <code> true </code> if a job query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the job query 
     *  @throws org.osid.UnimplementedException <code> supportsRuledJobQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.JobQuery getRuledJobQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobQuery() is false");
    }


    /**
     *  Matches mapped to any job. 
     *
     *  @param  match <code> true </code> for mapped to any job, <code> false 
     *          </code> to match mapped to no jobs 
     */

    @OSID @Override
    public void matchAnyRuledJob(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledJobColumn(), match);
        return;
    }


    /**
     *  Clears the job query terms. 
     */

    @OSID @Override
    public void clearRuledJobTerms() {
        getAssembler().clearTerms(getRuledJobColumn());
        return;
    }


    /**
     *  Gets the job query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.JobQueryInspector[] getRuledJobTerms() {
        return (new org.osid.resourcing.JobQueryInspector[0]);
    }


    /**
     *  Gets the RuledJob column name.
     *
     * @return the column name
     */

    protected String getRuledJobColumn() {
        return ("ruled_job");
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this jobConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  jobConstrainerRecordType a job constrainer record type 
     *  @return <code>true</code> if the jobConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type jobConstrainerRecordType) {
        for (org.osid.resourcing.rules.records.JobConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  jobConstrainerRecordType the job constrainer record type 
     *  @return the job constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerQueryRecord getJobConstrainerQueryRecord(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  jobConstrainerRecordType the job constrainer record type 
     *  @return the job constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerQueryInspectorRecord getJobConstrainerQueryInspectorRecord(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(jobConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param jobConstrainerRecordType the job constrainer record type
     *  @return the job constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerSearchOrderRecord getJobConstrainerSearchOrderRecord(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(jobConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this job constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param jobConstrainerQueryRecord the job constrainer query record
     *  @param jobConstrainerQueryInspectorRecord the job constrainer query inspector
     *         record
     *  @param jobConstrainerSearchOrderRecord the job constrainer search order record
     *  @param jobConstrainerRecordType job constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerQueryRecord</code>,
     *          <code>jobConstrainerQueryInspectorRecord</code>,
     *          <code>jobConstrainerSearchOrderRecord</code> or
     *          <code>jobConstrainerRecordTypejobConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addJobConstrainerRecords(org.osid.resourcing.rules.records.JobConstrainerQueryRecord jobConstrainerQueryRecord, 
                                      org.osid.resourcing.rules.records.JobConstrainerQueryInspectorRecord jobConstrainerQueryInspectorRecord, 
                                      org.osid.resourcing.rules.records.JobConstrainerSearchOrderRecord jobConstrainerSearchOrderRecord, 
                                      org.osid.type.Type jobConstrainerRecordType) {

        addRecordType(jobConstrainerRecordType);

        nullarg(jobConstrainerQueryRecord, "job constrainer query record");
        nullarg(jobConstrainerQueryInspectorRecord, "job constrainer query inspector record");
        nullarg(jobConstrainerSearchOrderRecord, "job constrainer search odrer record");

        this.queryRecords.add(jobConstrainerQueryRecord);
        this.queryInspectorRecords.add(jobConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(jobConstrainerSearchOrderRecord);
        
        return;
    }
}
