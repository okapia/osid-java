//
// AbstractSystemSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.system.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSystemSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.SystemSearchResults {

    private org.osid.control.SystemList systems;
    private final org.osid.control.SystemQueryInspector inspector;
    private final java.util.Collection<org.osid.control.records.SystemSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSystemSearchResults.
     *
     *  @param systems the result set
     *  @param systemQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>systems</code>
     *          or <code>systemQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSystemSearchResults(org.osid.control.SystemList systems,
                                            org.osid.control.SystemQueryInspector systemQueryInspector) {
        nullarg(systems, "systems");
        nullarg(systemQueryInspector, "system query inspectpr");

        this.systems = systems;
        this.inspector = systemQueryInspector;

        return;
    }


    /**
     *  Gets the system list resulting from a search.
     *
     *  @return a system list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.SystemList getSystems() {
        if (this.systems == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.SystemList systems = this.systems;
        this.systems = null;
	return (systems);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.SystemQueryInspector getSystemQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  system search record <code> Type. </code> This method must
     *  be used to retrieve a system implementing the requested
     *  record.
     *
     *  @param systemSearchRecordType a system search 
     *         record type 
     *  @return the system search
     *  @throws org.osid.NullArgumentException
     *          <code>systemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(systemSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemSearchResultsRecord getSystemSearchResultsRecord(org.osid.type.Type systemSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.records.SystemSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(systemSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(systemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record system search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSystemRecord(org.osid.control.records.SystemSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "system record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
