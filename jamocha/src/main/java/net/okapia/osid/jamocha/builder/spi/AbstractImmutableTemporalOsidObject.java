//
// AbstractImmutableTemporalOsidObject
//
//     Defines an immutable wrapper for a Temporal OsidObject.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for a Temporal OsidObject.
 */

public abstract class AbstractImmutableTemporalOsidObject
    extends AbstractImmutableOsidObject
    implements org.osid.Temporal,
               org.osid.OsidObject {

    private final org.osid.Temporal temporal;


    /**
     *  Constructs a new
     *  <code>AbstractImmutableTemporalOsidObject</code>.
     *
     *  @param object
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     *  @throws org.osid.UnsupportedException <code>object</code> is not
     *          a <code>Temporal</code>
     */

    protected AbstractImmutableTemporalOsidObject(org.osid.OsidObject object) {
        super(object);

        if (!(object instanceof org.osid.Temporal)) {
            throw new org.osid.UnsupportedException("object not a Temporal");
        }

        this.temporal = new ImmutableTemporal((org.osid.Temporal) object);
        return;
    }


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }


    protected class ImmutableTemporal
        extends AbstractImmutableTemporal
        implements org.osid.Temporal {


        /**
         *  Constructs a new <code>ImmutableTemporal</code>.
         *
         *  @param object
         *  @throws org.osid.NullArgumentException <code>object</code>
         *          is <code>null</code>
         */
        
        protected ImmutableTemporal(org.osid.Temporal object) {
            super(object);
            return;
        }
    }
}
