//
// AbstractQueryEdgeEnablerLookupSession.java
//
//    An EdgeEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EdgeEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterEdgeEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.rules.EdgeEnablerQuerySession {

    private final org.osid.topology.rules.EdgeEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterEdgeEnablerQuerySession.
     *
     *  @param session the underlying edge enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEdgeEnablerQuerySession(org.osid.topology.rules.EdgeEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeGraph</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeGraph Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the {@codeGraph</code> associated with this 
     *  session.
     *
     *  @return the {@codeGraph</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform {@codeEdgeEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchEdgeEnablers() {
        return (this.session.canSearchEdgeEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include edge enablers in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this graph only.
     */
    
    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    
      
    /**
     *  Gets an edge enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the edge enabler query 
     */
      
    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerQuery getEdgeEnablerQuery() {
        return (this.session.getEdgeEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  edgeEnablerQuery the edge enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code edgeEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code edgeEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablersByQuery(org.osid.topology.rules.EdgeEnablerQuery edgeEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getEdgeEnablersByQuery(edgeEnablerQuery));
    }
}
