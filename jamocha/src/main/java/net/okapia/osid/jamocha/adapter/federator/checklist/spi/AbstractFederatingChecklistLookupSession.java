//
// AbstractFederatingChecklistLookupSession.java
//
//     An abstract federating adapter for a ChecklistLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ChecklistLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingChecklistLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.checklist.ChecklistLookupSession>
    implements org.osid.checklist.ChecklistLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingChecklistLookupSession</code>.
     */

    protected AbstractFederatingChecklistLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.checklist.ChecklistLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Checklist</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupChecklists() {
        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            if (session.canLookupChecklists()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Checklist</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeChecklistView() {
        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            session.useComparativeChecklistView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Checklist</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryChecklistView() {
        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            session.usePlenaryChecklistView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Checklist</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Checklist</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Checklist</code> and
     *  retained for compatibility.
     *
     *  @param  checklistId <code>Id</code> of the
     *          <code>Checklist</code>
     *  @return the checklist
     *  @throws org.osid.NotFoundException <code>checklistId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>checklistId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            try {
                return (session.getChecklist(checklistId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(checklistId + " not found");
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  checklists specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Checklists</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  checklistIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>checklistIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByIds(org.osid.id.IdList checklistIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.checklist.checklist.MutableChecklistList ret = new net.okapia.osid.jamocha.checklist.checklist.MutableChecklistList();

        try (org.osid.id.IdList ids = checklistIds) {
            while (ids.hasNext()) {
                ret.addChecklist(getChecklist(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> which does not include
     *  checklists of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList ret = getChecklistList();

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            ret.addChecklistList(session.getChecklistsByGenusType(checklistGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ChecklistList</code> corresponding to the given
     *  checklist genus <code>Type</code> and include any additional
     *  checklists with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistGenusType a checklist genus type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByParentGenusType(org.osid.type.Type checklistGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList ret = getChecklistList();

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            ret.addChecklistList(session.getChecklistsByParentGenusType(checklistGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ChecklistList</code> containing the given
     *  checklist record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  checklistRecordType a checklist record type 
     *  @return the returned <code>Checklist</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checklistRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByRecordType(org.osid.type.Type checklistRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList ret = getChecklistList();

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            ret.addChecklistList(session.getChecklistsByRecordType(checklistRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ChecklistList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known checklists or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  checklists that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Checklist</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklistsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList ret = getChecklistList();

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            ret.addChecklistList(session.getChecklistsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Checklists</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  checklists or an error results. Otherwise, the returned list
     *  may contain only those checklists that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Checklists</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistList getChecklists()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList ret = getChecklistList();

        for (org.osid.checklist.ChecklistLookupSession session : getSessions()) {
            ret.addChecklistList(session.getChecklists());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.checklist.checklist.FederatingChecklistList getChecklistList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.checklist.ParallelChecklistList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.checklist.checklist.CompositeChecklistList());
        }
    }
}
