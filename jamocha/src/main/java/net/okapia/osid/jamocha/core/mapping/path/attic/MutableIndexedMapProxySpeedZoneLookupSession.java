//
// MutableIndexedMapProxySpeedZoneLookupSession
//
//    Implements a SpeedZone lookup service backed by a collection of
//    speedZones indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a SpeedZone lookup service backed by a collection of
 *  speedZones. The speed zones are indexed by <code>Id</code>, genus
 *  and record types.
 *
 *  The type indices are created from <code>getGenusType()</code>
 *  and <code>getRecordTypes()</code>. Some speedZones may be compatible
 *  with more types than are indicated through these speedZone
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The collection of speed zones can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxySpeedZoneLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapSpeedZoneLookupSession
    implements org.osid.mapping.path.SpeedZoneLookupSession {


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapProxySpeedZoneLookupSession</code> with
     *  no speed zone.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>proxy</code> is
     *          <code>null</code>
     */

    public MutableIndexedMapProxySpeedZoneLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapProxySpeedZoneLookupSession</code> with
     *  a single speed zone.
     *
     *  @param  speedZone an speed zone
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>speedZone</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableIndexedMapProxySpeedZoneLookupSession(org.osid.mapping.path.SpeedZone speedZone, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSpeedZone(speedZone);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapProxySpeedZoneLookupSession</code> using
     *  an array of speed zones.
     *
     *  @param  speedZones an array of speed zones
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>speedZones</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableIndexedMapProxySpeedZoneLookupSession(org.osid.mapping.path.SpeedZone[] speedZones, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSpeedZones(speedZones);
        return;
    }


    /**
     *  Constructs a new <code>MutableIndexedMapProxySpeedZoneLookupSession</code> using
     *  a collection of speed zones.
     *
     *  @param  speedZones a collection of speed zones
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException <code>speedZones</code> or
     *          <code>proxy</code> is <code>null</code>
     */

    public MutableIndexedMapProxySpeedZoneLookupSession(java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putSpeedZones(speedZones);
        return;
    }

    
    /**
     *  Makes a <code>SpeedZone</code> available in this session.
     *
     *  @param  speedZone a speed zone
     *  @throws org.osid.NullArgumentException <code>speedZone<code>
     *          is <code>null</code>
     */

    @Override
    public void putSpeedZone(org.osid.mapping.path.SpeedZone speedZone) {
        super.putSpeedZone(speedZone);
        return;
    }


    /**
     *  Makes an array of speed zones available in this session.
     *
     *  @param  speedZones an array of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZones<code>
     *          is <code>null</code>
     */

    @Override
    public void putSpeedZones(org.osid.mapping.path.SpeedZone[] speedZones) {
        super.putSpeedZones(speedZones);
        return;
    }


    /**
     *  Makes collection of speed zones available in this session.
     *
     *  @param  speedZones a collection of speed zones
     *  @throws org.osid.NullArgumentException <code>speedZone<code>
     *          is <code>null</code>
     */

    @Override
    public void putSpeedZones(java.util.Collection<? extends org.osid.mapping.path.SpeedZone> speedZones) {
        super.putSpeedZones(speedZones);
        return;
    }


    /**
     *  Removes a SpeedZone from this session.
     *
     *  @param speedZoneId the <code>Id</code> of the speed zone
     *  @throws org.osid.NullArgumentException <code>speedZoneId<code> is
     *          <code>null</code>
     */

    @Override
    public void removeSpeedZone(org.osid.id.Id speedZoneId) {
        super.removeSpeedZone(speedZoneId);
        return;
    }    
}
