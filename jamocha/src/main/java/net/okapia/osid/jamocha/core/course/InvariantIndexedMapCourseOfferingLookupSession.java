//
// InvariantIndexedMapCourseOfferingLookupSession
//
//    Implements a CourseOffering lookup service backed by a fixed
//    collection of courseOfferings indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a CourseOffering lookup service backed by a fixed
 *  collection of course offerings. The course offerings are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some course offerings may be compatible
 *  with more types than are indicated through these course offering
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapCourseOfferingLookupSession} using an
     *  array of courseOfferings.
     *
     *  @param courseCatalog the course catalog
     *  @param courseOfferings an array of course offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code courseOfferings} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    org.osid.course.CourseOffering[] courseOfferings) {

        setCourseCatalog(courseCatalog);
        putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapCourseOfferingLookupSession} using a
     *  collection of course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param courseOfferings a collection of course offerings
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code courseOfferings} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                    java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings) {

        setCourseCatalog(courseCatalog);
        putCourseOfferings(courseOfferings);
        return;
    }
}
