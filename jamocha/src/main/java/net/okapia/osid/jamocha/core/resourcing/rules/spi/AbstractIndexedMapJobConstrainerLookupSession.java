//
// AbstractIndexedMapJobConstrainerLookupSession.java
//
//    A simple framework for providing a JobConstrainer lookup service
//    backed by a fixed collection of job constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a JobConstrainer lookup service backed by a
 *  fixed collection of job constrainers. The job constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some job constrainers may be compatible
 *  with more types than are indicated through these job constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJobConstrainerLookupSession
    extends AbstractMapJobConstrainerLookupSession
    implements org.osid.resourcing.rules.JobConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobConstrainer> jobConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobConstrainer> jobConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobConstrainer>());


    /**
     *  Makes a <code>JobConstrainer</code> available in this session.
     *
     *  @param  jobConstrainer a job constrainer
     *  @throws org.osid.NullArgumentException <code>jobConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJobConstrainer(org.osid.resourcing.rules.JobConstrainer jobConstrainer) {
        super.putJobConstrainer(jobConstrainer);

        this.jobConstrainersByGenus.put(jobConstrainer.getGenusType(), jobConstrainer);
        
        try (org.osid.type.TypeList types = jobConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobConstrainersByRecord.put(types.getNextType(), jobConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a job constrainer from this session.
     *
     *  @param jobConstrainerId the <code>Id</code> of the job constrainer
     *  @throws org.osid.NullArgumentException <code>jobConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJobConstrainer(org.osid.id.Id jobConstrainerId) {
        org.osid.resourcing.rules.JobConstrainer jobConstrainer;
        try {
            jobConstrainer = getJobConstrainer(jobConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.jobConstrainersByGenus.remove(jobConstrainer.getGenusType());

        try (org.osid.type.TypeList types = jobConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobConstrainersByRecord.remove(types.getNextType(), jobConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJobConstrainer(jobConstrainerId);
        return;
    }


    /**
     *  Gets a <code>JobConstrainerList</code> corresponding to the given
     *  job constrainer genus <code>Type</code> which does not include
     *  job constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known job constrainers or an error results. Otherwise,
     *  the returned list may contain only those job constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  jobConstrainerGenusType a job constrainer genus type 
     *  @return the returned <code>JobConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByGenusType(org.osid.type.Type jobConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobconstrainer.ArrayJobConstrainerList(this.jobConstrainersByGenus.get(jobConstrainerGenusType)));
    }


    /**
     *  Gets a <code>JobConstrainerList</code> containing the given
     *  job constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known job constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  job constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  jobConstrainerRecordType a job constrainer record type 
     *  @return the returned <code>jobConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerList getJobConstrainersByRecordType(org.osid.type.Type jobConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobconstrainer.ArrayJobConstrainerList(this.jobConstrainersByRecord.get(jobConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobConstrainersByGenus.clear();
        this.jobConstrainersByRecord.clear();

        super.close();

        return;
    }
}
