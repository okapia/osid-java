//
// AbstractDirectionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDirectionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.recipe.DirectionSearchResults {

    private org.osid.recipe.DirectionList directions;
    private final org.osid.recipe.DirectionQueryInspector inspector;
    private final java.util.Collection<org.osid.recipe.records.DirectionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDirectionSearchResults.
     *
     *  @param directions the result set
     *  @param directionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>directions</code>
     *          or <code>directionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDirectionSearchResults(org.osid.recipe.DirectionList directions,
                                            org.osid.recipe.DirectionQueryInspector directionQueryInspector) {
        nullarg(directions, "directions");
        nullarg(directionQueryInspector, "direction query inspectpr");

        this.directions = directions;
        this.inspector = directionQueryInspector;

        return;
    }


    /**
     *  Gets the direction list resulting from a search.
     *
     *  @return a direction list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.recipe.DirectionList getDirections() {
        if (this.directions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.recipe.DirectionList directions = this.directions;
        this.directions = null;
	return (directions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.recipe.DirectionQueryInspector getDirectionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  direction search record <code> Type. </code> This method must
     *  be used to retrieve a direction implementing the requested
     *  record.
     *
     *  @param directionSearchRecordType a direction search 
     *         record type 
     *  @return the direction search
     *  @throws org.osid.NullArgumentException
     *          <code>directionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(directionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionSearchResultsRecord getDirectionSearchResultsRecord(org.osid.type.Type directionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.recipe.records.DirectionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(directionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(directionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record direction search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDirectionRecord(org.osid.recipe.records.DirectionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "direction record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
