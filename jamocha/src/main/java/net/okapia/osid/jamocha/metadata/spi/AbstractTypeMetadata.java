//
// AbstractTypeMetadata.java
//
//     Defines a type Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a type Metadata.
 */

public abstract class AbstractTypeMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final java.util.Collection<org.osid.type.Type> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.type.Type> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.type.Type> existing = new java.util.LinkedHashSet<>();    
    

    /**
     *  Constructs a new {@code AbstractTypeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractTypeMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.TYPE, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractTypeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractTypeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.TYPE, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable type values. 
     *
     *  @return a set of types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TYPE </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getTypeSet() {
        return (this.set.toArray(new org.osid.type.Type[this.set.size()]));
    }

    
    /**
     *  Sets the type set.
     *
     *  @param values a collection of accepted type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setTypeSet(java.util.Collection<org.osid.type.Type> values) {
        this.set.clear();
        addToTypeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the type set.
     *
     *  @param values a collection of accepted type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToTypeSet(java.util.Collection<org.osid.type.Type> values) {
        nullarg(values, "type set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the type set.
     *
     *  @param value a type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToTypeSet(org.osid.type.Type value) {
        nullarg(value, "type value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the type set.
     *
     *  @param value a type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromTypeSet(org.osid.type.Type value) {
        nullarg(value, "type value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the type set.
     */

    protected void clearTypeSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default Type values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default Type values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TYPE </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.type.Type[] getDefaultTypeValues() {
        return (this.defvals.toArray(new org.osid.type.Type[this.defvals.size()]));
    }


    /**
     *  Sets the default Type set.
     *
     *  @param values a collection of default Type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultTypeValues(java.util.Collection<org.osid.type.Type> values) {
        clearDefaultTypeValues();
        addDefaultTypeValues(values);
        return;
    }


    /**
     *  Adds a collection of default Type values.
     *
     *  @param values a collection of default Type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultTypeValues(java.util.Collection<org.osid.type.Type> values) {
        nullarg(values, "default Type values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultTypeValue(org.osid.type.Type value) {
        nullarg(value, "default Type value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultTypeValue(org.osid.type.Type value) {
        nullarg(value, "default Type value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default Type values.
     */

    protected void clearDefaultTypeValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing Type values. If <code> hasValue() </code>
     *  and <code> isRequired() </code> are <code> false, </code> then
     *  these values are the default values. If <code> isArray()
     *  </code> is false, then this method returns at most a single
     *  value.
     *
     *  @return the existing Type values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          TYPE </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getExistingTypeValues() {
        return (this.existing.toArray(new org.osid.type.Type[this.existing.size()]));
    }


    /**
     *  Sets the existing Type set.
     *
     *  @param values a collection of existing Type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingTypeValues(java.util.Collection<org.osid.type.Type> values) {
        clearExistingTypeValues();
        addExistingTypeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing Type values.
     *
     *  @param values a collection of existing Type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingTypeValues(java.util.Collection<org.osid.type.Type> values) {
        nullarg(values, "existing Type values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingTypeValue(org.osid.type.Type value) {
        nullarg(value, "existing Type value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingTypeValue(org.osid.type.Type value) {
        nullarg(value, "existing Type value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing Type values.
     */

    protected void clearExistingTypeValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }            
}