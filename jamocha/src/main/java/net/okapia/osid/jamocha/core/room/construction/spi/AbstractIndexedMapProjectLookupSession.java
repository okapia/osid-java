//
// AbstractIndexedMapProjectLookupSession.java
//
//    A simple framework for providing a Project lookup service
//    backed by a fixed collection of projects with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Project lookup service backed by a
 *  fixed collection of projects. The projects are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some projects may be compatible
 *  with more types than are indicated through these project
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Projects</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProjectLookupSession
    extends AbstractMapProjectLookupSession
    implements org.osid.room.construction.ProjectLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.construction.Project> projectsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.construction.Project>());
    private final MultiMap<org.osid.type.Type, org.osid.room.construction.Project> projectsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.construction.Project>());


    /**
     *  Makes a <code>Project</code> available in this session.
     *
     *  @param  project a project
     *  @throws org.osid.NullArgumentException <code>project<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProject(org.osid.room.construction.Project project) {
        super.putProject(project);

        this.projectsByGenus.put(project.getGenusType(), project);
        
        try (org.osid.type.TypeList types = project.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.projectsByRecord.put(types.getNextType(), project);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a project from this session.
     *
     *  @param projectId the <code>Id</code> of the project
     *  @throws org.osid.NullArgumentException <code>projectId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProject(org.osid.id.Id projectId) {
        org.osid.room.construction.Project project;
        try {
            project = getProject(projectId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.projectsByGenus.remove(project.getGenusType());

        try (org.osid.type.TypeList types = project.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.projectsByRecord.remove(types.getNextType(), project);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProject(projectId);
        return;
    }


    /**
     *  Gets a <code>ProjectList</code> corresponding to the given
     *  project genus <code>Type</code> which does not include
     *  projects of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known projects or an error results. Otherwise,
     *  the returned list may contain only those projects that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned <code>Project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.project.ArrayProjectList(this.projectsByGenus.get(projectGenusType)));
    }


    /**
     *  Gets a <code>ProjectList</code> containing the given
     *  project record <code>Type</code>. In plenary mode, the
     *  returned list contains all known projects or an error
     *  results. Otherwise, the returned list may contain only those
     *  projects that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  projectRecordType a project record type 
     *  @return the returned <code>project</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>projectRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByRecordType(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.construction.project.ArrayProjectList(this.projectsByRecord.get(projectRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.projectsByGenus.clear();
        this.projectsByRecord.clear();

        super.close();

        return;
    }
}
