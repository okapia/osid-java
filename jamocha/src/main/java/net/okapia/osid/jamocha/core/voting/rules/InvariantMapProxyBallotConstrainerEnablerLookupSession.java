//
// InvariantMapProxyBallotConstrainerEnablerLookupSession
//
//    Implements a BallotConstrainerEnabler lookup service backed by a fixed
//    collection of ballotConstrainerEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a BallotConstrainerEnabler lookup service backed by a fixed
 *  collection of ballot constrainer enablers. The ballot constrainer enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyBallotConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractMapBallotConstrainerEnablerLookupSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBallotConstrainerEnablerLookupSession} with no
     *  ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyBallotConstrainerEnablerLookupSession} with a single
     *  ballot constrainer enabler.
     *
     *  @param polls the polls
     *  @param ballotConstrainerEnabler a single ballot constrainer enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballotConstrainerEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainerEnabler ballotConstrainerEnabler, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallotConstrainerEnabler(ballotConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBallotConstrainerEnablerLookupSession} using
     *  an array of ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @param ballotConstrainerEnablers an array of ballot constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballotConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.BallotConstrainerEnabler[] ballotConstrainerEnablers, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBallotConstrainerEnablerLookupSession} using a
     *  collection of ballot constrainer enablers.
     *
     *  @param polls the polls
     *  @param ballotConstrainerEnablers a collection of ballot constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballotConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyBallotConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.BallotConstrainerEnabler> ballotConstrainerEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putBallotConstrainerEnablers(ballotConstrainerEnablers);
        return;
    }
}
