//
// AbstractInquiryRulesManager.java
//
//     An adapter for a InquiryRulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InquiryRulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInquiryRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.inquiry.rules.InquiryRulesManager>
    implements org.osid.inquiry.rules.InquiryRulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterInquiryRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInquiryRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInquiryRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInquiryRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerLookup() {
        return (getAdapteeManager().supportsInquiryEnablerLookup());
    }


    /**
     *  Tests if querying inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerQuery() {
        return (getAdapteeManager().supportsInquiryEnablerQuery());
    }


    /**
     *  Tests if searching inquiry enablers is supported. 
     *
     *  @return <code> true </code> if inquiry enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSearch() {
        return (getAdapteeManager().supportsInquiryEnablerSearch());
    }


    /**
     *  Tests if an inquiry enabler administrative service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerAdmin() {
        return (getAdapteeManager().supportsInquiryEnablerAdmin());
    }


    /**
     *  Tests if an inquiry enabler notification service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerNotification() {
        return (getAdapteeManager().supportsInquiryEnablerNotification());
    }


    /**
     *  Tests if an inquiry enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry enabler inquest lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerInquest() {
        return (getAdapteeManager().supportsInquiryEnablerInquest());
    }


    /**
     *  Tests if an inquiry enabler inquest service is supported. 
     *
     *  @return <code> true </code> if inquiry enabler inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerInquestAssignment() {
        return (getAdapteeManager().supportsInquiryEnablerInquestAssignment());
    }


    /**
     *  Tests if an inquiry enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an inquiry enabler inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSmartInquest() {
        return (getAdapteeManager().supportsInquiryEnablerSmartInquest());
    }


    /**
     *  Tests if looking up audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerLookup() {
        return (getAdapteeManager().supportsAuditEnablerLookup());
    }


    /**
     *  Tests if querying audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerQuery() {
        return (getAdapteeManager().supportsAuditEnablerQuery());
    }


    /**
     *  Tests if searching audit enablers is supported. 
     *
     *  @return <code> true </code> if audit enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSearch() {
        return (getAdapteeManager().supportsAuditEnablerSearch());
    }


    /**
     *  Tests if an audit enabler administrative service is supported. 
     *
     *  @return <code> true </code> if audit enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerAdmin() {
        return (getAdapteeManager().supportsAuditEnablerAdmin());
    }


    /**
     *  Tests if an audit enabler notification service is supported. 
     *
     *  @return <code> true </code> if audit enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerNotification() {
        return (getAdapteeManager().supportsAuditEnablerNotification());
    }


    /**
     *  Tests if an audit enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler inquest lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerInquest() {
        return (getAdapteeManager().supportsAuditEnablerInquest());
    }


    /**
     *  Tests if an audit enabler inquest service is supported. 
     *
     *  @return <code> true </code> if audit enabler inquest assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerInquestAssignment() {
        return (getAdapteeManager().supportsAuditEnablerInquestAssignment());
    }


    /**
     *  Tests if an audit enabler inquest lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler inquest service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSmartInquest() {
        return (getAdapteeManager().supportsAuditEnablerSmartInquest());
    }


    /**
     *  Tests if an audit enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an audit enabler rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRuleLookup() {
        return (getAdapteeManager().supportsAuditEnablerRuleLookup());
    }


    /**
     *  Tests if an audit enabler rule application service is supported. 
     *
     *  @return <code> true </code> if audit enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRuleApplication() {
        return (getAdapteeManager().supportsAuditEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> InquiryEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> InquiryEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryEnablerRecordTypes() {
        return (getAdapteeManager().getInquiryEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> InquiryEnabler </code> record type is 
     *  supported. 
     *
     *  @param  inquiryEnablerRecordType a <code> Type </code> indicating an 
     *          <code> InquiryEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerRecordType(org.osid.type.Type inquiryEnablerRecordType) {
        return (getAdapteeManager().supportsInquiryEnablerRecordType(inquiryEnablerRecordType));
    }


    /**
     *  Gets the supported <code> InquiryEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> InquiryEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInquiryEnablerSearchRecordTypes() {
        return (getAdapteeManager().getInquiryEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> InquiryEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  inquiryEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> InquiryEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inquiryEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInquiryEnablerSearchRecordType(org.osid.type.Type inquiryEnablerSearchRecordType) {
        return (getAdapteeManager().supportsInquiryEnablerSearchRecordType(inquiryEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> AuditEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> AuditEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditEnablerRecordTypes() {
        return (getAdapteeManager().getAuditEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> AuditEnabler </code> record type is 
     *  supported. 
     *
     *  @param  auditEnablerRecordType a <code> Type </code> indicating an 
     *          <code> AuditEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> auditEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditEnablerRecordType(org.osid.type.Type auditEnablerRecordType) {
        return (getAdapteeManager().supportsAuditEnablerRecordType(auditEnablerRecordType));
    }


    /**
     *  Gets the supported <code> AuditEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> AuditEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuditEnablerSearchRecordTypes() {
        return (getAdapteeManager().getAuditEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AuditEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  auditEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> AuditEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auditEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuditEnablerSearchRecordType(org.osid.type.Type auditEnablerSearchRecordType) {
        return (getAdapteeManager().supportsAuditEnablerSearchRecordType(auditEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service. 
     *
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerLookupSession getInquiryEnablerLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service. 
     *
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerQuerySession getInquiryEnablerQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerQuerySessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler search service. 
     *
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enablers earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSearchSession getInquiryEnablerSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerSearchSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service. 
     *
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerAdminSession getInquiryEnablerAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerAdminSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSession(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerNotificationSession(inquiryEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler notification service for the given inquest. 
     *
     *  @param  inquiryEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquiryEnablerReceiver 
     *          </code> or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerNotificationSession getInquiryEnablerNotificationSessionForInquest(org.osid.inquiry.rules.InquiryEnablerReceiver inquiryEnablerReceiver, 
                                                                                                                   org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerNotificationSessionForInquest(inquiryEnablerReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup inquiry enabler/inquest 
     *  mappings for inquiry enablers. 
     *
     *  @return an <code> InquiryEnablerInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestSession getInquiryEnablerInquestSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerInquestSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inquiry 
     *  enablers to inquests. 
     *
     *  @return an <code> InquiryEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerInquestAssignmentSession getInquiryEnablerInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerInquestAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage inquiry enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerSmartInquestSession getInquiryEnablerSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerSmartInquestSession(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service. 
     *
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleLookupSession getInquiryEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerRuleLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service. 
     *
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the inquiry 
     *  enabler assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> InquiryEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.InquiryEnablerRuleApplicationSession getInquiryEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryEnablerRuleApplicationSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service. 
     *
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerLookupSession getAuditEnablerLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service. 
     *
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  query service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerQuerySession getAuditEnablerQuerySessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerQuerySessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  search service. 
     *
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enablers 
     *  earch service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSearchSession getAuditEnablerSearchSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerSearchSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service. 
     *
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  administration service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerAdminSession getAuditEnablerAdminSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerAdminSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSession(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerNotificationSession(auditEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  notification service for the given inquest. 
     *
     *  @param  auditEnablerReceiver the notification callback 
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no inquest found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auditEnablerReceiver 
     *          </code> or <code> inquestId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerNotificationSession getAuditEnablerNotificationSessionForInquest(org.osid.inquiry.rules.AuditEnablerReceiver auditEnablerReceiver, 
                                                                                                               org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerNotificationSessionForInquest(auditEnablerReceiver, inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup audit enabler/inquest 
     *  mappings for audit enablers. 
     *
     *  @return an <code> AuditEnablerInquestSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquest() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestSession getAuditEnablerInquestSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerInquestSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning audit 
     *  enablers to inquests. 
     *
     *  @return an <code> AuditEnablerInquestAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerInquestAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerInquestAssignmentSession getAuditEnablerInquestAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerInquestAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage audit enabler smart 
     *  inquests. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerSmartInquestSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerSmartInquest() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerSmartInquestSession getAuditEnablerSmartInquestSession(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerSmartInquestSession(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  mapping lookup service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleLookupSession getAuditEnablerRuleLookupSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerRuleLookupSessionForInquest(inquestId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service. 
     *
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the audit enabler 
     *  assignment service for the given inquest. 
     *
     *  @param  inquestId the <code> Id </code> of the <code> Inquest </code> 
     *  @return an <code> AuditEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Inquest </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuditEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerRuleApplicationSession getAuditEnablerRuleApplicationSessionForInquest(org.osid.id.Id inquestId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuditEnablerRuleApplicationSessionForInquest(inquestId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
