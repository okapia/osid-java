//
// AbstractStockQuery.java
//
//     A template for making a Stock Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.stock.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for stocks.
 */

public abstract class AbstractStockQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.inventory.StockQuery {

    private final java.util.Collection<org.osid.inventory.records.StockQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a sku number. 
     *
     *  @param  sku a sku 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sku </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchSKU(String sku, org.osid.type.Type stringMatchType, 
                         boolean match) {
        return;
    }


    /**
     *  Matches items that have any sku. 
     *
     *  @param  match <code> true </code> to match items with any sku, <code> 
     *          false </code> to match items with no sku 
     */

    @OSID @Override
    public void matchAnySKU(boolean match) {
        return;
    }


    /**
     *  Clears the sku terms. 
     */

    @OSID @Override
    public void clearSKUTerms() {
        return;
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        return;
    }


    /**
     *  Clears the model <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inventory. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any related model. 
     *
     *  @param  match <code> true </code> to match stocks with any model, 
     *          <code> false </code> to match stocks with no models 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        return;
    }


    /**
     *  Matches a location description. 
     *
     *  @param  location a location string 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches items that have any location description. 
     *
     *  @param  match <code> true </code> to match stocks with any location 
     *          string, <code> false </code> to match stocks with no location 
     *          string 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches items that have any location. 
     *
     *  @param  match <code> true </code> to match stocks with any location, 
     *          <code> false </code> to match stocks with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the stock <code> Id </code> for this query to match stocks that 
     *  have the specified stock as an ancestor. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorStockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getAncestorStockQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorStockQuery() is false");
    }


    /**
     *  Matches stocks with any stock ancestor. 
     *
     *  @param  match <code> true </code> to match stocks with any ancestor, 
     *          <code> false </code> to match root stocks 
     */

    @OSID @Override
    public void matchAnyAncestorStock(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor stock query terms. 
     */

    @OSID @Override
    public void clearAncestorStockTerms() {
        return;
    }


    /**
     *  Sets the stock <code> Id </code> for this query to match stocks that 
     *  have the specified stock as an descendant. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantStockQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getDescendantStockQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantStockQuery() is false");
    }


    /**
     *  Matches stocks with any descendant stock. 
     *
     *  @param  match <code> true </code> to match stocks with any descendant, 
     *          <code> false </code> to match leaf stocks 
     */

    @OSID @Override
    public void matchAnyDescendantStock(boolean match) {
        return;
    }


    /**
     *  Clears the descendant stock query terms. 
     */

    @OSID @Override
    public void clearDescendantStockTerms() {
        return;
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match stocks 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given stock query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a stock implementing the requested record.
     *
     *  @param stockRecordType a stock record type
     *  @return the stock query record
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stockRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.StockQueryRecord getStockQueryRecord(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.records.StockQueryRecord record : this.records) {
            if (record.implementsRecordType(stockRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stockRecordType + " is not supported");
    }


    /**
     *  Adds a record to this stock query. 
     *
     *  @param stockQueryRecord stock query record
     *  @param stockRecordType stock record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStockQueryRecord(org.osid.inventory.records.StockQueryRecord stockQueryRecord, 
                                          org.osid.type.Type stockRecordType) {

        addRecordType(stockRecordType);
        nullarg(stockQueryRecord, "stock query record");
        this.records.add(stockQueryRecord);        
        return;
    }
}
