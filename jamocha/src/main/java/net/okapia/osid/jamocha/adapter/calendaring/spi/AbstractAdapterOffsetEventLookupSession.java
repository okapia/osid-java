//
// AbstractAdapterOffsetEventLookupSession.java
//
//    An OffsetEvent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OffsetEvent lookup session adapter.
 */

public abstract class AbstractAdapterOffsetEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.OffsetEventLookupSession {

    private final org.osid.calendaring.OffsetEventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOffsetEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOffsetEventLookupSession(org.osid.calendaring.OffsetEventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code OffsetEvent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOffsetEvents() {
        return (this.session.canLookupOffsetEvents());
    }


    /**
     *  A complete view of the {@code OffsetEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOffsetEventView() {
        this.session.useComparativeOffsetEventView();
        return;
    }


    /**
     *  A complete view of the {@code OffsetEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOffsetEventView() {
        this.session.usePlenaryOffsetEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offset events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active offset events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOffsetEventView() {
        this.session.useActiveOffsetEventView();
        return;
    }


    /**
     *  Active and inactive offset events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOffsetEventView() {
        this.session.useAnyStatusOffsetEventView();
        return;
    }
    
     
    /**
     *  Gets the {@code OffsetEvent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code OffsetEvent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code OffsetEvent} and
     *  retained for compatibility.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param offsetEventId {@code Id} of the {@code OffsetEvent}
     *  @return the offset event
     *  @throws org.osid.NotFoundException {@code offsetEventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code offsetEventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEvent getOffsetEvent(org.osid.id.Id offsetEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEvent(offsetEventId));
    }


    /**
     *  Gets an {@code OffsetEventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offsetEvents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code OffsetEvents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code OffsetEvent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByIds(org.osid.id.IdList offsetEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventsByIds(offsetEventIds));
    }


    /**
     *  Gets an {@code OffsetEventList} corresponding to the given
     *  offset event genus {@code Type} which does not include
     *  offset events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned {@code OffsetEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventsByGenusType(offsetEventGenusType));
    }


    /**
     *  Gets an {@code OffsetEventList} corresponding to the given
     *  offset event genus {@code Type} and include any additional
     *  offset events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventGenusType an offsetEvent genus type 
     *  @return the returned {@code OffsetEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByParentGenusType(org.osid.type.Type offsetEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventsByParentGenusType(offsetEventGenusType));
    }


    /**
     *  Gets an {@code OffsetEventList} containing the given
     *  offset event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  offsetEventRecordType an offsetEvent record type 
     *  @return the returned {@code OffsetEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByRecordType(org.osid.type.Type offsetEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventsByRecordType(offsetEventRecordType));
    }

    /**
     *  Gets the {@code OffsetEvents} using the given event as a start
     *  or ending offset.
     *  
     *  In plenary mode, the returned list contains all known offset
     *  events or an error results. Otherwise, the returned list may
     *  contain only those offset events that are accessible through
     *  this session.
     *  
     *  In active mode, offset events are returned that are currently
     *  active.  In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @param  eventId {@code Id} of the related event 
     *  @return the offset events 
     *  @throws org.osid.NullArgumentException {@code eventId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEventsByEvent(org.osid.id.Id eventId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventsByEvent(eventId));
    }


    /**
     *  Gets all {@code OffsetEvents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offset events or an error results. Otherwise, the returned list
     *  may contain only those offset events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset events are returned that are currently
     *  active. In any status mode, active and inactive offset events
     *  are returned.
     *
     *  @return a list of {@code OffsetEvents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventList getOffsetEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEvents());
    }
}
