//
// InvariantMapInstallationLookupSession
//
//    Implements an Installation lookup service backed by a fixed collection of
//    installations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements an Installation lookup service backed by a fixed
 *  collection of installations. The installations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapInstallationLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapInstallationLookupSession
    implements org.osid.installation.InstallationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapInstallationLookupSession</code> with no
     *  installations.
     *  
     *  @param site the site
     *  @throws org.osid.NullArgumnetException {@code site} is
     *          {@code null}
     */

    public InvariantMapInstallationLookupSession(org.osid.installation.Site site) {
        setSite(site);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInstallationLookupSession</code> with a single
     *  installation.
     *  
     *  @param site the site
     *  @param installation an single installation
     *  @throws org.osid.NullArgumentException {@code site} or
     *          {@code installation} is <code>null</code>
     */

      public InvariantMapInstallationLookupSession(org.osid.installation.Site site,
                                               org.osid.installation.Installation installation) {
        this(site);
        putInstallation(installation);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInstallationLookupSession</code> using an array
     *  of installations.
     *  
     *  @param site the site
     *  @param installations an array of installations
     *  @throws org.osid.NullArgumentException {@code site} or
     *          {@code installations} is <code>null</code>
     */

      public InvariantMapInstallationLookupSession(org.osid.installation.Site site,
                                               org.osid.installation.Installation[] installations) {
        this(site);
        putInstallations(installations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapInstallationLookupSession</code> using a
     *  collection of installations.
     *
     *  @param site the site
     *  @param installations a collection of installations
     *  @throws org.osid.NullArgumentException {@code site} or
     *          {@code installations} is <code>null</code>
     */

      public InvariantMapInstallationLookupSession(org.osid.installation.Site site,
                                               java.util.Collection<? extends org.osid.installation.Installation> installations) {
        this(site);
        putInstallations(installations);
        return;
    }
}
