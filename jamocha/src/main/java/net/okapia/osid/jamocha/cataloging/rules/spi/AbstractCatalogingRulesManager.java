//
// AbstractCatalogingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCatalogingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.cataloging.rules.CatalogingRulesManager,
               org.osid.cataloging.rules.CatalogingRulesProxyManager {

    private final Types catalogEnablerRecordTypes          = new TypeRefSet();
    private final Types catalogEnablerSearchRecordTypes    = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCatalogingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCatalogingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching catalog enablers is supported. 
     *
     *  @return <code> true </code> if catalog enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler administrative service is supported. 
     *
     *  @return <code> true </code> if catalog enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler notification service is supported. 
     *
     *  @return <code> true </code> if catalog enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler catalog lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler catalog lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerCatalog() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler catalog service is supported. 
     *
     *  @return <code> true </code> if catalog enabler catalog assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler catalog lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler catalog service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSmartCatalog() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a catalog enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a catalog enabler rule application service is supported. 
     *
     *  @return <code> true </code> if catalog enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> CatalogEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CatalogEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CatalogEnabler </code> record type is 
     *  supported. 
     *
     *  @param  catalogEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CatalogEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerRecordType(org.osid.type.Type catalogEnablerRecordType) {
        return (this.catalogEnablerRecordTypes.contains(catalogEnablerRecordType));
    }


    /**
     *  Adds support for a catalog enabler record type.
     *
     *  @param catalogEnablerRecordType a catalog enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogEnablerRecordType</code> is <code>null</code>
     */

    protected void addCatalogEnablerRecordType(org.osid.type.Type catalogEnablerRecordType) {
        this.catalogEnablerRecordTypes.add(catalogEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a catalog enabler record type.
     *
     *  @param catalogEnablerRecordType a catalog enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCatalogEnablerRecordType(org.osid.type.Type catalogEnablerRecordType) {
        this.catalogEnablerRecordTypes.remove(catalogEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CatalogEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> CatalogEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CatalogEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  catalogEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CatalogEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          catalogEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogEnablerSearchRecordType(org.osid.type.Type catalogEnablerSearchRecordType) {
        return (this.catalogEnablerSearchRecordTypes.contains(catalogEnablerSearchRecordType));
    }


    /**
     *  Adds support for a catalog enabler search record type.
     *
     *  @param catalogEnablerSearchRecordType a catalog enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCatalogEnablerSearchRecordType(org.osid.type.Type catalogEnablerSearchRecordType) {
        this.catalogEnablerSearchRecordTypes.add(catalogEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a catalog enabler search record type.
     *
     *  @param catalogEnablerSearchRecordType a catalog enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCatalogEnablerSearchRecordType(org.osid.type.Type catalogEnablerSearchRecordType) {
        this.catalogEnablerSearchRecordTypes.remove(catalogEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service. 
     *
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerLookupSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler lookup service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerLookupSession getCatalogEnablerLookupSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerLookupSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service. 
     *
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerQuerySessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler query service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuerySession getCatalogEnablerQuerySessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerQuerySessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler search service. 
     *
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enablers earch service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerSearchSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enablers earch service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSearchSession getCatalogEnablerSearchSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerSearchSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service. 
     *
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerAdminSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler administration service for the given catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerAdminSession getCatalogEnablerAdminSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerAdminSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSession(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSession(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service for the given catalog. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalog found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver 
     *          </code> or <code> catalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSessionForCatalog(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver, 
                                                                                                                      org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerNotificationSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler notification service for the given catalog. 
     *
     *  @param  catalogEnablerReceiver the notification callback 
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalog found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogEnablerReceiver, 
     *          catalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerNotificationSession getCatalogEnablerNotificationSessionForCatalog(org.osid.cataloging.rules.CatalogEnablerReceiver catalogEnablerReceiver, 
                                                                                                                      org.osid.id.Id catalogId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerNotificationSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup catalog enabler/catalog 
     *  mappings for cataloging enablers. 
     *
     *  @return a <code> CatalogEnablerCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogSession getCatalogEnablerCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup catalog enabler/catalog 
     *  mappings for cataloging enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogSession getCatalogEnablerCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning catalog 
     *  enablers to catalogs for cataloging. 
     *
     *  @return a <code> CatalogEnablerCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogAssignmentSession getCatalogEnablerCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning catalog 
     *  enablers to catalogs for cataloging. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerCatalogAssignmentSession getCatalogEnablerCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage catalog enabler smart 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerSmartCatalogSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSmartCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSmartCatalogSession getCatalogEnablerSmartCatalogSession(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerSmartCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage catalog enabler smart 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerSmartCatalogSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerSmartCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerSmartCatalogSession getCatalogEnablerSmartCatalogSession(org.osid.id.Id catalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerSmartCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  catalog. 
     *
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  catalog. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for the given catalog for looking up 
     *  rules applied to a catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerRuleLookupSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler mapping lookup service for the given catalog for looking up 
     *  rules applied to a catalog. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleLookupSession getCatalogEnablerRuleLookupSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerRuleLookupSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service to apply enablers to catalogs. 
     *
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service to apply enablers to catalogs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service for the given catalog to apply enablers to 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSessionForCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesManager.getCatalogEnablerRuleApplicationSessionForCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalog 
     *  enabler assignment service for the given catalog to apply enablers to 
     *  catalogs. 
     *
     *  @param  catalogId the <code> Id </code> of the <code> Catalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalog </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerRuleApplicationSession getCatalogEnablerRuleApplicationSessionForCatalog(org.osid.id.Id catalogId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.cataloging.rules.CatalogingRulesProxyManager.getCatalogEnablerRuleApplicationSessionForCatalog not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.catalogEnablerRecordTypes.clear();
        this.catalogEnablerRecordTypes.clear();

        this.catalogEnablerSearchRecordTypes.clear();
        this.catalogEnablerSearchRecordTypes.clear();

        return;
    }
}
