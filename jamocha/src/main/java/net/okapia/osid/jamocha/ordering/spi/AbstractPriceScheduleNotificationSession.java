//
// AbstractPriceScheduleNotificationSession.java
//
//     A template for making PriceScheduleNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code PriceSchedule} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code PriceSchedule} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for price schedule entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractPriceScheduleNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ordering.PriceScheduleNotificationSession {

    private boolean federated = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();


    /**
     *  Gets the {@code Store/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Store Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }

    
    /**
     *  Gets the {@code Store} associated with this session.
     *
     *  @return the {@code Store} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the {@code Store}.
     *
     *  @param store the store for this session
     *  @throws org.osid.NullArgumentException {@code store}
     *          is {@code null}
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can register for {@code PriceSchedule}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForPriceScheduleNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price schedules in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new price schedules. {@code
     *  PriceScheduleReceiver.newPriceSchedule()} is invoked when a
     *  new {@code PriceSchedule} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated price schedules. {@code
     *  PriceScheduleReceiver.changedPriceSchedule()} is invoked when
     *  a price schedule is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated price
     *  schedule. {@code PriceScheduleReceiver.changedPriceSchedule()}
     *  is invoked when the specified price schedule is changed.
     *
     *  @param priceScheduleId the {@code Id} of the {@code PriceSchedule} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code priceScheduleId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted price schedules. {@code
     *  PriceScheduleReceiver.deletedPriceSchedule()} is invoked when
     *  a price schedule is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted price schedule. {@code
     *  PriceScheduleReceiver.deletedPriceSchedule()} is invoked when
     *  the specified price schedule is deleted.
     *
     *  @param priceScheduleId the {@code Id} of the
     *          {@code PriceSchedule} to monitor
     *  @throws org.osid.NullArgumentException {@code priceScheduleId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
