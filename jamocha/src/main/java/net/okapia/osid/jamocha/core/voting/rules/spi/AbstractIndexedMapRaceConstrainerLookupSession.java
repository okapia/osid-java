//
// AbstractIndexedMapRaceConstrainerLookupSession.java
//
//    A simple framework for providing a RaceConstrainer lookup service
//    backed by a fixed collection of race constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RaceConstrainer lookup service backed by a
 *  fixed collection of race constrainers. The race constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some race constrainers may be compatible
 *  with more types than are indicated through these race constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RaceConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRaceConstrainerLookupSession
    extends AbstractMapRaceConstrainerLookupSession
    implements org.osid.voting.rules.RaceConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceConstrainer> raceConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.voting.rules.RaceConstrainer> raceConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.voting.rules.RaceConstrainer>());


    /**
     *  Makes a <code>RaceConstrainer</code> available in this session.
     *
     *  @param  raceConstrainer a race constrainer
     *  @throws org.osid.NullArgumentException <code>raceConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRaceConstrainer(org.osid.voting.rules.RaceConstrainer raceConstrainer) {
        super.putRaceConstrainer(raceConstrainer);

        this.raceConstrainersByGenus.put(raceConstrainer.getGenusType(), raceConstrainer);
        
        try (org.osid.type.TypeList types = raceConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceConstrainersByRecord.put(types.getNextType(), raceConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a race constrainer from this session.
     *
     *  @param raceConstrainerId the <code>Id</code> of the race constrainer
     *  @throws org.osid.NullArgumentException <code>raceConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRaceConstrainer(org.osid.id.Id raceConstrainerId) {
        org.osid.voting.rules.RaceConstrainer raceConstrainer;
        try {
            raceConstrainer = getRaceConstrainer(raceConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.raceConstrainersByGenus.remove(raceConstrainer.getGenusType());

        try (org.osid.type.TypeList types = raceConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.raceConstrainersByRecord.remove(types.getNextType(), raceConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRaceConstrainer(raceConstrainerId);
        return;
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> corresponding to the given
     *  race constrainer genus <code>Type</code> which does not include
     *  race constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known race constrainers or an error results. Otherwise,
     *  the returned list may contain only those race constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  raceConstrainerGenusType a race constrainer genus type 
     *  @return the returned <code>RaceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainer.ArrayRaceConstrainerList(this.raceConstrainersByGenus.get(raceConstrainerGenusType)));
    }


    /**
     *  Gets a <code>RaceConstrainerList</code> containing the given
     *  race constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known race constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  race constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  raceConstrainerRecordType a race constrainer record type 
     *  @return the returned <code>raceConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByRecordType(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainer.ArrayRaceConstrainerList(this.raceConstrainersByRecord.get(raceConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.raceConstrainersByGenus.clear();
        this.raceConstrainersByRecord.clear();

        super.close();

        return;
    }
}
