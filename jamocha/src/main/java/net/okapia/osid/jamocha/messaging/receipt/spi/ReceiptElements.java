//
// ReceiptElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.receipt.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ReceiptElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ReceiptElement Id.
     *
     *  @return the receipt element Id
     */

    public static org.osid.id.Id getReceiptEntityId() {
        return (makeEntityId("osid.messaging.Receipt"));
    }


    /**
     *  Gets the MessageId element Id.
     *
     *  @return the MessageId element Id
     */

    public static org.osid.id.Id getMessageId() {
        return (makeElementId("osid.messaging.receipt.MessageId"));
    }


    /**
     *  Gets the Message element Id.
     *
     *  @return the Message element Id
     */

    public static org.osid.id.Id getMessage() {
        return (makeElementId("osid.messaging.receipt.Message"));
    }


    /**
     *  Gets the ReceivedTime element Id.
     *
     *  @return the ReceivedTime element Id
     */

    public static org.osid.id.Id getReceivedTime() {
        return (makeElementId("osid.messaging.receipt.ReceivedTime"));
    }


    /**
     *  Gets the ReceivingAgentId element Id.
     *
     *  @return the ReceivingAgentId element Id
     */

    public static org.osid.id.Id getReceivingAgentId() {
        return (makeElementId("osid.messaging.receipt.ReceivingAgentId"));
    }


    /**
     *  Gets the ReceivingAgent element Id.
     *
     *  @return the ReceivingAgent element Id
     */

    public static org.osid.id.Id getReceivingAgent() {
        return (makeElementId("osid.messaging.receipt.ReceivingAgent"));
    }


    /**
     *  Gets the RecipientId element Id.
     *
     *  @return the RecipientId element Id
     */

    public static org.osid.id.Id getRecipientId() {
        return (makeElementId("osid.messaging.receipt.RecipientId"));
    }


    /**
     *  Gets the Recipient element Id.
     *
     *  @return the Recipient element Id
     */

    public static org.osid.id.Id getRecipient() {
        return (makeElementId("osid.messaging.receipt.Recipient"));
    }


    /**
     *  Gets the MailboxId element Id.
     *
     *  @return the MailboxId element Id
     */

    public static org.osid.id.Id getMailboxId() {
        return (makeQueryElementId("osid.messaging.receipt.MailboxId"));
    }


    /**
     *  Gets the Mailbox element Id.
     *
     *  @return the Mailbox element Id
     */

    public static org.osid.id.Id getMailbox() {
        return (makeQueryElementId("osid.messaging.receipt.Mailbox"));
    }
}
