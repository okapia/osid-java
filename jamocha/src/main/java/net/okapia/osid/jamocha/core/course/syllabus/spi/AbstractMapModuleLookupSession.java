//
// AbstractMapModuleLookupSession
//
//    A simple framework for providing a Module lookup service
//    backed by a fixed collection of modules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Module lookup service backed by a
 *  fixed collection of modules. The modules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Modules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapModuleLookupSession
    extends net.okapia.osid.jamocha.course.syllabus.spi.AbstractModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.syllabus.Module> modules = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.syllabus.Module>());


    /**
     *  Makes a <code>Module</code> available in this session.
     *
     *  @param  module a module
     *  @throws org.osid.NullArgumentException <code>module<code>
     *          is <code>null</code>
     */

    protected void putModule(org.osid.course.syllabus.Module module) {
        this.modules.put(module.getId(), module);
        return;
    }


    /**
     *  Makes an array of modules available in this session.
     *
     *  @param  modules an array of modules
     *  @throws org.osid.NullArgumentException <code>modules<code>
     *          is <code>null</code>
     */

    protected void putModules(org.osid.course.syllabus.Module[] modules) {
        putModules(java.util.Arrays.asList(modules));
        return;
    }


    /**
     *  Makes a collection of modules available in this session.
     *
     *  @param  modules a collection of modules
     *  @throws org.osid.NullArgumentException <code>modules<code>
     *          is <code>null</code>
     */

    protected void putModules(java.util.Collection<? extends org.osid.course.syllabus.Module> modules) {
        for (org.osid.course.syllabus.Module module : modules) {
            this.modules.put(module.getId(), module);
        }

        return;
    }


    /**
     *  Removes a Module from this session.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @throws org.osid.NullArgumentException <code>moduleId<code> is
     *          <code>null</code>
     */

    protected void removeModule(org.osid.id.Id moduleId) {
        this.modules.remove(moduleId);
        return;
    }


    /**
     *  Gets the <code>Module</code> specified by its <code>Id</code>.
     *
     *  @param  moduleId <code>Id</code> of the <code>Module</code>
     *  @return the module
     *  @throws org.osid.NotFoundException <code>moduleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>moduleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule(org.osid.id.Id moduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.syllabus.Module module = this.modules.get(moduleId);
        if (module == null) {
            throw new org.osid.NotFoundException("module not found: " + moduleId);
        }

        return (module);
    }


    /**
     *  Gets all <code>Modules</code>. In plenary mode, the returned
     *  list contains all known modules or an error
     *  results. Otherwise, the returned list may contain only those
     *  modules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Modules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.module.ArrayModuleList(this.modules.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.modules.clear();
        super.close();
        return;
    }
}
