//
// AbstractSupersedingEventLookupSession.java
//
//    A starter implementation framework for providing a SupersedingEvent
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a SupersedingEvent
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getSupersedingEvents(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractSupersedingEventLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();
    

    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }

    /**
     *  Tests if this user can perform <code>SupersedingEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSupersedingEvents() {
        return (true);
    }


    /**
     *  A complete view of the <code>SupersedingEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSupersedingEventView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>SupersedingEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySupersedingEventView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding events in calendars which are
     *  children of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active superseding events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive superseding events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SupersedingEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SupersedingEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SupersedingEvent</code> and
     *  retained for compatibility.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventId <code>Id</code> of the
     *          <code>SupersedingEvent</code>
     *  @return the superseding event
     *  @throws org.osid.NotFoundException <code>supersedingEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>supersedingEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getSupersedingEvent(org.osid.id.Id supersedingEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.SupersedingEventList supersedingEvents = getSupersedingEvents()) {
            while (supersedingEvents.hasNext()) {
                org.osid.calendaring.SupersedingEvent supersedingEvent = supersedingEvents.getNextSupersedingEvent();
                if (supersedingEvent.getId().equals(supersedingEventId)) {
                    return (supersedingEvent);
                }
            }
        } 

        throw new org.osid.NotFoundException(supersedingEventId + " not found");
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SupersedingEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getSupersedingEvents()</code>.
     *
     *  @param  supersedingEventIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByIds(org.osid.id.IdList supersedingEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.calendaring.SupersedingEvent> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = supersedingEventIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getSupersedingEvent(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("superseding event " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.calendaring.supersedingevent.LinkedSupersedingEventList(ret));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> which does not include
     *  superseding events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getSupersedingEvents()</code>.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.supersedingevent.SupersedingEventGenusFilterList(getSupersedingEvents(), supersedingEventGenusType));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> and include any additional
     *  superseding events with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSupersedingEvents()</code>.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByParentGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getSupersedingEventsByGenusType(supersedingEventGenusType));
    }


    /**
     *  Gets a <code>SupersedingEventList</code> containing the given
     *  superseding event record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, superseding events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getSupersedingEvents()</code>.
     *
     *  @param  supersedingEventRecordType a supersedingEvent record type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByRecordType(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.supersedingevent.SupersedingEventRecordFilterList(getSupersedingEvents(), supersedingEventRecordType));
    }


    /**
     *  Gets the <code>SupersedingEvents</code> related to the
     *  relative event <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session.
     *
     *  In active mode, supersdeing events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  @param  supersededEventId <code>Id</code> of the related event
     *  @return the superseding events
     *  @throws org.osid.NotFoundException
     *          <code>supersededEventId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>supersededEventId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsBySupersededEvent(org.osid.id.Id supersededEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.calendaring.supersedingevent.SupersedingEventFilterList(new SupersededEventFilter(supersededEventId), getSupersedingEvents()));
    }


    /**
     *  Gets all <code>SupersedingEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, superseding events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  @return a list of <code>SupersedingEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the superseding event list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of superseding events
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.calendaring.SupersedingEventList filterSupersedingEventsOnViews(org.osid.calendaring.SupersedingEventList list)
        throws org.osid.OperationFailedException {
            
        org.osid.calendaring.SupersedingEventList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.calendaring.supersedingevent.ActiveSupersedingEventFilterList(ret);
        }

        return (ret);
    }


    public static class SupersededEventFilter
        implements net.okapia.osid.jamocha.inline.filter.calendaring.supersedingevent.SupersedingEventFilter {
         
        private final org.osid.id.Id eventId;
         
         
        /**
         *  Constructs a new <code>SupersededEventFilter</code>.
         *
         *  @param eventId the superseded event to filter
         *  @throws org.osid.NullArgumentException
         *          <code>eventId</code> is <code>null</code>
         */
        
        public SupersededEventFilter(org.osid.id.Id eventId) {
            nullarg(eventId, "event Id");
            this.eventId = eventId;
            return;
        }

         
        /**
         *  Used by the EventFilterList to filter the 
         *  superseding event list based on superseded event.
         *
         *  @param event the event
         *  @return <code>true</code> to pass the event,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.calendaring.SupersedingEvent event) {
            return (event.getSupersededEventId().equals(this.eventId));
        }
    }
}
