//
// AbstractQueryOrderLookupSession.java
//
//    An inline adapter that maps an OrderLookupSession to
//    an OrderQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OrderLookupSession to
 *  an OrderQuerySession.
 */

public abstract class AbstractQueryOrderLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractOrderLookupSession
    implements org.osid.ordering.OrderLookupSession {

    private final org.osid.ordering.OrderQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOrderLookupSession.
     *
     *  @param querySession the underlying order query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOrderLookupSession(org.osid.ordering.OrderQuerySession querySession) {
        nullarg(querySession, "order query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Store</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform <code>Order</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOrders() {
        return (this.session.canSearchOrders());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include orders in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
     
    /**
     *  Gets the <code>Order</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Order</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Order</code> and
     *  retained for compatibility.
     *
     *  @param  orderId <code>Id</code> of the
     *          <code>Order</code>
     *  @return the order
     *  @throws org.osid.NotFoundException <code>orderId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>orderId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder(org.osid.id.Id orderId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchId(orderId, true);
        org.osid.ordering.OrderList orders = this.session.getOrdersByQuery(query);
        if (orders.hasNext()) {
            return (orders.getNextOrder());
        } 
        
        throw new org.osid.NotFoundException(orderId + " not found");
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  orders specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Orders</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  orderIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>orderIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByIds(org.osid.id.IdList orderIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();

        try (org.osid.id.IdList ids = orderIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> which does not include
     *  orders of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchGenusType(orderGenusType, true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets an <code>OrderList</code> corresponding to the given
     *  order genus <code>Type</code> and include any additional
     *  orders with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderGenusType an order genus type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByParentGenusType(org.osid.type.Type orderGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchParentGenusType(orderGenusType, true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets an <code>OrderList</code> containing the given
     *  order record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  orderRecordType an order record type 
     *  @return the returned <code>Order</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>orderRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByRecordType(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchRecordType(orderRecordType, true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets a list of all orders corresponding to a customer <code>
     *  Id.  </code> In plenary mode, the returned list contains all
     *  known orders or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session.
     *
     *  @param  resourceId the <code> Id </code> of the customer 
     *  @return the returned <code> OrderList </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchCustomerId(resourceId, true);
        return (this.session.getOrdersByQuery(query));
    }

    
    /**
     *  Gets a list of all orders corresponding to a date
     *  range. Entries are returned with a submitted date that falsl
     *  between the requested dates inclusive. In plenary mode, the
     *  returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> OrderList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchSubmitDate(from, to, true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets a list of all orders corresponding to a customer <code>
     *  Id </code> and date range. Entries are returned with submit
     *  dates that fall between the requested dates inclusive. In
     *  plenary mode, the returned list contains all known orders or
     *  an error results.  Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  @param  resourceId the <code> Id </code> of the customer 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> OrderList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersByCustomerAndDate(org.osid.id.Id resourceId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchCustomerId(resourceId, true);
        query.matchSubmitDate(from, to, true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets a list of all orders with an item for a product. Entries
     *  are returned with submit dates that fall between the requested
     *  dates inclusive. In plenary mode, the returned list contains
     *  all known orders or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  productId a product <code> Id </code> 
     *  @return the returned <code> OrderList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is
     *          less than <code> from </code>
     *  @throws org.osid.NullArgumentException <code> productId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        if (query.supportsItemQuery()) {
            query.getItemQuery().matchProductId(productId, true);
            return (this.session.getOrdersByQuery(query));
        }

        return (super.getOrdersForProduct(productId));
    }

    
    /**
     *  Gets a list of all orders with items for a product <code> Id
     *  </code> and date range. Entries are returned with submit dates
     *  that fall between the requested dates inclusive. In plenary
     *  mode, the returned list contains all known orders or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> OrderList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> productId, from
     *          </code>, or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrdersForProductAndDate(org.osid.id.Id productId, 
                                                                  org.osid.calendaring.DateTime from, 
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        if (query.supportsItemQuery()) {
            query.getItemQuery().matchProductId(productId, true);
            query.matchSubmitDate(from, to, true);
            return (this.session.getOrdersByQuery(query));
        }

        return (super.getOrdersForProductAndDate(productId, from, to));
    }


    /**
     *  Gets all <code>Orders</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  orders or an error results. Otherwise, the returned list
     *  may contain only those orders that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Orders</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrders()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ordering.OrderQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOrdersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ordering.OrderQuery getQuery() {
        org.osid.ordering.OrderQuery query = this.session.getOrderQuery();
        return (query);
    }
}
