//
// MutableMapSiteLookupSession
//
//    Implements a Site lookup service backed by a collection of
//    sites that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Site lookup service backed by a collection of
 *  sites. The sites are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of sites can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSiteLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapSiteLookupSession
    implements org.osid.installation.SiteLookupSession {


    /**
     *  Constructs a new {@code MutableMapSiteLookupSession}
     *  with no sites.
     */

    public MutableMapSiteLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSiteLookupSession} with a
     *  single site.
     *  
     *  @param site a site
     *  @throws org.osid.NullArgumentException {@code site}
     *          is {@code null}
     */

    public MutableMapSiteLookupSession(org.osid.installation.Site site) {
        putSite(site);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSiteLookupSession}
     *  using an array of sites.
     *
     *  @param sites an array of sites
     *  @throws org.osid.NullArgumentException {@code sites}
     *          is {@code null}
     */

    public MutableMapSiteLookupSession(org.osid.installation.Site[] sites) {
        putSites(sites);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSiteLookupSession}
     *  using a collection of sites.
     *
     *  @param sites a collection of sites
     *  @throws org.osid.NullArgumentException {@code sites}
     *          is {@code null}
     */

    public MutableMapSiteLookupSession(java.util.Collection<? extends org.osid.installation.Site> sites) {
        putSites(sites);
        return;
    }

    
    /**
     *  Makes a {@code Site} available in this session.
     *
     *  @param site a site
     *  @throws org.osid.NullArgumentException {@code site{@code  is
     *          {@code null}
     */

    @Override
    public void putSite(org.osid.installation.Site site) {
        super.putSite(site);
        return;
    }


    /**
     *  Makes an array of sites available in this session.
     *
     *  @param sites an array of sites
     *  @throws org.osid.NullArgumentException {@code sites{@code 
     *          is {@code null}
     */

    @Override
    public void putSites(org.osid.installation.Site[] sites) {
        super.putSites(sites);
        return;
    }


    /**
     *  Makes collection of sites available in this session.
     *
     *  @param sites a collection of sites
     *  @throws org.osid.NullArgumentException {@code sites{@code  is
     *          {@code null}
     */

    @Override
    public void putSites(java.util.Collection<? extends org.osid.installation.Site> sites) {
        super.putSites(sites);
        return;
    }


    /**
     *  Removes a Site from this session.
     *
     *  @param siteId the {@code Id} of the site
     *  @throws org.osid.NullArgumentException {@code siteId{@code 
     *          is {@code null}
     */

    @Override
    public void removeSite(org.osid.id.Id siteId) {
        super.removeSite(siteId);
        return;
    }    
}
