//
// AbstractMapDeviceLookupSession
//
//    A simple framework for providing a Device lookup service
//    backed by a fixed collection of devices.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Device lookup service backed by a
 *  fixed collection of devices. The devices are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Devices</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDeviceLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractDeviceLookupSession
    implements org.osid.control.DeviceLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Device> devices = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Device>());


    /**
     *  Makes a <code>Device</code> available in this session.
     *
     *  @param  device a device
     *  @throws org.osid.NullArgumentException <code>device<code>
     *          is <code>null</code>
     */

    protected void putDevice(org.osid.control.Device device) {
        this.devices.put(device.getId(), device);
        return;
    }


    /**
     *  Makes an array of devices available in this session.
     *
     *  @param  devices an array of devices
     *  @throws org.osid.NullArgumentException <code>devices<code>
     *          is <code>null</code>
     */

    protected void putDevices(org.osid.control.Device[] devices) {
        putDevices(java.util.Arrays.asList(devices));
        return;
    }


    /**
     *  Makes a collection of devices available in this session.
     *
     *  @param  devices a collection of devices
     *  @throws org.osid.NullArgumentException <code>devices<code>
     *          is <code>null</code>
     */

    protected void putDevices(java.util.Collection<? extends org.osid.control.Device> devices) {
        for (org.osid.control.Device device : devices) {
            this.devices.put(device.getId(), device);
        }

        return;
    }


    /**
     *  Removes a Device from this session.
     *
     *  @param  deviceId the <code>Id</code> of the device
     *  @throws org.osid.NullArgumentException <code>deviceId<code> is
     *          <code>null</code>
     */

    protected void removeDevice(org.osid.id.Id deviceId) {
        this.devices.remove(deviceId);
        return;
    }


    /**
     *  Gets the <code>Device</code> specified by its <code>Id</code>.
     *
     *  @param  deviceId <code>Id</code> of the <code>Device</code>
     *  @return the device
     *  @throws org.osid.NotFoundException <code>deviceId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>deviceId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Device getDevice(org.osid.id.Id deviceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Device device = this.devices.get(deviceId);
        if (device == null) {
            throw new org.osid.NotFoundException("device not found: " + deviceId);
        }

        return (device);
    }


    /**
     *  Gets all <code>Devices</code>. In plenary mode, the returned
     *  list contains all known devices or an error
     *  results. Otherwise, the returned list may contain only those
     *  devices that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Devices</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.DeviceList getDevices()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.device.ArrayDeviceList(this.devices.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.devices.clear();
        super.close();
        return;
    }
}
