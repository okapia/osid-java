//
// AbstractMapJobLookupSession
//
//    A simple framework for providing a Job lookup service
//    backed by a fixed collection of jobs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Job lookup service backed by a
 *  fixed collection of jobs. The jobs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Jobs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapJobLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractJobLookupSession
    implements org.osid.resourcing.JobLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.Job> jobs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.Job>());


    /**
     *  Makes a <code>Job</code> available in this session.
     *
     *  @param  job a job
     *  @throws org.osid.NullArgumentException <code>job<code>
     *          is <code>null</code>
     */

    protected void putJob(org.osid.resourcing.Job job) {
        this.jobs.put(job.getId(), job);
        return;
    }


    /**
     *  Makes an array of jobs available in this session.
     *
     *  @param  jobs an array of jobs
     *  @throws org.osid.NullArgumentException <code>jobs<code>
     *          is <code>null</code>
     */

    protected void putJobs(org.osid.resourcing.Job[] jobs) {
        putJobs(java.util.Arrays.asList(jobs));
        return;
    }


    /**
     *  Makes a collection of jobs available in this session.
     *
     *  @param  jobs a collection of jobs
     *  @throws org.osid.NullArgumentException <code>jobs<code>
     *          is <code>null</code>
     */

    protected void putJobs(java.util.Collection<? extends org.osid.resourcing.Job> jobs) {
        for (org.osid.resourcing.Job job : jobs) {
            this.jobs.put(job.getId(), job);
        }

        return;
    }


    /**
     *  Removes a Job from this session.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @throws org.osid.NullArgumentException <code>jobId<code> is
     *          <code>null</code>
     */

    protected void removeJob(org.osid.id.Id jobId) {
        this.jobs.remove(jobId);
        return;
    }


    /**
     *  Gets the <code>Job</code> specified by its <code>Id</code>.
     *
     *  @param  jobId <code>Id</code> of the <code>Job</code>
     *  @return the job
     *  @throws org.osid.NotFoundException <code>jobId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>jobId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob(org.osid.id.Id jobId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.Job job = this.jobs.get(jobId);
        if (job == null) {
            throw new org.osid.NotFoundException("job not found: " + jobId);
        }

        return (job);
    }


    /**
     *  Gets all <code>Jobs</code>. In plenary mode, the returned
     *  list contains all known jobs or an error
     *  results. Otherwise, the returned list may contain only those
     *  jobs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Jobs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.job.ArrayJobList(this.jobs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobs.clear();
        super.close();
        return;
    }
}
