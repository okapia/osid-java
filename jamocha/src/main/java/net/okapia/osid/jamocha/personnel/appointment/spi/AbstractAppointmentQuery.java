//
// AbstractAppointmentQuery.java
//
//     A template for making an Appointment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for appointments.
 */

public abstract class AbstractAppointmentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.personnel.AppointmentQuery {

    private final java.util.Collection<org.osid.personnel.records.AppointmentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a person <code> Id. </code> 
     *
     *  @param  personId a person <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> personId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPersonId(org.osid.id.Id personId, boolean match) {
        return;
    }


    /**
     *  Clears all person <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPersonIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PersonQuery </code> is available. 
     *
     *  @return <code> true </code> if a person query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a person query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the person query 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuery getPersonQuery() {
        throw new org.osid.UnimplementedException("supportsPersonQuery() is false");
    }


    /**
     *  Clears all person terms. 
     */

    @OSID @Override
    public void clearPersonTerms() {
        return;
    }


    /**
     *  Sets a position <code> Id. </code> 
     *
     *  @param  positionId a position <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> positionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPositionId(org.osid.id.Id positionId, boolean match) {
        return;
    }


    /**
     *  Clears all position <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPositionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a position query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the position query 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuery getPositionQuery() {
        throw new org.osid.UnimplementedException("supportsPositionQuery() is false");
    }


    /**
     *  Clears all position terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        return;
    }


    /**
     *  Matches a commitment between the given range inclusive. 
     *
     *  @param  from a starting range 
     *  @param  to an ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchCommitment(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches positions with any low salary. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          commitment, <code> false </code> to match appointments with no 
     *          commitment 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        return;
    }


    /**
     *  Clears all commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        return;
    }


    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches persons with any title. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          title, <code> false </code> to match appointments with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Matches a salary between the given range inclusive. 
     *
     *  @param  from a starting salary range 
     *  @param  to an ending salary range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSalary(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        return;
    }


    /**
     *  Matches positions with any salary. 
     *
     *  @param  match <code> true </code> to match appointments with any 
     *          salary, <code> false </code> to match appointments with no 
     *          salary 
     */

    @OSID @Override
    public void matchAnySalary(boolean match) {
        return;
    }


    /**
     *  Clears all salary terms. 
     */

    @OSID @Override
    public void clearSalaryTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match appointments 
     *  assigned to realms. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> supportsRealmQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getRealmQuery() {
        throw new org.osid.UnimplementedException("supportsRealmQuery() is false");
    }


    /**
     *  Clears all realm terms. 
     */

    @OSID @Override
    public void clearRealmTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given appointment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an appointment implementing the requested record.
     *
     *  @param appointmentRecordType an appointment record type
     *  @return the appointment query record
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentQueryRecord getAppointmentQueryRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentQueryRecord record : this.records) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this appointment query. 
     *
     *  @param appointmentQueryRecord appointment query record
     *  @param appointmentRecordType appointment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAppointmentQueryRecord(org.osid.personnel.records.AppointmentQueryRecord appointmentQueryRecord, 
                                          org.osid.type.Type appointmentRecordType) {

        addRecordType(appointmentRecordType);
        nullarg(appointmentQueryRecord, "appointment query record");
        this.records.add(appointmentQueryRecord);        
        return;
    }
}
