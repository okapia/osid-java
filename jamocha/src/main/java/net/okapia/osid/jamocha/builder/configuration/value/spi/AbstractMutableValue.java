//
// AbstractMutableValue.java
//
//     Defines a mutable Value.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Value</code>.
 */

public abstract class AbstractMutableValue
    extends net.okapia.osid.jamocha.configuration.value.spi.AbstractValue
    implements org.osid.configuration.Value,
               net.okapia.osid.jamocha.builder.configuration.value.ValueMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this value. 
     *
     *  @param record value record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addValueRecord(org.osid.configuration.records.ValueRecord record, org.osid.type.Type recordType) {
        super.addValueRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this value. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this value. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *          disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this value.
     *
     *  @param displayName the name for this value
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this value.
     *
     *  @param description the description of this value
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the parameter.
     *
     *  @param parameter a parameter
     *  @throws org.osid.NullArgumentException
     *          <code>parameter</code> is <code>null</code>
     */

    @Override
    public void setParameter(org.osid.configuration.Parameter parameter) {
        super.setParameter(parameter);
        return;
    }


    /**
     *  Sets the priority.
     *
     *  @param priority a priority
     *  @throws org.osid.InvalidArgumentException
     *          <code>priority</code> is negative
     */

    @Override
    public void setPriority(long priority) {
        super.setPriority(priority);
        return;
    }


    /**
     *  Sets the boolean value.
     *
     *  @param value a boolean value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setBooleanValue(boolean value) {
        super.setBooleanValue(value);
        return;
    }


    /**
     *  Sets the bytes value.
     *
     *  @param value a bytes value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setBytesValue(byte[] value) {
        super.setBytesValue(value);
        return;
    }


    /**
     *  Sets the cardinal value.
     *
     *  @param value a cardinal value
     *  @throws org.osid.InvalidArgumentException <code>value</code>
     *          is negative
     */

    @Override
    public void setCardinalValue(long value) {
        super.setCardinalValue(value);
        return;
    }


    /**
     *  Sets the coordinate value.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setCoordinateValue(org.osid.mapping.Coordinate value) {
        super.setCoordinateValue(value);
        return;
    }


    /**
     *  Sets the currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setCurrencyValue(org.osid.financials.Currency value) {
        super.setCurrencyValue(value);
        return;
    }


    /**
     *  Sets the date time value.
     *
     *  @param value a date time value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setDateTimeValue(org.osid.calendaring.DateTime value) {
        super.setDateTimeValue(value);
        return;
    }


    /**
     *  Sets the decimal value.
     *
     *  @param value a decimal value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setDecimalValue(java.math.BigDecimal value) {
        super.setDecimalValue(value);
        return;
    }


    /**
     *  Sets the distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setDistanceValue(org.osid.mapping.Distance value) {
        super.setDistanceValue(value);
        return;
    }


    /**
     *  Sets the duration value.
     *
     *  @param value a duration value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setDurationValue(org.osid.calendaring.Duration value) {
        super.setDurationValue(value);
        return;
    }


    /**
     *  Sets the heading value.
     *
     *  @param value a heading value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setHeadingValue(org.osid.mapping.Heading value) {
        super.setHeadingValue(value);
        return;
    }


    /**
     *  Sets the id value.
     *
     *  @param value an id value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setIdValue(org.osid.id.Id value) {
        super.setIdValue(value);
        return;
    }


    /**
     *  Sets the integer value.
     *
     *  @param value an integer value
     */

    @Override
    public void setIntegerValue(long value) {
        super.setIntegerValue(value);
        return;
    }


    /**
     *  Sets the object value.
     *
     *  @param value an object value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setObjectValue(java.lang.Object value) {
        super.setObjectValue(value);
        return;
    }


    /**
     *  Sets the spatial unit value.
     *
     *  @param value a spatial unit value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setSpatialUnitValue(org.osid.mapping.SpatialUnit value) {
        super.setSpatialUnitValue(value);
        return;
    }


    /**
     *  Sets the speed value.
     *
     *  @param value a speed value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setSpeedValue(org.osid.mapping.Speed value) {
        super.setSpeedValue(value);
        return;
    }


    /**
     *  Sets the string value.
     *
     *  @param value a string value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setStringValue(String value) {
        super.setStringValue(value);
        return;
    }


    /**
     *  Sets the time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setTimeValue(org.osid.calendaring.Time value) {
        super.setTimeValue(value);
        return;
    }


    /**
     *  Sets the type value.
     *
     *  @param value a type value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setTypeValue(org.osid.type.Type value) {
        super.setTypeValue(value);
        return;
    }


    /**
     *  Sets the version value.
     *
     *  @param value a version value
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    @Override
    public void setVersionValue(org.osid.installation.Version value) {
        super.setVersionValue(value);
        return;
    }
}

