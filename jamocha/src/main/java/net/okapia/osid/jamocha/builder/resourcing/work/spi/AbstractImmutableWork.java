//
// AbstractImmutableWork.java
//
//     Wraps a mutable Work to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Work</code> to hide modifiers. This
 *  wrapper provides an immutized Work from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying work whose state changes are visible.
 */

public abstract class AbstractImmutableWork
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.resourcing.Work {

    private final org.osid.resourcing.Work work;


    /**
     *  Constructs a new <code>AbstractImmutableWork</code>.
     *
     *  @param work the work to immutablize
     *  @throws org.osid.NullArgumentException <code>work</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableWork(org.osid.resourcing.Work work) {
        super(work);
        this.work = work;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the job of which this work is a part. 
     *
     *  @return the job <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getJobId() {
        return (this.work.getJobId());
    }


    /**
     *  Gets the job of which this work is a part. 
     *
     *  @return the job 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob()
        throws org.osid.OperationFailedException {

        return (this.work.getJob());
    }


    /**
     *  Tests if specific competencies are needed for this work. 
     *
     *  @return <code> true </code> if competency if specified, <code> false 
     *          </code> if incompetence will do 
     */

    @OSID @Override
    public boolean needsCompetence() {
        return (this.work.needsCompetence());
    }


    /**
     *  Gets the <code> Ids </code> of the competencies. 
     *
     *  @return the competency <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> neededCompetence() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCompetencyIds() {
        return (this.work.getCompetencyIds());
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> neededCompetence() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException {

        return (this.work.getCompetencies());
    }


    /**
     *  Gets the date this work was created. 
     *
     *  @return the created date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedDate() {
        return (this.work.getCreatedDate());
    }


    /**
     *  Tests if this work has been completed. 
     *
     *  @return <code> true </code> if this work is complete, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.work.isComplete());
    }


    /**
     *  Gets the completion date. 
     *
     *  @return the completion date 
     *  @throws org.osid.IllegalStateException <code> isComplete() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCompletionDate() {
        return (this.work.getCompletionDate());
    }


    /**
     *  Gets the work record corresponding to the given <code> Work </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record.. The <code> workRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(workRecordType) </code> is <code> true </code> . 
     *
     *  @param  workRecordType the type of work record to retrieve 
     *  @return the work record 
     *  @throws org.osid.NullArgumentException <code> workRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(workRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkRecord getWorkRecord(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException {

        return (this.work.getWorkRecord(workRecordType));
    }
}

