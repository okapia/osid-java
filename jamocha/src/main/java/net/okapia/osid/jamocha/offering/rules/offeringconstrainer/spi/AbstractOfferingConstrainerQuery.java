//
// AbstractOfferingConstrainerQuery.java
//
//     A template for making an OfferingConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for offering constrainers.
 */

public abstract class AbstractOfferingConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.offering.rules.OfferingConstrainerQuery {

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches mapped to the canonical unit. 
     *
     *  @param  canonicalUnitId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CanonicalUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getRuledCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitQuery() is false");
    }


    /**
     *  Matches rules mapped to any canonical unit. 
     *
     *  @param  match <code> true </code> for mapped to any canonical unit, 
     *          <code> false </code> to match mapped to no canonicalUnit 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnit(boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitTerms() {
        return;
    }


    /**
     *  Matches description overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideDescription(boolean match) {
        return;
    }


    /**
     *  Matches any description override. 
     *
     *  @param  match <code> true </code> for any description override, <code> 
     *          false </code> to match no description override 
     */

    @OSID @Override
    public void matchAnyOverrideDescription(boolean match) {
        return;
    }


    /**
     *  Clears the description override query terms. 
     */

    @OSID @Override
    public void clearOverrideDescriptionTerms() {
        return;
    }


    /**
     *  Matches title overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideTitle(boolean match) {
        return;
    }


    /**
     *  Matches any title override. 
     *
     *  @param  match <code> true </code> for any title override, <code> false 
     *          </code> to match no title override 
     */

    @OSID @Override
    public void matchAnyOverrideTitle(boolean match) {
        return;
    }


    /**
     *  Clears the title override query terms. 
     */

    @OSID @Override
    public void clearOverrideTitleTerms() {
        return;
    }


    /**
     *  Matches code overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideCode(boolean match) {
        return;
    }


    /**
     *  Matches any code override. 
     *
     *  @param  match <code> true </code> for any code override, <code> false 
     *          </code> to match no code override 
     */

    @OSID @Override
    public void matchAnyOverrideCode(boolean match) {
        return;
    }


    /**
     *  Clears the code override query terms. 
     */

    @OSID @Override
    public void clearOverrideCodeTerms() {
        return;
    }


    /**
     *  Matches time periods overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideTimePeriods(boolean match) {
        return;
    }


    /**
     *  Matches any time periods override. 
     *
     *  @param  match <code> true </code> for any time periods override, 
     *          <code> false </code> to match no time periods override 
     */

    @OSID @Override
    public void matchAnyOverrideTimePeriods(boolean match) {
        return;
    }


    /**
     *  Clears the time periods override query terms. 
     */

    @OSID @Override
    public void clearOverrideTimePeriodsTerms() {
        return;
    }


    /**
     *  Matches constrain time periods. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainTimePeriods(boolean match) {
        return;
    }


    /**
     *  Matches any constrain time periods. 
     *
     *  @param  match <code> true </code> for any time periods constraints, 
     *          <code> false </code> to match no time periods constraints 
     */

    @OSID @Override
    public void matchAnyConstrainTimePeriods(boolean match) {
        return;
    }


    /**
     *  Clears the constrain time periods query terms. 
     */

    @OSID @Override
    public void clearConstrainTimePeriodsTerms() {
        return;
    }


    /**
     *  Matches result options overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideResultOptions(boolean match) {
        return;
    }


    /**
     *  Matches any result options override. 
     *
     *  @param  match <code> true </code> for any result options override, 
     *          <code> false </code> to match no result options override 
     */

    @OSID @Override
    public void matchAnyOverrideResultOptions(boolean match) {
        return;
    }


    /**
     *  Clears the result options override query terms. 
     */

    @OSID @Override
    public void clearOverrideResultOptionsTerms() {
        return;
    }


    /**
     *  Matches constrain result options. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainResultOptions(boolean match) {
        return;
    }


    /**
     *  Matches any constrain result options. 
     *
     *  @param  match <code> true </code> for any result options constraints, 
     *          <code> false </code> to match no result options constraints 
     */

    @OSID @Override
    public void matchAnyConstrainResultOptions(boolean match) {
        return;
    }


    /**
     *  Clears the constrain result options query terms. 
     */

    @OSID @Override
    public void clearConstrainResultOptionsTerms() {
        return;
    }


    /**
     *  Matches result options overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideSponsors(boolean match) {
        return;
    }


    /**
     *  Matches any sponsors override. 
     *
     *  @param  match <code> true </code> for any sponsors override, <code> 
     *          false </code> to match no sponsors override 
     */

    @OSID @Override
    public void matchAnyOverrideSponsors(boolean match) {
        return;
    }


    /**
     *  Clears the sponsors override query terms. 
     */

    @OSID @Override
    public void clearOverrideSponsorsTerms() {
        return;
    }


    /**
     *  Matches constrain sponsors. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainSponsors(boolean match) {
        return;
    }


    /**
     *  Matches any constrain sponsors. 
     *
     *  @param  match <code> true </code> for any sponsors constraints, <code> 
     *          false </code> to match no sponsors constraints 
     */

    @OSID @Override
    public void matchAnyConstrainSponsors(boolean match) {
        return;
    }


    /**
     *  Clears the constrain sponsors query terms. 
     */

    @OSID @Override
    public void clearConstrainSponsorsTerms() {
        return;
    }


    /**
     *  Matches mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given offering constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offering constrainer implementing the requested record.
     *
     *  @param offeringConstrainerRecordType an offering constrainer record type
     *  @return the offering constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerQueryRecord getOfferingConstrainerQueryRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering constrainer query. 
     *
     *  @param offeringConstrainerQueryRecord offering constrainer query record
     *  @param offeringConstrainerRecordType offeringConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingConstrainerQueryRecord(org.osid.offering.rules.records.OfferingConstrainerQueryRecord offeringConstrainerQueryRecord, 
                                          org.osid.type.Type offeringConstrainerRecordType) {

        addRecordType(offeringConstrainerRecordType);
        nullarg(offeringConstrainerQueryRecord, "offering constrainer query record");
        this.records.add(offeringConstrainerQueryRecord);        
        return;
    }
}
