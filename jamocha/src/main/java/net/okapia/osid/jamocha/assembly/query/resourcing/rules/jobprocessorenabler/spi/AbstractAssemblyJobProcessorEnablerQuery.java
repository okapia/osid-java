//
// AbstractAssemblyJobProcessorEnablerQuery.java
//
//     A JobProcessorEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JobProcessorEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyJobProcessorEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.resourcing.rules.JobProcessorEnablerQuery,
               org.osid.resourcing.rules.JobProcessorEnablerQueryInspector,
               org.osid.resourcing.rules.JobProcessorEnablerSearchOrder {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobProcessorEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJobProcessorEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJobProcessorEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the job processor. 
     *
     *  @param  jobProcessorId the job processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobProcessorId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobProcessorId(org.osid.id.Id jobProcessorId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledJobProcessorIdColumn(), jobProcessorId, match);
        return;
    }


    /**
     *  Clears the job processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobProcessorIdTerms() {
        getAssembler().clearTerms(getRuledJobProcessorIdColumn());
        return;
    }


    /**
     *  Gets the job processor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledJobProcessorIdTerms() {
        return (getAssembler().getIdTerms(getRuledJobProcessorIdColumn()));
    }


    /**
     *  Gets the RuledJobProcessorId column name.
     *
     * @return the column name
     */

    protected String getRuledJobProcessorIdColumn() {
        return ("ruled_job_processor_id");
    }


    /**
     *  Tests if an <code> JobProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a job processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the job processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledJobProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQuery getRuledJobProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any job processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any job 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          job processors 
     */

    @OSID @Override
    public void matchAnyRuledJobProcessor(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledJobProcessorColumn(), match);
        return;
    }


    /**
     *  Clears the job processor query terms. 
     */

    @OSID @Override
    public void clearRuledJobProcessorTerms() {
        getAssembler().clearTerms(getRuledJobProcessorColumn());
        return;
    }


    /**
     *  Gets the job processor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorQueryInspector[] getRuledJobProcessorTerms() {
        return (new org.osid.resourcing.rules.JobProcessorQueryInspector[0]);
    }


    /**
     *  Gets the RuledJobProcessor column name.
     *
     * @return the column name
     */

    protected String getRuledJobProcessorColumn() {
        return ("ruled_job_processor");
    }


    /**
     *  Matches enablers mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this jobProcessorEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  jobProcessorEnablerRecordType a job processor enabler record type 
     *  @return <code>true</code> if the jobProcessorEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type jobProcessorEnablerRecordType) {
        for (org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  jobProcessorEnablerRecordType the job processor enabler record type 
     *  @return the job processor enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord getJobProcessorEnablerQueryRecord(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  jobProcessorEnablerRecordType the job processor enabler record type 
     *  @return the job processor enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord getJobProcessorEnablerQueryInspectorRecord(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param jobProcessorEnablerRecordType the job processor enabler record type
     *  @return the job processor enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobProcessorEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobProcessorEnablerSearchOrderRecord getJobProcessorEnablerSearchOrderRecord(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobProcessorEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(jobProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this job processor enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param jobProcessorEnablerQueryRecord the job processor enabler query record
     *  @param jobProcessorEnablerQueryInspectorRecord the job processor enabler query inspector
     *         record
     *  @param jobProcessorEnablerSearchOrderRecord the job processor enabler search order record
     *  @param jobProcessorEnablerRecordType job processor enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerQueryRecord</code>,
     *          <code>jobProcessorEnablerQueryInspectorRecord</code>,
     *          <code>jobProcessorEnablerSearchOrderRecord</code> or
     *          <code>jobProcessorEnablerRecordTypejobProcessorEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addJobProcessorEnablerRecords(org.osid.resourcing.rules.records.JobProcessorEnablerQueryRecord jobProcessorEnablerQueryRecord, 
                                      org.osid.resourcing.rules.records.JobProcessorEnablerQueryInspectorRecord jobProcessorEnablerQueryInspectorRecord, 
                                      org.osid.resourcing.rules.records.JobProcessorEnablerSearchOrderRecord jobProcessorEnablerSearchOrderRecord, 
                                      org.osid.type.Type jobProcessorEnablerRecordType) {

        addRecordType(jobProcessorEnablerRecordType);

        nullarg(jobProcessorEnablerQueryRecord, "job processor enabler query record");
        nullarg(jobProcessorEnablerQueryInspectorRecord, "job processor enabler query inspector record");
        nullarg(jobProcessorEnablerSearchOrderRecord, "job processor enabler search odrer record");

        this.queryRecords.add(jobProcessorEnablerQueryRecord);
        this.queryInspectorRecords.add(jobProcessorEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(jobProcessorEnablerSearchOrderRecord);
        
        return;
    }
}
