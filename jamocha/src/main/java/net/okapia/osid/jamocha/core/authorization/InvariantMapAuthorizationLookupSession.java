//
// InvariantMapAuthorizationLookupSession
//
//    Implements an Authorization lookup service backed by a fixed collection of
//    authorizations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements an Authorization lookup service backed by a fixed
 *  collection of authorizations. The authorizations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuthorizationLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationLookupSession</code> with no
     *  authorizations.
     *  
     *  @param vault the vault
     *  @throws org.osid.NullArgumnetException {@code vault} is
     *          {@code null}
     */

    public InvariantMapAuthorizationLookupSession(org.osid.authorization.Vault vault) {
        setVault(vault);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationLookupSession</code> with a single
     *  authorization.
     *  
     *  @param vault the vault
     *  @param authorization an single authorization
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorization} is <code>null</code>
     */

      public InvariantMapAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Authorization authorization) {
        this(vault);
        putAuthorization(authorization);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationLookupSession</code> using an array
     *  of authorizations.
     *  
     *  @param vault the vault
     *  @param authorizations an array of authorizations
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorizations} is <code>null</code>
     */

      public InvariantMapAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.Authorization[] authorizations) {
        this(vault);
        putAuthorizations(authorizations);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationLookupSession</code> using a
     *  collection of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations a collection of authorizations
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorizations} is <code>null</code>
     */

      public InvariantMapAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                               java.util.Collection<? extends org.osid.authorization.Authorization> authorizations) {
        this(vault);
        putAuthorizations(authorizations);
        return;
    }
}
