//
// MutableMapLogLookupSession
//
//    Implements a Log lookup service backed by a collection of
//    logs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging;


/**
 *  Implements a Log lookup service backed by a collection of
 *  logs. The logs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of logs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapLogLookupSession
    extends net.okapia.osid.jamocha.core.logging.spi.AbstractMapLogLookupSession
    implements org.osid.logging.LogLookupSession {


    /**
     *  Constructs a new {@code MutableMapLogLookupSession}
     *  with no logs.
     */

    public MutableMapLogLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLogLookupSession} with a
     *  single log.
     *  
     *  @param log a log
     *  @throws org.osid.NullArgumentException {@code log}
     *          is {@code null}
     */

    public MutableMapLogLookupSession(org.osid.logging.Log log) {
        putLog(log);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLogLookupSession}
     *  using an array of logs.
     *
     *  @param logs an array of logs
     *  @throws org.osid.NullArgumentException {@code logs}
     *          is {@code null}
     */

    public MutableMapLogLookupSession(org.osid.logging.Log[] logs) {
        putLogs(logs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapLogLookupSession}
     *  using a collection of logs.
     *
     *  @param logs a collection of logs
     *  @throws org.osid.NullArgumentException {@code logs}
     *          is {@code null}
     */

    public MutableMapLogLookupSession(java.util.Collection<? extends org.osid.logging.Log> logs) {
        putLogs(logs);
        return;
    }

    
    /**
     *  Makes a {@code Log} available in this session.
     *
     *  @param log a log
     *  @throws org.osid.NullArgumentException {@code log{@code  is
     *          {@code null}
     */

    @Override
    public void putLog(org.osid.logging.Log log) {
        super.putLog(log);
        return;
    }


    /**
     *  Makes an array of logs available in this session.
     *
     *  @param logs an array of logs
     *  @throws org.osid.NullArgumentException {@code logs{@code 
     *          is {@code null}
     */

    @Override
    public void putLogs(org.osid.logging.Log[] logs) {
        super.putLogs(logs);
        return;
    }


    /**
     *  Makes collection of logs available in this session.
     *
     *  @param logs a collection of logs
     *  @throws org.osid.NullArgumentException {@code logs{@code  is
     *          {@code null}
     */

    @Override
    public void putLogs(java.util.Collection<? extends org.osid.logging.Log> logs) {
        super.putLogs(logs);
        return;
    }


    /**
     *  Removes a Log from this session.
     *
     *  @param logId the {@code Id} of the log
     *  @throws org.osid.NullArgumentException {@code logId{@code 
     *          is {@code null}
     */

    @Override
    public void removeLog(org.osid.id.Id logId) {
        super.removeLog(logId);
        return;
    }    
}
