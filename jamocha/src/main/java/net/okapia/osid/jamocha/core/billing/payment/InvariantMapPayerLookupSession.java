//
// InvariantMapPayerLookupSession
//
//    Implements a Payer lookup service backed by a fixed collection of
//    payers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment;


/**
 *  Implements a Payer lookup service backed by a fixed
 *  collection of payers. The payers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPayerLookupSession
    extends net.okapia.osid.jamocha.core.billing.payment.spi.AbstractMapPayerLookupSession
    implements org.osid.billing.payment.PayerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPayerLookupSession</code> with no
     *  payers.
     *  
     *  @param business the business
     *  @throws org.osid.NullArgumnetException {@code business} is
     *          {@code null}
     */

    public InvariantMapPayerLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPayerLookupSession</code> with a single
     *  payer.
     *  
     *  @param business the business
     *  @param payer a single payer
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payer} is <code>null</code>
     */

      public InvariantMapPayerLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.payment.Payer payer) {
        this(business);
        putPayer(payer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPayerLookupSession</code> using an array
     *  of payers.
     *  
     *  @param business the business
     *  @param payers an array of payers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payers} is <code>null</code>
     */

      public InvariantMapPayerLookupSession(org.osid.billing.Business business,
                                               org.osid.billing.payment.Payer[] payers) {
        this(business);
        putPayers(payers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPayerLookupSession</code> using a
     *  collection of payers.
     *
     *  @param business the business
     *  @param payers a collection of payers
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payers} is <code>null</code>
     */

      public InvariantMapPayerLookupSession(org.osid.billing.Business business,
                                               java.util.Collection<? extends org.osid.billing.payment.Payer> payers) {
        this(business);
        putPayers(payers);
        return;
    }
}
