//
// AbstractPost.java
//
//     Defines a Post.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.post.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Post</code>.
 */

public abstract class AbstractPost
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.financials.posting.Post {

    private boolean posted = false;
    private org.osid.financials.FiscalPeriod fiscalPeriod;
    private org.osid.calendaring.DateTime date;
    private final java.util.Collection<org.osid.financials.posting.PostEntry> postEntries = new java.util.LinkedHashSet<>();
    private org.osid.financials.posting.Post correctedPost;

    private final java.util.Collection<org.osid.financials.posting.records.PostRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> FiscalPeriod. </code> 
     *
     *  @return the <code> FiscalPeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.fiscalPeriod.getId());
    }


    /**
     *  Gets the <code> FiscalPeriod. </code> 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {

        return (this.fiscalPeriod);
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setFiscalPeriod(org.osid.financials.FiscalPeriod period) {
        nullarg(period, "fiscal period");
        this.fiscalPeriod = period;
        return;
    }


    /**
     *  Tests if this has been posted. 
     *
     *  @return <code> true </code> if this has been posted, <code> false 
     *          </code> if just lying around 
     */

    @OSID @Override
    public boolean isPosted() {
        return (this.posted);
    }


    /**
     *  Sets the posted.
     *
     *  @param posted <code> true </code> if this has been posted,
     *          <code> false </code> if just lying around
     */

    protected void setPosted(boolean posted) {
        this.posted = posted;
        return;
    }


    /**
     *  Gets the posting date. 
     *
     *  @return the posting date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "posting date");
        this.date = date;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the <code> PostEntries. </code> 
     *
     *  @return the <code> PostEntry </code> <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPostEntryIds() {
        try {
            org.osid.financials.posting.PostEntryList postEntries = getPostEntries();
            return (new net.okapia.osid.jamocha.adapter.converter.financials.posting.postentry.PostEntryToIdList(postEntries));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> PostEntries. </code> 
     *
     *  @return the post entries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.financials.posting.postentry.ArrayPostEntryList(this.postEntries));
    }


    /**
     *  Adds a post entry.
     *
     *  @param entry a post entry
     *  @throws org.osid.NullArgumentException
     *          <code>entry</code> is <code>null</code>
     */

    protected void addPostEntry(org.osid.financials.posting.PostEntry entry) {
        nullarg(entry, "posting entry");
        this.postEntries.add(entry);
        return;
    }


    /**
     *  Sets all the post entries.
     *
     *  @param entries a collection of post entries
     *  @throws org.osid.NullArgumentException
     *          <code>entries</code> is <code>null</code>
     */

    protected void setPostEntries(java.util.Collection<org.osid.financials.posting.PostEntry> entries) {
        nullarg(entries, "posting entries");

        this.postEntries.clear();
        this.postEntries.addAll(entries);

        return;
    }


    /**
     *  Tests if this <code> Post </code> is a correction to a
     *  previous post.
     *
     *  @return <code> true </code> if this post is a correction,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isCorrection() {
        return (this.correctedPost != null);
    }


    /**
     *  Gets the <code> Id </code> of the corrected <code> Post. </code> 
     *
     *  @return the corrected <code> Post </code> <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCorrection() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCorrectedPostId() {
        if (!isCorrection()) {
            throw new org.osid.IllegalStateException("isCorrection() is false");
        }

        return (this.correctedPost.getId());
    }


    /**
     *  Gets the corrected <code> Post. </code> 
     *
     *  @return the corrected post 
     *  @throws org.osid.IllegalStateException <code> isCorrection() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getCorrectedPost()
        throws org.osid.OperationFailedException {

        if (!isCorrection()) {
            throw new org.osid.IllegalStateException("isCorrection() is false");
        }

        return (this.correctedPost);
    }


    /**
     *  Sets the corrected post.
     *
     *  @param post a corrected post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    protected void setCorrectedPost(org.osid.financials.posting.Post post) {
        nullarg(post, "corrected post");
        this.correctedPost = post;
        return;
    }


    /**
     *  Tests if this post supports the given record
     *  <code>Type</code>.
     *
     *  @param  postRecordType a post record type 
     *  @return <code>true</code> if the postRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postRecordType) {
        for (org.osid.financials.posting.records.PostRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Post</code> record <code>Type</code>.
     *
     *  @param  postRecordType the post record type 
     *  @return the post record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostRecord getPostRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postRecord the post record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException
     *          <code>postRecord</code> or
     *          <code>postRecordTypepost</code> is
     *          <code>null</code>
     */
            
    protected void addPostRecord(org.osid.financials.posting.records.PostRecord postRecord, 
                                 org.osid.type.Type postRecordType) {
        
        nullarg(postRecord, "post record");
        addRecordType(postRecordType);
        this.records.add(postRecord);
        
        return;
    }
}
