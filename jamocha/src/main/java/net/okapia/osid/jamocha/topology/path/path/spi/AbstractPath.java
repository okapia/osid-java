//
// AbstractPath.java
//
//     Defines a Path.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Path</code>.
 */

public abstract class AbstractPath
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.topology.path.Path {

    private org.osid.topology.Node startingNode;
    private org.osid.topology.Node endingNode;
    private boolean complete = false;
    private long hops;
    private java.math.BigDecimal distance;
    private java.math.BigDecimal cost;
    private final java.util.Collection<org.osid.topology.Edge> edges = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.topology.path.records.PathRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the starting node of this path. 
     *
     *  @return the starting node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStartingNodeId() {
        return (this.startingNode.getId());
    }


    /**
     *  Gets the starting node of this path. 
     *
     *  @return the starting node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getStartingNode()
        throws org.osid.OperationFailedException {

        return (this.startingNode);
    }


    /**
     *  Sets the starting node.
     *
     *  @param node a starting node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void setStartingNode(org.osid.topology.Node node) {
        nullarg(node, "starting node");
        this.startingNode = node;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the ending node of this path. 
     *
     *  @return the ending node <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndingNodeId() {
        return (this.endingNode.getId());
    }


    /**
     *  Gets the ending node of this path. 
     *
     *  @return the ending node 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.Node getEndingNode()
        throws org.osid.OperationFailedException {

        return (this.endingNode);
    }


    /**
     *  Sets the ending node.
     *
     *  @param node an ending node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    protected void setEndingNode(org.osid.topology.Node node) {
        nullarg(node, "ending node");
        this.endingNode = node;
        return;
    }


    /**
     *  Tests if all edges exist between the start and end nodes. A path may 
     *  be complete and inactive. 
     *
     *  @return <code> true </code> if the path is complete, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.complete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the path is complete,
     *          <code> false </code> otherwise
     */

    protected void setComplete(boolean complete) {
        this.complete = complete;
        return;
    }


    /**
     *  Test if the starting node and ending node are equal. 
     *
     *  @return <code> true </code> if the path is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return (this.startingNode.equals(this.endingNode));
    }


    /**
     *  Gets the total number of nodes of this path. 
     *
     *  @return the total path hops 
     */

    @OSID @Override
    public long getHops() {
        return (this.hops);
    }


    /**
     *  Sets the hops.
     *
     *  @param hops the number of hops
     *  @throws org.osid.InvalidArgumentException <code>hop</code> is
     *          negative
     */

    protected void setHops(long hops) {
        cardinalarg(hops, "hops");
        this.hops = hops;
        return;
    }


    /**
     *  Gets the total distance of this path. 
     *
     *  @return the total path distance 
     */

    @OSID @Override
    public java.math.BigDecimal getDistance() {
        return (this.distance);
    }


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    protected void setDistance(java.math.BigDecimal distance) {
        nullarg(distance, "distance");
        this.distance = distance;
        return;
    }


    /**
     *  Gets the total cost of this path. 
     *
     *  @return the total path cost 
     */

    @OSID @Override
    public java.math.BigDecimal getCost() {
        return (this.cost);
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException
     *          <code>cost</code> is <code>null</code>
     */

    protected void setCost(java.math.BigDecimal cost) {
        nullarg(cost, "cost");
        this.cost = cost;
        return;
    }


    /**
     *  Gets the edge <code> Ids </code> of this path. 
     *
     *  @return the edge <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEdgeIds() {
        try {
            org.osid.topology.EdgeList edges = getEdges();
            return (new net.okapia.osid.jamocha.adapter.converter.topology.edge.EdgeToIdList(edges));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the edges of this path. 
     *
     *  @return the edges 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.topology.EdgeList getEdges()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.topology.edge.ArrayEdgeList(this.edges));
    }


    /**
     *  Adds an edge.
     *
     *  @param edge an edge
     *  @throws org.osid.NullArgumentException
     *          <code>edge</code> is <code>null</code>
     */

    protected void addEdge(org.osid.topology.Edge edge) {
        nullarg(edge, "edge");
        this.edges.add(edge);
        return;
    }


    /**
     *  Sets all the edges.
     *
     *  @param edges a collection of edges
     *  @throws org.osid.NullArgumentException
     *          <code>edges</code> is <code>null</code>
     */

    protected void setEdges(java.util.Collection<org.osid.topology.Edge> edges) {
        nullarg(edges, "edges");
        this.edges.clear();
        this.edges.addAll(edges);
        return;
    }


    /**
     *  Tests if this path supports the given record
     *  <code>Type</code>.
     *
     *  @param  pathRecordType a path record type 
     *  @return <code>true</code> if the pathRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pathRecordType) {
        for (org.osid.topology.path.records.PathRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Path</code>
     *  record <code>Type</code>.
     *
     *  @param  pathRecordType the path record type 
     *  @return the path record 
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pathRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.path.records.PathRecord getPathRecord(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.path.records.PathRecord record : this.records) {
            if (record.implementsRecordType(pathRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pathRecordType + " is not supported");
    }


    /**
     *  Adds a record to this path. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pathRecord the path record
     *  @param pathRecordType path record type
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecord</code> or
     *          <code>pathRecordTypepath</code> is
     *          <code>null</code>
     */
            
    protected void addPathRecord(org.osid.topology.path.records.PathRecord pathRecord, 
                                 org.osid.type.Type pathRecordType) {
        
        nullarg(pathRecord, "path record");
        addRecordType(pathRecordType);
        this.records.add(pathRecord);
        
        return;
    }
}
