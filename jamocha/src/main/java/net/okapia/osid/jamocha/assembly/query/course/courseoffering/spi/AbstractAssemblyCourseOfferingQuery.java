//
// AbstractAssemblyCourseOfferingQuery.java
//
//     A CourseOfferingQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CourseOfferingQuery that stores terms.
 */

public abstract class AbstractAssemblyCourseOfferingQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.CourseOfferingQuery,
               org.osid.course.CourseOfferingQueryInspector,
               org.osid.course.CourseOfferingSearchOrder {

    private final java.util.Collection<org.osid.course.records.CourseOfferingQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseOfferingQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseOfferingSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCourseOfferingQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCourseOfferingQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the course <code> Id </code> for this query to match course 
     *  offerings that have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseColumn(), style);
        return;
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course query terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match course 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by term. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTermColumn(), style);
        return;
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term query terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Adds a title for this query. 
     *
     *  @param  title title string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches a title that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          title, <code> false </code> to match course offerings with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course offering 
     *  title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Adds a course number for this query. 
     *
     *  @param  number course number string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        getAssembler().addStringTerm(getNumberColumn(), number, stringMatchType, match);
        return;
    }


    /**
     *  Matches a course number that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          number, <code> false </code> to match course offerings with no 
     *          number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        getAssembler().addStringWildcardTerm(getNumberColumn(), match);
        return;
    }


    /**
     *  Clears the number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        getAssembler().clearTerms(getNumberColumn());
        return;
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (getAssembler().getStringTerms(getNumberColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course offering 
     *  number. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNumberColumn(), style);
        return;
    }


    /**
     *  Gets the Number column name.
     *
     * @return the column name
     */

    protected String getNumberColumn() {
        return ("number");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match course 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getInstructorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        getAssembler().clearTerms(getInstructorIdColumn());
        return;
    }


    /**
     *  Gets the instructor <code> Id </code> query terms. 
     *
     *  @return the instructor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstructorIdTerms() {
        return (getAssembler().getIdTerms(getInstructorIdColumn()));
    }


    /**
     *  Gets the InstructorId column name.
     *
     * @return the column name
     */

    protected String getInstructorIdColumn() {
        return ("instructor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches course offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          instructor, <code> false </code> to match course offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        getAssembler().addIdWildcardTerm(getInstructorColumn(), match);
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        getAssembler().clearTerms(getInstructorColumn());
        return;
    }


    /**
     *  Gets the instructor query terms. 
     *
     *  @return the instructor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getInstructorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Instructor column name.
     *
     * @return the column name
     */

    protected String getInstructorColumn() {
        return ("instructor");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match courses 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches courses that have any sponsor. 
     *
     *  @param  match <code> true </code> to match courses with any sponsor, 
     *          <code> false </code> to match courses with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        getAssembler().addIdWildcardTerm(getSponsorColumn(), match);
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Sets the grade <code> Id </code> for this query to match courses 
     *  that have a credit amount.
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditAmountId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getCreditAmountIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the credit amount <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditAmountIdTerms() {
        getAssembler().clearTerms(getCreditAmountIdColumn());
        return;
    }


    /**
     *  Gets the credit amount <code> Id </code> query terms. 
     *
     *  @return the credit amount <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditAmountIdTerms() {
        return (getAssembler().getIdTerms(getCreditAmountIdColumn()));
    }


    /**
     *  Gets the CreditAmountId column name.
     *
     * @return the column name
     */

    protected String getCreditAmountIdColumn() {
        return ("credit_amount_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditAmountQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credit amount. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditAmountQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getCreditAmountQuery() {
        throw new org.osid.UnimplementedException("supportsCreditAmountQuery() is false");
    }


    /**
     *  Matches courses that have any credit amount. 
     *
     *  @param match <code> true </code> to match courses with any
     *         credit amount, <code> false </code> to match courses
     *         with no credit amount
     */

    @OSID @Override
    public void matchAnyCreditAmount(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditAmountColumn(), match);
        return;
    }


    /**
     *  Clears the credit anount terms. 
     */

    @OSID @Override
    public void clearCreditAmountTerms() {
        getAssembler().clearTerms(getCreditAmountColumn());
        return;
    }


    /**
     *  Gets the creditamount query terms. 
     *
     *  @return the credit amountquery terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getCreditAmountTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the CreditAmount column name.
     *
     *  @return the column name
     */

    protected String getCreditAmountColumn() {
        return ("credit_amount");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        getAssembler().addIdTerm(getGradingOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        getAssembler().clearTerms(getGradingOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (getAssembler().getIdTerms(getGradingOptionIdColumn()));
    }


    /**
     *  Gets the GradingOptionId column name.
     *
     * @return the column name
     */

    protected String getGradingOptionIdColumn() {
        return ("grading_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches course offerings that have any grading option. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          grading option, <code> false </code> to match course offerings 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        getAssembler().addIdWildcardTerm(getGradingOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        getAssembler().clearTerms(getGradingOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the GradingOption column name.
     *
     * @return the column name
     */

    protected String getGradingOptionColumn() {
        return ("grading_option");
    }


    /**
     *  Matches course offerings that require registration. 
     *
     *  @param  match <code> true </code> to match course offerings requiring 
     *          registration,, <code> false </code> to match course offerings 
     *          not requiring registration 
     */

    @OSID @Override
    public void matchRequiresRegistration(boolean match) {
        getAssembler().addBooleanTerm(getRequiresRegistrationColumn(), match);
        return;
    }


    /**
     *  Clears the requires registration terms. 
     */

    @OSID @Override
    public void clearRequiresRegistrationTerms() {
        getAssembler().clearTerms(getRequiresRegistrationColumn());
        return;
    }


    /**
     *  Gets the requires registration query terms. 
     *
     *  @return the requires registration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiresRegistrationTerms() {
        return (getAssembler().getBooleanTerms(getRequiresRegistrationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course offerings 
     *  requiring registration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequiresRegistration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequiresRegistrationColumn(), style);
        return;
    }


    /**
     *  Gets the RequiresRegistration column name.
     *
     * @return the column name
     */

    protected String getRequiresRegistrationColumn() {
        return ("requires_registration");
    }


    /**
     *  Matches course offerings with minimum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches a course offering that has any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          minimum seats, <code> false </code> to match course offerings 
     *          with no minimum seats 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the minimum seats terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        getAssembler().clearTerms(getMinimumSeatsColumn());
        return;
    }


    /**
     *  Gets the minimum seats query terms. 
     *
     *  @return the minimum seats query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumSeats column name.
     *
     * @return the column name
     */

    protected String getMinimumSeatsColumn() {
        return ("minimum_seats");
    }


    /**
     *  Matches course offerings with maximum seating between the given 
     *  numbers inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumSeatsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches a course offering that has any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          maximum seats, <code> false </code> to match course offerings 
     *          with no maximum seats 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMaximumSeatsColumn(), match);
        return;
    }


    /**
     *  Clears the maximum seats terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        getAssembler().clearTerms(getMaximumSeatsColumn());
        return;
    }


    /**
     *  Gets the maximum seats query terms. 
     *
     *  @return the maximum seats query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumSeatsTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumSeatsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  seats. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumSeats(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumSeatsColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumSeats column name.
     *
     * @return the column name
     */

    protected String getMaximumSeatsColumn() {
        return ("maximum_seats");
    }


    /**
     *  Adds a class url for this query. 
     *
     *  @param  url url string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        getAssembler().addStringTerm(getURLColumn(), url, stringMatchType, match);
        return;
    }


    /**
     *  Matches a url that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          url, <code> false </code> to match course offerings with no 
     *          url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        getAssembler().addStringWildcardTerm(getURLColumn(), match);
        return;
    }


    /**
     *  Clears the url terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        getAssembler().clearTerms(getURLColumn());
        return;
    }


    /**
     *  Gets the url query terms. 
     *
     *  @return the url query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getURLTerms() {
        return (getAssembler().getStringTerms(getURLColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by url. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByURL(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getURLColumn(), style);
        return;
    }


    /**
     *  Gets the URL column name.
     *
     * @return the column name
     */

    protected String getURLColumn() {
        return ("u_rl");
    }


    /**
     *  Adds a schedule informational string for this query. 
     *
     *  @param  scheduleInfo schedule string string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> scheduleInfo </code> 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleInfo </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchScheduleInfo(String scheduleInfo, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getScheduleInfoColumn(), scheduleInfo, stringMatchType, match);
        return;
    }


    /**
     *  Matches a schedule informational string that has any value. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          schedule string, <code> false </code> to match course 
     *          offerings with no schedule string 
     */

    @OSID @Override
    public void matchAnyScheduleInfo(boolean match) {
        getAssembler().addStringWildcardTerm(getScheduleInfoColumn(), match);
        return;
    }


    /**
     *  Clears the schedule info terms. 
     */

    @OSID @Override
    public void clearScheduleInfoTerms() {
        getAssembler().clearTerms(getScheduleInfoColumn());
        return;
    }


    /**
     *  Gets the scheudle info query terms. 
     *
     *  @return the schedule info query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getScheduleInfoTerms() {
        return (getAssembler().getStringTerms(getScheduleInfoColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by schedule info. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScheduleInfo(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getScheduleInfoColumn(), style);
        return;
    }


    /**
     *  Gets the ScheduleInfo column name.
     *
     * @return the column name
     */

    protected String getScheduleInfoColumn() {
        return ("schedule_info");
    }


    /**
     *  Sets the event <code> Id </code> for this query to match course 
     *  offerings that have an event. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        getAssembler().clearTerms(getEventIdColumn());
        return;
    }


    /**
     *  Gets the event <code> Id </code> query terms. 
     *
     *  @return the event <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEventIdTerms() {
        return (getAssembler().getIdTerms(getEventIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEventColumn(), style);
        return;
    }


    /**
     *  Gets the EventId column name.
     *
     * @return the column name
     */

    protected String getEventIdColumn() {
        return ("event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches any event. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          event., <code> false </code> to match course offerings with no 
     *          events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getEventColumn(), match);
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        getAssembler().clearTerms(getEventColumn());
        return;
    }


    /**
     *  Gets the event query terms. 
     *
     *  @return the event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an event search order is available. 
     *
     *  @return <code> true </code> if an event order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the event order. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEventSearchOrder() is false");
    }


    /**
     *  Gets the Event column name.
     *
     * @return the column name
     */

    protected String getEventColumn() {
        return ("event");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match course 
     *  offerings that have a related activity. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches any related activity. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          activity, <code> false </code> to match course offerings with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course offerings assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this courseOffering supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseOfferingRecordType a course offering record type 
     *  @return <code>true</code> if the courseOfferingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseOfferingRecordType) {
        for (org.osid.course.records.CourseOfferingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  courseOfferingRecordType the course offering record type 
     *  @return the course offering query record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingQueryRecord getCourseOfferingQueryRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  courseOfferingRecordType the course offering record type 
     *  @return the course offering query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingQueryInspectorRecord getCourseOfferingQueryInspectorRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param courseOfferingRecordType the course offering record type
     *  @return the course offering search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseOfferingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingSearchOrderRecord getCourseOfferingSearchOrderRecord(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseOfferingSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(courseOfferingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseOfferingRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this course offering. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseOfferingQueryRecord the course offering query record
     *  @param courseOfferingQueryInspectorRecord the course offering query inspector
     *         record
     *  @param courseOfferingSearchOrderRecord the course offering search order record
     *  @param courseOfferingRecordType course offering record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingQueryRecord</code>,
     *          <code>courseOfferingQueryInspectorRecord</code>,
     *          <code>courseOfferingSearchOrderRecord</code> or
     *          <code>courseOfferingRecordTypecourseOffering</code> is
     *          <code>null</code>
     */
            
    protected void addCourseOfferingRecords(org.osid.course.records.CourseOfferingQueryRecord courseOfferingQueryRecord, 
                                      org.osid.course.records.CourseOfferingQueryInspectorRecord courseOfferingQueryInspectorRecord, 
                                      org.osid.course.records.CourseOfferingSearchOrderRecord courseOfferingSearchOrderRecord, 
                                      org.osid.type.Type courseOfferingRecordType) {

        addRecordType(courseOfferingRecordType);

        nullarg(courseOfferingQueryRecord, "course offering query record");
        nullarg(courseOfferingQueryInspectorRecord, "course offering query inspector record");
        nullarg(courseOfferingSearchOrderRecord, "course offering search odrer record");

        this.queryRecords.add(courseOfferingQueryRecord);
        this.queryInspectorRecords.add(courseOfferingQueryInspectorRecord);
        this.searchOrderRecords.add(courseOfferingSearchOrderRecord);
        
        return;
    }
}
