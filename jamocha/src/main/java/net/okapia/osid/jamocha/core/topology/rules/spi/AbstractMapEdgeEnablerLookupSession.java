//
// AbstractMapEdgeEnablerLookupSession
//
//    A simple framework for providing an EdgeEnabler lookup service
//    backed by a fixed collection of edge enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an EdgeEnabler lookup service backed by a
 *  fixed collection of edge enablers. The edge enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>EdgeEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEdgeEnablerLookupSession
    extends net.okapia.osid.jamocha.topology.rules.spi.AbstractEdgeEnablerLookupSession
    implements org.osid.topology.rules.EdgeEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.topology.rules.EdgeEnabler> edgeEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.topology.rules.EdgeEnabler>());


    /**
     *  Makes an <code>EdgeEnabler</code> available in this session.
     *
     *  @param  edgeEnabler an edge enabler
     *  @throws org.osid.NullArgumentException <code>edgeEnabler<code>
     *          is <code>null</code>
     */

    protected void putEdgeEnabler(org.osid.topology.rules.EdgeEnabler edgeEnabler) {
        this.edgeEnablers.put(edgeEnabler.getId(), edgeEnabler);
        return;
    }


    /**
     *  Makes an array of edge enablers available in this session.
     *
     *  @param  edgeEnablers an array of edge enablers
     *  @throws org.osid.NullArgumentException <code>edgeEnablers<code>
     *          is <code>null</code>
     */

    protected void putEdgeEnablers(org.osid.topology.rules.EdgeEnabler[] edgeEnablers) {
        putEdgeEnablers(java.util.Arrays.asList(edgeEnablers));
        return;
    }


    /**
     *  Makes a collection of edge enablers available in this session.
     *
     *  @param  edgeEnablers a collection of edge enablers
     *  @throws org.osid.NullArgumentException <code>edgeEnablers<code>
     *          is <code>null</code>
     */

    protected void putEdgeEnablers(java.util.Collection<? extends org.osid.topology.rules.EdgeEnabler> edgeEnablers) {
        for (org.osid.topology.rules.EdgeEnabler edgeEnabler : edgeEnablers) {
            this.edgeEnablers.put(edgeEnabler.getId(), edgeEnabler);
        }

        return;
    }


    /**
     *  Removes an EdgeEnabler from this session.
     *
     *  @param  edgeEnablerId the <code>Id</code> of the edge enabler
     *  @throws org.osid.NullArgumentException <code>edgeEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeEdgeEnabler(org.osid.id.Id edgeEnablerId) {
        this.edgeEnablers.remove(edgeEnablerId);
        return;
    }


    /**
     *  Gets the <code>EdgeEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  edgeEnablerId <code>Id</code> of the <code>EdgeEnabler</code>
     *  @return the edgeEnabler
     *  @throws org.osid.NotFoundException <code>edgeEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>edgeEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnabler getEdgeEnabler(org.osid.id.Id edgeEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.topology.rules.EdgeEnabler edgeEnabler = this.edgeEnablers.get(edgeEnablerId);
        if (edgeEnabler == null) {
            throw new org.osid.NotFoundException("edgeEnabler not found: " + edgeEnablerId);
        }

        return (edgeEnabler);
    }


    /**
     *  Gets all <code>EdgeEnablers</code>. In plenary mode, the returned
     *  list contains all known edgeEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  edgeEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>EdgeEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.rules.EdgeEnablerList getEdgeEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.rules.edgeenabler.ArrayEdgeEnablerList(this.edgeEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.edgeEnablers.clear();
        super.close();
        return;
    }
}
