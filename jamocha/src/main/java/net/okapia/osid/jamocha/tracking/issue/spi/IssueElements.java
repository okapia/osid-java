//
// IssueElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.issue.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class IssueElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the IssueElement Id.
     *
     *  @return the issue element Id
     */

    public static org.osid.id.Id getIssueEntityId() {
        return (makeEntityId("osid.tracking.Issue"));
    }


    /**
     *  Gets the QueueId element Id.
     *
     *  @return the QueueId element Id
     */

    public static org.osid.id.Id getQueueId() {
        return (makeElementId("osid.tracking.issue.QueueId"));
    }


    /**
     *  Gets the Queue element Id.
     *
     *  @return the Queue element Id
     */

    public static org.osid.id.Id getQueue() {
        return (makeElementId("osid.tracking.issue.Queue"));
    }


    /**
     *  Gets the CustomerId element Id.
     *
     *  @return the CustomerId element Id
     */

    public static org.osid.id.Id getCustomerId() {
        return (makeElementId("osid.tracking.issue.CustomerId"));
    }


    /**
     *  Gets the Customer element Id.
     *
     *  @return the Customer element Id
     */

    public static org.osid.id.Id getCustomer() {
        return (makeElementId("osid.tracking.issue.Customer"));
    }


    /**
     *  Gets the TopicId element Id.
     *
     *  @return the TopicId element Id
     */

    public static org.osid.id.Id getTopicId() {
        return (makeElementId("osid.tracking.issue.TopicId"));
    }


    /**
     *  Gets the Topic element Id.
     *
     *  @return the Topic element Id
     */

    public static org.osid.id.Id getTopic() {
        return (makeElementId("osid.tracking.issue.Topic"));
    }


    /**
     *  Gets the MasterIssueId element Id.
     *
     *  @return the MasterIssueId element Id
     */

    public static org.osid.id.Id getMasterIssueId() {
        return (makeElementId("osid.tracking.issue.MasterIssueId"));
    }


    /**
     *  Gets the MasterIssue element Id.
     *
     *  @return the MasterIssue element Id
     */

    public static org.osid.id.Id getMasterIssue() {
        return (makeElementId("osid.tracking.issue.MasterIssue"));
    }


    /**
     *  Gets the DuplicateIssueIds element Id.
     *
     *  @return the DuplicateIssueIds element Id
     */

    public static org.osid.id.Id getDuplicateIssueIds() {
        return (makeElementId("osid.tracking.issue.DuplicateIssueIds"));
    }


    /**
     *  Gets the DuplicateIssues element Id.
     *
     *  @return the DuplicateIssues element Id
     */

    public static org.osid.id.Id getDuplicateIssues() {
        return (makeElementId("osid.tracking.issue.DuplicateIssues"));
    }


    /**
     *  Gets the BranchedIssueId element Id.
     *
     *  @return the BranchedIssueId element Id
     */

    public static org.osid.id.Id getBranchedIssueId() {
        return (makeElementId("osid.tracking.issue.BranchedIssueId"));
    }


    /**
     *  Gets the BranchedIssue element Id.
     *
     *  @return the BranchedIssue element Id
     */

    public static org.osid.id.Id getBranchedIssue() {
        return (makeElementId("osid.tracking.issue.BranchedIssue"));
    }


    /**
     *  Gets the RootIssueId element Id.
     *
     *  @return the RootIssueId element Id
     */

    public static org.osid.id.Id getRootIssueId() {
        return (makeElementId("osid.tracking.issue.RootIssueId"));
    }


    /**
     *  Gets the RootIssue element Id.
     *
     *  @return the RootIssue element Id
     */

    public static org.osid.id.Id getRootIssue() {
        return (makeElementId("osid.tracking.issue.RootIssue"));
    }


    /**
     *  Gets the PriorityType element Id.
     *
     *  @return the PriorityType element Id
     */

    public static org.osid.id.Id getPriorityType() {
        return (makeElementId("osid.tracking.issue.PriorityType"));
    }


    /**
     *  Gets the CreatorId element Id.
     *
     *  @return the CreatorId element Id
     */

    public static org.osid.id.Id getCreatorId() {
        return (makeElementId("osid.tracking.issue.CreatorId"));
    }


    /**
     *  Gets the Creator element Id.
     *
     *  @return the Creator element Id
     */

    public static org.osid.id.Id getCreator() {
        return (makeElementId("osid.tracking.issue.Creator"));
    }


    /**
     *  Gets the CreatingAgentId element Id.
     *
     *  @return the CreatingAgentId element Id
     */

    public static org.osid.id.Id getCreatingAgentId() {
        return (makeElementId("osid.tracking.issue.CreatingAgentId"));
    }


    /**
     *  Gets the CreatingAgent element Id.
     *
     *  @return the CreatingAgent element Id
     */

    public static org.osid.id.Id getCreatingAgent() {
        return (makeElementId("osid.tracking.issue.CreatingAgent"));
    }


    /**
     *  Gets the CreatedDate element Id.
     *
     *  @return the CreatedDate element Id
     */

    public static org.osid.id.Id getCreatedDate() {
        return (makeElementId("osid.tracking.issue.CreatedDate"));
    }


    /**
     *  Gets the ReopenerId element Id.
     *
     *  @return the ReopenerId element Id
     */

    public static org.osid.id.Id getReopenerId() {
        return (makeElementId("osid.tracking.issue.ReopenerId"));
    }


    /**
     *  Gets the Reopener element Id.
     *
     *  @return the Reopener element Id
     */

    public static org.osid.id.Id getReopener() {
        return (makeElementId("osid.tracking.issue.Reopener"));
    }


    /**
     *  Gets the ReopeningAgentId element Id.
     *
     *  @return the ReopeningAgentId element Id
     */

    public static org.osid.id.Id getReopeningAgentId() {
        return (makeElementId("osid.tracking.issue.ReopeningAgentId"));
    }


    /**
     *  Gets the ReopeningAgent element Id.
     *
     *  @return the ReopeningAgent element Id
     */

    public static org.osid.id.Id getReopeningAgent() {
        return (makeElementId("osid.tracking.issue.ReopeningAgent"));
    }


    /**
     *  Gets the ReopenedDate element Id.
     *
     *  @return the ReopenedDate element Id
     */

    public static org.osid.id.Id getReopenedDate() {
        return (makeElementId("osid.tracking.issue.ReopenedDate"));
    }


    /**
     *  Gets the DueDate element Id.
     *
     *  @return the DueDate element Id
     */

    public static org.osid.id.Id getDueDate() {
        return (makeElementId("osid.tracking.issue.DueDate"));
    }


    /**
     *  Gets the BlockerIds element Id.
     *
     *  @return the BlockerIds element Id
     */

    public static org.osid.id.Id getBlockerIds() {
        return (makeElementId("osid.tracking.issue.BlockerIds"));
    }


    /**
     *  Gets the Blockers element Id.
     *
     *  @return the Blockers element Id
     */

    public static org.osid.id.Id getBlockers() {
        return (makeElementId("osid.tracking.issue.Blockers"));
    }


    /**
     *  Gets the ResolverId element Id.
     *
     *  @return the ResolverId element Id
     */

    public static org.osid.id.Id getResolverId() {
        return (makeElementId("osid.tracking.issue.ResolverId"));
    }


    /**
     *  Gets the Resolver element Id.
     *
     *  @return the Resolver element Id
     */

    public static org.osid.id.Id getResolver() {
        return (makeElementId("osid.tracking.issue.Resolver"));
    }


    /**
     *  Gets the ResolvingAgentId element Id.
     *
     *  @return the ResolvingAgentId element Id
     */

    public static org.osid.id.Id getResolvingAgentId() {
        return (makeElementId("osid.tracking.issue.ResolvingAgentId"));
    }


    /**
     *  Gets the ResolvingAgent element Id.
     *
     *  @return the ResolvingAgent element Id
     */

    public static org.osid.id.Id getResolvingAgent() {
        return (makeElementId("osid.tracking.issue.ResolvingAgent"));
    }


    /**
     *  Gets the ResolvedDate element Id.
     *
     *  @return the ResolvedDate element Id
     */

    public static org.osid.id.Id getResolvedDate() {
        return (makeElementId("osid.tracking.issue.ResolvedDate"));
    }


    /**
     *  Gets the ResolutionType element Id.
     *
     *  @return the ResolutionType element Id
     */

    public static org.osid.id.Id getResolutionType() {
        return (makeElementId("osid.tracking.issue.ResolutionType"));
    }


    /**
     *  Gets the CloserId element Id.
     *
     *  @return the CloserId element Id
     */

    public static org.osid.id.Id getCloserId() {
        return (makeElementId("osid.tracking.issue.CloserId"));
    }


    /**
     *  Gets the Closer element Id.
     *
     *  @return the Closer element Id
     */

    public static org.osid.id.Id getCloser() {
        return (makeElementId("osid.tracking.issue.Closer"));
    }


    /**
     *  Gets the ClosingAgentId element Id.
     *
     *  @return the ClosingAgentId element Id
     */

    public static org.osid.id.Id getClosingAgentId() {
        return (makeElementId("osid.tracking.issue.ClosingAgentId"));
    }


    /**
     *  Gets the ClosingAgent element Id.
     *
     *  @return the ClosingAgent element Id
     */

    public static org.osid.id.Id getClosingAgent() {
        return (makeElementId("osid.tracking.issue.ClosingAgent"));
    }


    /**
     *  Gets the ClosedDate element Id.
     *
     *  @return the ClosedDate element Id
     */

    public static org.osid.id.Id getClosedDate() {
        return (makeElementId("osid.tracking.issue.ClosedDate"));
    }


    /**
     *  Gets the AssignedResourceId element Id.
     *
     *  @return the AssignedResourceId element Id
     */

    public static org.osid.id.Id getAssignedResourceId() {
        return (makeElementId("osid.tracking.issue.AssignedResourceId"));
    }


    /**
     *  Gets the AssignedResource element Id.
     *
     *  @return the AssignedResource element Id
     */

    public static org.osid.id.Id getAssignedResource() {
        return (makeElementId("osid.tracking.issue.AssignedResource"));
    }


    /**
     *  Gets the SubtaskId element Id.
     *
     *  @return the SubtaskId element Id
     */

    public static org.osid.id.Id getSubtaskId() {
        return (makeQueryElementId("osid.tracking.issue.SubtaskId"));
    }


    /**
     *  Gets the Subtask element Id.
     *
     *  @return the Subtask element Id
     */

    public static org.osid.id.Id getSubtask() {
        return (makeQueryElementId("osid.tracking.issue.Subtask"));
    }


    /**
     *  Gets the Pending element Id.
     *
     *  @return the Pending element Id
     */

    public static org.osid.id.Id getPending() {
        return (makeQueryElementId("osid.tracking.issue.Pending"));
    }


    /**
     *  Gets the BlockingIssueId element Id.
     *
     *  @return the BlockingIssueId element Id
     */

    public static org.osid.id.Id getBlockingIssueId() {
        return (makeQueryElementId("osid.tracking.issue.BlockingIssueId"));
    }


    /**
     *  Gets the BlockingIssue element Id.
     *
     *  @return the BlockingIssue element Id
     */

    public static org.osid.id.Id getBlockingIssue() {
        return (makeQueryElementId("osid.tracking.issue.BlockingIssue"));
    }


    /**
     *  Gets the BlockedIssueId element Id.
     *
     *  @return the BlockedIssueId element Id
     */

    public static org.osid.id.Id getBlockedIssueId() {
        return (makeQueryElementId("osid.tracking.issue.BlockedIssueId"));
    }


    /**
     *  Gets the BlockedIssue element Id.
     *
     *  @return the BlockedIssue element Id
     */

    public static org.osid.id.Id getBlockedIssue() {
        return (makeQueryElementId("osid.tracking.issue.BlockedIssue"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeQueryElementId("osid.tracking.issue.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeQueryElementId("osid.tracking.issue.Resource"));
    }


    /**
     *  Gets the LogEntryId element Id.
     *
     *  @return the LogEntryId element Id
     */

    public static org.osid.id.Id getLogEntryId() {
        return (makeQueryElementId("osid.tracking.issue.LogEntryId"));
    }


    /**
     *  Gets the LogEntry element Id.
     *
     *  @return the LogEntry element Id
     */

    public static org.osid.id.Id getLogEntry() {
        return (makeQueryElementId("osid.tracking.issue.LogEntry"));
    }


    /**
     *  Gets the FrontOfficeId element Id.
     *
     *  @return the FrontOfficeId element Id
     */

    public static org.osid.id.Id getFrontOfficeId() {
        return (makeQueryElementId("osid.tracking.issue.FrontOfficeId"));
    }


    /**
     *  Gets the FrontOffice element Id.
     *
     *  @return the FrontOffice element Id
     */

    public static org.osid.id.Id getFrontOffice() {
        return (makeQueryElementId("osid.tracking.issue.FrontOffice"));
    }


    /**
     *  Gets the Status element Id.
     *
     *  @return the Status element Id
     */

    public static org.osid.id.Id getStatus() {
        return (makeSearchOrderElementId("osid.tracking.issue.Status"));
    }
}
