//
// AbstractCreditSearch.java
//
//     A template for making a Credit Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing credit searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCreditSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.acknowledgement.CreditSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.CreditSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.acknowledgement.CreditSearchOrder creditSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of credits. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  creditIds list of credits
     *  @throws org.osid.NullArgumentException
     *          <code>creditIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCredits(org.osid.id.IdList creditIds) {
        while (creditIds.hasNext()) {
            try {
                this.ids.add(creditIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCredits</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of credit Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCreditIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  creditSearchOrder credit search order 
     *  @throws org.osid.NullArgumentException
     *          <code>creditSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>creditSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCreditResults(org.osid.acknowledgement.CreditSearchOrder creditSearchOrder) {
	this.creditSearchOrder = creditSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.acknowledgement.CreditSearchOrder getCreditSearchOrder() {
	return (this.creditSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given credit search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a credit implementing the requested record.
     *
     *  @param creditSearchRecordType a credit search record
     *         type
     *  @return the credit search record
     *  @throws org.osid.NullArgumentException
     *          <code>creditSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(creditSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditSearchRecord getCreditSearchRecord(org.osid.type.Type creditSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.acknowledgement.records.CreditSearchRecord record : this.records) {
            if (record.implementsRecordType(creditSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(creditSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this credit search. 
     *
     *  @param creditSearchRecord credit search record
     *  @param creditSearchRecordType credit search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCreditSearchRecord(org.osid.acknowledgement.records.CreditSearchRecord creditSearchRecord, 
                                           org.osid.type.Type creditSearchRecordType) {

        addRecordType(creditSearchRecordType);
        this.records.add(creditSearchRecord);        
        return;
    }
}
