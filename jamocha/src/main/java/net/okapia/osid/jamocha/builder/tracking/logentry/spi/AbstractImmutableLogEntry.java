//
// AbstractImmutableLogEntry.java
//
//     Wraps a mutable LogEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.tracking.logentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>LogEntry</code> to hide modifiers. This
 *  wrapper provides an immutized LogEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying logEntry whose state changes are visible.
 */

public abstract class AbstractImmutableLogEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.tracking.LogEntry {

    private final org.osid.tracking.LogEntry logEntry;


    /**
     *  Constructs a new <code>AbstractImmutableLogEntry</code>.
     *
     *  @param logEntry the log entry to immutablize
     *  @throws org.osid.NullArgumentException <code>logEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLogEntry(org.osid.tracking.LogEntry logEntry) {
        super(logEntry);
        this.logEntry = logEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.logEntry.getAgentId());
    }


    /**
     *  Gets the agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.logEntry.getAgent());
    }


    /**
     *  Gets the <code> Id </code> of the issue of this log entry. 
     *
     *  @return the issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIssueId() {
        return (this.logEntry.getIssueId());
    }


    /**
     *  Gets the issue of this log entry. 
     *
     *  @return the issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue()
        throws org.osid.OperationFailedException {

        return (this.logEntry.getIssue());
    }


    /**
     *  Gets the date of this log entry. 
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.logEntry.getDate());
    }


    /**
     *  Gets the action of this log entry. 
     *
     *  @return the issue action 
     */

    @OSID @Override
    public org.osid.tracking.IssueAction getAction() {
        return (this.logEntry.getAction());
    }


    /**
     *  Gets a summary header for this entry. 
     *
     *  @return the summary 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSummary() {
        return (this.logEntry.getSummary());
    }


    /**
     *  Gets the text of this entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.logEntry.getMessage());
    }


    /**
     *  Gets the log entry record corresponding to the given <code> LogEntry 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  logEntryRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(logEntryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  logEntryRecordType the type of log entry record to retrieve 
     *  @return the log entry record 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(logEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntryRecord getLogEntryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.logEntry.getLogEntryRecord(logEntryRecordType));
    }
}

