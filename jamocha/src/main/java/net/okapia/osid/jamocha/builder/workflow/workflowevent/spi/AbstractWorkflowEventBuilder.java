//
// AbstractWorkflowEvent.java
//
//     Defines a WorkflowEvent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.workflowevent.spi;


/**
 *  Defines a <code>WorkflowEvent</code> builder.
 */

public abstract class AbstractWorkflowEventBuilder<T extends AbstractWorkflowEventBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.workflow.workflowevent.WorkflowEventMiter workflowEvent;


    /**
     *  Constructs a new <code>AbstractWorkflowEventBuilder</code>.
     *
     *  @param workflowEvent the workflow event to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractWorkflowEventBuilder(net.okapia.osid.jamocha.builder.workflow.workflowevent.WorkflowEventMiter workflowEvent) {
        super(workflowEvent);
        this.workflowEvent = workflowEvent;
        return;
    }


    /**
     *  Builds the workflow event.
     *
     *  @return the new workflow event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.workflow.WorkflowEvent build() {
        (new net.okapia.osid.jamocha.builder.validator.workflow.workflowevent.WorkflowEventValidator(getValidations())).validate(this.workflowEvent);
        return (new net.okapia.osid.jamocha.builder.workflow.workflowevent.ImmutableWorkflowEvent(this.workflowEvent));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the workflow event miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.workflow.workflowevent.WorkflowEventMiter getMiter() {
        return (this.workflowEvent);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public T timestamp(org.osid.calendaring.DateTime timestamp) {
        getMiter().setTimestamp(timestamp);
        return (self());
    }


    /**
     *  Sets the process.
     *
     *  @param process a process
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>process</code> is
     *          <code>null</code>
     */

    public T process(org.osid.workflow.Process process) {
        getMiter().setProcess(process);
        return (self());
    }


    /**
     *  Sets the worker.
     *
     *  @param worker a worker
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>worker</code> is
     *          <code>null</code>
     */

    public T worker(org.osid.resource.Resource worker) {
        getMiter().setWorker(worker);
        return (self());
    }


    /**
     *  Sets the working agent.
     *
     *  @param agent a working agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T workingAgent(org.osid.authentication.Agent agent) {
        getMiter().setWorkingAgent(agent);
        return (self());
    }


    /**
     *  Sets the work.
     *
     *  @param work a work
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>work</code> is
     *          <code>null</code>
     */

    public T work(org.osid.workflow.Work work) {
        getMiter().setWork(work);
        return (self());
    }


    /**
     *  Sets the step.
     *
     *  @param step a step
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>step</code> is
     *          <code>null</code>
     */

    public T step(org.osid.workflow.Step step) {
        getMiter().setStep(step);
        return (self());
    }


    /**
     *  Adds a WorkflowEvent record.
     *
     *  @param record a workflow event record
     *  @param recordType the type of workflow event record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.workflow.records.WorkflowEventRecord record, org.osid.type.Type recordType) {
        getMiter().addWorkflowEventRecord(record, recordType);
        return (self());
    }
}       


