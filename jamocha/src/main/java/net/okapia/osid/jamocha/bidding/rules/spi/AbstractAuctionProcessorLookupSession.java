//
// AbstractAuctionProcessorLookupSession.java
//
//    A starter implementation framework for providing an AuctionProcessor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AuctionProcessor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuctionProcessors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    

    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouse</code> is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionProcessor</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionProcessors() {
        return (true);
    }


    /**
     *  A complete view of the <code>AuctionProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AuctionProcessor</code> returns
     *  is desired.  Methods will return what is requested or result
     *  in an error. This view is used when greater precision is
     *  desired at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processors in auction houses
     *  which are children of this auction house in the auction house
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active auction processors are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction processors are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionProcessor</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AuctionProcessor</code> and retained for compatibility.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  @param  auctionProcessorId <code>Id</code> of the
     *          <code>AuctionProcessor</code>
     *  @return the auction processor
     *  @throws org.osid.NotFoundException <code>auctionProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessor getAuctionProcessor(org.osid.id.Id auctionProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.rules.AuctionProcessorList auctionProcessors = getAuctionProcessors()) {
            while (auctionProcessors.hasNext()) {
                org.osid.bidding.rules.AuctionProcessor auctionProcessor = auctionProcessors.getNextAuctionProcessor();
                if (auctionProcessor.getId().equals(auctionProcessorId)) {
                    return (auctionProcessor);
                }
            }
        } 

        throw new org.osid.NotFoundException(auctionProcessorId + " not found");
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessors specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>AuctionProcessors</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuctionProcessors()</code>.
     *
     *  @param  auctionProcessorIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByIds(org.osid.id.IdList auctionProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.rules.AuctionProcessor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auctionProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuctionProcessor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("auction processor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessor.LinkedAuctionProcessorList(ret));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the
     *  given auction processor genus <code>Type</code> which does not
     *  include auction processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuctionProcessors()</code>.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessor.AuctionProcessorGenusFilterList(getAuctionProcessors(), auctionProcessorGenusType));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the
     *  given auction processor genus <code>Type</code> and include
     *  any additional auction processors with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionProcessors()</code>.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByParentGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionProcessorsByGenusType(auctionProcessorGenusType));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> containing the given
     *  auction processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known auction
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, auction processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  auction processors are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionProcessors()</code>.
     *
     *  @param  auctionProcessorRecordType an auctionProcessor record type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByRecordType(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessor.AuctionProcessorRecordFilterList(getAuctionProcessors(), auctionProcessorRecordType));
    }


    /**
     *  Gets all <code>AuctionProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @return a list of <code>AuctionProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the auction processor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of auction processors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.rules.AuctionProcessorList filterAuctionProcessorsOnViews(org.osid.bidding.rules.AuctionProcessorList list)
        throws org.osid.OperationFailedException {

        org.osid.bidding.rules.AuctionProcessorList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessor.ActiveAuctionProcessorFilterList(ret);
        }

        return (ret);
    }
}
