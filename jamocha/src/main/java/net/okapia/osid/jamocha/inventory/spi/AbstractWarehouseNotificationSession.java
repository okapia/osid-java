//
// AbstractWarehouseNotificationSession.java
//
//     A template for making WarehouseNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Warehouse} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Warehouse} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for warehouse entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractWarehouseNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.WarehouseNotificationSession {


    /**
     *  Tests if this user can register for {@code Warehouse}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForWarehouseNotifications() {
        return (true);
    }


    /**
     *  Register for notifications of new warehouses. {@code
     *  WarehouseReceiver.newWarehouse()} is invoked when a new {@code
     *  Warehouse} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  warehouse. {@code WarehouseReceiver.newAncestorWarehouse()} is
     *  invoked when the specified warehouse node gets a new ancestor.
     *
     *  @param warehouseId the {@code Id} of the
     *         {@code Warehouse} node to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewWarehouseAncestors(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  warehouse. {@code WarehouseReceiver.newDescendantWarehouse()}
     *  is invoked when the specified warehouse node gets a new
     *  descendant.
     *
     *  @param warehouseId the {@code Id} of the
     *         {@code Warehouse} node to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewWarehouseDescendants(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated warehouses. {@code
     *  WarehouseReceiver.changedWarehouse()} is invoked when a
     *  warehouse is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated warehouse. {@code
     *  WarehouseReceiver.changedWarehouse()} is invoked when the
     *  specified warehouse is changed.
     *
     *  @param warehouseId the {@code Id} of the {@code Warehouse} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted warehouses. {@code
     *  WarehouseReceiver.deletedWarehouse()} is invoked when a
     *  warehouse is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedWarehouses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted warehouse. {@code
     *  WarehouseReceiver.deletedWarehouse()} is invoked when the
     *  specified warehouse is deleted.
     *
     *  @param warehouseId the {@code Id} of the
     *          {@code Warehouse} to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedWarehouse(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified warehouse. {@code
     *  WarehouseReceiver.deletedAncestor()} is invoked when the
     *  specified warehouse node loses an ancestor.
     *
     *  @param warehouseId the {@code Id} of the
     *         {@code Warehouse} node to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedWarehouseAncestors(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified warehouse. {@code
     *  WarehouseReceiver.deletedDescendant()} is invoked when the
     *  specified warehouse node loses a descendant.
     *
     *  @param warehouseId the {@code Id} of the
     *          {@code Warehouse} node to monitor
     *  @throws org.osid.NullArgumentException {@code warehouseId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedWarehouseDescendants(org.osid.id.Id warehouseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
