//
// AbstractBrokerList
//
//     Implements a filter for a BrokerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.provisioning.broker.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a BrokerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedBrokerList
 *  to improve performance.
 */

public abstract class AbstractBrokerFilterList
    extends net.okapia.osid.jamocha.provisioning.broker.spi.AbstractBrokerList
    implements org.osid.provisioning.BrokerList,
               net.okapia.osid.jamocha.inline.filter.provisioning.broker.BrokerFilter {

    private org.osid.provisioning.Broker broker;
    private final org.osid.provisioning.BrokerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractBrokerFilterList</code>.
     *
     *  @param brokerList a <code>BrokerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>brokerList</code> is <code>null</code>
     */

    protected AbstractBrokerFilterList(org.osid.provisioning.BrokerList brokerList) {
        nullarg(brokerList, "broker list");
        this.list = brokerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.broker == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Broker </code> in this list. 
     *
     *  @return the next <code> Broker </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Broker </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getNextBroker()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.provisioning.Broker broker = this.broker;
            this.broker = null;
            return (broker);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in broker list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.broker = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Brokers.
     *
     *  @param broker the broker to filter
     *  @return <code>true</code> if the broker passes the filter,
     *          <code>false</code> if the broker should be filtered
     */

    public abstract boolean pass(org.osid.provisioning.Broker broker);


    protected void prime() {
        if (this.broker != null) {
            return;
        }

        org.osid.provisioning.Broker broker = null;

        while (this.list.hasNext()) {
            try {
                broker = this.list.getNextBroker();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(broker)) {
                this.broker = broker;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
