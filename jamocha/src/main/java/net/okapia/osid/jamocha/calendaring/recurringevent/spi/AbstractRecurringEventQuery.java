//
// AbstractRecurringEventQuery.java
//
//     A template for making a RecurringEvent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for recurring events.
 */

public abstract class AbstractRecurringEventQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.calendaring.RecurringEventQuery {

    private final java.util.Collection<org.osid.calendaring.records.RecurringEventQueryRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Sets the schedule <code> Id </code> for this query for matching 
     *  schedules. 
     *
     *  @param  scheduleId a schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        return;
    }


    /**
     *  Clears the schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ScheduleQuery </code> is available for querying 
     *  schedules. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedule. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Matches a recurring event that has any schedule assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          schedules, <code> false </code> to match recurring events with 
     *          no schedules 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        return;
    }


    /**
     *  Clears the schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        return;
    }


    /**
     *  Sets the superseding event <code> Id </code> for this query. 
     *
     *  @param  supersedingEventId a superseding event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supersedingEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the superseding event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available for 
     *  querying superseding events. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Matches a recurring event that has any superseding event assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          superseding events, <code> false </code> to match events with 
     *          no superseding events 
     */

    @OSID @Override
    public void matchAnySupersedingEvent(boolean match) {
        return;
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        return;
    }


    /**
     *  Matches recurring events with specific dates between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchSpecificMeetingTime(org.osid.calendaring.DateTime start, 
                                         org.osid.calendaring.DateTime end, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches a recurring event that has any specific date assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          specific date, <code> false </code> to match recurring events 
     *          with no specific date 
     */

    @OSID @Override
    public void matchAnySpecificMeetingTime(boolean match) {
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearSpecificMeetingTimeTerms() {
        return;
    }


    /**
     *  Sets the composed event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  composed events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a recurring event that has any composed event assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          composed events, <code> false </code> to match events with no 
     *          composed events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        return;
    }


    /**
     *  Matches a blackout that contains the given date time. 
     *
     *  @param  datetime a datetime 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> datetime </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBlackout(org.osid.calendaring.DateTime datetime, 
                              boolean match) {
        return;
    }


    /**
     *  Matches a recurring event that has any blackout assigned. 
     *
     *  @param  match <code> true </code> to match recurring events with any 
     *          blackout, <code> false </code> to match recurring events with 
     *          no blackout 
     */

    @OSID @Override
    public void matchAnyBlackout(boolean match) {
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutTerms() {
        return;
    }


    /**
     *  Matches recurring events with blackouts between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchBlackoutInclusive(org.osid.calendaring.DateTime start, 
                                       org.osid.calendaring.DateTime end, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutInclusiveTerms() {
        return;
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given recurring event query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recurring event implementing the requested record.
     *
     *  @param recurringEventRecordType a recurring event record type
     *  @return the recurring event query record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventQueryRecord getRecurringEventQueryRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventQueryRecord record : this.records) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event query. 
     *
     *  @param recurringEventQueryRecord recurring event query record
     *  @param recurringEventRecordType recurringEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventQueryRecord(org.osid.calendaring.records.RecurringEventQueryRecord recurringEventQueryRecord, 
                                          org.osid.type.Type recurringEventRecordType) {

        addRecordType(recurringEventRecordType);
        nullarg(recurringEventQueryRecord, "recurring event query record");
        this.records.add(recurringEventQueryRecord);        
        return;
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {

    }
}
