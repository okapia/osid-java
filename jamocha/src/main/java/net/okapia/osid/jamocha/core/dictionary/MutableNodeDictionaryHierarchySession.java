//
// MutableNodeDictionaryHierarchySession.java
//
//     Defines a Dictionary hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Defines a dictionary hierarchy session for delivering a hierarchy
 *  of dictionaries using the DictionaryNode interface.
 */

public final class MutableNodeDictionaryHierarchySession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractNodeDictionaryHierarchySession
    implements org.osid.dictionary.DictionaryHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeDictionaryHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDictionaryHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeDictionaryHierarchySession(org.osid.dictionary.DictionaryNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getDictionary().getDisplayName())
                     .description(root.getDictionary().getDescription())
                     .build());

        addRootDictionary(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDictionaryHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.dictionary.DictionaryNode root) {
        setHierarchy(hierarchy);
        addRootDictionary(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeDictionaryHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeDictionaryHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.dictionary.DictionaryNode> roots) {
        setHierarchy(hierarchy);
        addRootDictionaries(roots);
        return;
    }


    /**
     *  Adds a root dictionary node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootDictionary(org.osid.dictionary.DictionaryNode root) {
        super.addRootDictionary(root);
        return;
    }


    /**
     *  Adds a collection of root dictionary nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootDictionaries(java.util.Collection<org.osid.dictionary.DictionaryNode> roots) {
        super.addRootDictionaries(roots);
        return;
    }


    /**
     *  Removes a root dictionary node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootDictionary(org.osid.id.Id rootId) {
        super.removeRootDictionary(rootId);
        return;
    }
}
