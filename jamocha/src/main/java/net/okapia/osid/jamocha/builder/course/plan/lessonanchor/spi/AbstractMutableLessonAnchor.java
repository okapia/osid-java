//
// AbstractMutableLessonAnchor.java
//
//     Defines a mutable LessonAnchor.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.plan.lessonanchor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>LessonAnchor</code>.
 */

public abstract class AbstractMutableLessonAnchor
    extends net.okapia.osid.jamocha.course.plan.lessonanchor.spi.AbstractLessonAnchor
    implements org.osid.course.plan.LessonAnchor,
               net.okapia.osid.jamocha.builder.course.plan.lessonanchor.LessonAnchorMiter {


    /**
     *  Sets the lesson.
     *
     *  @param lesson the lesson
     *  @throws org.osid.NullArgumentException <code>lesson</code> is
     *          <code>null</code>
     */

    @Override
    public void setLesson(org.osid.course.plan.Lesson lesson) {
        super.setLesson(lesson);
        return;
    }


    /**
     *  Sets the activity.
     *
     *  @param activity the activity
     *  @throws org.osid.NullArgumentException <code>activity</code> is
     *          <code>null</code>
     */

    @Override
    public void setActivity(org.osid.course.Activity activity) {
        super.setActivity(activity);
        return;
    }


    /**
     *  Sets the time offset.
     *
     *  @param duration the duration from the start of the activity
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    @Override
    public void setTime(org.osid.calendaring.Duration duration) {
        super.setTime(duration);
        return;
    }
}


