//
// AbstractMutableCourse.java
//
//     Defines a mutable Course.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Course</code>.
 */

public abstract class AbstractMutableCourse
    extends net.okapia.osid.jamocha.course.course.spi.AbstractCourse
    implements org.osid.course.Course,
               net.okapia.osid.jamocha.builder.course.course.CourseMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this course. 
     *
     *  @param record course record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addCourseRecord(org.osid.course.records.CourseRecord record, org.osid.type.Type recordType) {
        super.addCourseRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this course. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled,
     *         <code>false<code> otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this course. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this course.
     *
     *  @param displayName the name for this course
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this course.
     *
     *  @param description the description of this course
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    @Override
    public void setNumber(String number) {
        super.setNumber(number);
        return;
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    @Override
    public void addSponsor(org.osid.resource.Resource sponsor) {
        super.addSponsor(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    @Override
    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        super.setSponsors(sponsors);
        return;
    }


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void addCreditAmount(org.osid.grading.Grade amount) {
        super.addCreditAmount(amount);
        return;
    }


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    @Override
    public void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts) {
        super.setCreditAmounts(amounts);
        return;
    }


    /**
     *  Sets the prerequisites info.
     *
     *  @param info a prerequisites info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    @Override
    public void setPrerequisitesInfo(org.osid.locale.DisplayText info) {
        super.setPrerequisitesInfo(info);
        return;
    }


    /**
     *  Adds a prerequisite.
     *
     *  @param prerequisite a prerequisite
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisite</code> is <code>null</code>
     */

    @Override
    public void addPrerequisite(org.osid.course.requisite.Requisite prerequisite) {
        super.addPrerequisite(prerequisite);
        return;
    }


    /**
     *  Sets all the prerequisites.
     *
     *  @param prerequisites a collection of prerequisites
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisites</code> is <code>null</code>
     */

    @Override
    public void setPrerequisites(java.util.Collection<org.osid.course.requisite.Requisite> prerequisites) {
        super.setPrerequisites(prerequisites);
        return;
    }


    /**
     *  Adds a level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    @Override
    public void addLevel(org.osid.grading.Grade level) {
        super.addLevel(level);
        return;
    }


    /**
     *  Sets all the levels.
     *
     *  @param levels a collection of levels
     *  @throws org.osid.NullArgumentException <code>levels</code> is
     *          <code>null</code>
     */

    @Override
    public void setLevels(java.util.Collection<org.osid.grading.Grade> levels) {
        super.setLevels(levels);
        return;
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    @Override
    public void addGradingOption(org.osid.grading.GradeSystem option) {
        super.addGradingOption(option);
        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    @Override
    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        super.setGradingOptions(options);
        return;
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    @Override
    public void addLearningObjective(org.osid.learning.Objective objective) {
        super.addLearningObjective(objective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    @Override
    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        super.setLearningObjectives(objectives);
        return;
    }
}

