//
// TemporalCommitmentEnablerFilterList.java
//
//     Implements a filter for Temporal objects.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for Temporal objects.
 */

public final class TemporalCommitmentEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.calendaring.rules.commitmentenabler.spi.AbstractCommitmentEnablerFilterList
    implements org.osid.calendaring.rules.CommitmentEnablerList,
               CommitmentEnablerFilter {

    private final org.osid.calendaring.DateTime start;
    private final org.osid.calendaring.DateTime end;


    /**
     *  Creates a new
     *  <code>TemporalCommitmentEnablerFilterList</code>. Temporals pass the
     *  filter if the given date range falls inside the start and end
     *  dates inclusive.
     *
     *  @param list a <code>CommitmentEnablerList</code>
     *  @param from start of date range
     *  @param to end of date range
     *  @throws org.osid.NullArgumentException <code>list</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     */

    public TemporalCommitmentEnablerFilterList(org.osid.calendaring.rules.CommitmentEnablerList list, 
                                      org.osid.calendaring.DateTime from,
                                      org.osid.calendaring.DateTime to) {
        super(list);

        nullarg(from, "start date");
        nullarg(to, "end date");

        this.start = from;
        this.end = to;

        return;
    }    

    
    /**
     *  Filters CommitmentEnablers.
     *
     *  @param commitmentEnabler the commitment enabler to filter
     *  @return <code>true</code> if the commitment enabler passes the filter,
     *          <code>false</code> if the commitment enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler) {
        if (commitmentEnabler.getStartDate().isGreater(this.start)) {
            return (false);
        }

        if (commitmentEnabler.getEndDate().isLess(this.end)) {
            return (false);
        }

        return (true);
    }
}
