//
// AbstractBrokerConstrainer.java
//
//     Defines a BrokerConstrainer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainer.spi;


/**
 *  Defines a <code>BrokerConstrainer</code> builder.
 */

public abstract class AbstractBrokerConstrainerBuilder<T extends AbstractBrokerConstrainerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidConstrainerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainer.BrokerConstrainerMiter brokerConstrainer;


    /**
     *  Constructs a new <code>AbstractBrokerConstrainerBuilder</code>.
     *
     *  @param brokerConstrainer the broker constrainer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBrokerConstrainerBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainer.BrokerConstrainerMiter brokerConstrainer) {
        super(brokerConstrainer);
        this.brokerConstrainer = brokerConstrainer;
        return;
    }


    /**
     *  Builds the broker constrainer.
     *
     *  @return the new broker constrainer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.BrokerConstrainer build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.brokerconstrainer.BrokerConstrainerValidator(getValidations())).validate(this.brokerConstrainer);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainer.ImmutableBrokerConstrainer(this.brokerConstrainer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the broker constrainer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.brokerconstrainer.BrokerConstrainerMiter getMiter() {
        return (this.brokerConstrainer);
    }


    /**
     *  Adds a BrokerConstrainer record.
     *
     *  @param record a broker constrainer record
     *  @param recordType the type of broker constrainer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.BrokerConstrainerRecord record, org.osid.type.Type recordType) {
        getMiter().addBrokerConstrainerRecord(record, recordType);
        return (self());
    }
}       


