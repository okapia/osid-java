//
// FiscalPeriodMiter.java
//
//     Defines a FiscalPeriod miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.fiscalperiod;


/**
 *  Defines a <code>FiscalPeriod</code> miter for use with the builders.
 */

public interface FiscalPeriodMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.financials.FiscalPeriod {


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public void setDisplayLabel(org.osid.locale.DisplayText label);


    /**
     *  Sets the fiscal year.
     *
     *  @param year a fiscal year
     *  @throws org.osid.NullArgumentException <code>year</code> is
     *          <code>null</code>
     */

    public void setFiscalYear(long year);


    /**
     *  Sets the start date.
     *
     *  @param date a start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setStartDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the end date.
     *
     *  @param date an end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setEndDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the budget deadline.
     *
     *  @param date a budget deadline
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setBudgetDeadline(org.osid.calendaring.DateTime date);


    /**
     *  Sets the posting deadline.
     *
     *  @param date a posting deadline
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setPostingDeadline(org.osid.calendaring.DateTime date);


    /**
     *  Sets the closing.
     *
     *  @param date the closing date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    public void setClosing(org.osid.calendaring.DateTime date);


    /**
     *  Adds a FiscalPeriod record.
     *
     *  @param record a fiscalPeriod record
     *  @param recordType the type of fiscalPeriod record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addFiscalPeriodRecord(org.osid.financials.records.FiscalPeriodRecord record, org.osid.type.Type recordType);
}       


