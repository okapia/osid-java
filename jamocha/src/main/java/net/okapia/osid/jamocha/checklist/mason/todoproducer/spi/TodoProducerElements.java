//
// TodoProducerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class TodoProducerElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the TodoProducerElement Id.
     *
     *  @return the todo producer element Id
     */

    public static org.osid.id.Id getTodoProducerEntityId() {
        return (makeEntityId("osid.checklist.mason.TodoProducer"));
    }


    /**
     *  Gets the CyclicEventId element Id.
     *
     *  @return the CyclicEventId element Id
     */

    public static org.osid.id.Id getCyclicEventId() {
        return (makeElementId("osid.checklist.mason.todoproducer.CyclicEventId"));
    }


    /**
     *  Gets the CyclicEvent element Id.
     *
     *  @return the CyclicEvent element Id
     */

    public static org.osid.id.Id getCyclicEvent() {
        return (makeElementId("osid.checklist.mason.todoproducer.CyclicEvent"));
    }


    /**
     *  Gets the StockLevel element Id.
     *
     *  @return the StockLevel element Id
     */

    public static org.osid.id.Id getStockLevel() {
        return (makeElementId("osid.checklist.mason.todoproducer.StockLevel"));
    }


    /**
     *  Gets the StockId element Id.
     *
     *  @return the StockId element Id
     */

    public static org.osid.id.Id getStockId() {
        return (makeElementId("osid.checklist.mason.todoproducer.StockId"));
    }


    /**
     *  Gets the Stock element Id.
     *
     *  @return the Stock element Id
     */

    public static org.osid.id.Id getStock() {
        return (makeElementId("osid.checklist.mason.todoproducer.Stock"));
    }


    /**
     *  Gets the CreationRule element Id.
     *
     *  @return the CreationRule element Id
     */

    public static org.osid.id.Id getCreationRule() {
        return (makeElementId("osid.checklist.mason.todoproducer.CreationRule"));
    }


    /**
     *  Gets the ProducedTodoId element Id.
     *
     *  @return the ProducedTodoId element Id
     */

    public static org.osid.id.Id getProducedTodoId() {
        return (makeQueryElementId("osid.checklist.mason.todoproducer.ProducedTodoId"));
    }


    /**
     *  Gets the ProducedTodo element Id.
     *
     *  @return the ProducedTodo element Id
     */

    public static org.osid.id.Id getProducedTodo() {
        return (makeQueryElementId("osid.checklist.mason.todoproducer.ProducedTodo"));
    }


    /**
     *  Gets the ChecklistId element Id.
     *
     *  @return the ChecklistId element Id
     */

    public static org.osid.id.Id getChecklistId() {
        return (makeQueryElementId("osid.checklist.mason.todoproducer.ChecklistId"));
    }


    /**
     *  Gets the Checklist element Id.
     *
     *  @return the Checklist element Id
     */

    public static org.osid.id.Id getChecklist() {
        return (makeQueryElementId("osid.checklist.mason.todoproducer.Checklist"));
    }
}
