//
// AbstractRelationshipEnablerSearch.java
//
//     A template for making a RelationshipEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.rules.relationshipenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing relationship enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRelationshipEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.relationship.rules.RelationshipEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.relationship.rules.records.RelationshipEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.relationship.rules.RelationshipEnablerSearchOrder relationshipEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of relationship enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  relationshipEnablerIds list of relationship enablers
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRelationshipEnablers(org.osid.id.IdList relationshipEnablerIds) {
        while (relationshipEnablerIds.hasNext()) {
            try {
                this.ids.add(relationshipEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRelationshipEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of relationship enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRelationshipEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  relationshipEnablerSearchOrder relationship enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>relationshipEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRelationshipEnablerResults(org.osid.relationship.rules.RelationshipEnablerSearchOrder relationshipEnablerSearchOrder) {
	this.relationshipEnablerSearchOrder = relationshipEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.relationship.rules.RelationshipEnablerSearchOrder getRelationshipEnablerSearchOrder() {
	return (this.relationshipEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given relationship enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relationship enabler implementing the requested record.
     *
     *  @param relationshipEnablerSearchRecordType a relationship enabler search record
     *         type
     *  @return the relationship enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.rules.records.RelationshipEnablerSearchRecord getRelationshipEnablerSearchRecord(org.osid.type.Type relationshipEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.relationship.rules.records.RelationshipEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(relationshipEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship enabler search. 
     *
     *  @param relationshipEnablerSearchRecord relationship enabler search record
     *  @param relationshipEnablerSearchRecordType relationshipEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipEnablerSearchRecord(org.osid.relationship.rules.records.RelationshipEnablerSearchRecord relationshipEnablerSearchRecord, 
                                           org.osid.type.Type relationshipEnablerSearchRecordType) {

        addRecordType(relationshipEnablerSearchRecordType);
        this.records.add(relationshipEnablerSearchRecord);        
        return;
    }
}
