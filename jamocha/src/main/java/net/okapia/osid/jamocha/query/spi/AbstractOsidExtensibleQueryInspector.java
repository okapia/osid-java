//
// AbstractOsidExtensibleQueryInspector.java
//
//     Defines an OsidExtensibleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidExtensibleQueryInspector
    extends AbstractOsidQueryInspector
    implements org.osid.OsidExtensibleQueryInspector {

    private final java.util.Collection<org.osid.search.terms.TypeTerm> typeTerms = new java.util.LinkedHashSet<>();
    private final Extensible extensible = new Extensible();


    /**
     *  Gets the record type query terms.
     *
     *  @return the record type terms
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getRecordTypeTerms() {
        return (this.typeTerms.toArray(new org.osid.search.terms.TypeTerm[this.typeTerms.size()]));
    }

    
    /**
     *  Adds a record type term.
     *
     *  @param term a record type term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addRecordTypeTerm(org.osid.search.terms.TypeTerm term) {
        nullarg(term, "type term");
        this.typeTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of record type terms.
     *
     *  @param terms a collection of type terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addRecordTypeTerms(java.util.Collection<org.osid.search.terms.TypeTerm> terms) {
        nullarg(terms, "type terms");
        this.typeTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a record type term.
     *
     *  @param type the type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    protected void addRecordTypeTerm(org.osid.type.Type type, boolean match) {
        this.typeTerms.add(new net.okapia.osid.primordium.terms.TypeTerm(type, match));
        return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.extensible.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.extensible.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.extensible.addRecordType(recordType);
        return;
    }

    
    protected class Extensible
        extends net.okapia.osid.jamocha.spi.AbstractExtensible 
        implements org.osid.Extensible {


        /**
         *  Gets the record types available in this object.
         *
         *  @return the record types
         */

        @OSID @Override
        public org.osid.type.TypeList getRecordTypes() {
            return (super.getRecordTypes());
        }


        /**
         *  Tests if this object supports the given record <code>
         *  Type. </code>
         *
         *  @param  recordType a type 
         *  @return <code>true</code> if <code>recordType</code> is
         *          supported, <code> false </code> otherwise
         *  @throws org.osid.NullArgumentException <code> recordType </code> is 
         *          <code> null </code> 
         */

        @OSID @Override
        public boolean hasRecordType(org.osid.type.Type recordType) {
            return (super.hasRecordType(recordType));
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */

        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
        }
    }
}
