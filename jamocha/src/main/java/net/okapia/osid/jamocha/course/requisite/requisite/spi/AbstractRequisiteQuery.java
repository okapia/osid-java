//
// AbstractRequisiteQuery.java
//
//     A template for making a Requisite Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for requisites.
 */

public abstract class AbstractRequisiteQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.course.requisite.RequisiteQuery {

    private final java.util.Collection<org.osid.course.requisite.records.RequisiteQueryRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Sets the requisite <code> Id </code> for this query to match 
     *  requisites that a requisite option. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequisiteOptionId(org.osid.id.Id requisiteId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRequisiteOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequisiteQuery </code> is available. 
     *
     *  @return <code> true </code> if a requisite query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requisite option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getRequisiteOptionQuery() {
        throw new org.osid.UnimplementedException("supportsRequisiteOptionQuery() is false");
    }


    /**
     *  Matches requisites that have any requisite option. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          requisite option, <code> false </code> to match requisites 
     *          with no requisite options 
     */

    @OSID @Override
    public void matchAnyRequisiteOption(boolean match) {
        return;
    }


    /**
     *  Clears the requisite option terms. 
     */

    @OSID @Override
    public void clearRequisiteOptionTerms() {
        return;
    }


    /**
     *  Sets the course <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course requirement. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches requisites that have any course requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any course 
     *          requirement, <code> false </code> to match requisites with no 
     *          course requirements 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets the program <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given program. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program requirement. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return a program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches requisites that have any program requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any program 
     *          requirement, <code> false </code> to match requisites with no 
     *          program requirements 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        return;
    }


    /**
     *  Sets the credential <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given credential. 
     *
     *  @param  credentialId a credential <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> credentialId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCredentialId(org.osid.id.Id credentialId, boolean match) {
        return;
    }


    /**
     *  Clears the credential <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCredentialIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CredentialQuery </code> is available. 
     *
     *  @return <code> true </code> if a credential query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credential requirement. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return a credential query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuery getCredentialQuery() {
        throw new org.osid.UnimplementedException("supportsCredentialQuery() is false");
    }


    /**
     *  Matches requisites that have any credential requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          credential requirement, <code> false </code> to match 
     *          requisites with no credential requirements 
     */

    @OSID @Override
    public void matchAnyCredential(boolean match) {
        return;
    }


    /**
     *  Clears the credential terms. 
     */

    @OSID @Override
    public void clearCredentialTerms() {
        return;
    }


    /**
     *  Sets the learning objective <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given learning objective. 
     *
     *  @param  objectiveId a learning objective <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the learning objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LearningObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if a learning objective query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for a learning objective requirement. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return a learning objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches requisites that have any learning objective requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          learning objective requirement, <code> false </code> to match 
     *          requisites with no learning objective requirements 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        return;
    }


    /**
     *  Clears the learning objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        return;
    }


    /**
     *  Sets the assessment <code> Id </code> for this query to match 
     *  requisites that have a requirement for the given assessment. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a assessment requirement. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return an assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches requisites that have any assessment requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          assessment requirement, <code> false </code> to match 
     *          requisites with no assessment requirements 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match requisites 
     *  that have a requirement for the given award. 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for a award requirement. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return an award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches requisites that have any award requirement. 
     *
     *  @param  match <code> true </code> to match requisites with any award 
     *          requirement, <code> false </code> to match requisites with no 
     *          award requirements 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        return;
    }


    /**
     *  Sets the requisite <code> Id </code> for this query to match 
     *  requisites that have the specified requisite as an ancestor. 
     *
     *  @param  requisiteId a requisite <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requisiteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingRequisiteId(org.osid.id.Id requisiteId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the containing requisite <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingRequisiteIdTerms() {
        return;
    }


    /**
     *  Tests if a containing requisite query is available. 
     *
     *  @return <code> true </code> if a containing requisite query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingRequisiteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing requisite. 
     *
     *  @return the containing requisite query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingRequisiteQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getContainingRequisiteQuery() {
        throw new org.osid.UnimplementedException("supportsContainingRequisiteQuery() is false");
    }


    /**
     *  Matches requisites with any ancestor requisite. 
     *
     *  @param  match <code> true </code> to match requisites with any 
     *          ancestor requisite, <code> false </code> to match requisites 
     *          with no ancestor requisites 
     */

    @OSID @Override
    public void matchAnyContainingRequisite(boolean match) {
        return;
    }


    /**
     *  Clears the containing requisite terms. 
     */

    @OSID @Override
    public void clearContainingRequisiteTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  requisites assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given requisite query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a requisite implementing the requested record.
     *
     *  @param requisiteRecordType a requisite record type
     *  @return the requisite query record
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requisiteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteQueryRecord getRequisiteQueryRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.RequisiteQueryRecord record : this.records) {
            if (record.implementsRecordType(requisiteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requisiteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this requisite query. 
     *
     *  @param requisiteQueryRecord requisite query record
     *  @param requisiteRecordType requisite record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRequisiteQueryRecord(org.osid.course.requisite.records.RequisiteQueryRecord requisiteQueryRecord, 
                                           org.osid.type.Type requisiteRecordType) {

        addRecordType(requisiteRecordType);
        nullarg(requisiteQueryRecord, "requisite query record");
        this.records.add(requisiteQueryRecord);        
        return;
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {

    }
}
