//
// AbstractCookbookQueryInspector.java
//
//     A template for making a CookbookQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.cookbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for cookbooks.
 */

public abstract class AbstractCookbookQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.recipe.CookbookQueryInspector {

    private final java.util.Collection<org.osid.recipe.records.CookbookQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the recipe <code> Id </code> terms. 
     *
     *  @return the recipe <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recipe terms. 
     *
     *  @return the recipe terms 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQueryInspector[] getRecipeTerms() {
        return (new org.osid.recipe.RecipeQueryInspector[0]);
    }


    /**
     *  Gets the procedure <code> Id </code> terms. 
     *
     *  @return the procedure <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcedureIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the procedure terms. 
     *
     *  @return the procedure terms 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQueryInspector[] getProcedureTerms() {
        return (new org.osid.recipe.ProcedureQueryInspector[0]);
    }


    /**
     *  Gets the direction <code> Id </code> terms. 
     *
     *  @return the direction <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDirectionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the direction terms. 
     *
     *  @return the direction terms 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQueryInspector[] getDirectionTerms() {
        return (new org.osid.recipe.DirectionQueryInspector[0]);
    }


    /**
     *  Gets the ancestor cook book <code> Id </code> terms. 
     *
     *  @return the ancestor cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCookbookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor cook book terms. 
     *
     *  @return the ancestor cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getAncestorCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the descendant cook book <code> Id </code> terms. 
     *
     *  @return the descendant cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCookbookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant cook book terms. 
     *
     *  @return the descendant cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getDescendantCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given cookbook query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a cookbook implementing the requested record.
     *
     *  @param cookbookRecordType a cookbook record type
     *  @return the cookbook query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cookbookRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.CookbookQueryInspectorRecord getCookbookQueryInspectorRecord(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.CookbookQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(cookbookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cookbookRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cookbook query. 
     *
     *  @param cookbookQueryInspectorRecord cookbook query inspector
     *         record
     *  @param cookbookRecordType cookbook record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCookbookQueryInspectorRecord(org.osid.recipe.records.CookbookQueryInspectorRecord cookbookQueryInspectorRecord, 
                                                   org.osid.type.Type cookbookRecordType) {

        addRecordType(cookbookRecordType);
        nullarg(cookbookRecordType, "cookbook record type");
        this.records.add(cookbookQueryInspectorRecord);        
        return;
    }
}
