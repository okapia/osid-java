//
// AbstractDispatchLookupSession.java
//
//    A starter implementation framework for providing a Dispatch
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Dispatch
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDispatches(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDispatchLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.DispatchLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();
    

    /**
     *  Gets the <code>Publisher/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Publisher Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPublisherId() {
        return (this.publisher.getId());
    }


    /**
     *  Gets the <code>Publisher</code> associated with this session.
     *
     *  @return the <code>Publisher</code> associated with this
     *          session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.publisher);
    }


    /**
     *  Sets the <code>Publisher</code>.
     *
     *  @param  publisher the publisher for this session
     *  @throws org.osid.NullArgumentException <code>publisher</code>
     *          is <code>null</code>
     */

    protected void setPublisher(org.osid.subscription.Publisher publisher) {
        nullarg(publisher, "publisher");
        this.publisher = publisher;
        return;
    }


    /**
     *  Tests if this user can perform <code>Dispatch</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDispatches() {
        return (true);
    }


    /**
     *  A complete view of the <code>Dispatch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDispatchView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Dispatch</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDispatchView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include dispatches in publishers which are children
     *  of this publisher in the publisher hierarchy.
     */

    @OSID @Override
    public void useFederatedPublisherView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this publisher only.
     */

    @OSID @Override
    public void useIsolatedPublisherView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active dispatches are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveDispatchView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive dispatches are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDispatchView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Dispatch</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Dispatch</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Dispatch</code> and
     *  retained for compatibility.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches
     *  are returned.
     *
     *  @param  dispatchId <code>Id</code> of the
     *          <code>Dispatch</code>
     *  @return the dispatch
     *  @throws org.osid.NotFoundException <code>dispatchId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Dispatch getDispatch(org.osid.id.Id dispatchId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.subscription.DispatchList dispatches = getDispatches()) {
            while (dispatches.hasNext()) {
                org.osid.subscription.Dispatch dispatch = dispatches.getNextDispatch();
                if (dispatch.getId().equals(dispatchId)) {
                    return (dispatch);
                }
            }
        } 

        throw new org.osid.NotFoundException(dispatchId + " not found");
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  dispatches specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Dispatches</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDispatches()</code>.
     *
     *  @param dispatchIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByIds(org.osid.id.IdList dispatchIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.subscription.Dispatch> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = dispatchIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDispatch(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("dispatch " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.subscription.dispatch.LinkedDispatchList(ret));
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  dispatch genus <code>Type</code> which does not include
     *  dispatches of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDispatches()</code>.
     *
     *  @param dispatchGenusType a dispatch genus type
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.dispatch.DispatchGenusFilterList(getDispatches(), dispatchGenusType));
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  dispatch genus <code>Type</code> and include any additional
     *  dispatches with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDispatches()</code>.
     *
     *  @param dispatchGenusType a dispatch genus type
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByParentGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDispatchesByGenusType(dispatchGenusType));
    }


    /**
     *  Gets a <code>DispatchList</code> containing the given dispatch
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDispatches()</code>.
     *
     *  @param dispatchRecordType a dispatch record type
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByRecordType(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.dispatch.DispatchRecordFilterList(getDispatches(), dispatchRecordType));
    }


    /**
     *  Gets a <code>DispatchList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Dispatch</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.subscription.dispatch.DispatchProviderFilterList(getDispatches(), resourceId));
    }


    /**
     *  Gets all <code>Dispatches</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dispatches or an error results. Otherwise, the returned list
     *  may contain only those dispatches that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, dispatches are returned that are currently
     *  active. In any status mode, active and inactive dispatches are
     *  returned.
     *
     *  @return a list of <code>Dispatches</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.subscription.DispatchList getDispatches()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the dispatch list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of dispatches
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.subscription.DispatchList filterDispatchesOnViews(org.osid.subscription.DispatchList list)
        throws org.osid.OperationFailedException {

        org.osid.subscription.DispatchList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.subscription.dispatch.ActiveDispatchFilterList(ret);
        }

        return (ret);
    }
}
