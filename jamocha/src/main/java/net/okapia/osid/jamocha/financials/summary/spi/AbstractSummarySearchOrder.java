//
// AbstractSummarySearchOdrer.java
//
//     Defines a SummarySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SummarySearchOrder}.
 */

public abstract class AbstractSummarySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumSearchOrder
    implements org.osid.financials.SummarySearchOrder {

    private final java.util.Collection<org.osid.financials.records.SummarySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the account. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAccount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> AccountSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an account search order is available, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsAccountSearchOrder() {
        return (false);
    }


    /**
     *  Gets the account search order. 
     *
     *  @return the account search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAccountSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAccountSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the fiscal 
     *  period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalPeriod(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> FiscalPeriodSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period search order is 
     *          available, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the fiscal period search order. 
     *
     *  @return the fiscal period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchOrder getFiscalPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the credits. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCredits(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the debits. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDebits(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the balance. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBalance(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the budget 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBudget(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the delta. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDelta(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the forecast. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByForecast(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  summaryRecordType a summary record type 
     *  @return {@code true} if the summaryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code summaryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type summaryRecordType) {
        for (org.osid.financials.records.SummarySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  summaryRecordType the summary record type 
     *  @return the summary search order record
     *  @throws org.osid.NullArgumentException
     *          {@code summaryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(summaryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.financials.records.SummarySearchOrderRecord getSummarySearchOrderRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummarySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this summary. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param summaryRecord the summary search odrer record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException
     *          {@code summaryRecord} or
     *          {@code summaryRecordTypesummary} is
     *          {@code null}
     */
            
    protected void addSummaryRecord(org.osid.financials.records.SummarySearchOrderRecord summarySearchOrderRecord, 
                                     org.osid.type.Type summaryRecordType) {

        addRecordType(summaryRecordType);
        this.records.add(summarySearchOrderRecord);
        
        return;
    }
}
