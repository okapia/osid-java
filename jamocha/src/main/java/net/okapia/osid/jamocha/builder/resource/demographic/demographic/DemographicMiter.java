//
// DemographicMiter.java
//
//     Defines a Demographic miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographic;


/**
 *  Defines a <code>Demographic</code> miter for use with the builders.
 */

public interface DemographicMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.resource.demographic.Demographic {


    /**
     *  Adds an included demographic.
     *
     *  @param demographic an included demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public void addIncludedDemographic(org.osid.resource.demographic.Demographic demographic);


    /**
     *  Sets all the included demographics.
     *
     *  @param demographics a collection of included demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public void setIncludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics);


    /**
     *  Adds an included intersecting demographic.
     *
     *  @param demographic an included intersecting demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public void addIncludedIntersectingDemographic(org.osid.resource.demographic.Demographic demographic);


    /**
     *  Sets all the included intersecting demographics.
     *
     *  @param demographics a collection of included intersecting demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public void setIncludedIntersectingDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics);


    /**
     *  Adds an included exclusive demographic.
     *
     *  @param demographic an included exclusive demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public void addIncludedExclusiveDemographic(org.osid.resource.demographic.Demographic demographic);


    /**
     *  Sets all the included exclusive demographics.
     *
     *  @param demographics a collection of included exclusive demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public void setIncludedExclusiveDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics);


    /**
     *  Adds an excluded demographic.
     *
     *  @param demographic an excluded demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    public void addExcludedDemographic(org.osid.resource.demographic.Demographic demographic);


    /**
     *  Sets all the excluded demographics.
     *
     *  @param demographics a collection of excluded demographics
     *  @throws org.osid.NullArgumentException
     *          <code>demographics</code> is <code>null</code>
     */

    public void setExcludedDemographics(java.util.Collection<org.osid.resource.demographic.Demographic> demographics);


    /**
     *  Adds an included resource.
     *
     *  @param resource an included resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public void addIncludedResource(org.osid.resource.Resource resource);


    /**
     *  Sets all the included resources.
     *
     *  @param resources a collection of included resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    public void setIncludedResources(java.util.Collection<org.osid.resource.Resource> resources);


    /**
     *  Adds an excluded resource.
     *
     *  @param resource an excluded resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public void addExcludedResource(org.osid.resource.Resource resource);


    /**
     *  Sets all the excluded resources.
     *
     *  @param resources a collection of excluded resources
     *  @throws org.osid.NullArgumentException
     *          <code>resources</code> is <code>null</code>
     */

    public void setExcludedResources(java.util.Collection<org.osid.resource.Resource> resources);


    /**
     *  Adds a Demographic record.
     *
     *  @param record a demographic record
     *  @param recordType the type of demographic record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addDemographicRecord(org.osid.resource.demographic.records.DemographicRecord record, org.osid.type.Type recordType);
}       


