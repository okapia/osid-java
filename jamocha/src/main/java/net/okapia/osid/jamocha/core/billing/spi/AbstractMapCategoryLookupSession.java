//
// AbstractMapCategoryLookupSession
//
//    A simple framework for providing a Category lookup service
//    backed by a fixed collection of categories.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Category lookup service backed by a
 *  fixed collection of categories. The categories are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Categories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCategoryLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractCategoryLookupSession
    implements org.osid.billing.CategoryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.billing.Category> categories = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.billing.Category>());


    /**
     *  Makes a <code>Category</code> available in this session.
     *
     *  @param  category a category
     *  @throws org.osid.NullArgumentException <code>category<code>
     *          is <code>null</code>
     */

    protected void putCategory(org.osid.billing.Category category) {
        this.categories.put(category.getId(), category);
        return;
    }


    /**
     *  Makes an array of categories available in this session.
     *
     *  @param  categories an array of categories
     *  @throws org.osid.NullArgumentException <code>categories<code>
     *          is <code>null</code>
     */

    protected void putCategories(org.osid.billing.Category[] categories) {
        putCategories(java.util.Arrays.asList(categories));
        return;
    }


    /**
     *  Makes a collection of categories available in this session.
     *
     *  @param  categories a collection of categories
     *  @throws org.osid.NullArgumentException <code>categories<code>
     *          is <code>null</code>
     */

    protected void putCategories(java.util.Collection<? extends org.osid.billing.Category> categories) {
        for (org.osid.billing.Category category : categories) {
            this.categories.put(category.getId(), category);
        }

        return;
    }


    /**
     *  Removes a Category from this session.
     *
     *  @param  categoryId the <code>Id</code> of the category
     *  @throws org.osid.NullArgumentException <code>categoryId<code> is
     *          <code>null</code>
     */

    protected void removeCategory(org.osid.id.Id categoryId) {
        this.categories.remove(categoryId);
        return;
    }


    /**
     *  Gets the <code>Category</code> specified by its <code>Id</code>.
     *
     *  @param  categoryId <code>Id</code> of the <code>Category</code>
     *  @return the category
     *  @throws org.osid.NotFoundException <code>categoryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>categoryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory(org.osid.id.Id categoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.billing.Category category = this.categories.get(categoryId);
        if (category == null) {
            throw new org.osid.NotFoundException("category not found: " + categoryId);
        }

        return (category);
    }


    /**
     *  Gets all <code>Categories</code>. In plenary mode, the returned
     *  list contains all known categories or an error
     *  results. Otherwise, the returned list may contain only those
     *  categories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Categories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.category.ArrayCategoryList(this.categories.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.categories.clear();
        super.close();
        return;
    }
}
