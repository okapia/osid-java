//
// AbstractImmutableAssetContent.java
//
//     Wraps a mutable AssetContent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssetContent</code> to hide modifiers. This
 *  wrapper provides an immutized AssetContent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assetContent whose state changes are visible.
 */

public abstract class AbstractImmutableAssetContent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.repository.AssetContent {

    private final org.osid.repository.AssetContent assetContent;


    /**
     *  Constructs a new <code>AbstractImmutableAssetContent</code>.
     *
     *  @param assetContent the asset content to immutablize
     *  @throws org.osid.NullArgumentException <code>assetContent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssetContent(org.osid.repository.AssetContent assetContent) {
        super(assetContent);
        this.assetContent = assetContent;
        return;
    }


    /**
     *  Gets the <code> Asset Id </code> corresponding to this content. 
     *
     *  @return the asset <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssetId() {
        return (this.assetContent.getAssetId());
    }


    /**
     *  Gets the <code> Asset </code> corresponding to this content. 
     *
     *  @return the asset 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset() {
        return (this.assetContent.getAsset());
    }


    /**
     *  Gets the accessibility types associated with this content. 
     *
     *  @return list of content accessibility types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccessibilityTypes() {
        return (this.assetContent.getAccessibilityTypes());
    }


    /**
     *  Tests if a data length is available. 
     *
     *  @return <code> true </code> if a length is available for this content, 
     *          <code> false </code> otherwise. 
     */

    @OSID @Override
    public boolean hasDataLength() {
        return (this.assetContent.hasDataLength());
    }


    /**
     *  Gets the length of the data represented by this content in bytes. 
     *
     *  @return the length of the data stream 
     *  @throws org.osid.IllegalStateException <code> hasDataLength() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getDataLength() {
        return (this.assetContent.getDataLength());
    }


    /**
     *  Gets the asset content data. 
     *
     *  @return the length of the content data 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.transport.DataInputStream getData()
        throws org.osid.OperationFailedException {

        return (this.assetContent.getData());
    }


    /**
     *  Tests if a URL is associated with this content. 
     *
     *  @return <code> true </code> if a URL is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasURL() {
        return (this.assetContent.hasURL());
    }


    /**
     *  Gets the URL associated with this content for web-based retrieval. 
     *
     *  @return the url for this data 
     *  @throws org.osid.IllegalStateException <code> hasURL() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public String getURL() {
        return (this.assetContent.getURL());
    }


    /**
     *  Gets the asset content record corresponding to the given <code> 
     *  AssetContent </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  assetRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(assetRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  assetContentContentRecordType the type of the record to 
     *          retrieve 
     *  @return the asset content record 
     *  @throws org.osid.NullArgumentException <code> assetContentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assetContentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentRecord getAssetContentRecord(org.osid.type.Type assetContentContentRecordType)
        throws org.osid.OperationFailedException {

        return (this.assetContent.getAssetContentRecord(assetContentContentRecordType));
    }
}

