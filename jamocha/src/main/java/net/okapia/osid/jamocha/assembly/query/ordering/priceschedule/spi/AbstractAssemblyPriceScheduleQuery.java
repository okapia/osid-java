//
// AbstractAssemblyPriceScheduleQuery.java
//
//     A PriceScheduleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PriceScheduleQuery that stores terms.
 */

public abstract class AbstractAssemblyPriceScheduleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.ordering.PriceScheduleQuery,
               org.osid.ordering.PriceScheduleQueryInspector,
               org.osid.ordering.PriceScheduleSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.PriceScheduleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceScheduleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.PriceScheduleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPriceScheduleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPriceScheduleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the price <code> Id </code> for this query. 
     *
     *  @param  priceId a price <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriceId(org.osid.id.Id priceId, boolean match) {
        getAssembler().addIdTerm(getPriceIdColumn(), priceId, match);
        return;
    }


    /**
     *  Clears the price <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceIdTerms() {
        getAssembler().clearTerms(getPriceIdColumn());
        return;
    }


    /**
     *  Gets the price <code> Id </code> terms. 
     *
     *  @return the price <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceIdTerms() {
        return (getAssembler().getIdTerms(getPriceIdColumn()));
    }


    /**
     *  Gets the PriceId column name.
     *
     * @return the column name
     */

    protected String getPriceIdColumn() {
        return ("price_id");
    }


    /**
     *  Tests if a price query is available. 
     *
     *  @return <code> true </code> if a price query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price. 
     *
     *  @return the price query 
     *  @throws org.osid.UnimplementedException <code> supportsPriceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceQuery getPriceQuery() {
        throw new org.osid.UnimplementedException("supportsPriceQuery() is false");
    }


    /**
     *  Matches price schedules with any price. 
     *
     *  @param  match <code> true </code> to match price sschedules with any 
     *          price, <code> false </code> to match price schedules with no 
     *          prices 
     */

    @OSID @Override
    public void matchAnyPrice(boolean match) {
        getAssembler().addIdWildcardTerm(getPriceColumn(), match);
        return;
    }


    /**
     *  Clears the price terms. 
     */

    @OSID @Override
    public void clearPriceTerms() {
        getAssembler().clearTerms(getPriceColumn());
        return;
    }


    /**
     *  Gets the price terms. 
     *
     *  @return the price terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceQueryInspector[] getPriceTerms() {
        return (new org.osid.ordering.PriceQueryInspector[0]);
    }


    /**
     *  Gets the Price column name.
     *
     * @return the column name
     */

    protected String getPriceColumn() {
        return ("price");
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        getAssembler().addIdTerm(getProductIdColumn(), productId, match);
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        getAssembler().clearTerms(getProductIdColumn());
        return;
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (getAssembler().getIdTerms(getProductIdColumn()));
    }


    /**
     *  Gets the ProductId column name.
     *
     * @return the column name
     */

    protected String getProductIdColumn() {
        return ("product_id");
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches price schedules used with any product. 
     *
     *  @param  match <code> true </code> to match price schedules with any 
     *          product, <code> false </code> to match price schedules with no 
     *          products 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        getAssembler().addIdWildcardTerm(getProductColumn(), match);
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        getAssembler().clearTerms(getProductColumn());
        return;
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Gets the Product column name.
     *
     * @return the column name
     */

    protected String getProductColumn() {
        return ("product");
    }


    /**
     *  Sets the price <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this priceSchedule supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceScheduleRecordType a price schedule record type 
     *  @return <code>true</code> if the priceScheduleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceScheduleRecordType) {
        for (org.osid.ordering.records.PriceScheduleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  priceScheduleRecordType the price schedule record type 
     *  @return the price schedule query record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleQueryRecord getPriceScheduleQueryRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  priceScheduleRecordType the price schedule record type 
     *  @return the price schedule query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleQueryInspectorRecord getPriceScheduleQueryInspectorRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param priceScheduleRecordType the price schedule record type
     *  @return the price schedule search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceScheduleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleSearchOrderRecord getPriceScheduleSearchOrderRecord(org.osid.type.Type priceScheduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceScheduleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(priceScheduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceScheduleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this price schedule. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceScheduleQueryRecord the price schedule query record
     *  @param priceScheduleQueryInspectorRecord the price schedule query inspector
     *         record
     *  @param priceScheduleSearchOrderRecord the price schedule search order record
     *  @param priceScheduleRecordType price schedule record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleQueryRecord</code>,
     *          <code>priceScheduleQueryInspectorRecord</code>,
     *          <code>priceScheduleSearchOrderRecord</code> or
     *          <code>priceScheduleRecordTypepriceSchedule</code> is
     *          <code>null</code>
     */
            
    protected void addPriceScheduleRecords(org.osid.ordering.records.PriceScheduleQueryRecord priceScheduleQueryRecord, 
                                      org.osid.ordering.records.PriceScheduleQueryInspectorRecord priceScheduleQueryInspectorRecord, 
                                      org.osid.ordering.records.PriceScheduleSearchOrderRecord priceScheduleSearchOrderRecord, 
                                      org.osid.type.Type priceScheduleRecordType) {

        addRecordType(priceScheduleRecordType);

        nullarg(priceScheduleQueryRecord, "price schedule query record");
        nullarg(priceScheduleQueryInspectorRecord, "price schedule query inspector record");
        nullarg(priceScheduleSearchOrderRecord, "price schedule search odrer record");

        this.queryRecords.add(priceScheduleQueryRecord);
        this.queryInspectorRecords.add(priceScheduleQueryInspectorRecord);
        this.searchOrderRecords.add(priceScheduleSearchOrderRecord);
        
        return;
    }
}
