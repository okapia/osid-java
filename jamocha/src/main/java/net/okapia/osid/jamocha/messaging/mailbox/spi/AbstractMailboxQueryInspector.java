//
// AbstractMailboxQueryInspector.java
//
//     A template for making a MailboxQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.mailbox.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for mailboxes.
 */

public abstract class AbstractMailboxQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.messaging.MailboxQueryInspector {

    private final java.util.Collection<org.osid.messaging.records.MailboxQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the message <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMessageIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MessageQueryInspector[] getMessageTerms() {
        return (new org.osid.messaging.MessageQueryInspector[0]);
    }


    /**
     *  Gets the ancestor mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorMailboxIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getAncestorMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }


    /**
     *  Gets the descendant mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantMailboxIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getDescendantMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given mailbox query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a mailbox implementing the requested record.
     *
     *  @param mailboxRecordType a mailbox record type
     *  @return the mailbox query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mailboxRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxQueryInspectorRecord getMailboxQueryInspectorRecord(org.osid.type.Type mailboxRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.MailboxQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(mailboxRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mailboxRecordType + " is not supported");
    }


    /**
     *  Adds a record to this mailbox query. 
     *
     *  @param mailboxQueryInspectorRecord mailbox query inspector
     *         record
     *  @param mailboxRecordType mailbox record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMailboxQueryInspectorRecord(org.osid.messaging.records.MailboxQueryInspectorRecord mailboxQueryInspectorRecord, 
                                                   org.osid.type.Type mailboxRecordType) {

        addRecordType(mailboxRecordType);
        nullarg(mailboxRecordType, "mailbox record type");
        this.records.add(mailboxQueryInspectorRecord);        
        return;
    }
}
