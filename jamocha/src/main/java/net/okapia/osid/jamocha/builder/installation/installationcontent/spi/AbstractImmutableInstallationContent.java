//
// AbstractImmutableInstallationContent.java
//
//     Wraps a mutable InstallationContent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.installationcontent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>InstallationContent</code> to hide modifiers. This
 *  wrapper provides an immutized InstallationContent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying installationContent whose state changes are visible.
 */

public abstract class AbstractImmutableInstallationContent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.installation.InstallationContent {

    private final org.osid.installation.InstallationContent installationContent;


    /**
     *  Constructs a new <code>AbstractImmutableInstallationContent</code>.
     *
     *  @param installationContent the installation content to immutablize
     *  @throws org.osid.NullArgumentException <code>installationContent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInstallationContent(org.osid.installation.InstallationContent installationContent) {
        super(installationContent);
        this.installationContent = installationContent;
        return;
    }


    /**
     *  Gets the <code> Package Id </code> corresponding to this content. 
     *
     *  @return the package <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPackageId() {
        return (this.installationContent.getPackageId());
    }


    /**
     *  Gets the <code> Package </code> corresponding to this content. 
     *
     *  @return the package 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage() {
        return (this.installationContent.getPackage());
    }


    /**
     *  Tests if a data length is available. 
     *
     *  @return <code> true </code> if a length is available for this content, 
     *          <code> false </code> otherwise. 
     */

    @OSID @Override
    public boolean hasDataLength() {
        return (this.installationContent.hasDataLength());
    }


    /**
     *  Gets the length of the data represented by this content in bytes. 
     *
     *  @return the length of the data stream 
     *  @throws org.osid.IllegalStateException <code> hasDataLength() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getDataLength() {
        return (this.installationContent.getDataLength());
    }


    /**
     *  Gets the asset content data. 
     *
     *  @return the length of the content data 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.transport.DataInputStream getData()
        throws org.osid.OperationFailedException {

        return (this.installationContent.getData());
    }


    /**
     *  Gets the installation content record corresponding to the given <code> 
     *  InstallationContent </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> installationContentRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(installationContentRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  installationContentRecordType an installation content record 
     *          type 
     *  @return the installation content record 
     *  @throws org.osid.NullArgumentException <code> 
     *          installationContentRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(installationContentRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.installation.records.InstallationContentRecord getInstallationContentRecord(org.osid.type.Type installationContentRecordType)
        throws org.osid.OperationFailedException {

        return (this.installationContent.getInstallationContentRecord(installationContentRecordType));
    }
}

