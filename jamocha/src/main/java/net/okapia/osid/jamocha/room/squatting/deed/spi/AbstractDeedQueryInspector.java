//
// AbstractDeedQueryInspector.java
//
//     A template for making a DeedQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for deeds.
 */

public abstract class AbstractDeedQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.room.squatting.DeedQueryInspector {

    private final java.util.Collection<org.osid.room.squatting.records.DeedQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the owner resource <code> Id </code> terms. 
     *
     *  @return the owner <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOwnerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the owner resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getOwnerTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given deed query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a deed implementing the requested record.
     *
     *  @param deedRecordType a deed record type
     *  @return the deed query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>deedRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deedRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedQueryInspectorRecord getDeedQueryInspectorRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.squatting.records.DeedQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(deedRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deedRecordType + " is not supported");
    }


    /**
     *  Adds a record to this deed query. 
     *
     *  @param deedQueryInspectorRecord deed query inspector
     *         record
     *  @param deedRecordType deed record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeedQueryInspectorRecord(org.osid.room.squatting.records.DeedQueryInspectorRecord deedQueryInspectorRecord, 
                                                   org.osid.type.Type deedRecordType) {

        addRecordType(deedRecordType);
        nullarg(deedRecordType, "deed record type");
        this.records.add(deedQueryInspectorRecord);        
        return;
    }
}
