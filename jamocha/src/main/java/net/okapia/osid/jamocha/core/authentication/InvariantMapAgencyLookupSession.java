//
// InvariantMapAgencyLookupSession
//
//    Implements an Agency lookup service backed by a fixed collection of
//    agencies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication;


/**
 *  Implements an Agency lookup service backed by a fixed
 *  collection of agencies. The agencies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAgencyLookupSession
    extends net.okapia.osid.jamocha.core.authentication.spi.AbstractMapAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAgencyLookupSession</code> with no
     *  agencies.
     */

    public InvariantMapAgencyLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgencyLookupSession</code> with a single
     *  agency.
     *  
     *  @throws org.osid.NullArgumentException {@code agency}
     *          is <code>null</code>
     */

    public InvariantMapAgencyLookupSession(org.osid.authentication.Agency agency) {
        putAgency(agency);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgencyLookupSession</code> using an array
     *  of agencies.
     *  
     *  @throws org.osid.NullArgumentException {@code agencies}
     *          is <code>null</code>
     */

    public InvariantMapAgencyLookupSession(org.osid.authentication.Agency[] agencies) {
        putAgencies(agencies);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAgencyLookupSession</code> using a
     *  collection of agencies.
     *
     *  @throws org.osid.NullArgumentException {@code agencies}
     *          is <code>null</code>
     */

    public InvariantMapAgencyLookupSession(java.util.Collection<? extends org.osid.authentication.Agency> agencies) {
        putAgencies(agencies);
        return;
    }
}
