//
// MutableIndexedMapProxySubjectLookupSession
//
//    Implements a Subject lookup service backed by a collection of
//    subjects indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology;


/**
 *  Implements a Subject lookup service backed by a collection of
 *  subjects. The subjects are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some subjects may be compatible
 *  with more types than are indicated through these subject
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of subjects can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxySubjectLookupSession
    extends net.okapia.osid.jamocha.core.ontology.spi.AbstractIndexedMapSubjectLookupSession
    implements org.osid.ontology.SubjectLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySubjectLookupSession} with
     *  no subject.
     *
     *  @param ontology the ontology
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                       org.osid.proxy.Proxy proxy) {
        setOntology(ontology);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySubjectLookupSession} with
     *  a single subject.
     *
     *  @param ontology the ontology
     *  @param  subject an subject
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subject}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                       org.osid.ontology.Subject subject, org.osid.proxy.Proxy proxy) {

        this(ontology, proxy);
        putSubject(subject);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySubjectLookupSession} using
     *  an array of subjects.
     *
     *  @param ontology the ontology
     *  @param  subjects an array of subjects
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subjects}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                       org.osid.ontology.Subject[] subjects, org.osid.proxy.Proxy proxy) {

        this(ontology, proxy);
        putSubjects(subjects);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxySubjectLookupSession} using
     *  a collection of subjects.
     *
     *  @param ontology the ontology
     *  @param  subjects a collection of subjects
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code ontology},
     *          {@code subjects}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxySubjectLookupSession(org.osid.ontology.Ontology ontology,
                                                       java.util.Collection<? extends org.osid.ontology.Subject> subjects,
                                                       org.osid.proxy.Proxy proxy) {
        this(ontology, proxy);
        putSubjects(subjects);
        return;
    }

    
    /**
     *  Makes a {@code Subject} available in this session.
     *
     *  @param  subject a subject
     *  @throws org.osid.NullArgumentException {@code subject{@code 
     *          is {@code null}
     */

    @Override
    public void putSubject(org.osid.ontology.Subject subject) {
        super.putSubject(subject);
        return;
    }


    /**
     *  Makes an array of subjects available in this session.
     *
     *  @param  subjects an array of subjects
     *  @throws org.osid.NullArgumentException {@code subjects{@code 
     *          is {@code null}
     */

    @Override
    public void putSubjects(org.osid.ontology.Subject[] subjects) {
        super.putSubjects(subjects);
        return;
    }


    /**
     *  Makes collection of subjects available in this session.
     *
     *  @param  subjects a collection of subjects
     *  @throws org.osid.NullArgumentException {@code subject{@code 
     *          is {@code null}
     */

    @Override
    public void putSubjects(java.util.Collection<? extends org.osid.ontology.Subject> subjects) {
        super.putSubjects(subjects);
        return;
    }


    /**
     *  Removes a Subject from this session.
     *
     *  @param subjectId the {@code Id} of the subject
     *  @throws org.osid.NullArgumentException {@code subjectId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSubject(org.osid.id.Id subjectId) {
        super.removeSubject(subjectId);
        return;
    }    
}
