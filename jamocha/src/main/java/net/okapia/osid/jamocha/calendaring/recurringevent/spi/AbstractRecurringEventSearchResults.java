//
// AbstractRecurringEventSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRecurringEventSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.RecurringEventSearchResults {

    private org.osid.calendaring.RecurringEventList recurringEvents;
    private final org.osid.calendaring.RecurringEventQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.RecurringEventSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRecurringEventSearchResults.
     *
     *  @param recurringEvents the result set
     *  @param recurringEventQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>recurringEvents</code>
     *          or <code>recurringEventQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRecurringEventSearchResults(org.osid.calendaring.RecurringEventList recurringEvents,
                                            org.osid.calendaring.RecurringEventQueryInspector recurringEventQueryInspector) {
        nullarg(recurringEvents, "recurring events");
        nullarg(recurringEventQueryInspector, "recurring event query inspectpr");

        this.recurringEvents = recurringEvents;
        this.inspector = recurringEventQueryInspector;

        return;
    }


    /**
     *  Gets the recurring event list resulting from a search.
     *
     *  @return a recurring event list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventList getRecurringEvents() {
        if (this.recurringEvents == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.RecurringEventList recurringEvents = this.recurringEvents;
        this.recurringEvents = null;
	return (recurringEvents);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.RecurringEventQueryInspector getRecurringEventQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  recurring event search record <code> Type. </code> This method must
     *  be used to retrieve a recurringEvent implementing the requested
     *  record.
     *
     *  @param recurringEventSearchRecordType a recurringEvent search 
     *         record type 
     *  @return the recurring event search
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recurringEventSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventSearchResultsRecord getRecurringEventSearchResultsRecord(org.osid.type.Type recurringEventSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.RecurringEventSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(recurringEventSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(recurringEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record recurring event search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRecurringEventRecord(org.osid.calendaring.records.RecurringEventSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "recurring event record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
