//
// InvariantMapProxyPackageLookupSession
//
//    Implements a Package lookup service backed by a fixed
//    collection of packages. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Package lookup service backed by a fixed
 *  collection of packages. The packages are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPackageLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapPackageLookupSession
    implements org.osid.installation.PackageLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPackageLookupSession} with no
     *  packages.
     *
     *  @param depot the depot
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depot} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPackageLookupSession(org.osid.installation.Depot depot,
                                                  org.osid.proxy.Proxy proxy) {
        setDepot(depot);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPackageLookupSession} with a single
     *  package.
     *
     *  @param depot the depot
     *  @param pkg a single package
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depot},
     *          {@code pkg} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPackageLookupSession(org.osid.installation.Depot depot,
                                                  org.osid.installation.Package pkg, org.osid.proxy.Proxy proxy) {

        this(depot, proxy);
        putPackage(pkg);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPackageLookupSession} using
     *  an array of packages.
     *
     *  @param depot the depot
     *  @param packages an array of packages
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depot},
     *          {@code packages} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPackageLookupSession(org.osid.installation.Depot depot,
                                                  org.osid.installation.Package[] packages, org.osid.proxy.Proxy proxy) {

        this(depot, proxy);
        putPackages(packages);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPackageLookupSession} using a
     *  collection of packages.
     *
     *  @param depot the depot
     *  @param packages a collection of packages
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code depot},
     *          {@code packages} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPackageLookupSession(org.osid.installation.Depot depot,
                                                  java.util.Collection<? extends org.osid.installation.Package> packages,
                                                  org.osid.proxy.Proxy proxy) {

        this(depot, proxy);
        putPackages(packages);
        return;
    }
}
