//
// AbstractFederatingBudgetEntryLookupSession.java
//
//     An abstract federating adapter for a BudgetEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BudgetEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBudgetEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.financials.budgeting.BudgetEntryLookupSession>
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {

    private boolean parallel = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingBudgetEntryLookupSession</code>.
     */

    protected AbstractFederatingBudgetEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.financials.budgeting.BudgetEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>BudgetEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBudgetEntries() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            if (session.canLookupBudgetEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>BudgetEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetEntryView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.useComparativeBudgetEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>BudgetEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetEntryView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.usePlenaryBudgetEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budget entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }


    /**
     *  Only budget entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveBudgetEntryView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.useEffectiveBudgetEntryView();
        }

        return;
    }


    /**
     *  All budget entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveBudgetEntryView() {
        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            session.useAnyEffectiveBudgetEntryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>BudgetEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BudgetEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BudgetEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  budgetEntryId <code>Id</code> of the
     *          <code>BudgetEntry</code>
     *  @return the budget entry
     *  @throws org.osid.NotFoundException <code>budgetEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>budgetEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntry getBudgetEntry(org.osid.id.Id budgetEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            try {
                return (session.getBudgetEntry(budgetEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(budgetEntryId + " not found");
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgetEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>BudgetEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  budgetEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByIds(org.osid.id.IdList budgetEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.financials.budgeting.budgetentry.MutableBudgetEntryList ret = new net.okapia.osid.jamocha.financials.budgeting.budgetentry.MutableBudgetEntryList();

        try (org.osid.id.IdList ids = budgetEntryIds) {
            while (ids.hasNext()) {
                ret.addBudgetEntry(getBudgetEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  budget entry genus <code>Type</code> which does not include
     *  budget entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesByGenusType(budgetEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  budget entry genus <code>Type</code> and include any additional
     *  budget entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByParentGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesByParentGenusType(budgetEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetEntryList</code> containing the given
     *  budget entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryRecordType a budgetEntry record type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByRecordType(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesByRecordType(budgetEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BudgetEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *  
     *  In active mode, budget entries are returned that are currently
     *  active. In any status mode, active and inactive budget entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BudgetEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of budget entries corresponding to a budget
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudget(org.osid.id.Id budgetId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForBudget(budgetId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of budget entries corresponding to a budget
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetOnDate(org.osid.id.Id budgetId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForBudgetOnDate(budgetId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of budget entries corresponding to an account
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>accountId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccount(org.osid.id.Id accountId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForAccount(accountId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of budget entries corresponding to an account
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>accountId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccountOnDate(org.osid.id.Id accountId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForAccountOnDate(accountId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of budget entries corresponding to budget and account
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @param  accountId the <code>Id</code> of the account
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>accountId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccount(org.osid.id.Id budgetId,
                                                                                             org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForBudgetAndAccount(budgetId, accountId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of budget entries corresponding to budget and account
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>accountId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccountOnDate(org.osid.id.Id budgetId,
                                                                                                   org.osid.id.Id accountId,
                                                                                                   org.osid.calendaring.DateTime from,
                                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntriesForBudgetAndAccountOnDate(budgetId, accountId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>BudgetEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>BudgetEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList ret = getBudgetEntryList();

        for (org.osid.financials.budgeting.BudgetEntryLookupSession session : getSessions()) {
            ret.addBudgetEntryList(session.getBudgetEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.FederatingBudgetEntryList getBudgetEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.ParallelBudgetEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.financials.budgeting.budgetentry.CompositeBudgetEntryList());
        }
    }
}
