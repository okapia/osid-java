//
// BinNode.java
//
//     Defines a Bin node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  A class for managing a hierarchy of bin nodes in core.
 */

public final class BinNode
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractBinNode
    implements org.osid.resource.BinNode {


    /**
     *  Constructs a new <code>BinNode</code> from a single
     *  bin.
     *
     *  @param bin the bin
     *  @throws org.osid.NullArgumentException <code>bin</code> is 
     *          <code>null</code>.
     */

    public BinNode(org.osid.resource.Bin bin) {
        super(bin);
        return;
    }


    /**
     *  Constructs a new <code>BinNode</code>.
     *
     *  @param bin the bin
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>.
     */

    public BinNode(org.osid.resource.Bin bin, boolean root, boolean leaf) {
        super(bin, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this bin.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.resource.BinNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this bin.
     *
     *  @param bin the bin to add as a parent
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.resource.Bin bin) {
        addParent(new BinNode(bin));
        return;
    }


    /**
     *  Adds a child to this bin.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.resource.BinNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this bin.
     *
     *  @param bin the bin to add as a child
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.resource.Bin bin) {
        addChild(new BinNode(bin));
        return;
    }
}
