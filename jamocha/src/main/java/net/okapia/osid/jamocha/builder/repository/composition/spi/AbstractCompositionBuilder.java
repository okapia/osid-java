//
// AbstractComposition.java
//
//     Defines a Composition builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.composition.spi;


/**
 *  Defines a <code>Composition</code> builder.
 */

public abstract class AbstractCompositionBuilder<T extends AbstractCompositionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractSourceableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.repository.composition.CompositionMiter composition;


    /**
     *  Constructs a new <code>AbstractCompositionBuilder</code>.
     *
     *  @param composition the composition to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCompositionBuilder(net.okapia.osid.jamocha.builder.repository.composition.CompositionMiter composition) {
        super(composition);
        this.composition = composition;
        return;
    }


    /**
     *  Builds the composition.
     *
     *  @return the new composition
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.repository.Composition build() {
        (new net.okapia.osid.jamocha.builder.validator.repository.composition.CompositionValidator(getValidations())).validate(this.composition);
        return (new net.okapia.osid.jamocha.builder.repository.composition.ImmutableComposition(this.composition));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the composition miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.repository.composition.CompositionMiter getMiter() {
        return (this.composition);
    }


    /**
     *  Enables this composition. Enabling a composition overrides any
     *  enabling rule that may exist.
     */
    
    public T enabled() {
        getMiter().setEnabled(true);
        return (self());
    }


    /**
     *  Disables this composition. Disabling a composition overrides
     *  any enabling rule that may exist.
     */
    
    public T disabled() {
        getMiter().setDisabled(true);
        return (self());
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public T operational(boolean operational) {
        getMiter().setOperational(operational);
        return (self());
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }

    
    /**
     *  Adds a child.
     *
     *  @param child a child
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>child</code> is
     *          <code>null</code>
     */

    public T child(org.osid.repository.Composition child) {
        getMiter().addChild(child);
        return (self());
    }


    /**
     *  Sets all the children.
     *
     *  @param children a collection of children
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>children</code>
     *          is <code>null</code>
     */

    public T children(java.util.Collection<org.osid.repository.Composition> children) {
        getMiter().setChildren(children);
        return (self());
    }


    /**
     *  Adds a Composition record.
     *
     *  @param record a composition record
     *  @param recordType the type of composition record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.repository.records.CompositionRecord record, org.osid.type.Type recordType) {
        getMiter().addCompositionRecord(record, recordType);
        return (self());
    }
}       


