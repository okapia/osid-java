//
// AbstractFederatingAddressBookLookupSession.java
//
//     An abstract federating adapter for an AddressBookLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AddressBookLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAddressBookLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.contact.AddressBookLookupSession>
    implements org.osid.contact.AddressBookLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingAddressBookLookupSession</code>.
     */

    protected AbstractFederatingAddressBookLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.contact.AddressBookLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>AddressBook</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAddressBooks() {
        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            if (session.canLookupAddressBooks()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AddressBook</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAddressBookView() {
        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            session.useComparativeAddressBookView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AddressBook</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAddressBookView() {
        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            session.usePlenaryAddressBookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AddressBook</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AddressBook</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AddressBook</code> and
     *  retained for compatibility.
     *
     *  @param  addressBookId <code>Id</code> of the
     *          <code>AddressBook</code>
     *  @return the address book
     *  @throws org.osid.NotFoundException <code>addressBookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>addressBookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook(org.osid.id.Id addressBookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            try {
                return (session.getAddressBook(addressBookId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(addressBookId + " not found");
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  addressBooks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AddressBooks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  addressBookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByIds(org.osid.id.IdList addressBookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.contact.addressbook.MutableAddressBookList ret = new net.okapia.osid.jamocha.contact.addressbook.MutableAddressBookList();

        try (org.osid.id.IdList ids = addressBookIds) {
            while (ids.hasNext()) {
                ret.addAddressBook(getAddressBook(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  address book genus <code>Type</code> which does not include
     *  address books of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList ret = getAddressBookList();

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            ret.addAddressBookList(session.getAddressBooksByGenusType(addressBookGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  address book genus <code>Type</code> and include any additional
     *  address books with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookGenusType an addressBook genus type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByParentGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList ret = getAddressBookList();

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            ret.addAddressBookList(session.getAddressBooksByParentGenusType(addressBookGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressBookList</code> containing the given
     *  address book record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  addressBookRecordType an addressBook record type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByRecordType(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList ret = getAddressBookList();

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            ret.addAddressBookList(session.getAddressBooksByRecordType(addressBookRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AddressBookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known address books or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  address books that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>AddressBook</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList ret = getAddressBookList();

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            ret.addAddressBookList(session.getAddressBooksByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AddressBooks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  address books or an error results. Otherwise, the returned list
     *  may contain only those address books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AddressBooks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList ret = getAddressBookList();

        for (org.osid.contact.AddressBookLookupSession session : getSessions()) {
            ret.addAddressBookList(session.getAddressBooks());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.contact.addressbook.FederatingAddressBookList getAddressBookList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.addressbook.ParallelAddressBookList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.addressbook.CompositeAddressBookList());
        }
    }
}
