//
// AbstractDocetSearch.java
//
//     A template for making a Docet Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing docet searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDocetSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.syllabus.DocetSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.DocetSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.syllabus.DocetSearchOrder docetSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of docets. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  docetIds list of docets
     *  @throws org.osid.NullArgumentException
     *          <code>docetIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDocets(org.osid.id.IdList docetIds) {
        while (docetIds.hasNext()) {
            try {
                this.ids.add(docetIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDocets</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of docet Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDocetIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  docetSearchOrder docet search order 
     *  @throws org.osid.NullArgumentException
     *          <code>docetSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>docetSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDocetResults(org.osid.course.syllabus.DocetSearchOrder docetSearchOrder) {
	this.docetSearchOrder = docetSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.syllabus.DocetSearchOrder getDocetSearchOrder() {
	return (this.docetSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given docet search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a docet implementing the requested record.
     *
     *  @param docetSearchRecordType a docet search record
     *         type
     *  @return the docet search record
     *  @throws org.osid.NullArgumentException
     *          <code>docetSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetSearchRecord getDocetSearchRecord(org.osid.type.Type docetSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.syllabus.records.DocetSearchRecord record : this.records) {
            if (record.implementsRecordType(docetSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this docet search. 
     *
     *  @param docetSearchRecord docet search record
     *  @param docetSearchRecordType docet search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDocetSearchRecord(org.osid.course.syllabus.records.DocetSearchRecord docetSearchRecord, 
                                           org.osid.type.Type docetSearchRecordType) {

        addRecordType(docetSearchRecordType);
        this.records.add(docetSearchRecord);        
        return;
    }
}
