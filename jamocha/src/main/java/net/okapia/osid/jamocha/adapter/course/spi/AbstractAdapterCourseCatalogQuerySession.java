//
// AbstractQueryCourseCatalogLookupSession.java
//
//    A CourseCatalogQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CourseCatalogQuerySession adapter.
 */

public abstract class AbstractAdapterCourseCatalogQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.CourseCatalogQuerySession {

    private final org.osid.course.CourseCatalogQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCourseCatalogQuerySession.
     *
     *  @param session the underlying course catalog query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCourseCatalogQuerySession(org.osid.course.CourseCatalogQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeCourseCatalog</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCourseCatalogs() {
        return (this.session.canSearchCourseCatalogs());
    }

      
    /**
     *  Gets a course catalog query. The returned query will not have an
     *  extension query.
     *
     *  @return the course catalog query 
     */
      
    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        return (this.session.getCourseCatalogQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  courseCatalogQuery the course catalog query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code courseCatalogQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code courseCatalogQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByQuery(org.osid.course.CourseCatalogQuery courseCatalogQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCourseCatalogsByQuery(courseCatalogQuery));
    }
}
