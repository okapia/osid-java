//
// InvariantMapAuditEnablerLookupSession
//
//    Implements an AuditEnabler lookup service backed by a fixed collection of
//    auditEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an AuditEnabler lookup service backed by a fixed
 *  collection of audit enablers. The audit enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuditEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractMapAuditEnablerLookupSession
    implements org.osid.inquiry.rules.AuditEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditEnablerLookupSession</code> with no
     *  audit enablers.
     *  
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumnetException {@code inquest} is
     *          {@code null}
     */

    public InvariantMapAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditEnablerLookupSession</code> with a single
     *  audit enabler.
     *  
     *  @param inquest the inquest
     *  @param auditEnabler an single audit enabler
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code auditEnabler} is <code>null</code>
     */

      public InvariantMapAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.rules.AuditEnabler auditEnabler) {
        this(inquest);
        putAuditEnabler(auditEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditEnablerLookupSession</code> using an array
     *  of audit enablers.
     *  
     *  @param inquest the inquest
     *  @param auditEnablers an array of audit enablers
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code auditEnablers} is <code>null</code>
     */

      public InvariantMapAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.rules.AuditEnabler[] auditEnablers) {
        this(inquest);
        putAuditEnablers(auditEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuditEnablerLookupSession</code> using a
     *  collection of audit enablers.
     *
     *  @param inquest the inquest
     *  @param auditEnablers a collection of audit enablers
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code auditEnablers} is <code>null</code>
     */

      public InvariantMapAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                               java.util.Collection<? extends org.osid.inquiry.rules.AuditEnabler> auditEnablers) {
        this(inquest);
        putAuditEnablers(auditEnablers);
        return;
    }
}
