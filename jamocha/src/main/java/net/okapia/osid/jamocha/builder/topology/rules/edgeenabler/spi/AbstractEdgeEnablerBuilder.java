//
// AbstractEdgeEnabler.java
//
//     Defines an EdgeEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.rules.edgeenabler.spi;


/**
 *  Defines an <code>EdgeEnabler</code> builder.
 */

public abstract class AbstractEdgeEnablerBuilder<T extends AbstractEdgeEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.topology.rules.edgeenabler.EdgeEnablerMiter edgeEnabler;


    /**
     *  Constructs a new <code>AbstractEdgeEnablerBuilder</code>.
     *
     *  @param edgeEnabler the edge enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEdgeEnablerBuilder(net.okapia.osid.jamocha.builder.topology.rules.edgeenabler.EdgeEnablerMiter edgeEnabler) {
        super(edgeEnabler);
        this.edgeEnabler = edgeEnabler;
        return;
    }


    /**
     *  Builds the edge enabler.
     *
     *  @return the new edge enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.topology.rules.EdgeEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.topology.rules.edgeenabler.EdgeEnablerValidator(getValidations())).validate(this.edgeEnabler);
        return (new net.okapia.osid.jamocha.builder.topology.rules.edgeenabler.ImmutableEdgeEnabler(this.edgeEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the edge enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.topology.rules.edgeenabler.EdgeEnablerMiter getMiter() {
        return (this.edgeEnabler);
    }


    /**
     *  Adds an EdgeEnabler record.
     *
     *  @param record an edge enabler record
     *  @param recordType the type of edge enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.topology.rules.records.EdgeEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addEdgeEnablerRecord(record, recordType);
        return (self());
    }
}       


