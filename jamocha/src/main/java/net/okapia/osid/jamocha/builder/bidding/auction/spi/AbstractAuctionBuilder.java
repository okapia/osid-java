//
// AbstractAuction.java
//
//     Defines an Auction builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.auction.spi;


/**
 *  Defines an <code>Auction</code> builder.
 */

public abstract class AbstractAuctionBuilder<T extends AbstractAuctionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.bidding.auction.AuctionMiter auction;


    /**
     *  Constructs a new <code>AbstractAuctionBuilder</code>.
     *
     *  @param auction the auction to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuctionBuilder(net.okapia.osid.jamocha.builder.bidding.auction.AuctionMiter auction) {
        super(auction);
        this.auction = auction;
        return;
    }


    /**
     *  Builds the auction.
     *
     *  @return the new auction
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.bidding.Auction build() {
        (new net.okapia.osid.jamocha.builder.validator.bidding.auction.AuctionValidator(getValidations())).validate(this.auction);
        return (new net.okapia.osid.jamocha.builder.bidding.auction.ImmutableAuction(this.auction));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the auction miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.bidding.auction.AuctionMiter getMiter() {
        return (this.auction);
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T startDate(org.osid.calendaring.DateTime date) {
        this.auction.setStartDate(date);
        return (self());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T endDate(org.osid.calendaring.DateTime date) {
        this.auction.setEndDate(date);
        return (self());
    }


    /**
     *  Sets the currency type.
     *
     *  @param currencyType a currency type
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>currencyType</code> is <code>null</code>
     */

    public T currencyType(org.osid.type.Type currencyType) {
        getMiter().setCurrencyType(currencyType);
        return (self());
    }


    /**
     *  Sets the minimum bidders.
     *
     *  @param bidders the minimum bidders
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>bidders</code>
     *          is negative
     */

    public T minimumBidders(long bidders) {
        getMiter().setMinimumBidders(bidders);
        return (self());
    }


    /**
     *  Sets the sealed flag.
     *
     *  @return the builder
     */

    public T sealed() {
        getMiter().setSealed(true);
        return (self());
    }


    /**
     *  Unsets the sealed flag.
     *
     *  @return the builder
     */

    public T opened() {
        getMiter().setSealed(false);
        return (self());
    }


    /**
     *  Sets the seller.
     *
     *  @param seller a seller
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>seller</code> is <code>null</code>
     */

    public T seller(org.osid.resource.Resource seller) {
        getMiter().setSeller(seller);
        return (self());
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    public T item(org.osid.resource.Resource item) {
        getMiter().setItem(item);
        return (self());
    }


    /**
     *  Sets the lot size.
     *
     *  @param size a lot size
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>size</code> is
     *          negative
     */

    public T lotSize(long size) {
        getMiter().setLotSize(size);
        return (self());
    }


    /**
     *  Sets the remaining items.
     *
     *  @param number a remaining items
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>number</code>
     *          is negative
     */

    public T remainingItems(long number) {
        getMiter().setRemainingItems(number);
        return (self());
    }


    /**
     *  Sets the item limit.
     *
     *  @param limit an item limit
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>limit</code>
     *          is negative
     */

    public T itemLimit(long limit) {
        getMiter().setItemLimit(limit);
        return (self());
    }


    /**
     *  Sets the starting price.
     *
     *  @param price a starting price
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public T startingPrice(org.osid.financials.Currency price) {
        getMiter().setStartingPrice(price);
        return (self());
    }


    /**
     *  Sets the price increment.
     *
     *  @param increment a price increment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    public T priceIncrement(org.osid.financials.Currency increment) {
        getMiter().setPriceIncrement(increment);
        return (self());
    }


    /**
     *  Sets the reserve price.
     *
     *  @param price a reserve price
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public T reservePrice(org.osid.financials.Currency price) {
        getMiter().setReservePrice(price);
        return (self());
    }


    /**
     *  Sets the buyout price.
     *
     *  @param price a buyout price
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public T buyoutPrice(org.osid.financials.Currency price) {
        getMiter().setBuyoutPrice(price);
        return (self());
    }


    /**
     *  Adds an Auction record.
     *
     *  @param record an auction record
     *  @param recordType the type of auction record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.bidding.records.AuctionRecord record, org.osid.type.Type recordType) {
        getMiter().addAuctionRecord(record, recordType);
        return (self());
    }
}       


