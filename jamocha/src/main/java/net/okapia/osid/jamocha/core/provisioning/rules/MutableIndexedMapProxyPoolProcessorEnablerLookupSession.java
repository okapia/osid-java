//
// MutableIndexedMapProxyPoolProcessorEnablerLookupSession
//
//    Implements a PoolProcessorEnabler lookup service backed by a collection of
//    poolProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolProcessorEnabler lookup service backed by a collection of
 *  poolProcessorEnablers. The pool processor enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some poolProcessorEnablers may be compatible
 *  with more types than are indicated through these poolProcessorEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of pool processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyPoolProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractIndexedMapPoolProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolProcessorEnablerLookupSession} with
     *  no pool processor enabler.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolProcessorEnablerLookupSession} with
     *  a single pool processor enabler.
     *
     *  @param distributor the distributor
     *  @param  poolProcessorEnabler an pool processor enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolProcessorEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPoolProcessorEnabler(poolProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolProcessorEnablerLookupSession} using
     *  an array of pool processor enablers.
     *
     *  @param distributor the distributor
     *  @param  poolProcessorEnablers an array of pool processor enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolProcessorEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyPoolProcessorEnablerLookupSession} using
     *  a collection of pool processor enablers.
     *
     *  @param distributor the distributor
     *  @param  poolProcessorEnablers a collection of pool processor enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolProcessorEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyPoolProcessorEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                       java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code PoolProcessorEnabler} available in this session.
     *
     *  @param  poolProcessorEnabler a pool processor enabler
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolProcessorEnabler(org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        super.putPoolProcessorEnabler(poolProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of pool processor enablers available in this session.
     *
     *  @param  poolProcessorEnablers an array of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolProcessorEnablers(org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers) {
        super.putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of pool processor enablers available in this session.
     *
     *  @param  poolProcessorEnablers a collection of pool processor enablers
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putPoolProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers) {
        super.putPoolProcessorEnablers(poolProcessorEnablers);
        return;
    }


    /**
     *  Removes a PoolProcessorEnabler from this session.
     *
     *  @param poolProcessorEnablerId the {@code Id} of the pool processor enabler
     *  @throws org.osid.NullArgumentException {@code poolProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removePoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId) {
        super.removePoolProcessorEnabler(poolProcessorEnablerId);
        return;
    }    
}
