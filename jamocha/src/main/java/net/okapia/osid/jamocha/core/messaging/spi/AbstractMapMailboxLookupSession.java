//
// AbstractMapMailboxLookupSession
//
//    A simple framework for providing a Mailbox lookup service
//    backed by a fixed collection of mailboxes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Mailbox lookup service backed by a
 *  fixed collection of mailboxes. The mailboxes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Mailboxes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapMailboxLookupSession
    extends net.okapia.osid.jamocha.messaging.spi.AbstractMailboxLookupSession
    implements org.osid.messaging.MailboxLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.messaging.Mailbox> mailboxes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.messaging.Mailbox>());


    /**
     *  Makes a <code>Mailbox</code> available in this session.
     *
     *  @param  mailbox a mailbox
     *  @throws org.osid.NullArgumentException <code>mailbox<code>
     *          is <code>null</code>
     */

    protected void putMailbox(org.osid.messaging.Mailbox mailbox) {
        this.mailboxes.put(mailbox.getId(), mailbox);
        return;
    }


    /**
     *  Makes an array of mailboxes available in this session.
     *
     *  @param  mailboxes an array of mailboxes
     *  @throws org.osid.NullArgumentException <code>mailboxes<code>
     *          is <code>null</code>
     */

    protected void putMailboxes(org.osid.messaging.Mailbox[] mailboxes) {
        putMailboxes(java.util.Arrays.asList(mailboxes));
        return;
    }


    /**
     *  Makes a collection of mailboxes available in this session.
     *
     *  @param  mailboxes a collection of mailboxes
     *  @throws org.osid.NullArgumentException <code>mailboxes<code>
     *          is <code>null</code>
     */

    protected void putMailboxes(java.util.Collection<? extends org.osid.messaging.Mailbox> mailboxes) {
        for (org.osid.messaging.Mailbox mailbox : mailboxes) {
            this.mailboxes.put(mailbox.getId(), mailbox);
        }

        return;
    }


    /**
     *  Removes a Mailbox from this session.
     *
     *  @param  mailboxId the <code>Id</code> of the mailbox
     *  @throws org.osid.NullArgumentException <code>mailboxId<code> is
     *          <code>null</code>
     */

    protected void removeMailbox(org.osid.id.Id mailboxId) {
        this.mailboxes.remove(mailboxId);
        return;
    }


    /**
     *  Gets the <code>Mailbox</code> specified by its <code>Id</code>.
     *
     *  @param  mailboxId <code>Id</code> of the <code>Mailbox</code>
     *  @return the mailbox
     *  @throws org.osid.NotFoundException <code>mailboxId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>mailboxId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.messaging.Mailbox mailbox = this.mailboxes.get(mailboxId);
        if (mailbox == null) {
            throw new org.osid.NotFoundException("mailbox not found: " + mailboxId);
        }

        return (mailbox);
    }


    /**
     *  Gets all <code>Mailboxes</code>. In plenary mode, the returned
     *  list contains all known mailboxes or an error
     *  results. Otherwise, the returned list may contain only those
     *  mailboxes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Mailboxes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.messaging.mailbox.ArrayMailboxList(this.mailboxes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.mailboxes.clear();
        super.close();
        return;
    }
}
