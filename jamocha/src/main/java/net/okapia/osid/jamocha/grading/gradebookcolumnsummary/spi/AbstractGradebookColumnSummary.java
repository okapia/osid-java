//
// AbstractGradebookColumnSummary.java
//
//     Defines a GradebookColumnSummary.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradebookColumnSummary</code>.
 */

public abstract class AbstractGradebookColumnSummary
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.grading.GradebookColumnSummary {

    private org.osid.grading.GradebookColumn gradebookColumn;
    private java.math.BigDecimal mean;
    private java.math.BigDecimal median;
    private java.math.BigDecimal mode;
    private java.math.BigDecimal rms;
    private java.math.BigDecimal standardDeviation;
    private java.math.BigDecimal sum;

    private final java.util.Collection<org.osid.grading.records.GradebookColumnSummaryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> GradebookColumn. </code> 
     *
     *  @return the <code> Id </code> of the <code> GradebookColumn </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradebookColumn.getId());
    }


    /**
     *  Gets the <code> GradebookColumn. </code> 
     *
     *  @return the <code> GradebookColumn </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumn);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    protected void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        nullarg(gradebookColumn, "gradebook column");
        this.gradebookColumn = gradebookColumn;
        return;
    }


    /**
     *  Gets the mean score. If this system is based on grades, the mean 
     *  output score is returned. 
     *
     *  @return the mean score 
     */

    @OSID @Override
    public java.math.BigDecimal getMean() {
        return (this.mean);
    }


    /**
     *  Sets the mean.
     *
     *  @param mean a mean score
     *  @throws org.osid.NullArgumentException
     *          <code>mean</code> is <code>null</code>
     */

    protected void setMean(java.math.BigDecimal mean) {
        nullarg(mean, "mean score");
        this.mean = mean;
        return;
    }


    /**
     *  Gets the median score. If this system is based on grades, the mean 
     *  output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getMedian() {
        return (this.median);
    }


    /**
     *  Sets the median score.
     *
     *  @param median a median score
     *  @throws org.osid.NullArgumentException <code>median</code> is
     *          <code>null</code>
     */

    protected void setMedian(java.math.BigDecimal median) {
        nullarg(median, "median score");
        this.median = median;
        return;
    }


    /**
     *  Gets the mode of the score. If this system is based on grades, the 
     *  mode of the output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getMode() {
        return (this.mode);
    }


    /**
     *  Sets the mode.
     *
     *  @param mode a mode
     *  @throws org.osid.NullArgumentException <code>mode</code> is
     *          <code>null</code>
     */

    protected void setMode(java.math.BigDecimal mode) {
        nullarg(mode, "mode");
        this.mode = mode;
        return;
    }


    /**
     *  Gets the root mean square of the score. If this system is based on 
     *  grades, the RMS of the output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getRMS() {
        return (this.rms);
    }


    /**
     *  Sets the rms.
     *
     *  @param rms the root mean square
     *  @throws org.osid.NullArgumentException <code>rms</code> is
     *          <code>null</code>
     */

    protected void setRMS(java.math.BigDecimal rms) {
        nullarg(rms, "root mean square");
        this.rms = rms;
        return;
    }


    /**
     *  Gets the standard deviation. If this system is based on grades, the 
     *  spread of the output scores is returned. 
     *
     *  @return the standard deviation 
     */

    @OSID @Override
    public java.math.BigDecimal getStandardDeviation() {
        return (this.standardDeviation);
    }


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    protected void setStandardDeviation(java.math.BigDecimal standardDeviation) {
        nullarg(standardDeviation, "standard deviation");
        this.standardDeviation = standardDeviation;
        return;
    }


    /**
     *  Gets the sum of the scores. If this system is based on grades, the sum 
     *  of the output scores is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getSum() {
        return (this.sum);
    }


    /**
     *  Sets the sum of the scores.
     *
     *  @param sum the score sum
     *  @throws org.osid.NullArgumentException <code>sum</code> is
     *          <code>null</code>
     */

    protected void setSum(java.math.BigDecimal sum) {
        nullarg(sum, "sum");
        this.sum = sum;
        return;
    }


    /**
     *  Tests if this gradebookColumnSummary supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnSummaryRecordType a gradebook column summary record type 
     *  @return <code>true</code> if the gradebookColumnSummaryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        for (org.osid.grading.records.GradebookColumnSummaryRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradebookColumnSummary</code> record <code>Type</code>.
     *
     *  @param  gradebookColumnSummaryRecordType the gradebook column summary record type 
     *  @return the gradebook column summary record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSummaryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryRecord getGradebookColumnSummaryRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnSummaryRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSummaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSummaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column summary. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnSummaryRecord the gradebook column summary record
     *  @param gradebookColumnSummaryRecordType gradebook column summary record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSummaryRecord</code> or
     *          <code>gradebookColumnSummaryRecordTypegradebookColumnSummary</code> is
     *          <code>null</code>
     */
            
    protected void addGradebookColumnSummaryRecord(org.osid.grading.records.GradebookColumnSummaryRecord gradebookColumnSummaryRecord, 
                                                   org.osid.type.Type gradebookColumnSummaryRecordType) {
        
        nullarg(gradebookColumnSummaryRecord, "gradebook column summary record");
        addRecordType(gradebookColumnSummaryRecordType);
        this.records.add(gradebookColumnSummaryRecord);
        
        return;
    }
}
