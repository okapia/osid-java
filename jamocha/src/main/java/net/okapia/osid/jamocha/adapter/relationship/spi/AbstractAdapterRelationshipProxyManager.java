//
// AbstractRelationshipProxyManager.java
//
//     An adapter for a RelationshipProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RelationshipProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRelationshipProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.relationship.RelationshipProxyManager>
    implements org.osid.relationship.RelationshipProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRelationshipProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRelationshipProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRelationshipProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any family federation is exposed. Federation is exposed when 
     *  a specific family may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  families appears as a single family. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up relationships is supported. 
     *
     *  @return <code> true </code> if relationship lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipLookup() {
        return (getAdapteeManager().supportsRelationshipLookup());
    }


    /**
     *  Tests if querying relationships is supported. 
     *
     *  @return <code> true </code> if relationship query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (getAdapteeManager().supportsRelationshipQuery());
    }


    /**
     *  Tests if searching relationships is supported. 
     *
     *  @return <code> true </code> if relationship search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipSearch() {
        return (getAdapteeManager().supportsRelationshipSearch());
    }


    /**
     *  Tests if relationship administrative service is supported. 
     *
     *  @return <code> true </code> if relationship administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipAdmin() {
        return (getAdapteeManager().supportsRelationshipAdmin());
    }


    /**
     *  Tests if a relationship notification service is supported. 
     *
     *  @return <code> true </code> if relationship notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipNotification() {
        return (getAdapteeManager().supportsRelationshipNotification());
    }


    /**
     *  Tests if a relationship family cataloging service is supported. 
     *
     *  @return <code> true </code> if relationship families are supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipFamily() {
        return (getAdapteeManager().supportsRelationshipFamily());
    }


    /**
     *  Tests if a relationship cataloging service is supported. A 
     *  relationship cataloging service maps relationships to families. 
     *
     *  @return <code> true </code> if relationship families are supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipFamilyAssignment() {
        return (getAdapteeManager().supportsRelationshipFamilyAssignment());
    }


    /**
     *  Tests if a relationship smart family cataloging service is supported. 
     *
     *  @return <code> true </code> if relationship smart families are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipSmartFamily() {
        return (getAdapteeManager().supportsRelationshipSmartFamily());
    }


    /**
     *  Tests if looking up families is supported. 
     *
     *  @return <code> true </code> if family lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyLookup() {
        return (getAdapteeManager().supportsFamilyLookup());
    }


    /**
     *  Tests if querying families is supported. 
     *
     *  @return <code> true </code> if family query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyQuery() {
        return (getAdapteeManager().supportsFamilyQuery());
    }


    /**
     *  Tests if searching families is supported. 
     *
     *  @return <code> true </code> if family search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilySearch() {
        return (getAdapteeManager().supportsFamilySearch());
    }


    /**
     *  Tests if family <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if family administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyAdmin() {
        return (getAdapteeManager().supportsFamilyAdmin());
    }


    /**
     *  Tests if a family notification service is supported. 
     *
     *  @return <code> true </code> if family notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyNotification() {
        return (getAdapteeManager().supportsFamilyNotification());
    }


    /**
     *  Tests for the availability of a family hierarchy traversal service. 
     *
     *  @return <code> true </code> if family hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyHierarchy() {
        return (getAdapteeManager().supportsFamilyHierarchy());
    }


    /**
     *  Tests for the availability of a family hierarchy design service. 
     *
     *  @return <code> true </code> if family hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFamilyHierarchyDesign() {
        return (getAdapteeManager().supportsFamilyHierarchyDesign());
    }


    /**
     *  Tests for the availability of a relationship batch service. 
     *
     *  @return <code> true </code> if a relationship batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipBatch() {
        return (getAdapteeManager().supportsRelationshipBatch());
    }


    /**
     *  Tests if a relationship rules service is supported. 
     *
     *  @return <code> true </code> if relationship rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipRules() {
        return (getAdapteeManager().supportsRelationshipRules());
    }


    /**
     *  Gets the supported <code> Relationship </code> record types. 
     *
     *  @return a list containing the supported <code> Relationship </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipRecordTypes() {
        return (getAdapteeManager().getRelationshipRecordTypes());
    }


    /**
     *  Tests if the given <code> Relationship </code> record type is 
     *  supported. 
     *
     *  @param  relationshipRecordType a <code> Type </code> indicating a 
     *          <code> Relationship </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> relationshipRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipRecordType(org.osid.type.Type relationshipRecordType) {
        return (getAdapteeManager().supportsRelationshipRecordType(relationshipRecordType));
    }


    /**
     *  Gets the supported <code> Relationship </code> search record types. 
     *
     *  @return a list containing the supported <code> Relationship </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRelationshipSearchRecordTypes() {
        return (getAdapteeManager().getRelationshipSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Relationship </code> search record type is 
     *  supported. 
     *
     *  @param  relationshipSearchRecordType a <code> Type </code> indicating 
     *          a <code> Relationship </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          relationshipSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRelationshipSearchRecordType(org.osid.type.Type relationshipSearchRecordType) {
        return (getAdapteeManager().supportsRelationshipSearchRecordType(relationshipSearchRecordType));
    }


    /**
     *  Gets the supported <code> Family </code> record types. 
     *
     *  @return a list containing the supported <code> Family </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFamilyRecordTypes() {
        return (getAdapteeManager().getFamilyRecordTypes());
    }


    /**
     *  Tests if the given <code> Family </code> record type is supported. 
     *
     *  @param  familyRecordType a <code> Type </code> indicating a <code> 
     *          Family </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> familyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFamilyRecordType(org.osid.type.Type familyRecordType) {
        return (getAdapteeManager().supportsFamilyRecordType(familyRecordType));
    }


    /**
     *  Gets the supported <code> Family </code> search record types. 
     *
     *  @return a list containing the supported <code> Family </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFamilySearchRecordTypes() {
        return (getAdapteeManager().getFamilySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Family </code> search record type is 
     *  supported. 
     *
     *  @param  familySearchRecordType a <code> Type </code> indicating a 
     *          <code> Family </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> familySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFamilySearchRecordType(org.osid.type.Type familySearchRecordType) {
        return (getAdapteeManager().supportsFamilySearchRecordType(familySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  lookup service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipLookupSession getRelationshipLookupSessionForFamily(org.osid.id.Id familyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipLookupSessionForFamily(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  query service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuerySession getRelationshipQuerySessionForFamily(org.osid.id.Id familyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipQuerySessionForFamily(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  search service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSearchSession getRelationshipSearchSessionForFamily(org.osid.id.Id familyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipSearchSessionForFamily(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  administration service for the given family. 
     *
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipAdminSession getRelationshipAdminSessionForFamily(org.osid.id.Id familyId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipAdminSessionForFamily(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSession(org.osid.relationship.RelationshipReceiver relationshipReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipNotificationSession(relationshipReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the relationship 
     *  notification service for the given family. 
     *
     *  @param  relationshipReceiver the receiver 
     *  @param  familyId the <code> Id </code> of the family 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Family </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> relationshipReceiver, 
     *          familyId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipNotificationSession getRelationshipNotificationSessionForFamily(org.osid.relationship.RelationshipReceiver relationshipReceiver, 
                                                                                                             org.osid.id.Id familyId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipNotificationSessionForFamily(relationshipReceiver, familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup relationship/family 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipFamilySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamily() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilySession getRelationshipFamilySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipFamilySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  relationships to families. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipFamilyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipFamilyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipFamilyAssignmentSession getRelationshipFamilyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipFamilyAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic families of 
     *  retlationships. 
     *
     *  @param  familyId the <code> Id </code> of the <code> Family </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelationshipSmartFamilySession </code> 
     *  @throws org.osid.NotFoundException no family found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> familyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipSmartFamily() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipSmartFamilySession getRelationshipSmartFamilySession(org.osid.id.Id familyId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipSmartFamilySession(familyId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyLookupSession getFamilyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyQuerySession getFamilyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilySearchSession getFamilySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFamilyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyAdminSession getFamilyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  notification service. 
     *
     *  @param  familyReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> FamilyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> familyReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyNotificationSession getFamilyNotificationSession(org.osid.relationship.FamilyReceiver familyReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyNotificationSession(familyReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FamilyHierarchySession </code> for families 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchySession getFamilyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the family 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for families 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFamilyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.FamilyHierarchyDesignSession getFamilyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFamilyHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the relationship batch proxy manager. 
     *
     *  @return a <code> RelationshipBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.batch.RelationshipBatchProxyManager getRelationshipBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipBatchProxyManager());
    }


    /**
     *  Gets the relationship rules proxy manager. 
     *
     *  @return a <code> RelationshipRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipRulesProxyManager getRelationshipRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
