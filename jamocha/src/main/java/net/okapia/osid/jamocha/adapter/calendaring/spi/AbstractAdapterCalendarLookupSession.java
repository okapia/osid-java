//
// AbstractAdapterCalendarLookupSession.java
//
//    A Calendar lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Calendar lookup session adapter.
 */

public abstract class AbstractAdapterCalendarLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.CalendarLookupSession {

    private final org.osid.calendaring.CalendarLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCalendarLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCalendarLookupSession(org.osid.calendaring.CalendarLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Calendar} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCalendars() {
        return (this.session.canLookupCalendars());
    }


    /**
     *  A complete view of the {@code Calendar} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCalendarView() {
        this.session.useComparativeCalendarView();
        return;
    }


    /**
     *  A complete view of the {@code Calendar} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCalendarView() {
        this.session.usePlenaryCalendarView();
        return;
    }

     
    /**
     *  Gets the {@code Calendar} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Calendar} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Calendar} and
     *  retained for compatibility.
     *
     *  @param calendarId {@code Id} of the {@code Calendar}
     *  @return the calendar
     *  @throws org.osid.NotFoundException {@code calendarId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code calendarId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendar(calendarId));
    }


    /**
     *  Gets a {@code CalendarList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  calendars specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Calendars} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  calendarIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Calendar} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code calendarIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByIds(org.osid.id.IdList calendarIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendarsByIds(calendarIds));
    }


    /**
     *  Gets a {@code CalendarList} corresponding to the given
     *  calendar genus {@code Type} which does not include
     *  calendars of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  calendarGenusType a calendar genus type 
     *  @return the returned {@code Calendar} list
     *  @throws org.osid.NullArgumentException
     *          {@code calendarGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByGenusType(org.osid.type.Type calendarGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendarsByGenusType(calendarGenusType));
    }


    /**
     *  Gets a {@code CalendarList} corresponding to the given
     *  calendar genus {@code Type} and include any additional
     *  calendars with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  calendarGenusType a calendar genus type 
     *  @return the returned {@code Calendar} list
     *  @throws org.osid.NullArgumentException
     *          {@code calendarGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByParentGenusType(org.osid.type.Type calendarGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendarsByParentGenusType(calendarGenusType));
    }


    /**
     *  Gets a {@code CalendarList} containing the given
     *  calendar record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  calendarRecordType a calendar record type 
     *  @return the returned {@code Calendar} list
     *  @throws org.osid.NullArgumentException
     *          {@code calendarRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByRecordType(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendarsByRecordType(calendarRecordType));
    }


    /**
     *  Gets a {@code CalendarList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Calendar} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendarsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendarsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Calendars}. 
     *
     *  In plenary mode, the returned list contains all known
     *  calendars or an error results. Otherwise, the returned list
     *  may contain only those calendars that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Calendars} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getCalendars()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCalendars());
    }
}
