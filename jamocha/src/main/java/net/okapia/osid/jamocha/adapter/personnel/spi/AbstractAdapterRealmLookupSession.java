//
// AbstractAdapterRealmLookupSession.java
//
//    A Realm lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.personnel.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Realm lookup session adapter.
 */

public abstract class AbstractAdapterRealmLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.personnel.RealmLookupSession {

    private final org.osid.personnel.RealmLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRealmLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRealmLookupSession(org.osid.personnel.RealmLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Realm} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRealms() {
        return (this.session.canLookupRealms());
    }


    /**
     *  A complete view of the {@code Realm} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRealmView() {
        this.session.useComparativeRealmView();
        return;
    }


    /**
     *  A complete view of the {@code Realm} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRealmView() {
        this.session.usePlenaryRealmView();
        return;
    }

     
    /**
     *  Gets the {@code Realm} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Realm} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Realm} and
     *  retained for compatibility.
     *
     *  @param realmId {@code Id} of the {@code Realm}
     *  @return the realm
     *  @throws org.osid.NotFoundException {@code realmId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code realmId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm(org.osid.id.Id realmId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealm(realmId));
    }


    /**
     *  Gets a {@code RealmList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  realms specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Realms} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  realmIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Realm} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code realmIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByIds(org.osid.id.IdList realmIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealmsByIds(realmIds));
    }


    /**
     *  Gets a {@code RealmList} corresponding to the given
     *  realm genus {@code Type} which does not include
     *  realms of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  realmGenusType a realm genus type 
     *  @return the returned {@code Realm} list
     *  @throws org.osid.NullArgumentException
     *          {@code realmGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByGenusType(org.osid.type.Type realmGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealmsByGenusType(realmGenusType));
    }


    /**
     *  Gets a {@code RealmList} corresponding to the given
     *  realm genus {@code Type} and include any additional
     *  realms with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  realmGenusType a realm genus type 
     *  @return the returned {@code Realm} list
     *  @throws org.osid.NullArgumentException
     *          {@code realmGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByParentGenusType(org.osid.type.Type realmGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealmsByParentGenusType(realmGenusType));
    }


    /**
     *  Gets a {@code RealmList} containing the given
     *  realm record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  realmRecordType a realm record type 
     *  @return the returned {@code Realm} list
     *  @throws org.osid.NullArgumentException
     *          {@code realmRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByRecordType(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealmsByRecordType(realmRecordType));
    }


    /**
     *  Gets a {@code RealmList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Realm} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealmsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Realms}. 
     *
     *  In plenary mode, the returned list contains all known
     *  realms or an error results. Otherwise, the returned list
     *  may contain only those realms that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Realms} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRealms());
    }
}
