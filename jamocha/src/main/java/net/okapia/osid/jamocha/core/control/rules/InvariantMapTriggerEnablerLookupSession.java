//
// InvariantMapTriggerEnablerLookupSession
//
//    Implements a TriggerEnabler lookup service backed by a fixed collection of
//    triggerEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a TriggerEnabler lookup service backed by a fixed
 *  collection of trigger enablers. The trigger enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapTriggerEnablerLookupSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapTriggerEnablerLookupSession</code> with no
     *  trigger enablers.
     *  
     *  @param system the system
     *  @throws org.osid.NullArgumnetException {@code system} is
     *          {@code null}
     */

    public InvariantMapTriggerEnablerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTriggerEnablerLookupSession</code> with a single
     *  trigger enabler.
     *  
     *  @param system the system
     *  @param triggerEnabler a single trigger enabler
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnabler} is <code>null</code>
     */

      public InvariantMapTriggerEnablerLookupSession(org.osid.control.System system,
                                               org.osid.control.rules.TriggerEnabler triggerEnabler) {
        this(system);
        putTriggerEnabler(triggerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTriggerEnablerLookupSession</code> using an array
     *  of trigger enablers.
     *  
     *  @param system the system
     *  @param triggerEnablers an array of trigger enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnablers} is <code>null</code>
     */

      public InvariantMapTriggerEnablerLookupSession(org.osid.control.System system,
                                               org.osid.control.rules.TriggerEnabler[] triggerEnablers) {
        this(system);
        putTriggerEnablers(triggerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTriggerEnablerLookupSession</code> using a
     *  collection of trigger enablers.
     *
     *  @param system the system
     *  @param triggerEnablers a collection of trigger enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code triggerEnablers} is <code>null</code>
     */

      public InvariantMapTriggerEnablerLookupSession(org.osid.control.System system,
                                               java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers) {
        this(system);
        putTriggerEnablers(triggerEnablers);
        return;
    }
}
