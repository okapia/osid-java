//
// AbstractProxy.java
//
//     Defines a Proxy.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.proxy.proxy.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Proxy</code>.
 */

public abstract class AbstractProxy
    extends net.okapia.osid.jamocha.spi.AbstractOsidResult
    implements org.osid.proxy.Proxy {

    private org.osid.authentication.process.Authentication authentication;
    private org.osid.authentication.Agent agent;
    private org.osid.locale.Locale locale;
    private java.util.Date date;
    private java.math.BigDecimal rate;
    private org.osid.type.Type formatType;
    
    private final java.util.Collection<org.osid.proxy.records.ProxyRecord> records = new java.util.LinkedHashSet<>();

    
    /**
     *  Tests if an authentication is available. 
     *
     *  @return <code> true </code> if an <code> Authentication </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAuthentication() {
        return (this.authentication != null);
    }


    /**
     *  Gets the <code> Authentication </code> for this proxy. 
     *
     *  @return the authentication 
     *  @throws org.osid.IllegalStateException <code> hasAuthentication() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.Authentication getAuthentication() {
        if (!hasAuthentication()) {
            throw new org.osid.IllegalStateException("hasAuthentication() is false");
        }

        return (this.authentication);
    }


    /**
     *  Sets the authentication.
     *
     *  @param authentication the authentication
     *  @throws org.osid.NullArgumentException
     *          <code>authentication</code> is <code>null</code.
     */

    protected void setAuthentication(org.osid.authentication.process.Authentication authentication) {
        nullarg(authentication, "authentication");
        this.authentication = authentication;
        return;
    }


    /**
     *  Tests if an effective agent is available. 
     *
     *  @return <code> true </code> if an effective agent is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasEffectiveAgent() {
        return (this.agent != null);
    }


    /**
     *  Gets the effective <code> Agent Id </code> for this proxy. 
     *
     *  @return the effective agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveAgent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEffectiveAgentId() {
        if (!hasEffectiveAgent()) {
            throw new org.osid.IllegalStateException("hasEffectiveAgent() is false");
        }

        return (this.agent.getId());
    }


    /**
     *  Gets the effective <code> Agent </code> for this proxy. 
     *
     *  @return the effective agent
     *  @throws org.osid.IllegalStateException <code> hasEffectiveAgent() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.authentication.Agent getEffectiveAgent()
        throws org.osid.OperationFailedException {

        if (!hasEffectiveAgent()) {
            throw new org.osid.IllegalStateException("hasEffectiveAgent() is false");
        }

        return (this.agent);
    }


    /**
     *  Sets the effective agent.
     *
     *  @param agent the agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code.
     */

    protected void setEffectiveAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests if an effective date is available. 
     *
     *  @return <code> true </code> if an effective date is available,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean hasEffectiveDate() {
        return (this.date != null);
    }


    /**
     *  Gets the effective date. 
     *
     *  @return the effective date 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.util.Date getEffectiveDate() {
        if (!hasEffectiveDate()) {
            throw new org.osid.IllegalStateException("hasEffectiveDate() is false");
        }

        return (this.date);
    }


    /**
     *  Gets the rate of the clock. 
     *
     *  @return the rate 
     *  @throws org.osid.IllegalStateException <code> hasEffectiveDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getEffectiveClockRate() {
        if (!hasEffectiveDate()) {
            throw new org.osid.IllegalStateException("hasEffectiveDate() is false");
        }

        return (this.rate);
    }


    /**
     *  Sets the effective date. 
     *
     *  @param  date a date 
     *  @param  rate the rate at which the clock should tick from the given 
     *          effective date. 0 is a clock that is fixed 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    protected void setEffectiveDate(java.util.Date date, java.math.BigDecimal rate) {
        nullarg(date, "effective date");
        nullarg(rate, "clock rate");

        this.date = date;
        this.rate = rate;
        
        return;
    }
    

    /**
     *  Gets the locale. 
     *
     *  @return a locale 
     */

    public org.osid.locale.Locale getLocale() {
        return (this.locale);
    }


    /**
     *  Sets the locale.
     *
     *  @param locale the locale
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code.
     */

    protected void setLocale(org.osid.locale.Locale locale) {
        nullarg(locale,"locale");
        this.locale = locale;
        return;
    }


    /**
     *  Tests if a <code> DisplayText </code> format <code> Type </code> is 
     *  available. 
     *
     *  @return <code> true </code> if a format type is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasFormatType() {
        return (this.formatType != null);
    }


    /**
     *  Gets the <code> DisplayText </code> format <code> Type. </code> 
     *
     *  @return the format <code> Type </code> 
     *  @throws org.osid.IllegalStateException <code> hasFormatType() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.Type getFormatType() {
        if (!hasFormatType()) {
            throw new org.osid.IllegalStateException("hasFormatType() is false");
        }

        return (this.formatType);
    }


    /**
     *  Sets the <code> DisplayText </code> format <code>
     *  Type. </code>
     *
     *  @param formatType the format <code> Type </code>
     *  @throws org.osid.NullArgumentException <code>formatType</code>
     *          is <code>null</code>
     */

    protected void setFormatType(org.osid.type.Type formatType) {
        nullarg(formatType, "format type");
        this.formatType = formatType;
        return;
    }


    /**
     *  Tests if this proxy supports the given record
     *  <code>Type</code>.
     *
     *  @param  proxyRecordType a proxy record type 
     *  @return <code>true</code> if the proxyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>proxyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type proxyRecordType) {
        for (org.osid.proxy.records.ProxyRecord record : this.records) {
            if (record.implementsRecordType(proxyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Proxy</code> record <code>Type</code>.
     *
     *  @param  proxyRecordType the proxy record type 
     *  @return the proxy record 
     *  @throws org.osid.NullArgumentException
     *          <code>proxyRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(proxyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.proxy.records.ProxyRecord getProxyRecord(org.osid.type.Type proxyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.proxy.records.ProxyRecord record : this.records) {
            if (record.implementsRecordType(proxyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(proxyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this proxy. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param proxyRecord the proxy record
     *  @param proxyRecordType proxy record type
     *  @throws org.osid.NullArgumentException
     *          <code>proxyRecord</code> or
     *          <code>proxyRecordTypeproxy</code> is
     *          <code>null</code>
     */
            
    protected void addProxyRecord(org.osid.proxy.records.ProxyRecord proxyRecord, 
                                  org.osid.type.Type proxyRecordType) {
        
        nullarg(proxyRecord, "proxy record");
        addRecordType(proxyRecordType);
        this.records.add(proxyRecord);
        
        return;
    }
}
