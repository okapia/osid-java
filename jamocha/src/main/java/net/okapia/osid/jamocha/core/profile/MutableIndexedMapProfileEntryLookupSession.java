//
// MutableIndexedMapProfileEntryLookupSession
//
//    Implements a ProfileEntry lookup service backed by a collection of
//    profileEntries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Implements a ProfileEntry lookup service backed by a collection of
 *  profile entries. The profile entries are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some profile entries may be compatible
 *  with more types than are indicated through these profile entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of profile entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProfileEntryLookupSession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractIndexedMapProfileEntryLookupSession
    implements org.osid.profile.ProfileEntryLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapProfileEntryLookupSession} with no profile entries.
     *
     *  @param profile the profile
     *  @throws org.osid.NullArgumentException {@code profile}
     *          is {@code null}
     */

      public MutableIndexedMapProfileEntryLookupSession(org.osid.profile.Profile profile) {
        setProfile(profile);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileEntryLookupSession} with a
     *  single profile entry.
     *  
     *  @param profile the profile
     *  @param  profileEntry a single profileEntry
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileEntry} is {@code null}
     */

    public MutableIndexedMapProfileEntryLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileEntry profileEntry) {
        this(profile);
        putProfileEntry(profileEntry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileEntryLookupSession} using an
     *  array of profile entries.
     *
     *  @param profile the profile
     *  @param  profileEntries an array of profile entries
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileEntries} is {@code null}
     */

    public MutableIndexedMapProfileEntryLookupSession(org.osid.profile.Profile profile,
                                                  org.osid.profile.ProfileEntry[] profileEntries) {
        this(profile);
        putProfileEntries(profileEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProfileEntryLookupSession} using a
     *  collection of profile entries.
     *
     *  @param profile the profile
     *  @param  profileEntries a collection of profile entries
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code profileEntries} is {@code null}
     */

    public MutableIndexedMapProfileEntryLookupSession(org.osid.profile.Profile profile,
                                                  java.util.Collection<? extends org.osid.profile.ProfileEntry> profileEntries) {

        this(profile);
        putProfileEntries(profileEntries);
        return;
    }
    

    /**
     *  Makes a {@code ProfileEntry} available in this session.
     *
     *  @param  profileEntry a profile entry
     *  @throws org.osid.NullArgumentException {@code profileEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putProfileEntry(org.osid.profile.ProfileEntry profileEntry) {
        super.putProfileEntry(profileEntry);
        return;
    }


    /**
     *  Makes an array of profile entries available in this session.
     *
     *  @param  profileEntries an array of profile entries
     *  @throws org.osid.NullArgumentException {@code profileEntries{@code 
     *          is {@code null}
     */

    @Override
    public void putProfileEntries(org.osid.profile.ProfileEntry[] profileEntries) {
        super.putProfileEntries(profileEntries);
        return;
    }


    /**
     *  Makes collection of profile entries available in this session.
     *
     *  @param  profileEntries a collection of profile entries
     *  @throws org.osid.NullArgumentException {@code profileEntry{@code  is
     *          {@code null}
     */

    @Override
    public void putProfileEntries(java.util.Collection<? extends org.osid.profile.ProfileEntry> profileEntries) {
        super.putProfileEntries(profileEntries);
        return;
    }


    /**
     *  Removes a ProfileEntry from this session.
     *
     *  @param profileEntryId the {@code Id} of the profile entry
     *  @throws org.osid.NullArgumentException {@code profileEntryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProfileEntry(org.osid.id.Id profileEntryId) {
        super.removeProfileEntry(profileEntryId);
        return;
    }    
}
