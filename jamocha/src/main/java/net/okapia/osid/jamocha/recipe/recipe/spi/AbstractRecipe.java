//
// AbstractRecipe.java
//
//     Defines a Recipe.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Recipe</code>.
 */

public abstract class AbstractRecipe
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObject
    implements org.osid.recipe.Recipe {

    private org.osid.calendaring.Duration totalEstimatedDuration;
    private final java.util.Collection<org.osid.repository.Asset> assets = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.recipe.records.RecipeRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the overall time required for this recipe. 
     *
     *  @return the estimated duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalEstimatedDuration() {
        return (this.totalEstimatedDuration);
    }


    /**
     *  Sets the total estimated duration.
     *
     *  @param duration a total estimated duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setTotalEstimatedDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.totalEstimatedDuration = duration;
        return;
    }


    /**
     *  Gets any asset <code> Ids </code> for the goal of the recipe. 
     *
     *  @return the asset <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        try {
            org.osid.repository.AssetList assets = getAssets();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(assets));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any assets for the goal of the recipe. 
     *
     *  @return the assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assets));
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException
     *          <code>asset</code> is <code>null</code>
     */

    protected void addAsset(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");
        this.assets.add(asset);
        return;
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException
     *          <code>assets</code> is <code>null</code>
     */

    protected void setAssets(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");
        this.assets.clear();
        this.assets.addAll(assets);
        return;
    }


    /**
     *  Tests if this recipe supports the given record
     *  <code>Type</code>.
     *
     *  @param  recipeRecordType a recipe record type 
     *  @return <code>true</code> if the recipeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recipeRecordType) {
        for (org.osid.recipe.records.RecipeRecord record : this.records) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Recipe</code>
     *  record <code>Type</code>.
     *
     *  @param  recipeRecordType the recipe record type 
     *  @return the recipe record 
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recipeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.RecipeRecord getRecipeRecord(org.osid.type.Type recipeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.RecipeRecord record : this.records) {
            if (record.implementsRecordType(recipeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recipeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recipe. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param recipeRecord the recipe record
     *  @param recipeRecordType recipe record type
     *  @throws org.osid.NullArgumentException
     *          <code>recipeRecord</code> or
     *          <code>recipeRecordTyperecipe</code> is
     *          <code>null</code>
     */
            
    protected void addRecipeRecord(org.osid.recipe.records.RecipeRecord recipeRecord, 
                                   org.osid.type.Type recipeRecordType) {

        nullarg(recipeRecord, "recipe record");
        addRecordType(recipeRecordType);
        this.records.add(recipeRecord);
        
        return;
    }
}
