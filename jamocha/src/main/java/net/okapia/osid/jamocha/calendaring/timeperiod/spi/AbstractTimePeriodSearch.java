//
// AbstractTimePeriodSearch.java
//
//     A template for making a TimePeriod Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing time period searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractTimePeriodSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.TimePeriodSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.TimePeriodSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.TimePeriodSearchOrder timePeriodSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of time periods. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  timePeriodIds list of time periods
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongTimePeriods(org.osid.id.IdList timePeriodIds) {
        while (timePeriodIds.hasNext()) {
            try {
                this.ids.add(timePeriodIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongTimePeriods</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of time period Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getTimePeriodIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  timePeriodSearchOrder time period search order 
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>timePeriodSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderTimePeriodResults(org.osid.calendaring.TimePeriodSearchOrder timePeriodSearchOrder) {
	this.timePeriodSearchOrder = timePeriodSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.TimePeriodSearchOrder getTimePeriodSearchOrder() {
	return (this.timePeriodSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given time period search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a time period implementing the requested record.
     *
     *  @param timePeriodSearchRecordType a time period search record
     *         type
     *  @return the time period search record
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodSearchRecord getTimePeriodSearchRecord(org.osid.type.Type timePeriodSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.records.TimePeriodSearchRecord record : this.records) {
            if (record.implementsRecordType(timePeriodSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this time period search. 
     *
     *  @param timePeriodSearchRecord time period search record
     *  @param timePeriodSearchRecordType timePeriod search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTimePeriodSearchRecord(org.osid.calendaring.records.TimePeriodSearchRecord timePeriodSearchRecord, 
                                           org.osid.type.Type timePeriodSearchRecordType) {

        addRecordType(timePeriodSearchRecordType);
        this.records.add(timePeriodSearchRecord);        
        return;
    }
}
