//
// InvariantMapProxyCredentialLookupSession
//
//    Implements a Credential lookup service backed by a fixed
//    collection of credentials. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Credential lookup service backed by a fixed
 *  collection of credentials. The credentials are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCredentialLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCredentialLookupSession} with no
     *  credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCredentialLookupSession} with a single
     *  credential.
     *
     *  @param courseCatalog the course catalog
     *  @param credential a single credential
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credential} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Credential credential, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredential(credential);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCredentialLookupSession} using
     *  an array of credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param credentials an array of credentials
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentials} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.program.Credential[] credentials, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentials(credentials);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCredentialLookupSession} using a
     *  collection of credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param credentials a collection of credentials
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentials} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.program.Credential> credentials,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentials(credentials);
        return;
    }
}
