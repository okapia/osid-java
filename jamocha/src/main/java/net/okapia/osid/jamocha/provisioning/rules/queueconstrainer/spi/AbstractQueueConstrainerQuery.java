//
// AbstractQueueConstrainerQuery.java
//
//     A template for making a QueueConstrainer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for queue constrainers.
 */

public abstract class AbstractQueueConstrainerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerQuery
    implements org.osid.provisioning.rules.QueueConstrainerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueConstrainerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches queues of the given size limit inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSizeLimit(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches queues with any known size limit. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySizeLimit(boolean match) {
        return;
    }


    /**
     *  Clears the size limit query terms. 
     */

    @OSID @Override
    public void clearSizeLimitTerms() {
        return;
    }


    /**
     *  Matches constraints that require provisions. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequiresProvisions(boolean match) {
        return;
    }


    /**
     *  Clears the requires provisions query terms. 
     */

    @OSID @Override
    public void clearRequiresProvisionsTerms() {
        return;
    }


    /**
     *  Sets the required provision pool <code> Id </code> for this query. 
     *
     *  @param  poolId a pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRequiredProvisionPoolId(org.osid.id.Id poolId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the required provision pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequiredProvisionPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a required provision pool query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequiredProvisionPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Pool. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the required provision pool query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequiredProvisionPoolQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getRequiredProvisionPoolQuery() {
        throw new org.osid.UnimplementedException("supportsRequiredProvisionPoolQuery() is false");
    }


    /**
     *  Matches queues that have any required provision pool. 
     *
     *  @param  match <code> true </code> to match queues with any required 
     *          provision pool, <code> false </code> to match queues with no 
     *          required provision pool 
     */

    @OSID @Override
    public void matchAnyRequiredProvisionPool(boolean match) {
        return;
    }


    /**
     *  Clears the required provision pool query terms. 
     */

    @OSID @Override
    public void clearRequiredProvisionPoolTerms() {
        return;
    }


    /**
     *  Matches mapped to a queue. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getRuledQueueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueQuery() is false");
    }


    /**
     *  Matches mapped to any queue. 
     *
     *  @param  match <code> true </code> for mapped to any queue, <code> 
     *          false </code> to match mapped to no queues 
     */

    @OSID @Override
    public void matchAnyRuledQueue(boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearRuledQueueTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given queue constrainer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue constrainer implementing the requested record.
     *
     *  @param queueConstrainerRecordType a queue constrainer record type
     *  @return the queue constrainer query record
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerQueryRecord getQueueConstrainerQueryRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueConstrainerQueryRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue constrainer query. 
     *
     *  @param queueConstrainerQueryRecord queue constrainer query record
     *  @param queueConstrainerRecordType queueConstrainer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueConstrainerQueryRecord(org.osid.provisioning.rules.records.QueueConstrainerQueryRecord queueConstrainerQueryRecord, 
                                          org.osid.type.Type queueConstrainerRecordType) {

        addRecordType(queueConstrainerRecordType);
        nullarg(queueConstrainerQueryRecord, "queue constrainer query record");
        this.records.add(queueConstrainerQueryRecord);        
        return;
    }
}
