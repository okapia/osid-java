//
// InvariantMapSiteLookupSession
//
//    Implements a Site lookup service backed by a fixed collection of
//    sites.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Site lookup service backed by a fixed
 *  collection of sites. The sites are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSiteLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapSiteLookupSession
    implements org.osid.installation.SiteLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSiteLookupSession</code> with no
     *  sites.
     */

    public InvariantMapSiteLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSiteLookupSession</code> with a single
     *  site.
     *  
     *  @throws org.osid.NullArgumentException {@code site}
     *          is <code>null</code>
     */

    public InvariantMapSiteLookupSession(org.osid.installation.Site site) {
        putSite(site);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSiteLookupSession</code> using an array
     *  of sites.
     *  
     *  @throws org.osid.NullArgumentException {@code sites}
     *          is <code>null</code>
     */

    public InvariantMapSiteLookupSession(org.osid.installation.Site[] sites) {
        putSites(sites);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSiteLookupSession</code> using a
     *  collection of sites.
     *
     *  @throws org.osid.NullArgumentException {@code sites}
     *          is <code>null</code>
     */

    public InvariantMapSiteLookupSession(java.util.Collection<? extends org.osid.installation.Site> sites) {
        putSites(sites);
        return;
    }
}
