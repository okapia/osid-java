//
// AbstractMapPriceEnablerLookupSession
//
//    A simple framework for providing a PriceEnabler lookup service
//    backed by a fixed collection of price enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PriceEnabler lookup service backed by a
 *  fixed collection of price enablers. The price enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PriceEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPriceEnablerLookupSession
    extends net.okapia.osid.jamocha.ordering.rules.spi.AbstractPriceEnablerLookupSession
    implements org.osid.ordering.rules.PriceEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ordering.rules.PriceEnabler> priceEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ordering.rules.PriceEnabler>());


    /**
     *  Makes a <code>PriceEnabler</code> available in this session.
     *
     *  @param  priceEnabler a price enabler
     *  @throws org.osid.NullArgumentException <code>priceEnabler<code>
     *          is <code>null</code>
     */

    protected void putPriceEnabler(org.osid.ordering.rules.PriceEnabler priceEnabler) {
        this.priceEnablers.put(priceEnabler.getId(), priceEnabler);
        return;
    }


    /**
     *  Makes an array of price enablers available in this session.
     *
     *  @param  priceEnablers an array of price enablers
     *  @throws org.osid.NullArgumentException <code>priceEnablers<code>
     *          is <code>null</code>
     */

    protected void putPriceEnablers(org.osid.ordering.rules.PriceEnabler[] priceEnablers) {
        putPriceEnablers(java.util.Arrays.asList(priceEnablers));
        return;
    }


    /**
     *  Makes a collection of price enablers available in this session.
     *
     *  @param  priceEnablers a collection of price enablers
     *  @throws org.osid.NullArgumentException <code>priceEnablers<code>
     *          is <code>null</code>
     */

    protected void putPriceEnablers(java.util.Collection<? extends org.osid.ordering.rules.PriceEnabler> priceEnablers) {
        for (org.osid.ordering.rules.PriceEnabler priceEnabler : priceEnablers) {
            this.priceEnablers.put(priceEnabler.getId(), priceEnabler);
        }

        return;
    }


    /**
     *  Removes a PriceEnabler from this session.
     *
     *  @param  priceEnablerId the <code>Id</code> of the price enabler
     *  @throws org.osid.NullArgumentException <code>priceEnablerId<code> is
     *          <code>null</code>
     */

    protected void removePriceEnabler(org.osid.id.Id priceEnablerId) {
        this.priceEnablers.remove(priceEnablerId);
        return;
    }


    /**
     *  Gets the <code>PriceEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  priceEnablerId <code>Id</code> of the <code>PriceEnabler</code>
     *  @return the priceEnabler
     *  @throws org.osid.NotFoundException <code>priceEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>priceEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnabler getPriceEnabler(org.osid.id.Id priceEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.rules.PriceEnabler priceEnabler = this.priceEnablers.get(priceEnablerId);
        if (priceEnabler == null) {
            throw new org.osid.NotFoundException("priceEnabler not found: " + priceEnablerId);
        }

        return (priceEnabler);
    }


    /**
     *  Gets all <code>PriceEnablers</code>. In plenary mode, the returned
     *  list contains all known priceEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  priceEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PriceEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.rules.priceenabler.ArrayPriceEnablerList(this.priceEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.priceEnablers.clear();
        super.close();
        return;
    }
}
