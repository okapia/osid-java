//
// InvariantMapProxyCanonicalUnitLookupSession
//
//    Implements a CanonicalUnit lookup service backed by a fixed
//    collection of canonicalUnits. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a CanonicalUnit lookup service backed by a fixed
 *  collection of canonical units. The canonical units are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractMapCanonicalUnitLookupSession
    implements org.osid.offering.CanonicalUnitLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCanonicalUnitLookupSession} with no
     *  canonical units.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCanonicalUnitLookupSession} with a single
     *  canonical unit.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnit a single canonical unit
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnit} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.CanonicalUnit canonicalUnit, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnit(canonicalUnit);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCanonicalUnitLookupSession} using
     *  an array of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnits an array of canonical units
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.CanonicalUnit[] canonicalUnits, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCanonicalUnitLookupSession} using a
     *  collection of canonical units.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnits a collection of canonical units
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnits} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.CanonicalUnit> canonicalUnits,
                                                  org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnits(canonicalUnits);
        return;
    }
}
