//
// MutableIndexedMapJobProcessorEnablerLookupSession
//
//    Implements a JobProcessorEnabler lookup service backed by a collection of
//    jobProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements a JobProcessorEnabler lookup service backed by a collection of
 *  job processor enablers. The job processor enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some job processor enablers may be compatible
 *  with more types than are indicated through these job processor enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of job processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractIndexedMapJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapJobProcessorEnablerLookupSession} with no job processor enablers.
     *
     *  @param foundry the foundry
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is {@code null}
     */

      public MutableIndexedMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry) {
        setFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapJobProcessorEnablerLookupSession} with a
     *  single job processor enabler.
     *  
     *  @param foundry the foundry
     *  @param  jobProcessorEnabler a single jobProcessorEnabler
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnabler} is {@code null}
     */

    public MutableIndexedMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        this(foundry);
        putJobProcessorEnabler(jobProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapJobProcessorEnablerLookupSession} using an
     *  array of job processor enablers.
     *
     *  @param foundry the foundry
     *  @param  jobProcessorEnablers an array of job processor enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers) {
        this(foundry);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapJobProcessorEnablerLookupSession} using a
     *  collection of job processor enablers.
     *
     *  @param foundry the foundry
     *  @param  jobProcessorEnablers a collection of job processor enablers
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code jobProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapJobProcessorEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers) {

        this(foundry);
        putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }
    

    /**
     *  Makes a {@code JobProcessorEnabler} available in this session.
     *
     *  @param  jobProcessorEnabler a job processor enabler
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putJobProcessorEnabler(org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        super.putJobProcessorEnabler(jobProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of job processor enablers available in this session.
     *
     *  @param  jobProcessorEnablers an array of job processor enablers
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putJobProcessorEnablers(org.osid.resourcing.rules.JobProcessorEnabler[] jobProcessorEnablers) {
        super.putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of job processor enablers available in this session.
     *
     *  @param  jobProcessorEnablers a collection of job processor enablers
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putJobProcessorEnablers(java.util.Collection<? extends org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablers) {
        super.putJobProcessorEnablers(jobProcessorEnablers);
        return;
    }


    /**
     *  Removes a JobProcessorEnabler from this session.
     *
     *  @param jobProcessorEnablerId the {@code Id} of the job processor enabler
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId) {
        super.removeJobProcessorEnabler(jobProcessorEnablerId);
        return;
    }    
}
