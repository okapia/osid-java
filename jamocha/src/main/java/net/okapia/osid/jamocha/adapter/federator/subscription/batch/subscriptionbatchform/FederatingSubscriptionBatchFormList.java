//
// FederatingSubscriptionBatchFormList.java
//
//     Interface for federating lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.subscription.batch.subscriptionbatchform;


/**
 *  Federating subscriptionbatchform lists.
 */

public interface FederatingSubscriptionBatchFormList
    extends org.osid.subscription.batch.SubscriptionBatchFormList {


    /**
     *  Adds a <code>SubscriptionBatchFormList</code> to this
     *  <code>SubscriptionBatchFormList</code> using the default buffer size.
     *
     *  @param subscriptionBatchFormList the <code>SubscriptionBatchFormList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionBatchFormList</code> is <code>null</code>
     */

    public void addSubscriptionBatchFormList(org.osid.subscription.batch.SubscriptionBatchFormList subscriptionBatchFormList);


    /**
     *  Adds a collection of <code>SubscriptionBatchFormLists</code> to this
     *  <code>SubscriptionBatchFormList</code>.
     *
     *  @param subscriptionBatchFormLists the <code>SubscriptionBatchFormList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionBatchFormLists</code> is <code>null</code>
     */

    public void addSubscriptionBatchFormLists(java.util.Collection<org.osid.subscription.batch.SubscriptionBatchFormList> subscriptionBatchFormLists);

        
    /**
     *  No more. Invoked when no more lists are to be added.
     */

    public void noMore();


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    public boolean isOperational();


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    public boolean isEmpty();
}
