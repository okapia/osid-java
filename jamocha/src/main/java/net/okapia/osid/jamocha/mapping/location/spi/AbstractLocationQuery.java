//
// AbstractLocationQuery.java
//
//     A template for making a Location Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for locations.
 */

public abstract class AbstractLocationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.mapping.LocationQuery {

    private final java.util.Collection<org.osid.mapping.records.LocationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches locations containing the specified <code> Coordinate. </code> 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        return;
    }


    /**
     *  Matches locations that have any coordinate assignment. 
     *
     *  @param  match <code> true </code> to match locations with any 
     *          coordinate, <code> false </code> to match locations with no 
     *          coordinates 
     */

    @OSID @Override
    public void matchAnyCoordinate(boolean match) {
        return;
    }


    /**
     *  Clears the coordinate query terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        return;
    }


    /**
     *  Matches locations containing the specified <code> SpatialUnit. </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearContainedSpatialUnitTerms() {
        return;
    }


    /**
     *  Matches locations overlapping with the specified <code> SpatialUnit. 
     *  </code> 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverlappingSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the overlapping spatial unit terms. 
     */

    @OSID @Override
    public void clearOverlappingSpatialUnitTerms() {
        return;
    }


    /**
     *  Matches locations that have any spatial unit assignment. 
     *
     *  @param  match <code> true </code> to match locations with any 
     *          boundary, <code> false </code> to match locations with no 
     *          boundaries 
     */

    @OSID @Override
    public void matchAnySpatialUnit(boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearSpatialUnitTerms() {
        return;
    }


    /**
     *  Sets the route <code> Id </code> for this query to match locations 
     *  along the given route. 
     *
     *  @param  routeId the route <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> routeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id routeId, boolean match) {
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches locations that are used on any route. 
     *
     *  @param  match <code> true </code> to match locations on any route, 
     *          <code> false </code> to match locations on no routes 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match locations 
     *  along the given path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches locations that exist along any path. 
     *
     *  @param  match <code> true </code> to match locations on any path, 
     *          <code> false </code> to match locations on no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query to match locations 
     *  contained within the given location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingLocationId(org.osid.id.Id locationId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the pcontaining location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearContainingLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a parent location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getContainingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsContainingLocationQuery() is false");
    }


    /**
     *  Matches locations that have any ancestor. 
     *
     *  @param  match <code> true </code> to match locations with any parent 
     *          location, <code> false </code> to match root locations 
     */

    @OSID @Override
    public void matchAnyContainingLocation(boolean match) {
        return;
    }


    /**
     *  Clears the containing location query terms. 
     */

    @OSID @Override
    public void clearContainingLocationTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query to match locations 
     *  containing the given location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedLocationId(org.osid.id.Id locationId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the contained location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearContainedLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contained location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getContainedLocationQuery() {
        throw new org.osid.UnimplementedException("supportsContainedLocationQuery() is false");
    }


    /**
     *  Matches locations that have any children. 
     *
     *  @param  match <code> true </code> to match locations containing any 
     *          other location, <code> false </code> to match empty locations 
     */

    @OSID @Override
    public void matchAnyContainedLocation(boolean match) {
        return;
    }


    /**
     *  Clears the contained location query terms. 
     */

    @OSID @Override
    public void clearContainedLocationTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given location query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a location implementing the requested record.
     *
     *  @param locationRecordType a location record type
     *  @return the location query record
     *  @throws org.osid.NullArgumentException
     *          <code>locationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(locationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.LocationQueryRecord getLocationQueryRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationQueryRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this location query. 
     *
     *  @param locationQueryRecord location query record
     *  @param locationRecordType location record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLocationQueryRecord(org.osid.mapping.records.LocationQueryRecord locationQueryRecord, 
                                          org.osid.type.Type locationRecordType) {

        addRecordType(locationRecordType);
        nullarg(locationQueryRecord, "location query record");
        this.records.add(locationQueryRecord);        
        return;
    }
}
