//
// AbstractGradingTransformProxyManager.java
//
//     An adapter for a GradingTransformProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.transform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a GradingTransformProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterGradingTransformProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.grading.transform.GradingTransformProxyManager>
    implements org.osid.grading.transform.GradingTransformProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterGradingTransformProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterGradingTransformProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterGradingTransformProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterGradingTransformProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a grade system transformation service is supported. 
     *
     *  @return true if grade system transformation is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformation() {
        return (getAdapteeManager().supportsGradeSystemTransformation());
    }


    /**
     *  Tests if a grade system transform lookup service is supported. 
     *
     *  @return true if grade system transform lookup is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformLookup() {
        return (getAdapteeManager().supportsGradeSystemTransformLookup());
    }


    /**
     *  Tests if a grade system transform admin service is supported. 
     *
     *  @return <code> true </code> if grade system transform admin is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformAdmin() {
        return (getAdapteeManager().supportsGradeSystemTransformAdmin());
    }


    /**
     *  Tests if a grade system transform notification service is supported. 
     *
     *  @return <code> true </code> if grade system transform notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformNotification() {
        return (getAdapteeManager().supportsGradeSystemTransformNotification());
    }


    /**
     *  Gets the supported <code> GradeSystemTransform </code> record types. 
     *
     *  @return a list containing the supported <code> GradeSystemTransform 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemTransformRecordTypes() {
        return (getAdapteeManager().getGradeSystemTransformRecordTypes());
    }


    /**
     *  Tests if the given <code> GradeSystemTransform </code> record type is 
     *  supported. 
     *
     *  @param  gradeSystemTransformRecordType a <code> Type </code> 
     *          indicating a <code> GradeSystemTransform </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeSystemTransformRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemTransformRecordType(org.osid.type.Type gradeSystemTransformRecordType) {
        return (getAdapteeManager().supportsGradeSystemTransformRecordType(gradeSystemTransformRecordType));
    }


    /**
     *  Gets the session for transforming grades among grade systems. The 
     *  available transformations can be examined through the <code> 
     *  GradeSystemTransformLookupSession. </code> 
     *
     *  @param  sourceGradeSystemId the <code> Id </code> of the source grade 
     *          system 
     *  @param  targetGradeSystemId the <code> Id </code> of the target grade 
     *          system 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformationSession </code> 
     *  @throws org.osid.NotFoundException no transform exists between <code> 
     *          souceGradebookId </code> and <code> targetGradeSystemId 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> sourceGradeSystemId, 
     *          targetGradeSystemIdId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformationSession getGradeSystemTransformationSession(org.osid.id.Id sourceGradeSystemId, 
                                                                                                           org.osid.id.Id targetGradeSystemId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformationSession(sourceGradeSystemId, targetGradeSystemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformLookupSession getGradeSystemTransformLookupSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformLookupSessionForGradebook(gradebookId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformAdminSession getGradeSystemTransformAdminSessionForGradebook(org.osid.id.Id gradebookId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformAdminSessionForGradebook(gradebookId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system transform changes. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @param  proxy a proxy 
     *  @return a <code> GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSession(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformNotificationSession(gradeSystemTransformReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  transform notification service for the given gradebook. 
     *
     *  @param  gradeSystemTransformReceiver the grade system transform 
     *          receiver interface 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @param  proxy a proxy 
     *  @return <code> a GradeSystemTransformNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver, 
     *          gradebookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemTransformNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeSystemTransformNotificationSession getGradeSystemTransformNotificationSessionForGradebook(org.osid.grading.transform.GradeSystemTransformReceiver gradeSystemTransformReceiver, 
                                                                                                                                     org.osid.id.Id gradebookId, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemTransformNotificationSessionForGradebook(gradeSystemTransformReceiver, gradebookId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
