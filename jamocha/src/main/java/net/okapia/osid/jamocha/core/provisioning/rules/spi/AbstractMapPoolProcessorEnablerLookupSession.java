//
// AbstractMapPoolProcessorEnablerLookupSession
//
//    A simple framework for providing a PoolProcessorEnabler lookup service
//    backed by a fixed collection of pool processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PoolProcessorEnabler lookup service backed by a
 *  fixed collection of pool processor enablers. The pool processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPoolProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractPoolProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.PoolProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.PoolProcessorEnabler>());


    /**
     *  Makes a <code>PoolProcessorEnabler</code> available in this session.
     *
     *  @param  poolProcessorEnabler a pool processor enabler
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessorEnabler(org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler) {
        this.poolProcessorEnablers.put(poolProcessorEnabler.getId(), poolProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of pool processor enablers available in this session.
     *
     *  @param  poolProcessorEnablers an array of pool processor enablers
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessorEnablers(org.osid.provisioning.rules.PoolProcessorEnabler[] poolProcessorEnablers) {
        putPoolProcessorEnablers(java.util.Arrays.asList(poolProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of pool processor enablers available in this session.
     *
     *  @param  poolProcessorEnablers a collection of pool processor enablers
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putPoolProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.PoolProcessorEnabler> poolProcessorEnablers) {
        for (org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler : poolProcessorEnablers) {
            this.poolProcessorEnablers.put(poolProcessorEnabler.getId(), poolProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a PoolProcessorEnabler from this session.
     *
     *  @param  poolProcessorEnablerId the <code>Id</code> of the pool processor enabler
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removePoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId) {
        this.poolProcessorEnablers.remove(poolProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>PoolProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  poolProcessorEnablerId <code>Id</code> of the <code>PoolProcessorEnabler</code>
     *  @return the poolProcessorEnabler
     *  @throws org.osid.NotFoundException <code>poolProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnabler getPoolProcessorEnabler(org.osid.id.Id poolProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.PoolProcessorEnabler poolProcessorEnabler = this.poolProcessorEnablers.get(poolProcessorEnablerId);
        if (poolProcessorEnabler == null) {
            throw new org.osid.NotFoundException("poolProcessorEnabler not found: " + poolProcessorEnablerId);
        }

        return (poolProcessorEnabler);
    }


    /**
     *  Gets all <code>PoolProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known poolProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  poolProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PoolProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.ArrayPoolProcessorEnablerList(this.poolProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolProcessorEnablers.clear();
        super.close();
        return;
    }
}
