//
// AbstractConferralSearchOdrer.java
//
//     Defines a ConferralSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ConferralSearchOrder}.
 */

public abstract class AbstractConferralSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.recognition.ConferralSearchOrder {

    private final java.util.Collection<org.osid.recognition.records.ConferralSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the award. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAward(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an award order is available. 
     *
     *  @return <code> true </code> if an award order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSearchOrder() {
        return (false);
    }


    /**
     *  Gets the award order. 
     *
     *  @return the award search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchOrder getAwardSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAwardSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the recipient. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipient(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a recipient order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientSearchOrder() {
        return (false);
    }


    /**
     *  Gets the recipient order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRecipientSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipientSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the convocation. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConvocation(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a convocation order is available. 
     *
     *  @return <code> true </code> if a convocation order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSearchOrder() {
        return (false);
    }


    /**
     *  Gets the convocation order. 
     *
     *  @return the convocation search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchOrder getConvocationSearchOrder() {
        throw new org.osid.UnimplementedException("supportsConvocationSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  conferralRecordType a conferral record type 
     *  @return {@code true} if the conferralRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code conferralRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type conferralRecordType) {
        for (org.osid.recognition.records.ConferralSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  conferralRecordType the conferral record type 
     *  @return the conferral search order record
     *  @throws org.osid.NullArgumentException
     *          {@code conferralRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(conferralRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralSearchOrderRecord getConferralSearchOrderRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConferralSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(conferralRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(conferralRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this conferral. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param conferralRecord the conferral search odrer record
     *  @param conferralRecordType conferral record type
     *  @throws org.osid.NullArgumentException
     *          {@code conferralRecord} or
     *          {@code conferralRecordTypeconferral} is
     *          {@code null}
     */
            
    protected void addConferralRecord(org.osid.recognition.records.ConferralSearchOrderRecord conferralSearchOrderRecord, 
                                     org.osid.type.Type conferralRecordType) {

        addRecordType(conferralRecordType);
        this.records.add(conferralSearchOrderRecord);
        
        return;
    }
}
