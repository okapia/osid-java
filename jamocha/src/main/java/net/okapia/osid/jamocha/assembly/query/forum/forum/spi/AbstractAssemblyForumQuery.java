//
// AbstractAssemblyForumQuery.java
//
//     A ForumQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ForumQuery that stores terms.
 */

public abstract class AbstractAssemblyForumQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.forum.ForumQuery,
               org.osid.forum.ForumQueryInspector,
               org.osid.forum.ForumSearchOrder {

    private final java.util.Collection<org.osid.forum.records.ForumQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.ForumQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.forum.records.ForumSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyForumQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyForumQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the reply <code> Id </code> for this query to match replies 
     *  assigned to forums. 
     *
     *  @param  replyId a reply <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> replyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReplyId(org.osid.id.Id replyId, boolean match) {
        getAssembler().addIdTerm(getReplyIdColumn(), replyId, match);
        return;
    }


    /**
     *  Clears the reply <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReplyIdTerms() {
        getAssembler().clearTerms(getReplyIdColumn());
        return;
    }


    /**
     *  Gets the reply <code> Id </code> terms. 
     *
     *  @return the reply <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReplyIdTerms() {
        return (getAssembler().getIdTerms(getReplyIdColumn()));
    }


    /**
     *  Gets the ReplyId column name.
     *
     * @return the column name
     */

    protected String getReplyIdColumn() {
        return ("reply_id");
    }


    /**
     *  Tests if a reply query is available. 
     *
     *  @return <code> true </code> if a reply query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. 
     *
     *  @return the reply query 
     *  @throws org.osid.UnimplementedException <code> supportsReplyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ReplyQuery getReplyQuery() {
        throw new org.osid.UnimplementedException("supportsReplyQuery() is false");
    }


    /**
     *  Matches forums with any reply. 
     *
     *  @param  match <code> true </code> to match forums with any reply, 
     *          <code> false </code> to match forums with no replies 
     */

    @OSID @Override
    public void matchAnyReply(boolean match) {
        getAssembler().addIdWildcardTerm(getReplyColumn(), match);
        return;
    }


    /**
     *  Clears the reply terms. 
     */

    @OSID @Override
    public void clearReplyTerms() {
        getAssembler().clearTerms(getReplyColumn());
        return;
    }


    /**
     *  Gets the reply terms. 
     *
     *  @return the reply terms 
     */

    @OSID @Override
    public org.osid.forum.ReplyQueryInspector[] getReplyTerms() {
        return (new org.osid.forum.ReplyQueryInspector[0]);
    }


    /**
     *  Gets the Reply column name.
     *
     * @return the column name
     */

    protected String getReplyColumn() {
        return ("reply");
    }


    /**
     *  Sets the post <code> Id </code> for this query to match replies 
     *  assigned to posts. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPostId(org.osid.id.Id postId, boolean match) {
        getAssembler().addIdTerm(getPostIdColumn(), postId, match);
        return;
    }


    /**
     *  Clears the post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostIdTerms() {
        getAssembler().clearTerms(getPostIdColumn());
        return;
    }


    /**
     *  Gets the post <code> Id </code> terms. 
     *
     *  @return the post <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostIdTerms() {
        return (getAssembler().getIdTerms(getPostIdColumn()));
    }


    /**
     *  Gets the PostId column name.
     *
     * @return the column name
     */

    protected String getPostIdColumn() {
        return ("post_id");
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> supportsPostQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.PostQuery getPostQuery() {
        throw new org.osid.UnimplementedException("supportsPostQuery() is false");
    }


    /**
     *  Matches forums with any post. 
     *
     *  @param  match <code> true </code> to match forums with any post, 
     *          <code> false </code> to match forums with no posts 
     */

    @OSID @Override
    public void matchAnyPost(boolean match) {
        getAssembler().addIdWildcardTerm(getPostColumn(), match);
        return;
    }


    /**
     *  Clears the post terms. 
     */

    @OSID @Override
    public void clearPostTerms() {
        getAssembler().clearTerms(getPostColumn());
        return;
    }


    /**
     *  Gets the post terms. 
     *
     *  @return the post terms 
     */

    @OSID @Override
    public org.osid.forum.PostQueryInspector[] getPostTerms() {
        return (new org.osid.forum.PostQueryInspector[0]);
    }


    /**
     *  Gets the Post column name.
     *
     * @return the column name
     */

    protected String getPostColumn() {
        return ("post");
    }


    /**
     *  Sets the forum <code> Id </code> for this query to match forums that 
     *  have the specified forum as an ancestor. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorForumId(org.osid.id.Id forumId, boolean match) {
        getAssembler().addIdTerm(getAncestorForumIdColumn(), forumId, match);
        return;
    }


    /**
     *  Clears the ancestor forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorForumIdTerms() {
        getAssembler().clearTerms(getAncestorForumIdColumn());
        return;
    }


    /**
     *  Gets the ancestor forum <code> Id </code> terms. 
     *
     *  @return the ancestor forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorForumIdTerms() {
        return (getAssembler().getIdTerms(getAncestorForumIdColumn()));
    }


    /**
     *  Gets the AncestorForumId column name.
     *
     * @return the column name
     */

    protected String getAncestorForumIdColumn() {
        return ("ancestor_forum_id");
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorForumQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getAncestorForumQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorForumQuery() is false");
    }


    /**
     *  Matches forums with any ancestor. 
     *
     *  @param  match <code> true </code> to match forums with any ancestor, 
     *          <code> false </code> to match root forums 
     */

    @OSID @Override
    public void matchAnyAncestorForum(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorForumColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor forum terms. 
     */

    @OSID @Override
    public void clearAncestorForumTerms() {
        getAssembler().clearTerms(getAncestorForumColumn());
        return;
    }


    /**
     *  Gets the ancestor forum terms. 
     *
     *  @return the ancestor forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getAncestorForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }


    /**
     *  Gets the AncestorForum column name.
     *
     * @return the column name
     */

    protected String getAncestorForumColumn() {
        return ("ancestor_forum");
    }


    /**
     *  Sets the forum <code> Id </code> for this query to match forums that 
     *  have the specified forum as a descendant. 
     *
     *  @param  forumId a forum <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantForumId(org.osid.id.Id forumId, boolean match) {
        getAssembler().addIdTerm(getDescendantForumIdColumn(), forumId, match);
        return;
    }


    /**
     *  Clears the descendant forum <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantForumIdTerms() {
        getAssembler().clearTerms(getDescendantForumIdColumn());
        return;
    }


    /**
     *  Gets the descendant forum <code> Id </code> terms. 
     *
     *  @return the descendant forum <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantForumIdTerms() {
        return (getAssembler().getIdTerms(getDescendantForumIdColumn()));
    }


    /**
     *  Gets the DescendantForumId column name.
     *
     * @return the column name
     */

    protected String getDescendantForumIdColumn() {
        return ("descendant_forum_id");
    }


    /**
     *  Tests if a <code> ForumQuery </code> is available. 
     *
     *  @return <code> true </code> if a forum query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantForumQuery() {
        return (false);
    }


    /**
     *  Gets the query for a forum. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the forum query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantForumQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumQuery getDescendantForumQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantForumQuery() is false");
    }


    /**
     *  Matches forums with any descendant. 
     *
     *  @param  match <code> true </code> to match forums with any descendant, 
     *          <code> false </code> to match leaf forums 
     */

    @OSID @Override
    public void matchAnyDescendantForum(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantForumColumn(), match);
        return;
    }


    /**
     *  Clears the descendant forum terms. 
     */

    @OSID @Override
    public void clearDescendantForumTerms() {
        getAssembler().clearTerms(getDescendantForumColumn());
        return;
    }


    /**
     *  Gets the descendant forum terms. 
     *
     *  @return the descendant forum terms 
     */

    @OSID @Override
    public org.osid.forum.ForumQueryInspector[] getDescendantForumTerms() {
        return (new org.osid.forum.ForumQueryInspector[0]);
    }


    /**
     *  Gets the DescendantForum column name.
     *
     * @return the column name
     */

    protected String getDescendantForumColumn() {
        return ("descendant_forum");
    }


    /**
     *  Tests if this forum supports the given record
     *  <code>Type</code>.
     *
     *  @param  forumRecordType a forum record type 
     *  @return <code>true</code> if the forumRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type forumRecordType) {
        for (org.osid.forum.records.ForumQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(forumRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  forumRecordType the forum record type 
     *  @return the forum query record 
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumQueryRecord getForumQueryRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  forumRecordType the forum record type 
     *  @return the forum query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumQueryInspectorRecord getForumQueryInspectorRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param forumRecordType the forum record type
     *  @return the forum search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>forumRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(forumRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumSearchOrderRecord getForumSearchOrderRecord(org.osid.type.Type forumRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.ForumSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(forumRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(forumRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this forum. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param forumQueryRecord the forum query record
     *  @param forumQueryInspectorRecord the forum query inspector
     *         record
     *  @param forumSearchOrderRecord the forum search order record
     *  @param forumRecordType forum record type
     *  @throws org.osid.NullArgumentException
     *          <code>forumQueryRecord</code>,
     *          <code>forumQueryInspectorRecord</code>,
     *          <code>forumSearchOrderRecord</code> or
     *          <code>forumRecordTypeforum</code> is
     *          <code>null</code>
     */
            
    protected void addForumRecords(org.osid.forum.records.ForumQueryRecord forumQueryRecord, 
                                      org.osid.forum.records.ForumQueryInspectorRecord forumQueryInspectorRecord, 
                                      org.osid.forum.records.ForumSearchOrderRecord forumSearchOrderRecord, 
                                      org.osid.type.Type forumRecordType) {

        addRecordType(forumRecordType);

        nullarg(forumQueryRecord, "forum query record");
        nullarg(forumQueryInspectorRecord, "forum query inspector record");
        nullarg(forumSearchOrderRecord, "forum search odrer record");

        this.queryRecords.add(forumQueryRecord);
        this.queryInspectorRecords.add(forumQueryInspectorRecord);
        this.searchOrderRecords.add(forumSearchOrderRecord);
        
        return;
    }
}
