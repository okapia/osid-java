//
// UnknownStepProcessorEnabler.java
//
//     Defines an unknown StepProcessorEnabler.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.workflow.rules.stepprocessorenabler;


/**
 *  Defines an unknown <code>StepProcessorEnabler</code>.
 */

public final class UnknownStepProcessorEnabler
    extends net.okapia.osid.jamocha.nil.workflow.rules.stepprocessorenabler.spi.AbstractUnknownStepProcessorEnabler
    implements org.osid.workflow.rules.StepProcessorEnabler {


    /**
     *  Constructs a new <code>UnknownStepProcessorEnabler</code>.
     */

    public UnknownStepProcessorEnabler() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownStepProcessorEnabler</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownStepProcessorEnabler(boolean optional) {
        super(optional);
        addStepProcessorEnablerRecord(new StepProcessorEnablerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown StepProcessorEnabler.
     *
     *  @return an unknown StepProcessorEnabler
     */

    public static org.osid.workflow.rules.StepProcessorEnabler create() {
        return (net.okapia.osid.jamocha.builder.validator.workflow.rules.stepprocessorenabler.StepProcessorEnablerValidator.validateStepProcessorEnabler(new UnknownStepProcessorEnabler()));
    }


    public class StepProcessorEnablerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.workflow.rules.records.StepProcessorEnablerRecord {

        
        protected StepProcessorEnablerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
