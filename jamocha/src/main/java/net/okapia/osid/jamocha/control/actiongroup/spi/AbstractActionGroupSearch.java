//
// AbstractActionGroupSearch.java
//
//     A template for making an ActionGroup Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing action group searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractActionGroupSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.ActionGroupSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.ActionGroupSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.ActionGroupSearchOrder actionGroupSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of action groups. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  actionGroupIds list of action groups
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongActionGroups(org.osid.id.IdList actionGroupIds) {
        while (actionGroupIds.hasNext()) {
            try {
                this.ids.add(actionGroupIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongActionGroups</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of action group Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getActionGroupIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  actionGroupSearchOrder action group search order 
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>actionGroupSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderActionGroupResults(org.osid.control.ActionGroupSearchOrder actionGroupSearchOrder) {
	this.actionGroupSearchOrder = actionGroupSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.ActionGroupSearchOrder getActionGroupSearchOrder() {
	return (this.actionGroupSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given action group search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an action group implementing the requested record.
     *
     *  @param actionGroupSearchRecordType an action group search record
     *         type
     *  @return the action group search record
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupSearchRecord getActionGroupSearchRecord(org.osid.type.Type actionGroupSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.ActionGroupSearchRecord record : this.records) {
            if (record.implementsRecordType(actionGroupSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action group search. 
     *
     *  @param actionGroupSearchRecord action group search record
     *  @param actionGroupSearchRecordType actionGroup search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionGroupSearchRecord(org.osid.control.records.ActionGroupSearchRecord actionGroupSearchRecord, 
                                           org.osid.type.Type actionGroupSearchRecordType) {

        addRecordType(actionGroupSearchRecordType);
        this.records.add(actionGroupSearchRecord);        
        return;
    }
}
