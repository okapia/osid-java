//
// InvariantMapProxyAuthorizationLookupSession
//
//    Implements an Authorization lookup service backed by a fixed
//    collection of authorizations. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements an Authorization lookup service backed by a fixed
 *  collection of authorizations. The authorizations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAuthorizationLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuthorizationLookupSession} with no
     *  authorizations.
     *
     *  @param vault the vault
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.proxy.Proxy proxy) {
        setVault(vault);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAuthorizationLookupSession} with a single
     *  authorization.
     *
     *  @param vault the vault
     *  @param authorization an single authorization
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorization} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Authorization authorization, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putAuthorization(authorization);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuthorizationLookupSession} using
     *  an array of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations an array of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                  org.osid.authorization.Authorization[] authorizations, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putAuthorizations(authorizations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuthorizationLookupSession} using a
     *  collection of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations a collection of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                  java.util.Collection<? extends org.osid.authorization.Authorization> authorizations,
                                                  org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putAuthorizations(authorizations);
        return;
    }
}
