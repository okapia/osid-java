//
// AuctionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AuctionElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the effective element Id.
     *
     *  @return the effective element Id
     */

    public static org.osid.id.Id getEffective() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEffective());
    }


    /**
     *  Gets the start date element Id.
     *
     *  @return the start date element Id
     */

    public static org.osid.id.Id getStartDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getStartDate());
    }


    /**
     *  Gets the end date element Id.
     *
     *  @return the end date element Id
     */

    public static org.osid.id.Id getEndDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getEndDate());
    }


    /**
     *  Gets the date element Id.
     *
     *  @return the date element Id
     */

    public static org.osid.id.Id getDate() {
        return (net.okapia.osid.jamocha.spi.TemporalElements.getDate());
    }
    

    /**
     *  Gets the AuctionElement Id.
     *
     *  @return the auction element Id
     */

    public static org.osid.id.Id getAuctionEntityId() {
        return (makeEntityId("osid.bidding.Auction"));
    }


    /**
     *  Gets the CurrencyType element Id.
     *
     *  @return the CurrencyType element Id
     */

    public static org.osid.id.Id getCurrencyType() {
        return (makeElementId("osid.bidding.auction.CurrencyType"));
    }


    /**
     *  Gets the MinimumBidders element Id.
     *
     *  @return the MinimumBidders element Id
     */

    public static org.osid.id.Id getMinimumBidders() {
        return (makeElementId("osid.bidding.auction.MinimumBidders"));
    }


    /**
     *  Gets the SellerId element Id.
     *
     *  @return the SellerId element Id
     */

    public static org.osid.id.Id getSellerId() {
        return (makeElementId("osid.bidding.auction.SellerId"));
    }


    /**
     *  Gets the Seller element Id.
     *
     *  @return the Seller element Id
     */

    public static org.osid.id.Id getSeller() {
        return (makeElementId("osid.bidding.auction.Seller"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeElementId("osid.bidding.auction.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeElementId("osid.bidding.auction.Item"));
    }


    /**
     *  Gets the LotSize element Id.
     *
     *  @return the LotSize element Id
     */

    public static org.osid.id.Id getLotSize() {
        return (makeElementId("osid.bidding.auction.LotSize"));
    }


    /**
     *  Gets the RemainingItems element Id.
     *
     *  @return the RemainingItems element Id
     */

    public static org.osid.id.Id getRemainingItems() {
        return (makeElementId("osid.bidding.auction.RemainingItems"));
    }


    /**
     *  Gets the ItemLimit element Id.
     *
     *  @return the ItemLimit element Id
     */

    public static org.osid.id.Id getItemLimit() {
        return (makeElementId("osid.bidding.auction.ItemLimit"));
    }


    /**
     *  Gets the StartingPrice element Id.
     *
     *  @return the StartingPrice element Id
     */

    public static org.osid.id.Id getStartingPrice() {
        return (makeElementId("osid.bidding.auction.StartingPrice"));
    }


    /**
     *  Gets the PriceIncrement element Id.
     *
     *  @return the PriceIncrement element Id
     */

    public static org.osid.id.Id getPriceIncrement() {
        return (makeElementId("osid.bidding.auction.PriceIncrement"));
    }


    /**
     *  Gets the ReservePrice element Id.
     *
     *  @return the ReservePrice element Id
     */

    public static org.osid.id.Id getReservePrice() {
        return (makeElementId("osid.bidding.auction.ReservePrice"));
    }


    /**
     *  Gets the BuyoutPrice element Id.
     *
     *  @return the BuyoutPrice element Id
     */

    public static org.osid.id.Id getBuyoutPrice() {
        return (makeElementId("osid.bidding.auction.BuyoutPrice"));
    }


    /**
     *  Gets the BidId element Id.
     *
     *  @return the BidId element Id
     */

    public static org.osid.id.Id getBidId() {
        return (makeQueryElementId("osid.bidding.auction.BidId"));
    }


    /**
     *  Gets the Bid element Id.
     *
     *  @return the Bid element Id
     */

    public static org.osid.id.Id getBid() {
        return (makeQueryElementId("osid.bidding.auction.Bid"));
    }


    /**
     *  Gets the AuctionHouseId element Id.
     *
     *  @return the AuctionHouseId element Id
     */

    public static org.osid.id.Id getAuctionHouseId() {
        return (makeQueryElementId("osid.bidding.auction.AuctionHouseId"));
    }


    /**
     *  Gets the AuctionHouse element Id.
     *
     *  @return the AuctionHouse element Id
     */

    public static org.osid.id.Id getAuctionHouse() {
        return (makeQueryElementId("osid.bidding.auction.AuctionHouse"));
    }
}
