//
// AbstractSummaryQueryInspector.java
//
//     A template for making a SummaryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.summary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for summaries.
 */

public abstract class AbstractSummaryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumQueryInspector
    implements org.osid.financials.SummaryQueryInspector {

    private final java.util.Collection<org.osid.financials.records.SummaryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCreditsTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the debits query terms. 
     *
     *  @return the debits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getDebitsTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the balacnce query terms. 
     *
     *  @return the balance query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getBalanceTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the budget query terms. 
     *
     *  @return the budget query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getBudgetTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the delta query terms. 
     *
     *  @return the delta query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDeltaTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the forecast query terms. 
     *
     *  @return the forecast query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getForecastTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given summary query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a summary implementing the requested record.
     *
     *  @param summaryRecordType a summary record type
     *  @return the summary query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>summaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(summaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.SummaryQueryInspectorRecord getSummaryQueryInspectorRecord(org.osid.type.Type summaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.SummaryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(summaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(summaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this summary query. 
     *
     *  @param summaryQueryInspectorRecord summary query inspector
     *         record
     *  @param summaryRecordType summary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSummaryQueryInspectorRecord(org.osid.financials.records.SummaryQueryInspectorRecord summaryQueryInspectorRecord, 
                                                   org.osid.type.Type summaryRecordType) {

        addRecordType(summaryRecordType);
        nullarg(summaryRecordType, "summary record type");
        this.records.add(summaryQueryInspectorRecord);        
        return;
    }
}
