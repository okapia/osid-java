//
// AbstractMapSettingLookupSession
//
//    A simple framework for providing a Setting lookup service
//    backed by a fixed collection of settings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Setting lookup service backed by a
 *  fixed collection of settings. The settings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Settings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSettingLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractSettingLookupSession
    implements org.osid.control.SettingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Setting> settings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Setting>());


    /**
     *  Makes a <code>Setting</code> available in this session.
     *
     *  @param  setting a setting
     *  @throws org.osid.NullArgumentException <code>setting<code>
     *          is <code>null</code>
     */

    protected void putSetting(org.osid.control.Setting setting) {
        this.settings.put(setting.getId(), setting);
        return;
    }


    /**
     *  Makes an array of settings available in this session.
     *
     *  @param  settings an array of settings
     *  @throws org.osid.NullArgumentException <code>settings<code>
     *          is <code>null</code>
     */

    protected void putSettings(org.osid.control.Setting[] settings) {
        putSettings(java.util.Arrays.asList(settings));
        return;
    }


    /**
     *  Makes a collection of settings available in this session.
     *
     *  @param  settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings<code>
     *          is <code>null</code>
     */

    protected void putSettings(java.util.Collection<? extends org.osid.control.Setting> settings) {
        for (org.osid.control.Setting setting : settings) {
            this.settings.put(setting.getId(), setting);
        }

        return;
    }


    /**
     *  Removes a Setting from this session.
     *
     *  @param  settingId the <code>Id</code> of the setting
     *  @throws org.osid.NullArgumentException <code>settingId<code> is
     *          <code>null</code>
     */

    protected void removeSetting(org.osid.id.Id settingId) {
        this.settings.remove(settingId);
        return;
    }


    /**
     *  Gets the <code>Setting</code> specified by its <code>Id</code>.
     *
     *  @param  settingId <code>Id</code> of the <code>Setting</code>
     *  @return the setting
     *  @throws org.osid.NotFoundException <code>settingId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>settingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Setting getSetting(org.osid.id.Id settingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Setting setting = this.settings.get(settingId);
        if (setting == null) {
            throw new org.osid.NotFoundException("setting not found: " + settingId);
        }

        return (setting);
    }


    /**
     *  Gets all <code>Settings</code>. In plenary mode, the returned
     *  list contains all known settings or an error
     *  results. Otherwise, the returned list may contain only those
     *  settings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Settings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SettingList getSettings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.setting.ArraySettingList(this.settings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.settings.clear();
        super.close();
        return;
    }
}
