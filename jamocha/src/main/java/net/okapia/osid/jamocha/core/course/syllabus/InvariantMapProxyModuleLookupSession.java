//
// InvariantMapProxyModuleLookupSession
//
//    Implements a Module lookup service backed by a fixed
//    collection of modules. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Module lookup service backed by a fixed
 *  collection of modules. The modules are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyModuleLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyModuleLookupSession} with no
     *  modules.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyModuleLookupSession} with a single
     *  module.
     *
     *  @param courseCatalog the course catalog
     *  @param module a single module
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code module} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Module module, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putModule(module);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyModuleLookupSession} using
     *  an array of modules.
     *
     *  @param courseCatalog the course catalog
     *  @param modules an array of modules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code modules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.syllabus.Module[] modules, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putModules(modules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyModuleLookupSession} using a
     *  collection of modules.
     *
     *  @param courseCatalog the course catalog
     *  @param modules a collection of modules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code modules} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyModuleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.syllabus.Module> modules,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putModules(modules);
        return;
    }
}
