//
// AbstractAssessmentRequirement.java
//
//     Defines an AssessmentRequirement.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.assessmentrequirement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssessmentRequirement</code>.
 */

public abstract class AbstractAssessmentRequirement
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.course.requisite.AssessmentRequirement {

    private org.osid.assessment.Assessment assessment;
    private org.osid.calendaring.Duration timeframe;
    private org.osid.grading.Grade minimumGrade;
    private org.osid.grading.GradeSystem minimumScoreSystem;
    private java.math.BigDecimal minimumScore;

    private final java.util.Collection<org.osid.course.requisite.Requisite> altRequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.AssessmentRequirementRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets any <code> Requisites </code> that may be substituted in
     *  place of this <code> AssessmentRequirement. </code> All <code>
     *  Requisites </code> must be satisifed to be a substitute for
     *  this assessment requirement. Inactive <code> Requisites
     *  </code> are not evaluated but if no applicable requisite
     *  exists, then the alternate requisite is not satisifed.
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.altRequisites.toArray(new org.osid.course.requisite.Requisite[this.altRequisites.size()]));
    }


    /**
     *  Adds an alternative requisite.
     *
     *  @param requisite an alternate requisite
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     */

    protected void addAltRequisite(org.osid.course.requisite.Requisite requisite) {
        nullarg(requisite, "alt requisite");
        this.altRequisites.add(requisite);
        return;
    }


    /**
     *  Sets all the alternative requisites.
     *
     *  @param requisites a collection of alternate requisites
     *  @throws org.osid.NullArgumentException
     *          <code>requisites</code> is <code>null</code>
     */

    protected void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> requisites) {
        nullarg(requisites, "alt requisites");

        this.altRequisites.clear();
        this.altRequisites.addAll(requisites);

        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        return (this.assessment.getId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Tests if the assessment must be completed within the required 
     *  duration. 
     *
     *  @return <code> true </code> if the assessment has to be
     *          completed within a required time, <code> false </code>
     *          if it could have been completed at any time in the
     *          past
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.timeframe != null);
    }


    /**
     *  Gets the timeframe in which the assessment has to be
     *  completed. A negative duration indicates the assessment had to
     *  be completed within the specified amount of time in the
     *  past. A posiitive duration indicates the assessment must be
     *  completed within the specified amount of time in the future. A
     *  zero duration indicates the assessment must be completed in
     *  the current term.
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        if (!hasTimeframe()) {
            throw new org.osid.IllegalStateException("hasTimeframe() is false");
        }

        return (this.timeframe);
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException <code>timeframe</code>
     *          is <code>null</code>
     */

    protected void setTimeframe(org.osid.calendaring.Duration timeframe) {
        nullarg(timeframe, "timeframe");
        this.timeframe = timeframe;
        return;
    }


    /**
     *  Tests if a minimum grade above passing is required in the
     *  completion of the assessment.
     *
     *  @return <code> true </code> if a minimum grade is required, <code> 
     *          false </code> if the course just has to be passed 
     */

    @OSID @Override
    public boolean hasMinimumGrade() {
        return (this.minimumGrade != null);
    }


    /**
     *  Gets the minimum grade <code> Id. </code> 
     *
     *  @return the minimum grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGrade() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumGradeId() {
        if (!hasMinimumGrade()) {
            throw new org.osid.IllegalStateException("hasMinimumGrade() is false");
        }

        return (this.minimumGrade.getId());
    }


    /**
     *  Gets the minimum grade. 
     *
     *  @return the minimum grade 
     *  @throws org.osid.IllegalStateException <code>
     *          hasMinimumGrade() </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getMinimumGrade()
        throws org.osid.OperationFailedException {

        if (!hasMinimumGrade()) {
            throw new org.osid.IllegalStateException("hasMinimumGrade() is false");
        }

        return (this.minimumGrade);
    }


    /**
     *  Sets the minimum grade.
     *
     *  @param grade a minimum grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    protected void setMinimumGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "minimum grade");
        this.minimumGrade = grade;
        return;
    }


    /**
     *  Tests if a minimum score above passing is required in the
     *  completion of the assessment.
     *
     *  @return <code> true </code> if a minimum score is required,
     *          <code> false </code> if the course just has to be
     *          passed
     */

    @OSID @Override
    public boolean hasMinimumScore() {
        return ((this.minimumScoreSystem != null) && (this.minimumScore != null));
    }


    /**
     *  Gets the scoring system <code> Id </code> for the minimum score. 
     *
     *  @return the scoring system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumScoreSystemId() {
        if (!hasMinimumScore()) {
            throw new org.osid.IllegalStateException("hasMinimumScore() is false");
        }

        return (this.minimumScoreSystem.getId());
    }


    /**
     *  Gets the scoring system for the minimum score. 
     *
     *  @return the scoring system 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getMinimumScoreSystem()
        throws org.osid.OperationFailedException {

        if (!hasMinimumScore()) {
            throw new org.osid.IllegalStateException("hasMinimumScore() is false");
        }

        return (this.minimumScoreSystem);
    }


    /**
     *  Sets the minimum score system.
     *
     *  @param system a minimum score system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    protected void setMinimumScoreSystem(org.osid.grading.GradeSystem system) {
        nullarg(system, "minimum score system");
        this.minimumScoreSystem = system;
        return;
    }


    /**
     *  Gets the minimum score. 
     *
     *  @return the minimum score 
     *  @throws org.osid.IllegalStateException <code> hasMinimumScore() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumScore() {
        if (!hasMinimumScore()) {
            throw new org.osid.IllegalStateException("hasMinimumScore() is false");
        }

        return (this.minimumScore);
    }


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @throws org.osid.NullArgumentException
     *          <code>score</code> is <code>null</code>
     */

    protected void setMinimumScore(java.math.BigDecimal score) {
        nullarg(score, "minimum score");
        this.minimumScore = score;
        return;
    }


    /**
     *  Tests if this assessmentRequirement supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentRequirementRecordType an assessment requirement record type 
     *  @return <code>true</code> if the assessmentRequirementRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentRequirementRecordType) {
        for (org.osid.course.requisite.records.AssessmentRequirementRecord record : this.records) {
            if (record.implementsRecordType(assessmentRequirementRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssessmentRequirement</code> record <code>Type</code>.
     *
     *  @param  assessmentRequirementRecordType the assessment requirement record type 
     *  @return the assessment requirement record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRequirementRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.AssessmentRequirementRecord getAssessmentRequirementRecord(org.osid.type.Type assessmentRequirementRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.AssessmentRequirementRecord record : this.records) {
            if (record.implementsRecordType(assessmentRequirementRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRequirementRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment requirement. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentRequirementRecord the assessment requirement record
     *  @param assessmentRequirementRecordType assessment requirement record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementRecord</code> or
     *          <code>assessmentRequirementRecordTypeassessmentRequirement</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentRequirementRecord(org.osid.course.requisite.records.AssessmentRequirementRecord assessmentRequirementRecord, 
                                     org.osid.type.Type assessmentRequirementRecordType) {

        nullarg(assessmentRequirementRecord, "assessment requirement record");
        addRecordType(assessmentRequirementRecordType);
        this.records.add(assessmentRequirementRecord);
        
        return;
    }
}
