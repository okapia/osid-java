//
// AbstractImmutableGradebookColumnSummary.java
//
//     Wraps a mutable GradebookColumnSummary to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradebookColumnSummary</code> to hide modifiers. This
 *  wrapper provides an immutized GradebookColumnSummary from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradebookColumnSummary whose state changes are visible.
 */

public abstract class AbstractImmutableGradebookColumnSummary
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.grading.GradebookColumnSummary {

    private final org.osid.grading.GradebookColumnSummary gradebookColumnSummary;


    /**
     *  Constructs a new <code>AbstractImmutableGradebookColumnSummary</code>.
     *
     *  @param gradebookColumnSummary the gradebook column summary to immutablize
     *  @throws org.osid.NullArgumentException <code>gradebookColumnSummary</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradebookColumnSummary(org.osid.grading.GradebookColumnSummary gradebookColumnSummary) {
        super(gradebookColumnSummary);
        this.gradebookColumnSummary = gradebookColumnSummary;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradebookColumn. </code> 
     *
     *  @return the <code> Id </code> of the <code> GradebookColumn </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradebookColumnSummary.getGradebookColumnId());
    }


    /**
     *  Gets the <code> GradebookColumn. </code> 
     *
     *  @return the <code> GradebookColumn </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumnSummary.getGradebookColumn());
    }


    /**
     *  Gets the mean score. If this system is based on grades, the mean 
     *  output score is returned. 
     *
     *  @return the mean score 
     */

    @OSID @Override
    public java.math.BigDecimal getMean() {
        return (this.gradebookColumnSummary.getMean());
    }


    /**
     *  Gets the median score. If this system is based on grades, the mean 
     *  output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getMedian() {
        return (this.gradebookColumnSummary.getMedian());
    }


    /**
     *  Gets the mode of the score. If this system is based on grades, the 
     *  mode of the output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getMode() {
        return (this.gradebookColumnSummary.getMode());
    }


    /**
     *  Gets the root mean square of the score. If this system is based on 
     *  grades, the RMS of the output score is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getRMS() {
        return (this.gradebookColumnSummary.getRMS());
    }


    /**
     *  Gets the standard deviation. If this system is based on grades, the 
     *  spread of the output scores is returned. 
     *
     *  @return the standard deviation 
     */

    @OSID @Override
    public java.math.BigDecimal getStandardDeviation() {
        return (this.gradebookColumnSummary.getStandardDeviation());
    }


    /**
     *  Gets the sum of the scores. If this system is based on grades, the sum 
     *  of the output scores is returned. 
     *
     *  @return the median score 
     */

    @OSID @Override
    public java.math.BigDecimal getSum() {
        return (this.gradebookColumnSummary.getSum());
    }


    /**
     *  Gets the gradebook column summary record corresponding to the given 
     *  <code> GradebookColumnSummary </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> gradebookColumnSummaryRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(gradebookColumnSummaryRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  gradebookColumnSummaryRecordType the type of the record to 
     *          retrieve 
     *  @return the gradebook column summary record 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnSummaryRecordType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradebookColumnSummaryRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSummaryRecord getGradebookColumnSummaryRecord(org.osid.type.Type gradebookColumnSummaryRecordType)
        throws org.osid.OperationFailedException {

        return (this.gradebookColumnSummary.getGradebookColumnSummaryRecord(gradebookColumnSummaryRecordType));
    }
}

