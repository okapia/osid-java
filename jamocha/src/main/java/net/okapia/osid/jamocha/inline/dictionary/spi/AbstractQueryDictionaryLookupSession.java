//
// AbstractQueryDictionaryLookupSession.java
//
//    An inline adapter that maps a DictionaryLookupSession to
//    a DictionaryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a DictionaryLookupSession to
 *  a DictionaryQuerySession.
 */

public abstract class AbstractQueryDictionaryLookupSession
    extends net.okapia.osid.jamocha.dictionary.spi.AbstractDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {

    private final org.osid.dictionary.DictionaryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryDictionaryLookupSession.
     *
     *  @param querySession the underlying dictionary query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryDictionaryLookupSession(org.osid.dictionary.DictionaryQuerySession querySession) {
        nullarg(querySession, "dictionary query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Dictionary</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDictionaries() {
        return (this.session.canSearchDictionaries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Dictionary</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Dictionary</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Dictionary</code> and
     *  retained for compatibility.
     *
     *  @param  dictionaryId <code>Id</code> of the
     *          <code>Dictionary</code>
     *  @return the dictionary
     *  @throws org.osid.NotFoundException <code>dictionaryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>dictionaryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchId(dictionaryId, true);
        org.osid.dictionary.DictionaryList dictionaries = this.session.getDictionariesByQuery(query);
        if (dictionaries.hasNext()) {
            return (dictionaries.getNextDictionary());
        } 
        
        throw new org.osid.NotFoundException(dictionaryId + " not found");
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  dictionaries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Dictionaries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  dictionaryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByIds(org.osid.id.IdList dictionaryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();

        try (org.osid.id.IdList ids = dictionaryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getDictionariesByQuery(query));
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  dictionary genus <code>Type</code> which does not include
     *  dictionaries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchGenusType(dictionaryGenusType, true);
        return (this.session.getDictionariesByQuery(query));
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  dictionary genus <code>Type</code> and include any additional
     *  dictionaries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByParentGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchParentGenusType(dictionaryGenusType, true);
        return (this.session.getDictionariesByQuery(query));
    }


    /**
     *  Gets a <code>DictionaryList</code> containing the given
     *  dictionary record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryRecordType a dictionary record type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByRecordType(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchRecordType(dictionaryRecordType, true);
        return (this.session.getDictionariesByQuery(query));
    }


    /**
     *  Gets a <code>DictionaryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known dictionaries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  dictionaries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Dictionary</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getDictionariesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Dictionaries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Dictionaries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionaries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.dictionary.DictionaryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getDictionariesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.dictionary.DictionaryQuery getQuery() {
        org.osid.dictionary.DictionaryQuery query = this.session.getDictionaryQuery();
        return (query);
    }
}
