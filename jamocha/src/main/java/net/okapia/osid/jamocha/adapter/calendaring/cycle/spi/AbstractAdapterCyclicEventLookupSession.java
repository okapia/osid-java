//
// AbstractAdapterCyclicEventLookupSession.java
//
//    A CyclicEvent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CyclicEvent lookup session adapter.
 */

public abstract class AbstractAdapterCyclicEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private final org.osid.calendaring.cycle.CyclicEventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCyclicEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCyclicEventLookupSession(org.osid.calendaring.cycle.CyclicEventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code CyclicEvent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCyclicEvents() {
        return (this.session.canLookupCyclicEvents());
    }


    /**
     *  A complete view of the {@code CyclicEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicEventView() {
        this.session.useComparativeCyclicEventView();
        return;
    }


    /**
     *  A complete view of the {@code CyclicEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicEventView() {
        this.session.usePlenaryCyclicEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the {@code CyclicEvent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CyclicEvent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CyclicEvent} and
     *  retained for compatibility.
     *
     *  @param cyclicEventId {@code Id} of the {@code CyclicEvent}
     *  @return the cyclic event
     *  @throws org.osid.NotFoundException {@code cyclicEventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code cyclicEventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent(org.osid.id.Id cyclicEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEvent(cyclicEventId));
    }


    /**
     *  Gets a {@code CyclicEventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicEvents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CyclicEvents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  cyclicEventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CyclicEvent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicEventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByIds(org.osid.id.IdList cyclicEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEventsByIds(cyclicEventIds));
    }


    /**
     *  Gets a {@code CyclicEventList} corresponding to the given
     *  cyclic event genus {@code Type} which does not include
     *  cyclic events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned {@code CyclicEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicEventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEventsByGenusType(cyclicEventGenusType));
    }


    /**
     *  Gets a {@code CyclicEventList} corresponding to the given
     *  cyclic event genus {@code Type} and include any additional
     *  cyclic events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned {@code CyclicEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicEventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByParentGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEventsByParentGenusType(cyclicEventGenusType));
    }


    /**
     *  Gets a {@code CyclicEventList} containing the given
     *  cyclic event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventRecordType a cyclicEvent record type 
     *  @return the returned {@code CyclicEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicEventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByRecordType(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEventsByRecordType(cyclicEventRecordType));
    }


    /**
     *  Gets all {@code CyclicEvents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code CyclicEvents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicEvents());
    }
}
