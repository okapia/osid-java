//
// AbstractJobQuery.java
//
//     A template for making a Job Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for jobs.
 */

public abstract class AbstractJobQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.resourcing.JobQuery {

    private final java.util.Collection<org.osid.resourcing.records.JobQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the competency <code> Id </code> for this query. 
     *
     *  @param  competencyId the competency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> competencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCompetencyId(org.osid.id.Id competencyId, boolean match) {
        return;
    }


    /**
     *  Clears the competency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCompetencyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CompetencyQuery </code> is available. 
     *
     *  @return <code> true </code> if a competency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompetencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a competency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the competency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompetencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyQuery getCompetencyQuery() {
        throw new org.osid.UnimplementedException("supportsCompetencyQuery() is false");
    }


    /**
     *  Matches work that have any competency. 
     *
     *  @param  match <code> true </code> to match jobs with any competency, 
     *          <code> false </code> to match work with no competency 
     */

    @OSID @Override
    public void matchAnyCompetency(boolean match) {
        return;
    }


    /**
     *  Clears the competency query terms. 
     */

    @OSID @Override
    public void clearCompetencyTerms() {
        return;
    }


    /**
     *  Sets the work <code> Id </code> for this query. 
     *
     *  @param  workId the work <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> workId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchWorkId(org.osid.id.Id workId, boolean match) {
        return;
    }


    /**
     *  Clears the work <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearWorkIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> WorkQuery </code> is available. 
     *
     *  @return <code> true </code> if a work query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkQuery() {
        return (false);
    }


    /**
     *  Gets the query for a work. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the work query 
     *  @throws org.osid.UnimplementedException <code> supportsWorkQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.WorkQuery getWorkQuery() {
        throw new org.osid.UnimplementedException("supportsWorkQuery() is false");
    }


    /**
     *  Clears the work query terms. 
     */

    @OSID @Override
    public void clearWorkTerms() {
        return;
    }


    /**
     *  Sets the availability <code> Id </code> for this query. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAvailabilityId(org.osid.id.Id availabilityId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAvailabilityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAvailabilityQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsAvailabilityQuery() is false");
    }


    /**
     *  Matches work that have any availability. 
     *
     *  @param  match <code> true </code> to match work with any availability, 
     *          <code> false </code> to match work with no availability 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        return;
    }


    /**
     *  Sets the foundry <code> Id </code> for this query to match jobs 
     *  assigned to foundries. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given job query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job implementing the requested record.
     *
     *  @param jobRecordType a job record type
     *  @return the job query record
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobQueryRecord getJobQueryRecord(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.JobQueryRecord record : this.records) {
            if (record.implementsRecordType(jobRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job query. 
     *
     *  @param jobQueryRecord job query record
     *  @param jobRecordType job record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobQueryRecord(org.osid.resourcing.records.JobQueryRecord jobQueryRecord, 
                                          org.osid.type.Type jobRecordType) {

        addRecordType(jobRecordType);
        nullarg(jobQueryRecord, "job query record");
        this.records.add(jobQueryRecord);        
        return;
    }
}
