//
// WorkElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.work.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class WorkElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the WorkElement Id.
     *
     *  @return the work element Id
     */

    public static org.osid.id.Id getWorkEntityId() {
        return (makeEntityId("osid.resourcing.Work"));
    }


    /**
     *  Gets the JobId element Id.
     *
     *  @return the JobId element Id
     */

    public static org.osid.id.Id getJobId() {
        return (makeElementId("osid.resourcing.work.JobId"));
    }


    /**
     *  Gets the Job element Id.
     *
     *  @return the Job element Id
     */

    public static org.osid.id.Id getJob() {
        return (makeElementId("osid.resourcing.work.Job"));
    }


    /**
     *  Gets the CompetencyIds element Id.
     *
     *  @return the CompetencyIds element Id
     */

    public static org.osid.id.Id getCompetencyIds() {
        return (makeElementId("osid.resourcing.work.CompetencyIds"));
    }


    /**
     *  Gets the Competencies element Id.
     *
     *  @return the Competencies element Id
     */

    public static org.osid.id.Id getCompetencies() {
        return (makeElementId("osid.resourcing.work.Competencies"));
    }


    /**
     *  Gets the CreatedDate element Id.
     *
     *  @return the CreatedDate element Id
     */

    public static org.osid.id.Id getCreatedDate() {
        return (makeElementId("osid.resourcing.work.CreatedDate"));
    }


    /**
     *  Gets the CompletionDate element Id.
     *
     *  @return the CompletionDate element Id
     */

    public static org.osid.id.Id getCompletionDate() {
        return (makeElementId("osid.resourcing.work.CompletionDate"));
    }


    /**
     *  Gets the CommissionId element Id.
     *
     *  @return the CommissionId element Id
     */

    public static org.osid.id.Id getCommissionId() {
        return (makeQueryElementId("osid.resourcing.work.CommissionId"));
    }


    /**
     *  Gets the Commission element Id.
     *
     *  @return the Commission element Id
     */

    public static org.osid.id.Id getCommission() {
        return (makeQueryElementId("osid.resourcing.work.Commission"));
    }


    /**
     *  Gets the FoundryId element Id.
     *
     *  @return the FoundryId element Id
     */

    public static org.osid.id.Id getFoundryId() {
        return (makeQueryElementId("osid.resourcing.work.FoundryId"));
    }


    /**
     *  Gets the Foundry element Id.
     *
     *  @return the Foundry element Id
     */

    public static org.osid.id.Id getFoundry() {
        return (makeQueryElementId("osid.resourcing.work.Foundry"));
    }
}
