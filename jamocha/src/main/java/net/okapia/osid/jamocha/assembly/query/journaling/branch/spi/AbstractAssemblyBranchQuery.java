//
// AbstractAssemblyBranchQuery.java
//
//     A BranchQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.journaling.branch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BranchQuery that stores terms.
 */

public abstract class AbstractAssemblyBranchQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.journaling.BranchQuery,
               org.osid.journaling.BranchQueryInspector,
               org.osid.journaling.BranchSearchOrder {

    private final java.util.Collection<org.osid.journaling.records.BranchQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.BranchQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.BranchSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBranchQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBranchQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the origin journal entry <code> Id </code> for this query. 
     *
     *  @param  journalEntryId a journal entry Id <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOriginJournalEntryId(org.osid.id.Id journalEntryId, 
                                          boolean match) {
        getAssembler().addIdTerm(getOriginJournalEntryIdColumn(), journalEntryId, match);
        return;
    }


    /**
     *  Clears the origin journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOriginJournalEntryIdTerms() {
        getAssembler().clearTerms(getOriginJournalEntryIdColumn());
        return;
    }


    /**
     *  Gets the origin journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOriginJournalEntryIdTerms() {
        return (getAssembler().getIdTerms(getOriginJournalEntryIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the origin journal 
     *  entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOriginJournalEntry(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOriginJournalEntryColumn(), style);
        return;
    }


    /**
     *  Gets the OriginJournalEntryId column name.
     *
     * @return the column name
     */

    protected String getOriginJournalEntryIdColumn() {
        return ("origin_journal_entry_id");
    }


    /**
     *  Tests if a <code> JournalEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an origin journal entry. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the origin journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getOriginJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsOriginJournalEntryQuery() is false");
    }


    /**
     *  Clears the origin journal entry terms. 
     */

    @OSID @Override
    public void clearOriginJournalEntryTerms() {
        getAssembler().clearTerms(getOriginJournalEntryColumn());
        return;
    }


    /**
     *  Gets the origin journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getOriginJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Tests if a <code> JournalEntrySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a journal entry search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginJournalEntrySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the origin journal entry. 
     *
     *  @return the journal entry search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntrySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchOrder getOriginJournalEntrySearchOrder() {
        throw new org.osid.UnimplementedException("supportsOriginJournalEntrySearchOrder() is false");
    }


    /**
     *  Gets the OriginJournalEntry column name.
     *
     * @return the column name
     */

    protected String getOriginJournalEntryColumn() {
        return ("origin_journal_entry");
    }


    /**
     *  Sets the latest journal entry <code> Id </code> for this query. 
     *
     *  @param  journalEntryId a journal entry Id <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchLatestJournalEntryId(org.osid.id.Id journalEntryId, 
                                          boolean match) {
        getAssembler().addIdTerm(getLatestJournalEntryIdColumn(), journalEntryId, match);
        return;
    }


    /**
     *  Clears the latest journal entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLatestJournalEntryIdTerms() {
        getAssembler().clearTerms(getLatestJournalEntryIdColumn());
        return;
    }


    /**
     *  Gets the latest journal entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLatestJournalEntryIdTerms() {
        return (getAssembler().getIdTerms(getLatestJournalEntryIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the latest journal 
     *  entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLatestJournalEntry(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLatestJournalEntryColumn(), style);
        return;
    }


    /**
     *  Gets the LatestJournalEntryId column name.
     *
     * @return the column name
     */

    protected String getLatestJournalEntryIdColumn() {
        return ("latest_journal_entry_id");
    }


    /**
     *  Tests if a <code> JournalEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLatestJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a latest journal entry. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the latest journal entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginJournalEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getLatestJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsLatestJournalEntryQuery() is false");
    }


    /**
     *  Clears the latest journal entry terms. 
     */

    @OSID @Override
    public void clearLatestJournalEntryTerms() {
        getAssembler().clearTerms(getLatestJournalEntryColumn());
        return;
    }


    /**
     *  Gets the latest journal entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getLatestJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Tests if a <code> JournalEntrySearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a journal entry search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLatestJournalEntrySearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the latest journal entry. 
     *
     *  @return the journal entry search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLatestJournalEntrySearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchOrder getLatestJournalEntrySearchOrder() {
        throw new org.osid.UnimplementedException("supportsLatestJournalEntrySearchOrder() is false");
    }


    /**
     *  Gets the LatestJournalEntry column name.
     *
     * @return the column name
     */

    protected String getLatestJournalEntryColumn() {
        return ("latest_journal_entry");
    }


    /**
     *  Tests if this branch supports the given record
     *  <code>Type</code>.
     *
     *  @param  branchRecordType a branch record type 
     *  @return <code>true</code> if the branchRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type branchRecordType) {
        for (org.osid.journaling.records.BranchQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(branchRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  branchRecordType the branch record type 
     *  @return the branch query record 
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchQueryRecord getBranchQueryRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  branchRecordType the branch record type 
     *  @return the branch query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchQueryInspectorRecord getBranchQueryInspectorRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param branchRecordType the branch record type
     *  @return the branch search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>branchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(branchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.BranchSearchOrderRecord getBranchSearchOrderRecord(org.osid.type.Type branchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.BranchSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(branchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(branchRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this branch. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param branchQueryRecord the branch query record
     *  @param branchQueryInspectorRecord the branch query inspector
     *         record
     *  @param branchSearchOrderRecord the branch search order record
     *  @param branchRecordType branch record type
     *  @throws org.osid.NullArgumentException
     *          <code>branchQueryRecord</code>,
     *          <code>branchQueryInspectorRecord</code>,
     *          <code>branchSearchOrderRecord</code> or
     *          <code>branchRecordTypebranch</code> is
     *          <code>null</code>
     */
            
    protected void addBranchRecords(org.osid.journaling.records.BranchQueryRecord branchQueryRecord, 
                                      org.osid.journaling.records.BranchQueryInspectorRecord branchQueryInspectorRecord, 
                                      org.osid.journaling.records.BranchSearchOrderRecord branchSearchOrderRecord, 
                                      org.osid.type.Type branchRecordType) {

        addRecordType(branchRecordType);

        nullarg(branchQueryRecord, "branch query record");
        nullarg(branchQueryInspectorRecord, "branch query inspector record");
        nullarg(branchSearchOrderRecord, "branch search odrer record");

        this.queryRecords.add(branchQueryRecord);
        this.queryInspectorRecords.add(branchQueryInspectorRecord);
        this.searchOrderRecords.add(branchSearchOrderRecord);
        
        return;
    }
}
