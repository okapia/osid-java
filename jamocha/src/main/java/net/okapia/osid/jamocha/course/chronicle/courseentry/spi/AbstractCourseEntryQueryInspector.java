//
// AbstractCourseEntryQueryInspector.java
//
//     A template for making a CourseEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for course entries.
 */

public abstract class AbstractCourseEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.chronicle.CourseEntryQueryInspector {

    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the resource <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the resource query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the complete query terms. 
     *
     *  @return the complete terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditScaleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getCreditScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the earned credits query terms. 
     *
     *  @return the earned credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getCreditsEarnedTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the grade <code> Id </code> query terms. 
     *
     *  @return the grade <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade query terms. 
     *
     *  @return the grade terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getGradeTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScoreScaleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getScoreScaleTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the score query terms. 
     *
     *  @return the score query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the registration <code> Id </code> query terms. 
     *
     *  @return the registration <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRegistrationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the registration query terms. 
     *
     *  @return the registration query terms 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQueryInspector[] getRegistrationTerms() {
        return (new org.osid.course.registration.RegistrationQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given course entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a course entry implementing the requested record.
     *
     *  @param courseEntryRecordType a course entry record type
     *  @return the course entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord getCourseEntryQueryInspectorRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course entry query. 
     *
     *  @param courseEntryQueryInspectorRecord course entry query inspector
     *         record
     *  @param courseEntryRecordType courseEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseEntryQueryInspectorRecord(org.osid.course.chronicle.records.CourseEntryQueryInspectorRecord courseEntryQueryInspectorRecord, 
                                                   org.osid.type.Type courseEntryRecordType) {

        addRecordType(courseEntryRecordType);
        nullarg(courseEntryRecordType, "course entry record type");
        this.records.add(courseEntryQueryInspectorRecord);        
        return;
    }
}
