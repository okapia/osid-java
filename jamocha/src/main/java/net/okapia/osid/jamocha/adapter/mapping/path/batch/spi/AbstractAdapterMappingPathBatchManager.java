//
// AbstractMappingPathBatchManager.java
//
//     An adapter for a MappingPathBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MappingPathBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMappingPathBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.mapping.path.batch.MappingPathBatchManager>
    implements org.osid.mapping.path.batch.MappingPathBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMappingPathBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMappingPathBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMappingPathBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of paths is available. 
     *
     *  @return <code> true </code> if a path bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathBatchAdmin() {
        return (getAdapteeManager().supportsPathBatchAdmin());
    }


    /**
     *  Tests if bulk administration of intersections is available. 
     *
     *  @return <code> true </code> if an intersection bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionBatchAdmin() {
        return (getAdapteeManager().supportsIntersectionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of speed zones is available. 
     *
     *  @return <code> true </code> if a speed zone bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneBatchAdmin() {
        return (getAdapteeManager().supportsSpeedZoneBatchAdmin());
    }


    /**
     *  Tests if bulk administration of signals is available. 
     *
     *  @return <code> true </code> if a signal bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalBatchAdmin() {
        return (getAdapteeManager().supportsSignalBatchAdmin());
    }


    /**
     *  Tests if bulk administration of obstacles is available. 
     *
     *  @return <code> true </code> if an obstacle bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleBatchAdmin() {
        return (getAdapteeManager().supportsObstacleBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk path 
     *  administration service. 
     *
     *  @return a <code> PathBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.PathBatchAdminSession getPathBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPathBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk path 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.PathBatchAdminSession getPathBatchAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPathBatchAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  intersection administration service. 
     *
     *  @return an <code> IntersectionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.IntersectionBatchAdminSession getIntersectionBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  intersection administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.IntersectionBatchAdminSession getIntersectionBatchAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIntersectionBatchAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk speed 
     *  zone administration service. 
     *
     *  @return a <code> SpeedZoneBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.SpeedZoneBatchAdminSession getSpeedZoneBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk speed 
     *  zone administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.SpeedZoneBatchAdminSession getSpeedZoneBatchAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSpeedZoneBatchAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk signal 
     *  administration service. 
     *
     *  @return a <code> SignalBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.SignalBatchAdminSession getSignalBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk signal 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.SignalBatchAdminSession getSignalBatchAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSignalBatchAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk obstacle 
     *  administration service. 
     *
     *  @return an <code> ObstacleBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.ObstacleBatchAdminSession getObstacleBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk obstacle 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.ObstacleBatchAdminSession getObstacleBatchAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getObstacleBatchAdminSessionForMap(mapId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();
	
        return;
    }
}
