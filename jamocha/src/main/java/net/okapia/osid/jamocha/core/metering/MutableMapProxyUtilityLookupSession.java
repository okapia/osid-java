//
// MutableMapProxyUtilityLookupSession
//
//    Implements an Utility lookup service backed by a collection of
//    utilities that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering;


/**
 *  Implements an Utility lookup service backed by a collection of
 *  utilities. The utilities are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of utilities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyUtilityLookupSession
    extends net.okapia.osid.jamocha.core.metering.spi.AbstractMapUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyUtilityLookupSession} with no
     *  utilities.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyUtilityLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyUtilityLookupSession} with a
     *  single utility.
     *
     *  @param utility an utility
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utility} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyUtilityLookupSession(org.osid.metering.Utility utility, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putUtility(utility);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyUtilityLookupSession} using an
     *  array of utilities.
     *
     *  @param utilities an array of utilities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utilities} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyUtilityLookupSession(org.osid.metering.Utility[] utilities, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putUtilities(utilities);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyUtilityLookupSession} using
     *  a collection of utilities.
     *
     *  @param utilities a collection of utilities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code utilities} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyUtilityLookupSession(java.util.Collection<? extends org.osid.metering.Utility> utilities,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putUtilities(utilities);
        return;
    }

    
    /**
     *  Makes a {@code Utility} available in this session.
     *
     *  @param utility an utility
     *  @throws org.osid.NullArgumentException {@code utility{@code 
     *          is {@code null}
     */

    @Override
    public void putUtility(org.osid.metering.Utility utility) {
        super.putUtility(utility);
        return;
    }


    /**
     *  Makes an array of utilities available in this session.
     *
     *  @param utilities an array of utilities
     *  @throws org.osid.NullArgumentException {@code utilities{@code 
     *          is {@code null}
     */

    @Override
    public void putUtilities(org.osid.metering.Utility[] utilities) {
        super.putUtilities(utilities);
        return;
    }


    /**
     *  Makes collection of utilities available in this session.
     *
     *  @param utilities
     *  @throws org.osid.NullArgumentException {@code utility{@code 
     *          is {@code null}
     */

    @Override
    public void putUtilities(java.util.Collection<? extends org.osid.metering.Utility> utilities) {
        super.putUtilities(utilities);
        return;
    }


    /**
     *  Removes a Utility from this session.
     *
     *  @param utilityId the {@code Id} of the utility
     *  @throws org.osid.NullArgumentException {@code utilityId{@code  is
     *          {@code null}
     */

    @Override
    public void removeUtility(org.osid.id.Id utilityId) {
        super.removeUtility(utilityId);
        return;
    }    
}
