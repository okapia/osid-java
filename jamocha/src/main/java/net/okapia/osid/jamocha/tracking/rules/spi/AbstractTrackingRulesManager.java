//
// AbstractTrackingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTrackingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.tracking.rules.TrackingRulesManager,
               org.osid.tracking.rules.TrackingRulesProxyManager {

    private final Types queueConstrainerRecordTypes        = new TypeRefSet();
    private final Types queueConstrainerSearchRecordTypes  = new TypeRefSet();

    private final Types queueConstrainerEnablerRecordTypes = new TypeRefSet();
    private final Types queueConstrainerEnablerSearchRecordTypes= new TypeRefSet();

    private final Types queueProcessorRecordTypes          = new TypeRefSet();
    private final Types queueProcessorSearchRecordTypes    = new TypeRefSet();

    private final Types queueProcessorEnablerRecordTypes   = new TypeRefSet();
    private final Types queueProcessorEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTrackingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTrackingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if queue constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer notification service is supported. 
     *
     *  @return <code> true </code> if queue constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer front office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer front office service is supported. 
     *
     *  @return <code> true </code> if queue constrainer front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer front office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler front 
     *          office lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler front office service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler front 
     *          office service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a queue constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a queue processor administrative service is supported. 
     *
     *  @return <code> true </code> if queue processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue processor notification service is supported. 
     *
     *  @return <code> true </code> if queue processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a queue processor front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor front office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue processor front office service is supported. 
     *
     *  @return <code> true </code> if queue processor front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue processor front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor front office service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a queue processor rule application service is supported. 
     *
     *  @return <code> true </code> if queue processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler front office 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler front office service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler front office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler front office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler front office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a queue processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  queueConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> QueueConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRecordType(org.osid.type.Type queueConstrainerRecordType) {
        return (this.queueConstrainerRecordTypes.contains(queueConstrainerRecordType));
    }


    /**
     *  Adds support for a queue constrainer record type.
     *
     *  @param queueConstrainerRecordType a queue constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerRecordType</code> is <code>null</code>
     */

    protected void addQueueConstrainerRecordType(org.osid.type.Type queueConstrainerRecordType) {
        this.queueConstrainerRecordTypes.add(queueConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for a queue constrainer record type.
     *
     *  @param queueConstrainerRecordType a queue constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeQueueConstrainerRecordType(org.osid.type.Type queueConstrainerRecordType) {
        this.queueConstrainerRecordTypes.remove(queueConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  queueConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearchRecordType(org.osid.type.Type queueConstrainerSearchRecordType) {
        return (this.queueConstrainerSearchRecordTypes.contains(queueConstrainerSearchRecordType));
    }


    /**
     *  Adds support for a queue constrainer search record type.
     *
     *  @param queueConstrainerSearchRecordType a queue constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueConstrainerSearchRecordType(org.osid.type.Type queueConstrainerSearchRecordType) {
        this.queueConstrainerSearchRecordTypes.add(queueConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue constrainer search record type.
     *
     *  @param queueConstrainerSearchRecordType a queue constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueConstrainerSearchRecordType(org.osid.type.Type queueConstrainerSearchRecordType) {
        this.queueConstrainerSearchRecordTypes.remove(queueConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  queueConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        return (this.queueConstrainerEnablerRecordTypes.contains(queueConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for a queue constrainer enabler record type.
     *
     *  @param queueConstrainerEnablerRecordType a queue constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addQueueConstrainerEnablerRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        this.queueConstrainerEnablerRecordTypes.add(queueConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a queue constrainer enabler record type.
     *
     *  @param queueConstrainerEnablerRecordType a queue constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeQueueConstrainerEnablerRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        this.queueConstrainerEnablerRecordTypes.remove(queueConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  queueConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearchRecordType(org.osid.type.Type queueConstrainerEnablerSearchRecordType) {
        return (this.queueConstrainerEnablerSearchRecordTypes.contains(queueConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a queue constrainer enabler search record type.
     *
     *  @param queueConstrainerEnablerSearchRecordType a queue constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueConstrainerEnablerSearchRecordType(org.osid.type.Type queueConstrainerEnablerSearchRecordType) {
        this.queueConstrainerEnablerSearchRecordTypes.add(queueConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue constrainer enabler search record type.
     *
     *  @param queueConstrainerEnablerSearchRecordType a queue constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueConstrainerEnablerSearchRecordType(org.osid.type.Type queueConstrainerEnablerSearchRecordType) {
        this.queueConstrainerEnablerSearchRecordTypes.remove(queueConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> record type is 
     *  supported. 
     *
     *  @param  queueProcessorRecordType a <code> Type </code> indicating a 
     *          <code> QueueProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRecordType(org.osid.type.Type queueProcessorRecordType) {
        return (this.queueProcessorRecordTypes.contains(queueProcessorRecordType));
    }


    /**
     *  Adds support for a queue processor record type.
     *
     *  @param queueProcessorRecordType a queue processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorRecordType</code> is <code>null</code>
     */

    protected void addQueueProcessorRecordType(org.osid.type.Type queueProcessorRecordType) {
        this.queueProcessorRecordTypes.add(queueProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a queue processor record type.
     *
     *  @param queueProcessorRecordType a queue processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorRecordType</code> is <code>null</code>
     */

    protected void removeQueueProcessorRecordType(org.osid.type.Type queueProcessorRecordType) {
        this.queueProcessorRecordTypes.remove(queueProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  queueProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearchRecordType(org.osid.type.Type queueProcessorSearchRecordType) {
        return (this.queueProcessorSearchRecordTypes.contains(queueProcessorSearchRecordType));
    }


    /**
     *  Adds support for a queue processor search record type.
     *
     *  @param queueProcessorSearchRecordType a queue processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueProcessorSearchRecordType(org.osid.type.Type queueProcessorSearchRecordType) {
        this.queueProcessorSearchRecordTypes.add(queueProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue processor search record type.
     *
     *  @param queueProcessorSearchRecordType a queue processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueProcessorSearchRecordType(org.osid.type.Type queueProcessorSearchRecordType) {
        this.queueProcessorSearchRecordTypes.remove(queueProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  queueProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        return (this.queueProcessorEnablerRecordTypes.contains(queueProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a queue processor enabler record type.
     *
     *  @param queueProcessorEnablerRecordType a queue processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addQueueProcessorEnablerRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        this.queueProcessorEnablerRecordTypes.add(queueProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a queue processor enabler record type.
     *
     *  @param queueProcessorEnablerRecordType a queue processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeQueueProcessorEnablerRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        this.queueProcessorEnablerRecordTypes.remove(queueProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  queueProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearchRecordType(org.osid.type.Type queueProcessorEnablerSearchRecordType) {
        return (this.queueProcessorEnablerSearchRecordTypes.contains(queueProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a queue processor enabler search record type.
     *
     *  @param queueProcessorEnablerSearchRecordType a queue processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueProcessorEnablerSearchRecordType(org.osid.type.Type queueProcessorEnablerSearchRecordType) {
        this.queueProcessorEnablerSearchRecordTypes.add(queueProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue processor enabler search record type.
     *
     *  @param queueProcessorEnablerSearchRecordType a queue processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueProcessorEnablerSearchRecordType(org.osid.type.Type queueProcessorEnablerSearchRecordType) {
        this.queueProcessorEnablerSearchRecordTypes.remove(queueProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service. 
     *
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service. 
     *
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer search service. 
     *
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service. 
     *
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSession(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSession(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service for the given front office. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> or <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                                            org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service for the given front office. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerReceiver, frontOfficeId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                                            org.osid.id.Id frontOfficeId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer/office 
     *  mappings for queue constrainers. 
     *
     *  @return a <code> QueueConstrainerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeSession getQueueConstrainerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer/office 
     *  mappings for queue constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeSession getQueueConstrainerFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer to front office. 
     *
     *  @return a <code> QueueConstrainerFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeAssignmentSession getQueueConstrainerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer to front office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerFrontOfficeAssignmentSession getQueueConstrainerFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSmartFrontOfficeSession getQueueConstrainerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerSmartFrontOfficeSession getQueueConstrainerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a queue. 
     *
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  ta queue. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for the given front office for 
     *  looking up rules applied to a queue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for the given front office for 
     *  looking up rules applied to a qeue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service to apply to queues. 
     *
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service to apply to queues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service for the given front office to apply to 
     *  queues. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service for the given front office to apply to 
     *  queues. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service. 
     *
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler search service. 
     *
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSession(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSession(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service for the given front office. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> or <code> 
     *          frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                                          org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service for the given front office. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver, frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                                          org.osid.id.Id frontOfficeId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer 
     *  enabler/office mappings for queue constrainer enablers. 
     *
     *  @return a <code> QueueConstrainerEnablerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeSession getQueueConstrainerEnablerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer 
     *  enabler/office mappings for queue constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeSession getQueueConstrainerEnablerFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer enablers to front office. 
     *
     *  @return a <code> QueueConstrainerEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeAssignmentSession getQueueConstrainerEnablerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer enablers to front office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerFrontOfficeAssignmentSession getQueueConstrainerEnablerFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer 
     *  enabler smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerSmartFrontOfficeSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSmartFrontOfficeSession getQueueConstrainerEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer 
     *  enabler smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSmartFrontOfficeSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerSmartFrontOfficeSession getQueueConstrainerEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueConstrainerEnablerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service. 
     *
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorLookupSession getQueueProcessorLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service. 
     *
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorQuerySession getQueueProcessorQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor search service. 
     *
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSearchSession getQueueProcessorSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service. 
     *
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorAdminSession getQueueProcessorAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSession(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSession(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service for the given front office. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> or <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                                        org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service for the given front office. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                                        org.osid.id.Id frontOfficeId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor/office 
     *  mappings for queue processors. 
     *
     *  @return a <code> QueueProcessorFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeSession getQueueProcessorFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor/office 
     *  mappings for queue processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeSession getQueueProcessorFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor to front office. 
     *
     *  @return a <code> QueueProcessorFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeAssignmentSession getQueueProcessorFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor to front office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorFrontOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorFrontOfficeAssignmentSession getQueueProcessorFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSmartFrontOfficeSession getQueueProcessorSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor smart 
     *  front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSmartFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorSmartFrontOfficeSession getQueueProcessorSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for looking up the rules applied to a 
     *  queue. 
     *
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for looking up the rules applied to a 
     *  queue processor. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for the given front office for 
     *  looking up rules applied to a queue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for the given front office for 
     *  looking up rules applied to a queue. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service. 
     *
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service to apply to queues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service for the given front office to apply to 
     *  queues. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service. 
     *
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service. 
     *
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler search service. 
     *
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enablers earch service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service. 
     *
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSession(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSession(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service for the given front office. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> or <code> frontOfficeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                                      org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service for the given front office. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver, frontOfficeId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSessionForFrontOffice(org.osid.tracking.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                                      org.osid.id.Id frontOfficeId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor 
     *  enabler/office mappings for queue processor enablers. 
     *
     *  @return a <code> QueueProcessorEnablerFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeSession getQueueProcessorEnablerFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor 
     *  enabler/office mappings for queue processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeSession getQueueProcessorEnablerFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor enablers to front office. 
     *
     *  @return a <code> QueueProcessorEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeAssignmentSession getQueueProcessorEnablerFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor enablers to queue processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerFrontOfficeAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerFrontOfficeAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerFrontOfficeAssignmentSession getQueueProcessorEnablerFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor enabler 
     *  smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSmartFrontOfficeSession getQueueProcessorEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor enabler 
     *  smart front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSmartFrontOffice() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerSmartFrontOfficeSession getQueueProcessorEnablerSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerRuleLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service. 
     *
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesManager.getQueueProcessorEnablerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.rules.TrackingRulesProxyManager.getQueueProcessorEnablerRuleApplicationSessionForFrontOffice not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.queueConstrainerRecordTypes.clear();
        this.queueConstrainerRecordTypes.clear();

        this.queueConstrainerSearchRecordTypes.clear();
        this.queueConstrainerSearchRecordTypes.clear();

        this.queueConstrainerEnablerRecordTypes.clear();
        this.queueConstrainerEnablerRecordTypes.clear();

        this.queueConstrainerEnablerSearchRecordTypes.clear();
        this.queueConstrainerEnablerSearchRecordTypes.clear();

        this.queueProcessorRecordTypes.clear();
        this.queueProcessorRecordTypes.clear();

        this.queueProcessorSearchRecordTypes.clear();
        this.queueProcessorSearchRecordTypes.clear();

        this.queueProcessorEnablerRecordTypes.clear();
        this.queueProcessorEnablerRecordTypes.clear();

        this.queueProcessorEnablerSearchRecordTypes.clear();
        this.queueProcessorEnablerSearchRecordTypes.clear();

        return;
    }
}
