//
// AbstractPriceScheduleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.priceschedule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPriceScheduleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ordering.PriceScheduleSearchResults {

    private org.osid.ordering.PriceScheduleList priceSchedules;
    private final org.osid.ordering.PriceScheduleQueryInspector inspector;
    private final java.util.Collection<org.osid.ordering.records.PriceScheduleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPriceScheduleSearchResults.
     *
     *  @param priceSchedules the result set
     *  @param priceScheduleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>priceSchedules</code>
     *          or <code>priceScheduleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPriceScheduleSearchResults(org.osid.ordering.PriceScheduleList priceSchedules,
                                            org.osid.ordering.PriceScheduleQueryInspector priceScheduleQueryInspector) {
        nullarg(priceSchedules, "price schedules");
        nullarg(priceScheduleQueryInspector, "price schedule query inspectpr");

        this.priceSchedules = priceSchedules;
        this.inspector = priceScheduleQueryInspector;

        return;
    }


    /**
     *  Gets the price schedule list resulting from a search.
     *
     *  @return a price schedule list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules() {
        if (this.priceSchedules == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ordering.PriceScheduleList priceSchedules = this.priceSchedules;
        this.priceSchedules = null;
	return (priceSchedules);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ordering.PriceScheduleQueryInspector getPriceScheduleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  price schedule search record <code> Type. </code> This method must
     *  be used to retrieve a priceSchedule implementing the requested
     *  record.
     *
     *  @param priceScheduleSearchRecordType a priceSchedule search 
     *         record type 
     *  @return the price schedule search
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(priceScheduleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceScheduleSearchResultsRecord getPriceScheduleSearchResultsRecord(org.osid.type.Type priceScheduleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ordering.records.PriceScheduleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(priceScheduleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(priceScheduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record price schedule search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPriceScheduleRecord(org.osid.ordering.records.PriceScheduleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "price schedule record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
