//
// AbstractGradeSystemSearch.java
//
//     A template for making a GradeSystem Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing grade system searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractGradeSystemSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.grading.GradeSystemSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeSystemSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.grading.GradeSystemSearchOrder gradeSystemSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of grade systems. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  gradeSystemIds list of grade systems
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongGradeSystems(org.osid.id.IdList gradeSystemIds) {
        while (gradeSystemIds.hasNext()) {
            try {
                this.ids.add(gradeSystemIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongGradeSystems</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of grade system Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getGradeSystemIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  gradeSystemSearchOrder grade system search order 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>gradeSystemSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderGradeSystemResults(org.osid.grading.GradeSystemSearchOrder gradeSystemSearchOrder) {
	this.gradeSystemSearchOrder = gradeSystemSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.grading.GradeSystemSearchOrder getGradeSystemSearchOrder() {
	return (this.gradeSystemSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given grade system search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a grade system implementing the requested record.
     *
     *  @param gradeSystemSearchRecordType a grade system search record
     *         type
     *  @return the grade system search record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemSearchRecord getGradeSystemSearchRecord(org.osid.type.Type gradeSystemSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.grading.records.GradeSystemSearchRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade system search. 
     *
     *  @param gradeSystemSearchRecord grade system search record
     *  @param gradeSystemSearchRecordType gradeSystem search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeSystemSearchRecord(org.osid.grading.records.GradeSystemSearchRecord gradeSystemSearchRecord, 
                                           org.osid.type.Type gradeSystemSearchRecordType) {

        addRecordType(gradeSystemSearchRecordType);
        this.records.add(gradeSystemSearchRecord);        
        return;
    }
}
