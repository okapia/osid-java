//
// AbstractAdapterSignalEnablerLookupSession.java
//
//    A SignalEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SignalEnabler lookup session adapter.
 */

public abstract class AbstractAdapterSignalEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.rules.SignalEnablerLookupSession {

    private final org.osid.mapping.path.rules.SignalEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSignalEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSignalEnablerLookupSession(org.osid.mapping.path.rules.SignalEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code SignalEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSignalEnablers() {
        return (this.session.canLookupSignalEnablers());
    }


    /**
     *  A complete view of the {@code SignalEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSignalEnablerView() {
        this.session.useComparativeSignalEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code SignalEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySignalEnablerView() {
        this.session.usePlenarySignalEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include signal enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active signal enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSignalEnablerView() {
        this.session.useActiveSignalEnablerView();
        return;
    }


    /**
     *  Active and inactive signal enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSignalEnablerView() {
        this.session.useAnyStatusSignalEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code SignalEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SignalEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SignalEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param signalEnablerId {@code Id} of the {@code SignalEnabler}
     *  @return the signal enabler
     *  @throws org.osid.NotFoundException {@code signalEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code signalEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnabler getSignalEnabler(org.osid.id.Id signalEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnabler(signalEnablerId));
    }


    /**
     *  Gets a {@code SignalEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  signalEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SignalEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  signalEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SignalEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code signalEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByIds(org.osid.id.IdList signalEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablersByIds(signalEnablerIds));
    }


    /**
     *  Gets a {@code SignalEnablerList} corresponding to the given
     *  signal enabler genus {@code Type} which does not include
     *  signal enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  signalEnablerGenusType a signalEnabler genus type 
     *  @return the returned {@code SignalEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablersByGenusType(signalEnablerGenusType));
    }


    /**
     *  Gets a {@code SignalEnablerList} corresponding to the given
     *  signal enabler genus {@code Type} and include any additional
     *  signal enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  signalEnablerGenusType a signalEnabler genus type 
     *  @return the returned {@code SignalEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByParentGenusType(org.osid.type.Type signalEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablersByParentGenusType(signalEnablerGenusType));
    }


    /**
     *  Gets a {@code SignalEnablerList} containing the given
     *  signal enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  signalEnablerRecordType a signalEnabler record type 
     *  @return the returned {@code SignalEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code signalEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersByRecordType(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablersByRecordType(signalEnablerRecordType));
    }


    /**
     *  Gets a {@code SignalEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *  
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code SignalEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SignalEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible
     *  through this session.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code SignalEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getSignalEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code SignalEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  signal enablers or an error results. Otherwise, the returned list
     *  may contain only those signal enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, signal enablers are returned that are currently
     *  active. In any status mode, active and inactive signal enablers
     *  are returned.
     *
     *  @return a list of {@code SignalEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SignalEnablerList getSignalEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSignalEnablers());
    }
}
