//
// AbstractProcedureSearch.java
//
//     A template for making a Procedure Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing procedure searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractProcedureSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.recipe.ProcedureSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.recipe.records.ProcedureSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.recipe.ProcedureSearchOrder procedureSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of procedures. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  procedureIds list of procedures
     *  @throws org.osid.NullArgumentException
     *          <code>procedureIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongProcedures(org.osid.id.IdList procedureIds) {
        while (procedureIds.hasNext()) {
            try {
                this.ids.add(procedureIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongProcedures</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of procedure Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getProcedureIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  procedureSearchOrder procedure search order 
     *  @throws org.osid.NullArgumentException
     *          <code>procedureSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>procedureSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderProcedureResults(org.osid.recipe.ProcedureSearchOrder procedureSearchOrder) {
	this.procedureSearchOrder = procedureSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.recipe.ProcedureSearchOrder getProcedureSearchOrder() {
	return (this.procedureSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given procedure search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a procedure implementing the requested record.
     *
     *  @param procedureSearchRecordType a procedure search record
     *         type
     *  @return the procedure search record
     *  @throws org.osid.NullArgumentException
     *          <code>procedureSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureSearchRecord getProcedureSearchRecord(org.osid.type.Type procedureSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.recipe.records.ProcedureSearchRecord record : this.records) {
            if (record.implementsRecordType(procedureSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this procedure search. 
     *
     *  @param procedureSearchRecord procedure search record
     *  @param procedureSearchRecordType procedure search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProcedureSearchRecord(org.osid.recipe.records.ProcedureSearchRecord procedureSearchRecord, 
                                           org.osid.type.Type procedureSearchRecordType) {

        addRecordType(procedureSearchRecordType);
        this.records.add(procedureSearchRecord);        
        return;
    }
}
