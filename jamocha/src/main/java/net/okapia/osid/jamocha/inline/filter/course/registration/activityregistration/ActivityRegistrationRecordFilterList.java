//
// ActivityRegistrationRecordFilterList.java
//
//     Implements a filter for record types.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for record types.
 */

public final class ActivityRegistrationRecordFilterList
    extends net.okapia.osid.jamocha.inline.filter.course.registration.activityregistration.spi.AbstractActivityRegistrationFilterList
    implements org.osid.course.registration.ActivityRegistrationList,
               ActivityRegistrationFilter {

    private final java.util.Collection<org.osid.type.Type> recordTypes = new java.util.HashSet<>();


    /**
     *  Creates a new <code>ActivityRegistrationRecordFilterList</code> passing the
     *  given record type.
     *
     *  @param list an <code>ActivityRegistrationList</code>
     *  @param type a record type
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>type</code> is <code>null</code>
     */

    public ActivityRegistrationRecordFilterList(org.osid.course.registration.ActivityRegistrationList list, 
                                   org.osid.type.Type type) {
        super(list);

        nullarg(type, "record type");
        this.recordTypes.add(type);

        return;
    }    


    /**
     *  Creates a new <code>ActivityRegistrationFilterList</code> passing the
     *  given collection of record types.
     *
     *  @param list an <code>ActivityRegistrationList</code>
     *  @param types a collection of record types
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>types</code> is <code>null</code>
     */
    
    public ActivityRegistrationRecordFilterList(org.osid.course.registration.ActivityRegistrationList list, 
                                   java.util.Collection<org.osid.type.Type> types) {
        super(list);

        nullarg(types, "record types");
        this.recordTypes.addAll(types);

        return;
    }    

    
    /**
     *  Filters ActivityRegistrations.
     *
     *  @param activityRegistration the activity registration to filter
     *  @return <code>true</code> if the activity registration passes the filter,
     *          <code>false</code> if the activity registration should be filtered
     */

    @Override
    public boolean pass(org.osid.course.registration.ActivityRegistration activityRegistration) {
        for (org.osid.type.Type type : this.recordTypes) {
            if (activityRegistration.hasRecordType(type)) {
                return (true);
            }
        }

        return (false);
    }
}
