//
// AbstractFederatingDictionaryLookupSession.java
//
//     An abstract federating adapter for a DictionaryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DictionaryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDictionaryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.dictionary.DictionaryLookupSession>
    implements org.osid.dictionary.DictionaryLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingDictionaryLookupSession</code>.
     */

    protected AbstractFederatingDictionaryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.dictionary.DictionaryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Dictionary</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDictionaries() {
        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            if (session.canLookupDictionaries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Dictionary</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDictionaryView() {
        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            session.useComparativeDictionaryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Dictionary</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDictionaryView() {
        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            session.usePlenaryDictionaryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Dictionary</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Dictionary</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Dictionary</code> and
     *  retained for compatibility.
     *
     *  @param  dictionaryId <code>Id</code> of the
     *          <code>Dictionary</code>
     *  @return the dictionary
     *  @throws org.osid.NotFoundException <code>dictionaryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>dictionaryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary(org.osid.id.Id dictionaryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            try {
                return (session.getDictionary(dictionaryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(dictionaryId + " not found");
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  dictionaries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Dictionaries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  dictionaryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByIds(org.osid.id.IdList dictionaryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.dictionary.dictionary.MutableDictionaryList ret = new net.okapia.osid.jamocha.dictionary.dictionary.MutableDictionaryList();

        try (org.osid.id.IdList ids = dictionaryIds) {
            while (ids.hasNext()) {
                ret.addDictionary(getDictionary(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  dictionary genus <code>Type</code> which does not include
     *  dictionaries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList ret = getDictionaryList();

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            ret.addDictionaryList(session.getDictionariesByGenusType(dictionaryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  dictionary genus <code>Type</code> and include any additional
     *  dictionaries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByParentGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList ret = getDictionaryList();

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            ret.addDictionaryList(session.getDictionariesByParentGenusType(dictionaryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DictionaryList</code> containing the given
     *  dictionary record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  dictionaryRecordType a dictionary record type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByRecordType(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList ret = getDictionaryList();

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            ret.addDictionaryList(session.getDictionariesByRecordType(dictionaryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DictionaryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known dictionaries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  dictionaries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Dictionary</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList ret = getDictionaryList();

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            ret.addDictionaryList(session.getDictionariesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Dictionaries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  dictionaries or an error results. Otherwise, the returned list
     *  may contain only those dictionaries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Dictionaries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionaries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList ret = getDictionaryList();

        for (org.osid.dictionary.DictionaryLookupSession session : getSessions()) {
            ret.addDictionaryList(session.getDictionaries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.FederatingDictionaryList getDictionaryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.ParallelDictionaryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.dictionary.dictionary.CompositeDictionaryList());
        }
    }
}
