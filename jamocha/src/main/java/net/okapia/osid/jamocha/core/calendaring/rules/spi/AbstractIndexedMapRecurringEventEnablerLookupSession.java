//
// AbstractIndexedMapRecurringEventEnablerLookupSession.java
//
//    A simple framework for providing a RecurringEventEnabler lookup service
//    backed by a fixed collection of recurring event enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RecurringEventEnabler lookup service backed by a
 *  fixed collection of recurring event enablers. The recurring event enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some recurring event enablers may be compatible
 *  with more types than are indicated through these recurring event enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RecurringEventEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRecurringEventEnablerLookupSession
    extends AbstractMapRecurringEventEnablerLookupSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.RecurringEventEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.RecurringEventEnabler> recurringEventEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.RecurringEventEnabler>());


    /**
     *  Makes a <code>RecurringEventEnabler</code> available in this session.
     *
     *  @param  recurringEventEnabler a recurring event enabler
     *  @throws org.osid.NullArgumentException <code>recurringEventEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRecurringEventEnabler(org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler) {
        super.putRecurringEventEnabler(recurringEventEnabler);

        this.recurringEventEnablersByGenus.put(recurringEventEnabler.getGenusType(), recurringEventEnabler);
        
        try (org.osid.type.TypeList types = recurringEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recurringEventEnablersByRecord.put(types.getNextType(), recurringEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a recurring event enabler from this session.
     *
     *  @param recurringEventEnablerId the <code>Id</code> of the recurring event enabler
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId) {
        org.osid.calendaring.rules.RecurringEventEnabler recurringEventEnabler;
        try {
            recurringEventEnabler = getRecurringEventEnabler(recurringEventEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.recurringEventEnablersByGenus.remove(recurringEventEnabler.getGenusType());

        try (org.osid.type.TypeList types = recurringEventEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.recurringEventEnablersByRecord.remove(types.getNextType(), recurringEventEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRecurringEventEnabler(recurringEventEnablerId);
        return;
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> corresponding to the given
     *  recurring event enabler genus <code>Type</code> which does not include
     *  recurring event enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known recurring event enablers or an error results. Otherwise,
     *  the returned list may contain only those recurring event enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  recurringEventEnablerGenusType a recurring event enabler genus type 
     *  @return the returned <code>RecurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.ArrayRecurringEventEnablerList(this.recurringEventEnablersByGenus.get(recurringEventEnablerGenusType)));
    }


    /**
     *  Gets a <code>RecurringEventEnablerList</code> containing the given
     *  recurring event enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known recurring event enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  recurring event enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  recurringEventEnablerRecordType a recurring event enabler record type 
     *  @return the returned <code>recurringEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByRecordType(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.ArrayRecurringEventEnablerList(this.recurringEventEnablersByRecord.get(recurringEventEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.recurringEventEnablersByGenus.clear();
        this.recurringEventEnablersByRecord.clear();

        super.close();

        return;
    }
}
