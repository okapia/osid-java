//
// MutableMapProxyAvailabilityEnablerLookupSession
//
//    Implements an AvailabilityEnabler lookup service backed by a collection of
//    availabilityEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules;


/**
 *  Implements an AvailabilityEnabler lookup service backed by a collection of
 *  availabilityEnablers. The availabilityEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of availability enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAvailabilityEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.rules.spi.AbstractMapAvailabilityEnablerLookupSession
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAvailabilityEnablerLookupSession}
     *  with no availability enablers.
     *
     *  @param foundry the foundry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                  org.osid.proxy.Proxy proxy) {
        setFoundry(foundry);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAvailabilityEnablerLookupSession} with a
     *  single availability enabler.
     *
     *  @param foundry the foundry
     *  @param availabilityEnabler an availability enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availabilityEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putAvailabilityEnabler(availabilityEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAvailabilityEnablerLookupSession} using an
     *  array of availability enablers.
     *
     *  @param foundry the foundry
     *  @param availabilityEnablers an array of availability enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availabilityEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                org.osid.resourcing.rules.AvailabilityEnabler[] availabilityEnablers, org.osid.proxy.Proxy proxy) {
        this(foundry, proxy);
        putAvailabilityEnablers(availabilityEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAvailabilityEnablerLookupSession} using a
     *  collection of availability enablers.
     *
     *  @param foundry the foundry
     *  @param availabilityEnablers a collection of availability enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code foundry},
     *          {@code availabilityEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAvailabilityEnablerLookupSession(org.osid.resourcing.Foundry foundry,
                                                java.util.Collection<? extends org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(foundry, proxy);
        setSessionProxy(proxy);
        putAvailabilityEnablers(availabilityEnablers);
        return;
    }

    
    /**
     *  Makes a {@code AvailabilityEnabler} available in this session.
     *
     *  @param availabilityEnabler an availability enabler
     *  @throws org.osid.NullArgumentException {@code availabilityEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putAvailabilityEnabler(org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler) {
        super.putAvailabilityEnabler(availabilityEnabler);
        return;
    }


    /**
     *  Makes an array of availabilityEnablers available in this session.
     *
     *  @param availabilityEnablers an array of availability enablers
     *  @throws org.osid.NullArgumentException {@code availabilityEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putAvailabilityEnablers(org.osid.resourcing.rules.AvailabilityEnabler[] availabilityEnablers) {
        super.putAvailabilityEnablers(availabilityEnablers);
        return;
    }


    /**
     *  Makes collection of availability enablers available in this session.
     *
     *  @param availabilityEnablers
     *  @throws org.osid.NullArgumentException {@code availabilityEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putAvailabilityEnablers(java.util.Collection<? extends org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablers) {
        super.putAvailabilityEnablers(availabilityEnablers);
        return;
    }


    /**
     *  Removes a AvailabilityEnabler from this session.
     *
     *  @param availabilityEnablerId the {@code Id} of the availability enabler
     *  @throws org.osid.NullArgumentException {@code availabilityEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAvailabilityEnabler(org.osid.id.Id availabilityEnablerId) {
        super.removeAvailabilityEnabler(availabilityEnablerId);
        return;
    }    
}
