//
// AbstractIssue.java
//
//     Defines an Issue builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.hold.issue.spi;


/**
 *  Defines an <code>Issue</code> builder.
 */

public abstract class AbstractIssueBuilder<T extends AbstractIssueBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.hold.issue.IssueMiter issue;


    /**
     *  Constructs a new <code>AbstractIssueBuilder</code>.
     *
     *  @param issue the issue to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractIssueBuilder(net.okapia.osid.jamocha.builder.hold.issue.IssueMiter issue) {
        super(issue);
        this.issue = issue;
        return;
    }


    /**
     *  Builds the issue.
     *
     *  @return the new issue
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.hold.Issue build() {
        (new net.okapia.osid.jamocha.builder.validator.hold.issue.IssueValidator(getValidations())).validate(this.issue);
        return (new net.okapia.osid.jamocha.builder.hold.issue.ImmutableIssue(this.issue));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the issue miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.hold.issue.IssueMiter getMiter() {
        return (this.issue);
    }


    /**
     *  Sets the bureau.
     *
     *  @param bureau a bureau
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>bureau</code> is
     *          <code>null</code>
     */

    public T bureau(org.osid.resource.Resource bureau) {
        getMiter().setBureau(bureau);
        return (self());
    }


    /**
     *  Adds an Issue record.
     *
     *  @param record an issue record
     *  @param recordType the type of issue record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.hold.records.IssueRecord record, org.osid.type.Type recordType) {
        getMiter().addIssueRecord(record, recordType);
        return (self());
    }
}       


