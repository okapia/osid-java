//
// AbstractOrganization.java
//
//     Defines an Organization builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.organization.spi;


/**
 *  Defines an <code>Organization</code> builder.
 */

public abstract class AbstractOrganizationBuilder<T extends AbstractOrganizationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.personnel.organization.OrganizationMiter organization;


    /**
     *  Constructs a new <code>AbstractOrganizationBuilder</code>.
     *
     *  @param organization the organization to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractOrganizationBuilder(net.okapia.osid.jamocha.builder.personnel.organization.OrganizationMiter organization) {
        super(organization);
        this.organization = organization;
        return;
    }


    /**
     *  Builds the organization.
     *
     *  @return the new organization
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.personnel.Organization build() {
        (new net.okapia.osid.jamocha.builder.validator.personnel.organization.OrganizationValidator(getValidations())).validate(this.organization);
        return (new net.okapia.osid.jamocha.builder.personnel.organization.ImmutableOrganization(this.organization));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the organization miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.personnel.organization.OrganizationMiter getMiter() {
        return (this.organization);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public T displayLabel(org.osid.locale.DisplayText label) {
        getMiter().setDisplayLabel(label);
        return (self());
    }


    /**
     *  Adds an Organization record.
     *
     *  @param record an organization record
     *  @param recordType the type of organization record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.personnel.records.OrganizationRecord record, org.osid.type.Type recordType) {
        getMiter().addOrganizationRecord(record, recordType);
        return (self());
    }
}       


