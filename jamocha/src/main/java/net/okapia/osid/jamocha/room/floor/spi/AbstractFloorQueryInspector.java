//
// AbstractFloorQueryInspector.java
//
//     A template for making a FloorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for floors.
 */

public abstract class AbstractFloorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.room.FloorQueryInspector {

    private final java.util.Collection<org.osid.room.records.FloorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the terms terms. 
     *
     *  @return the terms terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the gross area terms. 
     *
     *  @return the gross area terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getGrossAreaTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the room <code> Id </code> terms. 
     *
     *  @return the room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRoomIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the room terms. 
     *
     *  @return the room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given floor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a floor implementing the requested record.
     *
     *  @param floorRecordType a floor record type
     *  @return the floor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>floorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(floorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorQueryInspectorRecord getFloorQueryInspectorRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.FloorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(floorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(floorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this floor query. 
     *
     *  @param floorQueryInspectorRecord floor query inspector
     *         record
     *  @param floorRecordType floor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFloorQueryInspectorRecord(org.osid.room.records.FloorQueryInspectorRecord floorQueryInspectorRecord, 
                                                   org.osid.type.Type floorRecordType) {

        addRecordType(floorRecordType);
        nullarg(floorRecordType, "floor record type");
        this.records.add(floorQueryInspectorRecord);        
        return;
    }
}
