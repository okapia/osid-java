//
// ConferralElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.conferral.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ConferralElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ConferralElement Id.
     *
     *  @return the conferral element Id
     */

    public static org.osid.id.Id getConferralEntityId() {
        return (makeEntityId("osid.recognition.Conferral"));
    }


    /**
     *  Gets the AwardId element Id.
     *
     *  @return the AwardId element Id
     */

    public static org.osid.id.Id getAwardId() {
        return (makeElementId("osid.recognition.conferral.AwardId"));
    }


    /**
     *  Gets the Award element Id.
     *
     *  @return the Award element Id
     */

    public static org.osid.id.Id getAward() {
        return (makeElementId("osid.recognition.conferral.Award"));
    }


    /**
     *  Gets the RecipientId element Id.
     *
     *  @return the RecipientId element Id
     */

    public static org.osid.id.Id getRecipientId() {
        return (makeElementId("osid.recognition.conferral.RecipientId"));
    }


    /**
     *  Gets the Recipient element Id.
     *
     *  @return the Recipient element Id
     */

    public static org.osid.id.Id getRecipient() {
        return (makeElementId("osid.recognition.conferral.Recipient"));
    }


    /**
     *  Gets the ReferenceId element Id.
     *
     *  @return the ReferenceId element Id
     */

    public static org.osid.id.Id getReferenceId() {
        return (makeElementId("osid.recognition.conferral.ReferenceId"));
    }


    /**
     *  Gets the ConvocationId element Id.
     *
     *  @return the ConvocationId element Id
     */

    public static org.osid.id.Id getConvocationId() {
        return (makeElementId("osid.recognition.conferral.ConvocationId"));
    }


    /**
     *  Gets the Convocation element Id.
     *
     *  @return the Convocation element Id
     */

    public static org.osid.id.Id getConvocation() {
        return (makeElementId("osid.recognition.conferral.Convocation"));
    }


    /**
     *  Gets the AcademyId element Id.
     *
     *  @return the AcademyId element Id
     */

    public static org.osid.id.Id getAcademyId() {
        return (makeQueryElementId("osid.recognition.conferral.AcademyId"));
    }


    /**
     *  Gets the Academy element Id.
     *
     *  @return the Academy element Id
     */

    public static org.osid.id.Id getAcademy() {
        return (makeQueryElementId("osid.recognition.conferral.Academy"));
    }


    /**
     *  Gets the Reference element Id.
     *
     *  @return the Reference element Id
     */

    public static org.osid.id.Id getReference() {
        return (makeElementId("osid.recognition.conferral.Reference"));
    }
}
