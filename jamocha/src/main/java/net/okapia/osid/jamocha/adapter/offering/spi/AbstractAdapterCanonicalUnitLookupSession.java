//
// AbstractAdapterCanonicalUnitLookupSession.java
//
//    A CanonicalUnit lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CanonicalUnit lookup session adapter.
 */

public abstract class AbstractAdapterCanonicalUnitLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.CanonicalUnitLookupSession {

    private final org.osid.offering.CanonicalUnitLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCanonicalUnitLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCanonicalUnitLookupSession(org.osid.offering.CanonicalUnitLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code CanonicalUnit} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCanonicalUnits() {
        return (this.session.canLookupCanonicalUnits());
    }


    /**
     *  A complete view of the {@code CanonicalUnit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCanonicalUnitView() {
        this.session.useComparativeCanonicalUnitView();
        return;
    }


    /**
     *  A complete view of the {@code CanonicalUnit} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCanonicalUnitView() {
        this.session.usePlenaryCanonicalUnitView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical units in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical units are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitView() {
        this.session.useActiveCanonicalUnitView();
        return;
    }


    /**
     *  Active and inactive canonical units are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitView() {
        this.session.useAnyStatusCanonicalUnitView();
        return;
    }
    
     
    /**
     *  Gets the {@code CanonicalUnit} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CanonicalUnit} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CanonicalUnit} and
     *  retained for compatibility.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param canonicalUnitId {@code Id} of the {@code CanonicalUnit}
     *  @return the canonical unit
     *  @throws org.osid.NotFoundException {@code canonicalUnitId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code canonicalUnitId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit(org.osid.id.Id canonicalUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnit(canonicalUnitId));
    }


    /**
     *  Gets a {@code CanonicalUnitList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnits specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CanonicalUnits} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CanonicalUnit} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByIds(org.osid.id.IdList canonicalUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitsByIds(canonicalUnitIds));
    }


    /**
     *  Gets a {@code CanonicalUnitList} corresponding to the given
     *  canonical unit genus {@code Type} which does not include
     *  canonical units of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned {@code CanonicalUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitsByGenusType(canonicalUnitGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitList} corresponding to the given
     *  canonical unit genus {@code Type} and include any additional
     *  canonical units with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitGenusType a canonicalUnit genus type 
     *  @return the returned {@code CanonicalUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByParentGenusType(org.osid.type.Type canonicalUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitsByParentGenusType(canonicalUnitGenusType));
    }


    /**
     *  Gets a {@code CanonicalUnitList} containing the given
     *  canonical unit record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @param  canonicalUnitRecordType a canonicalUnit record type 
     *  @return the returned {@code CanonicalUnit} list
     *  @throws org.osid.NullArgumentException
     *          {@code canonicalUnitRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByRecordType(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitsByRecordType(canonicalUnitRecordType));
    }


    /**
     *  Gets canonical units by code. 
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned
     *  list may contain only those canonical units that are
     *  accessible through this session.
     *  
     *  In active mode, canonicals are returned that are currently
     *  active. In any status mode, active and inactive canonicals are
     *  returned.
     *
     *  @param  code a code 
     *  @return a list of canonical units 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnitsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnitsByCode(code));
    }


    /**
     *  Gets all {@code CanonicalUnits}. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical units or an error results. Otherwise, the returned list
     *  may contain only those canonical units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical units are returned that are currently
     *  active. In any status mode, active and inactive canonical units
     *  are returned.
     *
     *  @return a list of {@code CanonicalUnits} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitList getCanonicalUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCanonicalUnits());
    }
}
