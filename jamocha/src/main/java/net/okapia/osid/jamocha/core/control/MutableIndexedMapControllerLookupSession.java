//
// MutableIndexedMapControllerLookupSession
//
//    Implements a Controller lookup service backed by a collection of
//    controllers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Controller lookup service backed by a collection of
 *  controllers. The controllers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some controllers may be compatible
 *  with more types than are indicated through these controller
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of controllers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapControllerLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractIndexedMapControllerLookupSession
    implements org.osid.control.ControllerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapControllerLookupSession} with no controllers.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

      public MutableIndexedMapControllerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapControllerLookupSession} with a
     *  single controller.
     *  
     *  @param system the system
     *  @param  controller a single controller
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code controller} is {@code null}
     */

    public MutableIndexedMapControllerLookupSession(org.osid.control.System system,
                                                  org.osid.control.Controller controller) {
        this(system);
        putController(controller);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapControllerLookupSession} using an
     *  array of controllers.
     *
     *  @param system the system
     *  @param  controllers an array of controllers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code controllers} is {@code null}
     */

    public MutableIndexedMapControllerLookupSession(org.osid.control.System system,
                                                  org.osid.control.Controller[] controllers) {
        this(system);
        putControllers(controllers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapControllerLookupSession} using a
     *  collection of controllers.
     *
     *  @param system the system
     *  @param  controllers a collection of controllers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code controllers} is {@code null}
     */

    public MutableIndexedMapControllerLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.Controller> controllers) {

        this(system);
        putControllers(controllers);
        return;
    }
    

    /**
     *  Makes a {@code Controller} available in this session.
     *
     *  @param  controller a controller
     *  @throws org.osid.NullArgumentException {@code controller{@code  is
     *          {@code null}
     */

    @Override
    public void putController(org.osid.control.Controller controller) {
        super.putController(controller);
        return;
    }


    /**
     *  Makes an array of controllers available in this session.
     *
     *  @param  controllers an array of controllers
     *  @throws org.osid.NullArgumentException {@code controllers{@code 
     *          is {@code null}
     */

    @Override
    public void putControllers(org.osid.control.Controller[] controllers) {
        super.putControllers(controllers);
        return;
    }


    /**
     *  Makes collection of controllers available in this session.
     *
     *  @param  controllers a collection of controllers
     *  @throws org.osid.NullArgumentException {@code controller{@code  is
     *          {@code null}
     */

    @Override
    public void putControllers(java.util.Collection<? extends org.osid.control.Controller> controllers) {
        super.putControllers(controllers);
        return;
    }


    /**
     *  Removes a Controller from this session.
     *
     *  @param controllerId the {@code Id} of the controller
     *  @throws org.osid.NullArgumentException {@code controllerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeController(org.osid.id.Id controllerId) {
        super.removeController(controllerId);
        return;
    }    
}
