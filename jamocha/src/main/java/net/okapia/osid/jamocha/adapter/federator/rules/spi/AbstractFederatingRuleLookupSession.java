//
// AbstractFederatingRuleLookupSession.java
//
//     An abstract federating adapter for a RuleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RuleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRuleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.rules.RuleLookupSession>
    implements org.osid.rules.RuleLookupSession {

    private boolean parallel = false;
    private org.osid.rules.Engine engine = new net.okapia.osid.jamocha.nil.rules.engine.UnknownEngine();


    /**
     *  Constructs a new <code>AbstractFederatingRuleLookupSession</code>.
     */

    protected AbstractFederatingRuleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.rules.RuleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Engine/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Engine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.engine.getId());
    }


    /**
     *  Gets the <code>Engine</code> associated with this 
     *  session.
     *
     *  @return the <code>Engine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.engine);
    }


    /**
     *  Sets the <code>Engine</code>.
     *
     *  @param  engine the engine for this session
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected void setEngine(org.osid.rules.Engine engine) {
        nullarg(engine, "engine");
        this.engine = engine;
        return;
    }


    /**
     *  Tests if this user can perform <code>Rule</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRules() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            if (session.canLookupRules()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Rule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRuleView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.useComparativeRuleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Rule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRuleView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.usePlenaryRuleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rules in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.useFederatedEngineView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.useIsolatedEngineView();
        }

        return;
    }


    /**
     *  Only active rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRuleView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.useActiveRuleView();
        }

        return;
    }


    /**
     *  Active and inactive rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRuleView() {
        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            session.useAnyStatusRuleView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Rule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Rule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Rule</code> and
     *  retained for compatibility.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleId <code>Id</code> of the
     *          <code>Rule</code>
     *  @return the rule
     *  @throws org.osid.NotFoundException <code>ruleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ruleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            try {
                return (session.getRule(ruleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(ruleId + " not found");
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the rules
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Rules</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules are
     *  returned.
     *
     *  @param  ruleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ruleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByIds(org.osid.id.IdList ruleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.rules.rule.MutableRuleList ret = new net.okapia.osid.jamocha.rules.rule.MutableRuleList();

        try (org.osid.id.IdList ids = ruleIds) {
            while (ids.hasNext()) {
                ret.addRule(getRule(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  rule genus <code>Type</code> which does not include
     *  rules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known rules or
     *  an error results. Otherwise, the returned list may contain
     *  only those rules that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules are
     *  returned.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.rule.FederatingRuleList ret = getRuleList();

        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            ret.addRuleList(session.getRulesByGenusType(ruleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RuleList</code> corresponding to the given
     *  rule genus <code>Type</code> and include any additional
     *  rules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known rules or
     *  an error results. Otherwise, the returned list may contain
     *  only those rules that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules are
     *  returned.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByParentGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.rule.FederatingRuleList ret = getRuleList();

        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            ret.addRuleList(session.getRulesByParentGenusType(ruleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RuleList</code> containing the given
     *  rule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known rules or
     *  an error results. Otherwise, the returned list may contain
     *  only those rules that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules are
     *  returned.
     *
     *  @param  ruleRecordType a rule record type 
     *  @return the returned <code>Rule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByRecordType(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.rule.FederatingRuleList ret = getRuleList();

        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            ret.addRuleList(session.getRulesByRecordType(ruleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Rules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @return a list of <code>Rules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.rules.rule.FederatingRuleList ret = getRuleList();

        for (org.osid.rules.RuleLookupSession session : getSessions()) {
            ret.addRuleList(session.getRules());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.rules.rule.FederatingRuleList getRuleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.rule.ParallelRuleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.rules.rule.CompositeRuleList());
        }
    }
}
