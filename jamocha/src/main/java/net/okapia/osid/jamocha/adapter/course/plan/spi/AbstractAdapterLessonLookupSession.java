//
// AbstractAdapterLessonLookupSession.java
//
//    A Lesson lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.plan.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Lesson lookup session adapter.
 */

public abstract class AbstractAdapterLessonLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.plan.LessonLookupSession {

    private final org.osid.course.plan.LessonLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterLessonLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterLessonLookupSession(org.osid.course.plan.LessonLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Lesson} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupLessons() {
        return (this.session.canLookupLessons());
    }


    /**
     *  A complete view of the {@code Lesson} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLessonView() {
        this.session.useComparativeLessonView();
        return;
    }


    /**
     *  A complete view of the {@code Lesson} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLessonView() {
        this.session.usePlenaryLessonView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include lessons in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only lessons whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveLessonView() {
        this.session.useEffectiveLessonView();
        return;
    }
    

    /**
     *  All lessons of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveLessonView() {
        this.session.useAnyEffectiveLessonView();
        return;
    }

     
    /**
     *  Gets the {@code Lesson} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Lesson} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Lesson} and
     *  retained for compatibility.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param lessonId {@code Id} of the {@code Lesson}
     *  @return the lesson
     *  @throws org.osid.NotFoundException {@code lessonId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code lessonId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Lesson getLesson(org.osid.id.Id lessonId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLesson(lessonId));
    }


    /**
     *  Gets a {@code LessonList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  lessons specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Lessons} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Lesson} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code lessonIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByIds(org.osid.id.IdList lessonIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsByIds(lessonIds));
    }


    /**
     *  Gets a {@code LessonList} corresponding to the given
     *  lesson genus {@code Type} which does not include
     *  lessons of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonGenusType a lesson genus type 
     *  @return the returned {@code Lesson} list
     *  @throws org.osid.NullArgumentException
     *          {@code lessonGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByGenusType(org.osid.type.Type lessonGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsByGenusType(lessonGenusType));
    }


    /**
     *  Gets a {@code LessonList} corresponding to the given
     *  lesson genus {@code Type} and include any additional
     *  lessons with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonGenusType a lesson genus type 
     *  @return the returned {@code Lesson} list
     *  @throws org.osid.NullArgumentException
     *          {@code lessonGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByParentGenusType(org.osid.type.Type lessonGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsByParentGenusType(lessonGenusType));
    }


    /**
     *  Gets a {@code LessonList} containing the given
     *  lesson record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  lessonRecordType a lesson record type 
     *  @return the returned {@code Lesson} list
     *  @throws org.osid.NullArgumentException
     *          {@code lessonRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsByRecordType(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsByRecordType(lessonRecordType));
    }


    /**
     *  Gets a {@code LessonList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *  
     *  In active mode, lessons are returned that are currently
     *  active. In any status mode, active and inactive lessons
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Lesson} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsOnDate(from, to));
    }
        

    /**
     *  Gets a list of lessons corresponding to a plan
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the {@code Id} of the plan
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code planId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlan(org.osid.id.Id planId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForPlan(planId));
    }


    /**
     *  Gets a list of lessons corresponding to a plan
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the {@code Id} of the plan
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code planId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanOnDate(org.osid.id.Id planId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForPlanOnDate(planId, from, to));
    }


    /**
     *  Gets a list of lessons corresponding to a docet
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  docetId the {@code Id} of the docet
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code docetId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForDocet(org.osid.id.Id docetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForDocet(docetId));
    }


    /**
     *  Gets a list of lessons corresponding to a docet
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  docetId the {@code Id} of the docet
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code docetId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForDocetOnDate(org.osid.id.Id docetId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForDocetOnDate(docetId, from, to));
    }


    /**
     *  Gets a list of lessons corresponding to plan and docet
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are
     *  currently effective.  In any effective mode, effective
     *  lessons and those currently expired are returned.
     *
     *  @param  planId the {@code Id} of the plan
     *  @param  docetId the {@code Id} of the docet
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code planId},
     *          {@code docetId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanAndDocet(org.osid.id.Id planId,
                                                                     org.osid.id.Id docetId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForPlanAndDocet(planId, docetId));
    }


    /**
     *  Gets a list of lessons corresponding to plan and docet
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible
     *  through this session.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective. In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @param  docetId the {@code Id} of the docet
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code LessonList}
     *  @throws org.osid.NullArgumentException {@code planId},
     *          {@code docetId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessonsForPlanAndDocetOnDate(org.osid.id.Id planId,
                                                                           org.osid.id.Id docetId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessonsForPlanAndDocetOnDate(planId, docetId, from, to));
    }


    /**
     *  Gets all {@code Lessons}. 
     *
     *  In plenary mode, the returned list contains all known
     *  lessons or an error results. Otherwise, the returned list
     *  may contain only those lessons that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, lessons are returned that are currently
     *  effective.  In any effective mode, effective lessons and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Lessons} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessons()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getLessons());
    }
}
