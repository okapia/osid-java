//
// AbstractAdapterActionEnablerLookupSession.java
//
//    An ActionEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ActionEnabler lookup session adapter.
 */

public abstract class AbstractAdapterActionEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.control.rules.ActionEnablerLookupSession {

    private final org.osid.control.rules.ActionEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterActionEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterActionEnablerLookupSession(org.osid.control.rules.ActionEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code System/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code System Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.session.getSystemId());
    }


    /**
     *  Gets the {@code System} associated with this session.
     *
     *  @return the {@code System} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getSystem());
    }


    /**
     *  Tests if this user can perform {@code ActionEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupActionEnablers() {
        return (this.session.canLookupActionEnablers());
    }


    /**
     *  A complete view of the {@code ActionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActionEnablerView() {
        this.session.useComparativeActionEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code ActionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActionEnablerView() {
        this.session.usePlenaryActionEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include action enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.session.useFederatedSystemView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.session.useIsolatedSystemView();
        return;
    }
    

    /**
     *  Only active action enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveActionEnablerView() {
        this.session.useActiveActionEnablerView();
        return;
    }


    /**
     *  Active and inactive action enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusActionEnablerView() {
        this.session.useAnyStatusActionEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code ActionEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ActionEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ActionEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param actionEnablerId {@code Id} of the {@code ActionEnabler}
     *  @return the action enabler
     *  @throws org.osid.NotFoundException {@code actionEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code actionEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnabler getActionEnabler(org.osid.id.Id actionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnabler(actionEnablerId));
    }


    /**
     *  Gets an {@code ActionEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  actionEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ActionEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  actionEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ActionEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code actionEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByIds(org.osid.id.IdList actionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablersByIds(actionEnablerIds));
    }


    /**
     *  Gets an {@code ActionEnablerList} corresponding to the given
     *  action enabler genus {@code Type} which does not include
     *  action enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  actionEnablerGenusType an actionEnabler genus type 
     *  @return the returned {@code ActionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByGenusType(org.osid.type.Type actionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablersByGenusType(actionEnablerGenusType));
    }


    /**
     *  Gets an {@code ActionEnablerList} corresponding to the given
     *  action enabler genus {@code Type} and include any additional
     *  action enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  actionEnablerGenusType an actionEnabler genus type 
     *  @return the returned {@code ActionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByParentGenusType(org.osid.type.Type actionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablersByParentGenusType(actionEnablerGenusType));
    }


    /**
     *  Gets an {@code ActionEnablerList} containing the given
     *  action enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  actionEnablerRecordType an actionEnabler record type 
     *  @return the returned {@code ActionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code actionEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByRecordType(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablersByRecordType(actionEnablerRecordType));
    }


    /**
     *  Gets an {@code ActionEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible
     *  through this session.
     *  
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ActionEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code ActionEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible
     *  through this session.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code ActionEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getActionEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code ActionEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @return a list of {@code ActionEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getActionEnablers());
    }
}
