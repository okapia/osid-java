//
// MutableMapSystemLookupSession
//
//    Implements a System lookup service backed by a collection of
//    systems that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a System lookup service backed by a collection of
 *  systems. The systems are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of systems can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSystemLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapSystemLookupSession
    implements org.osid.control.SystemLookupSession {


    /**
     *  Constructs a new {@code MutableMapSystemLookupSession}
     *  with no systems.
     */

    public MutableMapSystemLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSystemLookupSession} with a
     *  single system.
     *  
     *  @param system a system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

    public MutableMapSystemLookupSession(org.osid.control.System system) {
        putSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSystemLookupSession}
     *  using an array of systems.
     *
     *  @param systems an array of systems
     *  @throws org.osid.NullArgumentException {@code systems}
     *          is {@code null}
     */

    public MutableMapSystemLookupSession(org.osid.control.System[] systems) {
        putSystems(systems);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSystemLookupSession}
     *  using a collection of systems.
     *
     *  @param systems a collection of systems
     *  @throws org.osid.NullArgumentException {@code systems}
     *          is {@code null}
     */

    public MutableMapSystemLookupSession(java.util.Collection<? extends org.osid.control.System> systems) {
        putSystems(systems);
        return;
    }

    
    /**
     *  Makes a {@code System} available in this session.
     *
     *  @param system a system
     *  @throws org.osid.NullArgumentException {@code system{@code  is
     *          {@code null}
     */

    @Override
    public void putSystem(org.osid.control.System system) {
        super.putSystem(system);
        return;
    }


    /**
     *  Makes an array of systems available in this session.
     *
     *  @param systems an array of systems
     *  @throws org.osid.NullArgumentException {@code systems{@code 
     *          is {@code null}
     */

    @Override
    public void putSystems(org.osid.control.System[] systems) {
        super.putSystems(systems);
        return;
    }


    /**
     *  Makes collection of systems available in this session.
     *
     *  @param systems a collection of systems
     *  @throws org.osid.NullArgumentException {@code systems{@code  is
     *          {@code null}
     */

    @Override
    public void putSystems(java.util.Collection<? extends org.osid.control.System> systems) {
        super.putSystems(systems);
        return;
    }


    /**
     *  Removes a System from this session.
     *
     *  @param systemId the {@code Id} of the system
     *  @throws org.osid.NullArgumentException {@code systemId{@code 
     *          is {@code null}
     */

    @Override
    public void removeSystem(org.osid.id.Id systemId) {
        super.removeSystem(systemId);
        return;
    }    
}
