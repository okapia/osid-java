//
// DistanceMetadata.java
//
//     Defines a distance Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a distance Metadata.
 */

public final class DistanceMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractDistanceMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code DistanceMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DistanceMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code DistanceMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DistanceMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code DistanceMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public DistanceMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
	return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *  	is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }


    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }
   
    
    /**
     *  Sets the smallest resolution accepted.
     *
     *  @param resolution the smallest distance resolution
     *  @throws org.osid.NullArgumentException {@code resolution} is
     *          {@code null}
     */

    public void setDistanceResolution(org.osid.mapping.DistanceResolution resolution) {
        super.setDistanceResolution(resolution);
        return;
    }


    /**
     *  Sets the distance range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    public void setDistanceRange(org.osid.mapping.Distance min, org.osid.mapping.Distance max) {
        super.setDistanceRange(min, max);
        return;
    }


    /**
     *  Sets the distance set.
     *
     *  @param values a collection of accepted distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDistanceSet(java.util.Collection<org.osid.mapping.Distance> values) {
        super.setDistanceSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the distance set.
     *
     *  @param values a collection of accepted distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToDistanceSet(java.util.Collection<org.osid.mapping.Distance> values) {
        super.addToDistanceSet(values);
        return;
    }


    /**
     *  Adds a value to the distance set.
     *
     *  @param value a distance value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToDistanceSet(org.osid.mapping.Distance value) {
        super.addToDistanceSet(value);
        return;
    }


    /**
     *  Removes a value from the distance set.
     *
     *  @param value a distance value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromDistanceSet(org.osid.mapping.Distance value) {
        super.removeFromDistanceSet(value);
        return;
    }


    /**
     *  Clears the distance set.
     */

    public void clearDistanceSet() {
        super.clearDistanceSet();
        return;
    }


    /**
     *  Sets the default distance set.
     *
     *  @param values a collection of default distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        super.setDefaultDistanceValues(values);
        return;
    }


    /**
     *  Adds a collection of default distance values.
     *
     *  @param values a collection of default distance values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        super.addDefaultDistanceValues(values);
        return;
    }


    /**
     *  Adds a default distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultDistanceValue(org.osid.mapping.Distance value) {
        super.addDefaultDistanceValue(value);
        return;
    }


    /**
     *  Removes a default distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultDistanceValue(org.osid.mapping.Distance value) {
        super.removeDefaultDistanceValue(value);
        return;
    }


    /**
     *  Clears the default distance values.
     */

    public void clearDefaultDistanceValues() {
        super.clearDefaultDistanceValues();
        return;
    }


    /**
     *  Sets the existing distance set.
     *
     *  @param values a collection of existing distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        super.setExistingDistanceValues(values);
        return;
    }


    /**
     *  Adds a collection of existing distance values.
     *
     *  @param values a collection of existing distance values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        super.addExistingDistanceValues(values);
        return;
    }


    /**
     *  Adds a existing distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingDistanceValue(org.osid.mapping.Distance value) {
        super.addExistingDistanceValue(value);
        return;
    }


    /**
     *  Removes a existing distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingDistanceValue(org.osid.mapping.Distance value) {
        super.removeExistingDistanceValue(value);
        return;
    }


    /**
     *  Clears the existing distance values.
     */

    public void clearExistingDistanceValues() {
        super.clearExistingDistanceValues();
        return;
    }    
}
