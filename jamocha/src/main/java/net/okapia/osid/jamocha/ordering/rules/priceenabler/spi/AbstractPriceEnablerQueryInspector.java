//
// AbstractPriceEnablerQueryInspector.java
//
//     A template for making a PriceEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for price enablers.
 */

public abstract class AbstractPriceEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.ordering.rules.PriceEnablerQueryInspector {

    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the price <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPriceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the price query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getRuledPriceTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the store <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given price enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a price enabler implementing the requested record.
     *
     *  @param priceEnablerRecordType a price enabler record type
     *  @return the price enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord getPriceEnablerQueryInspectorRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price enabler query. 
     *
     *  @param priceEnablerQueryInspectorRecord price enabler query inspector
     *         record
     *  @param priceEnablerRecordType priceEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceEnablerQueryInspectorRecord(org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord priceEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type priceEnablerRecordType) {

        addRecordType(priceEnablerRecordType);
        nullarg(priceEnablerRecordType, "price enabler record type");
        this.records.add(priceEnablerQueryInspectorRecord);        
        return;
    }
}
