//
// AbstractStepProcessorSearch.java
//
//     A template for making a StepProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing step processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractStepProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.workflow.rules.StepProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.workflow.rules.StepProcessorSearchOrder stepProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of step processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  stepProcessorIds list of step processors
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongStepProcessors(org.osid.id.IdList stepProcessorIds) {
        while (stepProcessorIds.hasNext()) {
            try {
                this.ids.add(stepProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongStepProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of step processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getStepProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  stepProcessorSearchOrder step processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>stepProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderStepProcessorResults(org.osid.workflow.rules.StepProcessorSearchOrder stepProcessorSearchOrder) {
	this.stepProcessorSearchOrder = stepProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.workflow.rules.StepProcessorSearchOrder getStepProcessorSearchOrder() {
	return (this.stepProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given step processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step processor implementing the requested record.
     *
     *  @param stepProcessorSearchRecordType a step processor search record
     *         type
     *  @return the step processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorSearchRecord getStepProcessorSearchRecord(org.osid.type.Type stepProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.workflow.rules.records.StepProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(stepProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step processor search. 
     *
     *  @param stepProcessorSearchRecord step processor search record
     *  @param stepProcessorSearchRecordType stepProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepProcessorSearchRecord(org.osid.workflow.rules.records.StepProcessorSearchRecord stepProcessorSearchRecord, 
                                           org.osid.type.Type stepProcessorSearchRecordType) {

        addRecordType(stepProcessorSearchRecordType);
        this.records.add(stepProcessorSearchRecord);        
        return;
    }
}
