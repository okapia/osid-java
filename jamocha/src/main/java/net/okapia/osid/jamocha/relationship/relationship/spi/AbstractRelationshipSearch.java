//
// AbstractRelationshipSearch.java
//
//     A template for making a Relationship Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing relationship searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRelationshipSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.relationship.RelationshipSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.relationship.records.RelationshipSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.relationship.RelationshipSearchOrder relationshipSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of relationships. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  relationshipIds list of relationships
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRelationships(org.osid.id.IdList relationshipIds) {
        while (relationshipIds.hasNext()) {
            try {
                this.ids.add(relationshipIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRelationships</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of relationship Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRelationshipIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  relationshipSearchOrder relationship search order 
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>relationshipSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRelationshipResults(org.osid.relationship.RelationshipSearchOrder relationshipSearchOrder) {
	this.relationshipSearchOrder = relationshipSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.relationship.RelationshipSearchOrder getRelationshipSearchOrder() {
	return (this.relationshipSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given relationship search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relationship implementing the requested record.
     *
     *  @param relationshipSearchRecordType a relationship search record
     *         type
     *  @return the relationship search record
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relationshipSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.RelationshipSearchRecord getRelationshipSearchRecord(org.osid.type.Type relationshipSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.relationship.records.RelationshipSearchRecord record : this.records) {
            if (record.implementsRecordType(relationshipSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relationshipSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relationship search. 
     *
     *  @param relationshipSearchRecord relationship search record
     *  @param relationshipSearchRecordType relationship search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelationshipSearchRecord(org.osid.relationship.records.RelationshipSearchRecord relationshipSearchRecord, 
                                           org.osid.type.Type relationshipSearchRecordType) {

        addRecordType(relationshipSearchRecordType);
        this.records.add(relationshipSearchRecord);        
        return;
    }
}
