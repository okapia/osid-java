//
// AbstractTodoProducerValidator.java
//
//     Validates a TodoProducer.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.checklist.mason.todoproducer.spi;


/**
 *  Validates a TodoProducer.
 */

public abstract class AbstractTodoProducerValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractTodoProducerValidator</code>.
     */

    protected AbstractTodoProducerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractTodoProducerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractTodoProducerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a TodoProducer.
     *
     *  @param todoProducer a todo producer to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>todoProducer</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.checklist.mason.TodoProducer todoProducer) {
        super.validate(todoProducer);

        testConditionalMethod(todoProducer, "getCyclicEventId", todoProducer.isBasedOnTimeCycle(), "isBasedOnTimeCycle()");
        testConditionalMethod(todoProducer, "getCyclicEvent", todoProducer.isBasedOnTimeCycle(), "isBasedOnTimeCycle()");

        if (todoProducer.isBasedOnTimeCycle()) {
            testNestedObject(todoProducer, "getCyclicEvent");
        }

        testConditionalMethod(todoProducer, "getStockLevel", todoProducer.isBasedOnStock(), "isBasedOnStock()");
        testConditionalMethod(todoProducer, "getStockId", todoProducer.isBasedOnStock(), "isBasedOnStock()");
        testConditionalMethod(todoProducer, "getStock", todoProducer.isBasedOnStock(), "isBasedOnStock()");

        if (todoProducer.isBasedOnStock()) {
            testCardinal(todoProducer.getStockLevel(), "getStockLevel()");
            testNestedObject(todoProducer, "getStock");
        }

        return;
    }
}
