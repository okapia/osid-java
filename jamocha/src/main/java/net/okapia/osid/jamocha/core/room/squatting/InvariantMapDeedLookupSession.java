//
// InvariantMapDeedLookupSession
//
//    Implements a Deed lookup service backed by a fixed collection of
//    deeds.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting;


/**
 *  Implements a Deed lookup service backed by a fixed
 *  collection of deeds. The deeds are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDeedLookupSession
    extends net.okapia.osid.jamocha.core.room.squatting.spi.AbstractMapDeedLookupSession
    implements org.osid.room.squatting.DeedLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDeedLookupSession</code> with no
     *  deeds.
     *  
     *  @param campus the campus
     *  @throws org.osid.NullArgumnetException {@code campus} is
     *          {@code null}
     */

    public InvariantMapDeedLookupSession(org.osid.room.Campus campus) {
        setCampus(campus);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeedLookupSession</code> with a single
     *  deed.
     *  
     *  @param campus the campus
     *  @param deed a single deed
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code deed} is <code>null</code>
     */

      public InvariantMapDeedLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.squatting.Deed deed) {
        this(campus);
        putDeed(deed);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeedLookupSession</code> using an array
     *  of deeds.
     *  
     *  @param campus the campus
     *  @param deeds an array of deeds
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code deeds} is <code>null</code>
     */

      public InvariantMapDeedLookupSession(org.osid.room.Campus campus,
                                               org.osid.room.squatting.Deed[] deeds) {
        this(campus);
        putDeeds(deeds);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeedLookupSession</code> using a
     *  collection of deeds.
     *
     *  @param campus the campus
     *  @param deeds a collection of deeds
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code deeds} is <code>null</code>
     */

      public InvariantMapDeedLookupSession(org.osid.room.Campus campus,
                                               java.util.Collection<? extends org.osid.room.squatting.Deed> deeds) {
        this(campus);
        putDeeds(deeds);
        return;
    }
}
