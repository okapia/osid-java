//
// AbstractAdapterPathLookupSession.java
//
//    A Path lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.topology.path.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Path lookup session adapter.
 */

public abstract class AbstractAdapterPathLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.topology.path.PathLookupSession {

    private final org.osid.topology.path.PathLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPathLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPathLookupSession(org.osid.topology.path.PathLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Graph/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Graph Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGraphId() {
        return (this.session.getGraphId());
    }


    /**
     *  Gets the {@code Graph} associated with this session.
     *
     *  @return the {@code Graph} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGraph());
    }


    /**
     *  Tests if this user can perform {@code Path} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPaths() {
        return (this.session.canLookupPaths());
    }


    /**
     *  A complete view of the {@code Path} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        this.session.useComparativePathView();
        return;
    }


    /**
     *  A complete view of the {@code Path} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        this.session.usePlenaryPathView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in graphs which are children
     *  of this graph in the graph hierarchy.
     */

    @OSID @Override
    public void useFederatedGraphView() {
        this.session.useFederatedGraphView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this graph only.
     */

    @OSID @Override
    public void useIsolatedGraphView() {
        this.session.useIsolatedGraphView();
        return;
    }
    

    /**
     *  Only paths whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePathView() {
        this.session.useEffectivePathView();
        return;
    }
    

    /**
     *  All paths of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePathView() {
        this.session.useAnyEffectivePathView();
        return;
    }

     
    /**
     *  Gets the {@code Path} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Path} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Path} and
     *  retained for compatibility.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param pathId {@code Id} of the {@code Path}
     *  @return the path
     *  @throws org.osid.NotFoundException {@code pathId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code pathId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPath(pathId));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Paths} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code pathIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByIds(pathIds));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  path genus {@code Type} which does not include
     *  paths of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusType(pathGenusType));
    }


    /**
     *  Gets a {@code PathList} corresponding to the given
     *  path genus {@code Type} and include any additional
     *  paths with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByParentGenusType(pathGenusType));
    }


    /**
     *  Gets a {@code PathList} containing the given
     *  path record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned {@code Path} list
     *  @throws org.osid.NullArgumentException
     *          {@code pathRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByRecordType(pathRecordType));
    }


    /**
     *  Gets a {@code PathList} effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In active mode, paths are returned that are currently
     *  active. In any status mode, active and inactive paths are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.topology.path.PathList getPathsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsOnDate(from, to));
    }
        

    /**
     *  Gets a {@code PathList} of a genus type and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  pathGenusType a path genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code
     *         edgeGenusType, from} Is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeOnDate(org.osid.type.Type pathGenusType, 
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getPathsByGenusTypeOnDate(pathGenusType, from, to));
    }


    /**
     *  Gets a list of paths corresponding to a starting node {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session.
     *
     *  In effective mode, paths are returned that are
     *  currently effective.  In any effective mode, effective
     *  paths and those currently expired are returned.
     *
     *  @param  startingNodeId the {@code Id} of the starting node
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code startingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForStartingNode(org.osid.id.Id startingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForStartingNode(startingNodeId));
    }


    /**
     *  Gets a list of paths corresponding to a starting node {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the {@code Id} of the starting node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code startingNodeId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForStartingNodeOnDate(org.osid.id.Id startingNodeId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForStartingNodeOnDate(startingNodeId, from, to));
    }


    /**
     *  Gets a {@code PathList} starting from the given {@code Node}
     *  and path genus {@code Type including} any genus {@code Types}
     *  derived from the given genus {@code Type}.
     *  
     *  In plenary mode, the returned list contains all of the paths,
     *  or an error results if a path connected to the node is not
     *  found or inaccessible. Otherwise, inaccessible {@code Paths}
     *  may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeId a node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.NullArgumentException {@code nodeId} is or 
     *          {@code pathGenusType} {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNode(org.osid.id.Id nodeId, 
                                                                              org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForStartingNode(nodeId, pathGenusType));
    }


    /**
     *  Gets a {@code PathList} of the given genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In effective mode, paths are returned that are currently effective. In 
     *  any effective mode, effective paths and those currently expired are 
     *  returned. 
     *
     *  @param  nodeId a node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code from} Is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code nodeId, pathGenusType, 
     *          from} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForStartingNodeOnDate(org.osid.id.Id nodeId, 
                                                                                    org.osid.type.Type pathGenusType, 
                                                                                    org.osid.calendaring.DateTime from, 
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForStartingNodeOnDate(nodeId, pathGenusType, from, to));
    }


    /**
     *  Gets a list of paths corresponding to a ending node {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the {@code Id} of the ending node
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code endingNodeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForEndingNode(org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForEndingNode(endingNodeId));
    }


    /**
     *  Gets a list of paths corresponding to a ending node {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the {@code Id} of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code endingNodeId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForEndingNodeOnDate(org.osid.id.Id endingNodeId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForEndingNodeOnDate(endingNodeId, from, to));
    }


    /**
     *  Gets a {@code PathList} ending from the given {@code Node}
     *  and path genus {@code Type including} any genus {@code Types}
     *  derived from the given genus {@code Type}.
     *  
     *  In plenary mode, the returned list contains all of the paths,
     *  or an error results if a path connected to the node is not
     *  found or inaccessible. Otherwise, inaccessible {@code Paths}
     *  may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeId a node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.NullArgumentException {@code nodeId} is or 
     *          {@code pathGenusType} {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNode(org.osid.id.Id nodeId, 
                                                                            org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForEndingNode(nodeId, pathGenusType));
    }


    /**
     *  Gets a {@code PathList} of the given genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In effective mode, paths are returned that are currently effective. In 
     *  any effective mode, effective paths and those currently expired are 
     *  returned. 
     *
     *  @param  nodeId a node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @param  from ending date 
     *  @param  to ending date 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code from} Is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code nodeId, pathGenusType, 
     *          from} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForEndingNodeOnDate(org.osid.id.Id nodeId, 
                                                                                  org.osid.type.Type pathGenusType, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForEndingNodeOnDate(nodeId, pathGenusType, from, to));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId the {@code Id} of the starting node
     *  @param  endingNodeId the {@code Id} of the ending node
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code startingNodeId},
     *          {@code endingNodeId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodes(org.osid.id.Id startingNodeId,
                                                            org.osid.id.Id endingNodeId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForNodes(startingNodeId, endingNodeId));
    }


    /**
     *  Gets a list of paths corresponding to starting node and ending
     *  node {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  endingNodeId the {@code Id} of the ending node
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PathList}
     *  @throws org.osid.NullArgumentException {@code startingNodeId},
     *          {@code endingNodeId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsForNodesOnDate(org.osid.id.Id startingNodeId,
                                                                  org.osid.id.Id endingNodeId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsForNodesOnDate(startingNodeId, endingNodeId, from, to));
    }


    /**
     *  Gets a {@code PathList} of the given genus type between the 
     *  given {@code Nodes}.
     *  
     *  In plenary mode, the returned list contains all of the paths,
     *  or an error results if a path connected to the node is not
     *  found or inaccessible. Otherwise, inaccessible {@code Paths}
     *  may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId starting node {@code Id} 
     *  @param  endingNodeId ending node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.NullArgumentException {@code startingNodeId,
     *          endingNodeId} or {@code pathGenusType} {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodes(org.osid.id.Id startingNodeId, 
                                                                       org.osid.id.Id endingNodeId, 
                                                                       org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForNodes(startingNodeId, endingNodeId, pathGenusType));
    }

    
    /**
     *  Gets a {@code PathList} of the given genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known paths or
     *  an error results. Otherwise, the returned list may contain
     *  only those paths that are accessible through this session.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  startingNodeId starting node {@code Id} 
     *  @param  endingNodeId ending node {@code Id} 
     *  @param  pathGenusType a path genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code from} Is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code startingNodeId, 
     *          endingNodeId, pathGenusType, from} or {@code to} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusTypeForNodesOnDate(org.osid.id.Id startingNodeId, 
                                                                             org.osid.id.Id endingNodeId, 
                                                                             org.osid.type.Type pathGenusType, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsByGenusTypeForNodesOnDate(startingNodeId, endingNodeId, pathGenusType, from, to));
    }


    /**
     *  Gets a {@code PathList} connected to all the given {@code
     *  Nodes}.
     *  
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  {@code Paths} may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.NullArgumentException {@code nodeIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodes(org.osid.id.IdList nodeIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsAlongNodes(nodeIds));
    }


    /**
     *  Gets a {@code PathList} connected to all the given {@code
     *  Nodes} and and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all of the paths
     *  through the nodes, or an error results if a path connected to
     *  the node is not found or inaccessible. Otherwise, inaccessible
     *  {@code Paths} may be omitted from the list.
     *  
     *  In effective mode, paths are returned that are currently
     *  effective. In any effective mode, effective paths and those
     *  currently expired are returned.
     *
     *  @param  nodeIds the list of {@code Ids} to retrieve 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Path} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code nodeIds, from} , 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsAlongNodesOnDate(org.osid.id.IdList nodeIds, 
                                                                    org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPathsAlongNodesOnDate(nodeIds, from, to));
    }


    /**
     *  Gets all {@code Paths}. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, paths are returned that are currently
     *  effective.  In any effective mode, effective paths and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Paths} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPaths());
    }
}
