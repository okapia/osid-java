//
// AbstractProvisionReturnSearchOdrer.java
//
//     Defines a ProvisionReturnSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionreturn.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProvisionReturnSearchOrder}.
 */

public abstract class AbstractProvisionReturnSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.provisioning.ProvisionReturnSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the return date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturnDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the returner. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturner(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturnerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReturnerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getReturnerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReturnerSearchOrder() is false");
    }


    /**
     *  Orders the results by the returning agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReturningAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReturningAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsReturningAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getReturningAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsReturningAgentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  provisionReturnRecordType a provision return record type 
     *  @return {@code true} if the provisionReturnRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code provisionReturnRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionReturnRecordType) {
        for (org.osid.provisioning.records.ProvisionReturnSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  provisionReturnRecordType the provision return record type 
     *  @return the provision return search order record
     *  @throws org.osid.NullArgumentException
     *          {@code provisionReturnRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(provisionReturnRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnSearchOrderRecord getProvisionReturnSearchOrderRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this provision return. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionReturnRecord the provision return search odrer record
     *  @param provisionReturnRecordType provision return record type
     *  @throws org.osid.NullArgumentException
     *          {@code provisionReturnRecord} or
     *          {@code provisionReturnRecordTypeprovisionReturn} is
     *          {@code null}
     */
            
    protected void addProvisionReturnRecord(org.osid.provisioning.records.ProvisionReturnSearchOrderRecord provisionReturnSearchOrderRecord, 
                                     org.osid.type.Type provisionReturnRecordType) {

        addRecordType(provisionReturnRecordType);
        this.records.add(provisionReturnSearchOrderRecord);
        
        return;
    }
}
