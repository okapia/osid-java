//
// AbstractAdapterIssueLookupSession.java
//
//    An Issue lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Issue lookup session adapter.
 */

public abstract class AbstractAdapterIssueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.tracking.IssueLookupSession {

    private final org.osid.tracking.IssueLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterIssueLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterIssueLookupSession(org.osid.tracking.IssueLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code FrontOffice/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code FrontOffice Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFrontOfficeId() {
        return (this.session.getFrontOfficeId());
    }


    /**
     *  Gets the {@code FrontOffice} associated with this session.
     *
     *  @return the {@code FrontOffice} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.FrontOffice getFrontOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFrontOffice());
    }


    /**
     *  Tests if this user can perform {@code Issue} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupIssues() {
        return (this.session.canLookupIssues());
    }


    /**
     *  A complete view of the {@code Issue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeIssueView() {
        this.session.useComparativeIssueView();
        return;
    }


    /**
     *  A complete view of the {@code Issue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryIssueView() {
        this.session.usePlenaryIssueView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include issues in front offices which are children
     *  of this front office in the front office hierarchy.
     */

    @OSID @Override
    public void useFederatedFrontOfficeView() {
        this.session.useFederatedFrontOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this front office only.
     */

    @OSID @Override
    public void useIsolatedFrontOfficeView() {
        this.session.useIsolatedFrontOfficeView();
        return;
    }
    

    /**
     *  Only issues whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveIssueView() {
        this.session.useEffectiveIssueView();
        return;
    }
    

    /**
     *  All issues of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveIssueView() {
        this.session.useAnyEffectiveIssueView();
        return;
    }

     
    /**
     *  Gets the {@code Issue} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Issue} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Issue} and
     *  retained for compatibility.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param issueId {@code Id} of the {@code Issue}
     *  @return the issue
     *  @throws org.osid.NotFoundException {@code issueId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue(org.osid.id.Id issueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssue(issueId));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  issues specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Issues} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code issueIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByIds(org.osid.id.IdList issueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByIds(issueIds));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  issue genus {@code Type} which does not include
     *  issues of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByGenusType(issueGenusType));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  issue genus {@code Type} and include any additional
     *  issues with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByParentGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByParentGenusType(issueGenusType));
    }


    /**
     *  Gets an {@code IssueList} containing the given issue record
     *  {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  issueRecordType an issue record type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByRecordType(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByRecordType(issueRecordType));
    }


    /**
     *  Gets an {@code IssueList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *  
     *  In active mode, issues are returned that are currently
     *  active. In any status mode, active and inactive issues are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Issue} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.tracking.IssueList getIssuesOnDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesOnDate(from, to));
    }
        

    /**
     *  Gets a list of issues corresponding to a queue {@code Id}.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code queueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueue(org.osid.id.Id queueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForQueue(queueId));
    }


    /**
     *  Gets a list of issues corresponding to a queue {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code queueId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueOnDate(org.osid.id.Id queueId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForQueueOnDate(queueId, from, to));
    }


    /**
     *  Gets a list of issues corresponding to a customer {@code Id}.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the customer
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForCustomer(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForCustomer(resourceId));
    }


    /**
     *  Gets a list of issues corresponding to a customer {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForCustomerOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForCustomerOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param  queueId the {@code Id} of the queue
     *  @param  resourceId the {@code Id} of the customer
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code queueId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomer(org.osid.id.Id queueId,
                                                                    org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForQueueAndCustomer(queueId, resourceId));
    }


    /**
     *  Gets a list of issues corresponding to queue and customer
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known issues
     *  or an error results. Otherwise, the returned list may contain
     *  only those issues that are accessible through this session.
     *
     *  In effective mode, issues are returned that are currently
     *  effective. In any effective mode, effective issues and those
     *  currently expired are returned.
     *
     *  @param resourceId the {@code Id} of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code IssueList}
     *  @throws org.osid.NullArgumentException {@code queueId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesForQueueAndCustomerOnDate(org.osid.id.Id queueId,
                                                                          org.osid.id.Id resourceId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesForQueueAndCustomerOnDate(queueId, resourceId, from, to));
    }


    /**
     *  Gets all {@code Issues}. 
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, issues are returned that are currently
     *  effective.  In any effective mode, effective issues and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Issues} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssues());
    }
}
