//
// AbstractCheckSearch.java
//
//     A template for making a Check Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing check searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCheckSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.rules.check.CheckSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.CheckSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.rules.check.CheckSearchOrder checkSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of checks. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  checkIds list of checks
     *  @throws org.osid.NullArgumentException
     *          <code>checkIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongChecks(org.osid.id.IdList checkIds) {
        while (checkIds.hasNext()) {
            try {
                this.ids.add(checkIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongChecks</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of check Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCheckIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  checkSearchOrder check search order 
     *  @throws org.osid.NullArgumentException
     *          <code>checkSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>checkSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCheckResults(org.osid.rules.check.CheckSearchOrder checkSearchOrder) {
	this.checkSearchOrder = checkSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.rules.check.CheckSearchOrder getCheckSearchOrder() {
	return (this.checkSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given check search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a check implementing the requested record.
     *
     *  @param checkSearchRecordType a check search record
     *         type
     *  @return the check search record
     *  @throws org.osid.NullArgumentException
     *          <code>checkSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(checkSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.CheckSearchRecord getCheckSearchRecord(org.osid.type.Type checkSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.rules.check.records.CheckSearchRecord record : this.records) {
            if (record.implementsRecordType(checkSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(checkSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this check search. 
     *
     *  @param checkSearchRecord check search record
     *  @param checkSearchRecordType check search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCheckSearchRecord(org.osid.rules.check.records.CheckSearchRecord checkSearchRecord, 
                                           org.osid.type.Type checkSearchRecordType) {

        addRecordType(checkSearchRecordType);
        this.records.add(checkSearchRecord);        
        return;
    }
}
