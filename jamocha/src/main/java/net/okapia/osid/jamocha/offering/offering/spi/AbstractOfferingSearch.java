//
// AbstractOfferingSearch.java
//
//     A template for making an Offering Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing offering searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOfferingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.OfferingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.records.OfferingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.OfferingSearchOrder offeringSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offerings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  offeringIds list of offerings
     *  @throws org.osid.NullArgumentException
     *          <code>offeringIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOfferings(org.osid.id.IdList offeringIds) {
        while (offeringIds.hasNext()) {
            try {
                this.ids.add(offeringIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOfferings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of offering Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOfferingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  offeringSearchOrder offering search order 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>offeringSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOfferingResults(org.osid.offering.OfferingSearchOrder offeringSearchOrder) {
	this.offeringSearchOrder = offeringSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.OfferingSearchOrder getOfferingSearchOrder() {
	return (this.offeringSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given offering search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offering implementing the requested record.
     *
     *  @param offeringSearchRecordType an offering search record
     *         type
     *  @return the offering search record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.OfferingSearchRecord getOfferingSearchRecord(org.osid.type.Type offeringSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.records.OfferingSearchRecord record : this.records) {
            if (record.implementsRecordType(offeringSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering search. 
     *
     *  @param offeringSearchRecord offering search record
     *  @param offeringSearchRecordType offering search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingSearchRecord(org.osid.offering.records.OfferingSearchRecord offeringSearchRecord, 
                                           org.osid.type.Type offeringSearchRecordType) {

        addRecordType(offeringSearchRecordType);
        this.records.add(offeringSearchRecord);        
        return;
    }
}
