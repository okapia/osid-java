//
// AbstractOsidExtensibleForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;


/**
 *  A basic OsidExtensibleForm.
 */

public abstract class AbstractOsidExtensibleForm
    extends AbstractOsidForm
    implements org.osid.OsidExtensibleForm {
    
    private final Types recordTypes = new TypeSet();
    private final Types requiredRecordTypes = new TypeSet();
    private final Extensible extensible = new Extensible();


    /** 
     *  Constructs a new {@code AbstractOsidExtensibleForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidExtensibleForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the required record types for this form. The required
     *  records may change as a result of other data in this form and
     *  should be checked before submission.
     *
     *  @return a list of required record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequiredRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requiredRecordTypes.toCollection()));
    }        


    /**
     *  Adds a required record type. A required record type is one
     *  that must be present for successful submission of this form.
     *
     *  @param requiredRecordType a required record type
     *  @throws org.osid.NullArgumentException
     *          <code>requiredRecordType</code> is <code>null</code>
     */

    protected void addRequiredRecordType(org.osid.type.Type requiredRecordType) {
        nullarg(requiredRecordType, "required record type");
        this.requiredRecordTypes.add(requiredRecordType);
        return;
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.extensible.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.extensible.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.extensible.addRecordType(recordType);
        return;
    }


    protected class Extensible
        extends AbstractExtensible
        implements org.osid.Extensible {
        

        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
    }
}
