//
// AbstractFederatingSupersedingEventLookupSession.java
//
//     An abstract federating adapter for a SupersedingEventLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SupersedingEventLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSupersedingEventLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.SupersedingEventLookupSession>
    implements org.osid.calendaring.SupersedingEventLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingSupersedingEventLookupSession</code>.
     */

    protected AbstractFederatingSupersedingEventLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.SupersedingEventLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>SupersedingEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSupersedingEvents() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            if (session.canLookupSupersedingEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SupersedingEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSupersedingEventView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.useComparativeSupersedingEventView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SupersedingEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySupersedingEventView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.usePlenarySupersedingEventView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  Only active superseding events are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.useActiveSupersedingEventView();
        }

        return;
    }


    /**
     *  Active and inactive superseding events are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventView() {
        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            session.useAnyStatusSupersedingEventView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>SupersedingEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SupersedingEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SupersedingEvent</code> and
     *  retained for compatibility.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventId <code>Id</code> of the
     *          <code>SupersedingEvent</code>
     *  @return the superseding event
     *  @throws org.osid.NotFoundException <code>supersedingEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>supersedingEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEvent getSupersedingEvent(org.osid.id.Id supersedingEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            try {
                return (session.getSupersedingEvent(supersedingEventId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(supersedingEventId + " not found");
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SupersedingEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByIds(org.osid.id.IdList supersedingEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.supersedingevent.MutableSupersedingEventList ret = new net.okapia.osid.jamocha.calendaring.supersedingevent.MutableSupersedingEventList();

        try (org.osid.id.IdList ids = supersedingEventIds) {
            while (ids.hasNext()) {
                ret.addSupersedingEvent(getSupersedingEvent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> which does not include
     *  superseding events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList ret = getSupersedingEventList();

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            ret.addSupersedingEventList(session.getSupersedingEventsByGenusType(supersedingEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventList</code> corresponding to the given
     *  superseding event genus <code>Type</code> and include any additional
     *  superseding events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventGenusType a supersedingEvent genus type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByParentGenusType(org.osid.type.Type supersedingEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList ret = getSupersedingEventList();

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            ret.addSupersedingEventList(session.getSupersedingEventsByParentGenusType(supersedingEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventList</code> containing the given
     *  superseding event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @param  supersedingEventRecordType a supersedingEvent record type 
     *  @return the returned <code>SupersedingEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsByRecordType(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList ret = getSupersedingEventList();

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            ret.addSupersedingEventList(session.getSupersedingEventsByRecordType(supersedingEventRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the <code>SupersedingEvents</code> related to the
     *  relative event <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the
     *  returned list may contain only those superseding events that
     *  are accessible through this session.
     *
     *  In active mode, supersdeing events are returned that are
     *  currently active. In any status mode, active and inactive
     *  superseding events are returned.
     *
     *  @param  supersededEventId <code>Id</code> of the related event
     *  @return the superseding events
     *  @throws org.osid.NotFoundException
     *          <code>supersededEventId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>supersededEventId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEventsBySupersededEvent(org.osid.id.Id supersededEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList ret = getSupersedingEventList();

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            ret.addSupersedingEventList(session.getSupersedingEventsBySupersededEvent(supersededEventId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SupersedingEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding events or an error results. Otherwise, the returned list
     *  may contain only those superseding events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding events are returned that are currently
     *  active. In any status mode, active and inactive superseding events
     *  are returned.
     *
     *  @return a list of <code>SupersedingEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList ret = getSupersedingEventList();

        for (org.osid.calendaring.SupersedingEventLookupSession session : getSessions()) {
            ret.addSupersedingEventList(session.getSupersedingEvents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.FederatingSupersedingEventList getSupersedingEventList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.ParallelSupersedingEventList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.supersedingevent.CompositeSupersedingEventList());
        }
    }
}
