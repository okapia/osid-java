//
// ActivityBundleMiter.java
//
//     Defines an ActivityBundle miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activitybundle;


/**
 *  Defines an <code>ActivityBundle</code> miter for use with the builders.
 */

public interface ActivityBundleMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.course.registration.ActivityBundle {


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    public void setCourseOffering(org.osid.course.CourseOffering courseOffering);


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void addActivity(org.osid.course.Activity activity);


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public void setActivities(java.util.Collection<org.osid.course.Activity> activities);


    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void addCredits(java.math.BigDecimal credits);


    /**
     *  Sets all the credit options.
     *
     *  @param credits a collection of credit options
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public void setCredits(java.util.Collection<java.math.BigDecimal> credits);


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void addGradingOption(org.osid.grading.GradeSystem option);


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options);


    /**
     *  Adds an ActivityBundle record.
     *
     *  @param record an activityBundle record
     *  @param recordType the type of activityBundle record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addActivityBundleRecord(org.osid.course.registration.records.ActivityBundleRecord record, org.osid.type.Type recordType);
}       


