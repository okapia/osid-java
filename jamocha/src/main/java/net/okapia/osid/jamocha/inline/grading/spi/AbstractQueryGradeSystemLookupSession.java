//
// AbstractQueryGradeSystemLookupSession.java
//
//    An inline adapter that maps a GradeSystemLookupSession to
//    a GradeSystemQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a GradeSystemLookupSession to
 *  a GradeSystemQuerySession.
 */

public abstract class AbstractQueryGradeSystemLookupSession
    extends net.okapia.osid.jamocha.grading.spi.AbstractGradeSystemLookupSession
    implements org.osid.grading.GradeSystemLookupSession {

    private final org.osid.grading.GradeSystemQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryGradeSystemLookupSession.
     *
     *  @param querySession the underlying grade system query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryGradeSystemLookupSession(org.osid.grading.GradeSystemQuerySession querySession) {
        nullarg(querySession, "grade system query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Gradebook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Gradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the <code>Gradebook</code> associated with this 
     *  session.
     *
     *  @return the <code>Gradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform <code>GradeSystem</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGradeSystems() {
        return (this.session.canSearchGradeSystems());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade systems in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    
     
    /**
     *  Gets the <code>GradeSystem</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>GradeSystem</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>GradeSystem</code> and
     *  retained for compatibility.
     *
     *  @param  gradeSystemId <code>Id</code> of the
     *          <code>GradeSystem</code>
     *  @return the grade system
     *  @throws org.osid.NotFoundException <code>gradeSystemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>gradeSystemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem(org.osid.id.Id gradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();
        query.matchId(gradeSystemId, true);
        org.osid.grading.GradeSystemList gradeSystems = this.session.getGradeSystemsByQuery(query);
        if (gradeSystems.hasNext()) {
            return (gradeSystems.getNextGradeSystem());
        } 
        
        throw new org.osid.NotFoundException(gradeSystemId + " not found");
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystems specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>GradeSystems</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  gradeSystemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByIds(org.osid.id.IdList gradeSystemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();

        try (org.osid.id.IdList ids = gradeSystemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getGradeSystemsByQuery(query));
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  grade system genus <code>Type</code> which does not include
     *  grade systems of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();
        query.matchGenusType(gradeSystemGenusType, true);
        return (this.session.getGradeSystemsByQuery(query));
    }


    /**
     *  Gets a <code>GradeSystemList</code> corresponding to the given
     *  grade system genus <code>Type</code> and include any additional
     *  grade systems with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByParentGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();
        query.matchParentGenusType(gradeSystemGenusType, true);
        return (this.session.getGradeSystemsByQuery(query));
    }


    /**
     *  Gets a <code>GradeSystemList</code> containing the given
     *  grade system record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemRecordType a gradeSystem record type 
     *  @return the returned <code>GradeSystem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByRecordType(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();
        query.matchRecordType(gradeSystemRecordType, true);
        return (this.session.getGradeSystemsByQuery(query));
    }

    
    /**
     *  Gets all <code>GradeSystems</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>GradeSystems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.grading.GradeSystemQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getGradeSystemsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.grading.GradeSystemQuery getQuery() {
        org.osid.grading.GradeSystemQuery query = this.session.getGradeSystemQuery();
        return (query);
    }
}
