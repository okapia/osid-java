//
// AbstractOfferingConstrainerSearchOdrer.java
//
//     Defines an OfferingConstrainerSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code OfferingConstrainerSearchOrder}.
 */

public abstract class AbstractOfferingConstrainerSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidConstrainerSearchOrder
    implements org.osid.offering.rules.OfferingConstrainerSearchOrder {

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideDescription(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the title 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideTitle(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the code 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideCode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the time periods 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideTimePeriods(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  time periods flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainTimePeriods(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideResultOptions(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  result options flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainResultOptions(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideSponsors(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  sponsors flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainSponsors(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  offeringConstrainerRecordType an offering constrainer record type 
     *  @return {@code true} if the offeringConstrainerRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        for (org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  offeringConstrainerRecordType the offering constrainer record type 
     *  @return the offering constrainer search order record
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(offeringConstrainerRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord getOfferingConstrainerSearchOrderRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this offering constrainer. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offeringConstrainerRecord the offering constrainer search odrer record
     *  @param offeringConstrainerRecordType offering constrainer record type
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerRecord} or
     *          {@code offeringConstrainerRecordTypeofferingConstrainer} is
     *          {@code null}
     */
            
    protected void addOfferingConstrainerRecord(org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord offeringConstrainerSearchOrderRecord, 
                                     org.osid.type.Type offeringConstrainerRecordType) {

        addRecordType(offeringConstrainerRecordType);
        this.records.add(offeringConstrainerSearchOrderRecord);
        
        return;
    }
}
