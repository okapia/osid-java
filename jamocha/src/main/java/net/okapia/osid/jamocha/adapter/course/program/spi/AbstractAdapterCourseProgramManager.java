//
// AbstractCourseProgramManager.java
//
//     An adapter for a CourseProgramManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseProgramManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseProgramManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.program.CourseProgramManager>
    implements org.osid.course.program.CourseProgramManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseProgramManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseProgramManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseProgramManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseProgramManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up programs is supported. 
     *
     *  @return <code> true </code> if program lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramLookup() {
        return (getAdapteeManager().supportsProgramLookup());
    }


    /**
     *  Tests if querying programs is supported. 
     *
     *  @return <code> true </code> if program query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (getAdapteeManager().supportsProgramQuery());
    }


    /**
     *  Tests if searching programs is supported. 
     *
     *  @return <code> true </code> if program search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearch() {
        return (getAdapteeManager().supportsProgramSearch());
    }


    /**
     *  Tests if program administrative service is supported. 
     *
     *  @return <code> true </code> if program administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramAdmin() {
        return (getAdapteeManager().supportsProgramAdmin());
    }


    /**
     *  Tests if a program <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if program notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramNotification() {
        return (getAdapteeManager().supportsProgramNotification());
    }


    /**
     *  Tests if a program cataloging service is supported. 
     *
     *  @return <code> true </code> if program cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramCourseCatalog() {
        return (getAdapteeManager().supportsProgramCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps programs to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramCourseCatalogAssignment() {
        return (getAdapteeManager().supportsProgramCourseCatalogAssignment());
    }


    /**
     *  Tests if a program smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program smart course catalog session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSmartCourseCatalog() {
        return (getAdapteeManager().supportsProgramSmartCourseCatalog());
    }


    /**
     *  Tests if a course/program lookup service is supported. 
     *
     *  @return <code> true </code> if course/program lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramLookup() {
        return (getAdapteeManager().supportsCourseProgramLookup());
    }


    /**
     *  Tests if a course/program mapping service is supported. 
     *
     *  @return <code> true </code> if course/program mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramAssignment() {
        return (getAdapteeManager().supportsCourseProgramAssignment());
    }


    /**
     *  Tests if looking up program offerings is supported. 
     *
     *  @return <code> true </code> if program offering lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingLookup() {
        return (getAdapteeManager().supportsProgramOfferingLookup());
    }


    /**
     *  Tests if querying program offerings is supported. 
     *
     *  @return <code> true </code> if program offering query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (getAdapteeManager().supportsProgramOfferingQuery());
    }


    /**
     *  Tests if searching program offerings is supported. 
     *
     *  @return <code> true </code> if program offering search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearch() {
        return (getAdapteeManager().supportsProgramOfferingSearch());
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if program offering administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingAdmin() {
        return (getAdapteeManager().supportsProgramOfferingAdmin());
    }


    /**
     *  Tests if a program offering <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if program offering notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingNotification() {
        return (getAdapteeManager().supportsProgramOfferingNotification());
    }


    /**
     *  Tests if a program offering cataloging service is supported. 
     *
     *  @return <code> true </code> if program offering catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingCourseCatalog() {
        return (getAdapteeManager().supportsProgramOfferingCourseCatalog());
    }


    /**
     *  Tests if a program offering cataloging service is supported. A 
     *  cataloging service maps program offerings to catalogs. 
     *
     *  @return <code> true </code> if program offering cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingCourseCatalogAssignment() {
        return (getAdapteeManager().supportsProgramOfferingCourseCatalogAssignment());
    }


    /**
     *  Tests if a program offering smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program offering smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSmartCourseCatalog() {
        return (getAdapteeManager().supportsProgramOfferingSmartCourseCatalog());
    }


    /**
     *  Tests if looking up courses is supported. 
     *
     *  @return <code> true </code> if course lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialLookup() {
        return (getAdapteeManager().supportsCredentialLookup());
    }


    /**
     *  Tests if querying courses is supported. 
     *
     *  @return <code> true </code> if credential query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (getAdapteeManager().supportsCredentialQuery());
    }


    /**
     *  Tests if searching courses is supported. 
     *
     *  @return <code> true </code> if credential search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSearch() {
        return (getAdapteeManager().supportsCredentialSearch());
    }


    /**
     *  Tests if course <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if course administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialAdmin() {
        return (getAdapteeManager().supportsCredentialAdmin());
    }


    /**
     *  Tests if a course <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if course notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialNotification() {
        return (getAdapteeManager().supportsCredentialNotification());
    }


    /**
     *  Tests if a course cataloging service is supported. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialCourseCatalog() {
        return (getAdapteeManager().supportsCredentialCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps courses to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialCourseCatalogAssignment() {
        return (getAdapteeManager().supportsCredentialCourseCatalogAssignment());
    }


    /**
     *  Tests if a credential smart course catalog session is available. 
     *
     *  @return <code> true </code> if a credential smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSmartCourseCatalog() {
        return (getAdapteeManager().supportsCredentialSmartCourseCatalog());
    }


    /**
     *  Tests if looking up enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentLookup() {
        return (getAdapteeManager().supportsEnrollmentLookup());
    }


    /**
     *  Tests if querying enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentQuery() {
        return (getAdapteeManager().supportsEnrollmentQuery());
    }


    /**
     *  Tests if searching enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentSearch() {
        return (getAdapteeManager().supportsEnrollmentSearch());
    }


    /**
     *  Tests if an enrollment <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if enrollment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentAdmin() {
        return (getAdapteeManager().supportsEnrollmentAdmin());
    }


    /**
     *  Tests if an enrollment <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if enrollment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentNotification() {
        return (getAdapteeManager().supportsEnrollmentNotification());
    }


    /**
     *  Tests if an enrollment cataloging service is supported. 
     *
     *  @return <code> true </code> if enrollment catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentCourseCatalog() {
        return (getAdapteeManager().supportsEnrollmentCourseCatalog());
    }


    /**
     *  Tests if an enrollment cataloging service is supported. A cataloging 
     *  service maps enrollments to catalogs. 
     *
     *  @return <code> true </code> if enrollment cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentCourseCatalogAssignment() {
        return (getAdapteeManager().supportsEnrollmentCourseCatalogAssignment());
    }


    /**
     *  Tests if an enrollment smart course catalog session is available. 
     *
     *  @return <code> true </code> if an enrollment smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentSmartCourseCatalog() {
        return (getAdapteeManager().supportsEnrollmentSmartCourseCatalog());
    }


    /**
     *  Tests if a course program batch service is available. 
     *
     *  @return <code> true </code> if a course program batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramBatch() {
        return (getAdapteeManager().supportsCourseProgramBatch());
    }


    /**
     *  Gets the supported <code> program </code> record types. 
     *
     *  @return a list containing the supported <code> Program </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramRecordTypes() {
        return (getAdapteeManager().getProgramRecordTypes());
    }


    /**
     *  Tests if the given <code> program </code> record type is supported. 
     *
     *  @param  programRecordType a <code> Type </code> indicating a <code> 
     *          program </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramRecordType(org.osid.type.Type programRecordType) {
        return (getAdapteeManager().supportsProgramRecordType(programRecordType));
    }


    /**
     *  Gets the supported <code> program </code> search record types. 
     *
     *  @return a list containing the supported <code> program </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramSearchRecordTypes() {
        return (getAdapteeManager().getProgramSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> program </code> search record type is 
     *  supported. 
     *
     *  @param  programSearchRecordType a <code> Type </code> indicating a 
     *          <code> Program </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramSearchRecordType(org.osid.type.Type programSearchRecordType) {
        return (getAdapteeManager().supportsProgramSearchRecordType(programSearchRecordType));
    }


    /**
     *  Gets the supported <code> ProgramOffering </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramOffering </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramOfferingRecordTypes() {
        return (getAdapteeManager().getProgramOfferingRecordTypes());
    }


    /**
     *  Tests if the given <code> ProgramOffering </code> record type is 
     *  supported. 
     *
     *  @param  programOfferingRecordType a <code> Type </code> indicating an 
     *          <code> ProgramOffering </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programOfferingRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramOfferingRecordType(org.osid.type.Type programOfferingRecordType) {
        return (getAdapteeManager().supportsProgramOfferingRecordType(programOfferingRecordType));
    }


    /**
     *  Gets the supported <code> ProgramOffering </code> search record types. 
     *
     *  @return a list containing the supported <code> ProgramOffering </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramOfferingSearchRecordTypes() {
        return (getAdapteeManager().getProgramOfferingSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ProgramOffering </code> search record type 
     *  is supported. 
     *
     *  @param  programOfferingSearchRecordType a <code> Type </code> 
     *          indicating an <code> ProgramOffering </code> search record 
     *          type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programOfferingSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearchRecordType(org.osid.type.Type programOfferingSearchRecordType) {
        return (getAdapteeManager().supportsProgramOfferingSearchRecordType(programOfferingSearchRecordType));
    }


    /**
     *  Gets the supported <code> credential </code> record types. 
     *
     *  @return a list containing the supported <code> credential </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialRecordTypes() {
        return (getAdapteeManager().getCredentialRecordTypes());
    }


    /**
     *  Tests if the given <code> credential </code> record type is supported. 
     *
     *  @param  credentialRecordType a <code> Type </code> indicating a <code> 
     *          credential </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> credentialRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialRecordType(org.osid.type.Type credentialRecordType) {
        return (getAdapteeManager().supportsCredentialRecordType(credentialRecordType));
    }


    /**
     *  Gets the supported <code> credential </code> search record types. 
     *
     *  @return a list containing the supported <code> credential </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialSearchRecordTypes() {
        return (getAdapteeManager().getCredentialSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> credential </code> search record type is 
     *  supported. 
     *
     *  @param  credentialSearchRecordType a <code> Type </code> indicating a 
     *          <code> credential </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialSearchRecordType(org.osid.type.Type credentialSearchRecordType) {
        return (getAdapteeManager().supportsCredentialSearchRecordType(credentialSearchRecordType));
    }


    /**
     *  Gets the supported <code> Enrollment </code> record types. 
     *
     *  @return a list containing the supported <code> Enrollment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEnrollmentRecordTypes() {
        return (getAdapteeManager().getEnrollmentRecordTypes());
    }


    /**
     *  Tests if the given <code> Enrollment </code> record type is supported. 
     *
     *  @param  enrollmentRecordType a <code> Type </code> indicating an 
     *          <code> Enrollment </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> enrollmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEnrollmentRecordType(org.osid.type.Type enrollmentRecordType) {
        return (getAdapteeManager().supportsEnrollmentRecordType(enrollmentRecordType));
    }


    /**
     *  Gets the supported <code> Enrollment </code> search record types. 
     *
     *  @return a list containing the supported <code> Enrollment </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEnrollmentSearchRecordTypes() {
        return (getAdapteeManager().getEnrollmentSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Enrollment </code> search record type is 
     *  supported. 
     *
     *  @param  enrollmentSearchRecordType a <code> Type </code> indicating an 
     *          <code> Enrollment </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          enrollmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEnrollmentSearchRecordType(org.osid.type.Type enrollmentSearchRecordType) {
        return (getAdapteeManager().supportsEnrollmentSearchRecordType(enrollmentSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service. 
     *
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service. 
     *
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service. 
     *
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service. 
     *
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  notification service. 
     *
     *  @param  programReceiver the notification callback 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSession(org.osid.course.program.ProgramReceiver programReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramNotificationSession(programReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  programReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSessionForCourseCatalog(org.osid.course.program.ProgramReceiver programReceiver, 
                                                                                                            org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramNotificationSessionForCourseCatalog(programReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program/catalog 
     *  mappings. 
     *
     *  @return a <code> ProgramCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogSession getProgramCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning programs 
     *  to course catalogs. 
     *
     *  @return a <code> ProgramCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogAssignmentSession getProgramCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSmartCourseCatalogSession getProgramSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings. 
     *
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings. 
     *
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramAssignmentSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service. 
     *
     *  @return a <code> ProgramOfferingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ProgramOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service. 
     *
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramOfferingQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service. 
     *
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service. 
     *
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSession(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingNotificationSession(programOfferingReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service for the given course catalog. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSessionForCourseCatalog(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver, 
                                                                                                                            org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingNotificationSessionForCourseCatalog(programOfferingReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program offering/catalog 
     *  mappings. 
     *
     *  @return a <code> ProgramOfferingCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogSession getProgramOfferingCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  offerings to course catalogs. 
     *
     *  @return a <code> ProgramOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogAssignmentSession getProgramOfferingCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSmartCourseCatalogSession getProgramOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramOfferingSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service. 
     *
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service. 
     *
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service. 
     *
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  administration service. 
     *
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @return a <code> CredentialNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSession(org.osid.course.program.CredentialReceiver credentialReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialNotificationSession(credentialReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSessionForCourseCatalog(org.osid.course.program.CredentialReceiver credentialReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialNotificationSessionForCourseCatalog(credentialReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential/catalog 
     *  mappings. 
     *
     *  @return a <code> CredentialCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogSession getCredentialCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credentials to course catalogs. 
     *
     *  @return a <code> CredentialCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogAssignmentSession getCredentialCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSmartCourseCatalogSession getCredentialSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service. 
     *
     *  @return an <code> EnrollmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> EnrollmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service. 
     *
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service. 
     *
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service. 
     *
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSession(org.osid.course.program.EnrollmentReceiver enrollmentReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentNotificationSession(enrollmentReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service for the given course catalog. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSessionForCourseCatalog(org.osid.course.program.EnrollmentReceiver enrollmentReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentNotificationSessionForCourseCatalog(enrollmentReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup enrollment/catalog 
     *  mappings. 
     *
     *  @return an <code> EnrollmentCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogSession getEnrollmentCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  enrollments to course catalogs. 
     *
     *  @return an <code> EnrollmentCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogAssignmentSession getEnrollmentCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSmartCourseCatalogSession getEnrollmentSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEnrollmentSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets a <code> CourseProgramBatchManager. </code> 
     *
     *  @return a <code> CourseProgramBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.CourseProgramBatchManager getCourseProgramBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
