//
// AbstractLocationSearchOdrer.java
//
//     Defines a LocationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.location.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code LocationSearchOrder}.
 */

public abstract class AbstractLocationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.mapping.LocationSearchOrder {

    private final java.util.Collection<org.osid.mapping.records.LocationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  locationRecordType a location record type 
     *  @return {@code true} if the locationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code locationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type locationRecordType) {
        for (org.osid.mapping.records.LocationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  locationRecordType the location record type 
     *  @return the location search order record
     *  @throws org.osid.NullArgumentException
     *          {@code locationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(locationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.mapping.records.LocationSearchOrderRecord getLocationSearchOrderRecord(org.osid.type.Type locationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.LocationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(locationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(locationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this location. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param locationRecord the location search odrer record
     *  @param locationRecordType location record type
     *  @throws org.osid.NullArgumentException
     *          {@code locationRecord} or
     *          {@code locationRecordTypelocation} is
     *          {@code null}
     */
            
    protected void addLocationRecord(org.osid.mapping.records.LocationSearchOrderRecord locationSearchOrderRecord, 
                                     org.osid.type.Type locationRecordType) {

        addRecordType(locationRecordType);
        this.records.add(locationSearchOrderRecord);
        
        return;
    }
}
