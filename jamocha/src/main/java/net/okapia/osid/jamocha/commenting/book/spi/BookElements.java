//
// BookElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.book.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BookElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the BookElement Id.
     *
     *  @return the book element Id
     */

    public static org.osid.id.Id getBookEntityId() {
        return (makeEntityId("osid.commenting.Book"));
    }


    /**
     *  Gets the CommentId element Id.
     *
     *  @return the CommentId element Id
     */

    public static org.osid.id.Id getCommentId() {
        return (makeQueryElementId("osid.commenting.book.CommentId"));
    }


    /**
     *  Gets the Comment element Id.
     *
     *  @return the Comment element Id
     */

    public static org.osid.id.Id getComment() {
        return (makeQueryElementId("osid.commenting.book.Comment"));
    }


    /**
     *  Gets the AncestorBookId element Id.
     *
     *  @return the AncestorBookId element Id
     */

    public static org.osid.id.Id getAncestorBookId() {
        return (makeQueryElementId("osid.commenting.book.AncestorBookId"));
    }


    /**
     *  Gets the AncestorBook element Id.
     *
     *  @return the AncestorBook element Id
     */

    public static org.osid.id.Id getAncestorBook() {
        return (makeQueryElementId("osid.commenting.book.AncestorBook"));
    }


    /**
     *  Gets the DescendantBookId element Id.
     *
     *  @return the DescendantBookId element Id
     */

    public static org.osid.id.Id getDescendantBookId() {
        return (makeQueryElementId("osid.commenting.book.DescendantBookId"));
    }


    /**
     *  Gets the DescendantBook element Id.
     *
     *  @return the DescendantBook element Id
     */

    public static org.osid.id.Id getDescendantBook() {
        return (makeQueryElementId("osid.commenting.book.DescendantBook"));
    }
}
