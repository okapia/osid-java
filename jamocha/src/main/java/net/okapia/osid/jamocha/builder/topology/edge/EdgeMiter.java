//
// EdgeMiter.java
//
//     Defines an Edge miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.topology.edge;


/**
 *  Defines an <code>Edge</code> miter for use with the builders.
 */

public interface EdgeMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.topology.Edge {


    /**
     *  Sets the directional flag.
     *
     *  @param directional <code>true</code> if this is a directional
     *         edge, <code>false</code> if non-directional
     */

    public void setDirectional(boolean directional);


    /**
     *  Sets the bidirectional flag.
     *
     *  @param biDirectional <code>true</code> if this is a
     *         bidirectional edge, <code>false</code> if a
     *         unidirectional edge
     */

    public void setBiDirectional(boolean biDirectional);
    
    
    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public void setCost(java.math.BigDecimal cost);


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public void setDistance(java.math.BigDecimal distance);


    /**
     *  Sets the source node.
     *
     *  @param node a source node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void setSourceNode(org.osid.topology.Node node);


    /**
     *  Sets the destination node.
     *
     *  @param node a destination node
     *  @throws org.osid.NullArgumentException <code>node</code> is
     *          <code>null</code>
     */

    public void setDestinationNode(org.osid.topology.Node node);


    /**
     *  Adds an Edge record.
     *
     *  @param record an edge record
     *  @param recordType the type of edge record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addEdgeRecord(org.osid.topology.records.EdgeRecord record, org.osid.type.Type recordType);
}       


