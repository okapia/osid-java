//
// AbstractDictionaryQueryInspector.java
//
//     A template for making a DictionaryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for dictionaries.
 */

public abstract class AbstractDictionaryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.dictionary.DictionaryQueryInspector {

    private final java.util.Collection<org.osid.dictionary.records.DictionaryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.dictionary.EntryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorDictionaryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getAncestorDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }


    /**
     *  Gets the descendant dictionary <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantDictionaryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant dictionary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryQueryInspector[] getDescendantDictionaryTerms() {
        return (new org.osid.dictionary.DictionaryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given dictionary query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a dictionary implementing the requested record.
     *
     *  @param dictionaryRecordType a dictionary record type
     *  @return the dictionary query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dictionaryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.dictionary.records.DictionaryQueryInspectorRecord getDictionaryQueryInspectorRecord(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.dictionary.records.DictionaryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(dictionaryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dictionaryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dictionary query. 
     *
     *  @param dictionaryQueryInspectorRecord dictionary query inspector
     *         record
     *  @param dictionaryRecordType dictionary record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDictionaryQueryInspectorRecord(org.osid.dictionary.records.DictionaryQueryInspectorRecord dictionaryQueryInspectorRecord, 
                                                   org.osid.type.Type dictionaryRecordType) {

        addRecordType(dictionaryRecordType);
        nullarg(dictionaryRecordType, "dictionary record type");
        this.records.add(dictionaryQueryInspectorRecord);        
        return;
    }
}
