//
// AbstractCommunicationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.communication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCommunicationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.communication.CommunicationManager,
               org.osid.communication.CommunicationProxyManager {

    private final Types communiqueRecordTypes              = new TypeRefSet();
    private final Types responseOptionRecordTypes          = new TypeRefSet();
    private final Types responseRecordTypes                = new TypeRefSet();

    /**
     *  Constructs a new <code>AbstractCommunicationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCommunicationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if a communication service is available. 
     *
     *  @return <code> true </code> if communication is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunication() {
        return (false);
    }


    /**
     *  Gets the supported <code> Communique </code> record types. 
     *
     *  @return a list containing the supported <code> Communique </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommuniqueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.communiqueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Communique </code> record type is supported. 
     *
     *  @param  communiqueRecordType a <code> Type </code> indicating a <code> 
     *          Communique </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> communiqueRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommuniqueRecordType(org.osid.type.Type communiqueRecordType) {
        return (this.communiqueRecordTypes.contains(communiqueRecordType));
    }


    /**
     *  Adds support for a communique record type.
     *
     *  @param communiqueRecordType a communique record type
     *  @throws org.osid.NullArgumentException
     *  <code>communiqueRecordType</code> is <code>null</code>
     */

    protected void addCommuniqueRecordType(org.osid.type.Type communiqueRecordType) {
        this.communiqueRecordTypes.add(communiqueRecordType);
        return;
    }


    /**
     *  Removes support for a communique record type.
     *
     *  @param communiqueRecordType a communique record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>communiqueRecordType</code> is <code>null</code>
     */

    protected void removeCommuniqueRecordType(org.osid.type.Type communiqueRecordType) {
        this.communiqueRecordTypes.remove(communiqueRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ResponseOption </code> record types. 
     *
     *  @return a list containing the supported <code> ResponseOption </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseOptionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.responseOptionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ResponseOption </code> record type is 
     *  supported. 
     *
     *  @param  responseOptionRecordType a <code> Type </code> indicating a 
     *          <code> ResponseOption </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseOptionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseOptionRecordType(org.osid.type.Type responseOptionRecordType) {
        return (this.responseOptionRecordTypes.contains(responseOptionRecordType));
    }


    /**
     *  Adds support for a response option record type.
     *
     *  @param responseOptionRecordType a response option record type
     *  @throws org.osid.NullArgumentException
     *  <code>responseOptionRecordType</code> is <code>null</code>
     */

    protected void addResponseOptionRecordType(org.osid.type.Type responseOptionRecordType) {
        this.responseOptionRecordTypes.add(responseOptionRecordType);
        return;
    }


    /**
     *  Removes support for a response option record type.
     *
     *  @param responseOptionRecordType a response option record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>responseOptionRecordType</code> is <code>null</code>
     */

    protected void removeResponseOptionRecordType(org.osid.type.Type responseOptionRecordType) {
        this.responseOptionRecordTypes.remove(responseOptionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Response </code> record types. 
     *
     *  @return a list containing the supported <code> Response </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.responseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Response </code> record type is supported. 
     *
     *  @param  responseRecordType a <code> Type </code> indicating a <code> 
     *          Response </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (this.responseRecordTypes.contains(responseRecordType));
    }


    /**
     *  Adds support for a response record type.
     *
     *  @param responseRecordType a response record type
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void addResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.add(responseRecordType);
        return;
    }


    /**
     *  Removes support for a response record type.
     *
     *  @param responseRecordType a response record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void removeResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.remove(responseRecordType);
        return;
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the communication 
     *  service. 
     *
     *  @param  receiver the communication receiver 
     *  @return a <code> CommunicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommunication() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationSession getCommunicationSession(org.osid.communication.CommunicationReceiver receiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.communication.CommunicationManager.getCommunicationSession not implemented");
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the communication 
     *  service. 
     *
     *  @param  receiver the communication receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CommunicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommunication() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationSession getCommunicationSession(org.osid.communication.CommunicationReceiver receiver, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.communication.CommunicationProxyManager.getCommunicationSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.communiqueRecordTypes.clear();
        this.communiqueRecordTypes.clear();

        this.responseOptionRecordTypes.clear();
        this.responseOptionRecordTypes.clear();

        this.responseRecordTypes.clear();
        this.responseRecordTypes.clear();

        return;
    }
}
