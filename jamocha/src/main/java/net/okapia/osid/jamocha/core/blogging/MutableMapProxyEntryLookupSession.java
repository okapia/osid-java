//
// MutableMapProxyEntryLookupSession
//
//    Implements an Entry lookup service backed by a collection of
//    entries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging;


/**
 *  Implements an Entry lookup service backed by a collection of
 *  entries. The entries are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of entries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyEntryLookupSession
    extends net.okapia.osid.jamocha.core.blogging.spi.AbstractMapEntryLookupSession
    implements org.osid.blogging.EntryLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyEntryLookupSession}
     *  with no entries.
     *
     *  @param blog the blog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blog} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyEntryLookupSession(org.osid.blogging.Blog blog,
                                                  org.osid.proxy.Proxy proxy) {
        setBlog(blog);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyEntryLookupSession} with a
     *  single entry.
     *
     *  @param blog the blog
     *  @param entry an entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blog},
     *          {@code entry}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEntryLookupSession(org.osid.blogging.Blog blog,
                                                org.osid.blogging.Entry entry, org.osid.proxy.Proxy proxy) {
        this(blog, proxy);
        putEntry(entry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEntryLookupSession} using an
     *  array of entries.
     *
     *  @param blog the blog
     *  @param entries an array of entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blog},
     *          {@code entries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEntryLookupSession(org.osid.blogging.Blog blog,
                                                org.osid.blogging.Entry[] entries, org.osid.proxy.Proxy proxy) {
        this(blog, proxy);
        putEntries(entries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyEntryLookupSession} using a
     *  collection of entries.
     *
     *  @param blog the blog
     *  @param entries a collection of entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blog},
     *          {@code entries}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyEntryLookupSession(org.osid.blogging.Blog blog,
                                                java.util.Collection<? extends org.osid.blogging.Entry> entries,
                                                org.osid.proxy.Proxy proxy) {
   
        this(blog, proxy);
        setSessionProxy(proxy);
        putEntries(entries);
        return;
    }

    
    /**
     *  Makes a {@code Entry} available in this session.
     *
     *  @param entry an entry
     *  @throws org.osid.NullArgumentException {@code entry{@code 
     *          is {@code null}
     */

    @Override
    public void putEntry(org.osid.blogging.Entry entry) {
        super.putEntry(entry);
        return;
    }


    /**
     *  Makes an array of entries available in this session.
     *
     *  @param entries an array of entries
     *  @throws org.osid.NullArgumentException {@code entries{@code 
     *          is {@code null}
     */

    @Override
    public void putEntries(org.osid.blogging.Entry[] entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Makes collection of entries available in this session.
     *
     *  @param entries
     *  @throws org.osid.NullArgumentException {@code entry{@code 
     *          is {@code null}
     */

    @Override
    public void putEntries(java.util.Collection<? extends org.osid.blogging.Entry> entries) {
        super.putEntries(entries);
        return;
    }


    /**
     *  Removes a Entry from this session.
     *
     *  @param entryId the {@code Id} of the entry
     *  @throws org.osid.NullArgumentException {@code entryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeEntry(org.osid.id.Id entryId) {
        super.removeEntry(entryId);
        return;
    }    
}
