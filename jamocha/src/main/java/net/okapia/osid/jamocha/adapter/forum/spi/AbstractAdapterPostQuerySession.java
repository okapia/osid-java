//
// AbstractQueryPostLookupSession.java
//
//    A PostQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PostQuerySession adapter.
 */

public abstract class AbstractAdapterPostQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.forum.PostQuerySession {

    private final org.osid.forum.PostQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterPostQuerySession.
     *
     *  @param session the underlying post query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPostQuerySession(org.osid.forum.PostQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeForum</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeForum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.session.getForumId());
    }


    /**
     *  Gets the {@codeForum</code> associated with this 
     *  session.
     *
     *  @return the {@codeForum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getForum());
    }


    /**
     *  Tests if this user can perform {@codePost</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchPosts() {
        return (this.session.canSearchPosts());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.session.useFederatedForumView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this forum only.
     */
    
    @OSID @Override
    public void useIsolatedForumView() {
        this.session.useIsolatedForumView();
        return;
    }
    
      
    /**
     *  Gets a post query. The returned query will not have an
     *  extension query.
     *
     *  @return the post query 
     */
      
    @OSID @Override
    public org.osid.forum.PostQuery getPostQuery() {
        return (this.session.getPostQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  postQuery the post query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code postQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code postQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByQuery(org.osid.forum.PostQuery postQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getPostsByQuery(postQuery));
    }
}
