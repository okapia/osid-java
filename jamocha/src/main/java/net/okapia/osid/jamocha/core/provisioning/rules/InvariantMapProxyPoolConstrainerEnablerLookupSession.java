//
// InvariantMapProxyPoolConstrainerEnablerLookupSession
//
//    Implements a PoolConstrainerEnabler lookup service backed by a fixed
//    collection of poolConstrainerEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolConstrainerEnabler lookup service backed by a fixed
 *  collection of pool constrainer enablers. The pool constrainer enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPoolConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPoolConstrainerEnablerLookupSession} with no
     *  pool constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPoolConstrainerEnablerLookupSession} with a single
     *  pool constrainer enabler.
     *
     *  @param distributor the distributor
     *  @param poolConstrainerEnabler a single pool constrainer enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolConstrainerEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPoolConstrainerEnabler(poolConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPoolConstrainerEnablerLookupSession} using
     *  an array of pool constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainerEnablers an array of pool constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.rules.PoolConstrainerEnabler[] poolConstrainerEnablers, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPoolConstrainerEnablers(poolConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPoolConstrainerEnablerLookupSession} using a
     *  collection of pool constrainer enablers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainerEnablers a collection of pool constrainer enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code poolConstrainerEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPoolConstrainerEnablerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putPoolConstrainerEnablers(poolConstrainerEnablers);
        return;
    }
}
