//
// InvariantMapRouteLookupSession
//
//    Implements a Route lookup service backed by a fixed collection of
//    routes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.route;


/**
 *  Implements a Route lookup service backed by a fixed
 *  collection of routes. The routes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRouteLookupSession
    extends net.okapia.osid.jamocha.core.mapping.route.spi.AbstractMapRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRouteLookupSession</code> with no
     *  routes.
     */

    public InvariantMapRouteLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRouteLookupSession</code> with a single
     *  route.
     *  
     *  @param  route a single route
     *  @throws org.osid.NullArgumentException <code>route</code>
     *          is <code>null</code>
     */

    public InvariantMapRouteLookupSession(org.osid.mapping.route.Route route) {
        putRoute(route);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRouteLookupSession</code> using an
     *  array of routes.
     *
     *  @param  routes an array of routes
     *  @throws org.osid.NullArgumentException <code>routes</code>
     *          is <code>null</code>
     */

    public InvariantMapRouteLookupSession(org.osid.mapping.route.Route[] routes) {
        putRoutes(routes);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRouteLookupSession</code> using a
     *  collection of routes.
     *
     *  @param  routes a collection of routes
     *  @throws org.osid.NullArgumentException <code>routes</code>
     *          is <code>null</code>
     */

    public InvariantMapRouteLookupSession(java.util.Collection<? extends org.osid.mapping.route.Route> routes) {
        putRoutes(routes);
        return;
    }
}
