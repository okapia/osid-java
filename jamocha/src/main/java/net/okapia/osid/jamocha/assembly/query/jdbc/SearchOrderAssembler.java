//
// SearchOrderAssembler.java
//
//     An interface for assembling a search orders.
//
//
// Tom Coppeto
// OnTapSolutions
// 23 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A utility interface for assembling SQL search orders.
 */

public class SearchOrderAssembler 
    implements net.okapia.osid.jamocha.assembly.query.SearchOrderAssembler {

    private final java.util.Collection<String> orders = new java.util.ArrayList<String>();


    /**
     *  Adds an ordering request to this search.
     *
     *  @param key the <code>QueryTerm</code> key to order by
     *  @throws org.osid.NullArgumentException <code>key</code>
     *          is <code>null</code>
     */

    public void addSearchOrder(String key) {
        nullarg(key, "order by");
        this.orders.add(key);
        return;
    }


    /**
     *  Gets the search orders.
     *
     *  @return an iterable of search order terms
     */

    public Iterable<String> getSearchOrder() {
        return (this.orders);
    }


    /**
     *  Gets the resulting search string for limits and ordering.
     *
     *  @return the order string
     */

    public String getOrderString() {
        StringBuffer sb = new StringBuffer();

        boolean first = true;
        for (String s : getSearchOrder()) {
            if (!first) {
                sb.append(',');
            }
            sb.append(s);
            first = false;
        }
        
        return (sb.toString());
    }
}
