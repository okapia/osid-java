//
// AbstractQueryScheduleSlotLookupSession.java
//
//    A ScheduleSlotQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ScheduleSlotQuerySession adapter.
 */

public abstract class AbstractAdapterScheduleSlotQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.ScheduleSlotQuerySession {

    private final org.osid.calendaring.ScheduleSlotQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterScheduleSlotQuerySession.
     *
     *  @param session the underlying schedule slot query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterScheduleSlotQuerySession(org.osid.calendaring.ScheduleSlotQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCalendar</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCalendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@codeCalendar</code> associated with this 
     *  session.
     *
     *  @return the {@codeCalendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@codeScheduleSlot</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchScheduleSlots() {
        return (this.session.canSearchScheduleSlots());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedule slots in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this calendar only.
     */
    
    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods omit sequestered
     *  schedule slots.
     */

    @OSID @Override
    public void useSequesteredScheduleSlotView() {
        this.session.useSequesteredScheduleSlotView();
        return;
    }


    /**
     *  All schedule slots are returned including sequestered schedule slots.
     */

    @OSID @Override
    public void useUnsequesteredScheduleSlotView() {
        this.session.useUnsequesteredScheduleSlotView();
        return;
    }


    /**
     *  Gets a schedule slot query. The returned query will not have
     *  an extension query.
     *
     *  @return the schedule slot query 
     */
      
    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuery getScheduleSlotQuery() {
        return (this.session.getScheduleSlotQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  scheduleSlotQuery the schedule slot query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code scheduleSlotQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code scheduleSlotQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByQuery(org.osid.calendaring.ScheduleSlotQuery scheduleSlotQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getScheduleSlotsByQuery(scheduleSlotQuery));
    }
}
