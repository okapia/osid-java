//
// AbstractImmutableItem.java
//
//     Wraps a mutable Item to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Item</code> to hide modifiers. This
 *  wrapper provides an immutized Item from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying item whose state changes are visible.
 */

public abstract class AbstractImmutableItem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.billing.Item {

    private final org.osid.billing.Item item;


    /**
     *  Constructs a new <code>AbstractImmutableItem</code>.
     *
     *  @param item the item to immutablize
     *  @throws org.osid.NullArgumentException <code>item</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableItem(org.osid.billing.Item item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Gets the category <code> Id. </code> 
     *
     *  @return the category <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCategoryId() {
        return (this.item.getCategoryId());
    }


    /**
     *  Gets the item category. 
     *
     *  @return the category 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.billing.Category getCategory()
        throws org.osid.OperationFailedException {

        return (this.item.getCategory());
    }


    /**
     *  Tests if this item relates to s specific general ledger account. 
     *
     *  @return <code> true </code> if this item has an account, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasAccount() {
        return (this.item.hasAccount());
    }


    /**
     *  Gets the account <code> Id. </code> 
     *
     *  @return the account <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAccount() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.item.getAccountId());
    }


    /**
     *  Gets the item account. 
     *
     *  @return the account 
     *  @throws org.osid.IllegalStateException <code> hasAccount() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        return (this.item.getAccount());
    }


    /**
     *  Tests if this item relates to s specific product. 
     *
     *  @return <code> true </code> if this item has a product, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProduct() {
        return (this.item.hasProduct());
    }


    /**
     *  Gets the product <code> Id. </code> 
     *
     *  @return the product <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProductId() {
        return (this.item.getProductId());
    }


    /**
     *  Gets the product. 
     *
     *  @return the product 
     *  @throws org.osid.IllegalStateException <code> hasProduct() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct()
        throws org.osid.OperationFailedException {

        return (this.item.getProduct());
    }


    /**
     *  Gets the amount of this item. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.item.getAmount());
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this item amount is a debit, <code> 
     *          false </code> if it is a credit 
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.item.isDebit());
    }


    /**
     *  Tests if this item is a recurring charge. 
     *
     *  @return <code> true </code> if this item is recurring, <code> false 
     *          </code> if one-time 
     */

    @OSID @Override
    public boolean isRecurring() {
        return (this.item.isRecurring());
    }


    /**
     *  Gets the recurring interval. 
     *
     *  @return the interval 
     *  @throws org.osid.IllegalStateException <code> isRecurring() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRecurringInterval() {
        return (this.item.getRecurringInterval());
    }


    /**
     *  Gets the item record corresponding to the given <code> Item </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> itemRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(itemRecordType) </code> is <code> true </code> . 
     *
     *  @param  itemRecordType the type of item record to retrieve 
     *  @return the item record 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(itemRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        return (this.item.getItemRecord(itemRecordType));
    }
}

