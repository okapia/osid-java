//
// AbstractDirectoryQuery.java
//
//     A template for making a Directory Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for directories.
 */

public abstract class AbstractDirectoryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.filing.DirectoryQuery {

    private final java.util.Collection<org.osid.filing.records.DirectoryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches entry names. Supplying multiple strings behaves like a
     *  boolean <code> AND </code> among the elements each which must
     *  correspond to the <code> stringMatchType. </code> An <code> OR
     *  </code> can be performed with multiple queries.
     *
     *  @param  name name to match 
     *  @param  stringMatchType the string match type 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code> name </code>
     *          not of <code> stringMatchType </code>
     *  @throws org.osid.NullArgumentException <code> name </code> or
     *          <code> stringMatchType </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          supportsStringMatchType(stringMatchType) </code> is
     *          <code> false </code>
     */

    @OSID @Override
    public void matchName(String name, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the name terms. 
     */

    @OSID @Override
    public void clearNameTerms() {
        return;
    }


    /**
     *  Matches an absolute pathname of a directory entry. Supplying multiple 
     *  strings behaves like a boolean <code> AND </code> among the elements 
     *  each which must correspond to the <code> stringMatchType. </code> An 
     *  <code> OR </code> can be performed with multiple queries. 
     *
     *  @param  path path to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPath(String path, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the path terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Tests if a <code> DirectoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a directory query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a directory to match the parent directory. There is 
     *  only one <code> DirectoryQuery </code> per <code> 
     *  DifrectoryEntryQuery. </code> Multiple retrievals return the same 
     *  object. 
     *
     *  @return the directory query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuery getDirectoryQuery() {
        throw new org.osid.UnimplementedException("supportsDirectoryQuery() is false");
    }


    /**
     *  Clears the directory terms. 
     */

    @OSID @Override
    public void clearDirectoryTerms() {
        return;
    }


    /**
     *  Matches aliases only. 
     *
     *  @param  match <code> true </code> to match aliases, <code> false 
     *          </code> to match target files 
     */

    @OSID @Override
    public void matchAliases(boolean match) {
        return;
    }


    /**
     *  Matches a file that has any aliases. 
     *
     *  @param  match <code> true </code> to match any alias, <code> false 
     *          </code> to match objects with no aliases 
     */

    @OSID @Override
    public void matchAnyAliases(boolean match) {
        return;
    }


    /**
     *  Clears the aliases terms. 
     */

    @OSID @Override
    public void clearAliasesTerms() {
        return;
    }


    /**
     *  Matches files whose entries are owned by the given agent id. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOwnerId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the owner <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOwnerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getOwnerQuery() {
        throw new org.osid.UnimplementedException("supportsOwnerQuery() is false");
    }


    /**
     *  Clears the owner terms. 
     */

    @OSID @Override
    public void clearOwnerTerms() {
        return;
    }


    /**
     *  Match directory entries that are created between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedTime(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the created time terms. 
     */

    @OSID @Override
    public void clearCreatedTimeTerms() {
        return;
    }


    /**
     *  Match directory entries that are modified between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchModifiedTime(org.osid.calendaring.DateTime start, 
                                  org.osid.calendaring.DateTime end, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the modified time terms. 
     */

    @OSID @Override
    public void clearModifiedTimeTerms() {
        return;
    }


    /**
     *  Match directory entries that were last accessed between the specified 
     *  time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLastAccessTime(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the last access time terms. 
     */

    @OSID @Override
    public void clearLastAccessTimeTerms() {
        return;
    }


    /**
     *  Matches directories that contain the specified file name. 
     *
     *  @param  name a file name 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchFileName(String name, org.osid.type.Type stringMatchType, 
                              boolean match) {
        return;
    }


    /**
     *  Clears all file name terms. 
     */

    @OSID @Override
    public void clearFileNameTerms() {
        return;
    }


    /**
     *  Tests if a <code> FileQuery </code> is available. 
     *
     *  @return <code> true </code> if a file query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a file contained within the directory. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the directory query 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuery getFileQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsFileQuery() is false");
    }


    /**
     *  Matches directories with any file. 
     *
     *  @param  match <code> true </code> to match directories with any file,, 
     *          <code> false </code> to match directories with no file. 
     */

    @OSID @Override
    public void matchAnyFile(boolean match) {
        return;
    }


    /**
     *  Clears all file terms. 
     */

    @OSID @Override
    public void clearFileTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given directory query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a directory implementing the requested record.
     *
     *  @param directoryRecordType a directory record type
     *  @return the directory query record
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryQueryRecord getDirectoryQueryRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectoryQueryRecord record : this.records) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this directory query. 
     *
     *  @param directoryQueryRecord directory query record
     *  @param directoryRecordType directory record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDirectoryQueryRecord(org.osid.filing.records.DirectoryQueryRecord directoryQueryRecord, 
                                          org.osid.type.Type directoryRecordType) {

        addRecordType(directoryRecordType);
        nullarg(directoryQueryRecord, "directory query record");
        this.records.add(directoryQueryRecord);        
        return;
    }
}
