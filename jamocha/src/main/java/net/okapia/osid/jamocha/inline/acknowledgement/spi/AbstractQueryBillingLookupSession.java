//
// AbstractQueryBillingLookupSession.java
//
//    An inline adapter that maps a BillingLookupSession to
//    a BillingQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BillingLookupSession to
 *  a BillingQuerySession.
 */

public abstract class AbstractQueryBillingLookupSession
    extends net.okapia.osid.jamocha.acknowledgement.spi.AbstractBillingLookupSession
    implements org.osid.acknowledgement.BillingLookupSession {

    private final org.osid.acknowledgement.BillingQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBillingLookupSession.
     *
     *  @param querySession the underlying billing query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBillingLookupSession(org.osid.acknowledgement.BillingQuerySession querySession) {
        nullarg(querySession, "billing query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Billing</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBillings() {
        return (this.session.canSearchBillings());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Billing</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Billing</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Billing</code> and
     *  retained for compatibility.
     *
     *  @param  billingId <code>Id</code> of the
     *          <code>Billing</code>
     *  @return the billing
     *  @throws org.osid.NotFoundException <code>billingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>billingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchId(billingId, true);
        org.osid.acknowledgement.BillingList billings = this.session.getBillingsByQuery(query);
        if (billings.hasNext()) {
            return (billings.getNextBilling());
        } 
        
        throw new org.osid.NotFoundException(billingId + " not found");
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  billings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Billings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  billingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>billingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByIds(org.osid.id.IdList billingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();

        try (org.osid.id.IdList ids = billingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBillingsByQuery(query));
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  billing genus <code>Type</code> which does not include
     *  billings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchGenusType(billingGenusType, true);
        return (this.session.getBillingsByQuery(query));
    }


    /**
     *  Gets a <code>BillingList</code> corresponding to the given
     *  billing genus <code>Type</code> and include any additional
     *  billings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByParentGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchParentGenusType(billingGenusType, true);
        return (this.session.getBillingsByQuery(query));
    }


    /**
     *  Gets a <code>BillingList</code> containing the given
     *  billing record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  billingRecordType a billing record type 
     *  @return the returned <code>Billing</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByRecordType(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchRecordType(billingRecordType, true);
        return (this.session.getBillingsByQuery(query));
    }


    /**
     *  Gets a <code>BillingList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known billings or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  billings that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Billing</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBillingsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Billings</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Billings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.acknowledgement.BillingQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBillingsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.acknowledgement.BillingQuery getQuery() {
        org.osid.acknowledgement.BillingQuery query = this.session.getBillingQuery();
        return (query);
    }
}
