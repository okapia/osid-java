//
// AbstractBusinessQuery.java
//
//     A template for making a Business Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for businesses.
 */

public abstract class AbstractBusinessQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.billing.BusinessQuery {

    private final java.util.Collection<org.osid.billing.records.BusinessQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the customer <code> Id </code> for this query to match customers 
     *  that have a related customer. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        return;
    }


    /**
     *  Clears the customer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CustomerQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Matches businesses that have any customer. 
     *
     *  @param  match <code> true </code> to match customers with any 
     *          customer, <code> false </code> to match customers with no 
     *          customer 
     */

    @OSID @Override
    public void matchAnyCustomer(boolean match) {
        return;
    }


    /**
     *  Clears the customer query terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches businesses that have any item. 
     *
     *  @param  match <code> true </code> to match businesses with any item, 
     *          <code> false </code> to match businesses with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item query terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  categoryId a category <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> categoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCategoryId(org.osid.id.Id categoryId, boolean match) {
        return;
    }


    /**
     *  Clears the category <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCategoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CategoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a category query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCategoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a category. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the category query 
     *  @throws org.osid.UnimplementedException <code> supportsCategoryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CategoryQuery getCategoryQuery() {
        throw new org.osid.UnimplementedException("supportsCategoryQuery() is false");
    }


    /**
     *  Matches businesses that have any category. 
     *
     *  @param  match <code> true </code> to match customers with any 
     *          category, <code> false </code> to match customers with no 
     *          category 
     */

    @OSID @Override
    public void matchAnyCategory(boolean match) {
        return;
    }


    /**
     *  Clears the category query terms. 
     */

    @OSID @Override
    public void clearCategoryTerms() {
        return;
    }


    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  itemId an entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches businesses that have any entry. 
     *
     *  @param  match <code> true </code> to match businesses with any entry, 
     *          <code> false </code> to match businesses with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        return;
    }


    /**
     *  Clears the entry query terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        return;
    }


    /**
     *  Sets the period <code> Id </code> for this query to match catalogs 
     *  containing terms. 
     *
     *  @param  periodId the period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> periodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPeriodId(org.osid.id.Id periodId, boolean match) {
        return;
    }


    /**
     *  Clears the period <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a period query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a period Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the period query 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuery getPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsPeriodQuery() is false");
    }


    /**
     *  Matches businesses that have any period. 
     *
     *  @param  match <code> true </code> to match businesses with any period, 
     *          <code> false </code> to match customers with no period 
     */

    @OSID @Override
    public void matchAnyPeriod(boolean match) {
        return;
    }


    /**
     *  Clears the period query terms. 
     */

    @OSID @Override
    public void clearPeriodTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an ancestor. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBusinessId(org.osid.id.Id businessId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getAncestorBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any business ancestor. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          ancestor, <code> false </code> to match root businesses 
     */

    @OSID @Override
    public void matchAnyAncestorBusiness(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor business query terms. 
     */

    @OSID @Override
    public void clearAncestorBusinessTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match businesses 
     *  that have the specified business as an descendant. 
     *
     *  @param  businessId a business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBusinessId(org.osid.id.Id businessId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant business <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBusinessQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getDescendantBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBusinessQuery() is false");
    }


    /**
     *  Matches businesses with any descendant business. 
     *
     *  @param  match <code> true </code> to match businesses with any 
     *          descendant, <code> false </code> to match leaf businesses 
     */

    @OSID @Override
    public void matchAnyDescendantBusiness(boolean match) {
        return;
    }


    /**
     *  Clears the descendant business query terms. 
     */

    @OSID @Override
    public void clearDescendantBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given business query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a business implementing the requested record.
     *
     *  @param businessRecordType a business record type
     *  @return the business query record
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessQueryRecord getBusinessQueryRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.BusinessQueryRecord record : this.records) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record to this business query. 
     *
     *  @param businessQueryRecord business query record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBusinessQueryRecord(org.osid.billing.records.BusinessQueryRecord businessQueryRecord, 
                                          org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);
        nullarg(businessQueryRecord, "business query record");
        this.records.add(businessQueryRecord);        
        return;
    }
}
