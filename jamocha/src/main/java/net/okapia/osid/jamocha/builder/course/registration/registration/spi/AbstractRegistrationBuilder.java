//
// AbstractRegistration.java
//
//     Defines a Registration builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.registration.spi;


/**
 *  Defines a <code>Registration</code> builder.
 */

public abstract class AbstractRegistrationBuilder<T extends AbstractRegistrationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.registration.registration.RegistrationMiter registration;


    /**
     *  Constructs a new <code>AbstractRegistrationBuilder</code>.
     *
     *  @param registration the registration to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRegistrationBuilder(net.okapia.osid.jamocha.builder.course.registration.registration.RegistrationMiter registration) {
        super(registration);
        this.registration = registration;
        return;
    }


    /**
     *  Builds the registration.
     *
     *  @return the new registration
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.registration.Registration build() {
        (new net.okapia.osid.jamocha.builder.validator.course.registration.registration.RegistrationValidator(getValidations())).validate(this.registration);
        return (new net.okapia.osid.jamocha.builder.course.registration.registration.ImmutableRegistration(this.registration));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the registration miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.registration.registration.RegistrationMiter getMiter() {
        return (this.registration);
    }


    /**
     *  Sets the activity bundle.
     *
     *  @param activityBundle an activity bundle
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundle</code> is <code>null</code>
     */

    public T activityBundle(org.osid.course.registration.ActivityBundle activityBundle) {
        getMiter().setActivityBundle(activityBundle);
        return (self());
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Adds a credit option.
     *
     *  @param credits the number of credits
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T credits(java.math.BigDecimal credits) {
        getMiter().addCredits(credits);
        return (self());
    }


    /**
     *  Setsall  the credit options
     *
     *  @param credits a collection of credits
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T credits(java.util.Collection<java.math.BigDecimal> credits) {
        getMiter().setCredits(credits);
        return (self());
    }


    /**
     *  Sets the grading option.
     *
     *  @param option a grading option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T gradingOption(org.osid.grading.GradeSystem option) {
        getMiter().setGradingOption(option);
        return (self());
    }


    /**
     *  Adds a Registration record.
     *
     *  @param record a registration record
     *  @param recordType the type of registration record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.registration.records.RegistrationRecord record, org.osid.type.Type recordType) {
        getMiter().addRegistrationRecord(record, recordType);
        return (self());
    }
}       


