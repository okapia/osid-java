//
// AbstractCanonicalUnitQuery.java
//
//     A template for making a CanonicalUnit Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for canonical units.
 */

public abstract class AbstractCanonicalUnitQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.offering.CanonicalUnitQuery {

    private final java.util.Collection<org.osid.offering.records.CanonicalUnitQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches canonical units with any title. 
     *
     *  @param  match <code> true </code> to match canonical units with any 
     *          title, <code> false </code> to match canonical units with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Matches a code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches canonical units with any code. 
     *
     *  @param  match <code> true </code> to match canonical units with any 
     *          code, <code> false </code> to match canonical units with no 
     *          code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears all code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Sets the cyclic time period <code> Id </code> for this query. 
     *
     *  @param  cyclicTimePeriodId a cyclic time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchOfferedCyclicTimePeriodId(org.osid.id.Id cyclicTimePeriodId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears all offered cyclic time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferedCyclicTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CyclicTimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic time period query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferedCyclicTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic period query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the cyclic time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferedCyclicTimePeriodQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuery getOfferedCyclicTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsOfferedCyclicTimePeriodQuery() is false");
    }


    /**
     *  Matches canonicals that have any cyclic time period. 
     *
     *  @param  match <code> true </code> to match canonicals with any cyclic 
     *          time period, <code> false </code> to match canonicals with no 
     *          cyclic time periods 
     */

    @OSID @Override
    public void matchAnyOfferedCyclicTimePeriod(boolean match) {
        return;
    }


    /**
     *  Clears all cyclic time period terms. 
     */

    @OSID @Override
    public void clearOfferedCyclicTimePeriodTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultOptionId(org.osid.id.Id gradeSystemId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getResultOptionQuery() {
        throw new org.osid.UnimplementedException("supportsResultOptionQuery() is false");
    }


    /**
     *  Matches canonicals that have any grading option. 
     *
     *  @param  match <code> true </code> to match canonicals with any grading 
     *          option, <code> false </code> to match canonicals with no 
     *          grading options 
     */

    @OSID @Override
    public void matchAnyResultOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearResultOptionTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match canonicals 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches canonicals that have any sponsor. 
     *
     *  @param  match <code> true </code> to match canonicals with any 
     *          sponsor, <code> false </code> to match canonicals with no 
     *          sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match canonical 
     *  units assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given canonical unit query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit implementing the requested record.
     *
     *  @param canonicalUnitRecordType a canonical unit record type
     *  @return the canonical unit query record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitQueryRecord getCanonicalUnitQueryRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitQueryRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit query. 
     *
     *  @param canonicalUnitQueryRecord canonical unit query record
     *  @param canonicalUnitRecordType canonicalUnit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitQueryRecord(org.osid.offering.records.CanonicalUnitQueryRecord canonicalUnitQueryRecord, 
                                          org.osid.type.Type canonicalUnitRecordType) {

        addRecordType(canonicalUnitRecordType);
        nullarg(canonicalUnitQueryRecord, "canonical unit query record");
        this.records.add(canonicalUnitQueryRecord);        
        return;
    }
}
