//
// AbstractProfileItemQueryInspector.java
//
//     A template for making a ProfileItemQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileitem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for profile items.
 */

public abstract class AbstractProfileItemQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.profile.ProfileItemQueryInspector {

    private final java.util.Collection<org.osid.profile.records.ProfileItemQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given profile item query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a profile item implementing the requested record.
     *
     *  @param profileItemRecordType a profile item record type
     *  @return the profile item query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileItemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemQueryInspectorRecord getProfileItemQueryInspectorRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileItemQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileItemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile item query. 
     *
     *  @param profileItemQueryInspectorRecord profile item query inspector
     *         record
     *  @param profileItemRecordType profileItem record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileItemQueryInspectorRecord(org.osid.profile.records.ProfileItemQueryInspectorRecord profileItemQueryInspectorRecord, 
                                                   org.osid.type.Type profileItemRecordType) {

        addRecordType(profileItemRecordType);
        nullarg(profileItemRecordType, "profile item record type");
        this.records.add(profileItemQueryInspectorRecord);        
        return;
    }
}
