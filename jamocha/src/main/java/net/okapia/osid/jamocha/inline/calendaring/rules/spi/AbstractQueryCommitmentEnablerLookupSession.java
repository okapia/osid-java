//
// AbstractQueryCommitmentEnablerLookupSession.java
//
//    An inline adapter that maps a CommitmentEnablerLookupSession to
//    a CommitmentEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CommitmentEnablerLookupSession to
 *  a CommitmentEnablerQuerySession.
 */

public abstract class AbstractQueryCommitmentEnablerLookupSession
    extends net.okapia.osid.jamocha.calendaring.rules.spi.AbstractCommitmentEnablerLookupSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.calendaring.rules.CommitmentEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCommitmentEnablerLookupSession.
     *
     *  @param querySession the underlying commitment enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCommitmentEnablerLookupSession(org.osid.calendaring.rules.CommitmentEnablerQuerySession querySession) {
        nullarg(querySession, "commitment enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>CommitmentEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommitmentEnablers() {
        return (this.session.canSearchCommitmentEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commitment enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active commitment enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCommitmentEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive commitment enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCommitmentEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CommitmentEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CommitmentEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CommitmentEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerId <code>Id</code> of the
     *          <code>CommitmentEnabler</code>
     *  @return the commitment enabler
     *  @throws org.osid.NotFoundException <code>commitmentEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commitmentEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnabler getCommitmentEnabler(org.osid.id.Id commitmentEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchId(commitmentEnablerId, true);
        org.osid.calendaring.rules.CommitmentEnablerList commitmentEnablers = this.session.getCommitmentEnablersByQuery(query);
        if (commitmentEnablers.hasNext()) {
            return (commitmentEnablers.getNextCommitmentEnabler());
        } 
        
        throw new org.osid.NotFoundException(commitmentEnablerId + " not found");
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commitmentEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CommitmentEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByIds(org.osid.id.IdList commitmentEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = commitmentEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCommitmentEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  commitment enabler genus <code>Type</code> which does not include
     *  commitment enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchGenusType(commitmentEnablerGenusType, true);
        return (this.session.getCommitmentEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  commitment enabler genus <code>Type</code> and include any additional
     *  commitment enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerGenusType a commitmentEnabler genus type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByParentGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchParentGenusType(commitmentEnablerGenusType, true);
        return (this.session.getCommitmentEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> containing the given
     *  commitment enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  commitmentEnablerRecordType a commitmentEnabler record type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByRecordType(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchRecordType(commitmentEnablerRecordType, true);
        return (this.session.getCommitmentEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible
     *  through this session.
     *  
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CommitmentEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCommitmentEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>CommitmentEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commitment enablers or an error results. Otherwise, the returned list
     *  may contain only those commitment enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commitment enablers are returned that are currently
     *  active. In any status mode, active and inactive commitment enablers
     *  are returned.
     *
     *  @return a list of <code>CommitmentEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.rules.CommitmentEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCommitmentEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.rules.CommitmentEnablerQuery getQuery() {
        org.osid.calendaring.rules.CommitmentEnablerQuery query = this.session.getCommitmentEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
