//
// InvariantMapItemLookupSession
//
//    Implements an Item lookup service backed by a fixed collection of
//    items.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an Item lookup service backed by a fixed
 *  collection of items. The items are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapItemLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapItemLookupSession
    implements org.osid.assessment.ItemLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapItemLookupSession</code> with no
     *  items.
     *  
     *  @param bank the bank
     *  @throws org.osid.NullArgumnetException {@code bank} is
     *          {@code null}
     */

    public InvariantMapItemLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapItemLookupSession</code> with a single
     *  item.
     *  
     *  @param bank the bank
     *  @param item an single item
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code item} is <code>null</code>
     */

      public InvariantMapItemLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.Item item) {
        this(bank);
        putItem(item);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapItemLookupSession</code> using an array
     *  of items.
     *  
     *  @param bank the bank
     *  @param items an array of items
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code items} is <code>null</code>
     */

      public InvariantMapItemLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.Item[] items) {
        this(bank);
        putItems(items);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapItemLookupSession</code> using a
     *  collection of items.
     *
     *  @param bank the bank
     *  @param items a collection of items
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code items} is <code>null</code>
     */

      public InvariantMapItemLookupSession(org.osid.assessment.Bank bank,
                                               java.util.Collection<? extends org.osid.assessment.Item> items) {
        this(bank);
        putItems(items);
        return;
    }
}
