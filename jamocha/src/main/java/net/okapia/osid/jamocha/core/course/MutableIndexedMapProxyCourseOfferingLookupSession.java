//
// MutableIndexedMapProxyCourseOfferingLookupSession
//
//    Implements a CourseOffering lookup service backed by a collection of
//    courseOfferings indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements a CourseOffering lookup service backed by a collection of
 *  courseOfferings. The course offerings are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some courseOfferings may be compatible
 *  with more types than are indicated through these courseOffering
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of course offerings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCourseOfferingLookupSession} with
     *  no course offering.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCourseOfferingLookupSession} with
     *  a single course offering.
     *
     *  @param courseCatalog the course catalog
     *  @param  courseOffering an course offering
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code courseOffering}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.CourseOffering courseOffering, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCourseOffering(courseOffering);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCourseOfferingLookupSession} using
     *  an array of course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param  courseOfferings an array of course offerings
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code courseOfferings}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.CourseOffering[] courseOfferings, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCourseOfferingLookupSession} using
     *  a collection of course offerings.
     *
     *  @param courseCatalog the course catalog
     *  @param  courseOfferings a collection of course offerings
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code courseOfferings}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCourseOfferingLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putCourseOfferings(courseOfferings);
        return;
    }

    
    /**
     *  Makes a {@code CourseOffering} available in this session.
     *
     *  @param  courseOffering a course offering
     *  @throws org.osid.NullArgumentException {@code courseOffering{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseOffering(org.osid.course.CourseOffering courseOffering) {
        super.putCourseOffering(courseOffering);
        return;
    }


    /**
     *  Makes an array of course offerings available in this session.
     *
     *  @param  courseOfferings an array of course offerings
     *  @throws org.osid.NullArgumentException {@code courseOfferings{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseOfferings(org.osid.course.CourseOffering[] courseOfferings) {
        super.putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Makes collection of course offerings available in this session.
     *
     *  @param  courseOfferings a collection of course offerings
     *  @throws org.osid.NullArgumentException {@code courseOffering{@code 
     *          is {@code null}
     */

    @Override
    public void putCourseOfferings(java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings) {
        super.putCourseOfferings(courseOfferings);
        return;
    }


    /**
     *  Removes a CourseOffering from this session.
     *
     *  @param courseOfferingId the {@code Id} of the course offering
     *  @throws org.osid.NullArgumentException {@code courseOfferingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCourseOffering(org.osid.id.Id courseOfferingId) {
        super.removeCourseOffering(courseOfferingId);
        return;
    }    
}
