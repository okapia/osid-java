//
// InvariantIndexedMapProxyPostLookupSession
//
//    Implements a Post lookup service backed by a fixed
//    collection of posts indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum;


/**
 *  Implements a Post lookup service backed by a fixed
 *  collection of posts. The posts are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some posts may be compatible
 *  with more types than are indicated through these post
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyPostLookupSession
    extends net.okapia.osid.jamocha.core.forum.spi.AbstractIndexedMapPostLookupSession
    implements org.osid.forum.PostLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPostLookupSession}
     *  using an array of posts.
     *
     *  @param forum the forum
     *  @param posts an array of posts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum},
     *          {@code posts} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPostLookupSession(org.osid.forum.Forum forum,
                                                         org.osid.forum.Post[] posts, 
                                                         org.osid.proxy.Proxy proxy) {

        setForum(forum);
        setSessionProxy(proxy);
        putPosts(posts);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyPostLookupSession}
     *  using a collection of posts.
     *
     *  @param forum the forum
     *  @param posts a collection of posts
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum},
     *          {@code posts} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyPostLookupSession(org.osid.forum.Forum forum,
                                                         java.util.Collection<? extends org.osid.forum.Post> posts,
                                                         org.osid.proxy.Proxy proxy) {

        setForum(forum);
        setSessionProxy(proxy);
        putPosts(posts);

        return;
    }
}
