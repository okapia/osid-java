//
// AbstractCourseCatalogQuery.java
//
//     A template for making a CourseCatalog Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.coursecatalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for course catalogs.
 */

public abstract class AbstractCourseCatalogQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.course.CourseCatalogQuery {

    private final java.util.Collection<org.osid.course.records.CourseCatalogQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the course <code> Id </code> for this query to match courses that 
     *  have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        return;
    }


    /**
     *  Clears the course <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches course catalogs that have any course. 
     *
     *  @param  match <code> true </code> to match courses with any course, 
     *          <code> false </code> to match courses with no course 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        return;
    }


    /**
     *  Clears the course query terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        return;
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Matches course catalogs that have any activity unit. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          activity unit, <code> false </code> to match course catalogs 
     *          with no activity units 
     */

    @OSID @Override
    public void matchAnyActivityUnit(boolean match) {
        return;
    }


    /**
     *  Clears the activity unit query terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        return;
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches course catalogs that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any course 
     *          offering, <code> false </code> to match courses with no course 
     *          offering 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        return;
    }


    /**
     *  Clears the course offering query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityUnitId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches course catalogs that have any activity. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          activity, <code> false </code> to match course catalogs with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        return;
    }


    /**
     *  Clears the activity query terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match catalogs 
     *  containing terms. 
     *
     *  @param  termId the term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Matches course catalogs that have any term. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          term, <code> false </code> to match courses with no term 
     */

    @OSID @Override
    public void matchAnyTerm(boolean match) {
        return;
    }


    /**
     *  Clears the term query terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course catalogs that have the specified course catalog as an ancestor. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the ancestor course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCourseCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getAncestorCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCourseCatalogQuery() is false");
    }


    /**
     *  Matches course catalogs with any course catalog ancestor. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          ancestor, <code> false </code> to match root course catalogs 
     */

    @OSID @Override
    public void matchAnyAncestorCourseCatalog(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor course catalog query terms. 
     */

    @OSID @Override
    public void clearAncestorCourseCatalogTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course catalogs that have the specified course catalog as an 
     *  descendant. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears the descendant course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCourseCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getDescendantCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCourseCatalogQuery() is false");
    }


    /**
     *  Matches course catalogs with any descendant course catalog. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          descendant, <code> false </code> to match leaf course catalogs 
     */

    @OSID @Override
    public void matchAnyDescendantCourseCatalog(boolean match) {
        return;
    }


    /**
     *  Clears the descendant course catalog query terms. 
     */

    @OSID @Override
    public void clearDescendantCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given course catalog query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a course catalog implementing the requested record.
     *
     *  @param courseCatalogRecordType a course catalog record type
     *  @return the course catalog query record
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogQueryRecord getCourseCatalogQueryRecord(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseCatalogQueryRecord record : this.records) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course catalog query. 
     *
     *  @param courseCatalogQueryRecord course catalog query record
     *  @param courseCatalogRecordType courseCatalog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCourseCatalogQueryRecord(org.osid.course.records.CourseCatalogQueryRecord courseCatalogQueryRecord, 
                                          org.osid.type.Type courseCatalogRecordType) {

        addRecordType(courseCatalogRecordType);
        nullarg(courseCatalogQueryRecord, "course catalog query record");
        this.records.add(courseCatalogQueryRecord);        
        return;
    }
}
