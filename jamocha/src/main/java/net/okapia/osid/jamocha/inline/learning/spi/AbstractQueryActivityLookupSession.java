//
// AbstractQueryActivityLookupSession.java
//
//    An inline adapter that maps an ActivityLookupSession to
//    an ActivityQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityLookupSession to
 *  an ActivityQuerySession.
 */

public abstract class AbstractQueryActivityLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractActivityLookupSession
    implements org.osid.learning.ActivityLookupSession {

    private final org.osid.learning.ActivityQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityLookupSession.
     *
     *  @param querySession the underlying activity query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityLookupSession(org.osid.learning.ActivityQuerySession querySession) {
        nullarg(querySession, "activity query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>ObjectiveBank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (this.session.canSearchActivities());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    
     
    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchId(activityId, true);
        org.osid.learning.ActivityList activities = this.session.getActivitiesByQuery(query);
        if (activities.hasNext()) {
            return (activities.getNextActivity());
        } 
        
        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchParentGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchRecordType(activityRecordType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the activities for the given objective. In plenary mode,
     *  the returned list contains all of the activities mapped to the
     *  objective <code> Id </code> or an error results if an Id in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible <code> Activities </code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  @param  objectiveId <code> Id </code> of the <code> Objective </code> 
     *  @return list of enrollments 
     *  @throws org.osid.NotFoundException <code> objectiveId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchObjectiveId(objectiveId, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the activities for the given objectives. In plenary mode,
     *  the returned list contains all of the activities specified in
     *  the objective <code> Id </code> list, in the order of the
     *  list, including duplicates, or an error results if a course
     *  offering <code> Id </code> in the supplied list is not found
     *  or inaccessible. Otherwise, inaccessible <code> Activities
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  objectiveIds list of objective <code> Ids </code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an <code> objectiveId
     *          </code> not found
     *  @throws org.osid.NullArgumentException <code> objectiveIdList </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesForObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                query.matchObjectiveId(ids.getNextId(), true);
            }
        }

        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset <code> Id </code> or an error results if an <code> Id
     *  </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Activities
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  assetId <code> Id </code> of an <code> Asset </code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException <code> assetId </code> not found 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAsset(org.osid.id.Id assetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException  {
        
        org.osid.learning.ActivityQuery query = getQuery();
        query.matchAssetId(assetId, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the activities for the given asset. In plenary mode, the
     *  returned list contains all of the activities mapped to the
     *  asset <code> Id </code> or an error results if an <code> Id
     *  </code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code> Activities
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  @param  assetIds <code> Ids </code> of <code> Assets </code> 
     *  @return list of activities 
     *  @throws org.osid.NotFoundException an <code> assetId </code> not found 
     *  @throws org.osid.NullArgumentException <code> assetIdList
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivitiesByAssets(org.osid.id.IdList assetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        try (org.osid.id.IdList ids = assetIds) {
                while (ids.hasNext()) {
                    query.matchAssetId(ids.getNextId(), true);
                }
            }

        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets all <code>Activities</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ActivityQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.learning.ActivityQuery getQuery() {
        org.osid.learning.ActivityQuery query = this.session.getActivityQuery();
        return (query);
    }
}
