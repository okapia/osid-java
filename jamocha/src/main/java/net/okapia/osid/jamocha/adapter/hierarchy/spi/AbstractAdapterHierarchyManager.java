//
// AbstractHierarchyManager.java
//
//     An adapter for a HierarchyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a HierarchyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterHierarchyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.hierarchy.HierarchyManager>
    implements org.osid.hierarchy.HierarchyManager {


    /**
     *  Constructs a new {@code AbstractAdapterHierarchyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterHierarchyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterHierarchyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterHierarchyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. Visible federation allows for 
     *  selecting among multiple hierarchies. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if hierarchy traversal is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyTraversal() {
        return (getAdapteeManager().supportsHierarchyTraversal());
    }


    /**
     *  Tests if hierarchy design is supported. 
     *
     *  @return <code> true </code> if hierarchy design is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyDesign() {
        return (getAdapteeManager().supportsHierarchyDesign());
    }


    /**
     *  Tests if hierarchy sequencing is supported. 
     *
     *  @return <code> true </code> if hierarchy sequencing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchySequencing() {
        return (getAdapteeManager().supportsHierarchySequencing());
    }


    /**
     *  Tests if hierarchy structure notification is supported. 
     *
     *  @return <code> true </code> if hierarchy structure notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyStructureNotification() {
        return (getAdapteeManager().supportsHierarchyStructureNotification());
    }


    /**
     *  Tests if a hierarchy lookup is supported. 
     *
     *  @return <code> true </code> if hierarchy lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyLookup() {
        return (getAdapteeManager().supportsHierarchyLookup());
    }


    /**
     *  Tests if a hierarchy query is supported. 
     *
     *  @return <code> true </code> if hierarchy query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyQuery() {
        return (getAdapteeManager().supportsHierarchyQuery());
    }


    /**
     *  Tests if a hierarchy search is supported. 
     *
     *  @return <code> true </code> if hierarchy search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchySearch() {
        return (getAdapteeManager().supportsHierarchySearch());
    }


    /**
     *  Tests if a hierarchy administration is supported. 
     *
     *  @return <code> true </code> if hierarchy administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyAdmin() {
        return (getAdapteeManager().supportsHierarchyAdmin());
    }


    /**
     *  Tests if hierarchy notification is supported. Messages may be sent 
     *  when hierarchies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if hierarchy notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyNotification() {
        return (getAdapteeManager().supportsHierarchyNotification());
    }


    /**
     *  Gets the supported <code> Hierarchy </code> types. 
     *
     *  @return a list containing the supported <code> Hierarchy </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHierarchyRecordTypes() {
        return (getAdapteeManager().getHierarchyRecordTypes());
    }


    /**
     *  Tests if the given <code> Hierarchy </code> record type is supported. 
     *
     *  @param  hierarchyRecordType a <code> Type </code> indicating a <code> 
     *          Hierarchy </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> hierarchyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHierarchyRecordType(org.osid.type.Type hierarchyRecordType) {
        return (getAdapteeManager().supportsHierarchyRecordType(hierarchyRecordType));
    }


    /**
     *  Gets the supported <code> Hierarchy </code> search record types. 
     *
     *  @return a list containing the supported <code> Hierarchy </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHierarchySearchRecordTypes() {
        return (getAdapteeManager().getHierarchySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Hierarchy </code> search record type is 
     *  supported. 
     *
     *  @param  hierarchySearchRecordType a <code> Type </code> indicating a 
     *          <code> Hierarchy </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHierarchySearchRecordType(org.osid.type.Type hierarchySearchRecordType) {
        return (getAdapteeManager().supportsHierarchySearchRecordType(hierarchySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service. 
     *
     *  @return a <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyTraversalSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  traversal service for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the hierarchy 
     *  @return the new <code> HierarchyTraversalSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyid </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyTraversal() </code> or <code> 
     *          supportsVisibleFedaration() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyTraversalSession getHierarchyTraversalSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyTraversalSessionForHierarchy(hierarchyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyDesignSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the topology 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyDesignSession getHierarchyDesignSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyDesignSessionForHierarchy(hierarchyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  sequencing service. 
     *
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchySequencingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequencing 
     *  design service using for the given hierarchy. 
     *
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchySequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> hierarchyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySequencingSession getHierarchySequencingSessionForHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchySequencingSessionForHierarchy(hierarchyId));
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSession(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyStructureNotificationSession(hierarchyStructureReceiver));
    }


    /**
     *  Gets the session for subscribing to notifications of changes within a 
     *  hierarchy structure for the given hierarchy. 
     *
     *  @param  hierarchyStructureReceiver a receiver 
     *  @param  hierarchyId the <code> Id </code> of the graph 
     *  @return a <code> HierarchyStructureNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> hierarchyId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> 
     *          hierarchyStructureReceiver </code> or <code> hierarchyId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyStructureNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyStructureNotificationSession getHierarchyStructureNotificationSessionForHierarchy(org.osid.hierarchy.HierarchyStructureReceiver hierarchyStructureReceiver, 
                                                                                                                         org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyStructureNotificationSessionForHierarchy(hierarchyStructureReceiver, hierarchyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  lookup service. 
     *
     *  @return a <code> HierarchyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyLookupSession getHierarchyLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  query service. 
     *
     *  @return a <code> HierarchyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQuerySession getHierarchyQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hierarchy 
     *  search service. 
     *
     *  @return a <code> HierarchySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchySearchSession getHierarchySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchySearchSession());
    }


    /**
     *  Gets the hierarchy administrative session. 
     *
     *  @return a <code> HierarchyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyAdminSession getHierarchyAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyAdminSession());
    }


    /**
     *  Gets a hierarchy notification session. 
     *
     *  @param  hierarchyReceiver notification callback 
     *  @return a <code> HierarchyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> hierarchyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyNotificationSession getHierarchyNotificationSession(org.osid.hierarchy.HierarchyReceiver hierarchyReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyNotificationSession(hierarchyReceiver));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
