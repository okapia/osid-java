//
// AbstractAdapterProjectLookupSession.java
//
//    A Project lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.construction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Project lookup session adapter.
 */

public abstract class AbstractAdapterProjectLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.construction.ProjectLookupSession {

    private final org.osid.room.construction.ProjectLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProjectLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProjectLookupSession(org.osid.room.construction.ProjectLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Project} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProjects() {
        return (this.session.canLookupProjects());
    }


    /**
     *  A complete view of the {@code Project} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProjectView() {
        this.session.useComparativeProjectView();
        return;
    }


    /**
     *  A complete view of the {@code Project} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProjectView() {
        this.session.usePlenaryProjectView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include projects in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only projects whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProjectView() {
        this.session.useEffectiveProjectView();
        return;
    }
    

    /**
     *  All projects of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProjectView() {
        this.session.useAnyEffectiveProjectView();
        return;
    }

     
    /**
     *  Gets the {@code Project} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Project} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Project} and
     *  retained for compatibility.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param projectId {@code Id} of the {@code Project}
     *  @return the project
     *  @throws org.osid.NotFoundException {@code projectId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code projectId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Project getProject(org.osid.id.Id projectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProject(projectId));
    }


    /**
     *  Gets a {@code ProjectList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  projects specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Projects} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Project} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code projectIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByIds(org.osid.id.IdList projectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByIds(projectIds));
    }


    /**
     *  Gets a {@code ProjectList} corresponding to the given
     *  project genus {@code Type} which does not include
     *  projects of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned {@code Project} list
     *  @throws org.osid.NullArgumentException
     *          {@code projectGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByGenusType(projectGenusType));
    }


    /**
     *  Gets a {@code ProjectList} corresponding to the given
     *  project genus {@code Type} and include any additional
     *  projects with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @return the returned {@code Project} list
     *  @throws org.osid.NullArgumentException
     *          {@code projectGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByParentGenusType(org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByParentGenusType(projectGenusType));
    }


    /**
     *  Gets a {@code ProjectList} containing the given
     *  project record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectRecordType a project record type 
     *  @return the returned {@code Project} list
     *  @throws org.osid.NullArgumentException
     *          {@code projectRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByRecordType(org.osid.type.Type projectRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByRecordType(projectRecordType));
    }


    /**
     *  Gets a {@code ProjectList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible
     *  through this session.
     *  
     *  In active mode, projects are returned that are currently
     *  active. In any status mode, active and inactive projects
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsOnDate(from, to));
    }
        

    /**
     *  Gets a list of all projects with a genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  projectGenusType a project genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code
     *          projectGenusType, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeOnDate(org.osid.type.Type projectGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByGenusTypeOnDate(projectGenusType, from, to));
    }


    /**
     *  Gets a {@code ProjectList} containing the given building.
     *  
     *  {@code} In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list may
     *  contain only those projects that are accessible through this
     *  session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.NullArgumentException {@code buildingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProjectsForBuilding(buildingId));
    }


    /**
     *  Gets a {@code ProjectList} containing the given building and
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently effective. 
     *  In any effective mode, effective projects and those currently expired 
     *  are returned. 
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  projectGenusType a project genus type 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.NullArgumentException {@code buildingId} or 
     *          {@code projectGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                                    org.osid.type.Type projectGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getProjectsByGenusTypeForBuilding(buildingId, projectGenusType));
    }


    /**
     *  Gets a list of all projects for a building effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a list of all projects for a building with a genus type
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known projects
     *  or an error results. Otherwise, the returned list may contain
     *  only those projects that are accessible through this session.
     *  
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  projectGenusType a project genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Project} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code buildingId,
     *          projectGenusType, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjectsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                          org.osid.type.Type projectGenusType, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjectsByGenusTypeForBuildingOnDate(buildingId, projectGenusType, from, to));
    }


    /**
     *  Gets all {@code Projects}. 
     *
     *  In plenary mode, the returned list contains all known
     *  projects or an error results. Otherwise, the returned list
     *  may contain only those projects that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, projects are returned that are currently
     *  effective.  In any effective mode, effective projects and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Projects} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjects()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProjects());
    }
}
