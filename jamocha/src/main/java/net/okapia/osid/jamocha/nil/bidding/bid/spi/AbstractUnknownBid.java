//
// AbstractUnknownBid.java
//
//     Defines an unknown Bid.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.bidding.bid.spi;


/**
 *  Defines an unknown <code>Bid</code>.
 */

public abstract class AbstractUnknownBid
    extends net.okapia.osid.jamocha.bidding.bid.spi.AbstractBid
    implements org.osid.bidding.Bid {

    protected static final String OBJECT = "osid.bidding.Bid";


    /**
     *  Constructs a new <code>AbstractUnknownBid</code>.
     */

    public AbstractUnknownBid() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));
        
        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setAuction(new net.okapia.osid.jamocha.nil.bidding.auction.UnknownAuction());
        setBidder(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setBiddingAgent(new net.okapia.osid.jamocha.nil.authentication.agent.UnknownAgent());
        setCurrentBid(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setMaximumBid(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setSettlementAmount(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownBid</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownBid(boolean optional) {
        this();
        return;
    }
}
