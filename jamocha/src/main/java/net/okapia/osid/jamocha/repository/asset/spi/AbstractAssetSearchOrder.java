//
// AbstractAssetSearchOdrer.java
//
//     Defines an AssetSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.asset.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssetSearchOrder}.
 */

public abstract class AbstractAssetSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectSearchOrder
    implements org.osid.repository.AssetSearchOrder {

    private final java.util.Collection<org.osid.repository.records.AssetSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by asset title. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by published 
     *  domain. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublicDomain(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by copyright.
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCopyright(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by
     *  copyright registration.
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCopyrightRegistration(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute copies. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeVerbatim(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute alterations. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeAlterations(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by the ability to 
     *  distribute compositions. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistributeCompositions(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by asset source. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a source order interface is available. 
     *
     *  @return <code> true </code> if a source search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the source order. 
     *
     *  @return the resource search order for the source 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by created date. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreatedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for grouping the result set by published 
     *  status. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublished(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by published date. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPublishedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the principal 
     *  credit string. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPrincipalCreditString(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by temporal 
     *  coverage. 
     *
     *  @param  style search order record 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTemporalCoverage(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assetRecordType an asset record type 
     *  @return {@code true} if the assetRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assetRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assetRecordType) {
        for (org.osid.repository.records.AssetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assetRecordType the asset record type 
     *  @return the asset search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assetRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assetRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.repository.records.AssetSearchOrderRecord getAssetSearchOrderRecord(org.osid.type.Type assetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this asset. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assetRecord the asset search odrer record
     *  @param assetRecordType asset record type
     *  @throws org.osid.NullArgumentException
     *          {@code assetRecord} or
     *          {@code assetRecordTypeasset} is
     *          {@code null}
     */
            
    protected void addAssetRecord(org.osid.repository.records.AssetSearchOrderRecord assetSearchOrderRecord, 
                                     org.osid.type.Type assetRecordType) {

        addRecordType(assetRecordType);
        this.records.add(assetSearchOrderRecord);
        
        return;
    }
}
