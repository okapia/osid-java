//
// AbstractPackageQuery.java
//
//     A template for making a Package Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.pkg.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for packages.
 */

public abstract class AbstractPackageQuery    
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQuery
    implements org.osid.installation.PackageQuery {

    private final java.util.Collection<org.osid.installation.records.PackageQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches the version string. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException version type not supported 
     */

    @OSID @Override
    public void matchVersion(org.osid.installation.Version version, 
                             boolean match) {
        return;
    }


    /**
     *  Matches packages with any version. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyVersion(boolean match) {
        return;
    }


    /**
     *  Clears the version query terms. 
     */

    @OSID @Override
    public void clearVersionTerms() {
        return;
    }


    /**
     *  Matches packags with versions including and more recent than the given 
     *  version. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException version type not supported 
     */

    @OSID @Override
    public void matchVersionSince(org.osid.installation.Version version, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the version since query terms. 
     */

    @OSID @Override
    public void clearVersionSinceTerms() {
        return;
    }


    /**
     *  Matches the copyright string. 
     *
     *  @param  copyright copyright string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> copyright </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> copyright </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCopyright(String copyright, 
                               org.osid.type.Type stringMatchType, 
                               boolean match) {
        return;
    }


    /**
     *  Matches packages with any copyright. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCopyright(boolean match) {
        return;
    }


    /**
     *  Clears the copyright query terms. 
     */

    @OSID @Override
    public void clearCopyrightTerms() {
        return;
    }


    /**
     *  Matches packages that require license acknowledgement. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequiresLicenseAcknowledgement(boolean match) {
        return;
    }


    /**
     *  Matches packages that have any acknowledgement value. 
     *
     *  @param  match <code> true </code> to match packages that have any 
     *          acknowledgement value, <code> false </code> for to match 
     *          packages that have no value 
     */

    @OSID @Override
    public void matchAnyRequiresLicenseAcknowledgement(boolean match) {
        return;
    }


    /**
     *  Clears the license acknowledgement query terms. 
     */

    @OSID @Override
    public void clearRequiresLicenseAcknowledgementTerms() {
        return;
    }


    /**
     *  Sets the creator resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreatorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the creator <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCreatorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  creators. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreatorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a creator resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the creator resource query 
     *  @throws org.osid.UnimplementedException <code> supportsCreatorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCreatorQuery() {
        throw new org.osid.UnimplementedException("supportsCreatorQuery() is false");
    }


    /**
     *  Matches packages with any creator. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyCreator(boolean match) {
        return;
    }


    /**
     *  Clears the creator query terms. 
     */

    @OSID @Override
    public void clearCreatorTerms() {
        return;
    }


    /**
     *  Matches the release date between the given times inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is <code> 
     *          less than from </code> 
     */

    @OSID @Override
    public void matchReleaseDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        return;
    }


    /**
     *  Matches packages that have any release date. 
     *
     *  @param  match <code> true </code> to match packages with any release 
     *          date, <code> false </code> to match packages with no release 
     *          date 
     */

    @OSID @Override
    public void matchAnyReleaseDate(boolean match) {
        return;
    }


    /**
     *  Clears the release date query terms. 
     */

    @OSID @Override
    public void clearReleaseDateTerms() {
        return;
    }


    /**
     *  Matches the url string. 
     *
     *  @param  url url string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> url </code> is not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> url </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchURL(String url, org.osid.type.Type stringMatchType, 
                         boolean match) {
        return;
    }


    /**
     *  Sets the package <code> Id </code> to match packages on which a 
     *  package depends. 
     *
     *  @param  packageId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDependencyId(org.osid.id.Id packageId, boolean match) {
        return;
    }


    /**
     *  Clears the dependency <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDependencyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getDependencyQuery() {
        throw new org.osid.UnimplementedException("supportsDependencyQuery() is false");
    }


    /**
     *  Matches packages that have any dependency. 
     *
     *  @param  match <code> true </code> to match packages with any 
     *          dependency, <code> false </code> to match packages with no 
     *          dependencies 
     */

    @OSID @Override
    public void matchAnyDependency(boolean match) {
        return;
    }


    /**
     *  Clears the dependency query terms. 
     */

    @OSID @Override
    public void clearDependencyTerms() {
        return;
    }


    /**
     *  Matches packages that have any url. 
     *
     *  @param  match <code> true </code> to match packages with any url, 
     *          <code> false </code> to match packages with no url 
     */

    @OSID @Override
    public void matchAnyURL(boolean match) {
        return;
    }


    /**
     *  Clears the url query terms. 
     */

    @OSID @Override
    public void clearURLTerms() {
        return;
    }


    /**
     *  Sets the installation <code> Id </code> for this query. 
     *
     *  @param  installationId an installation <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> installationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchInstallationId(org.osid.id.Id installationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the installation <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstallationIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InstallationQuery </code> is available. 
     *
     *  @return <code> true </code> if an installation query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an installation. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the installation query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuery getInstallationQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationQuery() is false");
    }


    /**
     *  Matches any packages that are installed. 
     *
     *  @param  match <code> true </code> to match installed packages, <code> 
     *          false </code> for uninstalled packages 
     */

    @OSID @Override
    public void matchAnyInstallation(boolean match) {
        return;
    }


    /**
     *  Clears the installation query terms. 
     */

    @OSID @Override
    public void clearInstallationTerms() {
        return;
    }


    /**
     *  Sets the package <code> Id </code> to match packages on which other 
     *  packages depend. 
     *
     *  @param  packageId a package <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDependentId(org.osid.id.Id packageId, boolean match) {
        return;
    }


    /**
     *  Clears the dependent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDependentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dependent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getDependentQuery() {
        throw new org.osid.UnimplementedException("supportsDependentQuery() is false");
    }


    /**
     *  Matches packages that have any depenents. 
     *
     *  @param  match <code> true </code> to match packages with any 
     *          dependents, <code> false </code> to match packages with no 
     *          dependents 
     */

    @OSID @Override
    public void matchAnyDependent(boolean match) {
        return;
    }


    /**
     *  Clears the dependent query terms. 
     */

    @OSID @Override
    public void clearDependentTerms() {
        return;
    }


    /**
     *  Sets the package <code> Id </code> to match packages in the version 
     *  chain. 
     *
     *  @param  packageId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> packageId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionedPackageId(org.osid.id.Id packageId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the versioned package <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearVersionedPackageIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PackageQuery </code> is available. 
     *
     *  @return <code> true </code> if a package query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVersionedPackageQuery() {
        return (false);
    }


    /**
     *  Gets the query for a version chain. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the package query 
     *  @throws org.osid.UnimplementedException <code> supportsVersionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuery getVersionedPackageQuery() {
        throw new org.osid.UnimplementedException("supportsVersionedPackageQuery() is false");
    }


    /**
     *  Matches packages that have any versions. 
     *
     *  @param  match <code> true </code> to match packages with any versions, 
     *          <code> false </code> to match packages with no versions 
     */

    @OSID @Override
    public void matchAnyVersionedPackage(boolean match) {
        return;
    }


    /**
     *  Clears the versioned package query terms. 
     */

    @OSID @Override
    public void clearVersionedPackageTerms() {
        return;
    }


    /**
     *  Sets the installation content <code> Id </code> for this query. 
     *
     *  @param  installationContentId the installation content <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> installationContentId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchInstallationContentId(org.osid.id.Id installationContentId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the installation content <code> Id </code> terms. 
      */

    @OSID @Override
    public void clearInstallationContentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InstallationContentQuery </code> is available. 
     *
     *  @return <code> true </code> if an installation content query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationContentQuery() {
        return (false);
    }


    /**
     *  Gets the query for the installation content. Multiple queries can be 
     *  retrieved for a nested <code> OR </code> term. 
     *
     *  @return the installation content query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationContentQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationContentQuery getInstallationContentQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationContentQuery() is false");
    }


    /**
     *  Matches packages with any content. 
     *
     *  @param  match <code> true </code> to match packages with any content, 
     *          <code> false </code> to match packages with no content 
     */

    @OSID @Override
    public void matchAnyInstallationContent(boolean match) {
        return;
    }


    /**
     *  Clears the installation content terms. 
     */

    @OSID @Override
    public void clearInstallationContentTerms() {
        return;
    }


    /**
     *  Sets the depot <code> Id </code> for this query. 
     *
     *  @param  depotId a depot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDepotId(org.osid.id.Id depotId, boolean match) {
        return;
    }


    /**
     *  Clears the depot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDepotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DepotQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a depot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a depot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the depot query 
     *  @throws org.osid.UnimplementedException <code> supportsDepotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuery getDepotQuery() {
        throw new org.osid.UnimplementedException("supportsDepotQuery() is false");
    }


    /**
     *  Clears the depot query terms. 
     */

    @OSID @Override
    public void clearDepotTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given package query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a package implementing the requested record.
     *
     *  @param pkgRecordType a package record type
     *  @return the package query record
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pkgRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.PackageQueryRecord getPackageQueryRecord(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.PackageQueryRecord record : this.records) {
            if (record.implementsRecordType(pkgRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pkgRecordType + " is not supported");
    }


    /**
     *  Adds a record to this package query. 
     *
     *  @param pkgQueryRecord package query record
     *  @param pkgRecordType pkg record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPackageQueryRecord(org.osid.installation.records.PackageQueryRecord pkgQueryRecord, 
                                          org.osid.type.Type pkgRecordType) {

        addRecordType(pkgRecordType);
        nullarg(pkgQueryRecord, "package query record");
        this.records.add(pkgQueryRecord);        
        return;
    }
}
