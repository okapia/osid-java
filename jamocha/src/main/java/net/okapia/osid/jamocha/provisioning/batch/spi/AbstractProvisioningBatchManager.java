//
// AbstractProvisioningBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProvisioningBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.provisioning.batch.ProvisioningBatchManager,
               org.osid.provisioning.batch.ProvisioningBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractProvisioningBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProvisioningBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of provisions is available. 
     *
     *  @return <code> true </code> if a provision bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of queues is available. 
     *
     *  @return <code> true </code> if a queue bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of requests is available. 
     *
     *  @return <code> true </code> if a request bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of requests is available. 
     *
     *  @return <code> true </code> if a request transaction bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of pools is available. 
     *
     *  @return <code> true </code> if a pool bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of provisionables is available. 
     *
     *  @return <code> true </code> if a provisionable bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of brokers is available. 
     *
     *  @return <code> true </code> if a broker bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of distributors is available. 
     *
     *  @return <code> true </code> if a distributor bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service. 
     *
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getProvisionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getProvisionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getProvisionBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionBatchAdminSession getProvisionBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getProvisionBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service. 
     *
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getQueueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getQueueBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getQueueBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.QueueBatchAdminSession getQueueBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getQueueBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service. 
     *
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getRequestBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getRequestBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getRequestBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestBatchAdminSession getRequestBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getRequestBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service. 
     *
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getRequestTransactionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getRequestTransactionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getRequestTransactionBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk request 
     *  transaction administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.RequestTransactionBatchAdminSession getRequestTransactionBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getRequestTransactionBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service. 
     *
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getPoolBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getPoolBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getPoolBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.PoolBatchAdminSession getPoolBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getPoolBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service. 
     *
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getProvisionableBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getProvisionableBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getProvisionableBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  provisionable administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisionableBatchAdminSession getProvisionableBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getProvisionableBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service. 
     *
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getBrokerBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getBrokerBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getBrokerBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.BrokerBatchAdminSession getBrokerBatchAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getBrokerBatchAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  distributor administration service. 
     *
     *  @return a <code> DistributorBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.DistributorBatchAdminSession getDistributorBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchManager.getDistributorBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  distributor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.DistributorBatchAdminSession getDistributorBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.batch.ProvisioningBatchProxyManager.getDistributorBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
