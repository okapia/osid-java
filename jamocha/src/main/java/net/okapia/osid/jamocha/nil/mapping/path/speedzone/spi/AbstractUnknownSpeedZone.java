//
// AbstractUnknownSpeedZone.java
//
//     Defines an unknown SpeedZone.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.mapping.path.speedzone.spi;


/**
 *  Defines an unknown <code>SpeedZone</code>.
 */

public abstract class AbstractUnknownSpeedZone
    extends net.okapia.osid.jamocha.mapping.path.speedzone.spi.AbstractSpeedZone
    implements org.osid.mapping.path.SpeedZone {

    protected static final String OBJECT = "osid.mapping.path.SpeedZone";


    /**
     *  Constructs a new <code>AbstractUnknownSpeedZone</code>.
     */

    public AbstractUnknownSpeedZone() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setPath(new net.okapia.osid.jamocha.nil.mapping.path.path.UnknownPath());
        setStartingCoordinate(net.okapia.osid.primordium.mapping.GeographicCoordinate.unknown());
        setEndingCoordinate(net.okapia.osid.primordium.mapping.GeographicCoordinate.unknown());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownSpeedZone</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownSpeedZone(boolean optional) {
        this();
        setRule(new net.okapia.osid.jamocha.nil.rules.rule.UnknownRule());
        return;
    }
}
