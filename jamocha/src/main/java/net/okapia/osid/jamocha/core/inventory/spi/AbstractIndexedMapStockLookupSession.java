//
// AbstractIndexedMapStockLookupSession.java
//
//    A simple framework for providing a Stock lookup service
//    backed by a fixed collection of stocks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Stock lookup service backed by a
 *  fixed collection of stocks. The stocks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some stocks may be compatible
 *  with more types than are indicated through these stock
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Stocks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStockLookupSession
    extends AbstractMapStockLookupSession
    implements org.osid.inventory.StockLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inventory.Stock> stocksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Stock>());
    private final MultiMap<org.osid.type.Type, org.osid.inventory.Stock> stocksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inventory.Stock>());


    /**
     *  Makes a <code>Stock</code> available in this session.
     *
     *  @param  stock a stock
     *  @throws org.osid.NullArgumentException <code>stock<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStock(org.osid.inventory.Stock stock) {
        super.putStock(stock);

        this.stocksByGenus.put(stock.getGenusType(), stock);
        
        try (org.osid.type.TypeList types = stock.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stocksByRecord.put(types.getNextType(), stock);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a stock from this session.
     *
     *  @param stockId the <code>Id</code> of the stock
     *  @throws org.osid.NullArgumentException <code>stockId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStock(org.osid.id.Id stockId) {
        org.osid.inventory.Stock stock;
        try {
            stock = getStock(stockId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.stocksByGenus.remove(stock.getGenusType());

        try (org.osid.type.TypeList types = stock.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.stocksByRecord.remove(types.getNextType(), stock);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStock(stockId);
        return;
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  stock genus <code>Type</code> which does not include
     *  stocks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known stocks or an error results. Otherwise,
     *  the returned list may contain only those stocks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.stock.ArrayStockList(this.stocksByGenus.get(stockGenusType)));
    }


    /**
     *  Gets a <code>StockList</code> containing the given
     *  stock record <code>Type</code>. In plenary mode, the
     *  returned list contains all known stocks or an error
     *  results. Otherwise, the returned list may contain only those
     *  stocks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  stockRecordType a stock record type 
     *  @return the returned <code>stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByRecordType(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.stock.ArrayStockList(this.stocksByRecord.get(stockRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stocksByGenus.clear();
        this.stocksByRecord.clear();

        super.close();

        return;
    }
}
