//
// AbstractTodoProducerSearch.java
//
//     A template for making a TodoProducer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing todo producer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractTodoProducerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.checklist.mason.TodoProducerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.checklist.mason.TodoProducerSearchOrder todoProducerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of todo producers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  todoProducerIds list of todo producers
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongTodoProducers(org.osid.id.IdList todoProducerIds) {
        while (todoProducerIds.hasNext()) {
            try {
                this.ids.add(todoProducerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongTodoProducers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of todo producer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getTodoProducerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  todoProducerSearchOrder todo producer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>todoProducerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderTodoProducerResults(org.osid.checklist.mason.TodoProducerSearchOrder todoProducerSearchOrder) {
	this.todoProducerSearchOrder = todoProducerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.checklist.mason.TodoProducerSearchOrder getTodoProducerSearchOrder() {
	return (this.todoProducerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given todo producer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a todo producer implementing the requested record.
     *
     *  @param todoProducerSearchRecordType a todo producer search record
     *         type
     *  @return the todo producer search record
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerSearchRecord getTodoProducerSearchRecord(org.osid.type.Type todoProducerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.checklist.mason.records.TodoProducerSearchRecord record : this.records) {
            if (record.implementsRecordType(todoProducerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo producer search. 
     *
     *  @param todoProducerSearchRecord todo producer search record
     *  @param todoProducerSearchRecordType todoProducer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTodoProducerSearchRecord(org.osid.checklist.mason.records.TodoProducerSearchRecord todoProducerSearchRecord, 
                                           org.osid.type.Type todoProducerSearchRecordType) {

        addRecordType(todoProducerSearchRecordType);
        this.records.add(todoProducerSearchRecord);        
        return;
    }
}
