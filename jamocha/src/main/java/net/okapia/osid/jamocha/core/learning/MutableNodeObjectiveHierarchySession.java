//
// MutableNodeObjectiveHierarchySession.java
//
//     Defines an Objective hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Defines an objective hierarchy session for delivering a hierarchy
 *  of objectives using the ObjectiveNode interface.
 */

public final class MutableNodeObjectiveHierarchySession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractNodeObjectiveHierarchySession
    implements org.osid.learning.ObjectiveHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeObjectiveHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeObjectiveHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeObjectiveHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeObjectiveHierarchySession(org.osid.learning.ObjectiveNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getObjective().getDisplayName())
                     .description(root.getObjective().getDescription())
                     .build());

        addRootObjective(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeObjectiveHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeObjectiveHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.learning.ObjectiveNode root) {
        setHierarchy(hierarchy);
        addRootObjective(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeObjectiveHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeObjectiveHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.learning.ObjectiveNode> roots) {
        setHierarchy(hierarchy);
        addRootObjectives(roots);
        return;
    }


    /**
     *  Adds a root objective node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootObjective(org.osid.learning.ObjectiveNode root) {
        super.addRootObjective(root);
        return;
    }


    /**
     *  Adds a collection of root objective nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootObjectives(java.util.Collection<org.osid.learning.ObjectiveNode> roots) {
        super.addRootObjectives(roots);
        return;
    }


    /**
     *  Removes a root objective node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootObjective(org.osid.id.Id rootId) {
        super.removeRootObjective(rootId);
        return;
    }
}
