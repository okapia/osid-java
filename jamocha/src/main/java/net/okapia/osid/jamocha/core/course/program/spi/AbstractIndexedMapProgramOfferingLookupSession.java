//
// AbstractIndexedMapProgramOfferingLookupSession.java
//
//    A simple framework for providing a ProgramOffering lookup service
//    backed by a fixed collection of program offerings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProgramOffering lookup service backed by a
 *  fixed collection of program offerings. The program offerings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some program offerings may be compatible
 *  with more types than are indicated through these program offering
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProgramOfferings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProgramOfferingLookupSession
    extends AbstractMapProgramOfferingLookupSession
    implements org.osid.course.program.ProgramOfferingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.program.ProgramOffering> programOfferingsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.ProgramOffering>());
    private final MultiMap<org.osid.type.Type, org.osid.course.program.ProgramOffering> programOfferingsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.ProgramOffering>());


    /**
     *  Makes a <code>ProgramOffering</code> available in this session.
     *
     *  @param  programOffering a program offering
     *  @throws org.osid.NullArgumentException <code>programOffering<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProgramOffering(org.osid.course.program.ProgramOffering programOffering) {
        super.putProgramOffering(programOffering);

        this.programOfferingsByGenus.put(programOffering.getGenusType(), programOffering);
        
        try (org.osid.type.TypeList types = programOffering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programOfferingsByRecord.put(types.getNextType(), programOffering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a program offering from this session.
     *
     *  @param programOfferingId the <code>Id</code> of the program offering
     *  @throws org.osid.NullArgumentException <code>programOfferingId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProgramOffering(org.osid.id.Id programOfferingId) {
        org.osid.course.program.ProgramOffering programOffering;
        try {
            programOffering = getProgramOffering(programOfferingId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.programOfferingsByGenus.remove(programOffering.getGenusType());

        try (org.osid.type.TypeList types = programOffering.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programOfferingsByRecord.remove(types.getNextType(), programOffering);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProgramOffering(programOfferingId);
        return;
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> corresponding to the given
     *  program offering genus <code>Type</code> which does not include
     *  program offerings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known program offerings or an error results. Otherwise,
     *  the returned list may contain only those program offerings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  programOfferingGenusType a program offering genus type 
     *  @return the returned <code>ProgramOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByGenusType(org.osid.type.Type programOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.programoffering.ArrayProgramOfferingList(this.programOfferingsByGenus.get(programOfferingGenusType)));
    }


    /**
     *  Gets a <code>ProgramOfferingList</code> containing the given
     *  program offering record <code>Type</code>. In plenary mode, the
     *  returned list contains all known program offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  program offerings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  programOfferingRecordType a program offering record type 
     *  @return the returned <code>programOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingList getProgramOfferingsByRecordType(org.osid.type.Type programOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.programoffering.ArrayProgramOfferingList(this.programOfferingsByRecord.get(programOfferingRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programOfferingsByGenus.clear();
        this.programOfferingsByRecord.clear();

        super.close();

        return;
    }
}
