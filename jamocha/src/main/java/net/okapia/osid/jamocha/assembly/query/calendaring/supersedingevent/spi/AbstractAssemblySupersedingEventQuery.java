//
// AbstractAssemblySupersedingEventQuery.java
//
//     A SupersedingEventQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SupersedingEventQuery that stores terms.
 */

public abstract class AbstractAssemblySupersedingEventQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.calendaring.SupersedingEventQuery,
               org.osid.calendaring.SupersedingEventQueryInspector,
               org.osid.calendaring.SupersedingEventSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySupersedingEventQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySupersedingEventQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the event <code> Id </code> for this query for matching attached 
     *  events. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupersededEventId(org.osid.id.Id eventId, boolean match) {
        getAssembler().addIdTerm(getSupersededEventIdColumn(), eventId, match);
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersededEventIdTerms() {
        getAssembler().clearTerms(getSupersededEventIdColumn());
        return;
    }


    /**
     *  Gets the superseded event <code> Id </code> terms. 
     *
     *  @return the superseded event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersededEventIdTerms() {
        return (getAssembler().getIdTerms(getSupersededEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the superseded event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupersededEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupersededEventColumn(), style);
        return;
    }


    /**
     *  Gets the SupersededEventId column name.
     *
     * @return the column name
     */

    protected String getSupersededEventIdColumn() {
        return ("superseded_event_id");
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  attached events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersededEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an attached event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersededEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getSupersededEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersededEventQuery() is false");
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearSupersededEventTerms() {
        getAssembler().clearTerms(getSupersededEventColumn());
        return;
    }


    /**
     *  Gets the superseded event terms. 
     *
     *  @return the superseded event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getSupersededEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersededEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the superseded event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersededEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getSupersededEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSupersededEventSearchOrder() is false");
    }


    /**
     *  Gets the SupersededEvent column name.
     *
     * @return the column name
     */

    protected String getSupersededEventColumn() {
        return ("superseded_event");
    }


    /**
     *  Sets the superseding event <code> Id </code> for this query. 
     *
     *  @param  supersedingEventId a superseding event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> supersedingEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        getAssembler().addIdTerm(getSupersedingEventIdColumn(), supersedingEventId, match);
        return;
    }


    /**
     *  Clears the superseding event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        getAssembler().clearTerms(getSupersedingEventIdColumn());
        return;
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (getAssembler().getIdTerms(getSupersedingEventIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the superseding event. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupersedingEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupersedingEventColumn(), style);
        return;
    }


    /**
     *  Gets the SupersedingEventId column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventIdColumn() {
        return ("superseding_event_id");
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        getAssembler().clearTerms(getSupersedingEventColumn());
        return;
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Tests if an <code> EventSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an event search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for the superseding event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getSupersedingEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventSearchOrder() is false");
    }


    /**
     *  Gets the SupersedingEvent column name.
     *
     * @return the column name
     */

    protected String getSupersedingEventColumn() {
        return ("superseding_event");
    }


    /**
     *  Matches superseding events that supersede within the given dates 
     *  inclusive. 
     *
     *  @param  from start date 
     *  @param  to end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchSupersededDate(org.osid.calendaring.DateTime from, 
                                    org.osid.calendaring.DateTime to, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getSupersededDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a superseding event that has any superseded date. 
     *
     *  @param  match <code> true </code> to match superseding events with any 
     *          superseded date, false to match superseding events with no 
     *          superseded date 
     */

    @OSID @Override
    public void matchAnySupersededDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getSupersededDateColumn(), match);
        return;
    }


    /**
     *  Clears the superseded date terms. 
     */

    @OSID @Override
    public void clearSupersededDateTerms() {
        getAssembler().clearTerms(getSupersededDateColumn());
        return;
    }


    /**
     *  Gets the superseded date range terms. 
     *
     *  @return the superseded date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSupersededDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getSupersededDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the superseded date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupersededDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupersededDateColumn(), style);
        return;
    }


    /**
     *  Gets the SupersededDate column name.
     *
     * @return the column name
     */

    protected String getSupersededDateColumn() {
        return ("superseded_date");
    }


    /**
     *  Matches superseding events that supersede within the denormalized 
     *  event positions inclusive. 
     *
     *  @param  from start position 
     *  @param  to end position 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the absolute value of <code> 
     *          from </code> is greater than <code> to </code> 
     */

    @OSID @Override
    public void matchSupersededEventPosition(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getSupersededEventPositionColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a superseding event that has any superseded position. 
     *
     *  @param  match <code> true </code> to match superseding events with any 
     *          superseded event position, false to match superseding events 
     *          with no superseded event position 
     */

    @OSID @Override
    public void matchAnySupersededEventPosition(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getSupersededEventPositionColumn(), match);
        return;
    }


    /**
     *  Clears the superseded position terms. 
     */

    @OSID @Override
    public void clearSupersededEventPositionTerms() {
        getAssembler().clearTerms(getSupersededEventPositionColumn());
        return;
    }


    /**
     *  Gets the superseded event position terms. 
     *
     *  @return the superseded event position terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getSupersededEventPositionTerms() {
        return (getAssembler().getIntegerRangeTerms(getSupersededEventPositionColumn()));
    }


    /**
     *  Specified a preference for ordering results by the superseded event 
     *  position. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySupersededEventPosition(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSupersededEventPositionColumn(), style);
        return;
    }


    /**
     *  Gets the SupersededEventPosition column name.
     *
     * @return the column name
     */

    protected String getSupersededEventPositionColumn() {
        return ("superseded_event_position");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this supersedingEvent supports the given record
     *  <code>Type</code>.
     *
     *  @param  supersedingEventRecordType a superseding event record type 
     *  @return <code>true</code> if the supersedingEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type supersedingEventRecordType) {
        for (org.osid.calendaring.records.SupersedingEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  supersedingEventRecordType the superseding event record type 
     *  @return the superseding event query record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventQueryRecord getSupersedingEventQueryRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  supersedingEventRecordType the superseding event record type 
     *  @return the superseding event query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventQueryInspectorRecord getSupersedingEventQueryInspectorRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param supersedingEventRecordType the superseding event record type
     *  @return the superseding event search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventSearchOrderRecord getSupersedingEventSearchOrderRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this superseding event. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param supersedingEventQueryRecord the superseding event query record
     *  @param supersedingEventQueryInspectorRecord the superseding event query inspector
     *         record
     *  @param supersedingEventSearchOrderRecord the superseding event search order record
     *  @param supersedingEventRecordType superseding event record type
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventQueryRecord</code>,
     *          <code>supersedingEventQueryInspectorRecord</code>,
     *          <code>supersedingEventSearchOrderRecord</code> or
     *          <code>supersedingEventRecordTypesupersedingEvent</code> is
     *          <code>null</code>
     */
            
    protected void addSupersedingEventRecords(org.osid.calendaring.records.SupersedingEventQueryRecord supersedingEventQueryRecord, 
                                      org.osid.calendaring.records.SupersedingEventQueryInspectorRecord supersedingEventQueryInspectorRecord, 
                                      org.osid.calendaring.records.SupersedingEventSearchOrderRecord supersedingEventSearchOrderRecord, 
                                      org.osid.type.Type supersedingEventRecordType) {

        addRecordType(supersedingEventRecordType);

        nullarg(supersedingEventQueryRecord, "superseding event query record");
        nullarg(supersedingEventQueryInspectorRecord, "superseding event query inspector record");
        nullarg(supersedingEventSearchOrderRecord, "superseding event search odrer record");

        this.queryRecords.add(supersedingEventQueryRecord);
        this.queryInspectorRecords.add(supersedingEventQueryInspectorRecord);
        this.searchOrderRecords.add(supersedingEventSearchOrderRecord);
        
        return;
    }
}
