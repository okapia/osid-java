//
// AbstractAssemblyCanonicalUnitQuery.java
//
//     A CanonicalUnitQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.canonicalunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CanonicalUnitQuery that stores terms.
 */

public abstract class AbstractAssemblyCanonicalUnitQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOperableOsidObjectQuery
    implements org.osid.offering.CanonicalUnitQuery,
               org.osid.offering.CanonicalUnitQueryInspector,
               org.osid.offering.CanonicalUnitSearchOrder {

    private final java.util.Collection<org.osid.offering.records.CanonicalUnitQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.CanonicalUnitQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.records.CanonicalUnitSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCanonicalUnitQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCanonicalUnitQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        getAssembler().addStringTerm(getTitleColumn(), title, stringMatchType, match);
        return;
    }


    /**
     *  Matches canonical units with any title. 
     *
     *  @param  match <code> true </code> to match canonical units with any 
     *          title, <code> false </code> to match canonical units with no 
     *          title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        getAssembler().addStringWildcardTerm(getTitleColumn(), match);
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        getAssembler().clearTerms(getTitleColumn());
        return;
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (getAssembler().getStringTerms(getTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the title. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTitleColumn(), style);
        return;
    }


    /**
     *  Gets the Title column name.
     *
     * @return the column name
     */

    protected String getTitleColumn() {
        return ("title");
    }


    /**
     *  Matches a code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getCodeColumn(), code, stringMatchType, match);
        return;
    }


    /**
     *  Matches canonical units with any code. 
     *
     *  @param  match <code> true </code> to match canonical units with any 
     *          code, <code> false </code> to match canonical units with no 
     *          code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        getAssembler().addStringWildcardTerm(getCodeColumn(), match);
        return;
    }


    /**
     *  Clears all code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        getAssembler().clearTerms(getCodeColumn());
        return;
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (getAssembler().getStringTerms(getCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the code. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCodeColumn(), style);
        return;
    }


    /**
     *  Gets the Code column name.
     *
     * @return the column name
     */

    protected String getCodeColumn() {
        return ("code");
    }


    /**
     *  Sets the cyclic time period <code> Id </code> for this query. 
     *
     *  @param  cyclicTimePeriodId a cyclic time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicTimePeriodId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchOfferedCyclicTimePeriodId(org.osid.id.Id cyclicTimePeriodId, 
                                               boolean match) {
        getAssembler().addIdTerm(getOfferedCyclicTimePeriodIdColumn(), cyclicTimePeriodId, match);
        return;
    }


    /**
     *  Clears all offered cyclic time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOfferedCyclicTimePeriodIdTerms() {
        getAssembler().clearTerms(getOfferedCyclicTimePeriodIdColumn());
        return;
    }


    /**
     *  Gets the offered cyclic time period <code> Id </code> query terms. 
     *
     *  @return the cyclic time period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfferedCyclicTimePeriodIdTerms() {
        return (getAssembler().getIdTerms(getOfferedCyclicTimePeriodIdColumn()));
    }


    /**
     *  Gets the OfferedCyclicTimePeriodId column name.
     *
     * @return the column name
     */

    protected String getOfferedCyclicTimePeriodIdColumn() {
        return ("offered_cyclic_time_period_id");
    }


    /**
     *  Tests if a <code> CyclicTimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic time period query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferedCyclicTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic period query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the cyclic time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferedCyclicTimePeriodQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQuery getOfferedCyclicTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsOfferedCyclicTimePeriodQuery() is false");
    }


    /**
     *  Matches canonicals that have any cyclic time period. 
     *
     *  @param  match <code> true </code> to match canonicals with any cyclic 
     *          time period, <code> false </code> to match canonicals with no 
     *          cyclic time periods 
     */

    @OSID @Override
    public void matchAnyOfferedCyclicTimePeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getOfferedCyclicTimePeriodColumn(), match);
        return;
    }


    /**
     *  Clears all cyclic time period terms. 
     */

    @OSID @Override
    public void clearOfferedCyclicTimePeriodTerms() {
        getAssembler().clearTerms(getOfferedCyclicTimePeriodColumn());
        return;
    }


    /**
     *  Gets the offered cyclic time period query terms. 
     *
     *  @return the cyclic time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector[] getOfferedCyclicTimePeriodTerms() {
        return (new org.osid.calendaring.cycle.CyclicTimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the OfferedCyclicTimePeriod column name.
     *
     * @return the column name
     */

    protected String getOfferedCyclicTimePeriodColumn() {
        return ("offered_cyclic_time_period");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultOptionId(org.osid.id.Id gradeSystemId, 
                                    boolean match) {
        getAssembler().addIdTerm(getResultOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultOptionIdTerms() {
        getAssembler().clearTerms(getResultOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResultOptionIdTerms() {
        return (getAssembler().getIdTerms(getResultOptionIdColumn()));
    }


    /**
     *  Gets the ResultOptionId column name.
     *
     * @return the column name
     */

    protected String getResultOptionIdColumn() {
        return ("result_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getResultOptionQuery() {
        throw new org.osid.UnimplementedException("supportsResultOptionQuery() is false");
    }


    /**
     *  Matches canonicals that have any grading option. 
     *
     *  @param  match <code> true </code> to match canonicals with any grading 
     *          option, <code> false </code> to match canonicals with no 
     *          grading options 
     */

    @OSID @Override
    public void matchAnyResultOption(boolean match) {
        getAssembler().addIdWildcardTerm(getResultOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearResultOptionTerms() {
        getAssembler().clearTerms(getResultOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getResultOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the ResultOption column name.
     *
     * @return the column name
     */

    protected String getResultOptionColumn() {
        return ("result_option");
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match canonicals 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getSponsorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        getAssembler().clearTerms(getSponsorIdColumn());
        return;
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (getAssembler().getIdTerms(getSponsorIdColumn()));
    }


    /**
     *  Gets the SponsorId column name.
     *
     * @return the column name
     */

    protected String getSponsorIdColumn() {
        return ("sponsor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches canonicals that have any sponsor. 
     *
     *  @param  match <code> true </code> to match canonicals with any 
     *          sponsor, <code> false </code> to match canonicals with no 
     *          sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        getAssembler().addIdWildcardTerm(getSponsorColumn(), match);
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        getAssembler().clearTerms(getSponsorColumn());
        return;
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the Sponsor column name.
     *
     * @return the column name
     */

    protected String getSponsorColumn() {
        return ("sponsor");
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match canonical 
     *  units assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the catalogue <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the catalogue terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this canonicalUnit supports the given record
     *  <code>Type</code>.
     *
     *  @param  canonicalUnitRecordType a canonical unit record type 
     *  @return <code>true</code> if the canonicalUnitRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type canonicalUnitRecordType) {
        for (org.osid.offering.records.CanonicalUnitQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  canonicalUnitRecordType the canonical unit record type 
     *  @return the canonical unit query record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitQueryRecord getCanonicalUnitQueryRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  canonicalUnitRecordType the canonical unit record type 
     *  @return the canonical unit query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitQueryInspectorRecord getCanonicalUnitQueryInspectorRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param canonicalUnitRecordType the canonical unit record type
     *  @return the canonical unit search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.CanonicalUnitSearchOrderRecord getCanonicalUnitSearchOrderRecord(org.osid.type.Type canonicalUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.CanonicalUnitSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(canonicalUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this canonical unit. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param canonicalUnitQueryRecord the canonical unit query record
     *  @param canonicalUnitQueryInspectorRecord the canonical unit query inspector
     *         record
     *  @param canonicalUnitSearchOrderRecord the canonical unit search order record
     *  @param canonicalUnitRecordType canonical unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitQueryRecord</code>,
     *          <code>canonicalUnitQueryInspectorRecord</code>,
     *          <code>canonicalUnitSearchOrderRecord</code> or
     *          <code>canonicalUnitRecordTypecanonicalUnit</code> is
     *          <code>null</code>
     */
            
    protected void addCanonicalUnitRecords(org.osid.offering.records.CanonicalUnitQueryRecord canonicalUnitQueryRecord, 
                                      org.osid.offering.records.CanonicalUnitQueryInspectorRecord canonicalUnitQueryInspectorRecord, 
                                      org.osid.offering.records.CanonicalUnitSearchOrderRecord canonicalUnitSearchOrderRecord, 
                                      org.osid.type.Type canonicalUnitRecordType) {

        addRecordType(canonicalUnitRecordType);

        nullarg(canonicalUnitQueryRecord, "canonical unit query record");
        nullarg(canonicalUnitQueryInspectorRecord, "canonical unit query inspector record");
        nullarg(canonicalUnitSearchOrderRecord, "canonical unit search odrer record");

        this.queryRecords.add(canonicalUnitQueryRecord);
        this.queryInspectorRecords.add(canonicalUnitQueryInspectorRecord);
        this.searchOrderRecords.add(canonicalUnitSearchOrderRecord);
        
        return;
    }
}
