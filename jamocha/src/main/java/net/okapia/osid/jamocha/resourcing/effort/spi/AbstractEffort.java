//
// AbstractEffort.java
//
//     Defines an Effort.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.effort.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Effort</code>.
 */

public abstract class AbstractEffort
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.resourcing.Effort {

    private org.osid.resource.Resource resource;
    private org.osid.resourcing.Commission commission;
    private org.osid.calendaring.Duration timeSpent;

    private final java.util.Collection<org.osid.resourcing.records.EffortRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the commission. 
     *
     *  @return the commission <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommissionId() {
        return (this.commission.getId());
    }


    /**
     *  Gets the commission. 
     *
     *  @return the commission 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Commission getCommission()
        throws org.osid.OperationFailedException {

        return (this.commission);
    }


    /**
     *  Sets the commission.
     *
     *  @param commission a commission
     *  @throws org.osid.NullArgumentException
     *          <code>commission</code> is <code>null</code>
     */

    protected void setCommission(org.osid.resourcing.Commission commission) {
        nullarg(commission, "commission");
        this.commission = commission;
        return;
    }


    /**
     *  Gets the time spent on this commission. 
     *
     *  @return the time spent 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeSpent() {
        return (this.timeSpent);
    }


    /**
     *  Sets the time spent.
     *
     *  @param timeSpent a time spent
     *  @throws org.osid.NullArgumentException
     *          <code>timeSpent</code> is <code>null</code>
     */

    protected void setTimeSpent(org.osid.calendaring.Duration timeSpent) {
        nullarg(timeSpent, "time spent");
        this.timeSpent = timeSpent;
        return;
    }


    /**
     *  Tests if this effort supports the given record
     *  <code>Type</code>.
     *
     *  @param  effortRecordType an effort record type 
     *  @return <code>true</code> if the effortRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type effortRecordType) {
        for (org.osid.resourcing.records.EffortRecord record : this.records) {
            if (record.implementsRecordType(effortRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Effort</code>
     *  record <code>Type</code>.
     *
     *  @param  effortRecordType the effort record type 
     *  @return the effort record 
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(effortRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.EffortRecord getEffortRecord(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.EffortRecord record : this.records) {
            if (record.implementsRecordType(effortRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(effortRecordType + " is not supported");
    }


    /**
     *  Adds a record to this effort. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param effortRecord the effort record
     *  @param effortRecordType effort record type
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecord</code> or
     *          <code>effortRecordTypeeffort</code> is
     *          <code>null</code>
     */
            
    protected void addEffortRecord(org.osid.resourcing.records.EffortRecord effortRecord, 
                                   org.osid.type.Type effortRecordType) {
        
        nullarg(effortRecord, "effort record");
        addRecordType(effortRecordType);
        this.records.add(effortRecord);
        
        return;
    }
}
