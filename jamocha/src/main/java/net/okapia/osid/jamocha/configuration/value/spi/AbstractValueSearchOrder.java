//
// AbstractValueSearchOdrer.java
//
//     Defines a ValueSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ValueSearchOrder}.
 */

public abstract class AbstractValueSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectSearchOrder
    implements org.osid.configuration.ValueSearchOrder {

    private final java.util.Collection<org.osid.configuration.records.ValueSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the results by the value priority. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriority(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the results by the value. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByValue(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> ParameterSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a parameter search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the parameter search order. 
     *
     *  @return the parameter search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchOrder getParameterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsParameterSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  valueRecordType a value record type 
     *  @return {@code true} if the valueRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code valueRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type valueRecordType) {
        for (org.osid.configuration.records.ValueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  valueRecordType the value record type 
     *  @return the value search order record
     *  @throws org.osid.NullArgumentException
     *          {@code valueRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(valueRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.configuration.records.ValueSearchOrderRecord getValueSearchOrderRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ValueSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(valueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(valueRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this value. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param valueRecord the value search odrer record
     *  @param valueRecordType value record type
     *  @throws org.osid.NullArgumentException
     *          {@code valueRecord} or
     *          {@code valueRecordTypevalue} is
     *          {@code null}
     */
            
    protected void addValueRecord(org.osid.configuration.records.ValueSearchOrderRecord valueSearchOrderRecord, 
                                     org.osid.type.Type valueRecordType) {

        addRecordType(valueRecordType);
        this.records.add(valueSearchOrderRecord);
        
        return;
    }
}
