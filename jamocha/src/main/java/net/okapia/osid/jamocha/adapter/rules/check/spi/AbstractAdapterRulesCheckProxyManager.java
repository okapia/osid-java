//
// AbstractRulesCheckProxyManager.java
//
//     An adapter for a RulesCheckProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RulesCheckProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRulesCheckProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.rules.check.RulesCheckProxyManager>
    implements org.osid.rules.check.RulesCheckProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterRulesCheckProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRulesCheckProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRulesCheckProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRulesCheckProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an agenda evaluation service is supported for the current 
     *  agent. 
     *
     *  @return <code> true </code> if an evaluation service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEvaluation() {
        return (getAdapteeManager().supportsEvaluation());
    }


    /**
     *  Tests if an agenda processing service is supported . 
     *
     *  @return <code> true </code> if a processing service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessing() {
        return (getAdapteeManager().supportsProcessing());
    }


    /**
     *  Tests if looking up agendas is supported. 
     *
     *  @return <code> true </code> if agenda lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaLookup() {
        return (getAdapteeManager().supportsAgendaLookup());
    }


    /**
     *  Tests if querying agendas is supported. 
     *
     *  @return <code> true </code> if agenda query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaQuery() {
        return (getAdapteeManager().supportsAgendaQuery());
    }


    /**
     *  Tests if searching agendas is supported. 
     *
     *  @return <code> true </code> if agenda search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSearch() {
        return (getAdapteeManager().supportsAgendaSearch());
    }


    /**
     *  Tests if agenda administrative service is supported. 
     *
     *  @return <code> true </code> if agenda administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaAdmin() {
        return (getAdapteeManager().supportsAgendaAdmin());
    }


    /**
     *  Tests if an agenda notification service is supported. 
     *
     *  @return <code> true </code> if agenda notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaNotification() {
        return (getAdapteeManager().supportsAgendaNotification());
    }


    /**
     *  Tests if an agenda engine lookup service is supported. 
     *
     *  @return <code> true </code> if an agenda engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaEngine() {
        return (getAdapteeManager().supportsAgendaEngine());
    }


    /**
     *  Tests if an agenda engine service is supported. 
     *
     *  @return <code> true </code> if agenda to engine assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaEngineAssignment() {
        return (getAdapteeManager().supportsAgendaEngineAssignment());
    }


    /**
     *  Tests if an agenda smart engine lookup service is supported. 
     *
     *  @return <code> true </code> if an agenda smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSmartEngine() {
        return (getAdapteeManager().supportsAgendaSmartEngine());
    }


    /**
     *  Tests if looking up instructions is supported. 
     *
     *  @return <code> true </code> if instruction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionLookup() {
        return (getAdapteeManager().supportsInstructionLookup());
    }


    /**
     *  Tests if querying instructions is supported. 
     *
     *  @return <code> true </code> if instruction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionQuery() {
        return (getAdapteeManager().supportsInstructionQuery());
    }


    /**
     *  Tests if searching instructions is supported. 
     *
     *  @return <code> true </code> if instruction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionSearch() {
        return (getAdapteeManager().supportsInstructionSearch());
    }


    /**
     *  Tests if instruction <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if instruction administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionAdmin() {
        return (getAdapteeManager().supportsInstructionAdmin());
    }


    /**
     *  Tests if an instruction <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if instruction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionNotification() {
        return (getAdapteeManager().supportsInstructionNotification());
    }


    /**
     *  Tests if an instruction engine lookup service is supported. 
     *
     *  @return <code> true </code> if an instruction engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionEngine() {
        return (getAdapteeManager().supportsInstructionEngine());
    }


    /**
     *  Tests if an instruction engine assignment service is supported. 
     *
     *  @return <code> true </code> if an instruction to engine assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionEngineAssignment() {
        return (getAdapteeManager().supportsInstructionEngineAssignment());
    }


    /**
     *  Tests if an instruction smart engine service is supported. 
     *
     *  @return <code> true </code> if an instruction smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionSmartEngine() {
        return (getAdapteeManager().supportsInstructionSmartEngine());
    }


    /**
     *  Tests if looking up checks is supported. 
     *
     *  @return <code> true </code> if check lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckLookup() {
        return (getAdapteeManager().supportsCheckLookup());
    }


    /**
     *  Tests if querying checks is supported. 
     *
     *  @return <code> true </code> if check query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckQuery() {
        return (getAdapteeManager().supportsCheckQuery());
    }


    /**
     *  Tests if searching checks is supported. 
     *
     *  @return <code> true </code> if check search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSearch() {
        return (getAdapteeManager().supportsCheckSearch());
    }


    /**
     *  Tests if check administrative service is supported. 
     *
     *  @return <code> true </code> if check administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckAdmin() {
        return (getAdapteeManager().supportsCheckAdmin());
    }


    /**
     *  Tests if a check notification service is supported. 
     *
     *  @return <code> true </code> if check notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckNotification() {
        return (getAdapteeManager().supportsCheckNotification());
    }


    /**
     *  Tests if a check engine lookup service is supported. 
     *
     *  @return <code> true </code> if a check engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckEngine() {
        return (getAdapteeManager().supportsCheckEngine());
    }


    /**
     *  Tests if a check engine service is supported. 
     *
     *  @return <code> true </code> if check to engine assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckEngineAssignment() {
        return (getAdapteeManager().supportsCheckEngineAssignment());
    }


    /**
     *  Tests if a check smart engine lookup service is supported. 
     *
     *  @return <code> true </code> if a check smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSmartEngine() {
        return (getAdapteeManager().supportsCheckSmartEngine());
    }


    /**
     *  Gets the supported <code> Agenda </code> record types. 
     *
     *  @return a list containing the supported <code> Agenda </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgendaRecordTypes() {
        return (getAdapteeManager().getAgendaRecordTypes());
    }


    /**
     *  Tests if the given <code> Agenda </code> record type is supported. 
     *
     *  @param  agendaRecordType a <code> Type </code> indicating an <code> 
     *          Agenda </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agendaRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgendaRecordType(org.osid.type.Type agendaRecordType) {
        return (getAdapteeManager().supportsAgendaRecordType(agendaRecordType));
    }


    /**
     *  Gets the supported <code> Agenda </code> search record types. 
     *
     *  @return a list containing the supported <code> Agenda </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgendaSearchRecordTypes() {
        return (getAdapteeManager().getAgendaSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Agenda </code> search record type is 
     *  supported. 
     *
     *  @param  agendaSearchRecordType a <code> Type </code> indicating an 
     *          <code> Agenda </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agendaSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgendaSearchRecordType(org.osid.type.Type agendaSearchRecordType) {
        return (getAdapteeManager().supportsAgendaSearchRecordType(agendaSearchRecordType));
    }


    /**
     *  Gets the supported <code> Instruction </code> record types. 
     *
     *  @return a list containing the supported <code> Instruction </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstructionRecordTypes() {
        return (getAdapteeManager().getInstructionRecordTypes());
    }


    /**
     *  Tests if the given <code> Instruction </code> record type is 
     *  supported. 
     *
     *  @param  instructionRecordType a <code> Type </code> indicating an 
     *          <code> Instruction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> instructionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstructionRecordType(org.osid.type.Type instructionRecordType) {
        return (getAdapteeManager().supportsInstructionRecordType(instructionRecordType));
    }


    /**
     *  Gets the supported <code> Instruction </code> search types. 
     *
     *  @return a list containing the supported <code> Instruction </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstructionSearchRecordTypes() {
        return (getAdapteeManager().getInstructionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Instruction </code> search type is 
     *  supported. 
     *
     *  @param  instructionSearchRecordType a <code> Type </code> indicating 
     *          an <code> Instruction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          instructionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstructionSearchRecordType(org.osid.type.Type instructionSearchRecordType) {
        return (getAdapteeManager().supportsInstructionSearchRecordType(instructionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Check </code> record types. 
     *
     *  @return a list containing the supported <code> Check </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckRecordTypes() {
        return (getAdapteeManager().getCheckRecordTypes());
    }


    /**
     *  Tests if the given <code> Check </code> record type is supported. 
     *
     *  @param  checkRecordType a <code> Type </code> indicating a <code> 
     *          Check </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checkRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCheckRecordType(org.osid.type.Type checkRecordType) {
        return (getAdapteeManager().supportsCheckRecordType(checkRecordType));
    }


    /**
     *  Gets the supported <code> Check </code> search record types. 
     *
     *  @return a list containing the supported <code> Check </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckSearchRecordTypes() {
        return (getAdapteeManager().getCheckSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Check </code> search record type is 
     *  supported. 
     *
     *  @param  checkSearchRecordType a <code> Type </code> indicating a 
     *          <code> Check </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checkSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCheckSearchRecordType(org.osid.type.Type checkSearchRecordType) {
        return (getAdapteeManager().supportsCheckSearchRecordType(checkSearchRecordType));
    }


    /**
     *  Gets the supported <code> CheckResult </code> record types. 
     *
     *  @return a list containing the supported <code> CheckResult </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckResultRecordTypes() {
        return (getAdapteeManager().getCheckResultRecordTypes());
    }


    /**
     *  Tests if the given <code> CheckResult </code> record type is 
     *  supported. 
     *
     *  @param  checkResultRecordType a <code> Type </code> indicating a 
     *          <code> CheckResult </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checkResultRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCheckResultRecordType(org.osid.type.Type checkResultRecordType) {
        return (getAdapteeManager().supportsCheckResultRecordType(checkResultRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service to evaluate an agenda. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEvaluationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getEvaluationSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service to run checks. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessingSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaLookupSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaQuerySessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaSearchSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaAdminSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSession(org.osid.rules.check.AgendaReceiver agendaReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaNotificationSession(agendaReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service for the given engine. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver, engineId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSessionForEngine(org.osid.rules.check.AgendaReceiver agendaReceiver, 
                                                                                                org.osid.id.Id engineId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaNotificationSessionForEngine(agendaReceiver, engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup agenda/engine mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineSession getAgendaEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaEngineSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning agendas 
     *  to engines. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineAssignmentSession getAgendaEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaEngineAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage agenda smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSmartEngineSession getAgendaSmartEngineSession(org.osid.id.Id engineId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAgendaSmartEngineSession(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionLookupSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionQuerySessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionSearchSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionAdminSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSession(org.osid.rules.check.InstructionReceiver instructionReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionNotificationSession(instructionReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service for the given engine. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver, 
     *          </code> <code> engineId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSessionForEngine(org.osid.rules.check.InstructionReceiver instructionReceiver, 
                                                                                                          org.osid.id.Id engineId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionNotificationSessionForEngine(instructionReceiver, engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup instruction/engine 
     *  checks. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineSession getInstructionEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionEngineSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning mappings 
     *  to engines. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineAssignmentSession getInstructionEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionEngineAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSmartEngine() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSmartEngineSession getInstructionSmartEngineSession(org.osid.id.Id engineId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInstructionSmartEngineSession(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckLookupSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckQuerySessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckSearchSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckAdminSessionForEngine(engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service. 
     *
     *  @param  checkReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSession(org.osid.rules.check.CheckReceiver checkReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckNotificationSession(checkReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service for the given engine. 
     *
     *  @param  checkReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver, engineId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSessionForEngine(org.osid.rules.check.CheckReceiver checkReceiver, 
                                                                                              org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckNotificationSessionForEngine(checkReceiver, engineId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup check/engine mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineSession getCheckEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckEngineSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to engines. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineAssignmentSession getCheckEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckEngineAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage check smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSmartEngineSession getCheckSmartEngineSession(org.osid.id.Id engineId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCheckSmartEngineSession(engineId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
