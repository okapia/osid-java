//
// AbstractCategory.java
//
//     Defines a Category.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Category</code>.
 */

public abstract class AbstractCategory
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.billing.Category {

    private final java.util.Collection<org.osid.billing.records.CategoryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this category supports the given record
     *  <code>Type</code>.
     *
     *  @param  categoryRecordType a category record type 
     *  @return <code>true</code> if the categoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type categoryRecordType) {
        for (org.osid.billing.records.CategoryRecord record : this.records) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Category</code> record <code>Type</code>.
     *
     *  @param  categoryRecordType the category record type 
     *  @return the category record 
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategoryRecord getCategoryRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategoryRecord record : this.records) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this category. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param categoryRecord the category record
     *  @param categoryRecordType category record type
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecord</code> or
     *          <code>categoryRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addCategoryRecord(org.osid.billing.records.CategoryRecord categoryRecord, 
                                     org.osid.type.Type categoryRecordType) {

        nullarg(categoryRecord, "category record");
        addRecordType(categoryRecordType);
        this.records.add(categoryRecord);
        
        return;
    }
}
