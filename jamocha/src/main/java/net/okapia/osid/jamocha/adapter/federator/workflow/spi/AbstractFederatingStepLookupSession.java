//
// AbstractFederatingStepLookupSession.java
//
//     An abstract federating adapter for a StepLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  StepLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingStepLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.StepLookupSession>
    implements org.osid.workflow.StepLookupSession {

    private boolean parallel = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Constructs a new <code>AbstractFederatingStepLookupSession</code>.
     */

    protected AbstractFederatingStepLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.StepLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>Step</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSteps() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            if (session.canLookupSteps()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Step</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.useComparativeStepView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Step</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.usePlenaryStepView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include steps in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.useFederatedOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.useIsolatedOfficeView();
        }

        return;
    }


    /**
     *  Only active steps are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.useActiveStepView();
        }

        return;
    }


    /**
     *  Active and inactive steps are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepView() {
        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            session.useAnyStatusStepView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Step</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Step</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Step</code> and
     *  retained for compatibility.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  stepId <code>Id</code> of the
     *          <code>Step</code>
     *  @return the step
     *  @throws org.osid.NotFoundException <code>stepId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep(org.osid.id.Id stepId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            try {
                return (session.getStep(stepId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(stepId + " not found");
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  steps specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Steps</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stepIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByIds(org.osid.id.IdList stepIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.step.MutableStepList ret = new net.okapia.osid.jamocha.workflow.step.MutableStepList();

        try (org.osid.id.IdList ids = stepIds) {
            while (ids.hasNext()) {
                ret.addStep(getStep(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  step genus <code>Type</code> which does not include
     *  steps of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsByGenusType(stepGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepList</code> corresponding to the given
     *  step genus <code>Type</code> and include any additional
     *  steps with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stepGenusType a step genus type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByParentGenusType(org.osid.type.Type stepGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsByParentGenusType(stepGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepList</code> containing the given
     *  step record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stepRecordType a step record type 
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByRecordType(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsByRecordType(stepRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known steps or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  steps that are accessible through this session. 
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Step</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of steps by process.
     *
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  processId a process <code>Id /code>
     *  @return the returned <code>Step</code> list
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsForProcess(processId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of steps for which the given state is valid.
     *
     *  In plenary mode, the returned list contains all known steps or
     *  an error results. Otherwise, the returned list may contain
     *  only those steps that are accessible through this session.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps are
     *  returned.
     *
     *  @param  stateId a stateId <code> Id </code>
     *  @return the returned <code> Step </code> list
     *  @throws org.osid.NullArgumentException <code> stateId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.StepList getStepsByState(org.osid.id.Id stateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getStepsByState(stateId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Steps</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  steps or an error results. Otherwise, the returned list
     *  may contain only those steps that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, steps are returned that are currently
     *  active. In any status mode, active and inactive steps
     *  are returned.
     *
     *  @return a list of <code>Steps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getSteps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList ret = getStepList();

        for (org.osid.workflow.StepLookupSession session : getSessions()) {
            ret.addStepList(session.getSteps());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.step.FederatingStepList getStepList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.step.ParallelStepList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.step.CompositeStepList());
        }
    }
}
