//
// MutableIndexedMapRaceProcessorEnablerLookupSession
//
//    Implements a RaceProcessorEnabler lookup service backed by a collection of
//    raceProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceProcessorEnabler lookup service backed by a collection of
 *  race processor enablers. The race processor enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some race processor enablers may be compatible
 *  with more types than are indicated through these race processor enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of race processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapRaceProcessorEnablerLookupSession
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRaceProcessorEnablerLookupSession} with no race processor enablers.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorEnablerLookupSession} with a
     *  single race processor enabler.
     *  
     *  @param polls the polls
     *  @param  raceProcessorEnabler a single raceProcessorEnabler
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnabler} is {@code null}
     */

    public MutableIndexedMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler) {
        this(polls);
        putRaceProcessorEnabler(raceProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorEnablerLookupSession} using an
     *  array of race processor enablers.
     *
     *  @param polls the polls
     *  @param  raceProcessorEnablers an array of race processor enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceProcessorEnabler[] raceProcessorEnablers) {
        this(polls);
        putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceProcessorEnablerLookupSession} using a
     *  collection of race processor enablers.
     *
     *  @param polls the polls
     *  @param  raceProcessorEnablers a collection of race processor enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceProcessorEnablers} is {@code null}
     */

    public MutableIndexedMapRaceProcessorEnablerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablers) {

        this(polls);
        putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }
    

    /**
     *  Makes a {@code RaceProcessorEnabler} available in this session.
     *
     *  @param  raceProcessorEnabler a race processor enabler
     *  @throws org.osid.NullArgumentException {@code raceProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceProcessorEnabler(org.osid.voting.rules.RaceProcessorEnabler raceProcessorEnabler) {
        super.putRaceProcessorEnabler(raceProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of race processor enablers available in this session.
     *
     *  @param  raceProcessorEnablers an array of race processor enablers
     *  @throws org.osid.NullArgumentException {@code raceProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceProcessorEnablers(org.osid.voting.rules.RaceProcessorEnabler[] raceProcessorEnablers) {
        super.putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of race processor enablers available in this session.
     *
     *  @param  raceProcessorEnablers a collection of race processor enablers
     *  @throws org.osid.NullArgumentException {@code raceProcessorEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceProcessorEnablers(java.util.Collection<? extends org.osid.voting.rules.RaceProcessorEnabler> raceProcessorEnablers) {
        super.putRaceProcessorEnablers(raceProcessorEnablers);
        return;
    }


    /**
     *  Removes a RaceProcessorEnabler from this session.
     *
     *  @param raceProcessorEnablerId the {@code Id} of the race processor enabler
     *  @throws org.osid.NullArgumentException {@code raceProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId) {
        super.removeRaceProcessorEnabler(raceProcessorEnablerId);
        return;
    }    
}
