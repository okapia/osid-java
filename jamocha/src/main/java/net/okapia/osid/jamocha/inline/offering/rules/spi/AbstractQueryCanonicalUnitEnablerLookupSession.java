//
// AbstractQueryCanonicalUnitEnablerLookupSession.java
//
//    An inline adapter that maps a CanonicalUnitEnablerLookupSession to
//    a CanonicalUnitEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CanonicalUnitEnablerLookupSession to
 *  a CanonicalUnitEnablerQuerySession.
 */

public abstract class AbstractQueryCanonicalUnitEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractCanonicalUnitEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {

      private boolean activeonly    = false;
      private boolean effectiveonly = false;

    private final org.osid.offering.rules.CanonicalUnitEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCanonicalUnitEnablerLookupSession.
     *
     *  @param querySession the underlying canonical unit enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCanonicalUnitEnablerLookupSession(org.osid.offering.rules.CanonicalUnitEnablerQuerySession querySession) {
        nullarg(querySession, "canonical unit enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform <code>CanonicalUnitEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCanonicalUnitEnablers() {
        return (this.session.canSearchCanonicalUnitEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include canonical unit enablers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active canonical unit enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCanonicalUnitEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive canonical unit enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCanonicalUnitEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CanonicalUnitEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CanonicalUnitEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CanonicalUnitEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerId <code>Id</code> of the
     *          <code>CanonicalUnitEnabler</code>
     *  @return the canonical unit enabler
     *  @throws org.osid.NotFoundException <code>canonicalUnitEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>canonicalUnitEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnabler getCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchId(canonicalUnitEnablerId, true);
        org.osid.offering.rules.CanonicalUnitEnablerList canonicalUnitEnablers = this.session.getCanonicalUnitEnablersByQuery(query);
        if (canonicalUnitEnablers.hasNext()) {
            return (canonicalUnitEnablers.getNextCanonicalUnitEnabler());
        } 
        
        throw new org.osid.NotFoundException(canonicalUnitEnablerId + " not found");
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  canonicalUnitEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CanonicalUnitEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByIds(org.osid.id.IdList canonicalUnitEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = canonicalUnitEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to the given
     *  canonical unit enabler genus <code>Type</code> which does not include
     *  canonical unit enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerGenusType a canonicalUnitEnabler genus type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchGenusType(canonicalUnitEnablerGenusType, true);
        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> corresponding to the given
     *  canonical unit enabler genus <code>Type</code> and include any additional
     *  canonical unit enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerGenusType a canonicalUnitEnabler genus type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByParentGenusType(org.osid.type.Type canonicalUnitEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchParentGenusType(canonicalUnitEnablerGenusType, true);
        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> containing the given
     *  canonical unit enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  canonicalUnitEnablerRecordType a canonicalUnitEnabler record type 
     *  @return the returned <code>CanonicalUnitEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersByRecordType(org.osid.type.Type canonicalUnitEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchRecordType(canonicalUnitEnablerRecordType, true);
        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }


    /**
     *  Gets a <code>CanonicalUnitEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible
     *  through this session.
     *  
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CanonicalUnitEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>CanonicalUnitEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  canonical unit enablers or an error results. Otherwise, the returned list
     *  may contain only those canonical unit enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, canonical unit enablers are returned that are currently
     *  active. In any status mode, active and inactive canonical unit enablers
     *  are returned.
     *
     *  @return a list of <code>CanonicalUnitEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerList getCanonicalUnitEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.offering.rules.CanonicalUnitEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCanonicalUnitEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.offering.rules.CanonicalUnitEnablerQuery getQuery() {
        org.osid.offering.rules.CanonicalUnitEnablerQuery query = this.session.getCanonicalUnitEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
