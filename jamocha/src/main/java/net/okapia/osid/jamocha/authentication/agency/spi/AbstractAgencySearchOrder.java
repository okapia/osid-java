//
// AbstractAgencySearchOdrer.java
//
//     Defines an AgencySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AgencySearchOrder}.
 */

public abstract class AbstractAgencySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogSearchOrder
    implements org.osid.authentication.AgencySearchOrder {

    private final java.util.Collection<org.osid.authentication.records.AgencySearchOrderRecord> records = new java.util.LinkedHashSet<>();



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return {@code true} if the agencyRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code agencyRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type agencyRecordType) {
        for (org.osid.authentication.records.AgencySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  agencyRecordType the agency record type 
     *  @return the agency search order record
     *  @throws org.osid.NullArgumentException
     *          {@code agencyRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(agencyRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.authentication.records.AgencySearchOrderRecord getAgencySearchOrderRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this agency. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param agencyRecord the agency search odrer record
     *  @param agencyRecordType agency record type
     *  @throws org.osid.NullArgumentException
     *          {@code agencyRecord} or
     *          {@code agencyRecordTypeagency} is
     *          {@code null}
     */
            
    protected void addAgencyRecord(org.osid.authentication.records.AgencySearchOrderRecord agencySearchOrderRecord, 
                                     org.osid.type.Type agencyRecordType) {

        addRecordType(agencyRecordType);
        this.records.add(agencySearchOrderRecord);
        
        return;
    }
}
