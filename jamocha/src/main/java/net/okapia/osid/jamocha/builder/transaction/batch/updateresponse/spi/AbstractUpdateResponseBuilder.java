//
// AbstractUpdateResponse.java
//
//     Defines an UpdateResponse builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.spi;


/**
 *  Defines an <code>UpdateResponse</code> builder.
 */

public abstract class AbstractUpdateResponseBuilder<T extends AbstractUpdateResponseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.UpdateResponseMiter updateResponse;


    /**
     *  Constructs a new <code>AbstractUpdateResponseBuilder</code>.
     *
     *  @param updateResponse the update response to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractUpdateResponseBuilder(net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.UpdateResponseMiter updateResponse) {
        this.updateResponse = updateResponse;
        return;
    }


    /**
     *  Builds the update response.
     *
     *  @return the new update response
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>updateResponse</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.transaction.batch.UpdateResponse build() {
        (new net.okapia.osid.jamocha.builder.validator.transaction.batch.updateresponse.UpdateResponseValidator(getValidations())).validate(this.updateResponse);
        return (new net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.ImmutableUpdateResponse(this.updateResponse));
    }


    /**
     *  Gets the update response. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new updateResponse
     */

    @Override
    public net.okapia.osid.jamocha.builder.transaction.batch.updateresponse.UpdateResponseMiter getMiter() {
        return (this.updateResponse);
    }


    /**
     *  Sets the form id.
     *
     *  @param formId a form id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>formId</code> is <code>null</code>
     */

    public T form(org.osid.id.Id formId) {
        getMiter().setFormId(formId);
        return (self());
    }


    /**
     *  Sets the updated id.
     *
     *  @param updatedId an updated id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>updatedId</code> is <code>null</code>
     */

    public T updated(org.osid.id.Id updatedId) {
        getMiter().setUpdatedId(updatedId);
        return (self());
    }


    /**
     *  Sets the error message.
     *
     *  @param message an error message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T errorMessage(org.osid.locale.DisplayText message) {
        getMiter().setErrorMessage(message);
        return (self());
    }


}       


