//
// AbstractTriggerValidator.java
//
//     Validates a Trigger.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.control.trigger.spi;


/**
 *  Validates a Trigger.
 */

public abstract class AbstractTriggerValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractTriggerValidator</code>.
     */

    protected AbstractTriggerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractTriggerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractTriggerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Trigger.
     *
     *  @param trigger a trigger to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>trigger</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.control.Trigger trigger) {
        super.validate(trigger);

        testNestedObject(trigger, "getController");
        if (trigger.turnedOff() && trigger.turnedOn()) {
            throw new org.osid.BadLogicException("can't be on and off at the same time");
        }

        test(trigger.changedExceedsVariableAmount(), "changedExceedsVariableAmount()");
        test(trigger.changedDeceedsVariableAmount(), "changedDeceedsVariableAmount()");

        testConditionalMethod(trigger, "getDiscreetStateId", trigger.changedToDiscreetState(), "changedToDiscreetState()");
        testConditionalMethod(trigger, "getDiscreetState", trigger.changedToDiscreetState(), "changedToDiscreetState()");
        
        if (trigger.changedToDiscreetState()) {
            testNestedObject(trigger, "getDiscreetState");
        }

        testNestedObjects(trigger, "getActionGroupIds", "getActionGroups");
        testNestedObjects(trigger, "getSceneIds", "getScenes");
        testNestedObjects(trigger, "getSettingIds", "getSettings");

        return;
    }
}
