//
// MutableIndexedMapProxyInputEnablerLookupSession
//
//    Implements an InputEnabler lookup service backed by a collection of
//    inputEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements an InputEnabler lookup service backed by a collection of
 *  inputEnablers. The input enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some inputEnablers may be compatible
 *  with more types than are indicated through these inputEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of input enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyInputEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractIndexedMapInputEnablerLookupSession
    implements org.osid.control.rules.InputEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputEnablerLookupSession} with
     *  no input enabler.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputEnablerLookupSession(org.osid.control.System system,
                                                       org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputEnablerLookupSession} with
     *  a single input enabler.
     *
     *  @param system the system
     *  @param  inputEnabler an input enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code inputEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputEnablerLookupSession(org.osid.control.System system,
                                                       org.osid.control.rules.InputEnabler inputEnabler, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putInputEnabler(inputEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputEnablerLookupSession} using
     *  an array of input enablers.
     *
     *  @param system the system
     *  @param  inputEnablers an array of input enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code inputEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputEnablerLookupSession(org.osid.control.System system,
                                                       org.osid.control.rules.InputEnabler[] inputEnablers, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInputEnablerLookupSession} using
     *  a collection of input enablers.
     *
     *  @param system the system
     *  @param  inputEnablers a collection of input enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code inputEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInputEnablerLookupSession(org.osid.control.System system,
                                                       java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(system, proxy);
        putInputEnablers(inputEnablers);
        return;
    }

    
    /**
     *  Makes an {@code InputEnabler} available in this session.
     *
     *  @param  inputEnabler an input enabler
     *  @throws org.osid.NullArgumentException {@code inputEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putInputEnabler(org.osid.control.rules.InputEnabler inputEnabler) {
        super.putInputEnabler(inputEnabler);
        return;
    }


    /**
     *  Makes an array of input enablers available in this session.
     *
     *  @param  inputEnablers an array of input enablers
     *  @throws org.osid.NullArgumentException {@code inputEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putInputEnablers(org.osid.control.rules.InputEnabler[] inputEnablers) {
        super.putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Makes collection of input enablers available in this session.
     *
     *  @param  inputEnablers a collection of input enablers
     *  @throws org.osid.NullArgumentException {@code inputEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putInputEnablers(java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers) {
        super.putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Removes an InputEnabler from this session.
     *
     *  @param inputEnablerId the {@code Id} of the input enabler
     *  @throws org.osid.NullArgumentException {@code inputEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeInputEnabler(org.osid.id.Id inputEnablerId) {
        super.removeInputEnabler(inputEnablerId);
        return;
    }    
}
