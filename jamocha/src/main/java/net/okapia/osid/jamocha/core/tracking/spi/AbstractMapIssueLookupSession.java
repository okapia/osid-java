//
// AbstractMapIssueLookupSession
//
//    A simple framework for providing an Issue lookup service
//    backed by a fixed collection of issues.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Issue lookup service backed by a
 *  fixed collection of issues. The issues are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Issues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapIssueLookupSession
    extends net.okapia.osid.jamocha.tracking.spi.AbstractIssueLookupSession
    implements org.osid.tracking.IssueLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.tracking.Issue> issues = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.tracking.Issue>());


    /**
     *  Makes an <code>Issue</code> available in this session.
     *
     *  @param  issue an issue
     *  @throws org.osid.NullArgumentException <code>issue<code>
     *          is <code>null</code>
     */

    protected void putIssue(org.osid.tracking.Issue issue) {
        this.issues.put(issue.getId(), issue);
        return;
    }


    /**
     *  Makes an array of issues available in this session.
     *
     *  @param  issues an array of issues
     *  @throws org.osid.NullArgumentException <code>issues<code>
     *          is <code>null</code>
     */

    protected void putIssues(org.osid.tracking.Issue[] issues) {
        putIssues(java.util.Arrays.asList(issues));
        return;
    }


    /**
     *  Makes a collection of issues available in this session.
     *
     *  @param  issues a collection of issues
     *  @throws org.osid.NullArgumentException <code>issues<code>
     *          is <code>null</code>
     */

    protected void putIssues(java.util.Collection<? extends org.osid.tracking.Issue> issues) {
        for (org.osid.tracking.Issue issue : issues) {
            this.issues.put(issue.getId(), issue);
        }

        return;
    }


    /**
     *  Removes an Issue from this session.
     *
     *  @param  issueId the <code>Id</code> of the issue
     *  @throws org.osid.NullArgumentException <code>issueId<code> is
     *          <code>null</code>
     */

    protected void removeIssue(org.osid.id.Id issueId) {
        this.issues.remove(issueId);
        return;
    }


    /**
     *  Gets the <code>Issue</code> specified by its <code>Id</code>.
     *
     *  @param  issueId <code>Id</code> of the <code>Issue</code>
     *  @return the issue
     *  @throws org.osid.NotFoundException <code>issueId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>issueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue(org.osid.id.Id issueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.tracking.Issue issue = this.issues.get(issueId);
        if (issue == null) {
            throw new org.osid.NotFoundException("issue not found: " + issueId);
        }

        return (issue);
    }


    /**
     *  Gets all <code>Issues</code>. In plenary mode, the returned
     *  list contains all known issues or an error
     *  results. Otherwise, the returned list may contain only those
     *  issues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Issues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.issue.ArrayIssueList(this.issues.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.issues.clear();
        super.close();
        return;
    }
}
