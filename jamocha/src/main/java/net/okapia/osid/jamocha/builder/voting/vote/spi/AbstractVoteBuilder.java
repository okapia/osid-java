//
// AbstractVote.java
//
//     Defines a Vote builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.vote.spi;


/**
 *  Defines a <code>Vote</code> builder.
 */

public abstract class AbstractVoteBuilder<T extends AbstractVoteBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.voting.vote.VoteMiter vote;


    /**
     *  Constructs a new <code>AbstractVoteBuilder</code>.
     *
     *  @param vote the vote to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractVoteBuilder(net.okapia.osid.jamocha.builder.voting.vote.VoteMiter vote) {
        super(vote);
        this.vote = vote;
        return;
    }


    /**
     *  Builds the vote.
     *
     *  @return the new vote
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.voting.Vote build() {
        (new net.okapia.osid.jamocha.builder.validator.voting.vote.VoteValidator(getValidations())).validate(this.vote);
        return (new net.okapia.osid.jamocha.builder.voting.vote.ImmutableVote(this.vote));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the vote miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.voting.vote.VoteMiter getMiter() {
        return (this.vote);
    }


    /**
     *  Sets the candidate.
     *
     *  @param candidate a candidate
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>candidate</code>
     *          is <code>null</code>
     */

    public T candidate(org.osid.voting.Candidate candidate) {
        getMiter().setCandidate(candidate);
        return (self());
    }


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    public T voter(org.osid.resource.Resource voter) {
        getMiter().setVoter(voter);
        return (self());
    }


    /**
     *  Sets the voting agent.
     *
     *  @param agent a voting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T votingAgent(org.osid.authentication.Agent agent) {
        getMiter().setVotingAgent(agent);
        return (self());
    }


    /**
     *  Sets the votes.
     *
     *  @param vote the number of votes
     *  @return the builder
     */

    public T vote(long vote) {
        getMiter().setVotes(vote);
        return (self());
    }


    /**
     *  Adds a Vote record.
     *
     *  @param record a vote record
     *  @param recordType the type of vote record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.voting.records.VoteRecord record, org.osid.type.Type recordType) {
        getMiter().addVoteRecord(record, recordType);
        return (self());
    }
}       


