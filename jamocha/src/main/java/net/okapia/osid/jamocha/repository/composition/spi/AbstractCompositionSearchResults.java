//
// AbstractCompositionSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCompositionSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.repository.CompositionSearchResults {

    private org.osid.repository.CompositionList compositions;
    private final org.osid.repository.CompositionQueryInspector inspector;
    private final java.util.Collection<org.osid.repository.records.CompositionSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCompositionSearchResults.
     *
     *  @param compositions the result set
     *  @param compositionQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>compositions</code>
     *          or <code>compositionQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCompositionSearchResults(org.osid.repository.CompositionList compositions,
                                            org.osid.repository.CompositionQueryInspector compositionQueryInspector) {
        nullarg(compositions, "compositions");
        nullarg(compositionQueryInspector, "composition query inspectpr");

        this.compositions = compositions;
        this.inspector = compositionQueryInspector;

        return;
    }


    /**
     *  Gets the composition list resulting from a search.
     *
     *  @return a composition list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositions() {
        if (this.compositions == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.repository.CompositionList compositions = this.compositions;
        this.compositions = null;
	return (compositions);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.repository.CompositionQueryInspector getCompositionQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  composition search record <code> Type. </code> This method must
     *  be used to retrieve a composition implementing the requested
     *  record.
     *
     *  @param compositionSearchRecordType a composition search 
     *         record type 
     *  @return the composition search
     *  @throws org.osid.NullArgumentException
     *          <code>compositionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(compositionSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionSearchResultsRecord getCompositionSearchResultsRecord(org.osid.type.Type compositionSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.repository.records.CompositionSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(compositionSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(compositionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record composition search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCompositionRecord(org.osid.repository.records.CompositionSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "composition record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
