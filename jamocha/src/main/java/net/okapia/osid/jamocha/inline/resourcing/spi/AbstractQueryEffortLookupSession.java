//
// AbstractQueryEffortLookupSession.java
//
//    An inline adapter that maps an EffortLookupSession to
//    an EffortQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an EffortLookupSession to
 *  an EffortQuerySession.
 */

public abstract class AbstractQueryEffortLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractEffortLookupSession
    implements org.osid.resourcing.EffortLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.resourcing.EffortQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryEffortLookupSession.
     *
     *  @param querySession the underlying effort query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryEffortLookupSession(org.osid.resourcing.EffortQuerySession querySession) {
        nullarg(querySession, "effort query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Effort</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEfforts() {
        return (this.session.canSearchEfforts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include efforts in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only efforts whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEffortView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All efforts of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEffortView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Effort</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Effort</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Effort</code> and
     *  retained for compatibility.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @param  effortId <code>Id</code> of the
     *          <code>Effort</code>
     *  @return the effort
     *  @throws org.osid.NotFoundException <code>effortId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>effortId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Effort getEffort(org.osid.id.Id effortId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchId(effortId, true);
        org.osid.resourcing.EffortList efforts = this.session.getEffortsByQuery(query);
        if (efforts.hasNext()) {
            return (efforts.getNextEffort());
        } 
        
        throw new org.osid.NotFoundException(effortId + " not found");
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  efforts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Efforts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>effortIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByIds(org.osid.id.IdList effortIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();

        try (org.osid.id.IdList ids = effortIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  effort genus <code>Type</code> which does not include
     *  efforts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchGenusType(effortGenusType, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets an <code>EffortList</code> corresponding to the given
     *  effort genus <code>Type</code> and include any additional
     *  efforts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @param  effortGenusType an effort genus type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByParentGenusType(org.osid.type.Type effortGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchParentGenusType(effortGenusType, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets an <code>EffortList</code> containing the given
     *  effort record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @param  effortRecordType an effort record type 
     *  @return the returned <code>Effort</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>effortRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsByRecordType(org.osid.type.Type effortRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchRecordType(effortRecordType, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets an <code>EffortList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *  
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Effort</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsOnDate(org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getEffortsByQuery(query));
    }
        

    /**
     *  Gets a list of efforts corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to a commission
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>commissionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForCommission(org.osid.id.Id commissionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchCommissionId(commissionId, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to a commission
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  commissionId the <code>Id</code> of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>commissionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForCommissionOnDate(org.osid.id.Id commissionId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchCommissionId(commissionId, true);
        query.matchDate(from, to, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to resource and commission
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  commissionId the <code>Id</code> of the commission
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>commissionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommission(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id commissionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchCommissionId(commissionId, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to resource and
     *  commission <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  commissionId the <code>Id</code> of the commission
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>commissionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndCommissionOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id commissionId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchCommissionId(commissionId, true);
        query.matchDate(from, to, true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets a list of efforts corresponding to a work
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.EffortList getEffortsForWork(org.osid.id.Id workId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        if (query.supportsCommissionQuery()) {
            query.getCommissionQuery().matchWorkId(workId, true);
            return (this.session.getEffortsByQuery(query));
        }

        return (super.getEffortsForWork(workId));
    }


    /**
     *  Gets a list of efforts corresponding to a work
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForWorkOnDate(org.osid.id.Id workId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        if (query.supportsCommissionQuery()) {
            query.getCommissionQuery().matchWorkId(workId, true);
            query.matchDate(from, to, true);
            return (this.session.getEffortsByQuery(query));
        }

        return (super.getEffortsForWorkOnDate(workId, from, to));
    }


    /**
     *  Gets a list of efforts corresponding to resource and work
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are
     *  currently effective.  In any effective mode, effective
     *  efforts and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWork(org.osid.id.Id resourceId,
                                                                             org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.EffortQuery query = getQuery();
        if (query.supportsCommissionQuery()) {
            query.getCommissionQuery().matchWorkId(workId, true);
            query.matchResourceId(resourceId, true);
            return (this.session.getEffortsByQuery(query));
        }

        return (super.getEffortsForResourceAndWork(resourceId, workId));
    }


    /**
     *  Gets a list of efforts corresponding to resource and
     *  work <code>Ids</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible
     *  through this session.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and those
     *  currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  workId the <code>Id</code> of the work
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>EffortList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEffortsForResourceAndWorkOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.id.Id workId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.EffortQuery query = getQuery();
        if (query.supportsCommissionQuery()) {
            query.getCommissionQuery().matchWorkId(workId, true);
            query.matchResourceId(resourceId, true);
            query.matchDate(from, to, true);
            return (this.session.getEffortsByQuery(query));
        }

        return (super.getEffortsForResourceAndWorkOnDate(resourceId, workId, from, to));
    }

    
    /**
     *  Gets all <code>Efforts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  efforts or an error results. Otherwise, the returned list
     *  may contain only those efforts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, efforts are returned that are currently
     *  effective.  In any effective mode, effective efforts and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Efforts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.EffortList getEfforts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.resourcing.EffortQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getEffortsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.EffortQuery getQuery() {
        org.osid.resourcing.EffortQuery query = this.session.getEffortQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
