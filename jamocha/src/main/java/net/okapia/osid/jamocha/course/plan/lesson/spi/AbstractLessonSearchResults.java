//
// AbstractLessonSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractLessonSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.plan.LessonSearchResults {

    private org.osid.course.plan.LessonList lessons;
    private final org.osid.course.plan.LessonQueryInspector inspector;
    private final java.util.Collection<org.osid.course.plan.records.LessonSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractLessonSearchResults.
     *
     *  @param lessons the result set
     *  @param lessonQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>lessons</code>
     *          or <code>lessonQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractLessonSearchResults(org.osid.course.plan.LessonList lessons,
                                            org.osid.course.plan.LessonQueryInspector lessonQueryInspector) {
        nullarg(lessons, "lessons");
        nullarg(lessonQueryInspector, "lesson query inspectpr");

        this.lessons = lessons;
        this.inspector = lessonQueryInspector;

        return;
    }


    /**
     *  Gets the lesson list resulting from a search.
     *
     *  @return a lesson list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.plan.LessonList getLessons() {
        if (this.lessons == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.plan.LessonList lessons = this.lessons;
        this.lessons = null;
	return (lessons);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.plan.LessonQueryInspector getLessonQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  lesson search record <code> Type. </code> This method must
     *  be used to retrieve a lesson implementing the requested
     *  record.
     *
     *  @param lessonSearchRecordType a lesson search 
     *         record type 
     *  @return the lesson search
     *  @throws org.osid.NullArgumentException
     *          <code>lessonSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(lessonSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonSearchResultsRecord getLessonSearchResultsRecord(org.osid.type.Type lessonSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.plan.records.LessonSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(lessonSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(lessonSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record lesson search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addLessonRecord(org.osid.course.plan.records.LessonSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "lesson record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
