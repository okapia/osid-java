//
// AbstractJournalEntryQueryInspector.java
//
//     A template for making a JournalEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for journal entries.
 */

public abstract class AbstractJournalEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.journaling.JournalEntryQueryInspector {

    private final java.util.Collection<org.osid.journaling.records.JournalEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the branch <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBranchIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the branch query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.BranchQueryInspector[] getBranchTerms() {
        return (new org.osid.journaling.BranchQueryInspector[0]);
    }


    /**
     *  Gets the source <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the version <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVersionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the timestamp query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimestampTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the entries since query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getEntriesSinceTerms() {
        return (new org.osid.search.terms.DateTimeTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given journal entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a journal entry implementing the requested record.
     *
     *  @param journalEntryRecordType a journal entry record type
     *  @return the journal entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryQueryInspectorRecord getJournalEntryQueryInspectorRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this journal entry query. 
     *
     *  @param journalEntryQueryInspectorRecord journal entry query inspector
     *         record
     *  @param journalEntryRecordType journalEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJournalEntryQueryInspectorRecord(org.osid.journaling.records.JournalEntryQueryInspectorRecord journalEntryQueryInspectorRecord, 
                                                   org.osid.type.Type journalEntryRecordType) {

        addRecordType(journalEntryRecordType);
        nullarg(journalEntryRecordType, "journal entry record type");
        this.records.add(journalEntryQueryInspectorRecord);        
        return;
    }
}
