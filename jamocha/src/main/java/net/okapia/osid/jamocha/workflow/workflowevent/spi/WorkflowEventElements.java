//
// WorkflowEventElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.workflowevent.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class WorkflowEventElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the WorkflowEventElement Id.
     *
     *  @return the workflow event element Id
     */

    public static org.osid.id.Id getWorkflowEventEntityId() {
        return (makeEntityId("osid.workflow.WorkflowEvent"));
    }


    /**
     *  Gets the Timestamp element Id.
     *
     *  @return the Timestamp element Id
     */

    public static org.osid.id.Id getTimestamp() {
        return (makeElementId("osid.workflow.workflowevent.Timestamp"));
    }


    /**
     *  Gets the ProcessId element Id.
     *
     *  @return the ProcessId element Id
     */

    public static org.osid.id.Id getProcessId() {
        return (makeElementId("osid.workflow.workflowevent.ProcessId"));
    }


    /**
     *  Gets the Process element Id.
     *
     *  @return the Process element Id
     */

    public static org.osid.id.Id getProcess() {
        return (makeElementId("osid.workflow.workflowevent.Process"));
    }


    /**
     *  Gets the WorkerId element Id.
     *
     *  @return the WorkerId element Id
     */

    public static org.osid.id.Id getWorkerId() {
        return (makeElementId("osid.workflow.workflowevent.WorkerId"));
    }


    /**
     *  Gets the Worker element Id.
     *
     *  @return the Worker element Id
     */

    public static org.osid.id.Id getWorker() {
        return (makeElementId("osid.workflow.workflowevent.Worker"));
    }


    /**
     *  Gets the WorkingAgentId element Id.
     *
     *  @return the WorkingAgentId element Id
     */

    public static org.osid.id.Id getWorkingAgentId() {
        return (makeElementId("osid.workflow.workflowevent.WorkingAgentId"));
    }


    /**
     *  Gets the WorkingAgent element Id.
     *
     *  @return the WorkingAgent element Id
     */

    public static org.osid.id.Id getWorkingAgent() {
        return (makeElementId("osid.workflow.workflowevent.WorkingAgent"));
    }


    /**
     *  Gets the WorkId element Id.
     *
     *  @return the WorkId element Id
     */

    public static org.osid.id.Id getWorkId() {
        return (makeElementId("osid.workflow.workflowevent.WorkId"));
    }


    /**
     *  Gets the Work element Id.
     *
     *  @return the Work element Id
     */

    public static org.osid.id.Id getWork() {
        return (makeElementId("osid.workflow.workflowevent.Work"));
    }


    /**
     *  Gets the StepId element Id.
     *
     *  @return the StepId element Id
     */

    public static org.osid.id.Id getStepId() {
        return (makeElementId("osid.workflow.workflowevent.StepId"));
    }


    /**
     *  Gets the Step element Id.
     *
     *  @return the Step element Id
     */

    public static org.osid.id.Id getStep() {
        return (makeElementId("osid.workflow.workflowevent.Step"));
    }
}
