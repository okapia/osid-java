//
// AbstractGraphLookupSession.java
//
//    A starter implementation framework for providing a Graph
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Graph
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getGraphs(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractGraphLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.topology.GraphLookupSession {

    private boolean pedantic = false;
    private org.osid.topology.Graph graph = new net.okapia.osid.jamocha.nil.topology.graph.UnknownGraph();
    


    /**
     *  Tests if this user can perform <code>Graph</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupGraphs() {
        return (true);
    }


    /**
     *  A complete view of the <code>Graph</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGraphView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Graph</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGraphView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Graph</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Graph</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Graph</code> and retained for
     *  compatibility.
     *
     *  @param  graphId <code>Id</code> of the
     *          <code>Graph</code>
     *  @return the graph
     *  @throws org.osid.NotFoundException <code>graphId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>graphId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.Graph getGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.topology.GraphList graphs = getGraphs()) {
            while (graphs.hasNext()) {
                org.osid.topology.Graph graph = graphs.getNextGraph();
                if (graph.getId().equals(graphId)) {
                    return (graph);
                }
            }
        } 

        throw new org.osid.NotFoundException(graphId + " not found");
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the graphs
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Graphs</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getGraphs()</code>.
     *
     *  @param  graphIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>graphIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByIds(org.osid.id.IdList graphIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.topology.Graph> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = graphIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getGraph(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("graph " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.topology.graph.LinkedGraphList(ret));
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given graph
     *  genus <code>Type</code> which does not include graphs of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known graphs
     *  or an error results. Otherwise, the returned list may contain
     *  only those graphs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getGraphs()</code>.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.graph.GraphGenusFilterList(getGraphs(), graphGenusType));
    }


    /**
     *  Gets a <code>GraphList</code> corresponding to the given graph
     *  genus <code>Type</code> and include any additional graphs with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known graphs
     *  or an error results. Otherwise, the returned list may contain
     *  only those graphs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGraphs()</code>.
     *
     *  @param  graphGenusType a graph genus type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByParentGenusType(org.osid.type.Type graphGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getGraphsByGenusType(graphGenusType));
    }


    /**
     *  Gets a <code>GraphList</code> containing the given graph
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known graphs
     *  or an error results. Otherwise, the returned list may contain
     *  only those graphs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getGraphs()</code>.
     *
     *  @param  graphRecordType a graph record type 
     *  @return the returned <code>Graph</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByRecordType(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.topology.graph.GraphRecordFilterList(getGraphs(), graphRecordType));
    }


    /**
     *  Gets a <code>GraphList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known graphs
     *  or an error results. Otherwise, the returned list may contain
     *  only those graphs that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Graph</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.topology.GraphList getGraphsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.topology.graph.GraphProviderFilterList(getGraphs(), resourceId));
    }


    /**
     *  Gets all <code>Graphs</code>.
     *
     *  In plenary mode, the returned list contains all known graphs
     *  or an error results. Otherwise, the returned list may contain
     *  only those graphs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Graphs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.topology.GraphList getGraphs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the graph list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of graphs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.topology.GraphList filterGraphsOnViews(org.osid.topology.GraphList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
