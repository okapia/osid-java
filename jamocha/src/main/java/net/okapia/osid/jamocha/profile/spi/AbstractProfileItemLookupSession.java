//
// AbstractProfileItemLookupSession.java
//
//    A starter implementation framework for providing a ProfileItem
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ProfileItem
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProfileItems(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProfileItemLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.profile.ProfileItemLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();
    

    /**
     *  Gets the <code>Profile/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the <code>Profile</code>.
     *
     *  @param  profile the profile for this session
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProfileItem</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileItems() {
        return (true);
    }


    /**
     *  A complete view of the <code>ProfileItem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileItemView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ProfileItem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileItemView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile items in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>ProfileItem</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileItem</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileItem</code> and
     *  retained for compatibility.
     *
     *  @param  profileItemId <code>Id</code> of the
     *          <code>ProfileItem</code>
     *  @return the profile item
     *  @throws org.osid.NotFoundException <code>profileItemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.profile.ProfileItemList profileItems = getProfileItems()) {
            while (profileItems.hasNext()) {
                org.osid.profile.ProfileItem profileItem = profileItems.getNextProfileItem();
                if (profileItem.getId().equals(profileItemId)) {
                    return (profileItem);
                }
            }
        } 

        throw new org.osid.NotFoundException(profileItemId + " not found");
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileItems specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProfileItems</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProfileItems()</code>.
     *
     *  @param  profileItemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByIds(org.osid.id.IdList profileItemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.profile.ProfileItem> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = profileItemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProfileItem(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("profile item " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.profile.profileitem.LinkedProfileItemList(ret));
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  profile item genus <code>Type</code> which does not include
     *  profile items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProfileItems()</code>.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileitem.ProfileItemGenusFilterList(getProfileItems(), profileItemGenusType));
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  profile item genus <code>Type</code> and include any additional
     *  profile items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileItems()</code>.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByParentGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProfileItemsByGenusType(profileItemGenusType));
    }


    /**
     *  Gets a <code>ProfileItemList</code> containing the given
     *  profile item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProfileItems()</code>.
     *
     *  @param  profileItemRecordType a profileItem record type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByRecordType(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.profile.profileitem.ProfileItemRecordFilterList(getProfileItems(), profileItemRecordType));
    }


    /**
     *  Gets all <code>ProfileItems</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ProfileItems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.profile.ProfileItemList getProfileItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the profile item list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of profile items
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.profile.ProfileItemList filterProfileItemsOnViews(org.osid.profile.ProfileItemList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
