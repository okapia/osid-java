//
// AbstractPerson.java
//
//     Defines a Person builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.person.spi;


/**
 *  Defines a <code>Person</code> builder.
 */

public abstract class AbstractPersonBuilder<T extends AbstractPersonBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.personnel.person.PersonMiter person;


    /**
     *  Constructs a new <code>AbstractPersonBuilder</code>.
     *
     *  @param person the person to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPersonBuilder(net.okapia.osid.jamocha.builder.personnel.person.PersonMiter person) {
        super(person);
        this.person = person;
        return;
    }


    /**
     *  Builds the person.
     *
     *  @return the new person
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.personnel.Person build() {
        (new net.okapia.osid.jamocha.builder.validator.personnel.person.PersonValidator(getValidations())).validate(this.person);
        return (new net.okapia.osid.jamocha.builder.personnel.person.ImmutablePerson(this.person));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the person miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.personnel.person.PersonMiter getMiter() {
        return (this.person);
    }


    /**
     *  Sets the salutation.
     *
     *  @param salutation a salutation
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>salutation</code>
     *          is <code>null</code>
     */

    public T salutation(org.osid.locale.DisplayText salutation) {
        getMiter().setSalutation(salutation);
        return (self());
    }


    /**
     *  Sets the given name.
     *
     *  @param givenName a given name
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>givenName</code>
     *          is <code>null</code>
     */

    public T givenName(org.osid.locale.DisplayText givenName) {
        getMiter().setGivenName(givenName);
        return (self());
    }


    /**
     *  Sets the preferred name.
     *
     *  @param preferredName a preferred name
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>preferredName</code> is <code>null</code>
     */

    public T preferredName(org.osid.locale.DisplayText preferredName) {
        getMiter().setPreferredName(preferredName);
        return (self());
    }


    /**
     *  Adds a forename alias.
     *
     *  @param forenameAlias a forename alias
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAlias</code> is <code>null</code>
     */

    public T forenameAlias(org.osid.locale.DisplayText forenameAlias) {
        getMiter().addForenameAlias(forenameAlias);
        return (self());
    }


    /**
     *  Sets all the forename aliases.
     *
     *  @param forenameAliases a collection of forename aliases
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAliases</code> is <code>null</code>
     */

    public T forenameAliases(java.util.Collection<org.osid.locale.DisplayText> forenameAliases) {
        getMiter().setForenameAliases(forenameAliases);
        return (self());
    }


    /**
     *  Adds a middle name.
     *
     *  @param middleName a middle name
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>middleName</code> is <code>null</code>
     */

    public T middleName(org.osid.locale.DisplayText middleName) {
        getMiter().addMiddleName(middleName);
        return (self());
    }


    /**
     *  Sets all the middle names.
     *
     *  @param middleNames a collection of middle names
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>middleNames</code> is <code>null</code>
     */

    public T middleNames(java.util.Collection<org.osid.locale.DisplayText> middleNames) {
        getMiter().setMiddleNames(middleNames);
        return (self());
    }


    /**
     *  Sets the surname.
     *
     *  @param surname a surname
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>surname</code> is <code>null</code>
     */

    public T surname(org.osid.locale.DisplayText surname) {
        getMiter().setSurname(surname);
        return (self());
    }


    /**
     *  Adds a surname alias.
     *
     *  @param surnameAlias a surname alias
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAlias</code> is <code>null</code>
     */

    public T surnameAlias(org.osid.locale.DisplayText surnameAlias) {
        getMiter().addSurnameAlias(surnameAlias);
        return (self());
    }


    /**
     *  Sets all the the surname aliases.
     *
     *  @param surnameAliases a collection of surname aliases
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAliases</code> is <code>null</code>
     */

    public T surnameAliases(java.util.Collection<org.osid.locale.DisplayText> surnameAliases) {
        getMiter().setSurnameAliases(surnameAliases);
        return (self());
    }


    /**
     *  Sets the generation qualifier.
     *
     *  @param qualifier a generation qualifier
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>qualifier</code>
     *          is <code>null</code>
     */

    public T generationQualifier(org.osid.locale.DisplayText qualifier) {
        getMiter().setGenerationQualifier(qualifier);
        return (self());
    }


    /**
     *  Sets the qualification suffix.
     *
     *  @param suffix a qualification suffix
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>suffix</code> is
     *          <code>null</code>
     */

    public T qualificationSuffix(org.osid.locale.DisplayText suffix) {
        getMiter().setQualificationSuffix(suffix);
        return (self());
    }


    /**
     *  Sets the birth date.
     *
     *  @param date a birth date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T birthDate(org.osid.calendaring.DateTime date) {
        getMiter().setBirthDate(date);
        return (self());
    }


    /**
     *  Sets the death date.
     *
     *  @param date a death date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T deathDate(org.osid.calendaring.DateTime date) {
        getMiter().setDeathDate(date);
        return (self());
    }


    /**
     *  Sets the institutional identifier.
     *
     *  @param identifier an institutional identifier
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>identifier</code>
     *          is <code>null</code>
     */

    public T institutionalIdentifier(String identifier) {
        getMiter().setInstitutionalIdentifier(identifier);
        return (self());
    }


    /**
     *  Adds a Person record.
     *
     *  @param record a person record
     *  @param recordType the type of person record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.personnel.records.PersonRecord record, org.osid.type.Type recordType) {
        getMiter().addPersonRecord(record, recordType);
        return (self());
    }
}       


