//
// InvariantMapAuthorizationEnablerLookupSession
//
//    Implements an AuthorizationEnabler lookup service backed by a fixed collection of
//    authorizationEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.rules;


/**
 *  Implements an AuthorizationEnabler lookup service backed by a fixed
 *  collection of authorization enablers. The authorization enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAuthorizationEnablerLookupSession
    extends net.okapia.osid.jamocha.core.authorization.rules.spi.AbstractMapAuthorizationEnablerLookupSession
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationEnablerLookupSession</code> with no
     *  authorization enablers.
     *  
     *  @param vault the vault
     *  @throws org.osid.NullArgumnetException {@code vault} is
     *          {@code null}
     */

    public InvariantMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault) {
        setVault(vault);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationEnablerLookupSession</code> with a single
     *  authorization enabler.
     *  
     *  @param vault the vault
     *  @param authorizationEnabler an single authorization enabler
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorizationEnabler} is <code>null</code>
     */

      public InvariantMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler) {
        this(vault);
        putAuthorizationEnabler(authorizationEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationEnablerLookupSession</code> using an array
     *  of authorization enablers.
     *  
     *  @param vault the vault
     *  @param authorizationEnablers an array of authorization enablers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorizationEnablers} is <code>null</code>
     */

      public InvariantMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault,
                                               org.osid.authorization.rules.AuthorizationEnabler[] authorizationEnablers) {
        this(vault);
        putAuthorizationEnablers(authorizationEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAuthorizationEnablerLookupSession</code> using a
     *  collection of authorization enablers.
     *
     *  @param vault the vault
     *  @param authorizationEnablers a collection of authorization enablers
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code authorizationEnablers} is <code>null</code>
     */

      public InvariantMapAuthorizationEnablerLookupSession(org.osid.authorization.Vault vault,
                                               java.util.Collection<? extends org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablers) {
        this(vault);
        putAuthorizationEnablers(authorizationEnablers);
        return;
    }
}
