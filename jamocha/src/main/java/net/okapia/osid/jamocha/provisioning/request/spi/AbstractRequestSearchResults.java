//
// AbstractRequestSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRequestSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.RequestSearchResults {

    private org.osid.provisioning.RequestList requests;
    private final org.osid.provisioning.RequestQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.RequestSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRequestSearchResults.
     *
     *  @param requests the result set
     *  @param requestQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>requests</code>
     *          or <code>requestQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRequestSearchResults(org.osid.provisioning.RequestList requests,
                                            org.osid.provisioning.RequestQueryInspector requestQueryInspector) {
        nullarg(requests, "requests");
        nullarg(requestQueryInspector, "request query inspectpr");

        this.requests = requests;
        this.inspector = requestQueryInspector;

        return;
    }


    /**
     *  Gets the request list resulting from a search.
     *
     *  @return a request list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests() {
        if (this.requests == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.RequestList requests = this.requests;
        this.requests = null;
	return (requests);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.RequestQueryInspector getRequestQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  request search record <code> Type. </code> This method must
     *  be used to retrieve a request implementing the requested
     *  record.
     *
     *  @param requestSearchRecordType a request search 
     *         record type 
     *  @return the request search
     *  @throws org.osid.NullArgumentException
     *          <code>requestSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(requestSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestSearchResultsRecord getRequestSearchResultsRecord(org.osid.type.Type requestSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.RequestSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(requestSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(requestSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record request search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRequestRecord(org.osid.provisioning.records.RequestSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "request record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
