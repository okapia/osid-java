//
// AbstractAuctionHouseQueryInspector.java
//
//     A template for making an AuctionHouseQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auctionhouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for auction houses.
 */

public abstract class AbstractAuctionHouseQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.bidding.AuctionHouseQueryInspector {

    private final java.util.Collection<org.osid.bidding.records.AuctionHouseQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Gets the bid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBidIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.BidQueryInspector[] getBidTerms() {
        return (new org.osid.bidding.BidQueryInspector[0]);
    }


    /**
     *  Gets the ancestor auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAuctionHouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAncestorAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the descendant auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAuctionHouseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getDescendantAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given auction house query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an auction house implementing the requested record.
     *
     *  @param auctionHouseRecordType an auction house record type
     *  @return the auction house query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionHouseRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionHouseRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionHouseQueryInspectorRecord getAuctionHouseQueryInspectorRecord(org.osid.type.Type auctionHouseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionHouseQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auctionHouseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionHouseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction house query. 
     *
     *  @param auctionHouseQueryInspectorRecord auction house query inspector
     *         record
     *  @param auctionHouseRecordType auctionHouse record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionHouseQueryInspectorRecord(org.osid.bidding.records.AuctionHouseQueryInspectorRecord auctionHouseQueryInspectorRecord, 
                                                   org.osid.type.Type auctionHouseRecordType) {

        addRecordType(auctionHouseRecordType);
        nullarg(auctionHouseRecordType, "auction house record type");
        this.records.add(auctionHouseQueryInspectorRecord);        
        return;
    }
}
