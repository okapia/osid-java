//
// AbstractIndexedMapBudgetLookupSession.java
//
//    A simple framework for providing a Budget lookup service
//    backed by a fixed collection of budgets with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Budget lookup service backed by a
 *  fixed collection of budgets. The budgets are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some budgets may be compatible
 *  with more types than are indicated through these budget
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Budgets</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBudgetLookupSession
    extends AbstractMapBudgetLookupSession
    implements org.osid.financials.budgeting.BudgetLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.budgeting.Budget> budgetsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.budgeting.Budget>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.budgeting.Budget> budgetsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.budgeting.Budget>());


    /**
     *  Makes a <code>Budget</code> available in this session.
     *
     *  @param  budget a budget
     *  @throws org.osid.NullArgumentException <code>budget<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBudget(org.osid.financials.budgeting.Budget budget) {
        super.putBudget(budget);

        this.budgetsByGenus.put(budget.getGenusType(), budget);
        
        try (org.osid.type.TypeList types = budget.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.budgetsByRecord.put(types.getNextType(), budget);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a budget from this session.
     *
     *  @param budgetId the <code>Id</code> of the budget
     *  @throws org.osid.NullArgumentException <code>budgetId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBudget(org.osid.id.Id budgetId) {
        org.osid.financials.budgeting.Budget budget;
        try {
            budget = getBudget(budgetId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.budgetsByGenus.remove(budget.getGenusType());

        try (org.osid.type.TypeList types = budget.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.budgetsByRecord.remove(types.getNextType(), budget);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBudget(budgetId);
        return;
    }


    /**
     *  Gets a <code>BudgetList</code> corresponding to the given
     *  budget genus <code>Type</code> which does not include
     *  budgets of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known budgets or an error results. Otherwise,
     *  the returned list may contain only those budgets that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  budgetGenusType a budget genus type 
     *  @return the returned <code>Budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByGenusType(org.osid.type.Type budgetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budget.ArrayBudgetList(this.budgetsByGenus.get(budgetGenusType)));
    }


    /**
     *  Gets a <code>BudgetList</code> containing the given
     *  budget record <code>Type</code>. In plenary mode, the
     *  returned list contains all known budgets or an error
     *  results. Otherwise, the returned list may contain only those
     *  budgets that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return the returned <code>budget</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetList getBudgetsByRecordType(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budget.ArrayBudgetList(this.budgetsByRecord.get(budgetRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.budgetsByGenus.clear();
        this.budgetsByRecord.clear();

        super.close();

        return;
    }
}
