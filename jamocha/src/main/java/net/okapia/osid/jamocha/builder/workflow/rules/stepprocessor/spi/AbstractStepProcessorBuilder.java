//
// AbstractStepProcessor.java
//
//     Defines a StepProcessor builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.rules.stepprocessor.spi;


/**
 *  Defines a <code>StepProcessor</code> builder.
 */

public abstract class AbstractStepProcessorBuilder<T extends AbstractStepProcessorBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidProcessorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.workflow.rules.stepprocessor.StepProcessorMiter stepProcessor;


    /**
     *  Constructs a new <code>AbstractStepProcessorBuilder</code>.
     *
     *  @param stepProcessor the step processor to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractStepProcessorBuilder(net.okapia.osid.jamocha.builder.workflow.rules.stepprocessor.StepProcessorMiter stepProcessor) {
        super(stepProcessor);
        this.stepProcessor = stepProcessor;
        return;
    }


    /**
     *  Builds the step processor.
     *
     *  @return the new step processor
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.workflow.rules.StepProcessor build() {
        (new net.okapia.osid.jamocha.builder.validator.workflow.rules.stepprocessor.StepProcessorValidator(getValidations())).validate(this.stepProcessor);
        return (new net.okapia.osid.jamocha.builder.workflow.rules.stepprocessor.ImmutableStepProcessor(this.stepProcessor));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the step processor miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.workflow.rules.stepprocessor.StepProcessorMiter getMiter() {
        return (this.stepProcessor);
    }


    /**
     *  Adds a StepProcessor record.
     *
     *  @param record a step processor record
     *  @param recordType the type of step processor record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.workflow.rules.records.StepProcessorRecord record, org.osid.type.Type recordType) {
        getMiter().addStepProcessorRecord(record, recordType);
        return (self());
    }
}       


