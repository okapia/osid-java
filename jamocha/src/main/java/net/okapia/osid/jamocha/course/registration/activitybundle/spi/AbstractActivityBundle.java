//
// AbstractActivityBundle.java
//
//     Defines an ActivityBundle.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>ActivityBundle</code>.
 */

public abstract class AbstractActivityBundle
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.course.registration.ActivityBundle {

    private org.osid.course.CourseOffering courseOffering;

    private final java.util.Collection<java.math.BigDecimal> credits = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.Activity> activities = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.GradeSystem> gradingOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the course offering <code> Id </code> associated with this 
     *  activity bundle. 
     *
     *  @return the course offering <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.courseOffering.getId());
    }


    /**
     *  Gets the course offering associated with this activity bundle. 
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.courseOffering);
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    protected void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        nullarg(courseOffering, "course offering");
        this.courseOffering = courseOffering;
        return;
    }


    /**
     *  Gets the activity <code> Ids </code> in this bundle. 
     *
     *  @return <code> Ids </code> of the <code> l </code> earning objectives 
     */

    @OSID @Override
    public org.osid.id.IdList getActivityIds() {
        try {
            org.osid.course.ActivityList activities = getActivities();
            return (new net.okapia.osid.jamocha.adapter.converter.course.activity.ActivityToIdList(activities));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the activities in this bundle. 
     *
     *  @return the activites 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.course.activity.ArrayActivityList(this.activities));
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void addActivity(org.osid.course.Activity activity) {
        nullarg(activity, "activity");
        this.activities.add(activity);
        return;
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    protected void setActivities(java.util.Collection<org.osid.course.Activity> activities) {
        nullarg(activities, "activities");

        this.activities.clear();
        this.activities.addAll(activities);

        return;
    }


    /**
     *  Tests if registration to this bundle results in credits to be
     *  earned.
     *
     *  @return <code> true </code> if this course has credits, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean definesCredits() {
        return ((this.credits != null) && (this.credits.size() > 0));
    }


    /**
     *  Gets the number of credits available to register for in this course. 
     *  Each array element is a distinct option. 
     *
     *  @return the number of credits 
     *  @throws org.osid.IllegalStateException <code> definesCredits() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getCredits() {
        if (!definesCredits()) {
            throw new org.osid.IllegalStateException("definesCredits() is false");
        }

        return (this.credits.toArray(new java.math.BigDecimal[this.credits.size()]));
    }


    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void addCredits(java.math.BigDecimal credits) {
        nullarg(credits, "credits");
        this.credits.add(credits);
        return;
    }


    /**
     *  Sets the credit optionss.
     *
     *  @param credits a collection of credit options
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setCredits(java.util.Collection<java.math.BigDecimal> credits) {
        nullarg(credits, "credits");
        
        this.credits.clear();
        this.credits.addAll(credits);

        return;
    }


    /**
     *  Tests if a registration to this bundle will be for for grades. 
     *
     *  @return <code> true </code> if this registration is for grades, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return ((this.gradingOptions != null) && (this.gradingOptions.size() > 0));
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        try {
            org.osid.grading.GradeSystemList gradingOptions = getGradingOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(gradingOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.gradingOptions));
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    protected void addGradingOption(org.osid.grading.GradeSystem option) {
        nullarg(option, "grading option");
        this.gradingOptions.add(option);
        return;
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    protected void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        nullarg(options, "grading options");

        this.gradingOptions.clear();
        this.gradingOptions.addAll(options);

        return;
    }


    /**
     *  Tests if this activityBundle supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityBundleRecordType an activity bundle record type 
     *  @return <code>true</code> if the activityBundleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityBundleRecordType) {
        for (org.osid.course.registration.records.ActivityBundleRecord record : this.records) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ActivityBundle</code> record <code>Type</code>.
     *
     *  @param  activityBundleRecordType the activity bundle record type 
     *  @return the activity bundle record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleRecord getActivityBundleRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleRecord record : this.records) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity bundle. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityBundleRecord the activity bundle record
     *  @param activityBundleRecordType activity bundle record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecord</code> or
     *          <code>activityBundleRecordTypeactivityBundle</code> is
     *          <code>null</code>
     */
            
    protected void addActivityBundleRecord(org.osid.course.registration.records.ActivityBundleRecord activityBundleRecord, 
                                           org.osid.type.Type activityBundleRecordType) {

        nullarg(activityBundleRecord, "activity bundle record");
        addRecordType(activityBundleRecordType);
        this.records.add(activityBundleRecord);
        
        return;
    }
}
