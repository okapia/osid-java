//
// MutableMapEndpointLookupSession
//
//    Implements an Endpoint lookup service backed by a collection of
//    endpoints that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.transport;


/**
 *  Implements an Endpoint lookup service backed by a collection of
 *  endpoints. The endpoints are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of endpoints can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapEndpointLookupSession
    extends net.okapia.osid.jamocha.core.transport.spi.AbstractMapEndpointLookupSession
    implements org.osid.transport.EndpointLookupSession {


    /**
     *  Constructs a new {@code MutableMapEndpointLookupSession}
     *  with no endpoints.
     */

    public MutableMapEndpointLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEndpointLookupSession} with a
     *  single endpoint.
     *  
     *  @param endpoint an endpoint
     *  @throws org.osid.NullArgumentException {@code endpoint}
     *          is {@code null}
     */

    public MutableMapEndpointLookupSession(org.osid.transport.Endpoint endpoint) {
        putEndpoint(endpoint);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEndpointLookupSession}
     *  using an array of endpoints.
     *
     *  @param endpoints an array of endpoints
     *  @throws org.osid.NullArgumentException {@code endpoints}
     *          is {@code null}
     */

    public MutableMapEndpointLookupSession(org.osid.transport.Endpoint[] endpoints) {
        putEndpoints(endpoints);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapEndpointLookupSession}
     *  using a collection of endpoints.
     *
     *  @param endpoints a collection of endpoints
     *  @throws org.osid.NullArgumentException {@code endpoints}
     *          is {@code null}
     */

    public MutableMapEndpointLookupSession(java.util.Collection<? extends org.osid.transport.Endpoint> endpoints) {
        putEndpoints(endpoints);
        return;
    }

    
    /**
     *  Makes an {@code Endpoint} available in this session.
     *
     *  @param endpoint an endpoint
     *  @throws org.osid.NullArgumentException {@code endpoint{@code  is
     *          {@code null}
     */

    @Override
    public void putEndpoint(org.osid.transport.Endpoint endpoint) {
        super.putEndpoint(endpoint);
        return;
    }


    /**
     *  Makes an array of endpoints available in this session.
     *
     *  @param endpoints an array of endpoints
     *  @throws org.osid.NullArgumentException {@code endpoints{@code 
     *          is {@code null}
     */

    @Override
    public void putEndpoints(org.osid.transport.Endpoint[] endpoints) {
        super.putEndpoints(endpoints);
        return;
    }


    /**
     *  Makes collection of endpoints available in this session.
     *
     *  @param endpoints a collection of endpoints
     *  @throws org.osid.NullArgumentException {@code endpoints{@code  is
     *          {@code null}
     */

    @Override
    public void putEndpoints(java.util.Collection<? extends org.osid.transport.Endpoint> endpoints) {
        super.putEndpoints(endpoints);
        return;
    }


    /**
     *  Removes an Endpoint from this session.
     *
     *  @param endpointId the {@code Id} of the endpoint
     *  @throws org.osid.NullArgumentException {@code endpointId{@code 
     *          is {@code null}
     */

    @Override
    public void removeEndpoint(org.osid.id.Id endpointId) {
        super.removeEndpoint(endpointId);
        return;
    }    
}
