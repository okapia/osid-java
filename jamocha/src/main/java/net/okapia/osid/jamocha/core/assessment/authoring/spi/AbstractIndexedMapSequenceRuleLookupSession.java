//
// AbstractIndexedMapSequenceRuleLookupSession.java
//
//    A simple framework for providing a SequenceRule lookup service
//    backed by a fixed collection of sequence rules with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a SequenceRule lookup service backed by a
 *  fixed collection of sequence rules. The sequence rules are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some sequence rules may be compatible
 *  with more types than are indicated through these sequence rule
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>SequenceRules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSequenceRuleLookupSession
    extends AbstractMapSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.SequenceRule> sequenceRulesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.SequenceRule>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.authoring.SequenceRule> sequenceRulesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.authoring.SequenceRule>());


    /**
     *  Makes a <code>SequenceRule</code> available in this session.
     *
     *  @param  sequenceRule a sequence rule
     *  @throws org.osid.NullArgumentException <code>sequenceRule<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSequenceRule(org.osid.assessment.authoring.SequenceRule sequenceRule) {
        super.putSequenceRule(sequenceRule);

        this.sequenceRulesByGenus.put(sequenceRule.getGenusType(), sequenceRule);
        
        try (org.osid.type.TypeList types = sequenceRule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.sequenceRulesByRecord.put(types.getNextType(), sequenceRule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a sequence rule from this session.
     *
     *  @param sequenceRuleId the <code>Id</code> of the sequence rule
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSequenceRule(org.osid.id.Id sequenceRuleId) {
        org.osid.assessment.authoring.SequenceRule sequenceRule;
        try {
            sequenceRule = getSequenceRule(sequenceRuleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.sequenceRulesByGenus.remove(sequenceRule.getGenusType());

        try (org.osid.type.TypeList types = sequenceRule.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.sequenceRulesByRecord.remove(types.getNextType(), sequenceRule);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSequenceRule(sequenceRuleId);
        return;
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> which does not include
     *  sequence rules of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known sequence rules or an error results. Otherwise,
     *  the returned list may contain only those sequence rules that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  sequenceRuleGenusType a sequence rule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequencerule.ArraySequenceRuleList(this.sequenceRulesByGenus.get(sequenceRuleGenusType)));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> containing the given
     *  sequence rule record <code>Type</code>. In plenary mode, the
     *  returned list contains all known sequence rules or an error
     *  results. Otherwise, the returned list may contain only those
     *  sequence rules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  sequenceRuleRecordType a sequence rule record type 
     *  @return the returned <code>sequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByRecordType(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.authoring.sequencerule.ArraySequenceRuleList(this.sequenceRulesByRecord.get(sequenceRuleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.sequenceRulesByGenus.clear();
        this.sequenceRulesByRecord.clear();

        super.close();

        return;
    }
}
