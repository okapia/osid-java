//
// AbstractImmutableInventory.java
//
//     Wraps a mutable Inventory to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Inventory</code> to hide modifiers. This
 *  wrapper provides an immutized Inventory from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying inventory whose state changes are visible.
 */

public abstract class AbstractImmutableInventory
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.inventory.Inventory {

    private final org.osid.inventory.Inventory inventory;


    /**
     *  Constructs a new <code>AbstractImmutableInventory</code>.
     *
     *  @param inventory the inventory to immutablize
     *  @throws org.osid.NullArgumentException <code>inventory</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInventory(org.osid.inventory.Inventory inventory) {
        super(inventory);
        this.inventory = inventory;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Stock. </code> 
     *
     *  @return the stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.inventory.getStockId());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Stock. </code> 
     *
     *  @return the stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.inventory.getStock());
    }


    /**
     *  Gets the inventory date. 
     *
     *  @return the inventory date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.inventory.getDate());
    }


    /**
     *  Gets the quantity of the items in the stock. 
     *
     *  @return the quantity 
     */

    @OSID @Override
    public java.math.BigDecimal getQuantity() {
        return (this.inventory.getQuantity());
    }


    /**
     *  Gets the inventory record corresponding to the given <code> Inventory 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  inventoryRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(inventoryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  inventoryRecordType the type of inventory record to retrieve 
     *  @return the inventory record 
     *  @throws org.osid.NullArgumentException <code> inventoryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(inventoryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.records.InventoryRecord getInventoryRecord(org.osid.type.Type inventoryRecordType)
        throws org.osid.OperationFailedException {

        return (this.inventory.getInventoryRecord(inventoryRecordType));
    }
}

