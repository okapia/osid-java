//
// AbstractAvailabilityLookupSession.java
//
//    A starter implementation framework for providing an Availability
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Availability
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAvailabilities(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAvailabilityLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.AvailabilityLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Availability</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAvailabilities() {
        return (true);
    }


    /**
     *  A complete view of the <code>Availability</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAvailabilityView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Availability</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAvailabilityView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availabilities in foundries which are
     *  children of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only availabilities whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveAvailabilityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All availabilities of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAvailabilityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Availability</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Availability</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Availability</code>
     *  and retained for compatibility.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param availabilityId <code>Id</code> of the
     *          <code>Availability</code>
     *  @return the availability
     *  @throws org.osid.NotFoundException <code>availabilityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>availabilityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Availability getAvailability(org.osid.id.Id availabilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.AvailabilityList availabilities = getAvailabilities()) {
            while (availabilities.hasNext()) {
                org.osid.resourcing.Availability availability = availabilities.getNextAvailability();
                if (availability.getId().equals(availabilityId)) {
                    return (availability);
                }
            }
        } 

        throw new org.osid.NotFoundException(availabilityId + " not found");
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilities specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Availabilities</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAvailabilities()</code>.
     *
     *  @param  availabilityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByIds(org.osid.id.IdList availabilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Availability> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = availabilityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAvailability(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("availability " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.availability.LinkedAvailabilityList(ret));
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the
     *  given availability genus <code>Type</code> which does not
     *  include availabilities of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAvailabilities()</code>.
     *
     *  @param availabilityGenusType an availability genus type
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityGenusFilterList(getAvailabilities(), availabilityGenusType));
    }


    /**
     *  Gets an <code>AvailabilityList</code> corresponding to the
     *  given availability genus <code>Type</code> and include any
     *  additional availabilities with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAvailabilities()</code>.
     *
     *  @param availabilityGenusType an availability genus type
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByParentGenusType(org.osid.type.Type availabilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAvailabilitiesByGenusType(availabilityGenusType));
    }


    /**
     *  Gets an <code>AvailabilityList</code> containing the given
     *  availability record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAvailabilities()</code>.
     *
     *  @param availabilityRecordType an availability record type
     *  @return the returned <code>Availability</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesByRecordType(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityRecordFilterList(getAvailabilities(), availabilityRecordType));
    }


    /**
     *  Gets an <code>AvailabilityList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *  
     *  In active mode, availabilities are returned that are currently
     *  active. In any status mode, active and inactive availabilities
     *  are returned.
     *
     *  @param from start of date range
     *  @param  to end of date range 
     *  @return the returned <code>Availability</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.TemporalAvailabilityFilterList(getAvailabilities(), from, to));
    }
        

    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityFilterList(new ResourceFilter(resourceId), getAvailabilities()));
    }


    /**
     *  Gets a list of availabilities corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.TemporalAvailabilityFilterList(getAvailabilitiesForResource(resourceId), from, to));
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.resourcing.AvailabilityList getAvailabilitiesForJob(org.osid.id.Id jobId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityFilterList(new JobFilter(jobId), getAvailabilities()));
    }


    /**
     *  Gets a list of availabilities corresponding to a job
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>jobId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForJobOnDate(org.osid.id.Id jobId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.TemporalAvailabilityFilterList(getAvailabilitiesForJob(jobId), from, to));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  jobId the <code>Id</code> of the job
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>jobId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJob(org.osid.id.Id resourceId,
                                                                        org.osid.id.Id jobId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityFilterList(new JobFilter(jobId), getAvailabilitiesForResource(resourceId)));
    }


    /**
     *  Gets a list of availabilities corresponding to resource and
     *  job <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @param jobId the <code>Id</code> of the job
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AvailabilityList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>jobId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilitiesForResourceAndJobOnDate(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id jobId,
                                                                                         org.osid.calendaring.DateTime from,
                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.availability.TemporalAvailabilityFilterList(getAvailabilitiesForResourceAndJob(resourceId, jobId), from, to));
    }


    /**
     *  Gets all <code>Availabilities</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availabilities or an error results. Otherwise, the returned
     *  list may contain only those availabilities that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, availabilities are returned that are
     *  currently effective.  In any effective mode, effective
     *  availabilities and those currently expired are returned.
     *
     *  @return a list of <code>Availabilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.AvailabilityList getAvailabilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the availability list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of availabilities
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.AvailabilityList filterAvailabilitiesOnViews(org.osid.resourcing.AvailabilityList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.AvailabilityList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.availability.EffectiveAvailabilityFilterList(ret);
        }

        return (ret);
    }


    public static class ResourceFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>ResourceFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public ResourceFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the AvailabilityFilterList to filter the 
         *  availability list based on resource.
         *
         *  @param availability the availability
         *  @return <code>true</code> to pass the availability,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Availability availability) {
            return (availability.getResourceId().equals(this.resourceId));
        }
    }


    public static class JobFilter
        implements net.okapia.osid.jamocha.inline.filter.resourcing.availability.AvailabilityFilter {
         
        private final org.osid.id.Id jobId;
         
         
        /**
         *  Constructs a new <code>JobFilter</code>.
         *
         *  @param jobId the job to filter
         *  @throws org.osid.NullArgumentException
         *          <code>jobId</code> is <code>null</code>
         */
        
        public JobFilter(org.osid.id.Id jobId) {
            nullarg(jobId, "job Id");
            this.jobId = jobId;
            return;
        }

         
        /**
         *  Used by the AvailabilityFilterList to filter the 
         *  availability list based on job.
         *
         *  @param availability the availability
         *  @return <code>true</code> to pass the availability,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.resourcing.Availability availability) {
            return (availability.getJobId().equals(this.jobId));
        }
    }
}
