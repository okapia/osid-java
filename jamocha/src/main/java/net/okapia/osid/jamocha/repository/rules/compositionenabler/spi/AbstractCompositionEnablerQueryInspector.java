//
// AbstractCompositionEnablerQueryInspector.java
//
//     A template for making a CompositionEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.rules.compositionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for composition enablers.
 */

public abstract class AbstractCompositionEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.repository.rules.CompositionEnablerQueryInspector {

    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCompositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getRuledCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given composition enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a composition enabler implementing the requested record.
     *
     *  @param compositionEnablerRecordType a composition enabler record type
     *  @return the composition enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord getCompositionEnablerQueryInspectorRecord(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition enabler query. 
     *
     *  @param compositionEnablerQueryInspectorRecord composition enabler query inspector
     *         record
     *  @param compositionEnablerRecordType compositionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompositionEnablerQueryInspectorRecord(org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord compositionEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type compositionEnablerRecordType) {

        addRecordType(compositionEnablerRecordType);
        nullarg(compositionEnablerRecordType, "composition enabler record type");
        this.records.add(compositionEnablerQueryInspectorRecord);        
        return;
    }
}
