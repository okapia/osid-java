//
// AbstractRenovation.java
//
//     Defines a Renovation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.construction.renovation.spi;


/**
 *  Defines a <code>Renovation</code> builder.
 */

public abstract class AbstractRenovationBuilder<T extends AbstractRenovationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.room.construction.renovation.RenovationMiter renovation;


    /**
     *  Constructs a new <code>AbstractRenovationBuilder</code>.
     *
     *  @param renovation the renovation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRenovationBuilder(net.okapia.osid.jamocha.builder.room.construction.renovation.RenovationMiter renovation) {
        super(renovation);
        this.renovation = renovation;
        return;
    }


    /**
     *  Builds the renovation.
     *
     *  @return the new renovation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.room.construction.Renovation build() {
        (new net.okapia.osid.jamocha.builder.validator.room.construction.renovation.RenovationValidator(getValidations())).validate(this.renovation);
        return (new net.okapia.osid.jamocha.builder.room.construction.renovation.ImmutableRenovation(this.renovation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the renovation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.room.construction.renovation.RenovationMiter getMiter() {
        return (this.renovation);
    }


    /**
     *  Adds a room.
     *
     *  @param room a room
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>room</code> is
     *          <code>null</code>
     */

    public T room(org.osid.room.Room room) {
        getMiter().addRoom(room);
        return (self());
    }


    /**
     *  Sets all the rooms.
     *
     *  @param rooms a collection of rooms
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rooms</code> is
     *          <code>null</code>
     */

    public T rooms(java.util.Collection<org.osid.room.Room> rooms) {
        getMiter().setRooms(rooms);
        return (self());
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T cost(org.osid.financials.Currency cost) {
        getMiter().setCost(cost);
        return (self());
    }


    /**
     *  Adds a Renovation record.
     *
     *  @param record a renovation record
     *  @param recordType the type of renovation record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.room.construction.records.RenovationRecord record, org.osid.type.Type recordType) {
        getMiter().addRenovationRecord(record, recordType);
        return (self());
    }
}       


