//
// AbstractMapForumLookupSession
//
//    A simple framework for providing a Forum lookup service
//    backed by a fixed collection of forums.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Forum lookup service backed by a
 *  fixed collection of forums. The forums are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Forums</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapForumLookupSession
    extends net.okapia.osid.jamocha.forum.spi.AbstractForumLookupSession
    implements org.osid.forum.ForumLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.forum.Forum> forums = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.forum.Forum>());


    /**
     *  Makes a <code>Forum</code> available in this session.
     *
     *  @param  forum a forum
     *  @throws org.osid.NullArgumentException <code>forum<code>
     *          is <code>null</code>
     */

    protected void putForum(org.osid.forum.Forum forum) {
        this.forums.put(forum.getId(), forum);
        return;
    }


    /**
     *  Makes an array of forums available in this session.
     *
     *  @param  forums an array of forums
     *  @throws org.osid.NullArgumentException <code>forums<code>
     *          is <code>null</code>
     */

    protected void putForums(org.osid.forum.Forum[] forums) {
        putForums(java.util.Arrays.asList(forums));
        return;
    }


    /**
     *  Makes a collection of forums available in this session.
     *
     *  @param  forums a collection of forums
     *  @throws org.osid.NullArgumentException <code>forums<code>
     *          is <code>null</code>
     */

    protected void putForums(java.util.Collection<? extends org.osid.forum.Forum> forums) {
        for (org.osid.forum.Forum forum : forums) {
            this.forums.put(forum.getId(), forum);
        }

        return;
    }


    /**
     *  Removes a Forum from this session.
     *
     *  @param  forumId the <code>Id</code> of the forum
     *  @throws org.osid.NullArgumentException <code>forumId<code> is
     *          <code>null</code>
     */

    protected void removeForum(org.osid.id.Id forumId) {
        this.forums.remove(forumId);
        return;
    }


    /**
     *  Gets the <code>Forum</code> specified by its <code>Id</code>.
     *
     *  @param  forumId <code>Id</code> of the <code>Forum</code>
     *  @return the forum
     *  @throws org.osid.NotFoundException <code>forumId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>forumId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.forum.Forum forum = this.forums.get(forumId);
        if (forum == null) {
            throw new org.osid.NotFoundException("forum not found: " + forumId);
        }

        return (forum);
    }


    /**
     *  Gets all <code>Forums</code>. In plenary mode, the returned
     *  list contains all known forums or an error
     *  results. Otherwise, the returned list may contain only those
     *  forums that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Forums</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ForumList getForums()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.forum.forum.ArrayForumList(this.forums.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.forums.clear();
        super.close();
        return;
    }
}
