//
// MutableIndexedMapCheckLookupSession
//
//    Implements a Check lookup service backed by a collection of
//    checks indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements a Check lookup service backed by a collection of
 *  checks. The checks are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some checks may be compatible
 *  with more types than are indicated through these check
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of checks can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCheckLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractIndexedMapCheckLookupSession
    implements org.osid.rules.check.CheckLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCheckLookupSession} with no checks.
     *
     *  @param engine the engine
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

      public MutableIndexedMapCheckLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCheckLookupSession} with a
     *  single check.
     *  
     *  @param engine the engine
     *  @param  check a single check
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code check} is {@code null}
     */

    public MutableIndexedMapCheckLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.check.Check check) {
        this(engine);
        putCheck(check);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCheckLookupSession} using an
     *  array of checks.
     *
     *  @param engine the engine
     *  @param  checks an array of checks
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code checks} is {@code null}
     */

    public MutableIndexedMapCheckLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.check.Check[] checks) {
        this(engine);
        putChecks(checks);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCheckLookupSession} using a
     *  collection of checks.
     *
     *  @param engine the engine
     *  @param  checks a collection of checks
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code checks} is {@code null}
     */

    public MutableIndexedMapCheckLookupSession(org.osid.rules.Engine engine,
                                                  java.util.Collection<? extends org.osid.rules.check.Check> checks) {

        this(engine);
        putChecks(checks);
        return;
    }
    

    /**
     *  Makes a {@code Check} available in this session.
     *
     *  @param  check a check
     *  @throws org.osid.NullArgumentException {@code check{@code  is
     *          {@code null}
     */

    @Override
    public void putCheck(org.osid.rules.check.Check check) {
        super.putCheck(check);
        return;
    }


    /**
     *  Makes an array of checks available in this session.
     *
     *  @param  checks an array of checks
     *  @throws org.osid.NullArgumentException {@code checks{@code 
     *          is {@code null}
     */

    @Override
    public void putChecks(org.osid.rules.check.Check[] checks) {
        super.putChecks(checks);
        return;
    }


    /**
     *  Makes collection of checks available in this session.
     *
     *  @param  checks a collection of checks
     *  @throws org.osid.NullArgumentException {@code check{@code  is
     *          {@code null}
     */

    @Override
    public void putChecks(java.util.Collection<? extends org.osid.rules.check.Check> checks) {
        super.putChecks(checks);
        return;
    }


    /**
     *  Removes a Check from this session.
     *
     *  @param checkId the {@code Id} of the check
     *  @throws org.osid.NullArgumentException {@code checkId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCheck(org.osid.id.Id checkId) {
        super.removeCheck(checkId);
        return;
    }    
}
