//
// AbstractImmutableFunction.java
//
//     Wraps a mutable Function to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Function</code> to hide modifiers. This
 *  wrapper provides an immutized Function from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying function whose state changes are visible.
 */

public abstract class AbstractImmutableFunction
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.authorization.Function {

    private final org.osid.authorization.Function function;


    /**
     *  Constructs a new <code>AbstractImmutableFunction</code>.
     *
     *  @param function the function to immutablize
     *  @throws org.osid.NullArgumentException <code>function</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFunction(org.osid.authorization.Function function) {
        super(function);
        this.function = function;
        return;
    }


    /**
     *  Gets the qualifier hierarchy <code> Id </code> for this function. 
     *
     *  @return the qualifier hierarchy <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQualifierHierarchyId() {
        return (this.function.getQualifierHierarchyId());
    }


    /**
     *  Gets the qualifier hierarchy for this function. 
     *
     *  @return the qualifier hierarchy 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getQualifierHierarchy()
        throws org.osid.OperationFailedException {

        return (this.function.getQualifierHierarchy());
    }


    /**
     *  Gets the function record corresponding to the given <code> Function 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  functionRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(functionRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  functionRecordType the type of the record to retrieve 
     *  @return the function record 
     *  @throws org.osid.NullArgumentException <code> functionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(functionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionRecord getFunctionRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        return (this.function.getFunctionRecord(functionRecordType));
    }
}

