//
// InvariantMapProxyPublisherLookupSession
//
//    Implements a Publisher lookup service backed by a fixed
//    collection of publishers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Publisher lookup service backed by a fixed
 *  collection of publishers. The publishers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPublisherLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractMapPublisherLookupSession
    implements org.osid.subscription.PublisherLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPublisherLookupSession} with no
     *  publishers.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyPublisherLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPublisherLookupSession} with a
     *  single publisher.
     *
     *  @param publisher a single publisher
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPublisherLookupSession(org.osid.subscription.Publisher publisher, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublisher(publisher);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPublisherLookupSession} using
     *  an array of publishers.
     *
     *  @param publishers an array of publishers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publishers} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPublisherLookupSession(org.osid.subscription.Publisher[] publishers, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublishers(publishers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPublisherLookupSession} using a
     *  collection of publishers.
     *
     *  @param publishers a collection of publishers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publishers} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPublisherLookupSession(java.util.Collection<? extends org.osid.subscription.Publisher> publishers,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putPublishers(publishers);
        return;
    }
}
