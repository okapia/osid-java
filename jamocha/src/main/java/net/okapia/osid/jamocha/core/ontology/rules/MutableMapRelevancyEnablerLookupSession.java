//
// MutableMapRelevancyEnablerLookupSession
//
//    Implements a RelevancyEnabler lookup service backed by a collection of
//    relevancyEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.rules;


/**
 *  Implements a RelevancyEnabler lookup service backed by a collection of
 *  relevancy enablers. The relevancy enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of relevancy enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRelevancyEnablerLookupSession
    extends net.okapia.osid.jamocha.core.ontology.rules.spi.AbstractMapRelevancyEnablerLookupSession
    implements org.osid.ontology.rules.RelevancyEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapRelevancyEnablerLookupSession}
     *  with no relevancy enablers.
     *
     *  @param ontology the ontology
     *  @throws org.osid.NullArgumentException {@code ontology} is
     *          {@code null}
     */

      public MutableMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology) {
        setOntology(ontology);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelevancyEnablerLookupSession} with a
     *  single relevancyEnabler.
     *
     *  @param ontology the ontology  
     *  @param relevancyEnabler a relevancy enabler
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnabler} is {@code null}
     */

    public MutableMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                           org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        this(ontology);
        putRelevancyEnabler(relevancyEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelevancyEnablerLookupSession}
     *  using an array of relevancy enablers.
     *
     *  @param ontology the ontology
     *  @param relevancyEnablers an array of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnablers} is {@code null}
     */

    public MutableMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                           org.osid.ontology.rules.RelevancyEnabler[] relevancyEnablers) {
        this(ontology);
        putRelevancyEnablers(relevancyEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRelevancyEnablerLookupSession}
     *  using a collection of relevancy enablers.
     *
     *  @param ontology the ontology
     *  @param relevancyEnablers a collection of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code ontology} or
     *          {@code relevancyEnablers} is {@code null}
     */

    public MutableMapRelevancyEnablerLookupSession(org.osid.ontology.Ontology ontology,
                                           java.util.Collection<? extends org.osid.ontology.rules.RelevancyEnabler> relevancyEnablers) {

        this(ontology);
        putRelevancyEnablers(relevancyEnablers);
        return;
    }

    
    /**
     *  Makes a {@code RelevancyEnabler} available in this session.
     *
     *  @param relevancyEnabler a relevancy enabler
     *  @throws org.osid.NullArgumentException {@code relevancyEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRelevancyEnabler(org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        super.putRelevancyEnabler(relevancyEnabler);
        return;
    }


    /**
     *  Makes an array of relevancy enablers available in this session.
     *
     *  @param relevancyEnablers an array of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code relevancyEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRelevancyEnablers(org.osid.ontology.rules.RelevancyEnabler[] relevancyEnablers) {
        super.putRelevancyEnablers(relevancyEnablers);
        return;
    }


    /**
     *  Makes collection of relevancy enablers available in this session.
     *
     *  @param relevancyEnablers a collection of relevancy enablers
     *  @throws org.osid.NullArgumentException {@code relevancyEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putRelevancyEnablers(java.util.Collection<? extends org.osid.ontology.rules.RelevancyEnabler> relevancyEnablers) {
        super.putRelevancyEnablers(relevancyEnablers);
        return;
    }


    /**
     *  Removes a RelevancyEnabler from this session.
     *
     *  @param relevancyEnablerId the {@code Id} of the relevancy enabler
     *  @throws org.osid.NullArgumentException {@code relevancyEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRelevancyEnabler(org.osid.id.Id relevancyEnablerId) {
        super.removeRelevancyEnabler(relevancyEnablerId);
        return;
    }    
}
