//
// AbstractInquiryQueryInspector.java
//
//     A template for making an InquiryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for inquiries.
 */

public abstract class AbstractInquiryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.inquiry.InquiryQueryInspector {

    private final java.util.Collection<org.osid.inquiry.records.InquiryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the question query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getQuestionTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the required query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRequiredTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the affirmation required query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAffirmationRequiredTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the needs one response query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getNeedsOneResponseTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given inquiry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an inquiry implementing the requested record.
     *
     *  @param inquiryRecordType an inquiry record type
     *  @return the inquiry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryQueryInspectorRecord getInquiryQueryInspectorRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquiryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquiry query. 
     *
     *  @param inquiryQueryInspectorRecord inquiry query inspector
     *         record
     *  @param inquiryRecordType inquiry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquiryQueryInspectorRecord(org.osid.inquiry.records.InquiryQueryInspectorRecord inquiryQueryInspectorRecord, 
                                                   org.osid.type.Type inquiryRecordType) {

        addRecordType(inquiryRecordType);
        nullarg(inquiryRecordType, "inquiry record type");
        this.records.add(inquiryQueryInspectorRecord);        
        return;
    }
}
