//
// AbstractImmutableCourseEntry.java
//
//     Wraps a mutable CourseEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CourseEntry</code> to hide modifiers. This
 *  wrapper provides an immutized CourseEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying courseEntry whose state changes are visible.
 */

public abstract class AbstractImmutableCourseEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.chronicle.CourseEntry {

    private final org.osid.course.chronicle.CourseEntry courseEntry;


    /**
     *  Constructs a new <code>AbstractImmutableCourseEntry</code>.
     *
     *  @param courseEntry the course entry to immutablize
     *  @throws org.osid.NullArgumentException <code>courseEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCourseEntry(org.osid.course.chronicle.CourseEntry courseEntry) {
        super(courseEntry);
        this.courseEntry = courseEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.courseEntry.getStudentId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getStudent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.courseEntry.getCourseId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getCourse());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term. </code> 
     *
     *  @return the term <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.courseEntry.getTermId());
    }


    /**
     *  Gets the <code> Term. </code> 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getTerm());
    }


    /**
     *  Tests if the course has been completed. 
     *
     *  @return <code> true </code> if the course has been completed, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.courseEntry.isComplete());
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreditScaleId() {
        return (this.courseEntry.getCreditScaleId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getCreditScale()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getCreditScale());
    }


    /**
     *  Gets the number of credits earned in this course. 
     *
     *  @return the credits earned 
     */

    @OSID @Override
    public java.math.BigDecimal getCreditsEarned() {
        return (this.courseEntry.getCreditsEarned());
    }


    /**
     *  Tests if a grade is available. 
     *
     *  @return <code> true </code> if a grade is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasGrade() {
        return (this.courseEntry.hasGrade());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        return (this.courseEntry.getGradeId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getGrade());
    }


    /**
     *  Tests if a score is available. 
     *
     *  @return <code> true </code> if a score is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasScore() {
        return (this.courseEntry.hasScore());
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreScaleId() {
        return (this.courseEntry.getScoreScaleId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreScale()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getScoreScale());
    }


    /**
     *  Gets the cumulative score. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        return (this.courseEntry.getScore());
    }


    /**
     *  Tests if <code> Registrations </code> are available. 
     *
     *  @return <code> true </code> if registrations are available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRegistrations() {
        return (this.courseEntry.hasRegistrations());
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Registrations. </code> 
     *
     *  @return the registrations <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasRegistrations() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRegistrationIds() {
        return (this.courseEntry.getRegistrationIds());
    }


    /**
     *  Gets the <code> Registrations. </code> 
     *
     *  @return the registrations 
     *  @throws org.osid.IllegalStateException <code> hasRegistrations() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getRegistrations());
    }


    /**
     *  Gets the course entry record corresponding to the given <code> 
     *  CourseEntry </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  courseEntryRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(courseEntryRecordType) </code> is <code> true </code> . 
     *
     *  @param  courseEntryRecordType the type of course entry record to 
     *          retrieve 
     *  @return the course entry record 
     *  @throws org.osid.NullArgumentException <code> courseEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(courseEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryRecord getCourseEntryRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.courseEntry.getCourseEntryRecord(courseEntryRecordType));
    }
}

