//
// AbstractCalendarList
//
//     Implements a filter for a CalendarList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a CalendarList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedCalendarList
 *  to improve performance.
 */

public abstract class AbstractCalendarFilterList
    extends net.okapia.osid.jamocha.calendaring.calendar.spi.AbstractCalendarList
    implements org.osid.calendaring.CalendarList,
               net.okapia.osid.jamocha.inline.filter.calendaring.calendar.CalendarFilter {

    private org.osid.calendaring.Calendar calendar;
    private final org.osid.calendaring.CalendarList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractCalendarFilterList</code>.
     *
     *  @param calendarList a <code>CalendarList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>calendarList</code> is <code>null</code>
     */

    protected AbstractCalendarFilterList(org.osid.calendaring.CalendarList calendarList) {
        nullarg(calendarList, "calendar list");
        this.list = calendarList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.calendar == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Calendar </code> in this list. 
     *
     *  @return the next <code> Calendar </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Calendar </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getNextCalendar()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.Calendar calendar = this.calendar;
            this.calendar = null;
            return (calendar);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in calendar list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.calendar = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Calendars.
     *
     *  @param calendar the calendar to filter
     *  @return <code>true</code> if the calendar passes the filter,
     *          <code>false</code> if the calendar should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.Calendar calendar);


    protected void prime() {
        if (this.calendar != null) {
            return;
        }

        org.osid.calendaring.Calendar calendar = null;

        while (this.list.hasNext()) {
            try {
                calendar = this.list.getNextCalendar();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(calendar)) {
                this.calendar = calendar;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
