//
// AbstractOsidTemporalForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidTemporalForm.
 */

public abstract class AbstractOsidTemporalForm
    extends AbstractOsidForm
    implements org.osid.OsidTemporalForm {
    
    private org.osid.calendaring.DateTime start;
    private boolean startCleared = false;
    private org.osid.Metadata startMetadata = getDefaultDateTimeMetadata(getStartDateId(), "start date");    

    private org.osid.calendaring.DateTime end;
    private boolean endCleared = false;
    private org.osid.Metadata endMetadata = getDefaultDateTimeMetadata(getEndDateId(), "end date");    
    

    /** 
     *  Constructs a new {@code AbstractOsidTemporalForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidTemporalForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the metadata for a start date. 
     *
     *  @return metadata for the date 
     */

    @OSID @Override
    public org.osid.Metadata getStartDateMetadata() {
        return (this.startMetadata);
    }


    /**
     *  Sets the start date metadata.
     *
     *  @param metadata the start date metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setStartDateMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "start date metadata");
        this.startMetadata = metadata;
        return;
    }


    /**
     *  Gets the Id for the start date field.
     *
     *  @return the start date field Id
     */

    protected org.osid.id.Id getStartDateId() {
        return (TemporalElements.getStartDate());
    }


    /**
     *  Sets the start date. 
     *
     *  @param  date the new date 
     *  @throws org.osid.InvalidArgumentException <code> date </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(date, getStartDateMetadata());

        this.start = date;
        this.startCleared = false;

        return;
    }


    /**
     *  Clears the start date. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearStartDate() {
        if (getStartDateMetadata().isRequired()) {
            throw new org.osid.NoAccessException("start date is required");
        }

        this.start = null;
        this.startCleared = true;

        return;
    }


    /**
     *  Tests if a start has been set in this form.
     *
     *  @return {@code true} if the start has been set, {@code
     *          false} otherwise
     */

    protected boolean isStartDateSet() {
        return (this.start != null);
    }


    /**
     *  Tests if a start has been cleared in this form.
     *
     *  @return {@code true} if the start has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isStartDateCleared() {
        return (this.startCleared);
    }


    /**
     *  Gets the metadata for a end date. 
     *
     *  @return metadata for the date 
     */

    @OSID @Override
    public org.osid.Metadata getEndDateMetadata() {
        return (this.endMetadata);
    }


    /**
     *  Sets the end date metadata.
     *
     *  @param metadata the end date metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setEndDateMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "end date metadata");
        this.endMetadata = metadata;
        return;
    }


    /**
     *  Gets the Id for the end date field.
     *
     *  @return the end date field Id
     */

    protected org.osid.id.Id getEndDateId() {
        return (TemporalElements.getEndDate());
    }


    /**
     *  Sets the end date. 
     *
     *  @param  date the new date 
     *  @throws org.osid.InvalidArgumentException <code> date </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(date, getEndDateMetadata());

        this.end = date;
        this.endCleared = false;

        return;
    }


    /**
     *  Clears the end date. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearEndDate() {
        if (getEndDateMetadata().isRequired()) {
            throw new org.osid.NoAccessException("end date is required");
        }

        this.end = null;
        this.endCleared = true;

        return;
    }
}
