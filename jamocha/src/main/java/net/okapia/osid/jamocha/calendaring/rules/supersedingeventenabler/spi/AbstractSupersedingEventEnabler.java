//
// AbstractSupersedingEventEnabler.java
//
//     Defines a SupersedingEventEnabler.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>SupersedingEventEnabler</code>.
 */

public abstract class AbstractSupersedingEventEnabler
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnabler
    implements org.osid.calendaring.rules.SupersedingEventEnabler {

    private final java.util.Collection<org.osid.calendaring.rules.records.SupersedingEventEnablerRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this superseding event enabler supports the given
     *  record <code>Type</code>.
     *
     *  @param supersedingEventEnablerRecordType a superseding event
     *         enabler record type
     *  @return <code>true</code> if the supersedingEventEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        for (org.osid.calendaring.rules.records.SupersedingEventEnablerRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>SupersedingEventEnabler</code> record <code>Type</code>.
     *
     *  @param supersedingEventEnablerRecordType the superseding event
     *         enabler record type
     *  @return the superseding event enabler record 
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.SupersedingEventEnablerRecord getSupersedingEventEnablerRecord(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.SupersedingEventEnablerRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event enabler. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param supersedingEventEnablerRecord the superseding event enabler record
     *  @param supersedingEventEnablerRecordType superseding event enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecord</code> or
     *          <code>supersedingEventEnablerRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addSupersedingEventEnablerRecord(org.osid.calendaring.rules.records.SupersedingEventEnablerRecord supersedingEventEnablerRecord, 
                                                    org.osid.type.Type supersedingEventEnablerRecordType) {

        nullarg(supersedingEventEnablerRecord, "superseding event enabler record");
        addRecordType(supersedingEventEnablerRecordType);
        this.records.add(supersedingEventEnablerRecord);
        
        return;
    }
}
