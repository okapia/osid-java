//
// AbstractAdapterRuleLookupSession.java
//
//    A Rule lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Rule lookup session adapter.
 */

public abstract class AbstractAdapterRuleLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.RuleLookupSession {

    private final org.osid.rules.RuleLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRuleLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRuleLookupSession(org.osid.rules.RuleLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Engine/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the {@code Engine} associated with this session.
     *
     *  @return the {@code Engine} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform {@code Rule} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRules() {
        return (this.session.canLookupRules());
    }


    /**
     *  A complete view of the {@code Rule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRuleView() {
        this.session.useComparativeRuleView();
        return;
    }


    /**
     *  A complete view of the {@code Rule} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRuleView() {
        this.session.usePlenaryRuleView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include rules in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    

    /**
     *  Only active rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRuleView() {
        this.session.useActiveRuleView();
        return;
    }


    /**
     *  Active and inactive rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRuleView() {
        this.session.useAnyStatusRuleView();
        return;
    }
    
     
    /**
     *  Gets the {@code Rule} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Rule} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Rule} and
     *  retained for compatibility.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param ruleId {@code Id} of the {@code Rule}
     *  @return the rule
     *  @throws org.osid.NotFoundException {@code ruleId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code ruleId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Rule getRule(org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRule(ruleId));
    }


    /**
     *  Gets a {@code RuleList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  rules specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Rules} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Rule} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code ruleIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByIds(org.osid.id.IdList ruleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRulesByIds(ruleIds));
    }


    /**
     *  Gets a {@code RuleList} corresponding to the given
     *  rule genus {@code Type} which does not include
     *  rules of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned {@code Rule} list
     *  @throws org.osid.NullArgumentException
     *          {@code ruleGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRulesByGenusType(ruleGenusType));
    }


    /**
     *  Gets a {@code RuleList} corresponding to the given
     *  rule genus {@code Type} and include any additional
     *  rules with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleGenusType a rule genus type 
     *  @return the returned {@code Rule} list
     *  @throws org.osid.NullArgumentException
     *          {@code ruleGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByParentGenusType(org.osid.type.Type ruleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRulesByParentGenusType(ruleGenusType));
    }


    /**
     *  Gets a {@code RuleList} containing the given
     *  rule record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @param  ruleRecordType a rule record type 
     *  @return the returned {@code Rule} list
     *  @throws org.osid.NullArgumentException
     *          {@code ruleRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRulesByRecordType(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRulesByRecordType(ruleRecordType));
    }


    /**
     *  Gets all {@code Rules}. 
     *
     *  In plenary mode, the returned list contains all known
     *  rules or an error results. Otherwise, the returned list
     *  may contain only those rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, rules are returned that are currently
     *  active. In any status mode, active and inactive rules
     *  are returned.
     *
     *  @return a list of {@code Rules} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.RuleList getRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRules());
    }
}
