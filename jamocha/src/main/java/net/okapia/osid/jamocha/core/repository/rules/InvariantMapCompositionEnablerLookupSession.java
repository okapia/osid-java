//
// InvariantMapCompositionEnablerLookupSession
//
//    Implements a CompositionEnabler lookup service backed by a fixed collection of
//    compositionEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.rules;


/**
 *  Implements a CompositionEnabler lookup service backed by a fixed
 *  collection of composition enablers. The composition enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.repository.rules.spi.AbstractMapCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionEnablerLookupSession</code> with no
     *  composition enablers.
     *  
     *  @param repository the repository
     *  @throws org.osid.NullArgumnetException {@code repository} is
     *          {@code null}
     */

    public InvariantMapCompositionEnablerLookupSession(org.osid.repository.Repository repository) {
        setRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionEnablerLookupSession</code> with a single
     *  composition enabler.
     *  
     *  @param repository the repository
     *  @param compositionEnabler a single composition enabler
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnabler} is <code>null</code>
     */

      public InvariantMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        this(repository);
        putCompositionEnabler(compositionEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionEnablerLookupSession</code> using an array
     *  of composition enablers.
     *  
     *  @param repository the repository
     *  @param compositionEnablers an array of composition enablers
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnablers} is <code>null</code>
     */

      public InvariantMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                               org.osid.repository.rules.CompositionEnabler[] compositionEnablers) {
        this(repository);
        putCompositionEnablers(compositionEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCompositionEnablerLookupSession</code> using a
     *  collection of composition enablers.
     *
     *  @param repository the repository
     *  @param compositionEnablers a collection of composition enablers
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositionEnablers} is <code>null</code>
     */

      public InvariantMapCompositionEnablerLookupSession(org.osid.repository.Repository repository,
                                               java.util.Collection<? extends org.osid.repository.rules.CompositionEnabler> compositionEnablers) {
        this(repository);
        putCompositionEnablers(compositionEnablers);
        return;
    }
}
