//
// MutableMapProxyRelationshipEnablerLookupSession
//
//    Implements a RelationshipEnabler lookup service backed by a collection of
//    relationshipEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.rules;


/**
 *  Implements a RelationshipEnabler lookup service backed by a collection of
 *  relationshipEnablers. The relationshipEnablers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of relationship enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyRelationshipEnablerLookupSession
    extends net.okapia.osid.jamocha.core.relationship.rules.spi.AbstractMapRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyRelationshipEnablerLookupSession}
     *  with no relationship enablers.
     *
     *  @param family the family
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                  org.osid.proxy.Proxy proxy) {
        setFamily(family);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyRelationshipEnablerLookupSession} with a
     *  single relationship enabler.
     *
     *  @param family the family
     *  @param relationshipEnabler a relationship enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family},
     *          {@code relationshipEnabler}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                org.osid.relationship.rules.RelationshipEnabler relationshipEnabler, org.osid.proxy.Proxy proxy) {
        this(family, proxy);
        putRelationshipEnabler(relationshipEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRelationshipEnablerLookupSession} using an
     *  array of relationship enablers.
     *
     *  @param family the family
     *  @param relationshipEnablers an array of relationship enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family},
     *          {@code relationshipEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers, org.osid.proxy.Proxy proxy) {
        this(family, proxy);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyRelationshipEnablerLookupSession} using a
     *  collection of relationship enablers.
     *
     *  @param family the family
     *  @param relationshipEnablers a collection of relationship enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family},
     *          {@code relationshipEnablers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyRelationshipEnablerLookupSession(org.osid.relationship.Family family,
                                                java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(family, proxy);
        setSessionProxy(proxy);
        putRelationshipEnablers(relationshipEnablers);
        return;
    }

    
    /**
     *  Makes a {@code RelationshipEnabler} available in this session.
     *
     *  @param relationshipEnabler an relationship enabler
     *  @throws org.osid.NullArgumentException {@code relationshipEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putRelationshipEnabler(org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        super.putRelationshipEnabler(relationshipEnabler);
        return;
    }


    /**
     *  Makes an array of relationshipEnablers available in this session.
     *
     *  @param relationshipEnablers an array of relationship enablers
     *  @throws org.osid.NullArgumentException {@code relationshipEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRelationshipEnablers(org.osid.relationship.rules.RelationshipEnabler[] relationshipEnablers) {
        super.putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Makes collection of relationship enablers available in this session.
     *
     *  @param relationshipEnablers
     *  @throws org.osid.NullArgumentException {@code relationshipEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putRelationshipEnablers(java.util.Collection<? extends org.osid.relationship.rules.RelationshipEnabler> relationshipEnablers) {
        super.putRelationshipEnablers(relationshipEnablers);
        return;
    }


    /**
     *  Removes a RelationshipEnabler from this session.
     *
     *  @param relationshipEnablerId the {@code Id} of the relationship enabler
     *  @throws org.osid.NullArgumentException {@code relationshipEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRelationshipEnabler(org.osid.id.Id relationshipEnablerId) {
        super.removeRelationshipEnabler(relationshipEnablerId);
        return;
    }    
}
