//
// AbstractQueryCyclicEventLookupSession.java
//
//    A CyclicEventQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CyclicEventQuerySession adapter.
 */

public abstract class AbstractAdapterCyclicEventQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.cycle.CyclicEventQuerySession {

    private final org.osid.calendaring.cycle.CyclicEventQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCyclicEventQuerySession.
     *
     *  @param session the underlying cyclic event query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCyclicEventQuerySession(org.osid.calendaring.cycle.CyclicEventQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCalendar</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCalendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@codeCalendar</code> associated with this 
     *  session.
     *
     *  @return the {@codeCalendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@codeCyclicEvent</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCyclicEvents() {
        return (this.session.canSearchCyclicEvents());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this calendar only.
     */
    
    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
      
    /**
     *  Gets a cyclic event query. The returned query will not have an
     *  extension query.
     *
     *  @return the cyclic event query 
     */
      
    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getCyclicEventQuery() {
        return (this.session.getCyclicEventQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  cyclicEventQuery the cyclic event query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code cyclicEventQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code cyclicEventQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByQuery(org.osid.calendaring.cycle.CyclicEventQuery cyclicEventQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCyclicEventsByQuery(cyclicEventQuery));
    }
}
