//
// AbstractGradeEntryQuery.java
//
//     A template for making a GradeEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for grade entries.
 */

public abstract class AbstractGradeEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.grading.GradeEntryQuery {

    private final java.util.Collection<org.osid.grading.records.GradeEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available for 
     *  querying creators. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        return;
    }


    /**
     *  Sets the key resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchKeyResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the key resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearKeyResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQUery </code> is available for querying key 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsKeyResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a key resource. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsKeyResourceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getKeyResourceQuery() {
        throw new org.osid.UnimplementedException("supportsKeyResourceQuery() is false");
    }


    /**
     *  Matches grade entries with any key resource. 
     *
     *  @param  match <code> true </code> to match grade entries with any key 
     *          resource, <code> false </code> to match entries with no key 
     *          resource 
     */

    @OSID @Override
    public void matchAnyKeyResource(boolean match) {
        return;
    }


    /**
     *  Clears the key resource terms. 
     */

    @OSID @Override
    public void clearKeyResourceTerms() {
        return;
    }


    /**
     *  Matches derived grade entries. 
     *
     *  @param  match <code> true </code> to match derived grade entries , 
     *          <code> false </code> to match manual entries 
     */

    @OSID @Override
    public void matchDerived(boolean match) {
        return;
    }


    /**
     *  Clears the derived terms. 
     */

    @OSID @Override
    public void clearDerivedTerms() {
        return;
    }


    /**
     *  Sets the grade entry <code> Id </code> for an overridden calculated 
     *  grade entry. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOverriddenGradeEntryId(org.osid.id.Id gradeEntryId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the overridden grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOverriddenGradeEntryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeEntry </code> is available for querying 
     *  overridden calculated grade entries. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOverriddenGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an overridden derived grade entry. Multiple 
     *  retrievals produce a nested <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOverriddenGradeEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getOverriddenGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsOverriddenGradeEntryQuery() is false");
    }


    /**
     *  Matches grade entries overriding any calculated grade entry. 
     *
     *  @param  match <code> true </code> to match grade entries overriding 
     *          any grade entry, <code> false </code> to match entries not 
     *          overriding any entry 
     */

    @OSID @Override
    public void matchAnyOverriddenGradeEntry(boolean match) {
        return;
    }


    /**
     *  Clears the overridden grade entry terms. 
     */

    @OSID @Override
    public void clearOverriddenGradeEntryTerms() {
        return;
    }


    /**
     *  Matches grade entries ignored for calculations. 
     *
     *  @param  match <code> true </code> to match grade entries ignored for 
     *          calculations, <code> false </code> to match entries used in 
     *          calculations 
     */

    @OSID @Override
    public void matchIgnoredForCalculations(boolean match) {
        return;
    }


    /**
     *  Clears the ignored for calculation entries terms. 
     */

    @OSID @Override
    public void clearIgnoredForCalculationsTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying grades. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches grade entries with any grade. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grade, <code> false </code> to match entries with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        return;
    }


    /**
     *  Matches grade entries which score is between the specified score 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchScore(java.math.BigDecimal start, 
                           java.math.BigDecimal end, boolean match) {
        return;
    }


    /**
     *  Matches grade entries with any score. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          score, <code> false </code> to match entries with no score 
     */

    @OSID @Override
    public void matchAnyScore(boolean match) {
        return;
    }


    /**
     *  Clears the score terms. 
     */

    @OSID @Override
    public void clearScoreTerms() {
        return;
    }


    /**
     *  Matches grade entries which graded time is between the specified times 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     */

    @OSID @Override
    public void matchTimeGraded(org.osid.calendaring.DateTime start, 
                                org.osid.calendaring.DateTime end, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the time graded terms. 
     */

    @OSID @Override
    public void clearTimeGradedTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraderId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the grader <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraderIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  graders. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getGraderQuery() {
        throw new org.osid.UnimplementedException("supportsGraderQuery() is false");
    }


    /**
     *  Matches grade entries with any grader. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grader, <code> false </code> to match entries with no grader 
     */

    @OSID @Override
    public void matchAnyGrader(boolean match) {
        return;
    }


    /**
     *  Clears the grader terms. 
     */

    @OSID @Override
    public void clearGraderTerms() {
        return;
    }


    /**
     *  Sets the grading agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the grader <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  grading agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getGradingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsGradingAgentQuery() is false");
    }


    /**
     *  Matches grade entries with any grading agent. 
     *
     *  @param  match <code> true </code> to match grade entries with any 
     *          grading agent, <code> false </code> to match entries with no 
     *          grading agent 
     */

    @OSID @Override
    public void matchAnyGradingAgent(boolean match) {
        return;
    }


    /**
     *  Clears the grading agent terms. 
     */

    @OSID @Override
    public void clearGradingAgentTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given grade entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a grade entry implementing the requested record.
     *
     *  @param gradeEntryRecordType a grade entry record type
     *  @return the grade entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntryQueryRecord getGradeEntryQueryRecord(org.osid.type.Type gradeEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(gradeEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade entry query. 
     *
     *  @param gradeEntryQueryRecord grade entry query record
     *  @param gradeEntryRecordType gradeEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeEntryQueryRecord(org.osid.grading.records.GradeEntryQueryRecord gradeEntryQueryRecord, 
                                          org.osid.type.Type gradeEntryRecordType) {

        addRecordType(gradeEntryRecordType);
        nullarg(gradeEntryQueryRecord, "grade entry query record");
        this.records.add(gradeEntryQueryRecord);        
        return;
    }
}
