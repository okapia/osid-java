//
// AbstractAppointmentQueryInspector.java
//
//     A template for making an AppointmentQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.appointment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for appointments.
 */

public abstract class AbstractAppointmentQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.personnel.AppointmentQueryInspector {

    private final java.util.Collection<org.osid.personnel.records.AppointmentQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the person <code> Id </code> query terms. 
     *
     *  @return the person <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPersonIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the person query terms. 
     *
     *  @return the person terms 
     */

    @OSID @Override
    public org.osid.personnel.PersonQueryInspector[] getPersonTerms() {
        return (new org.osid.personnel.PersonQueryInspector[0]);
    }


    /**
     *  Gets the position <code> Id </code> query terms. 
     *
     *  @return the position <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPositionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the position terms 
     */

    @OSID @Override
    public org.osid.personnel.PositionQueryInspector[] getPositionTerms() {
        return (new org.osid.personnel.PositionQueryInspector[0]);
    }


    /**
     *  Gets the commitment query terms. 
     *
     *  @return the commitment terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getCommitmentTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the title query terms. 
     *
     *  @return the title terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the salary query terms. 
     *
     *  @return the salary terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getSalaryTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the realm <code> Id </code> query terms. 
     *
     *  @return the realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRealmIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the realm query terms. 
     *
     *  @return the realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given appointment query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an appointment implementing the requested record.
     *
     *  @param appointmentRecordType an appointment record type
     *  @return the appointment query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>appointmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(appointmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.AppointmentQueryInspectorRecord getAppointmentQueryInspectorRecord(org.osid.type.Type appointmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.AppointmentQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(appointmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(appointmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this appointment query. 
     *
     *  @param appointmentQueryInspectorRecord appointment query inspector
     *         record
     *  @param appointmentRecordType appointment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAppointmentQueryInspectorRecord(org.osid.personnel.records.AppointmentQueryInspectorRecord appointmentQueryInspectorRecord, 
                                                   org.osid.type.Type appointmentRecordType) {

        addRecordType(appointmentRecordType);
        nullarg(appointmentRecordType, "appointment record type");
        this.records.add(appointmentQueryInspectorRecord);        
        return;
    }
}
