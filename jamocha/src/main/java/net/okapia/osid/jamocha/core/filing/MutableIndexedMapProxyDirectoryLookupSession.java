//
// MutableIndexedMapProxyDirectoryLookupSession
//
//    Implements a Directory lookup service backed by a collection of
//    directories indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.filing;


/**
 *  Implements a Directory lookup service backed by a collection of
 *  directories. The directories are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some directories may be compatible
 *  with more types than are indicated through these directory
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of directories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyDirectoryLookupSession
    extends net.okapia.osid.jamocha.core.filing.spi.AbstractIndexedMapDirectoryLookupSession
    implements org.osid.filing.DirectoryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyDirectoryLookupSession} with
     *  no directory.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyDirectoryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyDirectoryLookupSession} with
     *  a single directory.
     *
     *  @param  directory an directory
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directory} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyDirectoryLookupSession(org.osid.filing.Directory directory, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectory(directory);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyDirectoryLookupSession} using
     *  an array of directories.
     *
     *  @param  directories an array of directories
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directories} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyDirectoryLookupSession(org.osid.filing.Directory[] directories, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectories(directories);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyDirectoryLookupSession} using
     *  a collection of directories.
     *
     *  @param  directories a collection of directories
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code directories} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyDirectoryLookupSession(java.util.Collection<? extends org.osid.filing.Directory> directories,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDirectories(directories);
        return;
    }

    
    /**
     *  Makes a {@code Directory} available in this session.
     *
     *  @param  directory a directory
     *  @throws org.osid.NullArgumentException {@code directory{@code 
     *          is {@code null}
     */

    @Override
    public void putDirectory(org.osid.filing.Directory directory) {
        super.putDirectory(directory);
        return;
    }


    /**
     *  Makes an array of directories available in this session.
     *
     *  @param  directories an array of directories
     *  @throws org.osid.NullArgumentException {@code directories{@code 
     *          is {@code null}
     */

    @Override
    public void putDirectories(org.osid.filing.Directory[] directories) {
        super.putDirectories(directories);
        return;
    }


    /**
     *  Makes collection of directories available in this session.
     *
     *  @param  directories a collection of directories
     *  @throws org.osid.NullArgumentException {@code directory{@code 
     *          is {@code null}
     */

    @Override
    public void putDirectories(java.util.Collection<? extends org.osid.filing.Directory> directories) {
        super.putDirectories(directories);
        return;
    }


    /**
     *  Removes a Directory from this session.
     *
     *  @param directoryId the {@code Id} of the directory
     *  @throws org.osid.NullArgumentException {@code directoryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDirectory(org.osid.id.Id directoryId) {
        super.removeDirectory(directoryId);
        return;
    }    
}
