//
// AbstractQueryBusinessLookupSession.java
//
//    An inline adapter that maps a BusinessLookupSession to
//    a BusinessQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BusinessLookupSession to
 *  a BusinessQuerySession.
 */

public abstract class AbstractQueryBusinessLookupSession
    extends net.okapia.osid.jamocha.billing.spi.AbstractBusinessLookupSession
    implements org.osid.billing.BusinessLookupSession {

    private final org.osid.billing.BusinessQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryBusinessLookupSession.
     *
     *  @param querySession the underlying business query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBusinessLookupSession(org.osid.billing.BusinessQuerySession querySession) {
        nullarg(querySession, "business query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Business</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBusinesses() {
        return (this.session.canSearchBusinesses());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Business</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Business</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Business</code> and
     *  retained for compatibility.
     *
     *  @param  businessId <code>Id</code> of the
     *          <code>Business</code>
     *  @return the business
     *  @throws org.osid.NotFoundException <code>businessId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>businessId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchId(businessId, true);
        org.osid.billing.BusinessList businesses = this.session.getBusinessesByQuery(query);
        if (businesses.hasNext()) {
            return (businesses.getNextBusiness());
        } 
        
        throw new org.osid.NotFoundException(businessId + " not found");
    }


    /**
     *  Gets a <code>BusinessList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  businesses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Businesses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  businessIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Business</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>businessIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByIds(org.osid.id.IdList businessIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();

        try (org.osid.id.IdList ids = businessIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBusinessesByQuery(query));
    }


    /**
     *  Gets a <code>BusinessList</code> corresponding to the given
     *  business genus <code>Type</code> which does not include
     *  businesses of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  businessGenusType a business genus type 
     *  @return the returned <code>Business</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>businessGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByGenusType(org.osid.type.Type businessGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchGenusType(businessGenusType, true);
        return (this.session.getBusinessesByQuery(query));
    }


    /**
     *  Gets a <code>BusinessList</code> corresponding to the given
     *  business genus <code>Type</code> and include any additional
     *  businesses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  businessGenusType a business genus type 
     *  @return the returned <code>Business</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>businessGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByParentGenusType(org.osid.type.Type businessGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchParentGenusType(businessGenusType, true);
        return (this.session.getBusinessesByQuery(query));
    }


    /**
     *  Gets a <code>BusinessList</code> containing the given
     *  business record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  businessRecordType a business record type 
     *  @return the returned <code>Business</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByRecordType(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchRecordType(businessRecordType, true);
        return (this.session.getBusinessesByQuery(query));
    }


    /**
     *  Gets a <code>BusinessList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known businesses or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  businesses that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Business</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getBusinessesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Businesses</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Businesses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.BusinessQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBusinessesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.BusinessQuery getQuery() {
        org.osid.billing.BusinessQuery query = this.session.getBusinessQuery();        
        return (query);
    }
}
