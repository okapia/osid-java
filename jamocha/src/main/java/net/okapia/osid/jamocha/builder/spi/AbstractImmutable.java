//
// AbstractImmutable
//
//     An immutable wrapper for an object.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for an Id based object.
 */

public abstract class AbstractImmutable {

    private final Object object;
    private String label;
    private int hash;


    /**
     *  Constructs a new <code>AbstractImmutable</code>.
     *
     *  @param object the object to wrap
     *  @throws org.osid.NullArgumentException <code>object</code>
     *          is <code>null</code>
     */

    protected AbstractImmutable(Object object) {
        nullarg(object, "object");
        this.object = object;
        return;
    }


    /**
     *  Returns a hash code value for this object.
     *
     *  @return a hash code value for this object
     */

    @Override
    public int hashCode() { 
        if (this.hash == 0) {
            this.hash = this.object.hashCode();
        }

        return (this.hash);
    }


    /**
     *  Returns a string representation of this object.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        if (this.label == null) {
            label = this.object.toString();
        }

        return (this.label);
    }
}
