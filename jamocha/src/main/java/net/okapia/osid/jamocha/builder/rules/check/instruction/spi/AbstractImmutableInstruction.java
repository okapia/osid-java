//
// AbstractImmutableInstruction.java
//
//     Wraps a mutable Instruction to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Instruction</code> to hide modifiers. This
 *  wrapper provides an immutized Instruction from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying instruction whose state changes are visible.
 */

public abstract class AbstractImmutableInstruction
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.rules.check.Instruction {

    private final org.osid.rules.check.Instruction instruction;


    /**
     *  Constructs a new <code>AbstractImmutableInstruction</code>.
     *
     *  @param instruction the instruction to immutablize
     *  @throws org.osid.NullArgumentException <code>instruction</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableInstruction(org.osid.rules.check.Instruction instruction) {
        super(instruction);
        this.instruction = instruction;
        return;
    }


    /**
     *  Tests if a reason this instruction came to an end is known. 
     *
     *  @return <code> true </code> if an end reason is available, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code>isEffective</code> is
     *          <code>true</code>
     */

    @OSID @Override
    public boolean hasEndReason() {
        return (this.instruction.hasEndReason());
    }


    /**
     *  Gets a state <code> Id </code> indicating why this instruction has 
     *  ended. 
     *
     *  @return a state <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEndReasonId() {
        return (this.instruction.getEndReasonId());
    }


    /**
     *  Gets a state indicating why this instruction has ended. 
     *
     *  @return a state 
     *  @throws org.osid.IllegalStateException <code> hasEndReason() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getEndReason()
        throws org.osid.OperationFailedException {

        return (this.instruction.getEndReason());
    }


    /**
     *  Gets the <code> Id </code> of the agenda. 
     *
     *  @return the agenda <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgendaId() {
        return (this.instruction.getAgendaId());
    }


    /**
     *  Gets the agenda. 
     *
     *  @return the agenda 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda()
        throws org.osid.OperationFailedException {

        return (this.instruction.getAgenda());
    }


    /**
     *  Gets the <code> Id </code> of the check. 
     *
     *  @return the check <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCheckId() {
        return (this.instruction.getCheckId());
    }


    /**
     *  Gets the check. 
     *
     *  @return the check 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.check.Check getCheck()
        throws org.osid.OperationFailedException {

        return (this.instruction.getCheck());
    }


    /**
     *  Gets the message to be returned upon failure of the check evaluation. 
     *
     *  @return the message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.instruction.getMessage());
    }


    /**
     *  Tests if failure of the check should be interpreted as a warning and 
     *  not a failure. 
     *
     *  @return <code> true </code> if this is a warning, <code> false </code> 
     *          if an error 
     */

    @OSID @Override
    public boolean isWarning() {
        return (this.instruction.isWarning());
    }


    /**
     *  Tests if evaluation of the next instruction should continue if the 
     *  check in this instruction fails. While the overall evaluation of the 
     *  agenda still fails, if true, allows for other messages to be gathered. 
     *
     *  @return <code> true </code> if this processing should continue on 
     *          failure, <code> false </code> if processing should cease upon 
     *          failure 
     */

    @OSID @Override
    public boolean continueOnFail() {
        return (this.instruction.continueOnFail());
    }


    /**
     *  Gets the instruction record corresponding to the given <code> 
     *  Instruction </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  instructionRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(instructionRecordType) </code> is <code> true </code> . 
     *
     *  @param  instructionRecordType the type of instruction record to 
     *          retrieve 
     *  @return the instruction record 
     *  @throws org.osid.NullArgumentException <code> instructionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(instructionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionRecord getInstructionRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.instruction.getInstructionRecord(instructionRecordType));
    }
}

