//
// AbstractQueryFunctionLookupSession.java
//
//    An inline adapter that maps a FunctionLookupSession to
//    a FunctionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a FunctionLookupSession to
 *  a FunctionQuerySession.
 */

public abstract class AbstractQueryFunctionLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {

    private boolean activeonly    = false;
    private final org.osid.authorization.FunctionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryFunctionLookupSession.
     *
     *  @param querySession the underlying function query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryFunctionLookupSession(org.osid.authorization.FunctionQuerySession querySession) {
        nullarg(querySession, "function query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Vault</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.session.getVaultId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getVault());
    }


    /**
     *  Tests if this user can perform <code>Function</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFunctions() {
        return (this.session.canSearchFunctions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include functions in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        this.session.useFederatedVaultView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        this.session.useIsolatedVaultView();
        return;
    }
    

    /**
     *  Only active functions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveFunctionView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive functions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusFunctionView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Function</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Function</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Function</code> and
     *  retained for compatibility.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionId <code>Id</code> of the
     *          <code>Function</code>
     *  @return the function
     *  @throws org.osid.NotFoundException <code>functionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>functionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Function getFunction(org.osid.id.Id functionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();
        query.matchId(functionId, true);
        org.osid.authorization.FunctionList functions = this.session.getFunctionsByQuery(query);
        if (functions.hasNext()) {
            return (functions.getNextFunction());
        } 
        
        throw new org.osid.NotFoundException(functionId + " not found");
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  functions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Functions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>functionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByIds(org.osid.id.IdList functionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();

        try (org.osid.id.IdList ids = functionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getFunctionsByQuery(query));
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  function genus <code>Type</code> which does not include
     *  functions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();
        query.matchGenusType(functionGenusType, true);
        return (this.session.getFunctionsByQuery(query));
    }


    /**
     *  Gets a <code>FunctionList</code> corresponding to the given
     *  function genus <code>Type</code> and include any additional
     *  functions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionGenusType a function genus type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByParentGenusType(org.osid.type.Type functionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();
        query.matchParentGenusType(functionGenusType, true);
        return (this.session.getFunctionsByQuery(query));
    }


    /**
     *  Gets a <code>FunctionList</code> containing the given
     *  function record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @param  functionRecordType a function record type 
     *  @return the returned <code>Function</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctionsByRecordType(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();
        query.matchRecordType(functionRecordType, true);
        return (this.session.getFunctionsByQuery(query));
    }

    
    /**
     *  Gets all <code>Functions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  functions or an error results. Otherwise, the returned list
     *  may contain only those functions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, functions are returned that are currently
     *  active. In any status mode, active and inactive functions
     *  are returned.
     *
     *  @return a list of <code>Functions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.FunctionList getFunctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authorization.FunctionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getFunctionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authorization.FunctionQuery getQuery() {
        org.osid.authorization.FunctionQuery query = this.session.getFunctionQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
