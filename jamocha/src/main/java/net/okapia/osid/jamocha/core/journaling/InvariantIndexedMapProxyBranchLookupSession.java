//
// InvariantIndexedMapProxyBranchLookupSession
//
//    Implements a Branch lookup service backed by a fixed
//    collection of branches indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a Branch lookup service backed by a fixed
 *  collection of branches. The branches are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some branches may be compatible
 *  with more types than are indicated through these branch
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyBranchLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractIndexedMapBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyBranchLookupSession}
     *  using an array of branches.
     *
     *  @param journal the journal
     *  @param branches an array of branches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code branches} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                         org.osid.journaling.Branch[] branches, 
                                                         org.osid.proxy.Proxy proxy) {

        setJournal(journal);
        setSessionProxy(proxy);
        putBranches(branches);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyBranchLookupSession}
     *  using a collection of branches.
     *
     *  @param journal the journal
     *  @param branches a collection of branches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code branches} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                         java.util.Collection<? extends org.osid.journaling.Branch> branches,
                                                         org.osid.proxy.Proxy proxy) {

        setJournal(journal);
        setSessionProxy(proxy);
        putBranches(branches);

        return;
    }
}
