//
// AbstractImmutableOfferingConstrainer.java
//
//     Wraps a mutable OfferingConstrainer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>OfferingConstrainer</code> to hide modifiers. This
 *  wrapper provides an immutized OfferingConstrainer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying offeringConstrainer whose state changes are visible.
 */

public abstract class AbstractImmutableOfferingConstrainer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidConstrainer
    implements org.osid.offering.rules.OfferingConstrainer {

    private final org.osid.offering.rules.OfferingConstrainer offeringConstrainer;


    /**
     *  Constructs a new <code>AbstractImmutableOfferingConstrainer</code>.
     *
     *  @param offeringConstrainer the offering constrainer to immutablize
     *  @throws org.osid.NullArgumentException <code>offeringConstrainer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOfferingConstrainer(org.osid.offering.rules.OfferingConstrainer offeringConstrainer) {
        super(offeringConstrainer);
        this.offeringConstrainer = offeringConstrainer;
        return;
    }


    /**
     *  Tests if the description can be overridden at offering. 
     *
     *  @return <code> true </code> if the description can be overridden, 
     *          <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideDescription() {
        return (this.offeringConstrainer.canOverrideDescription());
    }


    /**
     *  Tests if the title can be overridden at offering. 
     *
     *  @return <code> true </code> if the title can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideTitle() {
        return (this.offeringConstrainer.canOverrideTitle());
    }


    /**
     *  Tests if the code can be overridden at offering. 
     *
     *  @return <code> true </code> if the code can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideCode() {
        return (this.offeringConstrainer.canOverrideCode());
    }


    /**
     *  Tests if the offerings can be made outside the specified time period 
     *  cycles. 
     *
     *  @return <code> true </code> if the time period cycles can be 
     *          overridden, <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideTimePeriods() {
        return (this.offeringConstrainer.canOverrideTimePeriods());
    }


    /**
     *  Tests if offerings in all specified time periods are optional. 
     *
     *  @return <code> true </code> if the time period cycles can be optional, 
     *          <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canConstrainTimePeriods() {
        return (this.offeringConstrainer.canConstrainTimePeriods());
    }


    /**
     *  Tests if the result options can be overridden at offering. 
     *
     *  @return <code> true </code> if the result options can be overridden, 
     *          <code> false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideResultOptions() {
        return (this.offeringConstrainer.canOverrideResultOptions());
    }


    /**
     *  Tests if the offering result options can be a subset of the canonical 
     *  set. 
     *
     *  @return <code> true </code> if the result options can be a subset, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean canConstrainResultOptions() {
        return (this.offeringConstrainer.canConstrainResultOptions());
    }


    /**
     *  Tests if the sponsors can be overridden at offering. 
     *
     *  @return <code> true </code> if the sponsors can be overridden, <code> 
     *          false </code> if constrained 
     */

    @OSID @Override
    public boolean canOverrideSponsors() {
        return (this.offeringConstrainer.canOverrideSponsors());
    }


    /**
     *  Tests if the offering sponsors can be a subset of the canonical set. 
     *
     *  @return <code> true </code> if the sponsors can be a subset, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean canConstrainSponsors() {
        return (this.offeringConstrainer.canConstrainSponsors());
    }


    /**
     *  Gets the offering constrainer record corresponding to the given <code> 
     *  OfferingConstrainer </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> offeringConstrainerRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(offeringConstrainerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  offeringConstrainerRecordType the type of offering constrainer 
     *          record to retrieve 
     *  @return the offering constrainer record 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(offeringConstrainerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerRecord getOfferingConstrainerRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        return (this.offeringConstrainer.getOfferingConstrainerRecord(offeringConstrainerRecordType));
    }
}

