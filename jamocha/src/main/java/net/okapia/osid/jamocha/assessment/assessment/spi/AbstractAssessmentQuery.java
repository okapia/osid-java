//
// AbstractAssessmentQuery.java
//
//     A template for making an Assessment Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for assessments.
 */

public abstract class AbstractAssessmentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.assessment.AssessmentQuery {

    private final java.util.Collection<org.osid.assessment.records.AssessmentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the level grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLevelId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears all level <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLevelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLevelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsLevelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getLevelQuery() {
        throw new org.osid.UnimplementedException("supportsLevelQuery() is false");
    }


    /**
     *  Matches an assessment that has any level assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any level, 
     *          <code> false </code> to match assessments with no level 
     */

    @OSID @Override
    public void matchAnyLevel(boolean match) {
        return;
    }


    /**
     *  Clears all level terms. 
     */

    @OSID @Override
    public void clearLevelTerms() {
        return;
    }


    /**
     *  Sets the rubric assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRubricId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRubricIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if a rubric assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rubric assessment. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> supportsRubricQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getRubricQuery() {
        throw new org.osid.UnimplementedException("supportsRubricQuery() is false");
    }


    /**
     *  Matches an assessment that has any rubric assessment assigned. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          rubric, <code> false </code> to match assessments with no 
     *          rubric 
     */

    @OSID @Override
    public void matchAnyRubric(boolean match) {
        return;
    }


    /**
     *  Clears all rubric assessment terms. 
     */

    @OSID @Override
    public void clearRubricTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears all item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches an assessment that has any item. 
     *
     *  @param  match <code> true </code> to match assessments with any item, 
     *          <code> false </code> to match assessments with no items 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears all item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment offered <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment offered. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Matches an assessment that has any offering. 
     *
     *  @param  match <code> true </code> to match assessments with any 
     *          offering, <code> false </code> to match assessments with no 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyAssessmentOffered(boolean match) {
        return;
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        return;
    }


    /**
     *  Sets the assessment taken <code> Id </code> for this query. 
     *
     *  @param  assessmentTakenId an assessment taken <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentTakenId(org.osid.id.Id assessmentTakenId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears all assessment taken <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentTakenQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment taken query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment taken. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment taken query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuery getAssessmentTakenQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentTakenQuery() is false");
    }


    /**
     *  Matches an assessment that has any taken version. 
     *
     *  @param  match <code> true </code> to match assessments with any taken 
     *          assessments, <code> false </code> to match assessments with no 
     *          taken assessments 
     */

    @OSID @Override
    public void matchAnyAssessmentTaken(boolean match) {
        return;
    }


    /**
     *  Clears all assessment taken terms. 
     */

    @OSID @Override
    public void clearAssessmentTakenTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for this query. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears all bank terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given assessment query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment implementing the requested record.
     *
     *  @param assessmentRecordType an assessment record type
     *  @return the assessment query record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentQueryRecord getAssessmentQueryRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentQueryRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment query. 
     *
     *  @param assessmentQueryRecord assessment query record
     *  @param assessmentRecordType assessment record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentQueryRecord(org.osid.assessment.records.AssessmentQueryRecord assessmentQueryRecord, 
                                          org.osid.type.Type assessmentRecordType) {

        addRecordType(assessmentRecordType);
        nullarg(assessmentQueryRecord, "assessment query record");
        this.records.add(assessmentQueryRecord);        
        return;
    }
}
