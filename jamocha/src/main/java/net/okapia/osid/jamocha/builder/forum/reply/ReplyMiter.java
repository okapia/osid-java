//
// ReplyMiter.java
//
//     Defines a Reply miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.forum.reply;


/**
 *  Defines a <code>Reply</code> miter for use with the builders.
 */

public interface ReplyMiter
    extends net.okapia.osid.jamocha.builder.spi.ContainableOsidObjectMiter,
            org.osid.forum.Reply {


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    public void setPost(org.osid.forum.Post post);


    /**
     *  Adds a reply.
     *
     *  @param reply a reply
     *  @throws org.osid.NullArgumentException <code>reply</code> is
     *          <code>null</code>
     */

    public void addReply(org.osid.forum.Reply reply);


    /**
     *  Sets all the replies.
     *
     *  @param replies a collection of replies
     *  @throws org.osid.NullArgumentException <code>replies</code> is
     *          <code>null</code>
     */

    public void setReplies(java.util.Collection<org.osid.forum.Reply> replies);


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @throws org.osid.NullArgumentException <code>timestamp</code>
     *          is <code>null</code>
     */

    public void setTimestamp(org.osid.calendaring.DateTime timestamp);


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @throws org.osid.NullArgumentException <code>poster</code> is
     *          <code>null</code>
     */

    public void setPoster(org.osid.resource.Resource poster);


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setPostingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    public void setSubjectLine(org.osid.locale.DisplayText subjectLine);


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public void setText(org.osid.locale.DisplayText text);


    /**
     *  Adds a Reply record.
     *
     *  @param record a reply record
     *  @param recordType the type of reply record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addReplyRecord(org.osid.forum.records.ReplyRecord record, org.osid.type.Type recordType);
}       


