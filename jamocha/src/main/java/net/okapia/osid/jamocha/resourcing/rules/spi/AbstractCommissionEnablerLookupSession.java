//
// AbstractCommissionEnablerLookupSession.java
//
//    A starter implementation framework for providing a CommissionEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CommissionEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCommissionEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCommissionEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.rules.CommissionEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>CommissionEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCommissionEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>CommissionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCommissionEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CommissionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCommissionEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include commission enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active commission enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCommissionEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive commission enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCommissionEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>CommissionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CommissionEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CommissionEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  @param  commissionEnablerId <code>Id</code> of the
     *          <code>CommissionEnabler</code>
     *  @return the commission enabler
     *  @throws org.osid.NotFoundException <code>commissionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>commissionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnabler getCommissionEnabler(org.osid.id.Id commissionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.rules.CommissionEnablerList commissionEnablers = getCommissionEnablers()) {
            while (commissionEnablers.hasNext()) {
                org.osid.resourcing.rules.CommissionEnabler commissionEnabler = commissionEnablers.getNextCommissionEnabler();
                if (commissionEnabler.getId().equals(commissionEnablerId)) {
                    return (commissionEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(commissionEnablerId + " not found");
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  commissionEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CommissionEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCommissionEnablers()</code>.
     *
     *  @param  commissionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByIds(org.osid.id.IdList commissionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.rules.CommissionEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = commissionEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCommissionEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("commission enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.rules.commissionenabler.LinkedCommissionEnablerList(ret));
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the given
     *  commission enabler genus <code>Type</code> which does not include
     *  commission enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCommissionEnablers()</code>.
     *
     *  @param  commissionEnablerGenusType a commissionEnabler genus type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByGenusType(org.osid.type.Type commissionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.commissionenabler.CommissionEnablerGenusFilterList(getCommissionEnablers(), commissionEnablerGenusType));
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> corresponding to the given
     *  commission enabler genus <code>Type</code> and include any additional
     *  commission enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommissionEnablers()</code>.
     *
     *  @param  commissionEnablerGenusType a commissionEnabler genus type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByParentGenusType(org.osid.type.Type commissionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCommissionEnablersByGenusType(commissionEnablerGenusType));
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> containing the given
     *  commission enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCommissionEnablers()</code>.
     *
     *  @param  commissionEnablerRecordType a commissionEnabler record type 
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commissionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersByRecordType(org.osid.type.Type commissionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.commissionenabler.CommissionEnablerRecordFilterList(getCommissionEnablers(), commissionEnablerRecordType));
    }


    /**
     *  Gets a <code>CommissionEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible
     *  through this session.
     *  
     *  In active mode, commission enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive commission enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CommissionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.rules.commissionenabler.TemporalCommissionEnablerFilterList(getCommissionEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>CommissionEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible
     *  through this session.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive commission enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>CommissionEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getCommissionEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>CommissionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  commission enablers or an error results. Otherwise, the returned list
     *  may contain only those commission enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, commission enablers are returned that are currently
     *  active. In any status mode, active and inactive commission enablers
     *  are returned.
     *
     *  @return a list of <code>CommissionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the commission enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of commission enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.rules.CommissionEnablerList filterCommissionEnablersOnViews(org.osid.resourcing.rules.CommissionEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.rules.CommissionEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.rules.commissionenabler.ActiveCommissionEnablerFilterList(ret);
        }

        return (ret);
    }
}
