//
// AbstractActivityUnitSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractActivityUnitSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.ActivityUnitSearchResults {

    private org.osid.course.ActivityUnitList activityUnits;
    private final org.osid.course.ActivityUnitQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.ActivityUnitSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractActivityUnitSearchResults.
     *
     *  @param activityUnits the result set
     *  @param activityUnitQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>activityUnits</code>
     *          or <code>activityUnitQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractActivityUnitSearchResults(org.osid.course.ActivityUnitList activityUnits,
                                            org.osid.course.ActivityUnitQueryInspector activityUnitQueryInspector) {
        nullarg(activityUnits, "activity units");
        nullarg(activityUnitQueryInspector, "activity unit query inspectpr");

        this.activityUnits = activityUnits;
        this.inspector = activityUnitQueryInspector;

        return;
    }


    /**
     *  Gets the activity unit list resulting from a search.
     *
     *  @return an activity unit list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnits() {
        if (this.activityUnits == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.ActivityUnitList activityUnits = this.activityUnits;
        this.activityUnits = null;
	return (activityUnits);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.ActivityUnitQueryInspector getActivityUnitQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  activity unit search record <code> Type. </code> This method must
     *  be used to retrieve an activityUnit implementing the requested
     *  record.
     *
     *  @param activityUnitSearchRecordType an activityUnit search 
     *         record type 
     *  @return the activity unit search
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(activityUnitSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitSearchResultsRecord getActivityUnitSearchResultsRecord(org.osid.type.Type activityUnitSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.ActivityUnitSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(activityUnitSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(activityUnitSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record activity unit search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addActivityUnitRecord(org.osid.course.records.ActivityUnitSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "activity unit record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
