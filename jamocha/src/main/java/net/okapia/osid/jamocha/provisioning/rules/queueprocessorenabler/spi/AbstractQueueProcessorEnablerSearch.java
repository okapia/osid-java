//
// AbstractQueueProcessorEnablerSearch.java
//
//     A template for making a QueueProcessorEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing queue processor enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQueueProcessorEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.QueueProcessorEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueProcessorEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.QueueProcessorEnablerSearchOrder queueProcessorEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of queue processor enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  queueProcessorEnablerIds list of queue processor enablers
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQueueProcessorEnablers(org.osid.id.IdList queueProcessorEnablerIds) {
        while (queueProcessorEnablerIds.hasNext()) {
            try {
                this.ids.add(queueProcessorEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQueueProcessorEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of queue processor enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQueueProcessorEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  queueProcessorEnablerSearchOrder queue processor enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>queueProcessorEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQueueProcessorEnablerResults(org.osid.provisioning.rules.QueueProcessorEnablerSearchOrder queueProcessorEnablerSearchOrder) {
	this.queueProcessorEnablerSearchOrder = queueProcessorEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.QueueProcessorEnablerSearchOrder getQueueProcessorEnablerSearchOrder() {
	return (this.queueProcessorEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given queue processor enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue processor enabler implementing the requested record.
     *
     *  @param queueProcessorEnablerSearchRecordType a queue processor enabler search record
     *         type
     *  @return the queue processor enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorEnablerSearchRecord getQueueProcessorEnablerSearchRecord(org.osid.type.Type queueProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.QueueProcessorEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor enabler search. 
     *
     *  @param queueProcessorEnablerSearchRecord queue processor enabler search record
     *  @param queueProcessorEnablerSearchRecordType queueProcessorEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorEnablerSearchRecord(org.osid.provisioning.rules.records.QueueProcessorEnablerSearchRecord queueProcessorEnablerSearchRecord, 
                                           org.osid.type.Type queueProcessorEnablerSearchRecordType) {

        addRecordType(queueProcessorEnablerSearchRecordType);
        this.records.add(queueProcessorEnablerSearchRecord);        
        return;
    }
}
