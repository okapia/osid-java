//
// AbstractOntologyBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOntologyBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.ontology.batch.OntologyBatchManager,
               org.osid.ontology.batch.OntologyBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractOntologyBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOntologyBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of subjects is available. 
     *
     *  @return <code> true </code> if a subject bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of relevancies is available. 
     *
     *  @return <code> true </code> if a relevancy bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of ontologies is available. 
     *
     *  @return <code> true </code> if an ontology bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service. 
     *
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchManager.getSubjectBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchProxyManager.getSubjectBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchManager.getSubjectBatchAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk subject 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubjectBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.SubjectBatchAdminSession getSubjectBatchAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchProxyManager.getSubjectBatchAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service. 
     *
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchManager.getRelevancyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchProxyManager.getRelevancyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSessionForOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchManager.getRelevancyBatchAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk relevancy 
     *  administration service for the given ontology. 
     *
     *  @param  ontologyId the <code> Id </code> of the <code> Ontology 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RelevancyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Ontology </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.RelevancyBatchAdminSession getRelevancyBatchAdminSessionForOntology(org.osid.id.Id ontologyId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchProxyManager.getRelevancyBatchAdminSessionForOntology not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ontology 
     *  administration service. 
     *
     *  @return an <code> OntologyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchAdminSession getOntologyBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchManager.getOntologyBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ontology 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OntologyBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.batch.OntologyBatchAdminSession getOntologyBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.ontology.batch.OntologyBatchProxyManager.getOntologyBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
