//
// AbstractCurrencyMetadata.java
//
//     Defines a currency Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.financials.USDCurrency;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a currency Metadata.
 */

public abstract class AbstractCurrencyMetadata
    extends AbstractMetadata 
    implements org.osid.Metadata {

    private final Types types = new TypeSet();
    private org.osid.financials.Currency minimum = USDCurrency.valueOf("$0");
    private org.osid.financials.Currency maximum = USDCurrency.valueOf("$250,000");

    private final java.util.Collection<org.osid.financials.Currency> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.Currency> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.Currency> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractCurrencyMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCurrencyMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.CURRENCY, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractCurrencyMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCurrencyMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.CURRENCY, elementId, isArray, isLinked);
        return;
    }



    /**
     *  Gets the set of acceptable currency types. 
     *
     *  @return the set of currency types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          CURRENCY or SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getCurrencyTypes() {
        return (this.types.toArray());
    }


    /**
     *  Tests if the given currency type is supported. 
     *
     *  @param  currencyType a currency Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          CURRENCY </code> 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyType(org.osid.type.Type currencyType) {
        return (this.types.contains(currencyType));
    }


    /**
     *  Add support for a currency type.
     *
     *  @param currencyType the type of currency
     *  @throws org.osid.NullArgumentException {@code currencyType} is
     *          {@code null}
     */

    protected void addCurrencyType(org.osid.type.Type currencyType) {
        this.types.add(currencyType);
        return;
    }


    /**
     *  Gets the minimum currency value. 
     *
     *  @return the minimum currency 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code>
     */

    @OSID @Override
    public org.osid.financials.Currency getMinimumCurrency() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum currency value. 
     *
     *  @return the maximum currency 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code>
     */

    @OSID @Override
    public org.osid.financials.Currency getMaximumCurrency() {
        return (this.maximum);
    }


    /**
     *  Gets the set of acceptable currency values. 
     *
     *  @return a set of currencys or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code>
     */

    @OSID @Override
    public org.osid.financials.Currency[] getCurrencySet() {
        return (this.set.toArray(new org.osid.financials.Currency[this.set.size()]));
    }

    
    /**
     *  Sets the currency set.
     *
     *  @param values a collection of accepted currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setCurrencySet(java.util.Collection<org.osid.financials.Currency> values) {
        this.set.clear();
        addToCurrencySet(values);
        return;
    }


    /**
     *  Adds a collection of values to the currency set.
     *
     *  @param values a collection of accepted currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToCurrencySet(java.util.Collection<org.osid.financials.Currency> values) {
        nullarg(values, "currency set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the currency set.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToCurrencySet(org.osid.financials.Currency value) {
        nullarg(value, "currency value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the currency set.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromCurrencySet(org.osid.financials.Currency value) {
        nullarg(value, "currency value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the currency set.
     */

    protected void clearCurrencySet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default currency values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default currency values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          CURRENCY </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.financials.Currency[] getDefaultCurrencyValues() {
        return (this.defvals.toArray(new org.osid.financials.Currency[this.defvals.size()]));
    }


    /**
     *  Sets the default currency set.
     *
     *  @param values a collection of default currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        clearDefaultCurrencyValues();
        addDefaultCurrencyValues(values);
        return;
    }


    /**
     *  Adds a collection of default currency values.
     *
     *  @param values a collection of default currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        nullarg(values, "default currency values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultCurrencyValue(org.osid.financials.Currency value) {
        nullarg(value, "default currency value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultCurrencyValue(org.osid.financials.Currency value) {
        nullarg(value, "default currency value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default currency values.
     */

    protected void clearDefaultCurrencyValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing currency values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing currency values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          CURRENCY </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency[] getExistingCurrencyValues() {
        return (this.existing.toArray(new org.osid.financials.Currency[this.existing.size()]));
    }


    /**
     *  Sets the existing currency set.
     *
     *  @param values a collection of existing currency values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        clearExistingCurrencyValues();
        addExistingCurrencyValues(values);
        return;
    }


    /**
     *  Adds a collection of existing currency values.
     *
     *  @param values a collection of existing currency values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingCurrencyValues(java.util.Collection<org.osid.financials.Currency> values) {
        nullarg(values, "existing currency values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingCurrencyValue(org.osid.financials.Currency value) {
        nullarg(value, "existing currency value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing currency value.
     *
     *  @param value a currency value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingCurrencyValue(org.osid.financials.Currency value) {
        nullarg(value, "existing currency value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing currency values.
     */

    protected void clearExistingCurrencyValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}