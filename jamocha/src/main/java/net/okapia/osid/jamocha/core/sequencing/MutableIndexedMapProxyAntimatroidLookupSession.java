//
// MutableIndexedMapProxyAntimatroidLookupSession
//
//    Implements an Antimatroid lookup service backed by a collection of
//    antimatroids indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing;


/**
 *  Implements an Antimatroid lookup service backed by a collection of
 *  antimatroids. The antimatroids are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some antimatroids may be compatible
 *  with more types than are indicated through these antimatroid
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of antimatroids can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyAntimatroidLookupSession
    extends net.okapia.osid.jamocha.core.sequencing.spi.AbstractIndexedMapAntimatroidLookupSession
    implements org.osid.sequencing.AntimatroidLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAntimatroidLookupSession} with
     *  no antimatroid.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyAntimatroidLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAntimatroidLookupSession} with
     *  a single antimatroid.
     *
     *  @param  antimatroid an antimatroid
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroid} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAntimatroidLookupSession(org.osid.sequencing.Antimatroid antimatroid, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAntimatroid(antimatroid);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAntimatroidLookupSession} using
     *  an array of antimatroids.
     *
     *  @param  antimatroids an array of antimatroids
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroids} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAntimatroidLookupSession(org.osid.sequencing.Antimatroid[] antimatroids, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAntimatroids(antimatroids);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyAntimatroidLookupSession} using
     *  a collection of antimatroids.
     *
     *  @param  antimatroids a collection of antimatroids
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroids} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyAntimatroidLookupSession(java.util.Collection<? extends org.osid.sequencing.Antimatroid> antimatroids,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAntimatroids(antimatroids);
        return;
    }

    
    /**
     *  Makes an {@code Antimatroid} available in this session.
     *
     *  @param  antimatroid an antimatroid
     *  @throws org.osid.NullArgumentException {@code antimatroid{@code 
     *          is {@code null}
     */

    @Override
    public void putAntimatroid(org.osid.sequencing.Antimatroid antimatroid) {
        super.putAntimatroid(antimatroid);
        return;
    }


    /**
     *  Makes an array of antimatroids available in this session.
     *
     *  @param  antimatroids an array of antimatroids
     *  @throws org.osid.NullArgumentException {@code antimatroids{@code 
     *          is {@code null}
     */

    @Override
    public void putAntimatroids(org.osid.sequencing.Antimatroid[] antimatroids) {
        super.putAntimatroids(antimatroids);
        return;
    }


    /**
     *  Makes collection of antimatroids available in this session.
     *
     *  @param  antimatroids a collection of antimatroids
     *  @throws org.osid.NullArgumentException {@code antimatroid{@code 
     *          is {@code null}
     */

    @Override
    public void putAntimatroids(java.util.Collection<? extends org.osid.sequencing.Antimatroid> antimatroids) {
        super.putAntimatroids(antimatroids);
        return;
    }


    /**
     *  Removes an Antimatroid from this session.
     *
     *  @param antimatroidId the {@code Id} of the antimatroid
     *  @throws org.osid.NullArgumentException {@code antimatroidId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAntimatroid(org.osid.id.Id antimatroidId) {
        super.removeAntimatroid(antimatroidId);
        return;
    }    
}
