//
// AbstractAssemblyAddressQuery.java
//
//     An AddressQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.contact.address.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AddressQuery that stores terms.
 */

public abstract class AbstractAssemblyAddressQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.contact.AddressQuery,
               org.osid.contact.AddressQueryInspector,
               org.osid.contact.AddressSearchOrder {

    private final java.util.Collection<org.osid.contact.records.AddressQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.records.AddressSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAddressQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAddressQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddresseeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches the address text. 
     *
     *  @param  address the address text 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> address </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAddressText(String address, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getAddressTextColumn(), address, stringMatchType, match);
        return;
    }


    /**
     *  Clears the text address terms. 
     */

    @OSID @Override
    public void clearAddressTextTerms() {
        getAssembler().clearTerms(getAddressTextColumn());
        return;
    }


    /**
     *  Gets the address text terms. 
     *
     *  @return the string terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getAddressTextTerms() {
        return (getAssembler().getStringTerms(getAddressTextColumn()));
    }


    /**
     *  Gets the AddressText column name.
     *
     * @return the column name
     */

    protected String getAddressTextColumn() {
        return ("address_text");
    }


    /**
     *  Sets the contact <code> Id </code> for this query to match contacts 
     *  assigned to addresses. 
     *
     *  @param  contactId a contact <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContactId(org.osid.id.Id contactId, boolean match) {
        getAssembler().addIdTerm(getContactIdColumn(), contactId, match);
        return;
    }


    /**
     *  Clears the contact <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContactIdTerms() {
        getAssembler().clearTerms(getContactIdColumn());
        return;
    }


    /**
     *  Gets the contact <code> Id </code> terms. 
     *
     *  @return the contact <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getContactIdTerms() {
        return (getAssembler().getIdTerms(getContactIdColumn()));
    }


    /**
     *  Gets the ContactId column name.
     *
     * @return the column name
     */

    protected String getContactIdColumn() {
        return ("contact_id");
    }


    /**
     *  Tests if a contact query is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address. 
     *
     *  @return the contact query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getContactQuery() {
        throw new org.osid.UnimplementedException("supportsContactQuery() is false");
    }


    /**
     *  Matches addresses with any contact. 
     *
     *  @param  match <code> true </code> to match addresses with any contact, 
     *          <code> false </code> to match addresses with no contacts 
     */

    @OSID @Override
    public void matchAnyContact(boolean match) {
        getAssembler().addIdWildcardTerm(getContactColumn(), match);
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        getAssembler().clearTerms(getContactColumn());
        return;
    }


    /**
     *  Gets the contact terms. 
     *
     *  @return the contact terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the Contact column name.
     *
     * @return the column name
     */

    protected String getContactColumn() {
        return ("contact");
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        getAssembler().addIdTerm(getAddressBookIdColumn(), addressBookId, match);
        return;
    }


    /**
     *  Clears the address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        getAssembler().clearTerms(getAddressBookIdColumn());
        return;
    }


    /**
     *  Gets the address book <code> Id </code> terms. 
     *
     *  @return the address book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (getAssembler().getIdTerms(getAddressBookIdColumn()));
    }


    /**
     *  Gets the AddressBookId column name.
     *
     * @return the column name
     */

    protected String getAddressBookIdColumn() {
        return ("address_book_id");
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        getAssembler().clearTerms(getAddressBookColumn());
        return;
    }


    /**
     *  Gets the address book terms. 
     *
     *  @return the address book terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the AddressBook column name.
     *
     * @return the column name
     */

    protected String getAddressBookColumn() {
        return ("address_book");
    }


    /**
     *  Tests if this address supports the given record
     *  <code>Type</code>.
     *
     *  @param  addressRecordType an address record type 
     *  @return <code>true</code> if the addressRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type addressRecordType) {
        for (org.osid.contact.records.AddressQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(addressRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  addressRecordType the address record type 
     *  @return the address query record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressQueryRecord getAddressQueryRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  addressRecordType the address record type 
     *  @return the address query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressQueryInspectorRecord getAddressQueryInspectorRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param addressRecordType the address record type
     *  @return the address search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressSearchOrderRecord getAddressSearchOrderRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this address. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param addressQueryRecord the address query record
     *  @param addressQueryInspectorRecord the address query inspector
     *         record
     *  @param addressSearchOrderRecord the address search order record
     *  @param addressRecordType address record type
     *  @throws org.osid.NullArgumentException
     *          <code>addressQueryRecord</code>,
     *          <code>addressQueryInspectorRecord</code>,
     *          <code>addressSearchOrderRecord</code> or
     *          <code>addressRecordTypeaddress</code> is
     *          <code>null</code>
     */
            
    protected void addAddressRecords(org.osid.contact.records.AddressQueryRecord addressQueryRecord, 
                                      org.osid.contact.records.AddressQueryInspectorRecord addressQueryInspectorRecord, 
                                      org.osid.contact.records.AddressSearchOrderRecord addressSearchOrderRecord, 
                                      org.osid.type.Type addressRecordType) {

        addRecordType(addressRecordType);

        nullarg(addressQueryRecord, "address query record");
        nullarg(addressQueryInspectorRecord, "address query inspector record");
        nullarg(addressSearchOrderRecord, "address search odrer record");

        this.queryRecords.add(addressQueryRecord);
        this.queryInspectorRecords.add(addressQueryInspectorRecord);
        this.searchOrderRecords.add(addressSearchOrderRecord);
        
        return;
    }
}
