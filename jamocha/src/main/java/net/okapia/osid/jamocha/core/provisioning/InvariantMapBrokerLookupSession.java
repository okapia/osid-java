//
// InvariantMapBrokerLookupSession
//
//    Implements a Broker lookup service backed by a fixed collection of
//    brokers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Broker lookup service backed by a fixed
 *  collection of brokers. The brokers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBrokerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapBrokerLookupSession
    implements org.osid.provisioning.BrokerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBrokerLookupSession</code> with no
     *  brokers.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapBrokerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBrokerLookupSession</code> with a single
     *  broker.
     *  
     *  @param distributor the distributor
     *  @param broker a single broker
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code broker} is <code>null</code>
     */

      public InvariantMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.Broker broker) {
        this(distributor);
        putBroker(broker);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBrokerLookupSession</code> using an array
     *  of brokers.
     *  
     *  @param distributor the distributor
     *  @param brokers an array of brokers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokers} is <code>null</code>
     */

      public InvariantMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.Broker[] brokers) {
        this(distributor);
        putBrokers(brokers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBrokerLookupSession</code> using a
     *  collection of brokers.
     *
     *  @param distributor the distributor
     *  @param brokers a collection of brokers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code brokers} is <code>null</code>
     */

      public InvariantMapBrokerLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.Broker> brokers) {
        this(distributor);
        putBrokers(brokers);
        return;
    }
}
